/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_GETOPTS_H_
#define FUNEX_GETOPTS_H_

#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#define FX_GETOPT_MAX         (64)
#define FX_GETOPT_DELIMCHRS   "_,:"
#define FX_GETOPT_HASARGCHRS  ":="

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * State-full wrapper over 'getopt_long' functionality.
 */
struct funex_getopts {
	/* Parsed arguments */
	int argc;
	char **argv;

	/* References to getopt globals */
	char *optarg;
	int optind;
	int opterr;
	int optopt;
	int longindex;

	/* Last returned value */
	int optchr;

	/* User provided def string */
	char *optdefs;

	/* Decoded desc string into getopt representation */
	size_t optcount;
	char   optstring[FX_GETOPT_MAX * 4];
	struct option longopts[FX_GETOPT_MAX];

	/* User's private pointer */
	void *userp;
};
typedef struct funex_getopts  funex_getopts_t;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_getopts_init(funex_getopts_t *, int, char *[], const char *);

void funex_getopts_destroy(funex_getopts_t *);

int funex_getopts_lookupnextopt(funex_getopts_t *, int *c);

int funex_getopts_lookupnextarg(funex_getopts_t *, char const **arg);

const char *funex_getopts_lookuparg(const funex_getopts_t *);

#endif /* FUNEX_GETOPTS_H_ */





/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <sys/types.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "version.h"
#include "globals.h"
#include "system.h"
#include "logging.h"

#define globals (funex_globals)

/*
 * Logging facility configurations, based on global config settings. May have
 * one of the following logging-params configurations:
 *
 * 1. Default
 * 2. Post args parsing
 * 3. Daemon mode
 * 4. Command-line mode (printer=TRUE)
 */
static void logger_params(int printer, int *p_flags,
                          int *p_level, int *p_debug, size_t *p_rate)
{
	int log_flags, log_level, log_debug;
	size_t log_rate;

	log_flags   = 0;
	log_level   = FX_LL_INFO;
	log_debug   = FX_LL_NONE;
	log_rate    = FX_LR_DEFAULT;

	if (!printer) {
		log_flags |= FX_LF_PROGNAME | FX_LF_SEVERITY;
	} else {
		log_flags |= FX_LF_STDOUT;
	}

	if (globals.proc.debug > 0) {
		log_debug = FX_LL_DEBUG1;
		log_flags |= FX_LF_FILINE;
	}
	if (globals.proc.debug > 1) {
		log_debug = FX_LL_DEBUG1;
		log_rate  = FX_LR_UNLIMITED;
	}
	if (globals.proc.debug > 2) {
		log_debug = FX_LL_DEBUG2;
	}
	if (globals.log.showfunc) {
		log_flags |= FX_LF_FUNC;
	}
	if (globals.log.showloc) {
		log_flags |= FX_LF_FILINE;
	}
	if (globals.log.syslog) {
		log_flags |= FX_LF_SYSLOG;
	}
	if (globals.log.showprog) {
		log_flags |= FX_LF_PROGNAME;
	}
	if (globals.proc.nodaemon) {
		log_flags |= FX_LF_STDOUT;
	}
	if (globals.log.verbose) {
		log_level = FX_MIN(log_level, FX_LL_INFO);
	}
	if (globals.log.quiet) {
		log_level = FX_LL_ERROR;
	}

	*p_flags = log_flags;
	*p_level = log_level;
	*p_debug = log_debug;
	*p_rate  = log_rate;
}

static void log_first_line(const fx_logger_t *logger)
{
	pid_t pid;
	const char *prog;
	const char *vers;
	const char *hdrs = "++++++";

	pid  = getpid();
	prog = funex_globals.prog.name;
	vers = funex_version();

	fx_info("%s %s %s %s", hdrs, prog, vers, hdrs);
	fx_info("pid=%d", pid);
	fx_info("log_level=%d log_debug=%d log_flags=%#x log_rate=%ld",
	        logger->log_level, logger->log_debug,
	        logger->log_flags, (long)logger->log_rate);
}

/* Logger's default setting */
void funex_setup_logger(void)
{
	int log_flags, log_level, log_debug;
	size_t log_rate;
	const char  *ident  = globals.subc.name;
	fx_logger_t *logger = fx_default_logger;

	logger_params(0, &log_flags, &log_level, &log_debug, &log_rate);
	fx_logger_setup(logger, ident, log_level, log_debug, log_flags, log_rate);
}

/* Logger for printing utility */
void funex_setup_plogger(void)
{
	size_t log_rate;
	int log_flags, log_level, log_debug;
	fx_logger_t *logger = fx_default_logger;

	logger_params(1, &log_flags, &log_level, &log_debug, &log_rate);
	fx_logger_setup(logger, NULL, log_level, log_debug, log_flags, log_rate);
}


/*
 * Logging sub-system. Daemon's runtime logging uses syslog.
 * Here we initialize global logger object, sub-components my define and use
 * derived loggers.
 */
static char fx_s_syslog_open = FNX_FALSE;

void funex_open_syslog(void)
{
	int option, facility;
	const char *ident;

	ident    = funex_globals.prog.name;
	option   = LOG_NDELAY | LOG_PID;
	facility = LOG_USER;

	openlog(ident, option, facility);
	setlogmask(LOG_UPTO(LOG_DEBUG));
	atexit(funex_close_syslog);

	fx_s_syslog_open = FNX_TRUE;
}

void funex_close_syslog(void)
{
	if (fx_s_syslog_open) {
		fflush(stdout);
		fflush(stderr);
		closelog();
		fx_s_syslog_open = FNX_FALSE;
	}
}

/* Logger setting for daemon process */
void funex_setup_dlogger(void)
{
	int log_flags, log_level, log_debug;
	size_t log_rate;
	fx_logger_t *logger = fx_default_logger;

	logger_params(0, &log_flags, &log_level, &log_debug, &log_rate);

	if (log_flags & FX_LF_SYSLOG) {
		funex_open_syslog();
	}

	fx_logger_setup(logger, NULL, log_level, log_debug, log_flags, log_rate);
	log_first_line(logger);
}

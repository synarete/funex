/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "fnxdefs.h"
#include "fnxinfra.h"
#include "fnxcore.h"

#include "version.h"
#include "system.h"
#include "parse.h"
#include "getopts.h"
#include "globals.h"
#include "cmdline.h"

#define globals (funex_globals)

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_show_cmdhelp_usage(const funex_cmdent_t *cmdent, int pad)
{
	int c, n, has_newline = 0;
	const char *s;
	const char *prog = globals.prog.name;

	if (cmdent->usage == NULL) {
		return;
	}

	n = 0;
	s = cmdent->usage;
	while ((c = *s++) != '\0') {
		if (c == '@') {
			if (n++ > 0) {
				printf("%*c\n", pad, ' ');
			}
			printf("%s", prog);
			if (cmdent->name) {
				printf(" %s", cmdent->name);
			}
		} else if (c == '%') {
			printf("%s", prog);
		} else {
			printf("%c", c);
		}
		if (c == '\n') {
			has_newline = 1;
		}
	}
	if (!has_newline) {
		printf("%s\n", "");
	}
}

void funex_show_cmdhelp_options(const funex_cmdent_t *cmdent)
{
	const char *s;
	const char *ident = "    ";
	FILE *fp = stdout;

	printf("%s", ident);
	s = cmdent->optdesc;
	while (*s) {
		if (*s == '\n') {
			printf("\n%s", ident);
		} else {
			putc(*s, fp);
		}
		++s;
	}
	printf("\n");
}

static void funex_show_help(const funex_cmdent_t *cmdent)
{
	const char *s;

	if (cmdent->usage != NULL) {
		funex_show_cmdhelp_usage(cmdent, 1);
	}
	if ((s = cmdent->vdesc) != NULL) {
		printf("\n%s\n", s);
	} else if ((s = cmdent->desc) != NULL) {
		printf("\n%s\n", s);
	}
	if (cmdent->optdesc != NULL) {
		printf("\n%s:\n", "options");
		funex_show_cmdhelp_options(cmdent);
	}
	if ((s = cmdent->epilog) != NULL) {
		printf("\n%s\n", s);
	}
}

void funex_show_cmdhelp_and_goodbye(const funex_cmdent_t *cmdent)
{
	funex_show_help(cmdent);
	funex_goodbye();
}

void funex_show_cmdhelp_and_goodbye2(const struct funex_getopts *opt)
{
	funex_show_cmdhelp_and_goodbye((const funex_cmdent_t *)(opt->userp));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_show_version(int all)
{
	int c, dash;
	const char *prog_name;
	const char *version_str;
	const char *build_time;
	const char *str;

	prog_name   = globals.prog.name;
	version_str = funex_version();
	build_time  = funex_buildtime();

	printf("%s %s", prog_name, version_str);
	if (all) {
		printf("%c", ' ');
		str = build_time;
		dash = 0;
		while ((c = *str++) != '\0') {
			if (isspace(c) || !isascii(c) || !isprint(c)) {
				dash += 1;
				if (dash == 1) {
					printf("%c", '-');
				}
			} else {
				dash = 0;
				printf("%c", c);
			}
		}
	}
	printf("%s\n", "");
}

void funex_show_version_and_goodbye(void)
{
	funex_show_version(globals.opt.all);
	funex_goodbye();
}

void funex_show_license(void)
{
	printf("\n%s\n", funex_license());
}

void funex_show_license_and_goodbye(void)
{
	funex_show_license();
	funex_goodbye();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


int funex_getnextopt(funex_getopts_t *opt)
{
	int rc, c;

	rc = funex_getopts_lookupnextopt(opt, &c);
	if (rc != 0) {
		c = 0;
	} else {
		if ((c == ':') || (c == '?')) {
			funex_die_unknown_opt(opt, c);
		}
	}
	return c;
}

const char *funex_getoptarg(funex_getopts_t *opt, const char *argname)
{
	const char *arg;

	arg = funex_getopts_lookuparg(opt);
	if (arg == NULL) {
		funex_die_missing_arg(argname);
	}
	return arg;
}

const char *funex_getnextarg(funex_getopts_t *opt,
                             const char *argname, int flags)
{
	int rc;
	const char *arg = NULL;
	const char *arg_extra = NULL;

	rc = funex_getopts_lookupnextarg(opt, &arg);
	if (rc == 0) {
		if (flags & FUNEX_ARG_NONE) {
			funex_die_redundant_arg(arg);
		}
		if (flags & FUNEX_ARG_LAST) {
			rc = funex_getopts_lookupnextarg(opt, &arg_extra);
			if (rc == 0) {
				funex_die_redundant_arg(arg_extra);
			}
		}
	} else {
		if (flags & FUNEX_ARG_REQ) {
			funex_die_missing_arg(argname);
		}
	}
	return arg;
}

void funex_parse_cmdargs(int argc, char *argv[],
                         const funex_cmdent_t *cmdent,
                         funex_getopts_fn getopt_hook)
{
	const char *optdef;
	funex_getopts_t *opt;

	if (getopt_hook != NULL) {
		optdef = (cmdent && cmdent->optdef) ? cmdent->optdef : "";
		opt = (funex_getopts_t *)malloc(sizeof(*opt));
		if (opt == NULL) {
			funex_dief("no-malloc: size=%d", (int)sizeof(*opt));
		}

		funex_getopts_init(opt, argc, argv, optdef);
		opt->userp = (void *)cmdent;

		getopt_hook(opt);

		funex_getopts_destroy(opt);
		free(opt);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_die_unimplemented(const char *str)
{
	funex_dief("unimplemented: %s", str);
}

void funex_die_missing_arg(const char *arg_name)
{
	if (arg_name != NULL) {
		funex_dief("missing: %s", arg_name);
	} else {
		funex_dief("missing-arg");
	}
}

void funex_die_illegal_arg(const char *arg_name, const char *arg_value)
{
	if (arg_value != NULL) {
		funex_dief("illegal <%s>: %s", arg_name, arg_value);
	} else {
		funex_dief("illegal: %s", arg_name);
	}
}

void funex_die_redundant_arg(const char *arg_name)
{
	if (arg_name != NULL) {
		funex_dief("redundant-arg: %s", arg_name);
	} else {
		funex_dief("redundant-arg");
	}
}

void funex_die_unknown_opt(const funex_getopts_t *opt, int c)
{
	char oc;
	const char *ext = "try `--help' for more information";

	oc = (char)opt->optopt;
	if (((c == ':') || (c == '?')) && fx_chr_isprint(oc)) {
		funex_dief("unknown option: -%c\n%s", oc, ext);
	} else {
		funex_dief("illegal or missing option\n%s", ext);
	}
}

void funex_die_illegal_volsize(const char *arg, loff_t sz)
{
	const loff_t minsz = (loff_t)FNX_VOLNBK_MIN * FNX_BLKSIZE;
	const loff_t maxsz = (loff_t)FNX_VOLNBK_MAX * FNX_BLKSIZE;
	const loff_t minm  = (minsz / (loff_t)FNX_MEGA);
	const loff_t maxt  = (maxsz / (loff_t)FNX_TERA);

	if (arg == NULL) {
		funex_dief("illegal vol-size: %ld (min=%ldM max=%ldT)", sz, minm, maxt);
	} else {
		funex_dief("illegal vol-size: %s (min=%ldM max=%ldT)", arg, minm, maxt);
	}
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_CMDLINE_H_
#define FUNEX_CMDLINE_H_


/* Sub-commands control flags */
#define FUNEX_CMD_MAIN      FX_BITFLAG(0)
#define FUNEX_CMD_GOPTION   FX_BITFLAG(3)
#define FUNEX_CMD_PRIVATE   FX_BITFLAG(4)

/* Arguments control flags */
#define FUNEX_ARG_REQ       FX_BITFLAG(1)
#define FUNEX_ARG_LAST      FX_BITFLAG(2)
#define FUNEX_ARG_NONE      FX_BITFLAG(3)
#define FUNEX_ARG_REQLAST   FUNEX_ARG_REQ | FUNEX_ARG_LAST

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* hooks' typedefs */
struct funex_getopts;
typedef void (*funex_main_fn)(int, char **);
typedef void (*funex_exec_fn)(void);
typedef void (*funex_getopts_fn)(struct funex_getopts *);

/*
 * Sub-commands definition + reference entry
 */
struct funex_cmdent {
	funex_getopts_fn gopt;  /* Parse-options hook */
	funex_exec_fn  exec;    /* Execution hook */
	const int   flags;      /* Control flags */
	const char *name;       /* Command name */
	const char *alias;      /* Name-alias (optional) */
	const char *usage;      /* Program's usage description string */
	const char *desc;       /* Text to display before the options help */
	const char *vdesc;      /* More verbose command description help */
	const char *optdef;     /* Options definitions for getopt wrapper */
	const char *optdesc;    /* Options description help string */
	const char *epilog;     /* Text to display after options help */
};
typedef struct funex_cmdent  funex_cmdent_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_show_cmdhelp_usage(const funex_cmdent_t *, int);

void funex_show_cmdhelp_options(const funex_cmdent_t *);

void funex_show_cmdhelp_and_goodbye(const funex_cmdent_t *);

void funex_show_cmdhelp_and_goodbye2(const struct funex_getopts *);

void funex_show_version(int all);

void funex_show_version_and_goodbye(void);

void funex_show_license(void);

void funex_show_license_and_goodbye(void);

void funex_parse_cmdargs(int, char *[], const funex_cmdent_t *,
                         funex_getopts_fn);


int funex_getnextopt(struct funex_getopts *);

const char *funex_getoptarg(struct funex_getopts *, const char *);

const char *funex_getnextarg(struct funex_getopts *, const char *, int);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_die_unimplemented(const char *);

void funex_die_missing_arg(const char *);

void funex_die_illegal_arg(const char *, const char *);

void funex_die_redundant_arg(const char *);

void funex_die_unknown_opt(const struct funex_getopts *, int c);

void funex_die_illegal_volsize(const char *, loff_t);

#endif /* FUNEX_CMDLINE_H_ */



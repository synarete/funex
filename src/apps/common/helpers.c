/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "fnxdefs.h"
#include "fnxuser.h"

#include "fnxinfra.h"
#include "fnxcore.h"
#include "system.h"
#include "helpers.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_vnode_t *funex_newvobj(fx_vtype_e vtype)
{
	fx_vnode_t *vnode;
	vnode = fx_new_vobj(vtype, NULL);
	if (vnode == NULL) {
		funex_dief("no-new-vobj: vtype=%d", vtype);
	}
	return vnode;
}

void funex_delvobj(fx_vnode_t *vnode)
{
	fx_del_vobj(vnode, NULL);
}

void funex_dexport(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_dexport_vobj(vnode, hdr);
}

void funex_dimport(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_dimport_vobj(vnode, hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_vio_t *vio_new(void)
{
	fx_vio_t *vio;

	vio = fx_xmalloc(sizeof(*vio), FX_NOFAIL);
	fx_vio_init(vio);
	return vio;
}

static void vio_del(fx_vio_t *vio)
{
	fx_vio_destroy(vio);
	fx_free(vio, sizeof(*vio));
}

fx_dsec_t *funex_get_dsec(fx_vio_t *vio, fx_size_t sec_idx)
{
	int rc;
	fx_lba_t lba;
	fx_dblk_t *dblk;
	void *ptr = NULL;

	lba = sec_to_lba(sec_idx);
	rc = vio->mgetp(vio, lba, &ptr);
	if (rc != 0) {
		funex_dief("mgetp-error: lba=%#lx err=%d", lba, -rc);
	}
	dblk = (fx_dblk_t *)ptr;
	return dblk_to_dseg(dblk);
}

void funex_put_dsec(fx_vio_t *vio, const fx_dsec_t *dsec, int now)
{
	int rc;
	fx_lba_t lba;
	fx_off_t off;
	fx_bkcnt_t cnt;
	char const *beg, *pos;

	cnt = fx_bytes_to_nbk(sizeof(*dsec));
	beg = (const char *)(vio->maddr);
	pos = (const char *)dsec;
	off = (pos - beg);
	rc  = vio->off2lba(vio, off, &lba);
	if (rc != 0) {
		funex_dief("no-put-dsec: off=%ld err=%d", off, -rc);
	}
	rc = vio->msync(vio, lba, cnt, now);
	if (rc != 0) {
		funex_dief("msync-error: lba=%lu err=%d", lba, -rc);
	}
}

static void verify_sec_idx(fx_size_t sec)
{
	if (sec >= FNX_SEC_MAX) {
		funex_dief("illegal-sec-idx: %#lx", sec);
	}
}

void funex_read_dsec(fx_vio_t *vio, fx_size_t sec, fx_dsec_t *dsec)
{
	int rc;
	fx_lba_t lba;
	fx_bkcnt_t cnt;

	verify_sec_idx(sec);
	lba = sec_to_lba(sec);
	cnt = fx_bytes_to_nbk(sizeof(*dsec));
	rc = vio->read(vio, dsec, lba, cnt);
	if (rc != 0) {
		funex_dief("no-read: lba=%lu err=%d", lba, -rc);
	}
}

void funex_write_dsec(fx_vio_t *vio, fx_size_t sec, const fx_dsec_t *dsec)
{
	int rc;
	fx_lba_t lba;
	fx_bkcnt_t cnt;

	verify_sec_idx(sec);
	lba = sec_to_lba(sec);
	cnt = fx_bytes_to_nbk(sizeof(*dsec));
	rc = vio->write(vio, dsec, lba, cnt);
	if (rc != 0) {
		funex_dief("no-write: lba=%lu err=%d", lba, -rc);
	}
}

static void vio_open(fx_vio_t *vio, const char *path, int flags)
{
	int rc;

	rc = vio->open(vio, path, 0, 0, flags);
	if (rc != 0) {
		funex_dief("vio-open-failed: %s err=%d", path, -rc);
	}
	rc = vio->mmap(vio);
	if (rc != 0) {
		funex_dief("vio-mmap-failure: %s err=%d", path, -rc);
	}
}

fx_vio_t *funex_open_vio(const char *path, int flags)
{
	fx_vio_t *vio;

	vio = vio_new();
	vio_open(vio, path, flags | FX_VIOF_SAVEPATH);
	return vio;
}

static void
vio_create(fx_vio_t *vio, const char *path, fx_bkcnt_t bkcnt)
{
	int rc, flags;

	flags   = FX_VIOF_RDWR; /* XXX */
	rc = vio->create(vio, path, 0, bkcnt, flags);
	if (rc != 0) {
		funex_dief("create-failed: bkcnt=%ld path=%s", bkcnt, path);
	}
	rc = vio->mmap(vio);
	if (rc != 0) {
		funex_dief("mmap-failure %s", path);
	}
}

fx_vio_t *funex_create_vio(const char *path, fx_bkcnt_t bkcnt)
{
	fx_vio_t *vio;

	vio = vio_new();
	vio_create(vio, path, bkcnt);
	return vio;
}

static void vio_close(fx_vio_t *vio)
{
	int rc;

	rc = vio->mflush(vio, FNX_TRUE);
	if (rc != 0) {
		funex_dief("mflush-failure: %s err=%d", vio->path, -rc);
	}
	rc = vio->sync(vio);
	if (rc != 0) {
		funex_dief("sync-failure: %s err=%d", vio->path, -rc);
	}
	vio->close(vio);
}

void funex_close_vio(fx_vio_t *vio)
{
	vio_close(vio);
	vio_del(vio);
}


fx_dsec_t *funex_new_dsec(void)
{
	fx_dsec_t *dsec;

	dsec = (fx_dsec_t *)calloc(1, sizeof(*dsec));
	if (dsec == NULL) {
		funex_dief("no-malloc-dsec: size=%zu", sizeof(*dsec));
	}
	return dsec;
}

void funex_del_dsec(fx_dsec_t *dsec)
{
	free(dsec);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#define BUFSZ (2048)

static void print_repr(const fx_substr_t *ss, const char *head)
{
	fx_substr_t vs;
	fx_substr_pair_t sp;
	const char *pre = head ? head : "";
	const char *dot = head ? "."  : "";

	fx_substr_split_chr(ss, '\n', &sp);
	while (sp.first.len > 0) {
		fx_substr_strip_ws(&sp.first, &vs);
		if (vs.len) {
			fx_info("%s%s%.*s", pre, dot, (int)vs.len, vs.str);
		}
		fx_substr_split_chr(&sp.second, '\n', &sp);
	}
}

void funex_print_times(const fx_times_t *times, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_times(&ss, times, NULL);
	print_repr(&ss, pref);
}

void funex_print_fsattr(const fx_fsattr_t *fsattr, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_fsattr(&ss, fsattr, NULL);
	print_repr(&ss, pref);
}

void funex_print_fsstat(const fx_fsstat_t *fsstat, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_fsstat(&ss, fsstat, NULL);
	print_repr(&ss, pref);
}

void funex_print_vstats(const fx_vstats_t *vstats, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_vstats(&ss, vstats, NULL);
	print_repr(&ss, pref);
}

void funex_print_iostat(const fx_iostat_t *iostat, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_iostat(&ss, iostat, NULL);
	print_repr(&ss, pref);
}

void funex_print_opstat(const fx_opstat_t *opstat, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_opstat(&ss, opstat, NULL);
	print_repr(&ss, pref);
}

void funex_print_layout(const fx_layout_t *layout, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_layout(&ss, layout, NULL);
	print_repr(&ss, pref);
}

void funex_print_super(const fx_super_t *super, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_super(&ss, super, NULL);
	print_repr(&ss, pref);
}

void funex_print_iattr(const fx_iattr_t *iattr, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_iattr(&ss, iattr, NULL);
	print_repr(&ss, pref);
}

void funex_print_inode(const fx_inode_t *inode, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_inode(&ss, inode, NULL);
	print_repr(&ss, pref);
}

void funex_print_dir(const fx_dir_t *dir, const char *pref)
{
	fx_substr_t ss;
	char buf[BUFSZ] = "";

	fx_substr_init_rw(&ss, buf, 0, sizeof(buf));
	fx_repr_dir(&ss, dir, NULL);
	print_repr(&ss, pref);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_show_pseudo(const char *head, const char *name1,
                       const char *name2, const char *name3, int use_pref)
{
	int rc, fd = -1;
	size_t bsz, nrd = 0;
	char *path, *buf;
	fx_substr_t ss;
	const char *pref = NULL;

	if (use_pref) {
		pref = name3 ? name3 : name2 ? name2 : name1;
	}
	bsz = FNX_BLKSIZE;
	buf = (char *)malloc(bsz);
	if (buf == NULL) {
		funex_dief("no-memory: bsz=%u err=%d", bsz, errno);
	}
	path = fnx_makepath(head, name1, name2, name3, NULL);
	if (path == NULL) {
		funex_dief("no-memory: err=%d", errno);
	}
	rc = fnx_open(path, O_RDONLY, 0, &fd);
	if (rc != 0) {
		funex_dief("no-open: %s err=%d", path, -rc);
	}
	rc = fnx_read(fd, buf, bsz, &nrd);
	if (rc != 0) {
		funex_dief("read-error: %s err=%d", path, -rc);
	}
	rc = fnx_close(fd);
	if (rc != 0) {
		funex_dief("close-error: %s err=%d", path, -rc);
	}
	fnx_freepath(path);

	fx_substr_init_rd(&ss, buf, nrd);
	print_repr(&ss, pref);
	free(buf);
}

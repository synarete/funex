/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <err.h>
#include <limits.h>
#include <locale.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "balagan.h"
#include "fnxinfra.h"
#include "fnxcore.h"

#include "version.h"
#include "logging.h"
#include "getopts.h"
#include "globals.h"
#include "system.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
FX_ATTR_NORETURN void funex_exit(int rc)
{
	exit(rc);
}

FX_ATTR_NORETURN void funex_exiterr(void)
{
	funex_exit(EXIT_FAILURE);
}

FX_ATTR_NORETURN void funex_goodbye(void)
{
	fflush(stdout);
	fflush(stderr);
	funex_exit(EXIT_SUCCESS);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

FX_ATTR_NORETURN void funex_die(void)
{
	fx_die();
}

static void funex_die_errx(const char *fmt, ...)
{
	va_list ap;

	program_invocation_name = program_invocation_short_name;

	va_start(ap, fmt);
	verrx(EXIT_FAILURE, fmt, ap);
	va_end(ap);
}

FX_ATTR_NORETURN void funex_dief(const char *fmt, ...)
{
	va_list ap;
	size_t n;
	const char *subname;
	char msg[1024];

	n = sizeof(msg);
	memset(msg, 0, n);

	va_start(ap, fmt);
	vsnprintf(msg, n - 1, fmt, ap);
	va_end(ap);

	subname = funex_globals.subc.name;
	if ((subname != NULL) && strlen(subname)) {
		funex_die_errx("%s: %s", subname, msg);
	} else {
		funex_die_errx("%s", msg);
	}

	/* Never get here */
	funex_die();
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

FX_ATTR_NORETURN void funex_fatal_error(int errnum, const char *msg)
{
	char bt_msg[1024];

	fx_addr2line_backtrace(bt_msg, sizeof(bt_msg) - 1);

	fx_critical("%s errno=%d", msg, errnum);
	fx_critical("%s", bt_msg);
	fx_critical("version=%s buildtime=<%s>",
	            funex_version(), funex_buildtime());

	funex_close_syslog();
	funex_die();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

char *funex_username(void)
{
	char *str;

	str = getenv("USER");
	if (str != NULL) {
		str = strdup(str);
	}
	return str;
}

char *funex_selfexe(void)
{
	char *exe;
	const char *name;

	name = fx_execname;
	if (name == NULL) {
		name = program_invocation_name;
	}
	exe = strdup(name);
	return exe;
}

#ifndef SYSCONFDIR
#define SYSCONFDIR "../etc/"
#endif

char *funex_sysconfdir(void)
{
	size_t bufsz;
	char *ptr, *self, *buf, *rpath;
	const char *suffix = SYSCONFDIR;

	buf = rpath = NULL;
	self = funex_selfexe();
	if (self == NULL) {
		goto out;
	}
	ptr = strrchr(self, '/');
	if (ptr == NULL) {
		goto out;
	}
	*ptr = '\0';

	bufsz = PATH_MAX;
	buf = (char *)calloc(1, bufsz);
	if (buf == NULL) {
		goto out;
	}

	snprintf(buf, bufsz, "%s/%s", self, suffix);
	rpath = realpath(buf, NULL);

out:
	if (self != NULL) {
		free(self);
	}
	if (buf != NULL) {
		free(buf);
	}
	return rpath;
}

char *funex_auxdsock(void)
{
	const char *path;
#ifdef RUNDIR
	path = RUNDIR "/" FNX_AUXDSOCK;
#else
	path = "/tmp/" FNX_AUXDSOCK;
#endif
	return strdup(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_init_defaults(int argc, char *argv[])
{
	umask(077);
	setlocale(LC_ALL, "");
	fx_initexecname();

	funex_globals_setup();
	atexit(funex_globals_cleanup);

	funex_globals.prog.name = program_invocation_short_name;
	funex_globals.prog.argc = argc;
	funex_globals.prog.argv = argv;
	funex_globals.subc.name = NULL;
}

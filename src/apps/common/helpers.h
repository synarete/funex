/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_HELPERS_H_
#define FUNEX_HELPERS_H_


#define funex_help_and_goodbye(opt) \
	funex_show_cmdhelp_and_goodbye((const funex_cmdent_t *)(opt->userp))

#define funex_set_debug_level(opt) \
	funex_globals_set_debug(funex_getoptarg(opt, "debug"))


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_vnode_t *funex_newvobj(fx_vtype_e);

void funex_delvobj(fx_vnode_t *);

void funex_dexport(const fx_vnode_t *, fx_header_t *);

void funex_dimport(fx_vnode_t *, const fx_header_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_vio_t *funex_open_vio(const char *, int);

fx_vio_t *funex_create_vio(const char *, fx_bkcnt_t);

void funex_close_vio(fx_vio_t *);

fx_dsec_t *funex_get_dsec(fx_vio_t *, fx_size_t);

void funex_put_dsec(fx_vio_t *, const fx_dsec_t *, int);

void funex_read_dsec(fx_vio_t *, fx_size_t, fx_dsec_t *);

void funex_write_dsec(fx_vio_t *, fx_size_t, const fx_dsec_t *);


fx_dsec_t *funex_new_dsec(void);

void funex_del_dsec(fx_dsec_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_print_times(const fx_times_t *, const char *);

void funex_print_fsattr(const fx_fsattr_t *, const char *);

void funex_print_fsstat(const fx_fsstat_t *, const char *);

void funex_print_vstats(const fx_vstats_t *, const char *);

void funex_print_iostat(const fx_iostat_t *, const char *);

void funex_print_opstat(const fx_opstat_t *, const char *);

void funex_print_layout(const fx_layout_t *, const char *);

void funex_print_super(const fx_super_t *, const char *);

void funex_print_iattr(const fx_iattr_t *, const char *);

void funex_print_inode(const fx_inode_t *, const char *);

void funex_print_dir(const fx_dir_t *dir, const char *prefix);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_show_pseudo(const char *, const char *,
                       const char *, const char *, int);

#endif /* FUNEX_HELPERS_H_ */



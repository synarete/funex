/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_PARSE_H_
#define FUNEX_PARSE_H_

/* Key/Value limits (including nul) */
#define FNX_KEYMAX      (FNX_NAME_MAX + 1)
#define FNX_VALMAX      (FNX_PATH_MAX / 2)


int funex_parse_bool(const char *, int *);

int funex_parse_size(const char *, loff_t *);

int funex_parse_long(const char *, long *);

int funex_parse_uid(const char *, fx_uid_t *);

int funex_parse_gid(const char *, fx_gid_t *);

int funex_parse_uuid(const char *, fx_uuid_t *);

int funex_parse_mntops(const char *, fx_mntf_t *);


int funex_parse_kv(const char *, char[FNX_KEYMAX], char[FNX_VALMAX]);


#endif /* FUNEX_PARSE_H_ */



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/capability.h>
#include <unistd.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "system.h"
#include "globals.h"
#include "daemon.h"

#define globals (funex_globals)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Daemon operations:
 */
int funex_forknwait(void (*child_fn)(void), pid_t *p_pid)
{
	int rc, status = 0;
	pid_t pid;
	struct rlimit rlm;

	funex_getrlimit_nproc(&rlm);

	rc  = 0;
	pid = fork();
	if (pid > 0) {  /* Parent process */
		waitpid(pid, &status, 0);
		rc = WEXITSTATUS(status);
		if (!WIFEXITED(status)) {
			rc = -1;
		} else {
			*p_pid = pid;
		}
	} else if (pid == 0) { /* Child process  */
		child_fn();
	} else {
		fx_panic("no-fork RLIMIT_NPROC.rlim_cur=%ju", rlm.rlim_cur);
	}
	return rc;
}

static void funex_pinfo(const char *label)
{
	pid_t pid;
	char *wdir;

	pid  = getpid();
	wdir = get_current_dir_name();

	fx_trace1("%s pid=%d work-dir=%s", label, (int)pid, wdir);
	free(wdir);
}

static void funex_daemonize2(int dofork, fx_error_cb err_cb)
{
	int rc, nochdir, noclose;

	/* Do actual daemonization via syscall */
	if (dofork) {
		nochdir = noclose = 0;
		rc = daemon(nochdir, noclose);
		if (rc != 0) {
			fx_panic("daemon-failed rc=%d", rc);
		}
		funex_pinfo("daemonized");
	} else {
		funex_pinfo("non-daemon-mode");
	}

	/* ReBind panic call-back for daemon */
	fx_set_panic_callback(err_cb);
}

void funex_daemonize(void)
{
	const int dofork = globals.proc.nodaemon ? 0 : 1;

	/* Daemonize only if explicit global set; in such case, have syslog */
	if (dofork) {
		globals.log.syslog = FNX_TRUE;
	}
	funex_daemonize2(dofork, funex_fatal_error);
}


/*
 * Become a daemon process: fork child process, which in turn will do the
 * actual daemonization and do the rest of initialization & execution.
 */
void funex_forkd(void (*start)(void))
{
	int rc;
	pid_t pid = (pid_t)(-1);

	errno = 0;
	rc  = funex_forknwait(start, &pid);
	if (rc != 0) {
		funex_exiterr();
	}
	fx_info("forked-child pid=%d", (int)pid);
}

static void funex_execvp(const char *file, char *const argv[])
{
	int rc;

	errno = 0;
	rc = execvp(argv[0], argv);
	if (rc != 0) {
		funex_dief("no-execvp: %s (errno=%d)", file, errno);
	}
}

void funex_execsub(int argc, char *argv[], const char *subcmd)
{
	size_t len;
	char *file;

	if (argc >= 2) {
		len  = strlen(argv[0]) + strlen(subcmd) + 2;
		file = (char *)malloc(len);
		if (file == NULL) {
			funex_dief("no-malloc: size=%d", (int)len);
		}

		snprintf(file, len, "%s-%s", argv[0], subcmd);
		argv[1] = file;
		funex_execvp(file, argv + 1);
		free(file);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Resource operations:
 */
typedef __rlimit_resource_t fx_rlimit_resource_t;

static const char *rlimit_name(fx_rlimit_resource_t rsrc)
{
	const char *s = NULL;

	switch (rsrc) {
		case RLIMIT_CPU:
			s = "RLIMIT_CPU";
			break;
		case RLIMIT_FSIZE:
			s = "RLIMIT_FSIZE";
			break;
		case RLIMIT_DATA:
			s = "RLIMIT_DATA";
			break;
		case RLIMIT_STACK:
			s = "RLIMIT_STACK";
			break;
		case RLIMIT_CORE:
			s = "RLIMIT_CORE";
			break;
		case RLIMIT_RSS:
			s = "RLIMIT_RSS";
			break;
		case RLIMIT_NOFILE:
			s = "RLIMIT_NOFILE";
			break;
		case RLIMIT_AS:
			s = "RLIMIT_AS";
			break;
		case RLIMIT_NPROC:
			s = "RLIMIT_NPROC";
			break;
		case RLIMIT_MEMLOCK:
			s = "RLIMIT_MEMLOCK";
			break;
		case RLIMIT_LOCKS:
			s = "RLIMIT_LOCKS";
			break;
		case RLIMIT_SIGPENDING:
			s = "RLIMIT_SIGPENDING";
			break;
		case RLIMIT_MSGQUEUE:
			s = "RLIMIT_MSGQUEUE";
			break;
		case RLIMIT_NICE:
			s = "RLIMIT_NICE";
			break;
		case RLIMIT_RTPRIO:
			s = "RLIMIT_RTPRIO";
			break;
		case RLIMIT_RTTIME:
			s = "RLIMIT_RTTIME";
			break;
		case RLIM_NLIMITS:
		default:
			s = "RLIMIT_XXX";
			break;
	}
	return s;
}


static void
funex_getrlimit(fx_rlimit_resource_t rsrc, struct rlimit *rlm)
{
	int rc;

	rc = getrlimit(rsrc, rlm);
	if (rc != 0) {
		fx_panic("getrlimit-failure: %s err=%d", rlimit_name(rsrc), errno);
	}
}

static void
funex_setrlimit(fx_rlimit_resource_t rsrc, const struct rlimit *rlm)
{
	int rc;

	rc = setrlimit(rsrc, rlm);
	if (rc != 0) {
		fx_panic("setrlimit-failure: %s err=%d", rlimit_name(rsrc), errno);
	}
}

static void
funex_report_rlimit(fx_rlimit_resource_t rsrc, const struct rlimit *rlm)
{
	if (rlm->rlim_max == RLIM_INFINITY) {
		fx_info("%s rlim_cur=%ju rlim_max=INFINITY",
		        rlimit_name(rsrc), rlm->rlim_cur);
	} else {
		fx_info("%s rlim_cur=%ju rlim_max=%ju",
		        rlimit_name(rsrc), rlm->rlim_cur, rlm->rlim_max);
	}
}

void funex_getrlimit_as(struct rlimit *rlm)
{
	funex_getrlimit(RLIMIT_AS, rlm);
}

void funex_getrlimit_nproc(struct rlimit *rlm)
{
	funex_getrlimit(RLIMIT_NPROC, rlm);
}


void funex_setrlimit_core(rlim_t max_sz)
{
	fx_rlimit_resource_t rsrc = RLIMIT_CORE;
	struct rlimit rlim;

	funex_getrlimit(rsrc, &rlim);
	rlim.rlim_cur = fx_min(max_sz, rlim.rlim_max); /* 0 = No core */
	funex_setrlimit(rsrc, &rlim);
	funex_report_rlimit(rsrc, &rlim);
}




/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Signal operations:
 */
static void funex_sigaction_ignore(int signum)
{
	fx_info("Sigaction <%s>: ignore", fx_str_signum(signum));
	fx_sigaction_ignore(signum);
}

static void funex_sigaction_halt(int signum)
{
	fx_info("Sigaction <%s>: halt", fx_str_signum(signum));
	fx_sigaction_halt(signum);
}

static void funex_sigaction_abort(int signum)
{
	fx_info("Sigaction <%s>: abort", fx_str_signum(signum));
	fx_sigaction_abort(signum);
}

static void funex_sigaction_nocatch(int signum)
{
	fx_info("Sigaction <%s>: nocatch", fx_str_signum(signum));
}

void funex_register_sigactions(void)
{
	/*
	 * NB: SIGCHLD
	 * FUSE mount depends on SIGCHLD; do not ignore.
	 */
	funex_sigaction_ignore(SIGHUP);
	funex_sigaction_halt(SIGINT);
	funex_sigaction_halt(SIGQUIT);
	funex_sigaction_abort(SIGILL);
	funex_sigaction_ignore(SIGTRAP);
	funex_sigaction_nocatch(SIGABRT);
	funex_sigaction_halt(SIGBUS);
	funex_sigaction_nocatch(SIGFPE);
	funex_sigaction_nocatch(SIGKILL);
	funex_sigaction_ignore(SIGUSR1);
	funex_sigaction_abort(SIGSEGV);
	funex_sigaction_ignore(SIGUSR2);
	funex_sigaction_ignore(SIGPIPE);
	funex_sigaction_ignore(SIGALRM);
	funex_sigaction_halt(SIGTERM);
	funex_sigaction_abort(SIGSTKFLT);
	/*fx_sigaction_ignore(SIGCHLD);*/ /* FIXME */
	funex_sigaction_ignore(SIGCONT);
	funex_sigaction_nocatch(SIGSTOP);
	funex_sigaction_halt(SIGTSTP);
	funex_sigaction_halt(SIGTTIN);
	funex_sigaction_halt(SIGTTOU);
	funex_sigaction_ignore(SIGURG);
	funex_sigaction_halt(SIGXCPU);
	funex_sigaction_halt(SIGXFSZ);
	funex_sigaction_halt(SIGVTALRM);
	funex_sigaction_nocatch(SIGPROF);
	funex_sigaction_ignore(SIGWINCH);
	funex_sigaction_ignore(SIGIO);
	funex_sigaction_halt(SIGPWR);
	funex_sigaction_halt(SIGSYS);
}

/*
 * Sets signal-blocking mask for calling thread (main).
 */
void funex_sigblock(void)
{
	int rc;
	sigset_t sigset_th;

	sigemptyset(&sigset_th);
	sigaddset(&sigset_th, SIGHUP);
	sigaddset(&sigset_th, SIGINT);
	sigaddset(&sigset_th, SIGQUIT);
	sigaddset(&sigset_th, SIGTERM);
	sigaddset(&sigset_th, SIGTRAP);
	sigaddset(&sigset_th, SIGUSR1);
	sigaddset(&sigset_th, SIGUSR2);
	sigaddset(&sigset_th, SIGPIPE);
	sigaddset(&sigset_th, SIGALRM);
	sigaddset(&sigset_th, SIGCHLD);
	sigaddset(&sigset_th, SIGCONT);
	sigaddset(&sigset_th, SIGWINCH);
	sigaddset(&sigset_th, SIGIO);

	rc = pthread_sigmask(SIG_BLOCK, &sigset_th, NULL);
	if (rc != 0) {
		fx_panic("sigmask-failure rc=%d", rc);
	}
}

void funex_sigunblock(void)
{
	int rc;
	sigset_t sigset_th;

	sigfillset(&sigset_th);

	rc = pthread_sigmask(SIG_UNBLOCK, &sigset_th, NULL);
	if (rc != 0) {
		fx_panic("sigmask-failure rc=%d", rc);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_register_sighalt(fx_sigaction_cb cb)
{
	fx_set_sighalt_callback(cb);
}

void funex_report_siginfo(const void *ptr)
{
	const siginfo_t *si = (const siginfo_t *)ptr;

	fx_info("si_signo=%d si_errno=%d si_code=%d si_pid=%d si_uid=%d",
	        si->si_signo, si->si_errno, si->si_code,
	        si->si_pid, si->si_uid);
}

/* Report last signal, abort if fatal */
void funex_handle_lastsig(int signum, const void *ptr)
{
	if (signum != 0) {
		funex_report_siginfo(ptr);

		if ((signum == SIGBUS) ||
		    (signum == SIGFPE) ||
		    (signum == SIGSEGV)) {
			abort();
		}
	}
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int funex_capef_sys_admin(void)
{
	int rc, res;
	pid_t pid;
	cap_t cap = NULL;
	cap_flag_value_t flag;

	errno = 0;
	pid = getpid();
	cap = cap_get_pid(pid);
	if (cap == NULL) {
		return FNX_FALSE;
	}
	rc = cap_get_flag(cap, CAP_SYS_ADMIN, CAP_EFFECTIVE, &flag);
	if (rc != 0) {
		cap_free(cap);
		return FNX_FALSE;
	}
	res = (flag == CAP_SET);
	rc  = cap_free(cap);
	if (rc != 0) {
		fx_panic("cap_free-failure: cap=%p rc=%d", (void *)cap, rc);
	}
	return res;
}

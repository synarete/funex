/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>

#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxfusei.h"
#include "fnxserv.h"

#include "system.h"
#include "singleton.h"


/* Singleton-objects global instances */
fx_server_t *funex_server  = NULL;
fx_mntsrv_t *funex_mntsrv = NULL;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void *sobject_new(size_t size)
{
	size_t nbks;
	void *sobj = NULL;

	nbks = fx_bytes_to_nbk(size);
	sobj = fx_mmap_bks(nbks);
	if (sobj == NULL) {
		funex_dief("no-mmap size=%lu", size);
	}
	fx_burnstack(8);

	return sobj;
}

static void sobject_del(void *sobj, size_t size)
{
	size_t nbks;

	nbks = fx_bytes_to_nbk(size);
	fx_munmap_bks(sobj, nbks);
	fx_burnstack(8);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_new_server(void)
{
	fx_server_t **server = &funex_server;

	*server = sobject_new(sizeof(**server));
	fx_server_init(*server);
}

void funex_del_server(void)
{
	fx_server_t **server = &funex_server;

	if (*server != NULL) {
		fx_server_destroy(*server);
		sobject_del(*server, sizeof(**server));
		*server = NULL;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void funex_new_mntsrv(void)
{
	fx_mntsrv_t **mntsrv = &funex_mntsrv;

	*mntsrv = sobject_new(sizeof(**mntsrv));
	fx_mntsrv_init(*mntsrv);
}

void funex_del_mntsrv(void)
{
	fx_mntsrv_t **mntsrv = &funex_mntsrv;

	if (*mntsrv != NULL) {
		fx_mntsrv_destroy(*mntsrv);
		sobject_del(*mntsrv, sizeof(**mntsrv));
		*mntsrv = NULL;
	}
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_SYSTEM_H_
#define FUNEX_SYSTEM_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Gracefully/Failure exit */
FX_ATTR_NORETURN
void funex_exit(int rc);

FX_ATTR_NORETURN
void funex_exiterr(void);

FX_ATTR_NORETURN
void funex_goodbye(void);

/* Halt execution & die (abort)  */
FX_ATTR_NORETURN
void funex_die(void);

FX_ATTR_NORETURN
void funex_dief(const char *fmt, ...);

/* Daemon panic call-back */
FX_ATTR_NORETURN
void funex_fatal_error(int errnum, const char *msg);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Process configurations */
char *funex_username(void);

char *funex_selfexe(void);

char *funex_sysconfdir(void);

char *funex_auxdsock(void);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Arguments & defaults */
void funex_init_defaults(int argc, char *argv[]);

#endif /* FUNEX_SYSTEM_H_ */




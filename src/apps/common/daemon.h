/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_DAEMON_H_
#define FUNEX_DAEMON_H_

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

struct rlimit;

/* Daemon */
void funex_daemonize(void);

int funex_forknwait(void (*fn)(void), pid_t *);

void funex_forkd(void (*start)(void));

void funex_execsub(int, char *[], const char *);

/* Resource */
void funex_getrlimit_as(struct rlimit *);

void funex_getrlimit_nproc(struct rlimit *);

void funex_setrlimit_core(rlim_t);

/* Signals */
void funex_register_sighalt(fx_sigaction_cb);

void funex_report_siginfo(const void *);

void funex_handle_lastsig(int, const void *);

void funex_sigblock(void);

void funex_sigunblock(void);

void funex_register_sigactions(void);

/* Capabilities */
int funex_capef_sys_admin(void);


#endif /* FUNEX_DAEMON_H_ */




/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include <getopt.h>

#include "fnxinfra.h"
#include "system.h"
#include "getopts.h"


/* Convert long-opts to opt-string */
static void make_optstring(const struct option *longopts, char *s, size_t len)
{
	const char *end = (s + len) - 3;

	memset(s, 0, len);
	while ((longopts->name != NULL) && (longopts->val > 0) && (s < end)) {
		*s++ = (char) longopts->val;
		if (longopts->has_arg) {
			*s++ = ':';
		}
		*s = '\0';

		++longopts;
	}
}

/* Special chars helpers */
static int is_delimchr(int c)
{
	return (strchr(FX_GETOPT_DELIMCHRS, c) != NULL);
}

static int is_withargchr(int c)
{
	return (strchr(FX_GETOPT_HASARGCHRS, c) != NULL);
}

/* Refresh with getopt globals */
static void getopt_refresh(funex_getopts_t *gopt)
{
	gopt->optarg = optarg;
	gopt->optind = optind;
	gopt->opterr = opterr;
	gopt->optopt = optopt;
}

/* Construct from user provided description string */
static void
getopt_init(funex_getopts_t *gopt, int argc, char *argv[], const char *optdef)
{
	int rc;
	size_t i, n;
	fx_substr_t ss;
	fx_substr_t toks[FX_GETOPT_MAX];
	fx_substr_t *tok = NULL;
	char *p;
	size_t sz;
	struct option *p_option;

	if (optdef == NULL) {
		optdef = "";
	}

	/* Reset getopt globals */
	optarg = NULL;
	optind = 1;
	optopt = 0;
	opterr = 0;

	/* Set defaults */
	gopt->argc   = argc;
	gopt->argv   = argv;
	gopt->optdefs   = strdup(optdef);
	getopt_refresh(gopt);

	/* Parse optdef string into getopt long repr */
	fx_substr_init_rwa(&ss, gopt->optdefs);
	rc = fx_substr_tokenize_chr(&ss, ' ', toks, FX_ARRAYSIZE(toks), &n);
	if (rc != 0) {
		funex_dief("internal-error: optdefs=%s", gopt->optdefs);
	}

	for (i = 0; i < n; ++i) {
		tok = &toks[i];
		p   = fx_substr_data(tok);
		sz  = fx_substr_size(tok);

		p_option = &gopt->longopts[gopt->optcount++];

		p_option->val = (int)p[0];

		if (is_delimchr(p[1])) {
			p += 2;
			sz -= 2;
		}

		if (sz && is_withargchr(p[sz - 1])) {
			p_option->has_arg   = 1;
			p[sz - 1] = '\0';
		} else {
			p_option->has_arg   = 0;
			p[sz] = '\0';
		}

		p_option->name = p;
	}

	sz = sizeof(gopt->optstring);
	make_optstring(gopt->longopts, gopt->optstring, sz - 1);
}

/* Call getopt */
static int getopt_lookupnextopt(funex_getopts_t *gopt, int *c)
{
	gopt->optchr = getopt_long(gopt->argc,
	                           gopt->argv,
	                           gopt->optstring,
	                           gopt->longopts,
	                           &gopt->longindex);
	getopt_refresh(gopt);

	*c = gopt->optchr;
	return (gopt->optchr != EOF) ? 0 : -1;
}

/* Get current arg-string */
static const char *getopt_lookuparg(const funex_getopts_t *gopt)
{
	return gopt->optarg;
}

/* Parse non-option ARGV-elements: */
static int getopt_lookupnextarg(funex_getopts_t *gopt, char const **arg)
{
	int rc;

	if (gopt->optind < gopt->argc) {
		*arg = gopt->argv[gopt->optind];
		optind += 1;
		getopt_refresh(gopt);
		rc = 0;
	} else {
		rc = -1;
	}
	return rc;
}

/* Release resources */
static void getopt_destroy(funex_getopts_t *gopt)
{
	if (gopt->optdefs != NULL) {
		free(gopt->optdefs);
		gopt->optdefs = NULL;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * User's API: Wrappers over getopt_long
 */
void funex_getopts_init(funex_getopts_t *gopt,
                        int argc, char *argv[], const char *optdef)
{
	memset(gopt, 0, sizeof(*gopt));
	getopt_init(gopt, argc, argv, optdef);
}

int funex_getopts_lookupnextopt(funex_getopts_t *gopt, int *c)
{
	return getopt_lookupnextopt(gopt, c);
}

int funex_getopts_lookupnextarg(funex_getopts_t *gopt, char const **arg)
{
	return getopt_lookupnextarg(gopt, arg);
}

const char *funex_getopts_lookuparg(const funex_getopts_t *gopt)
{
	return getopt_lookuparg(gopt);
}

void funex_getopts_destroy(funex_getopts_t *gopt)
{
	getopt_destroy(gopt);
}



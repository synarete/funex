/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>

#include "funex-common.h"
#include "graybox/graybox.h"


#define globals (funex_globals)

/* Locals */
static void funex_gbx_parse_args(int argc, char *argv[]);
static void funex_gbx_set_panic_hook(void);
static void funex_gbx_setproc(void);
static void funex_gbx_execute(void);
static void funex_gbx_cleanup(void);
static void funex_gbx_prepare(void);

static int funex_gbx_mask = 0;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                         The Funex File-System                             *
 *                                                                           *
 *                           Test-Suite Utility                              *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int main(int argc, char *argv[])
{
	/* Begin with defaults */
	funex_init_defaults(argc, argv);

	/* Bind panic callback hook */
	funex_gbx_set_panic_hook();

	/* Parse command-line args */
	funex_gbx_parse_args(argc, argv);

	/* Set process defaults & limits */
	funex_gbx_setproc();

	/* Enable printing-logging */
	funex_setup_plogger();

	/* Check settings validity */
	funex_gbx_prepare();

	/* Run as long as active */
	funex_gbx_execute();

	/* Post-tests cleanups */
	funex_gbx_cleanup();

	/* Done. */
	return EXIT_SUCCESS;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_gbx_set_panic_hook(void)
{
	fx_set_panic_callback(funex_fatal_error);
}

static void funex_gbx_setproc(void)
{
	umask(S_IWGRP);

	/* TODO - core etc. */
}


static void funex_gbx_prepare(void)
{
	int rc;
	char *path, *real;

	path = globals.path;
	if ((rc = fnx_statdir(path, NULL)) != 0) {
		funex_dief("illegal-dir: %s err=%d", path, -rc);
	}
	if ((rc = fnx_realpath(path, &real)) != 0) {
		funex_dief("no-realpath: %s err=%d", path, -rc);
	}
	if (strlen(real) > (PATH_MAX / 2)) {
		funex_dief("base-too-long: %s", real);
	}
	funex_globals_set_path(real);
	funex_globals_set_head(real);
	fnx_freepath(real);

	if (funex_gbx_mask == 0) {
		funex_gbx_mask = GBX_RDWR | GBX_POSIX;
	}
}


static void funex_gbx_cleanup(void)
{
	fflush(stdout);
	fflush(stderr);
	sync();
}

static void funex_gbx_execute(void)
{
	gbx_ctx_t *gbx;

	gbx = (gbx_ctx_t *)malloc(sizeof(*gbx));
	gbx_init(gbx, globals.path, getpid());
	gbx->mask = funex_gbx_mask;
	gbx_execute(gbx, gbx_tests_list);
	gbx_destroy(gbx);
	free(gbx);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_gbx_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'a':
				funex_gbx_mask = GBX_ALL;
				break;
			case 'i':
				funex_gbx_mask |= GBX_RDWR;
				break;
			case 'p':
				funex_gbx_mask |= GBX_POSIX;
				break;
			case 'x':
				funex_gbx_mask |= GBX_RDWR | GBX_POSIX;
				break;
			case 'S':
				funex_gbx_mask |= GBX_STRESS;
				break;
			case 'C':
				funex_gbx_mask |= GBX_CUSTOM;
				break;
			case 'L':
				funex_show_license_and_goodbye();
				break;
			case 'v':
				funex_show_version_and_goodbye();
				break;
			case 'd':
				arg = funex_getoptarg(opt, "debug");
				funex_globals_set_debug(arg);
				break;
			case 'h':
			default:
				funex_show_cmdhelp_and_goodbye2(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "path", FUNEX_ARG_LAST);
	if (arg != NULL) {
		funex_globals_set_path(arg);
	} else {
		funex_globals_set_path(globals.proc.cwd);
	}
}

static const funex_cmdent_t funex_gbx_cmdent = {
	.gopt       = funex_gbx_getopts,
	.name       = "gbx",
	.usage      = "% [<testdir>]",
	.optdef     =
	"h,help d,debug= v,version L,license a,all x,main "\
	"i,rw p,posix S,stress C,custom",
	.desc       = "Run file-system test-suite",
	.optdesc    = \
	"-x, --main                 Run main tests (posix + I/O) \n"\
	"-i, --rw                   Run read-write I/O tests \n"\
	"-p, --posix                Run POSIX compatibility tests \n"\
	"-S, --stress               Run stress tests \n"\
	"-C, --custom               Run custom tests (Funex specific)\n"\
	"-a, --all                  Run all tests \n"\
	"-d, --debug=<level>        Debug mode level \n"\
	"-L, --license              Show license info \n"\
	"-v, --version              Show version info \n"\
	"-h, --help                 Show this help \n"
};

static void funex_gbx_parse_args(int argc, char *argv[])
{
	funex_parse_cmdargs(argc, argv, &funex_gbx_cmdent, funex_gbx_getopts);
}


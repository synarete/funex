/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>

#include "funex-common.h"
#include "funex-fsck.h"

typedef funex_fsck_ctx_t fsck_ctx_t;

static fsck_ctx_t *new_fsck_rdctx(const char *path)
{
	fsck_ctx_t *ctx;

	ctx = fx_xmalloc(sizeof(*ctx), FX_NOFAIL | FX_BZERO);
	ctx->vio    = funex_open_vio(path, FX_VIOF_RDONLY | FX_VIOF_LOCK);
	ctx->super  = fx_vnode_to_super(funex_newvobj(FNX_VTYPE_SUPER));
	ctx->rootd  = fx_vnode_to_dir(funex_newvobj(FNX_VTYPE_DIR));
	return ctx;
}

static void del_fsck_ctx(fsck_ctx_t *ctx)
{
	funex_close_vio(ctx->vio);
	funex_delvobj(&ctx->super->su_vnode);
	funex_delvobj(&ctx->rootd->d_inode.i_vnode);
	fx_xfree(ctx, sizeof(*ctx), FX_BZERO);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void check_raw_super(fsck_ctx_t *ctx)
{
	int rc;
	fx_size_t sec, frg;
	const fx_dfrg_t *dfrg;
	const fx_header_t *hdr;

	sec = FNX_SEC_SUPER;
	frg = FNX_FRG_SUPER;
	fx_info("checking-raw-super: sec=%#lx frg=%d", sec, (int)frg);

	funex_read_dsec(ctx->vio, sec, &ctx->dsec);
	dfrg = &ctx->dsec.sec_frgs[frg];
	hdr  = &dfrg->fr_hdr;
	rc = fx_check_dobj(hdr, FNX_VTYPE_SUPER);
	if (rc != 0) {
		funex_dief("illegal-super: sec=%#lx frg=%d", sec, (int)frg);
	}
}

static void check_raw_rootd(fsck_ctx_t *ctx)
{
	int rc;
	fx_size_t sec, frg;
	const fx_dfrg_t *dfrg;
	const fx_header_t *hdr;

	sec = FNX_SEC_SUPER;
	frg = FNX_FRG_ROOT;
	fx_info("checking-raw-rootd: sec=%#lx frg=%d", sec, (int)frg);

	funex_read_dsec(ctx->vio, sec, &ctx->dsec);
	dfrg = &ctx->dsec.sec_frgs[frg];
	hdr  = &dfrg->fr_hdr;
	rc = fx_check_dobj(hdr, FNX_VTYPE_DIR);
	if (rc != 0) {
		funex_dief("illegal-root: sec=%#lx frg=%d", sec, (int)frg);
	}
}

int funex_fsck_check(const char *path)
{
	fsck_ctx_t *ctx;

	fx_info("start-fsck: %s", path);
	ctx = new_fsck_rdctx(path);
	check_raw_super(ctx);
	check_raw_rootd(ctx);
	del_fsck_ctx(ctx);
	fx_info("done-checking: %s", path);

	return 0;
}

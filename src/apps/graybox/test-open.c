/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects successful open(3p) with O_CREAT to set the file's access time
 */
static void test_open_atime(gbx_ctx_t *gbx)
{
	int fd;
	char *path0, *path1;
	struct stat st0, st1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 1);
	gbx_expect(gbx, fnx_open(path1, O_CREAT | O_WRONLY, 0644, &fd), 0);
	gbx_expect(gbx, fnx_fstat(fd, &st1), 0);
	gbx_expect(gbx, st0.st_atim.tv_sec < st1.st_atim.tv_sec, 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects successful open(3p) with O_CREAT to update parent's ctime and mtime
 * only if file did *not* exist.
 */
static void test_open_mctime(gbx_ctx_t *gbx)
{
	int fd1, fd2;
	char *path0, *path1;
	struct stat st0, st1, st2, st3;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_open(path1, O_CREAT | O_WRONLY, 0644, &fd1), 0);
	gbx_expect(gbx, fnx_fstat(fd1, &st1), 0);
	gbx_expect(gbx, st0.st_mtim.tv_sec < st1.st_mtim.tv_sec, 1);
	gbx_expect(gbx, st0.st_ctim.tv_sec < st1.st_ctim.tv_sec, 1);
	gbx_expect(gbx, fnx_stat(path0, &st2), 0);
	gbx_expect(gbx, st0.st_mtim.tv_sec < st2.st_mtim.tv_sec, 1);
	gbx_expect(gbx, st0.st_ctim.tv_sec < st2.st_ctim.tv_sec, 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_close(fd1), 0);


	gbx_expect(gbx, fnx_create(path1, 0644, &fd1), 0);
	gbx_expect(gbx, fnx_fstat(fd1, &st1), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_open(path1, O_CREAT | O_RDONLY, 0644, &fd2), 0);
	gbx_expect(gbx, fnx_fstat(fd2, &st2), 0);
	gbx_expect(gbx, fnx_stat(path0, &st3), 0);
	gbx_expect(gbx, st1.st_mtim.tv_sec == st2.st_mtim.tv_sec, 1);
	gbx_expect(gbx, st1.st_ctim.tv_sec == st2.st_ctim.tv_sec, 1);
	gbx_expect(gbx, st0.st_mtim.tv_sec == st3.st_mtim.tv_sec, 1);
	gbx_expect(gbx, st0.st_ctim.tv_sec == st3.st_ctim.tv_sec, 1);

	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_close(fd1), 0);
	gbx_expect(gbx, fnx_close(fd2), 0);
	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects open(3p) to return ELOOP if too many symbolic links are encountered
 * while resolving pathname, or O_NOFOLLOW was specified but pathname was a
 * symbolic link.
 */
static void test_open_loop(gbx_ctx_t *gbx)
{
	int fd;
	char *path0, *path1, *path2, *path3;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(path0, gbx_genname(gbx));
	path3 = gbx_newpath(path1, gbx_genname(gbx));

	gbx_expect(gbx, fnx_symlink(path0, path1), 0);
	gbx_expect(gbx, fnx_symlink(path1, path0), 0);
	gbx_expect(gbx, fnx_open(path2, O_RDONLY, 0, &fd), -ELOOP);
	gbx_expect(gbx, fnx_open(path3, O_RDONLY, 0, &fd), -ELOOP);
	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	gbx_delpath(path3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects open(3p) to return EISDIR if the  named file is a directory and
 * oflag includes O_WRONLY or O_RDWR.
 */
static void test_open_isdir(gbx_ctx_t *gbx)
{
	int fd, fd2;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_mkdir(path, 0755), 0);
	gbx_expect(gbx, fnx_open(path, O_RDONLY, 0, &fd), 0);
	gbx_expect(gbx, fnx_open(path, O_WRONLY, 0, &fd2), -EISDIR);
	gbx_expect(gbx, fnx_open(path, O_RDWR, 0, &fd2), -EISDIR);
	gbx_expect(gbx, fnx_open(path, O_RDONLY | O_TRUNC, 0, &fd2), -EISDIR);
	gbx_expect(gbx, fnx_open(path, O_WRONLY | O_TRUNC, 0, &fd2), -EISDIR);
	gbx_expect(gbx, fnx_open(path, O_RDWR | O_TRUNC, 0, &fd2), -EISDIR);
	gbx_expect(gbx, fnx_rmdir(path), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_open(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_open_tests[] = {
		{ test_open_atime,  "open-atime",  GBX_POSIX },
		{ test_open_mctime, "open-mctime", GBX_POSIX },
		{ test_open_loop,   "open-loop",   GBX_POSIX },
		{ test_open_isdir,  "open-isdir",  GBX_POSIX },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gbx, s_open_tests);
}


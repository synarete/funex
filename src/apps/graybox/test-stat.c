/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fcntl.h>
#include <unistd.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects stat(3p) to successfully probe directory and return ENOENT if a
 * component of path does not name an existing file or path is an empty string.
 */
static void test_stat_simple(gbx_ctx_t *gbx)
{
	mode_t ifmt = S_IFMT;
	struct stat st0, st1;
	char *path0, *path1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, (int)(st0.st_mode & ~ifmt), 0700);
	gbx_expect(gbx, (long)st0.st_nlink, 2);
	gbx_expect(gbx, fnx_stat(path1, &st1), -ENOENT);

	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects stat(3p) to return ENOTDIR if a component of the path prefix is not
 * a directory.
 */
static void test_stat_notdir(gbx_ctx_t *gbx)
{
	int fd;
	struct stat st0, st1, st2;
	char *path0, *path1, *path2;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	path2 = gbx_newpath(path1, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, fnx_open(path1, O_CREAT | O_RDWR, 0644, &fd), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISREG(st1.st_mode), 1);
	gbx_expect(gbx, S_ISREG(st1.st_size), 0);
	gbx_expect(gbx, S_ISREG(st1.st_blocks), 0);
	gbx_expect(gbx, fnx_stat(path2, &st2), -ENOTDIR);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects statvfs(3p) to return valid result for dir-path, reg-path or rd-open
 * file-descriptor.
 */
static void test_stat_statvfs(gbx_ctx_t *gbx)
{
	int fd;
	struct statvfs stv0, stv1, stv2, stv3;
	char *path0, *path1, *path2, *path3;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	path2 = gbx_newpath(path1, gbx_genname(gbx));
	path3 = gbx_newpath(path0, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0750), 0);
	gbx_expect(gbx, fnx_create(path1, 0644, &fd), 0);
	gbx_expect(gbx, fnx_statvfs(path0, &stv0), 0);
	gbx_expect(gbx, fnx_statvfs(path1, &stv1), 0);
	gbx_expect(gbx, (stv0.f_bavail > 0), 1);
	gbx_expect(gbx, (long)stv0.f_fsid, (long)stv1.f_fsid);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv1), 0);
	gbx_expect(gbx, (long)stv0.f_fsid, (long)stv1.f_fsid);
	gbx_expect(gbx, fnx_statvfs(path2, &stv2), -ENOTDIR);
	gbx_expect(gbx, fnx_statvfs(path3, &stv3), -ENOENT);

	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	gbx_delpath(path3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_stat(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_stat_tests[] = {
		{ test_stat_simple,     "stat-simple",  GBX_POSIX },
		{ test_stat_notdir,     "stat-notdir",  GBX_POSIX },
		{ test_stat_statvfs,    "stat-statvfs", GBX_POSIX },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gbx, s_stat_tests);
}

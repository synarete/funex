/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

#include "fnxinfra.h"
#include "graybox.h"


struct thread_ctx {
	gbx_ctx_t  *gbx;
	pthread_t       pth;
	unsigned        idx;
};
typedef struct thread_ctx thread_ctx_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Tests read-write data-consistency when two-threads write to same file
 * into non-overlaping regions, but on same blocks.
 */
static void *do_rdwr_odd_even(void *p)
{
	int fd;
	char *path;
	loff_t pos = -1;
	size_t num, num2;
	size_t nsz, nwr, nrd;
	thread_ctx_t  *ctx = (thread_ctx_t *)p;
	gbx_ctx_t *gbx = ctx->gbx;
	const size_t   idx = ctx->idx;

	path = gbx_newpath(gbx->base, gbx->name);
	gbx_expect(gbx, fnx_open(path, O_RDWR, 0, &fd), 0);
	for (size_t itr = 0; itr < 7; ++itr) {
		for (size_t i = 0; i < 1171; ++i) {
			nsz = sizeof(num);
			pos = (loff_t)((i * 19) + ((idx - 1) * nsz));
			num = i + idx;
			gbx_expect(gbx, fnx_pwrite(fd, &num, nsz, pos, &nwr), 0);
			gbx_expect(gbx, (long)nwr, (long)nsz);
			gbx_expect(gbx, fnx_pread(fd, &num2, nsz, pos, &nrd), 0);
			gbx_expect(gbx, (long)nrd, (long)nsz);
			gbx_expect(gbx, (long)num, (long)num2);
		}
	}
	gbx_expect(gbx, fnx_fsync(fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_delpath(path);
	return NULL;
}

static void test_rw_threads_two(gbx_ctx_t *gbx)
{
	int fd;
	thread_ctx_t ctx_odd, ctx_eve;
	pthread_attr_t attr;
	char *path, *name;

	name = gbx_genname(gbx);
	path = gbx_newpath(gbx->base, name);
	gbx_expect(gbx, fnx_create(path, 0600, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	ctx_odd.gbx = gbx;
	ctx_odd.idx = 1;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&ctx_odd.pth, &attr, do_rdwr_odd_even, &ctx_odd);

	ctx_eve.gbx = gbx;
	ctx_eve.idx = 2;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&ctx_eve.pth, &attr, do_rdwr_odd_even, &ctx_eve);

	pthread_join(ctx_eve.pth, NULL);
	pthread_join(ctx_odd.pth, NULL);

	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Tests read-write data-consistency when multi-threads write to same file
 * into non-overlapping regions.
 */
static void *do_rdwr_multi(void *p)
{
	int fd;
	char *path, *name;
	loff_t pos = -1;
	size_t i, num, num2;
	size_t nsz, nwr, nrd;
	const size_t cnt  = 31;
	const size_t step = 21211;
	thread_ctx_t  *ctx = (thread_ctx_t *)p;
	gbx_ctx_t *gbx = ctx->gbx;

	name = gbx->name;
	path = gbx_newpath(gbx->base, name);
	gbx_expect(gbx, fnx_open(path, O_RDWR, 0, &fd), 0);
	for (size_t itr = 0; itr < 7; ++itr) {
		for (i = cnt; i > 0; --i) {
			pos = (loff_t)((i * step) + (ctx->idx * sizeof(num)));
			num = (size_t)pos + itr;
			nsz = sizeof(num);
			gbx_expect(gbx, fnx_pwrite(fd, &num, nsz, pos, &nwr), 0);
			gbx_expect(gbx, (long)nwr, (long)nsz);
		}
		for (i = cnt; i > 0; --i) {
			pos = (loff_t)((i * step) + (ctx->idx * sizeof(num)));
			num = (size_t)pos + itr;
			nsz = sizeof(num2);
			gbx_expect(gbx, fnx_pread(fd, &num2, nsz, pos, &nrd), 0);
			gbx_expect(gbx, (long)nrd, (long)nsz);
			gbx_expect(gbx, (long)num, (long)num2);
		}
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_delpath(path);

	return NULL;
}

static void test_rw_threads_multi(gbx_ctx_t *gbx)
{
	int fd;
	size_t i, nelems;
	thread_ctx_t ctx_arr[17];
	thread_ctx_t *ctx;
	pthread_attr_t attr;
	char *path, *name;

	name = gbx_genname(gbx);
	path = gbx_newpath(gbx->base, name);
	gbx_expect(gbx, fnx_create(path, 0600, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	memset(ctx_arr, 0, sizeof(ctx_arr));
	nelems = gbx_nelems(ctx_arr);
	for (i = 0; i < nelems; ++i) {
		ctx = &ctx_arr[i];
		ctx->gbx = gbx;
		ctx->idx = (unsigned)i;

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
		pthread_create(&ctx->pth, &attr, do_rdwr_multi, ctx);
		gbx_suspend(gbx, 0, 9);
	}
	for (i = 0; i < nelems; ++i) {
		ctx = &ctx_arr[i];
		pthread_join(ctx->pth, NULL);
	}

	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_rw_threads(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_io_threads_tests[] = {
		{ test_rw_threads_two,      "rw-threads-two",       GBX_RDWR },
		{ test_rw_threads_multi,    "rw-threads-multi",     GBX_RDWR },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_io_threads_tests);
}


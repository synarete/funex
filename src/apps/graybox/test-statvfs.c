/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fcntl.h>
#include <unistd.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects statvfs(3p) to successfully obtain information about the file system
 * containing base-dir, and return ENOENT if a component of path does not name
 * an existing file.
 */
static void test_statvfs_simple(gbx_ctx_t *gbx)
{
	struct statvfs stv;
	char *path, *name;

	name = gbx_genname(gbx);
	path = gbx_newpath(gbx->base, name);

	gbx_expect(gbx, fnx_statvfs(gbx->base, &stv), 0);
	gbx_expect(gbx, (stv.f_bsize > 0), 1);
	gbx_expect(gbx, (stv.f_bsize % FNX_FRGSIZE), 0);
	gbx_expect(gbx, (stv.f_frsize > 0), 1);
	gbx_expect(gbx, (stv.f_frsize % FNX_FRGSIZE), 0);
	gbx_expect(gbx, (stv.f_namemax > strlen(name)), 1);
	gbx_expect(gbx, fnx_statvfs(path, &stv), -ENOENT);

	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects fstatvfs(3p) to successfully obtain information about the file system
 * via open file-descriptor to regular-file.
 */
static void test_statvfs_regfile(gbx_ctx_t *gbx)
{
	int fd;
	struct statvfs stv;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path, 0644, &fd), 0);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv), 0);
	gbx_expect(gbx, (stv.f_bsize > 0), 1);
	gbx_expect(gbx, (stv.f_bsize % FNX_FRGSIZE), 0);
	gbx_expect(gbx, (stv.f_frsize > 0), 1);
	gbx_expect(gbx, (stv.f_frsize % FNX_FRGSIZE), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects statvfs(3p) to return ENOTDIR if a component of the path prefix of
 * path is not a directory.
 */
static void test_statvfs_notdir(gbx_ctx_t *gbx)
{
	int fd;
	struct statvfs stv0, stv1;
	char *path0, *path1, *path2;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	path2 = gbx_newpath(path1, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_statvfs(path0, &stv0), 0);
	gbx_expect(gbx, fnx_create(path1, 0644, &fd), 0);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv1), 0);
	gbx_expect(gbx, fnx_statvfs(path2, &stv0), -ENOTDIR);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects statvfs(3p) to change statvfs.f_ffree upon objects create/remove
 */
static void test_statvfs_ffree(gbx_ctx_t *gbx)
{
	int fd;
	struct statvfs stv0, stv1;
	char *path0, *path1, *path2, *dpath;

	dpath = gbx_newpath(gbx->base, gbx_genname(gbx));
	path0 = gbx_newpath(dpath, gbx_genname(gbx));
	path1 = gbx_newpath(dpath, gbx_genname(gbx));
	path2 = gbx_newpath(dpath, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(dpath, 0700), 0);
	gbx_expect(gbx, fnx_statvfs(dpath, &stv0), 0);
	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_statvfs(path0, &stv1), 0);
	gbx_expect(gbx, stv1.f_ffree == (stv0.f_ffree - 1), 1);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_statvfs(dpath, &stv1), 0);
	gbx_expect(gbx, stv0.f_ffree == stv1.f_ffree, 1);

	gbx_expect(gbx, fnx_statvfs(dpath, &stv0), 0);
	gbx_expect(gbx, fnx_create(path1, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv1), 0);
	gbx_expect(gbx, stv1.f_ffree == (stv0.f_ffree - 1), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_statvfs(dpath, &stv1), 0);
	gbx_expect(gbx, stv1.f_ffree == (stv0.f_ffree - 1), 1);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_statvfs(dpath, &stv1), 0);
	gbx_expect(gbx, stv0.f_ffree == stv1.f_ffree, 1);

	gbx_expect(gbx, fnx_statvfs(dpath, &stv0), 0);
	gbx_expect(gbx, fnx_symlink(dpath, path2), 0);
	gbx_expect(gbx, fnx_statvfs(path2, &stv1), 0);
	gbx_expect(gbx, stv1.f_ffree == (stv0.f_ffree - 1), 1);
	gbx_expect(gbx, fnx_unlink(path2), 0);
	gbx_expect(gbx, fnx_statvfs(dpath, &stv1), 0);
	gbx_expect(gbx, stv0.f_ffree == stv1.f_ffree, 1);
	gbx_expect(gbx, fnx_rmdir(dpath), 0);

	gbx_delpath(dpath);
	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects fstatvfs(3p) to change statvfs.f_bfree upon write/trim.
 */
static void test_statvfs_bfree(gbx_ctx_t *gbx)
{
	int fd;
	size_t nwr;
	struct statvfs stv0, stv1;
	char *path0, *path1;
	char str[] = "1";

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_create(path1, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv0), 0);
	gbx_expect(gbx, fnx_write(fd, str, strlen(str), &nwr), 0);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv1), 0);
	gbx_expect(gbx, stv0.f_bfree > stv1.f_bfree, 1);
	gbx_expect(gbx, fnx_ftruncate(fd, 0), 0);
	gbx_expect(gbx, fnx_fstatvfs(fd, &stv1), 0);
	gbx_expect(gbx, stv1.f_bfree == stv0.f_bfree, 1);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_statvfs(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_statvfs_tests[] = {
		{ test_statvfs_simple,  "statvfs-simple",   GBX_POSIX },
		{ test_statvfs_regfile, "statvfs-regfile",  GBX_POSIX },
		{ test_statvfs_notdir,  "statvfs-notdir",   GBX_POSIX },
		{ test_statvfs_ffree,   "statvfs-ffree",    GBX_POSIX },
		{ test_statvfs_bfree,   "statvfs-bfree",    GBX_POSIX },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gbx, s_statvfs_tests);
}

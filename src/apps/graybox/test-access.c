/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects access(3p) to return 0 on root-dir.
 */
static void test_access_rootdir(gbx_ctx_t *gbx)
{
	gbx_expect(gbx, fnx_access(gbx->base, R_OK | W_OK | X_OK), 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects access(3p) to return ENOENT if a component of path does not name an
 * existing file or path is an empty string.
 */
static void test_access_noent(gbx_ctx_t *gbx)
{
	char *path0, *path1, *path2;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	path2 = gbx_newpath(path1, "test");

	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	gbx_expect(gbx, fnx_access(path0, F_OK), 0);
	gbx_expect(gbx, fnx_access(path0, X_OK), 0);
	gbx_expect(gbx, fnx_access(path0, F_OK | X_OK), 0);

	gbx_expect(gbx, fnx_access(path1, R_OK), -ENOENT);
	gbx_expect(gbx, fnx_access(path1, F_OK), -ENOENT);
	gbx_expect(gbx, fnx_access(path1, F_OK | X_OK), -ENOENT);

	gbx_expect(gbx, fnx_access(path2, R_OK), -ENOENT);
	gbx_expect(gbx, fnx_access(path2, F_OK), -ENOENT);
	gbx_expect(gbx, fnx_access(path2, F_OK | X_OK), -ENOENT);

	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects access(3p) to return EINVAL if the value of the amode argument is
 * invalid.
 */
static void test_access_inval(gbx_ctx_t *gbx)
{
	int fd, mode;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_create(path, 0644, &fd), 0);

	mode = R_OK | W_OK | X_OK | F_OK;
	gbx_expect(gbx, fnx_access(path, ~mode), -EINVAL);

	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects access(3p) to return EACCES when a component of the path prefix
 * denies search permission
 */
static void test_access_prefix(gbx_ctx_t *gbx)
{
	int fd, mode = R_OK;
	char *path0, *path1, *path2, *path3;

	/* TODO: Better logic via external flags */
	if (gbx->isroot || gbx->cap_sysadmin) {
		return;
	}

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	path2 = gbx_newpath(path1, gbx_genname(gbx));
	path3 = gbx_newpath(path2, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0750), 0);
	gbx_expect(gbx, fnx_mkdir(path1, 0750), 0);
	gbx_expect(gbx, fnx_mkdir(path2, 0750), 0);
	gbx_expect(gbx, fnx_create(path3, 0600, &fd), 0);
	gbx_expect(gbx, fnx_access(path3, mode), 0);
	gbx_expect(gbx, fnx_chmod(path2, 0200), 0);
	gbx_suspend(gbx, 3, 0);
	gbx_expect(gbx, fnx_access(path3, mode), -EACCES);
	gbx_expect(gbx, fnx_chmod(path2, 0700), 0);
	gbx_suspend(gbx, 3, 0);
	gbx_expect(gbx, fnx_access(path3, mode), 0);

	gbx_expect(gbx, fnx_unlink(path3), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_rmdir(path2), 0);
	gbx_expect(gbx, fnx_rmdir(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	gbx_delpath(path3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_access(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_access_tests[] = {
		{ test_access_rootdir,  "access-rootdir",   GBX_POSIX },
		{ test_access_noent,    "access-noent",     GBX_POSIX },
		{ test_access_inval,    "access-inval",     GBX_POSIX },
		{ test_access_prefix,   "access-prefix",    GBX_POSIX },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gbx, s_access_tests);
}



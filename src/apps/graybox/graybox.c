/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <error.h>
#include <errno.h>
#include <err.h>
#include <time.h>
#include <limits.h>
#include <uuid/uuid.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/capability.h>
#include <unistd.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static char iscap(cap_t cap_p, cap_value_t value)
{
	int rc;
	cap_flag_value_t flag = 0;

	rc  = cap_get_flag(cap_p, value, CAP_EFFECTIVE, &flag);
	return ((rc == 0) && (flag == CAP_SET));
}

static void gbx_init_caps(gbx_ctx_t *gbx, pid_t pid)
{
	cap_t cap;

	if (pid == 0) {
		gbx->isroot = 1;
	}
	cap = cap_get_pid(pid);
	if (cap != NULL) {
		gbx->cap_chown      = iscap(cap, CAP_CHOWN);
		gbx->cap_fowner     = iscap(cap, CAP_FOWNER);
		gbx->cap_fsetid     = iscap(cap, CAP_FSETID);
		gbx->cap_sysadmin   = iscap(cap, CAP_SYS_ADMIN);
		gbx->cap_mknod      = iscap(cap, CAP_MKNOD);
		cap_free(cap);
	}
}

static void gbx_init_rnds(gbx_ctx_t *gbx)
{
	unsigned seed;

	seed = (unsigned)(time(NULL));
	initstate_r(seed, gbx->sbuf, sizeof(gbx->sbuf), &gbx->rnd);
	setstate_r(gbx->sbuf, &gbx->rnd);
}

void gbx_init(gbx_ctx_t *gbx, const char *base, pid_t pid)
{
	memset(gbx, 0, sizeof(*gbx));
	gbx->base = strdup(base);
	gbx_init_caps(gbx, pid);
	gbx_init_rnds(gbx);
	gbx->mask = 0;
}

void gbx_destroy(gbx_ctx_t *gbx)
{
	if (gbx->base != NULL) {
		free(gbx->base);
	}
	memset(gbx, 0, sizeof(*gbx));
}

void gbx_fillrand(gbx_ctx_t *gbx, void *buf, size_t bsz)
{
	void *ptr;
	char *dat, *end;
	int32_t num = 0;

	dat = (char *)buf;
	end = (dat + bsz) - sizeof(num);
	while (dat < end) {
		ptr = dat;
		random_r(&gbx->rnd, &num);
		memcpy(ptr, &num, sizeof(num));
		dat += sizeof(num);
	}
	dat = end;
	end = ((char *)buf + bsz);
	while (dat < end) {
		random_r(&gbx->rnd, &num);
		dat[0] = (char)num;
		dat += 1;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_suspend(gbx_ctx_t *gbx, int sec, int part)
{
	int rc;
	struct timespec rem = { 0, 0 };
	struct timespec req = { sec, (long)part * 1000000LL };

	rc = nanosleep(&req, &rem);
	while ((rc != 0) && (errno == EINTR)) {
		memcpy(&req, &rem, sizeof(req));
		rc = nanosleep(&req, &rem);
	}
	(void)gbx;
}

char *gbx_newpath(const char *path1, const char *path2)
{
	char *path;

	errno = 0;
	if ((path = fnx_joinpath(path1, path2)) == NULL) {
		err(errno, "no-joinpath: path1=%s path2=%s", path1, path2);
	}
	return path;
}

void gbx_delpath(char *path)
{
	fnx_freepath(path);
}

void *gbx_newbuf(size_t bsz)
{
	void *buf;

	errno = 0;
	buf = calloc(1, bsz);
	if (buf == NULL) {
		err(errno, "no-calloc: bsz=%zu", bsz);
	}
	return buf;
}

void gbx_delbuf(void *buf)
{
	free(buf);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const char *base(const char *path)
{
	const char *s;

	s = strrchr(path, '/');
	if (s != NULL) {
		s = s + 1;
	} else {
		s = path;
	}
	return s;
}

static void error_progname(void)
{
	fprintf(stderr, "%s: ", program_invocation_short_name);
}

void gbx_do_expect(gbx_ctx_t *gbx,
                   const char *file, unsigned line, long res, long exp)
{
	FILE *fp = stdout;
	const char *name = gbx->curr->name;

	if (res != exp) {
		fprintf(fp, "\r%-24s -- Failed\n", name);
		error_print_progname = error_progname;
		error_at_line(-1, 0, base(file), line,
		              "%s: res=%ld exp=%ld", name, res, exp);
	}
}

static void genname(const gbx_ctx_t *gbx, char *buf, size_t n)
{
	uuid_t uu;
	uint64_t val = 0;
	const char *curr_name = "x";

	uuid_generate(uu);
	val |= (uint64_t)(uu[0] ^ uu[1]);
	val |= (uint64_t)(uu[2] ^ uu[3]) << 8;
	val |= (uint64_t)(uu[4] ^ uu[5]) << 16;
	val |= (uint64_t)(uu[6] ^ uu[7]) << 24;
	val |= (uint64_t)(uu[8] ^ uu[9]) << 32;
	val |= (uint64_t)(uu[10] ^ uu[11]) << 40;
	val |= (uint64_t)(uu[12] ^ uu[13]) << 48;
	val |= (uint64_t)(uu[14] ^ uu[15]) << 56;
	val ^= (uint64_t)time(NULL);
	val ^= (val >> 32);

	if (gbx->curr && gbx->curr->name) {
		curr_name = gbx->curr->name;
	}
	snprintf(buf, n, "funex-test-%s.%08x", curr_name, (uint32_t)val);
}

char *gbx_genname(gbx_ctx_t *gbx)
{
	genname(gbx, gbx->name, sizeof(gbx->name) - 1);
	return gbx->name;
}

static void clocknow(struct timespec *ts)
{
	clock_gettime(CLOCK_MONOTONIC, ts);
}

static void clockdif(const struct timespec *beg,
                     const struct timespec *end,
                     struct timespec *dif)
{
	dif->tv_sec = end->tv_sec - beg->tv_sec;
	if (end->tv_nsec >= beg->tv_nsec) {
		dif->tv_nsec = end->tv_nsec - beg->tv_nsec;
	} else {
		dif->tv_sec -= 1;
		dif->tv_nsec = beg->tv_nsec - end->tv_nsec;
	}
}

static void runtest(gbx_ctx_t *gbx, const gbx_execdef_t *tdef)
{
	FILE *fp = stdout;
	int flags;
	struct timespec beg, end, dif;

	if (tdef && tdef->hook) {
		flags = tdef->flags;
		gbx->curr = tdef;
		if (flags & GBX_META) {
			tdef->hook(gbx);
		} else if (flags & gbx->mask) {
			fprintf(fp, "%s...\r", tdef->name);
			fflush(fp);

			clocknow(&beg);
			tdef->hook(gbx);
			clocknow(&end);
			clockdif(&beg, &end, &dif);

			fprintf(fp, "%-24s %ld.%03lds\n", tdef->name,
			        (long)dif.tv_sec, ((long)dif.tv_nsec) / 1000000);
		}
		gbx->curr = NULL;
	}
}

void gbx_execute(gbx_ctx_t *gbx, const gbx_execdef_t tdefs[])
{
	size_t i = 0;
	const gbx_execdef_t *tdef;

	tdef = &tdefs[0];
	while (tdef->hook != NULL) {
		runtest(gbx, tdef);
		tdef = &tdefs[++i];
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const gbx_execdef_t gbx_tests_list[]  = {
	{ gbx_test_access,          "access",           GBX_META },
	{ gbx_test_stat,            "stat",             GBX_META },
	{ gbx_test_statvfs,         "statvfs",          GBX_META },
	{ gbx_test_link,            "link",             GBX_META },
	{ gbx_test_mkdir,           "mkdir",            GBX_META },
	{ gbx_test_readdir,         "readdir",          GBX_META },
	{ gbx_test_chmod,           "chmod",            GBX_META },
	{ gbx_test_symlink,         "symlink",          GBX_META },
	{ gbx_test_open,            "open",             GBX_META },
	{ gbx_test_rename,          "rename",           GBX_META },
	{ gbx_test_fallocate,       "fallocate",        GBX_META },
	{ gbx_test_mmap,            "mmap",             GBX_META },
	{ gbx_test_rw_basic,        "rw-basic",         GBX_META },
	{ gbx_test_rw_sequencial,   "rw-sequencial",    GBX_META },
	{ gbx_test_rw_truncate,     "rw-truncate",      GBX_META },
	{ gbx_test_rw_sparse,       "rw-sparse",        GBX_META },
	{ gbx_test_rw_threads,      "rw-threads",       GBX_META },
	{ NULL, NULL, 0 }
};

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

#include "fnxinfra.h"
#include "graybox.h"


static void inc_buf(void *buf, size_t bsz)
{
	uint64_t *ptr = (uint64_t *)buf;
	const size_t cnt = (bsz / sizeof(*ptr));

	for (size_t i = 0; i < cnt; ++i) {
		*ptr++ += 1;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Tests data-consistency of sequencial write-read, segments aligned
 */
static void test_rw_sequencial_simple(gbx_ctx_t *gbx)
{
	int fd;
	loff_t pos = -1;
	size_t i, nwr, nrd;
	const size_t bsz = FNX_SEGSIZE;
	const size_t cnt = FNX_SLICENSEG;
	void *buf1, *buf2;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	buf1 = gbx_newbuf(bsz);
	buf2 = gbx_newbuf(bsz);

	gbx_fillrand(gbx, buf1, bsz);
	memcpy(buf2, buf1, bsz);

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	for (i = 0; i < cnt; ++i) {
		gbx_expect(gbx, fnx_write(fd, buf1, bsz, &nwr), 0);
		gbx_expect(gbx, (long)nwr, (long)bsz);
		inc_buf(buf1, bsz);
	}
	gbx_expect(gbx, fnx_lseek(fd, 0, SEEK_SET, &pos), 0);
	for (i = 0; i < cnt; ++i) {
		gbx_expect(gbx, fnx_read(fd, buf1, bsz, &nrd), 0);
		gbx_expect(gbx, (long)nrd, (long)bsz);
		gbx_expect(gbx, memcmp(buf1, buf2, bsz), 0);
		inc_buf(buf2, bsz);
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf1);
	gbx_delbuf(buf2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Tests data-consistency of sequencial write-read-trim, segments aligned
 */
static void test_rw_sequencial_rdwrtrim(gbx_ctx_t *gbx)
{
	int fd, mode;
	loff_t pos = -1;
	size_t i, nwr, nrd;
	const size_t bsz = FNX_SEGSIZE;
	const size_t cnt = FNX_SLICENSEG;
	void *buf1, *buf2;
	char *path;

	mode = FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE;
	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	buf1 = gbx_newbuf(bsz);
	buf2 = gbx_newbuf(bsz);

	gbx_fillrand(gbx, buf1, bsz);
	memcpy(buf2, buf1, bsz);

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	for (i = 0; i < cnt; ++i) {
		gbx_expect(gbx, fnx_write(fd, buf1, bsz, &nwr), 0);
		gbx_expect(gbx, (long)nwr, (long)bsz);
		gbx_expect(gbx, fnx_lseek(fd, -((off_t)bsz), SEEK_CUR, &pos), 0);
		gbx_expect(gbx, fnx_read(fd, buf2, bsz, &nrd), 0);
		gbx_expect(gbx, (long)nrd, (long)bsz);
		gbx_expect(gbx, memcmp(buf1, buf2, bsz), 0);
		gbx_expect(gbx, fnx_fallocate(fd, mode, pos, (loff_t)bsz), 0);
		inc_buf(buf1, bsz);
	}

	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf1);
	gbx_delbuf(buf2);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_rw_sequencial(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_io_sequencial_tests[] = {
		{ test_rw_sequencial_simple,    "rw-sequencial-simple",     GBX_RDWR },
		{ test_rw_sequencial_rdwrtrim,  "rw-sequencial-rdwrtrim",   GBX_RDWR },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_io_sequencial_tests);
}


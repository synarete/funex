/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects fallocate(2) to successfully allocate space, and return EBADF if
 * fd is not opened for writing.
 */
static void test_fallocate_basic(gbx_ctx_t *gbx)
{
	int fd;
	loff_t len = FNX_BLKSIZE;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_create(path, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 0, len), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_open(path, O_RDONLY, 0, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, len, 2 * len), -EBADF);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects fallocate(2) to successfully allocate space for file's sub-ranges.
 */
static void test_fallocate_chunks(gbx_ctx_t *gbx)
{
	int fd;
	loff_t segsz = FNX_SEGSIZE;
	loff_t slcsz = FNX_SLICESIZE;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_create(path, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 0, segsz), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 1, segsz), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 7 * segsz, 1), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, (7 * segsz) - 1, 2), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_expect(gbx, fnx_create(path, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 1, slcsz), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, (7 * slcsz) - 1, segsz), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_fallocate(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_fallocate_tests[] = {
		{ test_fallocate_basic,     "fallocate-basic",      GBX_POSIX },
		{ test_fallocate_chunks,    "fallocate-chunks",     GBX_POSIX },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_fallocate_tests);
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects symlink(3p) to succefully create symbloic-links.
 */
static void test_symlink_simple(gbx_ctx_t *gbx)
{
	int fd;
	mode_t ifmt = S_IFMT;
	struct stat st0, st1;
	char *path0, *path1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path0, 0600, &fd), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_expect(gbx, S_ISREG(st0.st_mode), 1);
	gbx_expect(gbx, (int)(st0.st_mode & ~ifmt), 0600);

	gbx_expect(gbx, fnx_symlink(path0, path1), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, (st0.st_ino == st1.st_ino), 1);
	gbx_expect(gbx, fnx_lstat(path1, &st1), 0);
	gbx_expect(gbx, (st0.st_ino == st1.st_ino), 0);
	gbx_expect(gbx, S_ISLNK(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0777);

	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects readlink(3p) to succefully read symbloic-links and return EINVAL if
 * the path argument names a file that is not a symbolic link.
 */
static void test_symlink_readlink(gbx_ctx_t *gbx)
{
	int fd;
	size_t nch, bsz = PATH_MAX;
	struct stat st1;
	char *path0, *path1, *path2, *buf;

	buf   = (char *)malloc(bsz);
	memset(buf, 0, bsz);

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path0, 0600, &fd), 0);
	gbx_expect(gbx, fnx_symlink(path0, path1), 0);
	gbx_expect(gbx, fnx_lstat(path1, &st1), 0);
	gbx_expect(gbx, S_ISLNK(st1.st_mode), 1);
	gbx_expect(gbx, (long)st1.st_size, (long)strlen(path0));

	gbx_expect(gbx, fnx_readlink(path1, buf, bsz, &nch), 0);
	gbx_expect(gbx, strncmp(buf, path0, nch), 0);
	gbx_expect(gbx, fnx_readlink(path0, buf, bsz, &nch), -EINVAL);
	gbx_expect(gbx, fnx_readlink(path2, buf, bsz, &nch), -ENOENT);

	memset(buf, 0, bsz);
	gbx_expect(gbx, fnx_readlink(path1, buf, 1, &nch), 0);
	gbx_expect(gbx, !strcmp(buf, path0), 0);

	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	free(buf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects readlink(3p) to update symlink access-time.
 */
static void test_readlink_atime(gbx_ctx_t *gbx)
{
	size_t nch, bsz = PATH_MAX;
	struct stat st;
	time_t atime1, atime2;
	char *path0, *path1, *buf;

	buf   = (char *)malloc(bsz);
	memset(buf, 0, bsz);

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_symlink(path0, path1), 0);
	gbx_expect(gbx, fnx_lstat(path1, &st), 0);
	gbx_expect(gbx, S_ISLNK(st.st_mode), 1);
	gbx_expect(gbx, (long)st.st_size, (long)strlen(path0));

	atime1 = st.st_atim.tv_sec;
	gbx_expect(gbx, fnx_readlink(path1, buf, bsz, &nch), 0);
	gbx_expect(gbx, fnx_lstat(path1, &st), 0);
	atime2 = st.st_atim.tv_sec;
	gbx_expect(gbx, strncmp(buf, path0, nch), 0);
	gbx_expect(gbx, (atime1 <= atime2), 1);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_readlink(path1, buf, bsz, &nch), 0);
	gbx_expect(gbx, fnx_lstat(path1, &st), 0);
	atime2 = st.st_atim.tv_sec;
	gbx_expect(gbx, strncmp(buf, path0, nch), 0);
	gbx_expect(gbx, (atime1 < atime2), 1);

	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	free(buf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_symlink(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_symlink_tests[] = {
		{ test_symlink_simple,      "symlink-simple",   GBX_POSIX },
		{ test_symlink_readlink,    "symlink-readlink", GBX_POSIX },
		{ test_readlink_atime,      "readlink-atime",   GBX_POSIX },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gbx, s_symlink_tests);
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <uuid/uuid.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects Linux getdents(2) to read all dir-entries.
 */
static void test_readdir_basic(gbx_ctx_t *gbx)
{
	int fd0, fd1;
	loff_t off;
	unsigned i, lim = FNX_DIRTOP_NDENT;
	struct stat st;
	char name[NAME_MAX + 1] = "";
	fnx_dirent_t dent;
	char *path0, *path1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	gbx_expect(gbx, fnx_open(path0, O_DIRECTORY | O_RDONLY, 0, &fd0), 0);
	for (i = 0; i < (lim - 2); ++i) {
		snprintf(name, sizeof(name) - 1, "%08x", i);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_create(path1, 0600, &fd1), 0);
		gbx_expect(gbx, fnx_close(fd1), 0);
		gbx_delpath(path1);
	}
	off = -1;
	for (i = 0; i < lim; ++i) {
		gbx_expect(gbx, fnx_getdent(fd0, off, &dent), 0);
		if (strcmp(".", dent.d_name) && strcmp("..", dent.d_name)) {
			gbx_expect(gbx, S_ISREG(dent.d_type), 1);
		}
		off = dent.d_off;
	}
	for (i = 0; i < (lim - 2); ++i) {
		snprintf(name, sizeof(name) - 1, "%08x", i);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_stat(path1, &st), 0);
		gbx_expect(gbx, fnx_unlink(path1), 0);
		gbx_expect(gbx, fnx_stat(path1, &st), -ENOENT);
		gbx_delpath(path1);
	}

	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_close(fd0), 0);
	gbx_delpath(path0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects getdents(2) to read all dir-entries while unlinking.
 */
static void test_readdir_unlink(gbx_ctx_t *gbx)
{
	int fd;
	loff_t off;
	unsigned i, cnt, lim = FNX_DIRTOP_NDENT * 2;
	struct stat st;
	char name[NAME_MAX + 1] = "";
	fnx_dirent_t dent;
	char *path0, *path1;
	uuid_t uu;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	for (i = 0; i < lim; ++i) {
		uuid_generate_time(uu);
		uuid_unparse(uu, name);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_mkdir(path1, 0750), 0);
		gbx_delpath(path1);
	}
	gbx_expect(gbx, fnx_open(path0, O_DIRECTORY | O_RDONLY, 0, &fd), 0);
	off = -1;
	cnt = 0;
	while (cnt < lim) {
		gbx_expect(gbx, cnt < (8 * lim), 1);
		gbx_expect(gbx, fnx_getdent(fd, off, &dent), 0);
		if (!strcmp(".", dent.d_name) || !strcmp("..", dent.d_name)) {
			off = dent.d_off;
		} else if (!strlen(dent.d_name)) {
			off = -1;
		} else {
			gbx_expect(gbx, S_ISDIR(dent.d_type), 1);
			path1 = gbx_newpath(path0, dent.d_name);
			gbx_expect(gbx, fnx_rmdir(path1), 0);
			gbx_expect(gbx, fnx_stat(path1, &st), -ENOENT);
			gbx_delpath(path1);
			off = dent.d_off;
			cnt++;
		}
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects Linux getdents(2) to read all dir-entries of large dir.
 */
static void test_readdir_getdents(gbx_ctx_t *gbx, int lim)
{
	int i, fd0, fd1;
	loff_t off;
	struct stat st;
	char name[NAME_MAX + 1] = "";
	fnx_dirent_t dent;
	char *path0, *path1;
	char *name1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	name1 = gbx_genname(gbx);

	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	for (i = 0; i < lim; ++i) {
		snprintf(name, sizeof(name) - 1, "%s-%08x", name1, i);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_create(path1, 0600, &fd1), 0);
		gbx_expect(gbx, fnx_close(fd1), 0);
		gbx_delpath(path1);
	}

	gbx_expect(gbx, fnx_open(path0, O_DIRECTORY | O_RDONLY, 0, &fd0), 0);
	off = -1;
	for (i = 0; i < lim; ++i) {
		gbx_expect(gbx, fnx_getdent(fd0, off, &dent), 0);
		if (strcmp(".", dent.d_name) && strcmp("..", dent.d_name)) {
			gbx_expect(gbx, S_ISREG(dent.d_type), 1);
			gbx_expect(gbx, strncmp(dent.d_name, name1, strlen(name1)), 0);
		}
		off = dent.d_off;
	}
	gbx_expect(gbx, fnx_close(fd0), 0);

	for (i = 0; i < lim; ++i) {
		snprintf(name, sizeof(name) - 1, "%s-%08x", name1, i);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_stat(path1, &st), 0);
		gbx_expect(gbx, fnx_unlink(path1), 0);
		gbx_expect(gbx, fnx_stat(path1, &st), -ENOENT);
		gbx_delpath(path1);
	}

	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path0);
}


static void test_readdir_big(gbx_ctx_t *gbx)
{
	const int lim = FNX_DIRTOP_NDENT + FNX_DIRSEG_NDENT;
	test_readdir_getdents(gbx, lim);
}

static void test_readdir_large(gbx_ctx_t *gbx)
{
	const int lim = FNX_DOFF_END / 8;
	test_readdir_getdents(gbx, lim);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_readdir(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_readdir_tests[] = {
		{ test_readdir_basic,  "readdir-basic",     GBX_POSIX },
		{ test_readdir_unlink, "readdir-unlink",    GBX_POSIX },
		{ test_readdir_big,    "readdir-big",       GBX_POSIX },
		{ test_readdir_large,  "readdir-large",     GBX_STRESS },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_readdir_tests);
}

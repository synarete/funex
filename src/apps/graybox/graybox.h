/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_GRAYBOX_H_
#define FUNEX_GRAYBOX_H_

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "fnxdefs.h"
#include "fnxuser.h"


/* Helper macros */
#define gbx_expect(ctx, res, exp) \
	gbx_do_expect(ctx, __FILE__, __LINE__, (long)(res), (long)(exp))

#define gbx_nelems(x) \
	(sizeof(x) / sizeof(x[0]))


/* Tests control flags */
#define GBX_META        (1 << 0)
#define GBX_POSIX       (1 << 1)
#define GBX_ADMIN       (1 << 2)
#define GBX_RDWR        (1 << 3)
#define GBX_CUSTOM      (1 << 4)
#define GBX_STRESS      (1 << 5)
#define GBX_PERF        (1 << 6)


#define GBX_ALL \
	(GBX_POSIX | GBX_RDWR | GBX_CUSTOM | GBX_STRESS)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Context object */
struct gbx_ctx {
	char *base;
	void *pctx;
	int  mask;
	char isroot;
	char cap_chown;
	char cap_mknod;
	char cap_fowner;
	char cap_fsetid;
	char cap_sysadmin;
	char name[256];
	char sbuf[1024];
	struct random_data rnd;
	const struct gbx_execdef *curr;
};
typedef struct gbx_ctx gbx_ctx_t;

/* Execution-hook */
typedef void (*gbx_exec_fn)(gbx_ctx_t *);

/* Execution (test) definer */
struct gbx_execdef {
	gbx_exec_fn  hook;
	const char  *name;
	signed int   flags;
};
typedef struct gbx_execdef gbx_execdef_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Context */
void gbx_init(gbx_ctx_t *, const char *, pid_t);

void gbx_destroy(gbx_ctx_t *);

void gbx_execute(gbx_ctx_t *, const gbx_execdef_t []);

void gbx_fillrand(gbx_ctx_t *, void *, size_t);

void gbx_suspend(gbx_ctx_t *, int, int);

char *gbx_genname(gbx_ctx_t *);

char *gbx_newpath(const char *, const char *);

void gbx_delpath(char *);

void *gbx_newbuf(size_t);

void gbx_delbuf(void *);

void gbx_do_expect(gbx_ctx_t *, const char *, unsigned, long, long);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Tests */
void gbx_test_stat(gbx_ctx_t *);
void gbx_test_statvfs(gbx_ctx_t *);
void gbx_test_mkdir(gbx_ctx_t *);
void gbx_test_readdir(gbx_ctx_t *);
void gbx_test_access(gbx_ctx_t *);
void gbx_test_chmod(gbx_ctx_t *);
void gbx_test_link(gbx_ctx_t *);
void gbx_test_open(gbx_ctx_t *);
void gbx_test_rename(gbx_ctx_t *);
void gbx_test_symlink(gbx_ctx_t *);
void gbx_test_fallocate(gbx_ctx_t *);
void gbx_test_rw_basic(gbx_ctx_t *);
void gbx_test_rw_truncate(gbx_ctx_t *);
void gbx_test_rw_sequencial(gbx_ctx_t *);
void gbx_test_rw_sparse(gbx_ctx_t *);
void gbx_test_rw_threads(gbx_ctx_t *);
void gbx_test_mmap(gbx_ctx_t *);

extern const gbx_execdef_t gbx_tests_list[];

#endif /* FUNEX_GRAYBOX_H_ */

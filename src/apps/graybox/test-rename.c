/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "graybox.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects rename(3p) to successfully change file's new-name and return ENOENT
 * on old-name.
 */
static void test_rename_simple(gbx_ctx_t *gbx)
{
	int fd;
	ino_t ino;
	mode_t ifmt = S_IFMT;
	struct stat st0, st1;
	char *path0, *path1, *path2;

	path2 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path0 = gbx_newpath(path2, gbx_genname(gbx));
	path1 = gbx_newpath(path2, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path2, 0755), 0);
	gbx_expect(gbx, fnx_create(path0, 0644, &fd), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_expect(gbx, S_ISREG(st0.st_mode), 1);
	gbx_expect(gbx, (int)(st0.st_mode & ~ifmt), 0644);
	gbx_expect(gbx, (long)st0.st_nlink, 1);

	ino = st0.st_ino;
	gbx_expect(gbx, fnx_rename(path0, path1), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), -ENOENT);
	gbx_expect(gbx, fnx_fstat(fd, &st0), 0);
	gbx_expect(gbx, (long)st0.st_ino, (long)ino);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISREG(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0644);
	gbx_expect(gbx, (long)st1.st_nlink, 1);

	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path2), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects rename(3p) to update ctime only when successful.
 */
static void test_rename_ctime(gbx_ctx_t *gbx)
{
	int fd;
	struct stat st0, st1;
	char *path0, *path1, *path2, *path3;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path3 = gbx_newpath(path2, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path0, 0644, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_rename(path0, path1), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, (st0.st_ctim.tv_sec < st1.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_rename(path0, path1), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, (st0.st_ctim.tv_sec < st1.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	gbx_expect(gbx, fnx_mkfifo(path0, 0644), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_rename(path0, path1), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, (st0.st_ctim.tv_sec < st1.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_symlink(path2, path0), 0);
	gbx_expect(gbx, fnx_lstat(path0, &st0), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_rename(path0, path1), 0);
	gbx_expect(gbx, fnx_lstat(path1, &st1), 0);
	gbx_expect(gbx, (st0.st_ctim.tv_sec < st1.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);


	gbx_expect(gbx, fnx_create(path0, 0644, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_suspend(gbx, 2, 1000);
	gbx_expect(gbx, fnx_rename(path0, path3), -ENOENT);
	gbx_expect(gbx, fnx_stat(path0, &st1), 0);
	gbx_expect(gbx, (st0.st_ctim.tv_sec == st1.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_unlink(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	gbx_delpath(path3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects rename(3p) to returns ENOTDIR when the 'from' argument is a
 * directory, but 'to' is not a directory.
 */
static void test_rename_notdirto(gbx_ctx_t *gbx)
{
	int fd;
	struct stat st0, st1;
	char *path0, *path1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0750), 0);
	gbx_expect(gbx, fnx_create(path1, 0644, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_rename(path0, path1), -ENOTDIR);
	gbx_expect(gbx, fnx_lstat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_symlink("test-rename-notdirto", path1), 0);
	gbx_expect(gbx, fnx_rename(path0, path1), -ENOTDIR);
	gbx_expect(gbx, fnx_lstat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, fnx_lstat(path1, &st1), 0);
	gbx_expect(gbx, S_ISLNK(st1.st_mode), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects rename(3p) to returns EISDIR when the 'to' argument is a
 * directory, but 'from' is not a directory.
 */
static void test_rename_isdirto(gbx_ctx_t *gbx)
{
	int fd;
	struct stat st0, st1;
	char *path0, *path1;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0750), 0);
	gbx_expect(gbx, fnx_create(path1, 0640, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_rename(path1, path0), -EISDIR);
	gbx_expect(gbx, fnx_lstat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_mkfifo(path1, 0644), 0);
	gbx_expect(gbx, fnx_rename(path1, path0), -EISDIR);
	gbx_expect(gbx, fnx_lstat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, fnx_lstat(path1, &st1), 0);
	gbx_expect(gbx, S_ISFIFO(st1.st_mode), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_symlink("test-rename-isdirto", path1), 0);
	gbx_expect(gbx, fnx_rename(path1, path0), -EISDIR);
	gbx_expect(gbx, fnx_lstat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);
	gbx_expect(gbx, fnx_lstat(path1, &st1), 0);
	gbx_expect(gbx, S_ISLNK(st1.st_mode), 1);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_rename(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_rename_tests[] = {
		{ test_rename_simple,   "rename-simple",    GBX_POSIX },
		{ test_rename_ctime,    "rename-ctime",     GBX_POSIX },
		{ test_rename_notdirto, "rename-nottdirto", GBX_POSIX },
		{ test_rename_isdirto,  "rename-isdirto",   GBX_POSIX },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_rename_tests);
}

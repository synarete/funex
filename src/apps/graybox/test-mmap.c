/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "graybox.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Expects mmap(3p) to successfully establish a mapping between a process'
 * address space and a file.
 */
static void test_mmap_basic(gbx_ctx_t *gbx)
{
	int fd, prot, flag;
	size_t mlen;
	char *path;
	void *addr;

	mlen = FNX_BLKSIZE;
	prot = PROT_READ | PROT_WRITE;
	flag = MAP_PRIVATE;
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 0, (loff_t)mlen), 0);
	gbx_expect(gbx, fnx_mmap(fd, 0, mlen, prot, flag, &addr), 0);
	strncpy((char *)addr, path, mlen);
	gbx_expect(gbx, strncmp((char *)addr, path, mlen), 0);
	gbx_expect(gbx, fnx_munmap(addr, mlen), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_mmap_simple(gbx_ctx_t *gbx)
{
	int fd, prot, flag;
	size_t mlen;
	char *path;
	void *addr, *mbuf;

	mlen = FNX_SEGSIZE;
	mbuf = gbx_newbuf(mlen);
	gbx_fillrand(gbx, mbuf, mlen);

	prot = PROT_READ | PROT_WRITE;
	flag = MAP_PRIVATE;
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 0, (loff_t)mlen), 0);
	gbx_expect(gbx, fnx_mmap(fd, 0, mlen, prot, flag, &addr), 0);
	memcpy(addr, mbuf, mlen);
	gbx_expect(gbx, memcmp(addr, mbuf, mlen), 0);
	gbx_expect(gbx, fnx_munmap(addr, mlen), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(mbuf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_mmap_sequential(gbx_ctx_t *gbx)
{
	int fd, prot, flag;
	size_t i, bsz, cnt, mmsz;
	char *path, *ptr;
	void *addr, *buf;

	cnt = FNX_KILO;
	bsz = FNX_SEGSIZE;
	buf = gbx_newbuf(bsz);
	gbx_fillrand(gbx, buf, bsz);

	prot = PROT_READ | PROT_WRITE;
	flag = MAP_PRIVATE;
	mmsz = bsz * cnt;
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 0, (loff_t)mmsz), 0);
	gbx_expect(gbx, fnx_mmap(fd, 0, mmsz, prot, flag, &addr), 0);
	ptr = (char *)addr;
	for (i = 0; i < cnt; ++i) {
		ptr = (char *)addr + (i * bsz);
		memcpy(ptr, buf, bsz);
	}
	for (i = 0; i < cnt; ++i) {
		ptr = (char *)addr + (i * bsz);
		gbx_expect(gbx, memcmp(ptr, buf, bsz), 0);
	}
	gbx_expect(gbx, fnx_munmap(addr, mmsz), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_mmap_sparse(gbx_ctx_t *gbx)
{
	int fd, prot, flag;
	size_t i, bsz, mmsz, nval;
	uint16_t val, val2, *ptr, *arr;
	char *path;
	void *addr, *buf;

	bsz = FNX_BLKSIZE;
	buf = gbx_newbuf(bsz);
	gbx_fillrand(gbx, buf, bsz);

	prot = PROT_READ | PROT_WRITE;
	flag = MAP_PRIVATE;
	nval = (uint16_t)(bsz / sizeof(val));
	mmsz = UINT32_MAX * sizeof(val);
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, 0, (loff_t)mmsz), 0);
	gbx_expect(gbx, fnx_mmap(fd, 0, mmsz, prot, flag, &addr), 0);
	ptr = (uint16_t *)addr;
	arr = (uint16_t *)buf;
	for (i = 0; i < nval; ++i) {
		val = arr[i];
		ptr[val] = val;
	}
	for (i = 0; i < nval; ++i) {
		val = arr[i];
		val2 = ptr[val];
		gbx_expect(gbx, val, val2);
	}
	gbx_expect(gbx, fnx_munmap(addr, mmsz), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_mmap_msync_at(gbx_ctx_t *gbx, loff_t step)
{
	int fd, prot, flag;
	size_t bsz;
	char *path;
	void *buf, *addr = NULL;
	loff_t off;

	off = step * sysconf(_SC_PAGE_SIZE);
	bsz = FNX_SPCSIZE;
	buf = gbx_newbuf(bsz);
	gbx_fillrand(gbx, buf, bsz);

	prot = PROT_READ | PROT_WRITE;
	flag = MAP_SHARED;
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fallocate(fd, 0, off, (loff_t)bsz), 0);
	gbx_expect(gbx, fnx_mmap(fd, off, bsz, prot, flag, &addr), 0);
	memcpy(addr, buf, bsz);
	gbx_expect(gbx, fnx_msync(addr, bsz, MS_SYNC), 0);
	gbx_expect(gbx, fnx_munmap(addr, bsz), 0);
	gbx_expect(gbx, fnx_mmap(fd, off, bsz, prot, flag, &addr), 0);
	gbx_expect(gbx, memcmp(addr, buf, bsz), 0);
	gbx_expect(gbx, fnx_munmap(addr, bsz), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf);
}


static void test_mmap_msync(gbx_ctx_t *gbx)
{
	test_mmap_msync_at(gbx, 0);
	test_mmap_msync_at(gbx, 1);
	test_mmap_msync_at(gbx, 11);
	test_mmap_msync_at(gbx, 111);
	test_mmap_msync_at(gbx, 1111);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_mmap(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_mmap_tests[] = {
		{ test_mmap_basic,      "mmap-basic",       GBX_POSIX },
		{ test_mmap_simple,     "mmap-simple",      GBX_RDWR },
		{ test_mmap_sequential, "mmap-sequential",  GBX_RDWR },
		{ test_mmap_sparse,     "mmap-sparse",      GBX_RDWR },
		{ test_mmap_msync,      "mmap-msync",       GBX_RDWR },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_mmap_tests);
}


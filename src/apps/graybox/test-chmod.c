/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "graybox.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects chmod(3p) to do change permissions.
 */
static void test_chmod_basic(gbx_ctx_t *gbx)
{
	int fd;
	mode_t ifmt = S_IFMT;
	struct stat st;
	char *path0, *path1, *path2;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path0, 0644, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_stat(path0, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0644);
	gbx_expect(gbx, fnx_chmod(path0, 0111), 0);
	gbx_expect(gbx, fnx_stat(path0, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0111);
	gbx_expect(gbx, fnx_unlink(path0), 0);

	gbx_expect(gbx, fnx_mkdir(path1, 0755), 0);
	gbx_expect(gbx, fnx_stat(path1, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0755);
	gbx_expect(gbx, fnx_chmod(path1, 0753), 0);
	gbx_expect(gbx, fnx_stat(path1, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0753);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	gbx_expect(gbx, fnx_create(path0, 0644, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_symlink(path0, path2), 0);
	gbx_expect(gbx, fnx_stat(path2, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0644);
	gbx_expect(gbx, fnx_chmod(path2, 0321), 0);
	gbx_expect(gbx, fnx_stat(path2, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0321);
	gbx_expect(gbx, fnx_stat(path0, &st), 0);
	gbx_expect(gbx, (int)(st.st_mode & ~ifmt), 0321);
	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_unlink(path2), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects chmod(3p) to updates ctime if successful.
 */
static void test_chmod_ctime(gbx_ctx_t *gbx)
{
	int fd;
	struct stat st1, st2;
	char *path0, *path1, *path2;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path0, 0644, &fd), 0);
	gbx_expect(gbx, fnx_fstat(fd, &st1), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_chmod(path0, 0111), 0);
	gbx_expect(gbx, fnx_fstat(fd, &st2), 0);
	gbx_expect(gbx, (st1.st_ctim.tv_sec < st2.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path0), 0);

	gbx_expect(gbx, fnx_mkdir(path1, 0755), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_chmod(path1, 0753), 0);
	gbx_expect(gbx, fnx_stat(path1, &st2), 0);
	gbx_expect(gbx, (st1.st_ctim.tv_sec < st2.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	gbx_expect(gbx, fnx_mkfifo(path2, 0640), 0);
	gbx_expect(gbx, fnx_stat(path2, &st1), 0);
	gbx_suspend(gbx, 3, 2);
	gbx_expect(gbx, fnx_chmod(path2, 0300), 0);
	gbx_expect(gbx, fnx_stat(path2, &st2), 0);
	gbx_expect(gbx, (st1.st_ctim.tv_sec < st2.st_ctim.tv_sec), 1);
	gbx_expect(gbx, fnx_unlink(path2), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_chmod(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_chmod_tests[] = {
		{ test_chmod_basic,     "chmod-basic",      GBX_POSIX },
		{ test_chmod_ctime,     "chmod-ctime",      GBX_POSIX },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_chmod_tests);
}


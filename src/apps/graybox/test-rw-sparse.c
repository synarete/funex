/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

#include "fnxinfra.h"
#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Tests read-write data-consistency over sparse file.
 */
static void test_rw_sparse_simple(gbx_ctx_t *gbx)
{
	int fd;
	char *path;
	loff_t pos = -1;
	size_t i, num, num2;
	size_t nsz, nwr, nrd;
	const size_t cnt = 7717;
	const size_t step = 524287;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	for (i = 0; i < cnt; ++i) {
		num = (i * step);
		pos = (loff_t)num;
		nsz = sizeof(num);
		gbx_expect(gbx, fnx_pwrite(fd, &num, nsz, pos, &nwr), 0);
		gbx_expect(gbx, (long)nwr, (long)nsz);
	}
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_expect(gbx, fnx_open(path, O_RDONLY, 0, &fd), 0);
	for (i = 0; i < cnt; ++i) {
		num = (i * step);
		pos = (loff_t)num;
		nsz = sizeof(num2);
		gbx_expect(gbx, fnx_pread(fd, &num2, nsz, pos, &nrd), 0);
		gbx_expect(gbx, (long)nrd, (long)nsz);
		gbx_expect(gbx, (long)num, (long)num2);
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Tests read-write data-consistency over sparse file with syncs over same file.
 */
static void test_rw_sparse_rdwr(gbx_ctx_t *gbx)
{
	int fd;
	char *path;
	loff_t pos = -1;
	size_t num, num2;
	size_t nsz, nwr, nrd;
	const size_t cnt  = 127;
	const size_t step = 524287;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	for (size_t i = 0; i < 17; ++i) {
		for (size_t j = 0; j < cnt; ++j) {
			gbx_expect(gbx, fnx_open(path, O_RDWR, 0, &fd), 0);
			num = i + (j * step);
			pos = (loff_t)num;
			nsz = sizeof(num);
			gbx_expect(gbx, fnx_pwrite(fd, &num, nsz, pos, &nwr), 0);
			gbx_expect(gbx, (long)nwr, (long)nsz);
			gbx_expect(gbx, fnx_fdatasync(fd), 0);
			gbx_expect(gbx, fnx_pread(fd, &num2, nsz, pos, &nrd), 0);
			gbx_expect(gbx, (long)nrd, (long)nsz);
			gbx_expect(gbx, (long)num, (long)num2);
			gbx_expect(gbx, fnx_close(fd), 0);
		}
	}

	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_rw_sparse(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_io_sparse_tests[] = {
		{ test_rw_sparse_simple,    "rw-sparse-simple",     GBX_RDWR },
		{ test_rw_sparse_rdwr,      "rw-sparse-rdwr",       GBX_RDWR },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_io_sparse_tests);
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "graybox.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects read-write data-consistency, sequential writes of single block.
 */
static void test_rw_basic_simple(gbx_ctx_t *gbx)
{
	int fd;
	loff_t pos = -1;
	size_t i, n, bsz;
	size_t nwr, nrd;
	struct stat st;
	char *path;
	void *buf;

	bsz  = FNX_BLKSIZE;
	buf  = gbx_newbuf(bsz);
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0644, &fd), 0);
	for (i = 0; i < FNX_SEGNBK; ++i) {
		n = i;
		memcpy(buf, &n, sizeof(n));
		gbx_expect(gbx, fnx_write(fd, buf, bsz, &nwr), 0);
		gbx_expect(gbx, fnx_fstat(fd, &st), 0);
		gbx_expect(gbx, (long)st.st_size, (long)((i + 1) * bsz));
	}
	gbx_expect(gbx, fnx_lseek(fd, 0, SEEK_SET, &pos), 0);
	for (i = 0; i < FNX_SEGNBK; ++i) {
		gbx_expect(gbx, fnx_read(fd, buf, bsz, &nrd), 0);
		memcpy(&n, buf, sizeof(n));
		gbx_expect(gbx, (long)i, (long)n);
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects read-write data-consistency for buffer-size of 1M.
 */
static void test_rw_buffer(gbx_ctx_t *gbx, size_t bsz)
{
	int fd, cmp;
	size_t i, n;
	void *buf1, *buf2;
	char *path;
	struct stat st;

	buf1 = gbx_newbuf(bsz);
	buf2 = gbx_newbuf(bsz);
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	for (i = 0; i < 5; ++i) {
		gbx_fillrand(gbx, buf1, bsz);
		gbx_expect(gbx, fnx_pwrite(fd, buf1, bsz, 0, &n), 0);
		gbx_expect(gbx, fnx_fsync(fd), 0);
		gbx_expect(gbx, fnx_pread(fd, buf2, bsz, 0, &n), 0);
		gbx_expect(gbx, fnx_fstat(fd, &st), 0);
		gbx_expect(gbx, (long)st.st_size, (long)bsz);
		cmp = memcmp(buf1, buf2, bsz);
		gbx_expect(gbx, (long)cmp, 0);
	}

	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
	gbx_delbuf(buf1);
	gbx_delbuf(buf2);
}

static void test_rw_basic_rdwr_1k(gbx_ctx_t *gbx)
{
	test_rw_buffer(gbx, 1024);
}

static void test_rw_basic_rdwr_8k(gbx_ctx_t *gbx)
{
	test_rw_buffer(gbx, 8 * 1024);
}

static void test_rw_basic_rdwr_1m(gbx_ctx_t *gbx)
{
	test_rw_buffer(gbx, 1024 * 1024);
}

static void test_rw_basic_rdwr_8m(gbx_ctx_t *gbx)
{
	test_rw_buffer(gbx, 8 * 1024 * 1024);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects read-write data-consistency, reverse writes of section block.
 */
static void test_rw_basic_reserve(gbx_ctx_t *gbx)
{
	int fd;
	loff_t pos = -1;
	size_t i, nwr, nrd;
	char *path;
	char  buf[1];

	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0644, &fd), 0);
	for (i = 0; i < FNX_SECSIZE; ++i) {
		buf[0] = (char)i;
		pos = (loff_t)(FNX_SECSIZE - i - 1);
		gbx_expect(gbx, fnx_pwrite(fd, buf, 1, pos, &nwr), 0);
		gbx_expect(gbx, (long)nwr, 1);
	}

	for (i = 0; i < FNX_SECSIZE; ++i) {
		pos = (loff_t)(FNX_SECSIZE - i - 1);
		gbx_expect(gbx, fnx_pread(fd, buf, 1, pos, &nrd), 0);
		gbx_expect(gbx, (long)nrd, 1);
		gbx_expect(gbx, (buf[0] == (char)i), 1);
	}

	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);
	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects read-write data-consistency when I/O overlaps
 */
static void test_rw_basic_overlap(gbx_ctx_t *gbx)
{
	int fd;
	loff_t off;
	size_t n, cnt, bsz = 2 * FNX_SECSIZE;
	void *buf1, *buf2, *buf3;
	char *path;

	buf1 = gbx_newbuf(bsz);
	buf2 = gbx_newbuf(bsz);
	buf3 = gbx_newbuf(bsz);
	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_fillrand(gbx, buf1, bsz);
	gbx_fillrand(gbx, buf2, bsz);

	gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
	gbx_expect(gbx, fnx_pwrite(fd, buf1, bsz, 0, &n), 0);
	gbx_expect(gbx, fnx_pread(fd, buf3, bsz, 0, &n), 0);
	gbx_expect(gbx, memcmp(buf1, buf3, bsz), 0);

	off = 17;
	cnt = 100;
	gbx_expect(gbx, fnx_pwrite(fd, buf2, cnt, off, &n), 0);
	gbx_expect(gbx, fnx_pread(fd, buf3, cnt, off, &n), 0);
	gbx_expect(gbx, memcmp(buf2, buf3, cnt), 0);

	off = 2099;
	cnt = 1000;
	gbx_expect(gbx, fnx_pwrite(fd, buf2, cnt, off, &n), 0);
	gbx_expect(gbx, fnx_pread(fd, buf3, cnt, off, &n), 0);
	gbx_expect(gbx, memcmp(buf2, buf3, cnt), 0);

	off = 32077;
	cnt = 10000;
	gbx_expect(gbx, fnx_pwrite(fd, buf2, cnt, off, &n), 0);
	gbx_expect(gbx, fnx_pread(fd, buf3, cnt, off, &n), 0);
	gbx_expect(gbx, memcmp(buf2, buf3, cnt), 0);

	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
	gbx_delbuf(buf1);
	gbx_delbuf(buf2);
	gbx_delbuf(buf3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Should not get ENOSPC for sequence of write-read-unlink (32M chunk each,
 * 2G total).
 */
static void test_rw_basic_space(gbx_ctx_t *gbx)
{
	int fd;
	size_t cnt, bsz = FNX_SPCSIZE;
	void *buf1;
	void *buf2;
	char *path;

	buf1  = gbx_newbuf(bsz);
	buf2  = gbx_newbuf(bsz);

	for (size_t i = 0; i < 100; ++i) {
		path = gbx_newpath(gbx->base, gbx_genname(gbx));
		gbx_fillrand(gbx, buf1, bsz);
		gbx_expect(gbx, fnx_open(path, O_CREAT | O_RDWR, 0600, &fd), 0);
		gbx_expect(gbx, fnx_pwrite(fd, buf1, bsz, 0, &cnt), 0);
		gbx_expect(gbx, fnx_pread(fd, buf2, bsz, 0, &cnt), 0);
		gbx_expect(gbx, memcmp(buf1, buf2, bsz), 0);
		gbx_expect(gbx, fnx_close(fd), 0);
		gbx_expect(gbx, fnx_unlink(path), 0);
		gbx_delpath(path);
	}

	gbx_delbuf(buf1);
	gbx_delbuf(buf2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_rw_basic(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_io_basic_tests[] = {
		{ test_rw_basic_simple,     "rw-basic-simple",      GBX_RDWR },
		{ test_rw_basic_rdwr_1k,    "rw-basic-buffer-1K",   GBX_RDWR },
		{ test_rw_basic_rdwr_8k,    "rw-basic-buffer-8K",   GBX_RDWR },
		{ test_rw_basic_rdwr_1m,    "rw-basic-buffer-1M",   GBX_RDWR },
		{ test_rw_basic_rdwr_8m,    "rw-basic-buffer-8M",   GBX_RDWR },
		{ test_rw_basic_reserve,    "rw-basic-reverse",     GBX_RDWR },
		{ test_rw_basic_overlap,    "rw-basic-overlap",     GBX_RDWR },
		{ test_rw_basic_space,      "rw-basic-space",       GBX_STRESS },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_io_basic_tests);
}


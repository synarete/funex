/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects truncate(3p) a regular file named by path to have a size which
 * shall be equal to length bytes.
 */
static void test_rw_truncate_basic(gbx_ctx_t *gbx)
{
	int fd;
	size_t i, nwr, cnt = 100;
	loff_t off;
	struct stat st;
	char *path;

	path = gbx_newpath(gbx->base, gbx_genname(gbx));

	gbx_expect(gbx, fnx_create(path, 0600, &fd), 0);
	for (i = 0; i < cnt; ++i) {
		gbx_expect(gbx, fnx_write(fd, path, strlen(path), &nwr), 0);
	}
	for (i = cnt; i > 0; i--) {
		off = (loff_t)(19 * i);
		gbx_expect(gbx, fnx_ftruncate(fd, off), 0);
		gbx_expect(gbx, fnx_fstat(fd, &st), 0);
		gbx_expect(gbx, (st.st_size == off), 1);
	}
	for (i = 0; i < cnt; i++) {
		off = (loff_t)(1811 * i);
		gbx_expect(gbx, fnx_ftruncate(fd, off), 0);
		gbx_expect(gbx, fnx_fstat(fd, &st), 0);
		gbx_expect(gbx, (st.st_size == off), 1);
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path), 0);

	gbx_delpath(path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_rw_truncate(gbx_ctx_t *gboxctx)
{
	static const gbx_execdef_t s_rw_truncate_tests[] = {
		{ test_rw_truncate_basic,  "rw-truncate-basic",   GBX_POSIX },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gboxctx, s_rw_truncate_tests);
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects mkdir(3p) to create a directory with a mode modified by the process'
 * umask.
 */
static void test_mkdir_umask(gbx_ctx_t *gbx)
{
	mode_t umsk, ifmt = S_IFMT;
	char *path0, *path1;
	struct stat st0, st1;

	umsk  = umask(S_IWGRP);
	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	gbx_expect(gbx, fnx_stat(path0, &st0), 0);
	gbx_expect(gbx, S_ISDIR(st0.st_mode), 1);

	gbx_expect(gbx, fnx_mkdir(path1, 0755), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISDIR(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0755);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	gbx_expect(gbx, fnx_mkdir(path1, 0153), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISDIR(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0153);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	umask(077);
	gbx_expect(gbx, fnx_mkdir(path1, 0151), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISDIR(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0100);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	umask(070);
	gbx_expect(gbx, fnx_mkdir(path1, 0345), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISDIR(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0305);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	umask(0501);
	gbx_expect(gbx, fnx_mkdir(path1, 0345), 0);
	gbx_expect(gbx, fnx_stat(path1, &st1), 0);
	gbx_expect(gbx, S_ISDIR(st1.st_mode), 1);
	gbx_expect(gbx, (int)(st1.st_mode & ~ifmt), 0244);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path0);
	gbx_delpath(path1);
	umask(umsk);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects mkdir(3p) to return ELOOP if too many symbolic links were
 * encountered in translating of the pathname.
 */
static void test_mkdir_loop(gbx_ctx_t *gbx)
{
	char *path0, *path1, *path2, *path3;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(path0, gbx_genname(gbx));
	path3 = gbx_newpath(path1, gbx_genname(gbx));

	gbx_expect(gbx, fnx_symlink(path0, path1), 0);
	gbx_expect(gbx, fnx_symlink(path1, path0), 0);
	gbx_expect(gbx, fnx_mkdir(path2, 0755), -ELOOP);
	gbx_expect(gbx, fnx_mkdir(path3, 0755), -ELOOP);
	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	gbx_delpath(path3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Verify creation & removal of many-many dir-entries.
 */
static void test_mkdir_many(gbx_ctx_t *gbx, size_t lim)
{
	int fd;
	size_t i;
	char *path0, *path1;
	struct stat st;
	char name[NAME_MAX] = "";

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	for (i = 0; i < lim; ++i) {
		snprintf(name, sizeof(name) - 1, "%08jx", i);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_open(path1, O_CREAT | O_RDWR, 0644, &fd), 0);
		gbx_expect(gbx, fnx_close(fd), 0);
		gbx_delpath(path1);
	}
	for (i = 0; i < lim; ++i) {
		snprintf(name, sizeof(name) - 1, "%08jx", i);
		path1 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_stat(path1, &st), 0);
		gbx_expect(gbx, fnx_unlink(path1), 0);
		gbx_delpath(path1);
	}
	gbx_expect(gbx, fnx_stat(path0, &st), 0);
	gbx_expect(gbx, (int)st.st_nlink, 2);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path0);
}

static void test_mkdir_big(gbx_ctx_t *gbx)
{
	test_mkdir_many(gbx, FNX_DIRTOP_NDENT * 2);
}

static void test_mkdir_bigger(gbx_ctx_t *gbx)
{
	test_mkdir_many(gbx, FNX_DIRTOP_NDENT * 32);
}

static void test_mkdir_large(gbx_ctx_t *gbx)
{
	test_mkdir_many(gbx, FNX_DOFF_END / 2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_mkdir(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_mkdir_tests[] = {
		{ test_mkdir_umask,     "mkdir-umask",  GBX_POSIX },
		{ test_mkdir_loop,      "mkdir-loop",   GBX_POSIX },
		{ test_mkdir_big,       "mkdir-big",    GBX_POSIX },
		{ test_mkdir_bigger,    "mkdir-bigger", GBX_POSIX },
		{ test_mkdir_large,     "mkdir-large",  GBX_STRESS },
		{ NULL, NULL, 0 }
	};
	gbx_execute(gbx, s_mkdir_tests);
}

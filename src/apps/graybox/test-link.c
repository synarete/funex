/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "graybox.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects link(3p) to return EEXIST if the path2 argument resolves to an
 * existing file or refers to a symbolic link.
 */
static void test_link_exists(gbx_ctx_t *gbx)
{
	int fd0, fd1;
	char *path0, *path1, *path2;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path2 = gbx_newpath(gbx->base, "test-link-to-symlink-exist");

	gbx_expect(gbx, fnx_create(path0, 0644, &fd0), 0);
	gbx_expect(gbx, fnx_create(path1, 0644, &fd1), 0);

	gbx_expect(gbx, fnx_link(path0, path1), -EEXIST);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_mkdir(path1, 0755), 0);
	gbx_expect(gbx, fnx_link(path0, path1), -EEXIST);
	gbx_expect(gbx, fnx_rmdir(path1), 0);

	gbx_expect(gbx, fnx_symlink(path1, path2), 0);
	gbx_expect(gbx, fnx_link(path0, path2), -EEXIST);
	gbx_expect(gbx, fnx_unlink(path2), 0);

	gbx_expect(gbx, fnx_mkfifo(path1, 0644), 0);
	gbx_expect(gbx, fnx_link(path0, path1), -EEXIST);
	gbx_expect(gbx, fnx_unlink(path1), 0);

	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_close(fd0), 0);
	gbx_expect(gbx, fnx_close(fd1), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects link(3p) to return ENOENT if the source file does not exist.
 */
static void test_link_noent(gbx_ctx_t *gbx)
{
	int fd;
	char *path0, *path1, *path2;

	path2 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path0 = gbx_newpath(path2, gbx_genname(gbx));
	path1 = gbx_newpath(path2, gbx_genname(gbx));

	gbx_expect(gbx, fnx_mkdir(path2, 0700), 0);
	gbx_expect(gbx, fnx_create(path0, 0640, &fd), 0);
	gbx_expect(gbx, fnx_link(path0, path1), 0);
	gbx_expect(gbx, fnx_unlink(path0), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_link(path0, path1), -ENOENT);
	gbx_expect(gbx, fnx_rmdir(path2), 0);
	gbx_expect(gbx, fnx_close(fd), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects link(3p) to return EEXIST if a component of either path prefix is
 * not a directory.
 */
static void test_link_notdir(gbx_ctx_t *gbx)
{
	int fd1, fd2;
	char *path0, *path1, *path2, *path3;

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	path2 = gbx_newpath(path0, gbx_genname(gbx));
	path3 = gbx_newpath(path1, "test-notdir");

	gbx_expect(gbx, fnx_mkdir(path0, 0755), 0);
	gbx_expect(gbx, fnx_create(path1, 0644, &fd1), 0);
	gbx_expect(gbx, fnx_link(path3, path2), -ENOTDIR);
	gbx_expect(gbx, fnx_create(path2, 0644, &fd2), 0);
	gbx_expect(gbx, fnx_link(path2, path3), -ENOTDIR);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_unlink(path2), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);

	gbx_expect(gbx, fnx_close(fd1), 0);
	gbx_expect(gbx, fnx_close(fd2), 0);

	gbx_delpath(path0);
	gbx_delpath(path1);
	gbx_delpath(path2);
	gbx_delpath(path3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects link(3p)/unlink(3p) sequence to succeed for renamed links.
 */
static void test_link_rename(gbx_ctx_t *gbx)
{
	int i, fd, nlink1, limit = 4096;
	struct stat st;
	char *path0, *path1, *path2, *path3;
	char name[NAME_MAX] = "";
	char name2[NAME_MAX] = "";

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	strncpy(name2, gbx_genname(gbx), sizeof(name2) - 1);

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_create(path1, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fstat(fd, &st), 0);
	nlink1 = (int)st.st_nlink;
	for (i = nlink1; i < limit; ++i) {
		snprintf(name, sizeof(name) - 1, "%s-%d", name2, i);
		path2 = gbx_newpath(path0, name);
		snprintf(name, sizeof(name) - 1, "%s-X-%d", name2, i);
		path3 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_link(path1, path2), 0);
		gbx_expect(gbx, fnx_rename(path2, path3), 0);
		gbx_expect(gbx, fnx_unlink(path2), -ENOENT);
		gbx_delpath(path2);
		gbx_delpath(path3);
	}
	for (i = limit - 1; i >= nlink1; --i) {
		snprintf(name, sizeof(name) - 1, "%s-X-%d", name2, i);
		path3 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_unlink(path3), 0);
		gbx_delpath(path3);
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path1);
	gbx_delpath(path0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects link(3p) to succeed for link count less then LINK_MAX.
 */
static void test_link_max(gbx_ctx_t *gbx)
{
	int i, fd, nlink1;
	struct stat st;
	char *path0, *path1, *path2;
	char name[NAME_MAX] = "";
	char name2[NAME_MAX] = "";

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	strncpy(name2, gbx_genname(gbx), sizeof(name2) - 1);

	gbx_expect(gbx, fnx_mkdir(path0, 0700), 0);
	gbx_expect(gbx, fnx_create(path1, 0600, &fd), 0);
	gbx_expect(gbx, fnx_fstat(fd, &st), 0);
	nlink1 = (int)st.st_nlink;
	for (i = nlink1; i < FNX_LINK_MAX; ++i) {
		snprintf(name, sizeof(name) - 1, "%s-%d", name2, i);
		path2 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_link(path1, path2), 0);
		gbx_delpath(path2);
	}
	gbx_expect(gbx, fnx_fstat(fd, &st), 0);
	gbx_expect(gbx, (st.st_nlink == FNX_LINK_MAX), 1);
	for (i = nlink1; i < FNX_LINK_MAX; ++i) {
		snprintf(name, sizeof(name) - 1, "%s-%d", name2, i);
		path2 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_unlink(path2), 0);
		gbx_delpath(path2);
	}
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_delpath(path1);
	gbx_delpath(path0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Expects link(3p) to return EMLINK if the link count of the file exceeds
 * LINK_MAX.
 */
static void test_link_limit(gbx_ctx_t *gbx)
{
	int i, fd, nlink1;
	struct stat st;
	char *path0, *path1, *path2;
	char name[NAME_MAX] = "";
	char name2[NAME_MAX] = "";

	path0 = gbx_newpath(gbx->base, gbx_genname(gbx));
	path1 = gbx_newpath(path0, gbx_genname(gbx));
	strncpy(name2, gbx_genname(gbx), sizeof(name2) - 1);

	gbx_expect(gbx, fnx_mkdir(path0, 0750), 0);
	gbx_expect(gbx, fnx_create(path1, 0640, &fd), 0);
	gbx_expect(gbx, fnx_fstat(fd, &st), 0);
	nlink1 = (int)st.st_nlink;
	for (i = nlink1; i < FNX_LINK_MAX; ++i) {
		snprintf(name, sizeof(name) - 1, "%d-%s", i, name2);
		path2 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_link(path1, path2), 0);
		gbx_delpath(path2);
	}
	gbx_expect(gbx, fnx_fstat(fd, &st), 0);
	gbx_expect(gbx, (st.st_nlink == FNX_LINK_MAX), 1);

	snprintf(name, sizeof(name) - 1, "link-fail-%s", name2);
	path2 = gbx_newpath(path0, name);
	gbx_expect(gbx, fnx_link(path1, path2), -EMLINK);
	gbx_delpath(path2);

	for (i = nlink1; i < FNX_LINK_MAX; i += 2) {
		snprintf(name, sizeof(name) - 1, "%d-%s", i, name2);
		path2 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_unlink(path2), 0);
		gbx_delpath(path2);
	}
	for (i = (nlink1 + 1); i < FNX_LINK_MAX; i += 2) {
		snprintf(name, sizeof(name) - 1, "%d-%s", i, name2);
		path2 = gbx_newpath(path0, name);
		gbx_expect(gbx, fnx_unlink(path2), 0);
		gbx_delpath(path2);
	}

	gbx_expect(gbx, fnx_unlink(path1), 0);
	gbx_expect(gbx, fnx_rmdir(path0), 0);
	gbx_expect(gbx, fnx_close(fd), 0);
	gbx_delpath(path1);
	gbx_delpath(path0);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void gbx_test_link(gbx_ctx_t *gbx)
{
	static const gbx_execdef_t s_link_tests[] = {
		{ test_link_exists,     "link-exists",  GBX_POSIX },
		{ test_link_noent,      "link-noent",   GBX_POSIX },
		{ test_link_notdir,     "link-notdir",  GBX_POSIX },
		{ test_link_rename,     "link-rename",  GBX_POSIX },
		{ test_link_max,        "link-max",     GBX_CUSTOM },
		{ test_link_limit,      "link-limit",   GBX_CUSTOM },
		{ NULL, NULL, 0 }
	};

	gbx_execute(gbx, s_link_tests);
}


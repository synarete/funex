/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_COMMON_H_
#define FUNEX_COMMON_H_

#include "fnxdefs.h"
#include "fnxnames.h"
#include "fnxuser.h"
#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxfusei.h"
#include "fnxserv.h"

#include "common/version.h"
#include "common/getopts.h"
#include "common/parse.h"
#include "common/system.h"
#include "common/globals.h"
#include "common/logging.h"
#include "common/daemon.h"
#include "common/cmdline.h"
#include "common/singleton.h"
#include "common/helpers.h"

#endif /* FUNEX_COMMON_H_ */


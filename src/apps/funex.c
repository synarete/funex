/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>

#include "funex-common.h"
#include "subcmd/commands.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Locals forward declarations: */
static void funex_parse_args(int argc, char *argv[]);
static void funex_set_limits(void);
static void funex_cleanup(void);
static void funex_execute(void);
static void funex_set_panic_hook(void);

/* Local variables: */
static const funex_cmdent_t *funex_cmdent;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                         The Funex File-System                             *
 *                                                                           *
 *                       Command-Line Front-End Tool                         *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int main(int argc, char *argv[])
{
	/* Begin with defaults */
	funex_init_defaults(argc, argv);

	/* Bind panic callback hook */
	funex_set_panic_hook();

	/* Command name & global opts */
	funex_parse_args(argc, argv);

	/* Override settings by config-file */
	funex_globals_byconf();

	/* Have some limits */
	funex_set_limits();

	/* Enable logging (default-mode) */
	funex_setup_logger();

	/* Execute sub-command */
	funex_execute();

	/* Final cleanups */
	funex_cleanup();

	/* Goodbye :) */
	funex_goodbye();

	return EXIT_SUCCESS;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Resolve sub-command */
static void funex_parse_args(int argc, char *argv[])
{
	const char *cmdname;
	const funex_cmdent_t *cmdent;

	if (argc <= 1) {
		funex_dief("no-args (try '--help')");
	}

	cmdname = argv[1];
	cmdent  = funex_lookup_cmdent(cmdname);
	if (cmdent == NULL) {
		funex_dief("unknown-command: %s", cmdname);
	}

	funex_cmdent = cmdent;
	funex_globals.subc.argc = argc - 1;
	funex_globals.subc.argv = argv + 1;
	funex_globals.subc.name = cmdent->name;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Process limits */
static void funex_set_limits(void)
{
	/* TODO: core, max-files etc. */
}

/* Cleanup & fatal-error handling */
static void funex_cleanup(void)
{
	/* FIXME */
	fflush(stdout);

	funex_globals_cleanup();
}

static void funex_set_panic_hook(void)
{
	fx_set_panic_callback(funex_fatal_error);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Sub-command execution */
static void funex_execute(void)
{
	int sargc    = funex_globals.subc.argc;
	char **sargv = funex_globals.subc.argv;
	const funex_cmdent_t *cmdent = funex_cmdent;

	if (sargc && cmdent && cmdent->gopt && cmdent->exec) {
		/* Arguments parsing */
		funex_parse_cmdargs(sargc, sargv, cmdent, cmdent->gopt);

		/* Re-setup printer-logger using parsed command-line args */
		funex_setup_plogger();

		/* Execute sub-command via hook */
		cmdent->exec();
	}
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <locale.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "funex-common.h"

#define globals (funex_globals)

/* Local funections */
static void funex_auxd_parse_args(int argc, char *argv[]);
static void funex_auxd_set_panic_hook(void);
static void funex_auxd_chkconfig(void);
static void funex_auxd_set_rlimits(void);
static void funex_auxd_preinit(void);
static void funex_auxd_bootstrap(void);
static void funex_auxd_execute(void);
static void funex_auxd_shutdown(void);
static void funex_auxd_enable_sighalt(void);

/* Local variables */
static int funex_auxd_started = 0;
static int funex_auxd_halted  = 0;
static int funex_auxd_signum  = 0;
static siginfo_t funex_auxd_siginfo;



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                         The Funex File-System                             *
 *                                                                           *
 *                        Auxiliary (Mount) Daemon                           *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int main(int argc, char *argv[])
{
	/* Begin with defaults */
	funex_init_defaults(argc, argv);

	/* Bind panic callback hook */
	funex_auxd_set_panic_hook();

	/* Parse command-line args */
	funex_auxd_parse_args(argc, argv);

	/* Override settings by conf-file */
	funex_globals_byconf();

	/* Check settings validity */
	funex_auxd_chkconfig();

	/* Become a daemon process */
	funex_daemonize();

	/* Enable logging facility for daemon */
	funex_setup_dlogger();

	/* Set process limits */
	funex_auxd_set_rlimits();

	/* Allow signals */
	funex_auxd_enable_sighalt();

	/* Create singleton instance */
	funex_new_mntsrv();

	/* Few extras before actual init */
	funex_auxd_preinit();

	/* Initialize mounting-daemon */
	funex_auxd_bootstrap();

	/* Run as long as active */
	funex_auxd_execute();

	/* Do orderly shutdown */
	funex_auxd_shutdown();

	/* Delete singleton instance */
	funex_del_mntsrv();

	/* Go home... */
	return EXIT_SUCCESS;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_auxd_set_panic_hook(void)
{
	fx_set_panic_callback(funex_fatal_error);
}

static void funex_auxd_chkconfig(void)
{
	int rc, cap_mount;
	mode_t mode;
	const char *usock = globals.auxd.usock;

	cap_mount = funex_capef_sys_admin();
	if (!cap_mount) {
		fx_error("Not CAP_SYS_ADMIN uid=%d", geteuid());
	}

	/* TODO: Move eleswhere */
	if (fnx_stat(usock, NULL) == 0) {
		fx_info("unlink-prev-sock %s", usock);
		rc = unlink(usock); /* XXX unlink only if dead */
		if (rc != 0) {
			funex_dief("exists-and-unlinkable: %s", usock);
		}

		mode = S_IFSOCK | S_IRWXU | S_IRWXG | S_IRWXO;
		rc = creat(usock, mode);
		if (rc != 0) {
			funex_dief("no-create: %s mode=%o errno=%d", usock, mode, errno);
		}
	}
}

static void funex_auxd_set_rlimits(void)
{
	funex_setrlimit_core(globals.proc.core);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Run as long as active. Do not block signals for main-thread; it is the only
 * thread which may execute signal-handlers.
 */
static void funex_auxd_start(void)
{
	int rc;
	const char  *usock  = globals.auxd.usock;
	fx_mntsrv_t *mntsrv = funex_mntsrv;

	rc = fx_mntsrv_start(mntsrv);
	if (rc != 0) {
		fx_panic("failed-start-mntsrv: sock=%s", usock);
	}
	funex_auxd_started = 1;
}

static void funex_auxd_waitactive(void)
{
	const int *signaled = &funex_auxd_signum;
	const fx_mntsrv_t *mntsrv = funex_mntsrv;

	while ((*signaled == 0) && (mntsrv->active)) {
		sleep(1); /* FIXME */
	}
}

static void funex_auxd_execute(void)
{
	funex_auxd_start();

	funex_sigunblock();

	funex_auxd_waitactive();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_auxd_preinit(void)
{
	/*
	 * fuse/lib/helper.c requires that we "make sure file descriptors 0, 1 and
	 * 2 are open, otherwise chaos would ensue".
	 */
	int fd;
	do {
		fd = open("/dev/null", O_RDWR);
		if (fd > 2) {
			close(fd);
		}
	} while (fd >= 0 && fd <= 2);
}

static void funex_auxd_bootstrap(void)
{
	int rc;
	fx_mntsrv_t *mntsrv;
	char const  *usock = globals.auxd.usock;

	mntsrv = funex_mntsrv;
	fx_info("new-mntsrv instance=%p", (void *)mntsrv);

	rc = fx_mntsrv_open(mntsrv, usock);
	if (rc != 0) {
		fx_panic("no-mntsrv-open usock=%s", usock);
	}
	fx_info("mount-server-opened usock=%s", usock);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_auxd_shutdown(void)
{
	const char *usock   = globals.auxd.usock;
	const char *version = funex_version();

	funex_auxd_halted = 1;
	funex_handle_lastsig(funex_auxd_signum, &funex_auxd_siginfo);

	fx_mntsrv_stop(funex_mntsrv);
	fx_info("mntsrv-stoped: usock=%s", usock);

	fx_mntsrv_close(funex_mntsrv);
	fx_info("mntsrv-closed: usock=%s", usock);

	fx_info("mntsrv-shutdown: usock=%s version=%s", usock, version);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
funex_auxd_sighalt_callback(int signum, const siginfo_t *si, void *p)
{
	siginfo_t *fsi = &funex_auxd_siginfo;

	/* Copy signal info into local variables */
	memcpy(fsi, si, sizeof(*fsi));
	funex_auxd_signum = signum;

	/* Try to wait for init-completion */
	if (!funex_auxd_started) {
		fx_msleep(1);
	}

	/* Try to let main-thread wake-up and halt */
	if (!funex_auxd_halted) {
		fx_msleep(1);
	}

	/* In all cases, don't got into 100% CPU if multi-signals */
	fx_msleep(10);
	fx_unused(p);
	return 0;
}

static void funex_auxd_enable_sighalt(void)
{
	funex_register_sigactions();
	funex_register_sighalt(funex_auxd_sighalt_callback);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_auxd_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 's':
				arg = funex_getoptarg(opt, "usock");
				funex_clone_str(&globals.auxd.usock, arg);
				break;
			case 'f':
				arg = funex_getoptarg(opt, "conf");
				funex_globals_set_conf(arg);
				break;
			case 'Z':
				globals.proc.nodaemon = FNX_TRUE;
				break;
			case 'L':
				funex_show_license_and_goodbye();
				break;
			case 'v':
				funex_show_version_and_goodbye();
				break;
			case 'd':
				arg = funex_getoptarg(opt, "debug");
				funex_globals_set_debug(arg);
				break;
			case 'h':
			default:
				funex_show_cmdhelp_and_goodbye2(opt);
				break;
		}
	}
}

static const funex_cmdent_t funex_auxd_cmdent = {
	.gopt       = funex_auxd_getopts,
	.name       = "auxd",
	.usage      = "% [--conf=<file>] [-s|--usock=<path>] ",
	.optdef     = \
	"h,help d,debug= v,version L,license Z,nodaemon f,config= s,usock= ",
	.desc       = "Execute mount-server daemon",
	.optdesc    = \
	"-f, --conf=<file>          Use configuration-file \n"\
	"-s, --usock=<path>         UNIX-domain socket for IPC \n"\
	"-Z, --nodaemon             Do not ran as daemon process \n"\
	"-v, --version              Show version info \n" \
	"-d, --debug=<level>        Debug mode level \n"\
	"-h, --help                 Show this help \n"
};

static void funex_auxd_parse_args(int argc, char *argv[])
{
	funex_parse_cmdargs(argc, argv, &funex_auxd_cmdent, funex_auxd_getopts);
}

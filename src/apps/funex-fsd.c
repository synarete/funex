/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <unistd.h>

#include "funex-common.h"

#define globals (funex_globals)

/* Local funections */
static void funex_fsd_parse_args(int, char *[]);
static void funex_fsd_set_panic_hook(void);
static void funex_fsd_chkconfig(void);
static void funex_fsd_set_rlimits(void);
static void funex_fsd_execute(void);
static void funex_fsd_bootstrap(void);
static void funex_fsd_shutdown(void);
static void funex_fsd_enable_sighalt(void);

/* Local variables */
static int funex_fsd_started = 0;
static int funex_fsd_halted  = 0;
static int funex_fsd_signum  = 0;
static int funex_fsd_sigcnt  = 0;
static siginfo_t funex_fsd_siginfo;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                          The Funex File-System                            *
 *                                                                           *
 *                        File-System Daemon-Server                          *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

int main(int argc, char *argv[])
{
	/* Begin with defaults */
	funex_init_defaults(argc, argv);

	/* Bind panic callback hook */
	funex_fsd_set_panic_hook();

	/* Parse command-line args */
	funex_fsd_parse_args(argc, argv);

	/* Override settings by conf-file */
	funex_globals_byconf();

	/* Check validity of config parameters */
	funex_fsd_chkconfig();

	/* Become a daemon process */
	funex_daemonize();

	/* Enable logging facility for daemon */
	funex_setup_dlogger();

	/* Set process limits */
	funex_fsd_set_rlimits();

	/* Allow signals */
	funex_fsd_enable_sighalt();

	/* Create singleton instance */
	funex_new_server();

	/* Initialize server's instance */
	funex_fsd_bootstrap();

	/* Run as long as active */
	funex_fsd_execute();

	/* Do orderly shutdown */
	funex_fsd_shutdown();

	/* Delete singleton instance */
	funex_del_server();

	/* Go home, be happy... */
	return EXIT_SUCCESS;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_fsd_set_panic_hook(void)
{
	fx_set_panic_callback(funex_fatal_error);
}

static void funex_fsd_chkconfig(void)
{
	int rc;
	const char *path;
	const mode_t mask = R_OK | W_OK;

	path = globals.fs.mntpoint;
	if (fx_check_mntpoint(path) != 0) {
		funex_dief("non-valid-mountpoint: %s", path);
	}
	path = globals.vol.path;
	if ((rc = fnx_statvol(path, NULL, mask)) != 0) {
		funex_dief("illegal-vol: %s err=%d", path, -rc);
	}
	if (globals.fs.mntf == 0) {
		funex_dief("illegal-mntf: %#lx", globals.fs.mntf);
	}
}

static void funex_fsd_set_rlimits(void)
{
	funex_setrlimit_core((size_t)globals.proc.core);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Run as long as active. Do not block signals for main-thread; it is the only
 * thread which may execute signal-handlers.
 */
static void funex_fsd_start(void)
{
	int rc;
	const char *mntpoint = globals.fs.mntpoint;
	fx_server_t *server = funex_server;

	rc = fx_server_start(server);
	if (rc != 0) {
		fx_panic("server-start-failed: mntpoint=%s", mntpoint);
	}
	fx_info("server-started mntpoint=%s", mntpoint);
	funex_fsd_started = 1;
}

static void funex_fsd_waitactive(void)
{
	const int *signaled  = &funex_fsd_signum;
	const fx_server_t *server  = funex_server;

	while ((*signaled == 0) && (server->active > 0)) {
		sleep(1);
	}
}

static void funex_fsd_execute(void)
{
	funex_fsd_start();

	funex_sigunblock();

	funex_fsd_waitactive();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_fsd_userctx(fx_uctx_t *uctx)
{
	memset(uctx, 0, sizeof(*uctx));
	uctx->u_root = 1;
	uctx->u_capf = FNX_CAPF_ALL;
	uctx->u_cred.cr_uid     = globals.uid;
	uctx->u_cred.cr_gid     = globals.gid;
	uctx->u_cred.cr_umask   = globals.umsk;
	uctx->u_cred.cr_ngids   = 0;
}

static void funex_fsd_bootstrap(void)
{
	int rc;
	fx_uctx_t uctx;
	fx_server_t *server;
	const fx_mntf_t mntf    = globals.fs.mntf;
	char const *mntpoint    = globals.fs.mntpoint;
	const char *volume      = globals.vol.path;
	const char *mntsock     = globals.auxd.usock;

	server = funex_server;
	fx_info("new-server: instance=%p mntf=%#lx", (void *)server, mntf);

	funex_fsd_userctx(&uctx);
	rc = fx_server_setup(server, &uctx, mntsock, mntf);
	if (rc != 0) {
		fx_panic("invalid-fs-args: mntpoint=%s", mntpoint);
	}
	rc = fx_server_open(server, volume);
	if (rc != 0) {
		fx_panic("no-server-open: volume=%s", volume);
	}
	rc = fx_server_mount(server, mntpoint);
	if (rc != 0) {
		fx_panic("no-mount: usock=%s mntpoint=%s", mntsock, mntpoint);
	}
	fx_info("server-opened: volume=%s mntpoint=%s", volume, mntpoint);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_fsd_shutdown(void)
{
	const char *mntpoint = globals.fs.mntpoint;
	const char *version  = funex_version();

	funex_fsd_halted = 1;
	funex_handle_lastsig(funex_fsd_signum, &funex_fsd_siginfo);

	fx_server_stop(funex_server);
	fx_info("server-stoped: mntpoint=%s", mntpoint);

	fx_server_cleanup(funex_server);
	fx_server_close(funex_server);
	fx_info("server-closed: mntpoint=%s", mntpoint);

	fx_info("server-shutdown: mntpoint=%s version=%s", mntpoint, version);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
funex_fsd_sighalt_callback(int signum, const siginfo_t *si, void *p)
{
	siginfo_t *fsi = &funex_fsd_siginfo;

	/* Only the first callback is relevant */
	funex_fsd_sigcnt += 1;
	if (funex_fsd_signum != 0) {
		return (funex_fsd_sigcnt < 100) ? 0 : 1; /* 1 = reraise */
	}

	/* Copy signal info into local variables */
	memcpy(fsi, si, sizeof(*fsi));
	funex_fsd_signum = signum;

	/* Try to wait for init-completion */
	if (!funex_fsd_started) {
		fx_msleep(1);
	}

	/* Try to let main-thread wake-up and halt */
	if (!funex_fsd_halted) {
		fx_msleep(1);
	}

	/* In all cases, don't got into 100% CPU if multi-signals */
	fx_msleep(10);
	fx_unused(p);
	return 0; /* 0 = No-reraise */
}

static void funex_fsd_enable_sighalt(void)
{
	funex_register_sigactions();
	funex_register_sighalt(funex_fsd_sighalt_callback);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void funex_fsd_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'o':
				arg = funex_getoptarg(opt, "o");
				funex_globals_set_mntf(arg);
				break;
			case 'R':
				globals.fs.mntf |= FNX_MNTF_RDONLY;
				break;
			case 'S':
				globals.fs.mntf |= FNX_MNTF_STRICT;
				break;
			case 'f':
				arg = funex_getoptarg(opt, "conf");
				funex_globals_set_conf(arg);
				break;
			case 'Z':
				globals.proc.nodaemon = FNX_TRUE;
				break;
			case 'c':
				arg = funex_getoptarg(opt, "core");
				funex_globals_set_core(arg);
				break;
			case 'L':
				funex_show_license_and_goodbye();
				break;
			case 'v':
				funex_show_version_and_goodbye();
				break;
			case 'd':
				arg = funex_getoptarg(opt, "debug");
				funex_globals_set_debug(arg);
				break;
			case 'h':
			default:
				funex_show_cmdhelp_and_goodbye2(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "volume", FUNEX_ARG_REQ);
	funex_globals_set_volpath(arg);
	arg  = funex_getnextarg(opt, "mntpoint", FUNEX_ARG_REQ | FUNEX_ARG_LAST);
	funex_globals_set_mntpoint(arg);
}

static const funex_cmdent_t funex_fsd_cmdent = {
	.gopt       = funex_fsd_getopts,
	.name       = "fsd",
	.usage      = \
	"% [--conf=<file>] [--rdonly] [-o opts] <volume> <mntpoint> ",
	.optdef     = \
	"h,help d,debug= v,version L,license f,conf= R,rdonly Z,nodaemon " \
	"o,options= S,strict c,core",
	.desc       = "Execute file-system daemon",
	.optdesc    = \
	"-f, --conf=<file>          Use configuration-file \n"\
	"-R, --rdonly               Read-only mode \n"\
	"-o, --option=<mntf>        Comma separated mount options \n"\
	"-Z, --nodaemon             Do not ran as daemon process \n"\
	"-S, --strict               Non-permissive mode for inode attributes \n"\
	"-c, --core=<size>          Core-dump file-size limit (0=no-core) \n" \
	"-d, --debug=<level>        Debug mode level \n"\
	"-L, --license              Show license info \n"\
	"-v, --version              Show version info \n"\
	"-h, --help                 Show this help \n"
};

static void funex_fsd_parse_args(int argc, char *argv[])
{
	funex_parse_cmdargs(argc, argv, &funex_fsd_cmdent, funex_fsd_getopts);
}

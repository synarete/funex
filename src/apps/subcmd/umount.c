/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)


static void do_umount(const char *mntp)
{
	int rc, fd = -1;
	size_t nwr = 0;
	char *path;

	path = fnx_makepath(mntp, FNX_PSROOTNAME, FNX_PSHALTNAME, NULL);
	if (path == NULL) {
		funex_dief("no-path: mntpoint=%s", mntp);
	}

	rc = fnx_access(path, R_OK | W_OK);
	if (rc != 0) {
		funex_dief("no-access: %s err=%d", path, -rc);
	}
	rc = fnx_open(path, O_RDWR, 0, &fd);
	if (rc != 0) {
		funex_dief("no-open: %s err=%d", path, -rc);
	}
	rc = fnx_write(fd, "0", 1, &nwr);
	if (rc != 0) {
		fnx_close(fd);
		funex_dief("no-write: %s err=%d", path, -rc);
	}
	fnx_fsync(fd);
	fnx_close(fd);

	/* Triggers final ops */
	for (size_t i = 0; (i < 20) && (rc == 0); i++) {
		rc = fnx_access(path, F_OK);
		sleep(1);
	}

	fnx_freepath(path);
}

static void prepare(void)
{
	int rc;
	const char *base;

	base = globals.fs.mntpoint;
	if (base == NULL) {
		base = globals.proc.cwd;
	}
	funex_globals_set_head(base);
	if ((rc = fnx_stat(globals.head, NULL)) != 0) {
		funex_dief("no-stat: %s", globals.head);
	}
}

static void execute(void)
{
	do_umount(globals.head);
}

static void umount_execute(void)
{
	size_t i, nent = 0;
	fnx_mntent_t mnts[128];

	if (!globals.opt.all) {
		prepare();
		execute();
	} else {
		fnx_funexmnts(mnts, FX_NELEMS(mnts), &nent);
		for (i = 0; i < nent; ++i) {
			do_umount(mnts[i].target);
		}
		fnx_freemnts(mnts, nent);
	}
}

static void umount_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg = NULL;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'a':
				globals.opt.all = FNX_TRUE;
				break;
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "mntpoint", FUNEX_ARG_LAST);
	if (arg != NULL) {
		funex_globals_set_mntpoint(arg);
	} else if (!globals.opt.all) {
		funex_die_missing_arg("mntpoint");
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_umount = {
	.name       = "umount",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = umount_getopts,
	.exec       = umount_execute,
	.usage      = "@ <mntpoint> ",
	.desc       = "Detach from file-system hierarchy \n",
	.optdef     = "h,help d,debug= a,all",
	.optdesc    = \
	"-a, --all              Detach all currently-mounted Funex filesystems \n"
	"-h, --help             Show this help \n"

};



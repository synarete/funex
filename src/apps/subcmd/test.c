/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)


static void test_execute(void)
{
	funex_execsub(globals.prog.argc, globals.prog.argv, "gbx");
}

static void test_getopts(funex_getopts_t *opt)
{
	fx_unused(opt);
}

const funex_cmdent_t funex_cmdent_test = {
	.name       = "test",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = test_getopts,
	.exec       = test_execute,
	.usage      = "@ [<testdir>]",
	.desc       = \
	"Run smoke-tests utility to check correctness of a mounted file-system",
	.optdef     = \
	"h,help d,debug= v,version L,license a,all x,main "\
	"i,rw p,posix S,stress C,custom",
};


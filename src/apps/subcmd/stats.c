/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void prepare(void)
{
	int rc;
	char *root;
	const char *base = globals.path ? globals.path : globals.proc.cwd;

	funex_globals_set_head(base);
	if ((rc = fnx_access(globals.head, R_OK)) != 0) {
		funex_dief("no-access: %s err=%d", globals.head, -rc);
	}
	root = fnx_joinpath(globals.head, FNX_PSROOTNAME);
	if ((rc = fnx_access(root, R_OK)) != 0) {
		funex_dief("no-access: %s err=%d", root, -rc);
	}
	fnx_freepath(root);
}

static void execute(void)
{
	const char *head    = globals.head;
	const char *proot   = FNX_PSROOTNAME;
	const char *psuper  = FNX_PSSUPERDNAME;
	const char *pcache  = FNX_PSCACHEDNAME;

	if (globals.opt.all) {
		funex_show_pseudo(head, proot, psuper, FNX_PSVSTATSNAME, 1);
		funex_show_pseudo(head, proot, psuper, FNX_PSOPSTATSNAME, 1);
		funex_show_pseudo(head, proot, psuper, FNX_PSIOSTATSNAME, 1);
		funex_show_pseudo(head, proot, psuper, FNX_PSLAYOUTNAME, 1);
		funex_show_pseudo(head, proot, pcache, FNX_PSCSTATSNAME, 1);
		funex_show_pseudo(head, proot, pcache, FNX_PSCLIMITSSNAME, 1);
	} else if (globals.opt.meta) {
		funex_show_pseudo(head, proot, psuper, FNX_PSVSTATSNAME, 0);
	} else if (globals.opt.opc) {
		funex_show_pseudo(head, proot, psuper, FNX_PSOPSTATSNAME, 0);
	} else if (globals.opt.ioc) {
		funex_show_pseudo(head, proot, psuper, FNX_PSIOSTATSNAME, 0);
	} else if (globals.opt.lay) {
		funex_show_pseudo(head, proot, psuper, FNX_PSLAYOUTNAME, 0);
	} else if (globals.opt.cst) {
		funex_show_pseudo(head, proot, pcache, FNX_PSCSTATSNAME, 0);
		funex_show_pseudo(head, proot, pcache, FNX_PSCLIMITSSNAME, 0);
	}
}

static void stats_execute(void)
{
	prepare();
	execute();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void stats_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg = NULL;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'm':
				globals.opt.meta = FNX_TRUE;
				break;
			case 'o':
				globals.opt.opc = FNX_TRUE;
				break;
			case 'i':
				globals.opt.ioc = FNX_TRUE;
				break;
			case 'c':
				globals.opt.cst = FNX_TRUE;
				break;
			case 'l':
				globals.opt.lay = FNX_TRUE;
				break;
			case 'a':
				globals.opt.all = FNX_TRUE;
				break;
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "path", FUNEX_ARG_LAST);
	funex_globals_set_path(arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_stats = {
	.name       = "stats",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = stats_getopts,
	.exec       = stats_execute,
	.usage      = "@ [<opts>] [<path>]",
	.desc       = "Show file-system counters and statistics",
	.optdef     = "h,help d,debug a,all m,meta i,ioc o,opc c,cache l,layout",
	.optdesc    = \
	"-m, --meta             Show meta-data counters \n" \
	"-i, --ioc              Show I/O counters \n" \
	"-o, --opc              Show operation counters \n" \
	"-c, --cache            Show cache stats \n" \
	"-l, --layout           Show volume layout \n" \
	"-a, --all              Show all counters and stats \n" \
	"-d, --debug=<level>    Debug mode level \n" \
	"-h, --help             Show this help \n"
};

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)


static void prepare(void)
{
	int rc;
	const char *path = globals.path;

	funex_globals_set_head(path);
	rc = fnx_stat(globals.head, NULL);
	if (rc != 0) {
		funex_dief("no-stat: %s", globals.head);
	}
	rc = fnx_stat(path, NULL);
	if (rc != 0) {
		funex_dief("no-stat: %s", path);
	}
}

static void execute(void)
{
	int rc, flags, src_fd, tgt_fd;
	const char *src = globals.path;
	const char *tgt = globals.path2;

	errno = 0;
	flags = O_RDONLY | O_NOATIME;
	src_fd = open(src, flags);
	if (src_fd < 0) {
		funex_dief("no-open: %s flags=%#o errno=%d", src, flags, errno);
	}
	flags = O_RDWR | O_CREAT | O_TRUNC;
	tgt_fd = open(tgt, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
	if (tgt_fd < 0) {
		close(src_fd);
		funex_dief("no-open: %s flags=%#o errno=%d", tgt, flags, errno);
	}

	/* Mimic coreutils' clone_file */
	rc = fnx_fclone(tgt_fd, src_fd);
	close(src_fd);
	close(tgt_fd);
	if (rc != 0) {
		funex_dief("failed: src=%s tgt=%s err=%d", src, tgt, -rc);
	}
}


static void clone_execute(void)
{
	prepare();
	execute();
}

static void clone_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "source-file", FUNEX_ARG_REQ);
	funex_clone_str(&globals.path, arg);
	arg = funex_getnextarg(opt, "target-file", FUNEX_ARG_REQLAST);
	funex_clone_str(&globals.path2, arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_clone = {
	.name       = "clone",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = clone_getopts,
	.exec       = clone_execute,
	.usage      = "@ <source-file> <target-file>",
	.desc       = "Make a shallow copy of regular file",
	.optdef     = "h,help d,debug=",
	.optdesc    = \
	"-d, --debug=<level>    Debug mode level \n" \
	"-h, --help             Show this help \n"
};


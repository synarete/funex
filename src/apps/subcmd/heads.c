/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "funex-common.h"
#include "commands.h"

#define globals     (funex_globals)


static const char *err2str(int err)
{
	const char *str = "";

	if (err == ENOTCONN) {
		str = "!";
	} else if (err == EACCES) {
		str = "?";
	} else if (err != 0) {
		str = "&";
	}
	return str;
}

static void showmnt(const char *target, const char *options)
{
	int rc, err;
	dev_t dev = 0;

	errno = 0;
	if (globals.opt.all) {
		rc  = fnx_statdev(target, NULL, &dev);
		err = errno;
		if (rc == 0) {
			fx_info("%s %u.%u %s", target, major(dev), minor(dev), options);
		} else {
			fx_info("%s %s %s", target, err2str(err), options);
		}
	} else {
		fx_info("%s", target);
	}
}

static void execute(void)
{
	int rc;
	size_t i, nent = 0;
	fnx_mntent_t ments[128]; /* TODO: Define me */

	memset(ments, 0, sizeof(ments));
	rc = fnx_funexmnts(ments, FX_NELEMS(ments), &nent);
	if (rc != 0) {
		funex_dief("no-parse-mounts: err=%d", -rc);
	}
	for (i = 0; i < nent; ++i) {
		showmnt(ments[i].target, ments[i].options);
	}
	fnx_freemnts(ments, nent);
}

static void heads_execute(void)
{
	execute();
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void heads_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg = NULL;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'a':
				globals.opt.all = FNX_TRUE;
				break;
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}

	arg = funex_getnextarg(opt, "path", FUNEX_ARG_LAST);
	funex_globals_set_path(arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_heads = {
	.name       = "heads",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = heads_getopts,
	.exec       = heads_execute,
	.usage      = "@ [<path>]",
	.desc       = "Show mount-point information",
	.optdef     = "h,help d,debug a,all",
	.optdesc    = \
	"-a, --all                  Show detailed info \n" \
	"-d, --debug=<level>        Debug mode level \n" \
	"-h, --help                 Show this help \n"
};


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libmount/libmount.h>

#include "funex-common.h"
#include "commands.h"

static void quota_execute(void)
{
	funex_die_unimplemented("quota");
}

static void quota_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg = NULL;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "mntpoint", FUNEX_ARG_REQLAST);
	funex_globals_set_mntpoint(arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_quota = {
	.name       = "quota",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = quota_getopts,
	.exec       = quota_execute,
	.usage      = "@ <mount-point> ",
	.desc       = "Quota operations",
	.optdef     = "h,help d,debug=",
	.optdesc    = \
	"-h, --help             Show this help \n"
};



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)

#define print_sizeof(t)     fx_info("sizeof(%s) = %zu", #t, sizeof(t))
#define print_strdef(s)     fx_info("%s = %s", #s, s)
#define print_constdef(d)   fx_info("%s = %ld", #d, (long)(d))
#define print_delim(s)      fx_info("%s", s)

#define print_globalstr(k)  \
	if (globals.k != NULL) \
	{ fx_info("%s = %s", FX_MAKESTR(k), globals.k); }

static void show_defs(void)
{
	/* Global defs */
	print_strdef(FNX_FSNAME);
	print_constdef(FNX_FSMAGIC);
	print_constdef(FNX_FSVERSION);
	print_constdef(FNX_FRGSIZE);
	print_constdef(FNX_BLKSIZE);
	print_constdef(FNX_SECSIZE);
	print_constdef(FNX_SLICESIZE);
	print_constdef(FNX_NAME_MAX);
	print_constdef(FNX_PATH_MAX);
	print_constdef(FNX_LINK_MAX);
	print_delim("");

	/* Directory defs */
	print_constdef(FNX_DOFF_NONE);
	print_constdef(FNX_DOFF_SELF);
	print_constdef(FNX_DOFF_PARENT);
	print_constdef(FNX_DIRTOP_NDENT);
	print_constdef(FNX_DIREXT_NDENT);
	print_constdef(FNX_DIREXT_NSEGS);
	print_constdef(FNX_DIRSEG_NDENT);
	print_constdef(FNX_DOFF_END);
	print_delim("");

	/* Regular-file defs */
	print_constdef(FNX_NSLICES_MAX);
	print_constdef(FNX_ROFF_BEGIN);
	print_constdef(FNX_ROFF_END);
	print_delim("");
}

static void show_sizeofs(void)
{
	/* Objects sizeof */
	print_sizeof(fx_bkref_t);
	print_sizeof(fx_iattr_t);
	print_sizeof(fx_fsattr_t);
	print_sizeof(fx_fsstat_t);
	print_sizeof(fx_vstats_t);
	print_sizeof(fx_iostat_t);
	print_sizeof(fx_opstat_t);
	print_sizeof(fx_fsinfo_t);
	print_sizeof(fx_super_t);
	print_sizeof(fx_spmap_t);
	print_sizeof(fx_inode_t);
	print_sizeof(fx_dir_t);
	print_sizeof(fx_dirext_t);
	print_sizeof(fx_dirseg_t);
	print_sizeof(fx_symlnk_t);
	print_sizeof(fx_slice_t);
	print_sizeof(fx_segmnt_t);
	print_sizeof(fx_segmap_t);
	print_sizeof(fx_task_t);
	print_sizeof(fx_cache_t);
	print_sizeof(fx_fusei_t);
	print_sizeof(fx_server_t);
	print_sizeof(funex_globals_t);
	print_delim("");
}

static void show_globals(void)
{
	print_globalstr(auxd.usock);
	print_globalstr(proc.cwd);
	print_globalstr(sys.username);
	print_globalstr(sys.selfexe);
	print_globalstr(sys.confdir);
	print_delim("");
}


static void debug_execute(void)
{
	show_defs();
	show_sizeofs();
	show_globals();
}

static void goptnone(funex_getopts_t *opt)
{
	fx_unused(opt);
}

const funex_cmdent_t funex_cmdent_debug = {
	.name       = "+debug",
	.flags      = FUNEX_CMD_PRIVATE,
	.exec       = debug_execute,
	.gopt       = goptnone,
	.usage      = "@ ",
	.desc       = "Show internal constants and sys-config params"
};

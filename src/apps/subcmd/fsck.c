/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "funex-common.h"
#include "funex-fsck.h"
#include "commands.h"

#define globals (funex_globals)

static void fsck_execute(void)
{
	int rc;

	rc = funex_fsck_check(globals.vol.path);
	if (rc != 0) {
		funex_exiterr();
	}
}

static void fsck_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'r':
				globals.opt.repair = FNX_TRUE;
				funex_die_unimplemented("fsck-repair");
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "volume", FUNEX_ARG_REQ);
	funex_clone_str(&globals.vol.path, arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_fsck = {
	.name       = "fsck",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = fsck_getopts,
	.exec       = fsck_execute,
	.usage      = "@ <volume>",
	.desc       = "Run file-system check & repair",
	.optdef     = "h,help d,debug= r,repair",
	.optdesc    = \
	"-r, --repair           Try to repair defects  \n" \
	"-d, --debug=<level>    Debug mode level \n" \
	"-h, --help             Show this help \n"
};


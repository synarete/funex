/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include "funex-common.h"
#include "commands.h"

#define globals     (funex_globals)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void prepare(void)
{
	int rc;
	off_t sz = 0;

	globals.fs.uid = getuid();
	globals.fs.gid = getgid();
	fx_times_fill(&globals.fs.tms, FNX_BAMCTIME_NOW, NULL);
	if (fx_uuid_isnull(&globals.fs.uuid)) {
		fx_uuid_generate(&globals.fs.uuid);
	}
	if (globals.vol.bkcnt == 0) {
		rc = fnx_statsz(globals.vol.path, NULL, &sz);
		if (rc != 0) {
			funex_dief("can-not-stat-size: path=%s", globals.vol.path);
		}
		globals.vol.bkcnt = (blkcnt_t)fx_volsz_to_bkcnt(sz);
	}
}

static void chkconf(void)
{
	int rc;
	const fx_bkcnt_t nbk = (fx_bkcnt_t)globals.vol.bkcnt;

	if (!nbk && ((rc = fnx_statblk(globals.vol.path, NULL)) != 0)) {
		funex_dief("missing vol-size: %s", globals.vol.path);
	}
	if ((nbk < FNX_VOLNBK_MIN) || (FNX_VOLNBK_MAX < nbk)) {
		funex_die_illegal_volsize(NULL, (loff_t)nbk * FNX_BLKSIZE);
	}
}

/*
 * Protect user from overriding existing volume, unless explicitly required
 * to do so with '--force' or fs-volume does-not exist.
 */
static void protect(void)
{
	int rc, has_super;
	fx_vio_t *vio;
	const fx_dsec_t *dsec;
	const fx_dfrg_t *dfrg;
	const fx_vinfo_t *vinfo;
	struct stat st;
	const char *path = globals.vol.path;

	if (globals.opt.force) {
		return;
	}
	if ((rc = fnx_stat(path, &st)) != 0) {
		return;
	}
	if (S_ISREG(st.st_mode) && (st.st_size < FNX_SECSIZE)) {
		return;
	}

	vio  = funex_open_vio(path, FX_VIOF_RDONLY);
	dsec = funex_get_dsec(vio, FNX_SEC_SUPER);
	dfrg = &dsec->sec_frgs[FNX_FRG_SUPER];

	vinfo = fx_get_vinfo(FNX_VTYPE_SUPER);
	rc = vinfo->dcheck(&dfrg->fr_hdr);
	has_super = (rc == 0);
	funex_close_vio(vio);

	if (has_super) {
		funex_dief("operation may destroy existing data: %s", path);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void fill_super(fx_super_t *super)
{
	fx_layout_t *layout = &super->su_layout;
	fx_fsattr_t *fsattr = &super->su_fsattr;
	fx_fsstat_t *fsstat = &super->su_fsstat;
	fx_vstats_t  *vstat  = &super->su_vstats;
	const fx_bkcnt_t bkcnt = (fx_bkcnt_t)globals.vol.bkcnt;

	fx_super_setup(super, globals.fs.name, NULL /* FIXME */);
	fx_super_settimes(super, &globals.fs.tms, FNX_BAMCTIME);
	fx_layout_devise(layout, bkcnt);
	fx_fsattr_setup(fsattr, &globals.fs.uuid, globals.fs.uid, globals.fs.gid);
	fx_fsstat_devise(fsstat, layout->l_index.cnt, layout->l_udata.cnt);
	fx_uuid_copy(&super->su_uuid, &globals.fs.uuid);
	fsattr->f_gen   = 1;
	vstat->v_super  = 1;
	vstat->v_dir    = 1;
	vstat->v_spmap  = layout->l_udata.cnt / FNX_SPCNBK;
}

static void fill_rootd(fx_dir_t *rootd)
{
	fx_uctx_t uctx;
	fx_iattr_t *iattr = &rootd->d_inode.i_iattr;

	memset(&uctx, 0, sizeof(uctx));
	uctx.u_root = 1;
	uctx.u_capf = FNX_CAPF_ALL;
	uctx.u_cred.cr_uid   = globals.uid;
	uctx.u_cred.cr_gid   = globals.gid;
	uctx.u_cred.cr_umask = globals.umsk;
	uctx.u_cred.cr_ngids = 0;

	fx_inode_setino(&rootd->d_inode, FNX_INO_ROOT);
	fx_dir_setup(rootd, &uctx, 0755);
	inode_setf(&rootd->d_inode, FNX_INODEF_ROOT);

	iattr->i_uid = globals.fs.uid;
	iattr->i_gid = globals.fs.gid;
	fx_times_copy(&iattr->i_times, &globals.fs.tms);
}

static void reset_supers(fx_vio_t *vio)
{
	fx_dsec_t  *dsec;

	dsec = funex_get_dsec(vio, FNX_SEC_SUPER);
	memset(dsec, 0, sizeof(*dsec));
}

static void sign_and_put_dsec(fx_vio_t *vio, fx_dsec_t *dsec, int now)
{
	fx_dsec_signsum(dsec);
	funex_put_dsec(vio, dsec, now);
}

static void write_super(fx_vio_t *vio, const fx_super_t *super)
{
	fx_dsec_t *dsec;
	fx_dfrg_t *dfrg;

	dsec = funex_get_dsec(vio, FNX_SEC_SUPER);
	dfrg = &dsec->sec_frgs[FNX_FRG_SUPER];
	funex_dexport(&super->su_vnode, &dfrg->fr_hdr);

	sign_and_put_dsec(vio, dsec, 1);
}

static void write_rootd(fx_vio_t *vio, const fx_dir_t *rootd)
{
	fx_dsec_t *dsec;
	fx_dfrg_t *dfrg;

	dsec = funex_get_dsec(vio, FNX_SEC_SUPER);
	dfrg = &dsec->sec_frgs[FNX_FRG_ROOT];
	funex_dexport(&rootd->d_inode.i_vnode, &dfrg->fr_hdr);

	sign_and_put_dsec(vio, dsec, 1);
}

static void reset_index(fx_vio_t *vio, const fx_super_t *super)
{
	fx_lba_t  lba;
	fx_size_t sec;
	fx_bkcnt_t bkcnt;
	fx_dsec_t *dsec;
	const fx_layout_t *layout;

	dsec = funex_new_dsec();
	layout = &super->su_layout;
	bkcnt  = layout->l_index.cnt;
	for (fx_bkcnt_t i = 0; i < bkcnt; i += FNX_SECNBK) {
		lba  = layout->l_index.lba + i;
		sec  = lba_to_sec(lba);
		fx_dsec_reset(dsec);
		fx_dsec_signsum(dsec);
		funex_write_dsec(vio, sec, dsec);
	}
	funex_del_dsec(dsec);
}

static void
format_spmaps(fx_vio_t *vio, const fx_super_t *super, fx_spmap_t *spmap)
{
	fx_lba_t ulba;
	fx_size_t sec_idx;
	fx_dsec_t *dsec;
	fx_dfrg_t *dfrg;
	fx_vaddr_t vaddr;
	fx_bkaddr_t bkaddr;
	const fx_layout_t *layout;
	const fx_extent_t *vindx;
	const fx_extent_t *udata;

	layout  = &super->su_layout;
	vindx   = &layout->l_index;
	udata   = &layout->l_udata;
	for (fx_bkcnt_t i = 0, j = 0; i < udata->cnt; i += FNX_SPCNBK, j++) {
		ulba = (j * FNX_SPCNBK);
		fx_spmap_setup(spmap, ulba);

		vaddr_for_spmap(&vaddr, ulba);
		fx_vaddr_hmap(&vaddr, vindx, &bkaddr);
		sec_idx = lba_to_sec(bkaddr.lba);
		fx_assert(bkaddr.lba >= vindx->lba);
		fx_assert(bkaddr.lba < (vindx->lba + vindx->cnt));
		dsec = funex_get_dsec(vio, sec_idx);
		dfrg = fx_dsec_search_free(dsec, vaddr.vtype);
		if (dfrg == NULL) {
			funex_dief("no-free-slot: sec=%#lu", sec_idx);
		}
		fx_dsec_chopat(dsec, dfrg, vaddr.vtype);
		fx_dexport_vobj(&spmap->sp_vnode, &dfrg->fr_hdr);
		fx_dsec_signsum(dsec);
		funex_put_dsec(vio, dsec, 0);
	}
	vio->msync(vio, vindx->lba, vindx->cnt, 1);
}

static void zfill_udata(fx_vio_t *vio, const fx_super_t *super)
{
	fx_lba_t   base, lba;
	fx_bkcnt_t bkcnt;
	fx_dsec_t *dsec;
	const fx_layout_t *layout;

	dsec = funex_new_dsec();
	layout = &super->su_layout;
	bkcnt = layout->l_udata.cnt;
	base  = layout->l_udata.lba;
	for (fx_bkcnt_t i = 0; (i < bkcnt) && ((int)i >= 0); i += FNX_SECNBK) {
		lba  = base + i;
		funex_write_dsec(vio, lba_to_sec(lba), dsec);
	}
	funex_del_dsec(dsec);
}

static void rfill_dsec(fx_dsec_t *dsec)
{
	int32_t cnt, *dat;
	struct random_data rnd;
	char sbuf[1024];
	unsigned seed;

	seed = (unsigned)(time(NULL));
	initstate_r(seed, sbuf, sizeof(sbuf), &rnd);
	setstate_r(sbuf, &rnd);

	dat = (int32_t *)((void *)dsec);
	cnt = (sizeof(*dsec) / sizeof(*dat));
	while (cnt-- > 0) {
		random_r(&rnd, dat++);
	}
}

static void rfill_udata(fx_vio_t *vio, const fx_super_t *super)
{
	fx_lba_t   base, lba;
	fx_bkcnt_t bkcnt;
	fx_dsec_t *dsec;
	const fx_layout_t *layout;

	dsec = funex_new_dsec();
	rfill_dsec(dsec);

	layout = &super->su_layout;
	bkcnt = layout->l_udata.cnt;
	base  = layout->l_udata.lba;
	for (fx_bkcnt_t i = 0; (i < bkcnt) && ((int)i >= 0); i += FNX_SECNBK) {
		lba  = base + i;
		funex_write_dsec(vio, lba_to_sec(lba), dsec);
	}
	funex_del_dsec(dsec);
}

static void show_info(const fx_super_t *super)
{
	fx_info("funex-version: %s", funex_version());
	fx_info("volume-path: %s", globals.vol.path);
	fx_info("filesystem-uuid: %s", globals.fs.uuid.str);
	fx_info("filesystem-name: %s", super->su_name.str);
	fx_info("block-size: %d", (int)FNX_BLKSIZE);
	fx_info("section-size: %d", (int)FNX_SECSIZE);
	fx_info("blocks-count: %ld", globals.vol.bkcnt);
	funex_print_layout(&super->su_layout, "volume");
}

static void execute(void)
{
	fx_vio_t   *vio;
	fx_super_t *super;
	fx_dir_t   *rootd;
	fx_spmap_t *spmap;
	fx_bkcnt_t  bkcnt;

	bkcnt = (fx_bkcnt_t)globals.vol.bkcnt;
	vio   = funex_create_vio(globals.vol.path, bkcnt);
	super = fx_vnode_to_super(funex_newvobj(FNX_VTYPE_SUPER));
	rootd = fx_vnode_to_dir(funex_newvobj(FNX_VTYPE_DIR));
	spmap = fx_vnode_to_spmap(funex_newvobj(FNX_VTYPE_SPMAP));

	fill_super(super);
	fill_rootd(rootd);
	/* TODO: Fill space block */
	reset_supers(vio);
	write_super(vio, super);
	write_rootd(vio, rootd);
	reset_index(vio, super);
	format_spmaps(vio, super, spmap);
	if (globals.vol.zfill) {
		zfill_udata(vio, super);
	} else if (globals.vol.rfill) {
		rfill_udata(vio, super);
	}
	show_info(super);

	funex_delvobj(&spmap->sp_vnode);
	funex_delvobj(&rootd->d_inode.i_vnode);
	funex_delvobj(&super->su_vnode);
	funex_close_vio(vio);
}

static void mkfs_execute(void)
{
	prepare();
	chkconf();
	protect();
	execute();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void mkfs_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg = NULL;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'u':
				arg = funex_getoptarg(opt, "uuid");
				funex_globals_set_fsuuid(arg);
				break;
			case 'L':
				arg = funex_getoptarg(opt, "name");
				funex_globals_set_fsname(arg);
				break;
			case 's':
				arg = funex_getoptarg(opt, "size");
				funex_globals_set_volsize(arg);
				break;
			case 'z':
				globals.vol.zfill = FNX_TRUE;
				break;
			case 'r':
				globals.vol.rfill = FNX_TRUE;
				break;
			case 'F':
				globals.opt.force = FNX_TRUE;
				break;
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "volume", FUNEX_ARG_REQLAST);
	funex_clone_str(&globals.vol.path, arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const struct funex_cmdent funex_cmdent_mkfs = {
	.name       = "mkfs",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = mkfs_getopts,
	.exec       = mkfs_execute,
	.usage      = \
	"@ [-u FS-UUID] [-L <name>] [-s <size>] <volume>",
	.desc       = \
	"Format raw volume with Funex file-system layout. Volume-path may be \n"
	"either a block-device or regular-file.",
	.optdef     = \
	"h,help d,debug= u,uuid= L,name= s,size= z,zfill r,rfill F,force",
	.optdesc    = \
	"-u, --uuid=<UUID>      File-system's UUID \n" \
	"-L, --name=<label>     FS label-name \n" \
	"-s, --size=<bytes>     Volume's calacity in bytes \n" \
	"-z, --zfill            Fill data-blocks with zeros \n" \
	"-r, --rfill            Fill data-blocks with random \n" \
	"-F, --force            Override existing data \n" \
	"-d, --debug=<level>    Debug mode level \n"
};


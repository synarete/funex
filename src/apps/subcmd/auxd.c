/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>

#include "funex-common.h"
#include "commands.h"

#define AUXD_START  (0x01)
#define AUXD_STOP   (0x02)
#define AUXD_STATUS (0x04)


#define globals (funex_globals)

static void perpare(void)
{
	switch (globals.auxd.oper) {
		case AUXD_START:
		case AUXD_STOP:
		case AUXD_STATUS:
			break;
		default:
			funex_die_missing_arg("--start|--stop|--status");
			break;
	}
}

static void execute_start(void)
{
	funex_die_unimplemented("auxd --start");
}

static void execute_stop(void)
{
	funex_die_unimplemented("auxd --stop");
}

static void execute_status(void)
{
	funex_die_unimplemented("auxd --status");
}

static void auxd_execute(void)
{
	perpare();

	if (globals.auxd.oper == AUXD_START) {
		execute_start();
	} else if (globals.auxd.oper == AUXD_STOP) {
		execute_stop();
	} else if (globals.auxd.oper == AUXD_STATUS) {
		execute_status();
	}
}

static void auxd_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'S':
				globals.auxd.oper = AUXD_START;
				break;
			case 'T':
				globals.auxd.oper = AUXD_STOP;
				break;
			case 'Q':
				globals.auxd.oper = AUXD_STATUS;
				break;
			case 's':
				arg = funex_getoptarg(opt, "usock");
				funex_clone_str(&globals.auxd.usock, arg);
				break;
			case 'f':
				arg = funex_getoptarg(opt, "conf");
				funex_globals_set_conf(arg);
				break;
			case 'Z':
				globals.proc.nodaemon = FNX_TRUE;
				break;
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
}

const funex_cmdent_t funex_cmdent_auxd = {
	.name       = "auxd",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = auxd_getopts,
	.exec       = auxd_execute,
	.usage      = "@ [--start|--stop|--status]",
	.desc       = "Control and query auxiliary-daemon",
	.optdef     = \
	"h,help d,debug= Z,nodaemon f,conf= s,usock= S,start T,stop Q,status",
	.optdesc    = \
	"-f, --conf=<file>      Use configuration-file \n"\
	"-s, --usock=<path>     UNIX-domain socket for IPC \n"\
	"-Z, --no-daemon        Do not ran as daemon process \n" \
	"-d, --debug=<level>    Debug mode level \n" \
};


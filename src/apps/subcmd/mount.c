/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libmount/libmount.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)


static void prepare(void)
{
	int rc, rdwr;
	mode_t mask;

	rdwr = fx_testlf(globals.fs.mntf, FNX_MNTF_RDONLY);
	mask = rdwr ? (R_OK | W_OK) : R_OK;

	if ((rc = fnx_access(globals.vol.path, (int)mask)) != 0) {
		funex_dief("no-access: %s", globals.vol.path);
	}
	if ((rc = fnx_statvol(globals.vol.path, NULL, mask)) != 0) {
		if (rc == -ENOTBLK) {
			funex_dief("not-reg-or-blk: %s", globals.vol.path);
		} else if (rc == -EACCES) {
			funex_dief("no-access: %s", globals.vol.path);
		} else {
			funex_dief("illegal-volume: %s err=%d", globals.vol.path, -rc);
		}
	}

	mask = (R_OK | W_OK | X_OK);
	if ((rc = fnx_access(globals.fs.mntpoint, (int)mask)) != 0) {
		funex_dief("no-rwx-access: %s", globals.fs.mntpoint);
	}
	if ((rc = fnx_checkfusemnt(globals.fs.mntpoint)) != 0) {
		funex_dief("illegal-mount-point: %s err=%d", globals.fs.mntpoint, -rc);
	}
}

static void execute(void)
{
	funex_execsub(globals.prog.argc, globals.prog.argv, "fsd");
}

static void mount_execute(void)
{
	prepare();
	execute();
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void mount_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg = NULL;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'o':
				arg = funex_getoptarg(opt, "options");
				funex_globals_set_mntf(arg);
				break;
			case 'Z':
				globals.proc.nodaemon = FNX_TRUE;
				break;
			case 'R':
				globals.fs.mntf |= FNX_MNTF_RDONLY;
				break;
			case 'f':
				arg = funex_getoptarg(opt, "conf");
				funex_globals_set_conf(arg);
				break;
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "volume", FUNEX_ARG_REQ);
	funex_globals_set_volpath(arg);
	arg = funex_getnextarg(opt, "mntpoint", FUNEX_ARG_REQLAST);
	funex_globals_set_mntpoint(arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_mount = {
	.name       = "mount",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = mount_getopts,
	.exec       = mount_execute,
	.usage      = \
	"@ [--conf=<file>] [--rdonly] [-o opts] <volume> <mntpoint> ",
	.desc       = \
	"Attach into file-system hierarchy",
	.optdef     = \
	"h,help d,debug= f,conf= R,rdonly Z,nodaemon o,options=",
	.optdesc    = \
	"-f, --conf=<file>      Use configuration-file \n"\
	"-R, --rdonly           Read-only mode \n"\
	"-o, --option=<mntops>  Comma separated mount options \n"\
	"-Z, --nodaemon         Do not ran as daemon process \n"\
	"-d, --debug=<level>    Debug mode level \n"\
	"-h, --help             Show this help \n"
};


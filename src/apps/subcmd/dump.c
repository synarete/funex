/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include "funex-common.h"
#include "commands.h"

#define globals     (funex_globals)


static void prepare(void)
{
	int rc;

	rc = fnx_statvol(globals.vol.path, NULL, R_OK);
	if (rc != 0) {
		if (rc == -ENOTBLK) {
			funex_dief("not-reg-or-blk: %s", globals.vol.path);
		} else if (rc == -EACCES) {
			funex_dief("no-access: %s", globals.vol.path);
		} else {
			funex_dief("illegal: %s err=%d", globals.vol.path, rc);
		}
	}
}

static void show_super(fx_vio_t *vio, fx_super_t *super)
{
	const fx_dsec_t *dsec;
	const fx_dfrg_t *dfrg;

	dsec = funex_get_dsec(vio, FNX_SEC_SUPER);
	dfrg = &dsec->sec_frgs[FNX_FRG_SUPER];
	funex_dimport(&super->su_vnode, &dfrg->fr_hdr);
	funex_print_super(super, "super");
}

static void show_rootd(fx_vio_t *vio, fx_dir_t *rootd)
{
	const fx_dsec_t   *dsec;
	const fx_dfrg_t   *dfrg;

	dsec = funex_get_dsec(vio, FNX_SEC_SUPER);
	dfrg = &dsec->sec_frgs[FNX_FRG_ROOT];
	funex_dimport(&rootd->d_inode.i_vnode, &dfrg->fr_hdr);
	funex_print_dir(rootd, "rootd");
}

static void execute(void)
{
	fx_super_t *super;
	fx_dir_t   *rootd;
	fx_vio_t   *vio;

	vio   = funex_open_vio(globals.vol.path, FX_VIOF_RDONLY);
	super = fx_vnode_to_super(funex_newvobj(FNX_VTYPE_SUPER));
	rootd = fx_vnode_to_dir(funex_newvobj(FNX_VTYPE_DIR));

	show_super(vio, super);
	show_rootd(vio, rootd);

	funex_delvobj(&rootd->d_inode.i_vnode);
	funex_delvobj(&super->su_vnode);
	funex_close_vio(vio);
}


static void dump_execute(void)
{
	prepare();
	execute();
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void dump_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'h':
				funex_help_and_goodbye(opt);
				break;
			case 'd':
			default:
				funex_set_debug_level(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "volume", FUNEX_ARG_REQLAST);
	funex_clone_str(&globals.vol.path, arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_dump = {
	.name       = "dump",
	.flags      = FUNEX_CMD_MAIN,
	.gopt       = dump_getopts,
	.exec       = dump_execute,
	.usage      = "@ <volume> ",
	.desc       = "Print volume's meta-data",
	.optdef     = "h,help d,debug="
};


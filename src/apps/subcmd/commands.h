/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_COMMANDS_H_
#define FUNEX_COMMANDS_H_

struct funex_cmdent;

extern const struct funex_cmdent  funex_cmdent_mkfs;
extern const struct funex_cmdent  funex_cmdent_heads;
extern const struct funex_cmdent  funex_cmdent_mount;
extern const struct funex_cmdent  funex_cmdent_umount;
extern const struct funex_cmdent  funex_cmdent_clone;
extern const struct funex_cmdent  funex_cmdent_stats;
extern const struct funex_cmdent  funex_cmdent_quota;
extern const struct funex_cmdent  funex_cmdent_query;
extern const struct funex_cmdent  funex_cmdent_fsck;
extern const struct funex_cmdent  funex_cmdent_dump;
extern const struct funex_cmdent  funex_cmdent_test;
extern const struct funex_cmdent  funex_cmdent_auxd;
extern const struct funex_cmdent  funex_cmdent_version;
extern const struct funex_cmdent  funex_cmdent_help;
extern const struct funex_cmdent  funex_cmdent_license;
extern const struct funex_cmdent  funex_cmdent_debug;
extern const struct funex_cmdent  funex_cmdent_complete;

extern const struct funex_cmdent *funex_commands[];

const struct funex_cmdent *funex_lookup_cmdent(const char *);

#endif /* FUNEX_COMMANDS_H_ */



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)

static void query_execute(void)
{
	int rc, fd = -1;
	unsigned long fid = 0;
	const char *path = globals.path;

	rc = fnx_open(path, O_RDONLY | O_NOATIME, 0, &fd);
	if (rc != 0) {
		funex_dief("no-open: %s err=%d", path, -rc);
	}
	rc = fnx_fquery(fd, &fid);
	if (rc != 0) {
		fnx_close(fd);
		funex_dief("no-fquery: %s err=%d", path, -rc);
	}
	fnx_close(fd);
	fx_info("fid: %#jx", fid);
}

static void query_getopts(funex_getopts_t *opt)
{
	int c;
	const char *arg;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'd':
				funex_set_debug_level(opt);
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
	arg = funex_getnextarg(opt, "path", FUNEX_ARG_REQLAST);
	funex_globals_set_path(arg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t funex_cmdent_query = {
	.name       = "query",
	.flags      = FUNEX_CMD_PRIVATE, /* XXX */
	.gopt       = query_getopts,
	.exec       = query_execute,
	.usage      = "@ <path>",
	.desc       = "Query regular file extended info",
	.optdef     = "h,help d,debug=",
	.optdesc    = \
	"-d, --debug=<level>    Debug mode level \n" \
	"-h, --help             Show this help \n"
};

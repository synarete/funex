/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "funex-common.h"
#include "commands.h"


const struct funex_cmdent *funex_commands[] = {
	/* Commands */
	&funex_cmdent_mkfs,
	&funex_cmdent_mount,
	&funex_cmdent_umount,
	&funex_cmdent_heads,
	&funex_cmdent_clone,
	&funex_cmdent_stats,
	&funex_cmdent_quota,
	&funex_cmdent_query,
	&funex_cmdent_fsck,
	&funex_cmdent_dump,
	&funex_cmdent_test,
	&funex_cmdent_auxd,
	/* Global Options */
	&funex_cmdent_version,
	&funex_cmdent_license,
	&funex_cmdent_help,
	/* Private */
	&funex_cmdent_debug,
	&funex_cmdent_complete,
	NULL
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const funex_cmdent_t *funex_lookup_cmdent(const char *name)
{
	const funex_cmdent_t *ent;

	for (size_t i = 0; i < FX_NELEMS(funex_commands); ++i) {
		ent = funex_commands[i];
		if (ent && ent->name && !strcmp(name, ent->name)) {
			return ent;
		}
		if (ent && ent->alias && !strcmp(name, ent->alias)) {
			return ent;
		}
	}
	return NULL;
}




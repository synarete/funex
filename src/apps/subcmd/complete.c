/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)

/*
 * Bash-completion helper utility.
 *
 * Usage: funex +complete $COMP_POINT ${#COMP_LINE} ${COMP_TYPE}
 *                        ($COMP_CWORD" - 1) ${COMP_WORDS[@]:1}
 *
 * Supports the following cases:
 * - No input: show all available global-options and commands.
 * - No command: show global options.
 * - Command: show sub-commands that match.
 * - Command+options: show possible commands list
 */

#define COMP_TYPE_TAB       ((int)('\t'))
#define COMP_TYPE_LIST      ((int)('?'))
#define COMP_TYPE_PART      ((int)('!'))
#define COMP_TYPE_NOTUNMOD  ((int)('@'))
#define COMP_TYPE_MENU      ((int)('%'))

struct comp_context {
	FILE   *c_fp;                   /* Output stream */
	int     c_debug;                /* Run in debug mode */
	int     c_nwords;               /* ${COMP_WORDS} length */
	char  **c_words;                /* ${COMP_WORDS} */
	char   *c_curr;                 /* Word to complete */
	int     c_cword;                /* ${COMP_CWORD} */
	int     c_point;                /* ${COMP_POINT} */
	int     c_linelen;              /* ${#COMP_LINE} */
	int     c_type;                 /* ${COMP_TYPE} */
	char   *c_command;              /* Command name */
	const funex_cmdent_t *c_cmdent; /* Command's entry */
};
typedef struct comp_context comp_context_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const char *trim_leading_dash(const char *s)
{
	if (s != NULL)
		while (*s && (*s == '-')) {
			++s;
		}
	return s;
}

static int isprefixof(const char *str, const char *prefix)
{
	int res = 0;
	fx_substr_t p;

	if (str && prefix) {
		fx_substr_init(&p, prefix);
		res = (fx_substr_common_prefix(&p, str) == p.len);
	}
	return res;
}

static int isprefixof2(const fx_substr_t *str, const char *prefix)
{
	fx_substr_t p;

	fx_substr_init(&p, prefix);
	return (fx_substr_ncommon_prefix(&p, str->str, str->len) == p.len);
}

static int isvisable(const funex_cmdent_t *cmdent)
{
	return !(cmdent->flags & FUNEX_CMD_PRIVATE);
}

static int isgoption(const funex_cmdent_t *cmdent)
{
	return (cmdent->flags & FUNEX_CMD_GOPTION);
}

static int ismain(const funex_cmdent_t *cmdent)
{
	return (cmdent->flags & FUNEX_CMD_MAIN);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int complete_parse(comp_context_t *ctx, int argc, char *argv[])
{
	int n;

	if (argc < 5) {
		return -1;
	}

	n = sscanf(argv[1], "%d", &ctx->c_point);
	if ((n != 1) || (ctx->c_point < 0)) {
		return -1;
	}

	n = sscanf(argv[2], "%d", &ctx->c_linelen);
	if ((n != 1) || (ctx->c_linelen < 0)) {
		return -1;
	}

	n = sscanf(argv[3], "%d", &ctx->c_type);
	if (n != 1) {
		return -1;
	}
	switch (ctx->c_type) {
		case COMP_TYPE_TAB:
		case COMP_TYPE_LIST:
		case COMP_TYPE_PART:
		case COMP_TYPE_NOTUNMOD:
		case COMP_TYPE_MENU:
			break;
		default:
			return -1;
	}

	n = sscanf(argv[4], "%d", &ctx->c_cword);
	if ((n != 1) || (ctx->c_cword < 0)) {
		return -1;
	}

	ctx->c_nwords = argc - 5;
	if (ctx->c_nwords <= 0) {
		return 0;
	}

	ctx->c_words = argv + 5;
	if ((ctx->c_cword >= 0) && (ctx->c_cword < ctx->c_nwords)) {
		ctx->c_curr = ctx->c_words[ctx->c_cword];
	}
	if (ctx->c_nwords > 0) {
		ctx->c_command = ctx->c_words[0];
		ctx->c_cmdent = funex_lookup_cmdent(ctx->c_command);
	}
	return 0;
}

static void complete_parse_or_die(comp_context_t *ctx, int argc, char *argv[])
{
	if (complete_parse(ctx, argc, argv) != 0) {
		exit(EXIT_FAILURE);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void complete_putc(const comp_context_t *ctx, char c)
{
	fputc(c, ctx->c_fp);
}

static void complete_printf(const comp_context_t *ctx, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(ctx->c_fp, fmt, ap);
	va_end(ap);
	fprintf(ctx->c_fp, "%c\n", ' ');
}

static void complete_show_mains(const comp_context_t *ctx)
{
	const funex_cmdent_t **p_ent;
	const funex_cmdent_t *ent;

	for (p_ent = funex_commands; *p_ent != NULL; p_ent++) {
		ent = *p_ent;
		if (isvisable(ent) && ismain(ent) && !isgoption(ent)) {
			complete_printf(ctx, "%s", ent->name);
		}
	}
}

static void complete_show_commands(const comp_context_t *ctx)
{
	const char *prefix;
	const funex_cmdent_t **p_ent;
	const funex_cmdent_t *ent;

	prefix = ctx->c_command;
	for (p_ent = funex_commands; *p_ent != NULL; p_ent++) {
		ent = *p_ent;
		if (!isvisable(ent)) {
			continue;
		}
		if (prefix == NULL) {
			complete_printf(ctx, "%s", ent->name);
		} else if (isprefixof(ent->name, prefix)) {
			complete_printf(ctx, "%s", ent->name);
		} else if (isprefixof(ent->alias, prefix)) {
			complete_printf(ctx, "%s", ent->alias);
		}
	}
}

static void complete_show_reals(const comp_context_t *ctx)
{
	const char *prefix;
	const funex_cmdent_t **p_ent;
	const funex_cmdent_t *ent;

	prefix = ctx->c_curr;
	for (p_ent = funex_commands; *p_ent != NULL; p_ent++) {
		ent = *p_ent;
		if (!isvisable(ent)) {
			continue;
		}
		if (isprefixof(ent->name, prefix)) {
			complete_printf(ctx, "%s", ent->name);
		} else if (isprefixof(ent->alias, prefix)) {
			complete_printf(ctx, "%s", ent->alias);
		}
	}
}


static void complete_show_command_opts(const comp_context_t *ctx)
{
	size_t i, ntoks, nelems;
	fx_substr_t opts;
	fx_substr_t toks[FX_GETOPT_MAX];
	fx_substr_pair_t sp;
	const fx_substr_t *tok = NULL;
	const fx_substr_t *str = NULL;
	const char *optpref;
	const funex_cmdent_t *ent;

	optpref = trim_leading_dash(ctx->c_curr);
	ent = ctx->c_cmdent;
	if (!isvisable(ent)) {
		return;
	}

	nelems  = FX_ARRAYSIZE(toks);
	ntoks   = 0;
	fx_substr_init(&opts, ent->optdef ? ent->optdef : "");
	fx_substr_tokenize_chr(&opts, ' ', toks, nelems, &ntoks);

	for (i = 0; i < ntoks; ++i) {
		tok = &toks[i];
		fx_substr_split(tok, FX_GETOPT_DELIMCHRS, &sp);

		str = &sp.first;
		if (isprefixof2(str, optpref)) {
			complete_printf(ctx, "-%.*s", (int)(str->len), str->str);
		}
		str = &sp.second;
		if (isprefixof2(str, optpref)) {
			complete_printf(ctx, "--%.*s", (int)(str->len), str->str);
		}
	}
}

static void complete_show_cmdhelp_usage(const comp_context_t *ctx)
{
	const char meta = '%';
	const funex_cmdent_t *ent;

	ent = ctx->c_cmdent;
	if (isvisable(ent) && ent->usage) {
		complete_putc(ctx, meta);
		funex_show_cmdhelp_usage(ent, 2);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void complete_try_resolve(const comp_context_t *ctx)
{
	const int type      = ctx->c_type;
	const int point     = ctx->c_point;
	const int linelen   = ctx->c_linelen;
	const char *curr    = ctx->c_curr;
	const char *command = ctx->c_command;
	const funex_cmdent_t *cmdent = ctx->c_cmdent;

	if (cmdent != NULL) { /* Managed to analyze command's entry */
		if (curr && (curr == command)) {
			complete_show_reals(ctx);
		} else if (curr && (curr[0] == '-')) {
			/* Complete command's options */
			complete_show_command_opts(ctx);
		} else if (!curr && (point == linelen) && (type == COMP_TYPE_LIST)) {
			/* Complete at the end of line where we have command but no args */
			complete_show_cmdhelp_usage(ctx);
		}
	} else { /* Unknown command entry */
		if ((command == NULL) && (curr == NULL)) {
			/* No input, show main commands */
			complete_show_mains(ctx);
		} else if ((command != NULL) && (curr == command)) {
			/* Complete possible commands */
			complete_show_commands(ctx);
		}
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void complete_execute(void)
{
	comp_context_t ctx;

	memset(&ctx, 0, sizeof(ctx));
	ctx.c_fp = stdout;

	complete_parse_or_die(&ctx, globals.subc.argc, globals.subc.argv);
	complete_try_resolve(&ctx);
}

static void goptnone(funex_getopts_t *opt)
{
	fx_unused(opt);
}

const funex_cmdent_t funex_cmdent_complete = {
	.name       = "+complete",
	.flags      = FUNEX_CMD_PRIVATE,
	.exec       = complete_execute,
	.gopt       = goptnone,
	.usage      = \
	"@ <COMP_POINT> <#COMP_LINE> <COMP_TYPE> <COMP_CWORD> <COMP_WORDS...>",
	.desc       = "Try to complete to valid command name or option",
};



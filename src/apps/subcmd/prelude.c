/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>

#include "funex-common.h"
#include "commands.h"

#define globals (funex_globals)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void version_execute(void)
{
	funex_show_version(globals.opt.all);
}

static void version_getopts(funex_getopts_t *opt)
{
	int c;

	while ((c = funex_getnextopt(opt)) != 0) {
		switch (c) {
			case 'a':
				globals.opt.all = FNX_TRUE;
				break;
			case 'h':
			default:
				funex_help_and_goodbye(opt);
				break;
		}
	}
}

const funex_cmdent_t funex_cmdent_version = {
	.name       = "--version",
	.alias      = "-v",
	.flags      = FUNEX_CMD_MAIN | FUNEX_CMD_GOPTION,
	.gopt       = version_getopts,
	.exec       = version_execute,
	.usage      = "@ [--all]",
	.desc       = "Show version",
	.optdef     = "h,help a,all",
	.optdesc    = \
	"-a, --all                  Detailed report\n" \
	"-h, --help                 Show this help "
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void goptnone(funex_getopts_t *opt)
{
	fx_unused(opt);
}

static void show_help(void)
{
	int mask1, mask2;
	const char *indent = "    ";
	const char *name;
	const funex_cmdent_t *ent;
	const funex_cmdent_t **p_ent;

	mask1 = FUNEX_CMD_MAIN;
	mask2 = FUNEX_CMD_GOPTION;
	name  = globals.prog.name;
	printf("usage: %s [-hv] <sub-command> <options>\n", name);

	printf("\n%s\n", "sub-commands:");
	p_ent = funex_commands;
	while ((ent = *p_ent++) != NULL) {
		if ((ent->flags & mask1) && !(ent->flags & mask2)) {
			name = ent->name;
			printf("%s%-18s\n", indent, name);
		}
	}

	printf("\n%s\n", "global-options:");
	p_ent = funex_commands;
	while ((ent = *p_ent++) != NULL) {
		if ((ent->flags & mask1) && (ent->flags & mask2)) {
			name  = ent->name;
			printf("%s%s\n", indent, name);
		}
	}
	printf("%s\n", "");
}

const funex_cmdent_t funex_cmdent_help = {
	.name       = "--help",
	.alias      = "-h",
	.flags      = FUNEX_CMD_MAIN | FUNEX_CMD_GOPTION,
	.exec       = show_help,
	.gopt       = goptnone,
	.usage      = "@ ",
	.desc       = "Show help message"
};

const funex_cmdent_t funex_cmdent_license = {
	.name       = "--license",
	.flags      = FUNEX_CMD_MAIN | FUNEX_CMD_GOPTION,
	.exec       = funex_show_license,
	.gopt       = goptnone,
	.usage      = "@ ",
	.desc       = "Show license"
};

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_PROTO_H_
#define FUNEX_PROTO_H_


/*
 * Operations-types numbering
 * NB: 1-43 Follows FUSE numbering
 */
enum FX_OPCODE {
	FX_OP_NONE         = 0,
	FX_OP_LOOKUP       = 1,
	FX_OP_FORGET       = 2,
	FX_OP_GETATTR      = 3,
	FX_OP_SETATTR      = 4,
	FX_OP_READLINK     = 5,
	FX_OP_SYMLINK      = 6,
	FX_OP_MKNOD        = 8,
	FX_OP_MKDIR        = 9,
	FX_OP_UNLINK       = 10,
	FX_OP_RMDIR        = 11,
	FX_OP_RENAME       = 12,
	FX_OP_LINK         = 13,
	FX_OP_OPEN         = 14,
	FX_OP_READ         = 15,
	FX_OP_WRITE        = 16,
	FX_OP_STATFS       = 17,
	FX_OP_RELEASE      = 18,
	FX_OP_FSYNC        = 20,
	FX_OP_FLUSH        = 25,
	FX_OP_OPENDIR      = 27,
	FX_OP_READDIR      = 28,
	FX_OP_RELEASEDIR   = 29,
	FX_OP_FSYNCDIR     = 30,
	FX_OP_ACCESS       = 34,
	FX_OP_CREATE       = 35,
	FX_OP_INTERRUPT    = 36,
	FX_OP_BMAP         = 37,
	FX_OP_POLL         = 40,
	FX_OP_TRUNCATE     = 42,
	FX_OP_FALLOCATE    = 43,
	FX_OP_FPUNCH       = 80,
	FX_OP_FQUERY       = 82,
	FX_OP_XCOPY        = 83,
	FX_OP_LAST         = 127, /* Keep last, DONT overflow uint8 (ioctl) */
};
typedef enum FX_OPCODE fx_opcode_e;


struct fx_lookup_req {
	fx_ino_t        parent;
	fx_name_t       name;
	fx_hash_t       hash;
};

struct fx_lookup_res {
	fx_iattr_t      iattr;
};

struct fx_forget_req {
	fx_ino_t        ino;
	fx_size_t       nlookup;
};

struct fx_getattr_req {
	fx_ino_t        ino;
};

struct fx_getattr_res {
	fx_iattr_t      iattr;
};

struct fx_setattr_req {
	fx_iattr_t      iattr;
	fx_flags_t      flags;
};

struct fx_setattr_res {
	fx_iattr_t      iattr;
};

struct fx_truncate_req {
	fx_ino_t        ino;
	fx_off_t        size;
};

struct fx_truncate_res {
	fx_iattr_t      iattr;
};

struct fx_readlink_req {
	fx_ino_t        ino;
};

struct fx_readlink_res {
	fx_path_t       slnk;
};

struct fx_mknod_req {
	fx_ino_t        parent;
	fx_mode_t       mode;
	fx_dev_t        rdev;
	fx_name_t       name;
	fx_hash_t       hash;
};

struct fx_mknod_res {
	fx_iattr_t      iattr;
};

struct fx_mkdir_req {
	fx_ino_t        parent;
	fx_mode_t       mode;
	fx_name_t       name;
	fx_hash_t       hash;
};

struct fx_mkdir_res {
	fx_iattr_t      iattr;
};

struct fx_unlink_req {
	fx_ino_t        parent;
	fx_name_t       name;
	fx_hash_t       hash;
};

struct fx_unlink_res {
	fx_ino_t        ino;
};

struct fx_rmdir_req {
	fx_ino_t        parent;
	fx_name_t       name;
	fx_hash_t       hash;
};

struct fx_symlink_req {
	fx_ino_t        parent;
	fx_name_t       name;
	fx_hash_t       hash;
	fx_path_t       slnk;
};

struct fx_symlink_res {
	fx_iattr_t      iattr;
};

struct fx_rename_req {
	fx_ino_t        parent;
	fx_ino_t        newparent;
	fx_name_t       name;
	fx_hash_t       hash;
	fx_name_t       newname;
	fx_hash_t       newhash;
};

struct fx_link_req {
	fx_ino_t        ino;
	fx_ino_t        newparent;
	fx_name_t       newname;
	fx_hash_t       newhash;
};

struct fx_link_res {
	fx_iattr_t      iattr;
};

struct fx_open_req {
	fx_ino_t        ino;
	fx_flags_t      flags;
};

struct fx_read_req {
	fx_ino_t        ino;
	fx_off_t        off;
	fx_size_t       size;
};

struct fx_read_res {
	fx_size_t       size;
};

struct fx_write_req {
	fx_ino_t        ino;
	fx_off_t        off;
	fx_size_t       size;
};

struct fx_write_res {
	fx_size_t       size;
};

struct fx_flush_req {
	fx_ino_t        ino;
};

struct fx_release_req {
	fx_ino_t        ino;
	fx_flags_t      flags;
};

struct fx_fsync_req {
	fx_ino_t        ino;
	fx_flags_t      datasync;
};

struct fx_opendir_req {
	fx_ino_t        ino;
};

struct fx_readdir_req {
	fx_ino_t        ino;
	fx_off_t        off;
	fx_size_t       size;
};

struct fx_readdir_res {
	fx_ino_t        child;
	fx_name_t       name;
	fx_mode_t       mode;
	fx_off_t        off_next;
};

struct fx_releasedir_req {
	fx_ino_t        ino;
};

struct fx_fsyncdir_req {
	fx_ino_t        ino;
	fx_bool_t       datasync;
};

struct fx_statfs_req {
	fx_ino_t        ino;
};

struct fx_statfs_res {
	fx_fsinfo_t     fsinfo;
};

struct fx_access_req {
	fx_ino_t        ino;
	fx_mode_t       mask;
};

struct fx_create_req {
	fx_ino_t        parent;
	fx_name_t       name;
	fx_hash_t       hash;
	fx_mode_t       mode;
	fx_flags_t      flags;
};

struct fx_create_res {
	fx_iattr_t      iattr;
};

struct fx_bmap_req {
	fx_ino_t        ino;
	fx_size_t       blocksize;
	fx_lba_t        idx;
};

struct fx_bmap_res {
	fx_lba_t        idx;
};

struct fx_poll_req {
	fx_ino_t        ino;
};

struct fx_poll_res {
	unsigned        revents;
};

struct fx_fallocate_req {
	fx_ino_t        ino;
	fx_off_t        off;
	fx_size_t       len;
	fx_bool_t       keep_size;
};

struct fx_fpunch_req {
	fx_ino_t        ino;
	fx_off_t        off;
	fx_size_t       len;
};

struct fx_fquery_req {
	fx_ino_t        ino;
};

struct fx_fquery_res {
	fx_iattr_t      iattr;
};

struct fx_xcopy_req {
	fx_ino_t        ino;
	fx_off_t        off;
	fx_size_t       len;
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

union fx_request {
	struct fx_lookup_req        req_lookup;
	struct fx_forget_req        req_forget;
	struct fx_getattr_req       req_getattr;
	struct fx_setattr_req       req_setattr;
	struct fx_truncate_req      req_truncate;
	struct fx_readlink_req      req_readlink;
	struct fx_mknod_req         req_mknod;
	struct fx_mkdir_req         req_mkdir;
	struct fx_unlink_req        req_unlink;
	struct fx_rmdir_req         req_rmdir;
	struct fx_symlink_req       req_symlink;
	struct fx_rename_req        req_rename;
	struct fx_link_req          req_link;
	struct fx_open_req          req_open;
	struct fx_read_req          req_read;
	struct fx_write_req         req_write;
	struct fx_flush_req         req_flush;
	struct fx_release_req       req_release;
	struct fx_fsync_req         req_fsync;
	struct fx_opendir_req       req_opendir;
	struct fx_readdir_req       req_readdir;
	struct fx_releasedir_req    req_releasedir;
	struct fx_fsyncdir_req      req_fsyncdir;
	struct fx_statfs_req        req_statfs;
	struct fx_access_req        req_access;
	struct fx_create_req        req_create;
	struct fx_bmap_req          req_bmap;
	struct fx_poll_req          req_pool;
	struct fx_fallocate_req     req_fallocate;
	struct fx_fpunch_req        req_fpunch;
	struct fx_fquery_req        req_fquery;
	struct fx_xcopy_req         req_xcopy;
};
typedef union fx_request fx_request_t;

union fx_response {
	struct fx_lookup_res        res_lookup;
	struct fx_getattr_res       res_getattr;
	struct fx_setattr_res       res_setattr;
	struct fx_truncate_res      res_truncate;
	struct fx_readlink_res      res_readlink;
	struct fx_mknod_res         res_mknod;
	struct fx_mkdir_res         res_mkdir;
	struct fx_unlink_res        res_unlink;
	struct fx_symlink_res       res_symlink;
	struct fx_link_res          res_link;
	struct fx_read_res          res_read;
	struct fx_write_res         res_write;
	struct fx_readdir_res       res_readdir;
	struct fx_statfs_res        res_statfs;
	struct fx_create_res        res_create;
	struct fx_bmap_res          res_bmap;
	struct fx_poll_res          res_poll;
	struct fx_fquery_res        res_fquery;
};
typedef union fx_response fx_response_t;


#endif /* FUNEX_PROTO_H_ */





/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "balagan.h"
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "metai.h"
#include "index.h"
#include "htod.h"
#include "addr.h"
#include "addri.h"
#include "elems.h"
#include "super.h"
#include "inode.h"
#include "inodei.h"
#include "iobuf.h"

#define VNODE_MAGIC      (0x33DEF44A)
#define MODE_BASE  (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_vtype_e fx_mode_to_vtype(fx_mode_t mode)
{
	fx_mode_t mtype;
	fx_vtype_e vtype;

	mtype = mode & S_IFMT;
	switch (mtype) {
		case S_IFCHR:
			vtype = FNX_VTYPE_CHRDEV;
			break;
		case S_IFBLK:
			vtype = FNX_VTYPE_BLKDEV;
			break;
		case S_IFSOCK:
			vtype = FNX_VTYPE_SOCK;
			break;
		case S_IFIFO:
			vtype = FNX_VTYPE_FIFO;
			break;
		case S_IFLNK:
			vtype = FNX_VTYPE_SSYMLNK;
			break;
		case S_IFREG:
			vtype = FNX_VTYPE_REG;
			break;
		case S_IFDIR:
			vtype = FNX_VTYPE_DIR;
			break;
		default:
			vtype = FNX_VTYPE_EMPTY;
			break;
	}
	return vtype;
}

fx_mode_t fx_vtype_to_mode(fx_vtype_e vtype)
{
	fx_mode_t mode = 0;

	if (vtype == FNX_VTYPE_CHRDEV) {
		mode = S_IFCHR;
	} else if (vtype == FNX_VTYPE_BLKDEV) {
		mode = S_IFBLK;
	} else if (vtype == FNX_VTYPE_SOCK) {
		mode = S_IFSOCK;
	} else if (vtype == FNX_VTYPE_FIFO) {
		mode = S_IFIFO;
	} else if (vtype == FNX_VTYPE_SSYMLNK) {
		mode = S_IFLNK;
	} else if (vtype == FNX_VTYPE_SYMLNK) {
		mode = S_IFLNK;
	} else if (vtype == FNX_VTYPE_REG) {
		mode = S_IFREG;
	} else if (vtype == FNX_VTYPE_DIR) {
		mode = S_IFDIR;
	} else {
		mode = 0;
	}
	return mode;
}

fx_mode_t fx_vtype_to_crmode(fx_vtype_e vtype, fx_mode_t mode, fx_mode_t umsk)
{
	fx_mode_t i_mode, c_mode, fmt_mask;

	i_mode   = fx_vtype_to_mode(vtype);
	fmt_mask = (fx_mode_t)S_IFMT;
	c_mode   = (mode & ~umsk) & ~fmt_mask;

	return (i_mode | c_mode);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_vnode_init(fx_vnode_t *vnode, fx_vtype_e vtype)
{
	link_init(&vnode->v_mlink);
	link_init(&vnode->v_clink);
	vaddr_init(&vnode->v_vaddr, vtype);
	bkaddr_reset(&vnode->v_bkaddr);
	vnode->v_vtype          = vtype;
	vnode->v_refcnt         = 0;
	vnode->v_stain          = FX_STAIN_NONE;
	vnode->v_cached         = FNX_FALSE;
	vnode->v_stable         = FNX_FALSE;
	vnode->v_removed        = FNX_FALSE;
	vnode->v_pseudo         = FNX_FALSE;
	vnode->v_magic          = VNODE_MAGIC;
	vnode->v_hlnk           = NULL;
}

void fx_vnode_destroy(fx_vnode_t *vnode)
{
	fx_assert(!vnode->v_cached);
	fx_assert(vnode->v_magic == VNODE_MAGIC);
	fx_assert(vnode->v_hlnk == NULL);

	link_destroy(&vnode->v_mlink);
	link_destroy(&vnode->v_clink);
	vaddr_destroy(&vnode->v_vaddr);
	bkaddr_reset(&vnode->v_bkaddr);
	vnode->v_refcnt         = 0;
	vnode->v_vaddr.ino      = FNX_INO_NULL;
	vnode->v_vaddr.xno      = 0;
	vnode->v_vaddr.vtype    = FNX_VTYPE_NONE;
	vnode->v_magic          = 0;
}

void fx_vnode_dexport(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_size_t nfrg;
	fx_vtype_e vtype;
	const fx_vaddr_t *vaddr;

	vaddr   = &vnode->v_vaddr;
	vtype   = vaddr->vtype;
	nfrg    = fx_dobjnfrg(vtype);
	fx_assert((hdr->h_len == 0) || (hdr->h_len == nfrg));
	fx_dobj_assign(hdr, vtype, vaddr->ino, vaddr->xno, nfrg);
}

void fx_vnode_dimport(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_size_t nfrg, hdrlen;
	fx_vaddr_t vaddr;

	vaddr.vtype = fx_dobj_vtype(hdr);
	vaddr.ino   = fx_dobj_getino(hdr);
	vaddr.xno   = fx_dobj_getxno(hdr);

	nfrg    = fx_dobjnfrg(vaddr.vtype);
	hdrlen  = fx_dobj_getlen(hdr);
	fx_assert(nfrg == hdrlen); /* XXX */

	vnode_setvaddr(vnode, &vaddr);
}

void fx_vnode_dverify(const fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_size_t nfrg;
	fx_vtype_e vtype;

	vtype   = vnode_vtype(vnode);
	nfrg    = fx_dobjnfrg(vtype);
	fx_assert((hdr->h_len == 0) || (hdr->h_len == nfrg));
}

void fx_vnode_setbkaddr(fx_vnode_t *vnode,
                        const fx_bkref_t *sec, const fx_dfrg_t *dfrg)
{
	fx_bkaddr_byref(&vnode->v_bkaddr, sec, dfrg);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_inode_t *fx_vnode_to_inode(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_inode_t, i_vnode);
}

void fx_inode_init(fx_inode_t *inode, fx_vtype_e vtype)
{
	fx_vnode_init(&inode->i_vnode, vtype);
	fx_name_init(&inode->i_name);
	fx_iattr_init(&inode->i_iattr);
	inode->i_flags      = 0;
	inode->i_parentd    = FNX_INO_NULL;
	inode->i_refino     = FNX_INO_NULL;
	inode->i_tlen       = 0;
	inode->i_nhfunc     = FNX_HFUNC_SIP_DINO;
	inode->i_vhfunc     = 0;
	inode->i_nhash      = 0;
	inode->i_magic      = FNX_INODE_MAGIC;
	inode->i_super      = NULL;
	inode->i_meta       = NULL;
	inode->i_server     = NULL;

	inode->i_iattr.i_mode = fx_vtype_to_crmode(vtype, MODE_BASE, 0);
}

void fx_inode_destroy(fx_inode_t *inode)
{
	fx_assert(inode->i_magic == FNX_INODE_MAGIC);
	fx_iattr_destroy(&inode->i_iattr);
	fx_vnode_destroy(&inode->i_vnode);
}

void fx_inode_setino(fx_inode_t *inode, fx_ino_t ino)
{
	fx_vtype_e vtype;
	fx_vaddr_t vaddr;

	vtype = inode_vtype(inode);
	vaddr_for_inode(&vaddr, vtype, ino);
	vnode_setvaddr(&inode->i_vnode, &vaddr);
	inode->i_iattr.i_ino = ino;
}

static void
inode_setcmode(fx_inode_t *inode, const fx_uctx_t *uctx,
               fx_mode_t mode, fx_mode_t xmsk)
{
	fx_mode_t cmod, umsk;

	umsk = uctx->u_cred.cr_umask;
	cmod = fx_vtype_to_crmode(inode_vtype(inode), mode, umsk);

	inode->i_iattr.i_mode = cmod | xmsk;
}

static void inode_setcred(fx_inode_t *inode, const fx_uctx_t *uctx)
{
	inode->i_iattr.i_uid = uctx->u_cred.cr_uid;
	inode->i_iattr.i_gid = uctx->u_cred.cr_gid;
}

void fx_inode_setup(fx_inode_t *inode, const fx_uctx_t *uctx,
                    fx_mode_t mode, fx_mode_t xmsk)
{
	inode_setcred(inode, uctx);
	inode_setcmode(inode, uctx, mode, xmsk);
	fx_inode_setitime(inode, FNX_BAMCTIME_NOW);
}

fx_hash_t fx_calc_inamehash(const fx_name_t *name, fx_ino_t parent_ino)
{
	uint64_t dino, hash = 0;
	uint8_t seed[16] = {
		/* ASCII: "funexfilesystem" */
		0x66, 0x75, 0x6e, 0x65, 0x78, 0x66, 0x69, 0x6c,
		0x65, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x00
	};

	dino = (uint64_t)parent_ino;
	for (size_t i = 0, j = 8; i < 8; ++i, ++j) {
		seed[i] ^= (uint8_t)(dino >> (8 * i));
		seed[j] ^= (uint8_t)(~dino >> (8 * i));
	}

	blgn_siphash64(seed, name->str, name->len, &hash);
	return hash;
}

void fx_inode_associate(fx_inode_t *inode,
                        fx_ino_t dino, const fx_name_t *name, fx_hash_t nhash)
{
	inode->i_parentd = dino;
	if (name != NULL) {
		fx_name_clone(name, &inode->i_name);
	} else {
		fx_name_setup(&inode->i_name, "");
	}
	if ((nhash == 0) && (name != NULL)) {
		inode->i_nhash = fx_calc_inamehash(name, dino);
	} else {
		inode->i_nhash = nhash;
	}
}

void fx_inode_htod(const fx_inode_t *inode, fx_dinode_t *dinode)
{
	fx_header_t *hdr = &dinode->i_hdr;

	/* Header & tail */
	fx_vnode_dexport(&inode->i_vnode, hdr);
	fx_dobj_zpad(hdr, inode_vtype(inode));

	/* R[0] */
	dinode->i_flags     = fx_htod_flags(inode->i_flags);
	dinode->i_parentd   = fx_htod_ino(inode->i_parentd);
	dinode->i_refino    = fx_htod_ino(inode->i_refino);

	/* R[1..2] */
	fx_iattr_htod(&inode->i_iattr, &dinode->i_attr);

	/* R[3..6] */
	fx_htod_name(dinode->i_name, &dinode->i_namelen, &inode->i_name);

	/* R[7] */
	dinode->i_tlen      = fx_htod_u16((uint16_t)(inode->i_tlen));
	dinode->i_vhfunc    = fx_htod_hfunc(inode->i_vhfunc);
	dinode->i_nhfunc    = fx_htod_hfunc(inode->i_nhfunc);
	dinode->i_nhash     = fx_htod_hash(inode->i_nhash);
	dinode->i_magic     = fx_htod_magic(inode->i_magic);
}

void fx_inode_dtoh(fx_inode_t *inode, const fx_dinode_t *dinode)
{
	const fx_header_t *hdr = &dinode->i_hdr;

	/* Header */
	fx_vnode_dimport(&inode->i_vnode, hdr);

	/* R[0] */
	inode->i_flags      = fx_dtoh_flags(dinode->i_flags);
	inode->i_parentd    = fx_dtoh_ino(dinode->i_parentd);
	inode->i_refino     = fx_dtoh_ino(dinode->i_refino);

	/* R[1..2] */
	fx_iattr_dtoh(&inode->i_iattr, &dinode->i_attr);
	inode->i_iattr.i_ino = inode->i_vnode.v_vaddr.ino;

	/* R[3..6] */
	fx_dtoh_name(&inode->i_name, dinode->i_name, dinode->i_namelen);

	/* R[7] */
	inode->i_tlen       = fx_dtoh_u16(dinode->i_tlen);
	inode->i_vhfunc     = fx_dtoh_hfunc(dinode->i_vhfunc);
	inode->i_nhfunc     = fx_dtoh_hfunc(dinode->i_nhfunc);
	inode->i_nhash      = fx_dtoh_hash(dinode->i_nhash);
}

int fx_inode_dcheck(const fx_header_t *hdr)
{
	int rc;
	fx_magic_t magic;
	fx_hfunc_e hfunc;
	const fx_dinode_t *dinode;

	rc = fx_dobj_check(hdr, FNX_VTYPE_ANY);
	if (rc != 0) {
		return rc;
	}
	dinode = header_to_dinode(hdr);
	magic = fx_dtoh_magic(dinode->i_magic);
	if (magic != FNX_INODE_MAGIC) {
		fx_warn("illegal-inode-magic: magic=%#x expected=%#x",
		        magic, FNX_INODE_MAGIC);
		return -1;
	}
	hfunc = fx_dtoh_hfunc(dinode->i_nhfunc);
	if (hfunc != FNX_HFUNC_SIP_DINO) {
		fx_assert(hfunc == FNX_HFUNC_SIP_DINO);
		return -1;
	}
	/* TODO: Check hash-value is correct
	 * NB: Have hcheck for checks after conversion to host repr?
	 */

	return 0;
}

void fx_inode_setitimes(fx_inode_t *inode, fx_flags_t flags,
                        const fx_times_t *tm)
{
	fx_times_fill(&inode->i_iattr.i_times, flags, tm);
}

void fx_inode_setitime(fx_inode_t *inode, fx_flags_t flags)
{
	fx_times_fill(&inode->i_iattr.i_times, flags, NULL);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_mode_t
inode_rwx(const fx_inode_t *inode, const fx_uctx_t *uctx)
{
	fx_mode_t i_mode, res_mask;
	fx_uid_t uid, i_uid;
	fx_gid_t gid, i_gid;
	const fx_cred_t *cred;
	const fx_iattr_t *iattr;

	cred = &uctx->u_cred;
	uid = cred->cr_uid;
	gid = cred->cr_gid;

	iattr = &inode->i_iattr;
	i_mode = iattr->i_mode;
	i_uid = iattr->i_uid;
	i_gid = iattr->i_gid;

	res_mask = F_OK;
	if (uctx->u_root) {
		res_mask |= R_OK | W_OK;
		if (inode_isreg(inode)) {
			if (i_mode & (S_IXUSR | S_IXGRP | S_IXOTH)) {
				res_mask |= X_OK;
			}
		} else {
			res_mask |= X_OK;
		}
	} else if (uid == i_uid) {
		/* Owner permissions */
		if (i_mode & S_IRUSR) {
			res_mask |= R_OK;
		}
		if (i_mode & S_IWUSR) {
			res_mask |= W_OK;
		}
		if (i_mode & S_IXUSR) {
			res_mask |= X_OK;
		}
	} else if (gid == i_gid) {
		/* Group permissions */
		if (i_mode & S_IRGRP) {
			res_mask |= R_OK;
		}
		if (i_mode & S_IWGRP) {
			res_mask |= W_OK;
		}
		if (i_mode & S_IXGRP) {
			res_mask |= X_OK;
		}
		/* TODO: Check for supplementary groups */
	} else {
		/* Other permissions */
		if (i_mode & S_IROTH) {
			res_mask |= R_OK;
		}
		if (i_mode & S_IWOTH) {
			res_mask |= W_OK;
		}
		if (i_mode & S_IXOTH) {
			res_mask |= X_OK;
		}
	}
	return res_mask;
}

int fx_inode_access(const fx_inode_t *inode,
                    const fx_uctx_t *uctx, fx_mode_t mask)
{
	fx_mode_t rwx;

	rwx = inode_rwx(inode, uctx);
	return ((rwx & mask) == mask) ? 0 : -1;
}


void fx_inode_getiattr(const fx_inode_t *inode, fx_iattr_t *iattr)
{
	fx_bcopy(iattr, &inode->i_iattr, sizeof(*iattr));
}

static int mode_isexec(fx_mode_t mode)
{
	const fx_mode_t mask = S_IXUSR | S_IXGRP | S_IXOTH;
	return ((mode & mask) != 0);
}

int fx_inode_isexec(const fx_inode_t *inode)
{
	return mode_isexec(inode->i_iattr.i_mode);
}

static fx_mode_t mode_clearbits(fx_mode_t mode, fx_mode_t bits)
{
	fx_mode_t mask;

	mask = ~((mode_t)(bits));
	return (mode & mask);
}

void fx_inode_clearsuid(fx_inode_t *inode)
{
	inode->i_iattr.i_mode = mode_clearbits(inode->i_iattr.i_mode, S_ISUID);
}

void fx_inode_clearsgid(fx_inode_t *inode)
{
	inode->i_iattr.i_mode = mode_clearbits(inode->i_iattr.i_mode, S_ISGID);
}



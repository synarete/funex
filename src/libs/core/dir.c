/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "metai.h"
#include "index.h"
#include "htod.h"
#include "addr.h"
#include "addri.h"
#include "elems.h"
#include "iobuf.h"
#include "super.h"
#include "inode.h"
#include "inodei.h"
#include "dir.h"
#include "diri.h"


#define FX_DIR_INIT_MODE \
	(S_IFDIR | S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)

#define FX_DDIRSIZE (sizeof(fx_ddir_t))

#define nelems(x) FX_NELEMS(x)

/* Locals */
static void dir_iref(fx_dir_t *, fx_dirent_t *, const fx_inode_t *);
static void dir_iref_self(fx_dir_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void dirent_clear(fx_dirent_t *dirent)
{
	dirent->de_ino  = FNX_INO_NULL;
	dirent->de_hash = 0;
	dirent->de_nlen = 0;
}

static int dirent_isfree(const fx_dirent_t *dirent)
{
	return (dirent->de_ino == FNX_INO_NULL);
}

static int dirent_isused(const fx_dirent_t *dirent)
{
	return !dirent_isfree(dirent);
}

static int dirent_isspecial(const fx_dirent_t *dirent)
{
	const fx_off_t doff = dirent->de_doff;
	return ((doff == FNX_DOFF_SELF) || (doff == FNX_DOFF_PARENT));
}

static int
dirent_isusedby(const fx_dirent_t *dirent,
                fx_hash_t hash, fx_size_t nlen, fx_ino_t ino)
{
	if ((dirent->de_hash != hash) ||
	    (dirent->de_nlen != nlen)) {
		return FNX_FALSE;
	}
	if ((ino != FNX_INO_ANY) &&
	    (dirent->de_ino != ino)) {
		return FNX_FALSE;
	}
	return FNX_TRUE;
}

static void dirent_assign(fx_dirent_t *dirent, const fx_inode_t *inode)
{
	dirent->de_ino  = inode_getino(inode);
	dirent->de_hash = inode->i_nhash;
	dirent->de_nlen = inode->i_name.len;
}

static void encode_ino_nlen(uint64_t *val, fx_ino_t ino, fx_size_t nlen)
{
	*val = ((uint64_t)(ino) << 8) | ((uint64_t)(nlen & 0xFF));
}

static void decode_ino_nlen(uint64_t val, fx_ino_t *ino, fx_size_t *nlen)
{
	*ino  = (fx_ino_t)(val >> 8);
	*nlen = (fx_size_t)(val & 0xFF);
}

static void dirent_htod(const fx_dirent_t *dirent, fx_ddirent_t *ddirent)
{
	uint64_t ino_namelen = 0;

	encode_ino_nlen(&ino_namelen, dirent->de_ino, dirent->de_nlen);
	ddirent->de_namehash      = fx_htod_hash(dirent->de_hash);
	ddirent->de_ino_namelen   = fx_htod_u64(ino_namelen);
}

static void dirent_dtoh(fx_dirent_t *dirent, const fx_ddirent_t *ddirent)
{
	uint64_t ino_namelen;

	ino_namelen     = fx_dtoh_u64(ddirent->de_ino_namelen);
	dirent->de_hash = fx_dtoh_hash(ddirent->de_namehash);
	decode_ino_nlen(ino_namelen, &dirent->de_ino, &dirent->de_nlen);
}


static void dents_init(fx_dirent_t dents[], size_t nelems, fx_off_t doff_base)
{
	for (size_t i = 0; i < nelems; ++i) {
		dirent_clear(&dents[i]);
		dents[i].de_doff = doff_base + (fx_off_t)i;
	}
}

static void dents_setup(fx_dirent_t dents[], size_t nelems, fx_off_t base)
{
	for (size_t i = 0; i < nelems; ++i) {
		dents[i].de_doff = base + (fx_off_t)i;
	}
}


static fx_dirent_t *
dents_lookup(const fx_dirent_t dents[], size_t nelems, size_t ndents,
             fx_hash_t hash, fx_size_t nlen)
{
	size_t pos, cnt = 0;
	const fx_dirent_t *dirent = NULL;

	if ((ndents > 0) && (nlen > 0)) {
		pos = hash % nelems;
		for (size_t i = 0; i < nelems; ++i) {
			dirent = &dents[pos];
			if (dirent_isused(dirent) && !dirent_isspecial(dirent)) {
				if (dirent_isusedby(dirent, hash, nlen, FNX_INO_ANY)) {
					return (fx_dirent_t *)dirent;
				}
				if (++cnt >= ndents) {
					break;
				}
			}
			pos = (pos + 1) % nelems;
		}
	}
	return NULL;
}

static fx_dirent_t *
dents_search(const fx_dirent_t dents[], size_t nelems, size_t ndents,
             fx_off_t doff_base, fx_off_t doff)
{
	size_t base;
	const fx_dirent_t *dirent = NULL;

	if (ndents > 0) {
		base = (doff >= doff_base) ? off_len(doff_base, doff) : 0;
		for (size_t i = base; i < nelems; ++i) {
			dirent = &dents[i];
			if (dirent_isused(dirent)) {
				return (fx_dirent_t *)dirent;
			}
		}
	}
	return NULL;
}

static fx_dirent_t *dents_insert(fx_dirent_t dents[], size_t nelems,
                                 size_t *ndents, const fx_inode_t *inode)
{
	size_t pos;
	fx_dirent_t *dirent;

	if (*ndents < nelems) {
		pos = inode->i_nhash % nelems;
		for (size_t i = 0; i < nelems; ++i) {
			dirent = &dents[pos];
			if (dirent_isfree(dirent) && !dirent_isspecial(dirent)) {
				dirent_assign(dirent, inode);
				*ndents += 1;
				return dirent;
			}
			pos = (pos + 1) % nelems;
		}
	}
	return NULL;
}

static fx_dirent_t *dents_remove(fx_dirent_t dents[], size_t nelems,
                                 size_t *ndents, const fx_inode_t *inode)
{
	size_t pos, cnt = 0;
	fx_dirent_t *dirent;
	const fx_ino_t  ino  = inode_getino(inode);
	const fx_hash_t hash = inode->i_nhash;
	const fx_size_t nlen = inode->i_name.len;

	if (*ndents > 0) {
		pos = hash % nelems;
		for (size_t i = 0; i < nelems; ++i) {
			dirent = &dents[pos];
			if (dirent_isused(dirent)) {
				if (dirent_isusedby(dirent, hash, nlen, ino)) {
					dirent_clear(dirent);
					*ndents -= 1;
					return dirent;
				}
				if (++cnt >= *ndents) {
					break;
				}
			}
			pos = (pos + 1) % nelems;
		}
	}
	return NULL;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_dirext_t *fx_vnode_to_dirext(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_dirext_t, dx_vnode);
}

void fx_dirext_init(fx_dirext_t *dirext)
{
	fx_vnode_init(&dirext->dx_vnode, FNX_VTYPE_DIREXT);
	fx_bzero(dirext->dx_segs, sizeof(dirext->dx_segs));
	dents_init(dirext->dx_dent, nelems(dirext->dx_dent), FNX_DOFF_BEGINX);
	dirext->dx_ndents   = 0;
	dirext->dx_nsegs    = 0;
	dirext->dx_magic    = FNX_DIREXT_MAGIC;
}

void fx_dirext_destroy(fx_dirext_t *dirext)
{
	fx_vnode_destroy(&dirext->dx_vnode);
	dirext->dx_ndents   = 0;
	dirext->dx_nsegs    = 0;
	dirext->dx_magic    = 1;
}


void fx_dirext_htod(const fx_dirext_t *dirext, fx_ddirext_t *ddirext)
{
	fx_header_t *hdr = &ddirext->dx_hdr;

	/* Header & tail */
	fx_vnode_dexport(&dirext->dx_vnode, hdr);
	fx_dobj_zpad(hdr, FNX_VTYPE_DIREXT);

	/* R[0] */
	ddirext->dx_ndents  = fx_htod_u32((uint32_t)dirext->dx_ndents);
	ddirext->dx_nsegs   = fx_htod_u32((uint32_t)dirext->dx_nsegs);
	ddirext->dx_magic   = fx_htod_magic(dirext->dx_magic);

	/* R[1..9] */
	for (size_t i = 0; i < nelems(ddirext->dx_segs); ++i) {
		ddirext->dx_segs[i] = fx_htod_u32(dirext->dx_segs[i]);
	}

	/* R[10..63] */
	for (size_t j = 0; j < nelems(ddirext->dx_dent); ++j) {
		dirent_htod(&dirext->dx_dent[j], &ddirext->dx_dent[j]);
	}
}

void fx_dirext_dtoh(fx_dirext_t *dirext, const fx_ddirext_t *ddirext)
{
	int rc;
	const fx_header_t *hdr = &ddirext->dx_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_DIREXT);
	fx_assert(rc == 0);
	fx_vnode_dimport(&dirext->dx_vnode, hdr);

	/* R[0] */
	dirext->dx_ndents   = fx_dtoh_u32(ddirext->dx_ndents);
	dirext->dx_nsegs    = fx_dtoh_u32(ddirext->dx_nsegs);

	/* R[1..9] */
	for (size_t i = 0; i < nelems(dirext->dx_segs); ++i) {
		dirext->dx_segs[i]  = fx_dtoh_u32(ddirext->dx_segs[i]);
	}

	/* R[10..63] */
	for (size_t j = 0; j < nelems(dirext->dx_dent); ++j) {
		dirent_dtoh(&dirext->dx_dent[j], &ddirext->dx_dent[j]);
	}
}

int fx_dirext_dcheck(const fx_header_t *hdr)
{
	int rc;
	unsigned mask;
	fx_vtype_e vtype;
	fx_magic_t magic;
	fx_size_t value1, value2;
	fx_dirent_t dirent;
	const fx_ddirext_t *ddirext;

	vtype = fx_dobj_vtype(hdr);
	if (vtype != FNX_VTYPE_DIREXT) {
		return -1;
	}
	rc = fx_dobj_check(hdr, vtype);
	if (rc != 0) {
		fx_assert(0); /* XXX */
		return -1;
	}
	ddirext = header_to_ddirext(hdr);
	magic = fx_dtoh_magic(ddirext->dx_magic);
	fx_assert(magic == FNX_DIREXT_MAGIC);
	if (magic != FNX_DIREXT_MAGIC) {
		return -1;
	}

	value1 = 0;
	for (size_t i = 0; i < nelems(ddirext->dx_segs); ++i) {
		mask = fx_dtoh_u32(ddirext->dx_segs[i]);
		if (mask) {
			value1 += (fx_size_t)fx_popcount(mask);
		}
	}
	value2 = fx_dtoh_u32(ddirext->dx_nsegs);
	fx_assert(value1 == value2);
	if (value1 != value2) {
		return -1;
	}

	value1 = 0;
	for (size_t j = 0; j < nelems(ddirext->dx_dent); ++j) {
		dirent_dtoh(&dirent, &ddirext->dx_dent[j]);
		if (dirent_isused(&dirent)) {
			value1 += 1;
		}
	}
	value2 = fx_dtoh_u32(ddirext->dx_ndents);
	fx_assert(value1 == value2);
	if (value1 != value2) {
		return -1;
	}

	return 0;
}


fx_dirent_t *fx_dirext_lookup(const fx_dirext_t *dirext,
                              fx_hash_t hash, fx_size_t nlen)
{
	return dents_lookup(dirext->dx_dent, nelems(dirext->dx_dent),
	                    dirext->dx_ndents, hash, nlen);
}

fx_dirent_t *fx_dirext_search(const fx_dirext_t *dirext, fx_off_t doff)
{
	return dents_search(dirext->dx_dent, nelems(dirext->dx_dent),
	                    dirext->dx_ndents, FNX_DOFF_BEGINX, doff);
}

fx_dirent_t *fx_dirext_link(fx_dirext_t *dirext, const fx_inode_t *inode)
{
	return dents_insert(dirext->dx_dent, nelems(dirext->dx_dent),
	                    &dirext->dx_ndents, inode);
}

fx_dirent_t *fx_dirext_unlink(fx_dirext_t *dirext, const fx_inode_t *inode)
{
	return dents_remove(dirext->dx_dent, nelems(dirext->dx_dent),
	                    &dirext->dx_ndents, inode);
}

static int dseg_isvalid(fx_off_t dseg)
{
	return ((dseg >= 0) && (dseg < FNX_DIREXT_NSEGS));
}

static fx_bits_t dseg_to_mask(fx_off_t dseg)
{
	return (1u << (dseg % 32));
}

static fx_size_t dseg_to_bpos(fx_off_t dseg)
{
	return ((fx_size_t)dseg / 32);
}

int fx_dirext_isempty(const fx_dirext_t *dirext)
{
	return ((dirext->dx_ndents == 0) && (dirext->dx_nsegs == 0));
}

int fx_dirext_hasseg(const fx_dirext_t *dirext, fx_off_t dseg)
{
	int res = FNX_FALSE;
	const fx_bits_t mask = dseg_to_mask(dseg);
	const fx_size_t bpos = dseg_to_bpos(dseg);

	fx_assert(dseg_isvalid(dseg));
	fx_assert(bpos < nelems(dirext->dx_segs));
	if (dirext->dx_nsegs > 0) {
		res = fx_testf(dirext->dx_segs[bpos], mask);
	}
	return res;
}

void fx_dirext_setseg(fx_dirext_t *dirext, fx_off_t dseg)
{
	const fx_bits_t mask = dseg_to_mask(dseg);
	const fx_size_t bpos = dseg_to_bpos(dseg);

	fx_assert(dseg_isvalid(dseg));
	fx_assert(bpos < nelems(dirext->dx_segs));
	fx_assert(dirext->dx_nsegs < FNX_DIREXT_NSEGS);
	fx_setf(&dirext->dx_segs[bpos], mask);
	dirext->dx_nsegs += 1;
}

void fx_dirext_unsetseg(fx_dirext_t *dirext, fx_off_t dseg)
{
	const fx_bits_t mask = dseg_to_mask(dseg);
	const fx_size_t bpos = dseg_to_bpos(dseg);

	fx_assert(dseg_isvalid(dseg));
	fx_assert(bpos < nelems(dirext->dx_segs));
	fx_assert(dirext->dx_nsegs > 0);
	fx_unsetf(&dirext->dx_segs[bpos], mask);
	dirext->dx_nsegs -= 1;
}

fx_off_t fx_dirext_nextseg(const fx_dirext_t *dirext, fx_off_t dseg)
{
	fx_off_t  ditr;
	fx_size_t bpos;
	fx_bits_t bits, mask;

	ditr = dseg;
	bpos = dseg_to_bpos(ditr);
	while (bpos < nelems(dirext->dx_segs)) {
		bits = dirext->dx_segs[bpos];
		if (bits) {
			mask = dseg_to_mask(ditr);
			while (mask) {
				if (fx_testf(bits, mask)) {
					return ditr;
				}
				ditr = ditr + 1;
				mask = mask << 1;
			}
		}
		bpos = bpos + 1;
		ditr = (fx_off_t)bpos * 32;
	}
	return FNX_DOFF_NONE;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_dirseg_t *fx_vnode_to_dirseg(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_dirseg_t, ds_vnode);
}

void fx_dirseg_init(fx_dirseg_t *dirseg)
{
	fx_vnode_init(&dirseg->ds_vnode, FNX_VTYPE_DIRSEG);
	dents_init(dirseg->ds_dent, nelems(dirseg->ds_dent), 0);
	dirseg->ds_index     = FNX_DOFF_NONE;
	dirseg->ds_ndents   = 0;
	dirseg->ds_magic    = FNX_DIRSEG_MAGIC;
}

void fx_dirseg_destroy(fx_dirseg_t *dirseg)
{
	fx_vnode_destroy(&dirseg->ds_vnode);
	dirseg->ds_index    = FNX_DOFF_NONE;
	dirseg->ds_ndents   = 0;
	dirseg->ds_magic    = 1;
}

void fx_dirseg_setup(fx_dirseg_t *dirseg, fx_off_t dseg)
{
	const fx_off_t doff_base = fx_dseg_to_doff(dseg);

	fx_assert(doff_base >= (FNX_DIRTOP_NDENT + FNX_DIREXT_NDENT));
	dirseg->ds_index = dseg;
	dents_setup(dirseg->ds_dent, nelems(dirseg->ds_dent), doff_base);
}

int fx_dirseg_isempty(const fx_dirseg_t *dirseg)
{
	return (dirseg->ds_ndents == 0);
}


void fx_dirseg_htod(const fx_dirseg_t *dirseg, fx_ddirseg_t *ddirseg)
{
	fx_header_t *hdr = &ddirseg->ds_hdr;

	/* Header & tail */
	fx_vnode_dexport(&dirseg->ds_vnode, hdr);
	fx_dobj_zpad(hdr, FNX_VTYPE_DIRSEG);

	/* R[0] */
	ddirseg->ds_index   = fx_htod_u32((uint32_t)dirseg->ds_index);
	ddirseg->ds_ndents  = (uint8_t)dirseg->ds_ndents;
	ddirseg->ds_magic   = fx_htod_magic(dirseg->ds_magic);

	/* R[1..15] */
	for (size_t i = 0; i < nelems(ddirseg->ds_dent); ++i) {
		dirent_htod(&dirseg->ds_dent[i], &ddirseg->ds_dent[i]);
	}
}


void fx_dirseg_dtoh(fx_dirseg_t *dirseg, const fx_ddirseg_t *ddirseg)
{
	int rc;
	const fx_header_t *hdr = &ddirseg->ds_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_DIRSEG);
	fx_assert(rc == 0);
	fx_vnode_dimport(&dirseg->ds_vnode, hdr);

	/* R[0] */
	dirseg->ds_index    = (fx_off_t)fx_dtoh_u32(ddirseg->ds_index);
	dirseg->ds_ndents   = ddirseg->ds_ndents;

	/* R[1..15] */
	for (size_t i = 0; i < nelems(dirseg->ds_dent); ++i) {
		dirent_dtoh(&dirseg->ds_dent[i], &ddirseg->ds_dent[i]);
	}

	/* Postop */
	fx_dirseg_setup(dirseg, dirseg->ds_index);
}

int fx_dirseg_dcheck(const fx_header_t *hdr)
{
	int rc;
	fx_vtype_e vtype;
	fx_magic_t magic;
	fx_size_t  value;
	fx_dirent_t dirent;
	const fx_ddirseg_t *ddirseg;

	vtype = fx_dobj_vtype(hdr);
	if (vtype != FNX_VTYPE_DIRSEG) {
		return -1;
	}
	rc = fx_dobj_check(hdr, vtype);
	if (rc != 0) {
		fx_assert(0); /* XXX */
		return -1;
	}
	ddirseg = header_to_ddirseg(hdr);
	magic = fx_dtoh_magic(ddirseg->ds_magic);
	fx_assert(magic == FNX_DIRSEG_MAGIC);
	if (magic != FNX_DIRSEG_MAGIC) {
		return -1;
	}
	value = 0;
	for (size_t i = 0; i < nelems(ddirseg->ds_dent); ++i) {
		dirent_dtoh(&dirent, &ddirseg->ds_dent[i]);
		if (dirent_isused(&dirent)) {
			value += 1;
		}
	}
	fx_assert(value == ddirseg->ds_ndents);
	if (value != ddirseg->ds_ndents) {
		return -1;
	}

	/* TODO: Check base */

	return 0;
}


fx_dirent_t *fx_dirseg_lookup(const fx_dirseg_t *dirseg,
                              fx_hash_t hash, fx_size_t nlen)
{
	return dents_lookup(dirseg->ds_dent, nelems(dirseg->ds_dent),
	                    dirseg->ds_ndents, hash, nlen);
}

fx_dirent_t *fx_dirseg_search(const fx_dirseg_t *dirseg, fx_off_t doff)
{
	const fx_off_t doff_base = fx_dseg_to_doff(dirseg->ds_index);

	return dents_search(dirseg->ds_dent, nelems(dirseg->ds_dent),
	                    dirseg->ds_ndents, doff_base, doff);
}

fx_dirent_t *fx_dirseg_link(fx_dirseg_t *dirseg, const fx_inode_t *inode)
{
	return dents_insert(dirseg->ds_dent, nelems(dirseg->ds_dent),
	                    &dirseg->ds_ndents, inode);
}

fx_dirent_t *fx_dirseg_unlink(fx_dirseg_t *dirseg, const fx_inode_t *inode)
{
	return dents_remove(dirseg->ds_dent, nelems(dirseg->ds_dent),
	                    &dirseg->ds_ndents, inode);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Calculate 64-bits hash value for entry-name within directory.
 *
 * TODO: Use directory meta-info to detramian hash-function.
 */
fx_hash_t fx_inamehash(const fx_name_t *name, const fx_dir_t *dir)
{
	const fx_ino_t dino = dir_getino(dir);
	return fx_calc_inamehash(name, dino);
}

fx_dir_t *fx_vnode_to_dir(const fx_vnode_t *vnode)
{
	return fx_inode_to_dir(fx_vnode_to_inode(vnode));
}

fx_dir_t *fx_inode_to_dir(const fx_inode_t *inode)
{
	return fx_container_of(inode, fx_dir_t, d_inode);
}

void fx_dir_init(fx_dir_t *dir)
{
	fx_inode_t *inode = &dir->d_inode;
	fx_staticassert(FNX_DOFF_END > FNX_DIRNCHILDS_MAX);

	fx_inode_init(inode, FNX_VTYPE_DIR);
	dents_init(dir->d_dent, nelems(dir->d_dent), FNX_DOFF_SELF);
	dir->d_magic    = FNX_DIR_MAGIC;
	dir->d_nchilds  = 0;
	dir->d_ndents   = 0;

	inode->i_iattr.i_mode   = FX_DIR_INIT_MODE;
	inode->i_iattr.i_nlink  = FX_DIR_INIT_NLINK;
	inode->i_iattr.i_size   = FX_DDIRSIZE;
}

void fx_dir_destroy(fx_dir_t *dir)
{
	fx_foreach_arrelem(dir->d_dent, dirent_clear);
	fx_inode_destroy(&dir->d_inode);
	dir->d_magic    = 1;
}

void fx_dir_setup(fx_dir_t *dir, const fx_uctx_t *uctx, fx_mode_t mode)
{
	fx_inode_setup(&dir->d_inode, uctx, mode, 0);

	dir_iref_self(dir);
	if (dir_isroot(dir)) {
		fx_dir_iref_parentd(dir, dir);
	}
}

void fx_dir_htod(const fx_dir_t *dir, fx_ddir_t *ddir)
{
	/* R[0..7] */
	fx_inode_htod(&dir->d_inode, &ddir->d_inode);

	/* R[8..63] */
	for (size_t i = 0; i < nelems(ddir->d_dent); ++i) {
		dirent_htod(&dir->d_dent[i], &ddir->d_dent[i]);
	}
	ddir->d_ndents  = fx_htod_u32((uint32_t)dir->d_ndents);
	ddir->d_nchilds = fx_htod_u32((uint32_t)dir->d_nchilds);
}

void fx_dir_dtoh(fx_dir_t *dir, const fx_ddir_t *ddir)
{
	int rc;
	const fx_header_t *hdr = &ddir->d_inode.i_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_DIR);
	fx_assert(rc == 0);

	/* R[0..7] */
	fx_inode_dtoh(&dir->d_inode, &ddir->d_inode);

	/* R[8..63] */
	for (size_t i = 0; i < nelems(dir->d_dent); ++i) {
		dirent_dtoh(&dir->d_dent[i], &ddir->d_dent[i]);
	}
	dir->d_nchilds  = fx_dtoh_u32(ddir->d_nchilds);
	dir->d_ndents   = fx_dtoh_u32(ddir->d_ndents);
}

int fx_dir_dcheck(const fx_header_t *hdr)
{
	int rc;
	fx_size_t value, limit;
	const fx_ddir_t *ddir;

	rc = fx_dobj_check(hdr, FNX_VTYPE_DIR);
	if (rc != 0) {
		return rc;
	}
	rc = fx_inode_dcheck(hdr);
	if (rc != 0) {
		return rc;
	}
	ddir = header_to_ddir(hdr);
	value = fx_dtoh_u32(ddir->d_nchilds);
	limit = FNX_DIRNCHILDS_MAX;
	if (value > limit) {
		fx_warn("illegal-nchilds: d_nchilds=%d limit=%d",
		        (int)value, (int)limit);
		return -1;
	}
	value = fx_dtoh_u32(ddir->d_ndents);
	limit = FNX_DIRTOP_NDENT;
	if ((value > limit) || (value < 2)) {
		fx_warn("illegal-ndents: d_ndents=%d limit=%d", (int)value, (int)limit);
		return -1;
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_dirent_t *self_dirent(const fx_dir_t *dir)
{
	const fx_dirent_t *dirent = &dir->d_dent[FNX_DOFF_SELF];
	return (fx_dirent_t *)dirent;
}

static fx_dirent_t *parent_dirent(const fx_dir_t *dir)
{
	const fx_dirent_t *dirent = &dir->d_dent[FNX_DOFF_PARENT];
	return (fx_dirent_t *)dirent;
}

static void dir_iref(fx_dir_t *dir, fx_dirent_t *dent, const fx_inode_t *inode)
{
	fx_assert(dir->d_ndents < nelems(dir->d_dent));
	dirent_assign(dent, inode);
	dir->d_ndents += 1;
}

static void dir_iref_self(fx_dir_t *dir)
{
	dir_iref(dir, self_dirent(dir), dir_inode(dir));
}

void fx_dir_iref_parentd(fx_dir_t *dir, const fx_dir_t *parentd)
{
	dir_iref(dir, parent_dirent(dir), dir_inode(parentd));
}

void fx_dir_unref_parentd(fx_dir_t *dir)
{
	fx_assert(dir->d_ndents > 0);
	dirent_clear(parent_dirent(dir));
	dir->d_ndents -= 1;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Lookup for special-dirent */
const fx_dirent_t *fx_dir_meta(const fx_dir_t *dir, const fx_name_t *name)
{
	const fx_dirent_t *dirent;

	/* Map "." to self */
	dirent = self_dirent(dir);
	if (fx_name_isnequal(name, ".", 1)) {
		return dirent;
	}
	/* Map ".." to parent */
	dirent = parent_dirent(dir);
	if (fx_name_isnequal(name, "..", 2)) {
		return dirent;
	}
	return NULL;
}

fx_dirent_t *fx_dir_lookup(const fx_dir_t *dir, fx_hash_t hash, size_t nlen)
{
	fx_dirent_t *dirent  = NULL;
	const size_t beg = FNX_DOFF_BEGIN;

	if (!dir_isempty(dir)) {
		dirent = dents_lookup(dir->d_dent + beg, nelems(dir->d_dent) - beg,
		                      dir->d_ndents, hash, nlen);
	}
	return dirent;
}

const fx_dirent_t *fx_dir_search(const fx_dir_t *dir, fx_off_t doff)
{
	const fx_dirent_t *dirent;
	const size_t beg = FNX_DOFF_BEGIN;

	if (doff == FNX_DOFF_SELF) {
		dirent = self_dirent(dir);
	} else if (doff == FNX_DOFF_PARENT) {
		dirent = parent_dirent(dir);
	} else {
		dirent = dents_search(dir->d_dent + beg, nelems(dir->d_dent) - beg,
		                      dir->d_ndents, (fx_off_t)beg, doff);
	}
	return dirent;
}

fx_dirent_t *fx_dir_link(fx_dir_t *dir, const fx_inode_t *inode)
{
	const size_t beg = FNX_DOFF_BEGIN;

	return dents_insert(dir->d_dent + beg, nelems(dir->d_dent) - beg,
	                    &dir->d_ndents, inode);
}

fx_dirent_t *fx_dir_unlink(fx_dir_t *dir, const fx_inode_t *inode)
{
	const size_t beg = FNX_DOFF_BEGIN;

	return dents_remove(dir->d_dent + beg, nelems(dir->d_dent) - beg,
	                    &dir->d_ndents, inode);
}

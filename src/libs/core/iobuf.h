/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_IOBUF_H_
#define FUNEX_IOBUF_H_

/* Forward declarations */
struct fx_balloc;



/* Range of data segments + associated blocks */
struct fx_iobuffer {
	fx_ino_t        ino;
	fx_segmap_t     sgm;
	fx_bkref_t     *bks[FNX_SEGNBK];
	struct fx_balloc *alloc;
};
typedef struct fx_iobuffer fx_iobuf_t;


/* I/O Buffers over continous range */
struct fx_iobufs {
	fx_iobuf_t iob[2];
};
typedef struct fx_iobufs fx_iobufs_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_bkref_init(fx_bkref_t *);

void fx_bkref_destroy(fx_bkref_t *);

void fx_bkref_setup(fx_bkref_t *, const fx_bkaddr_t *, void *);

void fx_bkref_merge(fx_bkref_t *, fx_off_t, fx_size_t, const fx_bkref_t *);

void fx_bkref_clone(const fx_bkref_t *, fx_bkref_t *);

fx_bkcnt_t fx_bkref_numbk(const fx_bkref_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_iobufs_init(fx_iobufs_t *);

void fx_iobufs_destroy(fx_iobufs_t *);

void fx_iobufs_setup(fx_iobufs_t *, fx_ino_t, struct fx_balloc *);

void fx_iobufs_release(fx_iobufs_t *);

fx_off_t fx_iobufs_end(const fx_iobufs_t *);

int fx_iobufs_assign(fx_iobufs_t *, fx_off_t, fx_size_t, const void *);

int fx_iobufs_mkiovec(const fx_iobufs_t *, fx_iovec_t *, size_t, size_t *);

#endif /* FUNEX_IOBUF_H_ */


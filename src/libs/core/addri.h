/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_ADDRI_H_
#define FUNEX_ADDRI_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline fx_size_t size_max(fx_size_t sz1, fx_size_t sz2)
{
	return ((sz1 > sz2) ? sz1 : sz2);
}

static inline fx_size_t size_min(fx_size_t sz1, fx_size_t sz2)
{
	return ((sz1 < sz2) ? sz1 : sz2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline fx_off_t off_max(fx_off_t off1, fx_off_t off2)
{
	return ((off1 > off2) ? off1 : off2);
}

static inline fx_off_t off_min(fx_off_t off1, fx_off_t off2)
{
	return ((off1 < off2) ? off1 : off2);
}

static inline fx_off_t off_floor(fx_off_t val, fx_size_t step)
{
	return ((val / (fx_off_t)step) * (fx_off_t)step);
}

static inline fx_size_t off_len(fx_off_t start, fx_off_t end)
{
	return (fx_size_t)(end - start);
}

static inline fx_off_t off_end(fx_off_t off, fx_size_t len)
{
	return (off + (fx_off_t)len);
}

static inline fx_size_t off_min_len(fx_off_t beg, fx_off_t end1, fx_off_t end2)
{
	return size_min(off_len(beg, end1), off_len(beg, end2));
}

static inline fx_off_t off_floor_blk(fx_off_t off)
{
	return off_floor(off, FNX_BLKSIZE);
}

static inline fx_off_t off_ceil_blk(fx_off_t off)
{
	return off_floor_blk(off_end(off, FNX_BLKSIZE));
}

static inline fx_off_t off_floor_seg(fx_off_t off)
{
	return off_floor(off, FNX_SEGSIZE);
}

static inline fx_off_t off_ceil_seg(fx_off_t off)
{
	return off_floor_seg(off_end(off, FNX_SEGSIZE));
}

static inline fx_off_t off_floor_slice(fx_off_t off)
{
	return off_floor(off, FNX_SLICESIZE);
}

static inline fx_off_t off_ceil_slice(fx_off_t off)
{
	return off_floor_slice(off_end(off, FNX_SLICESIZE));
}

static inline int off_isseg0(fx_off_t off)
{
	return ((off >= 0) && (off < FNX_SEGSIZE));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline fx_lba_t lba_end(fx_lba_t lba, fx_bkcnt_t cnt)
{
	return lba + cnt;
}

static inline fx_lba_t lba_floor(fx_lba_t val, fx_size_t step)
{
	return ((val / (fx_lba_t)step) * (fx_lba_t)step);
}

static inline fx_lba_t lba_floor_spseg(fx_lba_t lba)
{
	return lba_floor(lba, FNX_SPCNBK);
}

static inline fx_bkcnt_t bkcnt_max(fx_bkcnt_t cnt1, fx_bkcnt_t cnt2)
{
	return ((cnt1 > cnt2) ? cnt1 : cnt2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline fx_size_t nbk_to_sec(fx_bkcnt_t nbk)
{
	return (fx_size_t)(nbk / FNX_SECNBK);
}

static inline fx_size_t lba_to_sec(fx_lba_t lba)
{
	return (fx_size_t)(lba / FNX_SECNBK);
}

static inline fx_lba_t sec_to_lba(fx_size_t sec_idx)
{
	return (fx_lba_t)(sec_idx * FNX_SECNBK);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline void bkaddr_reset(fx_bkaddr_t *bkaddr)
{
	bkaddr->svol = FNX_SVOL_NULL;
	bkaddr->lba  = FNX_LBA_NULL;
	bkaddr->frg  = 0;
}

static inline void bkaddr_copy(fx_bkaddr_t *tgt, const fx_bkaddr_t *src)
{
	tgt->svol = src->svol;
	tgt->lba = src->lba;
	tgt->frg = src->frg;
}

static inline int bkaddr_isequal(const fx_bkaddr_t *ba1, const fx_bkaddr_t *ba2)
{
	return ((ba1->lba == ba2->lba) && (ba1->svol == ba2->svol) &&
	        (ba1->frg == ba2->frg));
}

static inline int bkaddr_isnull(const fx_bkaddr_t *bkaddr)
{
	return ((bkaddr->lba == FNX_LBA_NULL) ||
	        (bkaddr->svol == FNX_SVOL_NULL));
}

static inline int bkaddr_issuper(const fx_bkaddr_t *bkaddr)
{
	const fx_size_t sec = lba_to_sec(bkaddr->lba);
	return ((bkaddr->svol == FNX_SVOL_META) && (sec == FNX_SEC_SUPER));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline void vaddr_init(fx_vaddr_t *vaddr, fx_vtype_e vtype)
{
	vaddr->ino    = FNX_INO_NULL;
	vaddr->xno    = FNX_XNO_NULL;
	vaddr->vtype  = vtype;
}

static inline void vaddr_destroy(fx_vaddr_t *vaddr)
{
	vaddr->ino    = FNX_INO_NULL;
	vaddr->xno    = FNX_XNO_NULL;
	vaddr->vtype  = FNX_VTYPE_NONE;
}

static inline int vaddr_isequal(const fx_vaddr_t *x, const fx_vaddr_t *y)
{
	return (x->ino == y->ino) && (x->xno == y->xno) && (x->vtype == y->vtype);
}

static inline void
vaddr_setup(fx_vaddr_t *vaddr, fx_vtype_e vtype, fx_ino_t ino, fx_xno_t xno)
{
	vaddr->ino   = ino;
	vaddr->xno   = xno;
	vaddr->vtype = vtype;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * V-address generators: for each sub-type, creates unique address as a tuple
 * of (type, ino, xno).
 */

static inline void vaddr_for_super(fx_vaddr_t *vaddr)
{
	vaddr_setup(vaddr, FNX_VTYPE_SUPER, FNX_INO_SUPER, FNX_SVOL_META);
}

static inline void vaddr_for_spmap(fx_vaddr_t *vaddr, fx_lba_t ulba)
{
	fx_lba_t lba;

	lba = lba_floor_spseg(ulba);
	vaddr_setup(vaddr, FNX_VTYPE_SPMAP, FNX_INO_SPACE, (fx_xno_t)lba);
}

static inline void
vaddr_for_inode(fx_vaddr_t *vaddr, fx_vtype_e vtype, fx_ino_t ino)
{
	vaddr_setup(vaddr, vtype, ino, FNX_XNO_NULL);
}

static inline void
vaddr_for_anyino(fx_vaddr_t *vaddr, fx_ino_t ino)
{
	vaddr_for_inode(vaddr, FNX_VTYPE_ANY, ino);
}

static inline void
vaddr_for_dirext(fx_vaddr_t *vaddr, fx_ino_t ino)
{
	vaddr_setup(vaddr, FNX_VTYPE_DIREXT, ino, FNX_XNO_NULL);
}

static inline void
vaddr_for_dirseg(fx_vaddr_t *vaddr, fx_ino_t ino, fx_off_t dseg)
{
	vaddr_setup(vaddr, FNX_VTYPE_DIRSEG, ino, (fx_xno_t)dseg);
}

static inline void
vaddr_for_segmnt(fx_vaddr_t *vaddr, fx_ino_t ino, fx_off_t off)
{
	fx_off_t loff;

	loff = off_floor_seg(off);
	vaddr_setup(vaddr, FNX_VTYPE_SEGMNT, ino, (fx_xno_t)loff);
}

static inline void
vaddr_for_slice(fx_vaddr_t *vaddr, fx_ino_t ino, fx_off_t off)
{
	fx_off_t loff;

	loff = off_floor_slice(off);
	vaddr_setup(vaddr, FNX_VTYPE_SLICE, ino, (fx_xno_t)loff);
}

#endif /* FUNEX_ADDRI_H_ */


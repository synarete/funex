/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_FILE_H_
#define FUNEX_FILE_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* File Reference */
fx_fileref_t *fx_link_to_fileref(const fx_link_t *);

void fx_fileref_check(const fx_fileref_t *);

void fx_fileref_init(fx_fileref_t *);

void fx_fileref_destroy(fx_fileref_t *);

void fx_fileref_setup(fx_fileref_t *, fx_flags_t);

void fx_fileref_attach(fx_fileref_t *, fx_inode_t *);

fx_inode_t *fx_fileref_detach(fx_fileref_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Regular-file */
fx_inode_t *fx_vnode_to_regfile(const fx_vnode_t *);

void fx_regfile_init(fx_inode_t *);

void fx_regfile_destroy(fx_inode_t *);

void fx_regfile_htod(const fx_inode_t *, fx_dinode_t *);

void fx_regfile_dtoh(fx_inode_t *, const fx_dinode_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* File's slice */
fx_slice_t *fx_vnode_to_slice(const fx_vnode_t *);

void fx_slice_init(fx_slice_t *);

void fx_slice_destroy(fx_slice_t *);

void fx_slice_htod(const fx_slice_t *, fx_dslice_t *);

void fx_slice_dtoh(fx_slice_t *, const fx_dslice_t *);

int fx_slice_dcheck(const fx_header_t *);

void fx_slice_markseg(fx_slice_t *, fx_off_t);

void fx_slice_unmarkseg(fx_slice_t *, fx_off_t);

int fx_slice_testseg(const fx_slice_t *, fx_off_t);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* File's sub-segment */
fx_segmnt_t *fx_vnode_to_segmnt(const fx_vnode_t *);

void fx_segmnt_init(fx_segmnt_t *);

void fx_segmnt_destroy(fx_segmnt_t *);

int fx_segmnt_setup(fx_segmnt_t *, fx_ino_t, fx_off_t);

void fx_segmnt_htod(const fx_segmnt_t *, fx_dsegmnt_t *);

void fx_segmnt_dtoh(fx_segmnt_t *, const fx_dsegmnt_t *);

int fx_segmnt_dcheck(const fx_header_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Symlink */
fx_symlnk_t *fx_vnode_to_symlnk(const fx_vnode_t *);

fx_symlnk_t *fx_inode_to_symlnk(const fx_inode_t *);

void fx_symlnk_init(fx_symlnk_t *, int);

void fx_symlnk_destroy(fx_symlnk_t *);

void fx_symlnk_setup(fx_symlnk_t *, const fx_uctx_t *, const fx_path_t *);

void fx_symlnk_htod(const fx_symlnk_t *, fx_dsymlnk_t *);

void fx_symlnk_dtoh(fx_symlnk_t *, const fx_dsymlnk_t *);

void fx_ssymlnk_htod(const fx_symlnk_t *, fx_dssymlnk_t *);

void fx_ssymlnk_dtoh(fx_symlnk_t *, const fx_dssymlnk_t *);

int fx_symlnk_dcheck(const fx_header_t *);


#endif /* FUNEX_FILE_H_ */




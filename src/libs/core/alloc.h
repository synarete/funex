/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_ALLOC_H_
#define FUNEX_ALLOC_H_

/* Forward declaration */
struct fx_bkref;
struct fx_bkaddr;
struct fx_slab;

/* Allocator's per-elements bucket */
struct fx_bucket {
	fx_size_t   esize;      /* Element's size */
	fx_size_t   limit;      /* Upper limit of allowed elements */
	fx_size_t   total;      /* Total number of used */
	fx_size_t   pivot;      /* Ref-point for internal store */
	fx_size_t   inter;      /* Number of privately stored */
	fx_slink_t  head;       /* Free list's head */
};
typedef struct fx_bucket fx_bucket_t;


/* Blocks allocator */
struct fx_balloc {
	fx_mutex_t  mutex;
	fx_bucket_t blks;
	fx_bucket_t secs;
	fx_bucket_t bkrefs;
};
typedef struct fx_balloc fx_balloc_t;


/* V-Elements allocator */
struct fx_valloc {
	fx_malloc_t     alloc;
	fx_magic_t      magic;
	struct fx_slab *pools[8];
};
typedef struct fx_valloc fx_valloc_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


/* Raw memory allocation via mmap */
void *fx_mmap_bks(size_t);

void fx_munmap_bks(void *, size_t);


/* Blocks allocator */
void fx_balloc_init(fx_balloc_t *);

void fx_balloc_destroy(fx_balloc_t *);

void fx_balloc_clear(fx_balloc_t *);


struct fx_bkref *fx_balloc_new_sec(fx_balloc_t *, const struct fx_bkaddr *);

struct fx_bkref *fx_balloc_new_bk(fx_balloc_t *, const struct fx_bkaddr *);

void fx_balloc_del_bk(fx_balloc_t *, struct fx_bkref *);


/* V-elements allocator */
void fx_valloc_init(fx_valloc_t *);

void fx_valloc_destroy(fx_valloc_t *);

void fx_valloc_clear(fx_valloc_t *);


#endif /* FUNEX_ALLOC_H_ */

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_STATS_H_
#define FUNEX_STATS_H_

struct fx_dextent;
struct fx_dvoldsc;
struct fx_dlayout;
struct fx_diattr;
struct fx_dfsattr;
struct fx_dfsstat;
struct fx_dvstat;


/* Blocks-region descriptor */
struct fx_extent {
	fx_lba_t    lba;
	fx_bkcnt_t  cnt;
};
typedef struct fx_extent fx_extent_t;


/* Space layout descriptor */
struct fx_layout {
	fx_extent_t     l_super;        /* Heading super blocks */
	fx_extent_t     l_index;        /* Index table */
	fx_extent_t     l_extra;        /* Extra reserved space */
	fx_extent_t     l_udata;        /* User's data blocks */
	fx_extent_t     l_trail;        /* Trailing (unused) blocks */
};
typedef struct fx_layout fx_layout_t;


/* File-system's general attributes (set upon mkfs|mount) */
struct fx_fsattr {
	fx_uuid_t       f_fsuuid;       /* File-system's UUID */
	fx_fstype_t     f_fstype;       /* File-system type (FNX_FSID) */
	fx_version_t    f_version;      /* Version number (FNX_FSVERSION) */
	fx_size_t       f_gen;          /* Generation number */
	fx_zid_t        f_zid;          /* Zone-ID (unused) */
	fx_uid_t        f_uid;          /* Creator UID */
	fx_gid_t        f_gid;          /* Creator GID */
	fx_ino_t        f_rootino;      /* Root ino number (FNX_INO_ROOT) */
	fx_mntf_t       f_mntf;         /* Mount options bits-mask */
};
typedef struct fx_fsattr fx_fsattr_t;


/* File-systems global counters (exported via statvfs(2)) */
struct fx_fsstat {
	fx_filcnt_t     f_inodes;       /* Total number of available inodes */
	fx_filcnt_t     f_ifree;        /* Total number of free inodes */
	fx_filcnt_t     f_iavail;       /* Free inodes for unprivileged users */
	fx_ino_t        f_iapex;        /* Top-most ino number */

	fx_bkcnt_t      f_blocks;       /* Data capacity in block-units */
	fx_bkcnt_t      f_bfree;        /* Total number of free blocks */
	fx_bkcnt_t      f_bavail;       /* Free blocks for unprivileged users */
	fx_lba_t        f_bnext;        /* Next to-be-used block address */

	fx_bkcnt_t      f_pblocks;      /* Potential data-blocks capacity */
	fx_bkcnt_t      f_pfree;        /* Total number of potential free blocks */
	fx_bkcnt_t      f_pavail;       /* Potential free blocks for unprivileged */
	fx_lba_t        f_pnext;        /* Next to-be-used virtual block address */
};
typedef struct fx_fsstat fx_fsstat_t;


/* Meta-data sub-elements accounting */
struct fx_vstats {
	fx_magic_t      v_magic; /* Keep first (for offsets-table) */
	fx_size_t       v_super;
	fx_size_t       v_spmap;
	fx_size_t       v_dir;
	fx_size_t       v_dirext;
	fx_size_t       v_dirseg;
	fx_size_t       v_symlnk;
	fx_size_t       v_reflnk;
	fx_size_t       v_special;
	fx_size_t       v_regfile;
	fx_size_t       v_slice;
	fx_size_t       v_segmnt;
};
typedef struct fx_vstats fx_vstats_t;


/* Read/Write counters */
struct fx_iostat {
	fx_size_t       io_nread;       /* Total read bytes */
	fx_size_t       io_nwrite;      /* Total write bytes */
};
typedef struct fx_iostat fx_iostat_t;


/* Operations counters */
struct fx_opstat {
	fx_size_t       op_none;
	fx_size_t       op_lookup;
	fx_size_t       op_forget;
	fx_size_t       op_getattr;
	fx_size_t       op_setattr;
	fx_size_t       op_readlink;
	fx_size_t       op_mknod;
	fx_size_t       op_mkdir;
	fx_size_t       op_unlink;
	fx_size_t       op_rmdir;
	fx_size_t       op_symlink;
	fx_size_t       op_rename;
	fx_size_t       op_link;
	fx_size_t       op_open;
	fx_size_t       op_read;
	fx_size_t       op_write;
	fx_size_t       op_flush;
	fx_size_t       op_release;
	fx_size_t       op_fsync;
	fx_size_t       op_opendir;
	fx_size_t       op_readdir;
	fx_size_t       op_releasedir;
	fx_size_t       op_fsyncdir;
	fx_size_t       op_statfs;
	fx_size_t       op_access;
	fx_size_t       op_create;
	fx_size_t       op_bmap;
	fx_size_t       op_interrupt;
	fx_size_t       op_poll;
	fx_size_t       op_truncate;
	fx_size_t       op_fallocate;
	fx_size_t       op_fpunch;
	fx_size_t       op_stats;
	fx_size_t       op_xcopy;
	fx_size_t       op_last;
};
typedef struct fx_opstat  fx_opstat_t;


/* File-system's entire stats in single bundle */
struct fx_fsinfo {
	fx_fsattr_t     attr;           /* Global FS attributes */
	fx_fsstat_t     fsst;           /* Blocks/inodes accounting */
	fx_vstats_t     vsts;           /* Metadata accounting */
	fx_iostat_t     iost;           /* I/O counters */
	fx_opstat_t     opst;           /* Operations counters */
};
typedef struct fx_fsinfo    fx_fsinfo_t;


/* Inode's attributes (stats) */
struct fx_iattr {
	fx_ino_t        i_ino;          /* I-node number */
	fx_size_t       i_gen;          /* Generation number */
	fx_dev_t        i_dev;          /* Device */
	fx_mode_t       i_mode;         /* File mode */
	fx_nlink_t      i_nlink;        /* Link count */
	fx_uid_t        i_uid;          /* User ID */
	fx_gid_t        i_gid;          /* Group ID */
	fx_dev_t        i_rdev;         /* Device ID if special */
	fx_off_t        i_size;         /* Logical size (bytes) */
	fx_bkcnt_t      i_bcap;         /* Blocks capacity */
	fx_bkcnt_t      i_nblk;         /* Number in-use blocks */
	fx_size_t       i_wops;         /* Number of write ops */
	fx_size_t       i_wcnt;         /* Total writen bytes */
	fx_times_t      i_times;        /* Time-stamps */
};
typedef struct fx_iattr    fx_iattr_t;


/* Cache internals stats & limits */
struct fx_cstats {
	fx_size_t       c_frefs;
	fx_size_t       c_inodes;
	fx_size_t       c_vnodes;
	fx_size_t       c_vdirty;
	fx_size_t       c_vlru;
	fx_size_t       c_sblocks;
	fx_size_t       c_sdirty;
	fx_size_t       c_slru;
	fx_size_t       c_ublocks;
	fx_size_t       c_udirty;
	fx_size_t       c_ulru;
	fx_size_t       c_ufree;
	fx_size_t       c_sfree;
};
typedef struct fx_cstats    fx_cstats_t;

struct fx_climits {
	fx_size_t       c_pages_lim;
	fx_size_t       c_frefs_lim;
	fx_size_t       c_vinode_lim;
	fx_size_t       c_vdirty_lim;
	fx_size_t       c_sblock_lim;
	fx_size_t       c_sdirty_lim;
	fx_size_t       c_ublock_lim;
	fx_size_t       c_udirty_lim;
	fx_size_t       c_thresh_max;
};
typedef struct fx_climits   fx_climits_t;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Convert opcode-enum to counter */
fx_size_t *fx_opstat_getcnt(fx_opstat_t *, int);

/* Calculates inodes-accounting for meta-data volume of nsecs */
void fx_fill_icounts(fx_fsstat_t *, fx_size_t nsecs);

/* Update blocks-accounting for extra volume of nbks */
void fx_extend_bkcounts(fx_fsstat_t *, fx_bkcnt_t nbks);


/* Volumes' definition */
void fx_layout_init(fx_layout_t *);

void fx_layout_destroy(fx_layout_t *);

void fx_layout_htod(const fx_layout_t *, fx_dlayout_t *);

void fx_layout_dtoh(fx_layout_t *, const fx_dlayout_t *);

void fx_layout_devise(fx_layout_t *, fx_bkcnt_t);


/* File-system attributes & stats */
void fx_fsattr_init(fx_fsattr_t *);

void fx_fsattr_setup(fx_fsattr_t *, const fx_uuid_t *, fx_uid_t, fx_gid_t);

void fx_fsattr_htod(const fx_fsattr_t *, fx_dfsattr_t *);

void fx_fsattr_dtoh(fx_fsattr_t *, const fx_dfsattr_t *);

void fx_fsstat_init(fx_fsstat_t *);

void fx_fsstat_devise(fx_fsstat_t *, fx_bkcnt_t, fx_bkcnt_t);

void fx_fsstat_htod(const fx_fsstat_t *, fx_dfsstat_t *);

void fx_fsstat_dtoh(fx_fsstat_t *, const fx_dfsstat_t *);


void fx_vstats_htod(const fx_vstats_t *, fx_dvstats_t *);

void fx_vstats_dtoh(fx_vstats_t *, const fx_dvstats_t *);

fx_size_t *fx_vstats_getcnt(fx_vstats_t *, int);


/* I-node attributres */
void fx_iattr_init(fx_iattr_t *);

void fx_iattr_destroy(fx_iattr_t *);

void fx_iattr_copy(fx_iattr_t *, const fx_iattr_t *);

void fx_iattr_htod(const fx_iattr_t *, fx_diattr_t *);

void fx_iattr_dtoh(fx_iattr_t *, const fx_diattr_t *);


#endif /* FUNEX_STATS_H_ */




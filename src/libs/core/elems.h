/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_ELEMS_H_
#define FUNEX_ELEMS_H_


/* Forward declarations */
struct fx_vnode;
struct fx_inode;
struct fx_imeta;
struct fx_server;


/* Element's caching queue */
enum FX_STAIN_TYPE {
	FX_STAIN_NONE,
	FX_STAIN_LRU,
	FX_STAIN_DIRTY,
};
typedef enum FX_STAIN_TYPE fx_stain_e;


/* Execution object-type */
enum FX_XTYPE {
	FX_XTYPE_NONE,
	FX_XTYPE_TASK,
	FX_XTYPE_RDBK,
	FX_XTYPE_WRBK,
	FX_XTYPE_WSEC,
};
typedef enum FX_XTYPE  fx_xtype_e;


/* Block-reference control flags */
enum FX_BKREF_FLAGS {
	FX_BKREF_CACHED  = 0x01,        /* Stored in cache */
	FX_BKREF_RDTRANS = 0x02,        /* In read transition */
	FX_BKREF_WRTRANS = 0x04,        /* In write transition */
};


/* Execution element */
struct fx_xelem {
	fx_xtype_e      xtype;          /* Execution-object type */
	fx_status_t     status;         /* Operation status */
	fx_link_t       qlink;          /* Execution-queue link */
	fx_link_t       plink;          /* Pending-queue link */
};
typedef struct fx_xelem fx_xelem_t;


/* Block reference */
struct fx_bkref {
	fx_xelem_t      xelem;          /* Execution-queue link */
	fx_link_t       clink;          /* Cache LRU/dirty link */
	fx_vtype_e      vtype;          /* Object's type */
	fx_bkaddr_t     addr;           /* "Physical" address */
	fx_ino_t        ino;            /* Current owner ino */
	fx_size_t       refcnt;
	fx_stain_e      stain;
	signed int      flags;
	void            *blk;
	struct fx_bkref *next;
	struct fx_bkref *hlnk;
	fx_magic_t      magic;
};
typedef struct fx_bkref fx_bkref_t;


/* Block descriptor */
struct fx_bkdsc {
	fx_size_t       refs;           /* Number of file (clones) refs */
	unsigned char   flags;          /* Control flags */
};
typedef struct fx_bkdsc fx_bkdsc_t;


/* V-node */
struct fx_vnode {
	fx_vtype_e      v_vtype;        /* V-object's type */
	fx_link_t       v_clink;        /* Cache LRU/dirty link */
	fx_link_t       v_mlink;        /* Modified-queue link */
	fx_vaddr_t      v_vaddr;        /* Virtual-address (lookup key) */
	fx_bkaddr_t     v_bkaddr;       /* On-disk reference */
	fx_size_t       v_refcnt;       /* Num open refs */
	fx_stain_e      v_stain;        /* Cache queue ref (ditry/LRU) */
	fx_bool_t       v_cached;
	fx_bool_t       v_stable;
	fx_bool_t       v_removed;
	fx_bool_t       v_pseudo;
	fx_magic_t      v_magic;
	struct fx_vnode *v_hlnk;        /* Caching hash-link */
};
typedef struct fx_vnode fx_vnode_t;


/* Filesystem super-block */
struct fx_super_block {
	fx_vnode_t      su_vnode;
	fx_uuid_t       su_uuid;        /* Volume UUID (internal) */
	fx_flags_t      su_flags;       /* Control flags */
	fx_name_t       su_name;        /* Name (FS-label) */
	fx_name_t       su_uref;        /* User reference-name */
	fx_layout_t     su_layout;      /* Volumes' definition */
	fx_fsattr_t     su_fsattr;      /* Global FS attributes */
	fx_fsstat_t     su_fsstat;      /* Blocks/inodes accounting */
	fx_vstats_t     su_vstats;       /* Metadata accounting */
	fx_iostat_t     su_iostat;      /* I/O counters */
	fx_opstat_t     su_opstat;      /* Operations counters */
	fx_times_t      su_times;       /* Time stamps */
	fx_bool_t       su_active;      /* Is active fs? */
	fx_magic_t      su_magic;
};
typedef struct fx_super_block fx_super_t;


/* Space-block descriptor */
struct fx_spmap {
	fx_vnode_t      sp_vnode;
	fx_lba_t        sp_lba0;
	fx_bkcnt_t      sp_used;
	fx_bkdsc_t      sp_bkd[FNX_SPCNBK];
};
typedef struct fx_spmap fx_spmap_t;


/* I-node */
struct fx_inode {
	fx_vnode_t      i_vnode;        /* V-node base class */
	fx_iattr_t      i_iattr;        /* Attributes set */
	fx_flags_t      i_flags;        /* Control flags */
	fx_ino_t        i_refino;       /* Delegated hard-link */
	fx_ino_t        i_parentd;      /* Parent-dir ino */
	fx_hfunc_e      i_nhfunc;       /* Name's hash-function */
	fx_hfunc_e      i_vhfunc;       /* Meta-data hash-function */
	fx_name_t       i_name;         /* Name string */
	fx_hash_t       i_nhash;        /* Name's hash-value */
	fx_magic_t      i_magic;        /* Debug checker */
	fx_size_t       i_tlen;         /* Trailing length */
	fx_super_t     *i_super;        /* Back-ref to super-block */
	struct fx_server *i_server;     /* Pseudo-inode link-to-serve */
	const struct fx_imeta  *i_meta; /* Pseudo-inode meta info */
};
typedef struct fx_inode fx_inode_t;


/* Open file reference */
struct fx_fileref {
	fx_inode_t     *f_inode;
	fx_link_t       f_link;
	fx_bool_t       f_cached;
	fx_bool_t       f_readable;
	fx_bool_t       f_writeable;
	fx_bool_t       f_nonseekable;
	fx_bool_t       f_noatime;
	fx_bool_t       f_sync;
	fx_bool_t       f_direct_io;
	fx_bool_t       f_keep_cache;
	fx_magic_t      f_magic;
};
typedef struct fx_fileref fx_fileref_t;


/* Directory-entry for mapping name-->ino */
struct fx_dirent {
	fx_off_t        de_doff;   /* Directory offset */
	fx_hash_t       de_hash;   /* Name's hash value */
	fx_size_t       de_nlen;   /* Name's length */
	fx_ino_t        de_ino;    /* Mapped inode number */
};
typedef struct fx_dirent fx_dirent_t;


/* Directory top i-node */
struct fx_dir {
	fx_inode_t      d_inode;
	fx_size_t       d_nchilds;      /* Total children */
	fx_size_t       d_ndents;       /* Top-level dentries */
	fx_dirent_t     d_dent[FNX_DIRTOP_NDENT];
	fx_magic_t      d_magic;
};
typedef struct fx_dir fx_dir_t;


/* Directory extension node */
struct fx_dirext {
	fx_vnode_t      dx_vnode;
	fx_size_t       dx_ndents;
	fx_size_t       dx_nsegs;
	fx_dirent_t     dx_dent[FNX_DIREXT_NDENT];
	fx_bits_t       dx_segs[FNX_DIREXT_NSEGS / 32];
	fx_magic_t      dx_magic;
};
typedef struct fx_dirext fx_dirext_t;


/* Directory sub-segment node */
struct fx_dirseg {
	fx_vnode_t      ds_vnode;
	fx_off_t        ds_index;
	fx_size_t       ds_ndents;
	fx_dirent_t     ds_dent[FNX_DIRSEG_NDENT];
	fx_magic_t      ds_magic;
};
typedef struct fx_dirseg fx_dirseg_t;


/* Symbolic-link node */
struct fx_symlnk {
	fx_inode_t      sl_inode;
	fx_path_t       sl_value;
	fx_magic_t      sl_magic;
};
typedef struct fx_symlnk fx_symlnk_t;


/* Regular-file slice's bitmap */
struct fx_slice {
	fx_vnode_t      sc_vnode;
	fx_size_t       sc_count;
	uint32_t        sc_bitmap[FNX_SLICENSEG / 32];
};
typedef struct fx_slice fx_slice_t;


/* Regular-file segments-range address-mapping */
struct fx_segmnt {
	fx_vnode_t      sg_vnode;
	fx_segmap_t     sg_segmap;
	fx_magic_t      sg_magic;
};
typedef struct fx_segmnt fx_segmnt_t;


/* Meta information & ops associated with pseudo-inodes */
struct fx_imeta {
	const char *name;
	fx_mode_t   mode;
	int (*show)(fx_inode_t *, void *, size_t, size_t *);
	int (*save)(fx_inode_t *, const void *, size_t);
};
typedef struct fx_imeta fx_imeta_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

struct fx_vinfo_ {
	const char  *name;
	unsigned int size;
	unsigned int dsize;

	void (*init)(fx_vnode_t *);
	void (*destroy)(fx_vnode_t *);
	void (*dexport)(const fx_vnode_t *, fx_header_t *);
	void (*dimport)(fx_vnode_t *, const fx_header_t *);
	int (*dcheck)(const fx_header_t *);
};
typedef struct fx_vinfo_ fx_vinfo_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


void fx_verify_core(void);

const fx_vinfo_t *fx_get_vinfo(fx_vtype_e);

int fx_check_dobj(const fx_header_t *, fx_vtype_e);


void fx_init_vobj(fx_vnode_t *, fx_vtype_e);

void fx_destroy_vobj(fx_vnode_t *);

void fx_dexport_vobj(const fx_vnode_t *, fx_header_t *);

void fx_dimport_vobj(fx_vnode_t *, const fx_header_t *);

fx_vnode_t *fx_new_vobj(fx_vtype_e, fx_malloc_t *);

void fx_del_vobj(fx_vnode_t *, fx_malloc_t *);


#endif /* FUNEX_ELEMS_H_ */


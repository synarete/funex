/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_BLOBS_H_
#define FUNEX_BLOBS_H_

#define FX_BLOBHDRS     (8)
#define FX_BLOBSIZE     (FNX_SEGSIZE)
#define FX_SLABPADS     (FX_SLABSIZE - FX_SLABHDSZ)
#define FX_NBLOBS(t)    ((FX_BLOBSIZE - FX_BLOBHDRS) / sizeof(t))


typedef fx_spmap_t      fx_spmap_blobs_t[FX_NBLOBS(fx_spmap_t)];
typedef fx_inode_t      fx_inode_blobs_t[FX_NBLOBS(fx_inode_t)];
typedef fx_dir_t        fx_dir_blobs_t[FX_NBLOBS(fx_dir_t)];
typedef fx_dirext_t     fx_dirext_blobs_t[FX_NBLOBS(fx_dirext_t)];
typedef fx_dirseg_t     fx_dirseg_blobs_t[FX_NBLOBS(fx_dirseg_t)];
typedef fx_symlnk_t     fx_symlnk_blobs_t[FX_NBLOBS(fx_symlnk_t)];
typedef fx_slice_t      fx_slice_blobs_t[FX_NBLOBS(fx_slice_t)];
typedef fx_segmnt_t     fx_segmnt_blobs_t[FX_NBLOBS(fx_segmnt_t)];
typedef fx_byte_t       fx_align_blobs_t[FX_BLOBSIZE - FX_BLOBHDRS];


typedef union fx_packed fx_blobs {
	fx_spmap_blobs_t    spmap_blobs;
	fx_dir_blobs_t      dir_blobs;
	fx_dirext_blobs_t   dirext_blobs;
	fx_dirseg_blobs_t   dirseg_blobs;
	fx_inode_blobs_t    inode_blobs;
	fx_slice_blobs_t    slice_blobs;
	fx_segmnt_blobs_t   segmnt_blobs;
	fx_symlnk_blobs_t   symlnk_blobs;
	fx_align_blobs_t    align_;

} fx_blobs_t;

typedef struct fx_packed fx_blob {
	void      *next;
	fx_blobs_t blobs;

} fx_blob_t;



#endif /* FUNEX_BLOBS_H_ */

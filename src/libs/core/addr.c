/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "balagan.h"
#include "fnxinfra.h"

#include "meta.h"
#include "types.h"
#include "stats.h"
#include "index.h"
#include "addr.h"
#include "addri.h"
#include "elems.h"

/* Local function forwrad declarations */
static fx_size_t vaddr_resolve(const fx_vaddr_t *vaddr, fx_size_t nsec);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Directory addressing */
fx_off_t fx_hash_to_dseg(fx_hash_t hash)
{
	fx_hash_t h0, h1, h2, h3, h4, h5;
	const fx_hash_t mask = 0xFFF;

	fx_staticassert((FNX_DIREXT_NSEGS - 1) == 0xFFF);
	h0 = (hash & mask);
	h1 = ((hash >> 11) & mask);
	h2 = ((hash >> 19) & mask);
	h3 = ((hash >> 31) & mask);
	h4 = ((hash >> 41) & mask);
	h5 = ((hash >> 53) & mask);

	return (fx_off_t)(h0 ^ h1 ^ h2 ^ h3 ^ h4 ^ h5);
}

int fx_doff_isvalid(fx_off_t doff)
{
	return (((doff >= FNX_DOFF_SELF) && (doff < FNX_DOFF_END)) ||
	        (doff == FNX_DOFF_PROOT));
}

fx_off_t fx_doff_to_dseg(fx_off_t doff)
{
	return (doff - FNX_DOFF_BEGINS) / FNX_DIRSEG_NDENT;
}

fx_off_t fx_dseg_to_doff(fx_off_t dseg)
{
	return FNX_DOFF_BEGINS + (dseg * FNX_DIRSEG_NDENT);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Converts volume-size to blocks-count; forces section alignment */
fx_bkcnt_t fx_volsz_to_bkcnt(fx_off_t volsz)
{
	return (fx_bkcnt_t)(volsz / FNX_SECSIZE) * FNX_SECNBK;
}

fx_bkcnt_t fx_bytes_to_nfrg(fx_size_t nbytes)
{
	return (nbytes + FNX_FRGSIZE - 1) / FNX_FRGSIZE;
}

fx_bkcnt_t fx_bytes_to_nbk(fx_size_t nbytes)
{
	return (nbytes + FNX_BLKSIZE - 1) / FNX_BLKSIZE;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_bkcnt_t range_to_nbk(fx_off_t off, fx_size_t len)
{
	fx_size_t blen;
	fx_off_t boff, bend;
	fx_bkcnt_t bkcnt = 0;

	if (len > 0) {
		boff  = off_floor_blk(off);
		bend  = off_end(off, len);
		blen  = off_len(boff, bend);
		bkcnt = fx_bytes_to_nbk(blen);
	}
	return bkcnt;
}

/* Offset to block-index within-segment */
static fx_lba_t off_to_lba(fx_off_t off)
{
	return (fx_lba_t)(off / FNX_BLKSIZE);
}

static fx_size_t off_to_segbi(fx_off_t off)
{
	fx_off_t seg;
	fx_lba_t lba, beg;
	fx_size_t idx;

	seg = off_floor_seg(off);
	beg = off_to_lba(seg);
	lba = off_to_lba(off);
	idx = (lba - beg);
	return idx;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_isvalid_seg(fx_off_t off, fx_size_t len)
{
	fx_off_t beg, ref, end;
	fx_size_t dif, blk_size, seg_size;

	blk_size = FNX_BLKSIZE;
	seg_size = FNX_SEGSIZE;

	beg = off_floor_seg(off);
	ref = off_end(off, len + blk_size);
	end = off_floor_seg(ref);
	dif = off_len(beg, end);

	return (dif <= (2 * seg_size)); /* TODO: defne this 2 (iobufs) */
}

static int
is_subrange(fx_off_t roff, fx_size_t rlen, fx_off_t soff, fx_size_t slen)
{
	fx_off_t rfin, sfin;

	rfin = off_end(roff, rlen);
	sfin = off_end(soff, slen);

	return ((roff <= soff) && (sfin <= rfin));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_bkaddr_setup(fx_bkaddr_t *bkaddr,
                     fx_svol_t vol, fx_lba_t lba, fx_off_t frg)
{
	bkaddr->svol = vol;
	bkaddr->lba = lba;
	bkaddr->frg = (short)frg;
}

void fx_bkaddr_sfloor(const fx_bkaddr_t *bkaddr, fx_bkaddr_t *secaddr)
{
	fx_lba_t  lba;
	fx_size_t sec;

	sec = lba_to_sec(bkaddr->lba);
	lba = sec_to_lba(sec);

	fx_bkaddr_setup(secaddr, bkaddr->svol, lba, 0);
}

int fx_bkaddr_compare(const fx_bkaddr_t *bkaddr1,
                      const fx_bkaddr_t *bkaddr2)
{
	int res;

	res = fx_bkaddr_compare2(bkaddr1, bkaddr2);
	if (res != 0) {
		goto out;
	}
	res = fx_compare3(bkaddr1->frg, bkaddr2->frg);
	if (res != 0) {
		goto out;
	}
out:
	return res;
}

int fx_bkaddr_compare2(const fx_bkaddr_t *bkaddr1,
                       const fx_bkaddr_t *bkaddr2)
{
	int res;

	res = fx_compare3(bkaddr1->svol, bkaddr2->svol);
	if (res != 0) {
		goto out;
	}
	res = fx_ucompare3(bkaddr1->lba, bkaddr2->lba);
	if (res != 0) {
		goto out;
	}
out:
	return res;
}

int fx_bkaddr_isequal(const fx_bkaddr_t *bkaddr1,
                      const fx_bkaddr_t *bkaddr2)
{
	return (fx_bkaddr_compare(bkaddr1, bkaddr2) == 0);
}


/*
 * Retruns the distance between to block-address, in fragments units.
 * If not within the same volume, returns non-valid value.
 */
static fx_off_t lba_to_frg(fx_lba_t lba)
{
	return (fx_off_t)(lba * FNX_BLKNFRG);
}

fx_off_t fx_bkaddr_frgdist(const fx_bkaddr_t *addr1, const fx_bkaddr_t *addr2)
{
	fx_off_t frg1, frg2, dis = LONG_MIN;

	if (addr1->svol == addr2->svol) {
		frg1 = lba_to_frg(addr1->lba) + addr1->frg;
		frg2 = lba_to_frg(addr2->lba) + addr2->frg;
		dis  = frg2 - frg1;
	}
	return dis;
}

/*
 * Translates reference to dobject within on-disk meta-block to phisical block
 * address as a tuple of (svol,lba,frg).
 */
void fx_bkaddr_byref(fx_bkaddr_t *bkaddr,
                     const fx_bkref_t *sec, const fx_dfrg_t *dfrg)
{
	fx_off_t frgi;
	const fx_dsec_t *dsec;

	dsec  = fx_get_dsec(sec);
	frgi  = fx_dsec_frgoff(dsec, dfrg);
	fx_bkaddr_setup(bkaddr, sec->addr.svol, sec->addr.lba, frgi);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_brange_init(fx_brange_t *brange)
{
	brange->off = 0;
	brange->len = 0;
	brange->idx = 0;
	brange->cnt = 0;
}

void fx_brange_destroy(fx_brange_t *brange)
{
	brange->off = -1;
	brange->len = 0;
	brange->idx = 0;
	brange->cnt = 0;
}

void fx_brange_copy(fx_brange_t *brange, const fx_brange_t *other)
{
	brange->off = other->off;
	brange->len = other->len;
	brange->idx = other->idx;
	brange->cnt = other->cnt;
}

void fx_brange_setup(fx_brange_t *brange, fx_off_t off, fx_size_t len)
{
	brange->off = off;
	brange->len = len;
	brange->idx = off_to_segbi(off);
	brange->cnt = range_to_nbk(off, len);
}

int fx_brange_issub(const fx_brange_t *brange, fx_off_t off, fx_size_t len)
{
	return is_subrange(brange->off, brange->len, off, len);
}

int fx_brange_issub2(const fx_brange_t *brange, const fx_brange_t *sub)
{
	return fx_brange_issub(brange, sub->off, sub->len);
}

fx_off_t fx_brange_end(const fx_brange_t *brange)
{
	return off_end(brange->off, brange->len);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_segmap_init(fx_segmap_t *segmap)
{
	fx_brange_init(&segmap->rng);
	fx_foreach_arrelem(segmap->bka, bkaddr_reset);
}

void fx_segmap_destroy(fx_segmap_t *segmap)
{
	fx_brange_destroy(&segmap->rng);
}

int fx_segmap_setup(fx_segmap_t *segmap, fx_off_t off, fx_size_t len)
{
	int rc = 0;
	fx_size_t idx;
	fx_bkcnt_t cnt, nbk;

	fx_brange_setup(&segmap->rng, off, len);
	idx = segmap->rng.idx;
	cnt = segmap->rng.cnt;
	nbk = FX_NELEMS(segmap->bka);
	if ((idx + cnt) > nbk) {
		rc = -1;
	}
	return rc;
}

size_t fx_segmap_nmaps(const fx_segmap_t *segmap)
{
	size_t nmaps = 0;
	const fx_bkaddr_t *bkaddr;
	const fx_brange_t *brange = &segmap->rng;

	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos = brange->idx + i;
		bkaddr = &segmap->bka[pos];
		if (!bkaddr_isnull(bkaddr)) {
			++nmaps;
		}
	}
	return nmaps;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_vaddr_init(fx_vaddr_t *vaddr, fx_vtype_e vtype)
{
	vaddr_init(vaddr, vtype);
}

void fx_vaddr_destroy(fx_vaddr_t *vaddr)
{
	vaddr_destroy(vaddr);
}

/* V-address special-mapping for types in fixed locations */
static int vaddr_issuper(const fx_vaddr_t *vaddr)
{
	return (vaddr->vtype == FNX_VTYPE_SUPER);
}

static int vaddr_isrootd(const fx_vaddr_t *vaddr)
{
	return ((vaddr->vtype == FNX_VTYPE_DIR) &&
	        (vaddr->ino == FNX_INO_ROOT) &&
	        (vaddr->xno == FNX_XNO_NULL));
}

int fx_vaddr_smap(const fx_vaddr_t *vaddr, fx_bkaddr_t *bkaddr)
{
	int rc = 0;
	fx_lba_t lba;
	fx_off_t frg;

	/* Special cases for vobjs in fixed location */
	if (vaddr_issuper(vaddr)) {
		lba = sec_to_lba(FNX_SEC_SUPER);
		frg = FNX_FRG_SUPER;
		fx_bkaddr_setup(bkaddr, FNX_SVOL_META, lba, frg);
	} else if (vaddr_isrootd(vaddr)) {
		lba = sec_to_lba(FNX_SEC_SUPER);
		frg = FNX_FRG_ROOT;
		fx_bkaddr_setup(bkaddr, FNX_SVOL_META, lba, frg);
	} else {
		rc = -1;
	}
	return rc;
}

/* Transforms v-address to volume's block-address using on-way hash func */
void fx_vaddr_hmap(const fx_vaddr_t *vaddr,
                   const fx_extent_t *vindex, fx_bkaddr_t *bkaddr)
{
	fx_lba_t lba0, sec_lba;
	fx_bkcnt_t bkcnt;
	fx_size_t nvsec, sec_idx;

	lba0    = vindex->lba;
	bkcnt   = vindex->cnt;
	nvsec   = lba_to_sec(bkcnt);
	sec_idx = vaddr_resolve(vaddr, nvsec);
	sec_lba = sec_to_lba(sec_idx);

	fx_bkaddr_setup(bkaddr, FNX_SVOL_META, lba0 + sec_lba, 0);
}


static uint64_t vaddr_calcvhash1(const fx_vaddr_t *vaddr)
{
	uint64_t in[3], res = 0;
	const uint8_t seed[16] = {
		0x09, 0xae, 0x16, 0xa3, 0xb2, 0xf9, 0x04, 0x04,
		0xb4, 0x92, 0xb6, 0x6f, 0xbe, 0x98, 0xf2, 0x73
	};

	in[0] = (uint64_t)(vaddr->ino);
	in[1] = (uint64_t)(vaddr->xno);
	in[2] = (uint64_t)(vaddr->vtype) * 201326611ULL;
	blgn_siphash64(seed, in, sizeof(in), &res);
	return res;
}

static uint64_t vaddr_calcvhash2(const fx_vaddr_t *vaddr)
{
	uint64_t in[4], rc[2];

	in[0] = 0xb492b66fbe98f273ULL;
	in[1] = (uint64_t)(vaddr->xno);
	in[2] = (uint64_t)(vaddr->vtype) * 201326611ULL;
	in[3] = (uint64_t)(vaddr->ino);

	blgn_blake128(in, sizeof(in), rc);
	return (rc[0] ^ rc[1]);
}

static uint64_t vaddr_calcvhash3(const fx_vaddr_t *vaddr)
{
	uint64_t in[4];
	uint64_t rc[8];

	in[0] = (uint64_t)(vaddr->ino);
	in[1] = (uint64_t)(vaddr->xno);
	in[2] = 0x9ae16a3b2f90404fULL;
	in[3] = (uint64_t)(vaddr->vtype) * 201326611ULL;

	blgn_cubehash512(in, sizeof(in), rc);
	return (rc[0] ^ ~rc[1] ^ rc[2] ^ ~rc[3] ^
	        ~rc[4] ^ rc[5] ^ ~rc[6] ^ rc[7]);
}

static uint64_t vaddr_calcihash(const fx_vaddr_t *vaddr)
{
	uint64_t val, hash = 0;
	uint64_t ino  = (uint64_t)(vaddr->ino);

	val = (ino * 251);
	blgn_bardak64(val, &hash);
	return hash;
}

static fx_size_t rehash_to_nsec(fx_hash_t hash, fx_size_t nsec)
{
	fx_size_t hidx;
	fx_hash_t rhash, base;

	/* Euler's Mersenne-prime: 2^31 - 1 */
	base    = (1ULL << 31) - 1;
	rhash   = (hash ^ (hash >> 33)) & base;
	hidx    = ((rhash * nsec) / base);
	return hidx;
}

/* Hash 'vaddr' into (section) range [0, nsec). For vaddr with anonymous vtype,
 * calc hash by ino only */
static fx_size_t vaddr_resolve(const fx_vaddr_t *vaddr, fx_size_t nsec)
{
	fx_hash_t hash;
	fx_size_t sec_idx;
	const fx_vtype_e vtype = vaddr->vtype;

	/* Resolve vaddr to hash-value */
	if ((vtype == FNX_VTYPE_NONE) || (vtype == FNX_VTYPE_ANY)) {
		/* Special case: lookup-by-ino without explicit vtype */
		hash = vaddr_calcihash(vaddr);
	} else if (fx_isitype(vtype)) {
		/* Normal inode-lookup */
		hash = vaddr_calcihash(vaddr);
	} else {
		if (vtype == FNX_VTYPE_SEGMNT) {
			/*  Fast pseudo-random hash for file-segments */
			hash = vaddr_calcvhash1(vaddr);
		} else if (vtype == FNX_VTYPE_SPMAP) {
			/* Crypto-hash-2 for space-maps */
			hash = vaddr_calcvhash2(vaddr);
		} else {
			/* Crypto-hash-3 for other all other vnodes */
			hash = vaddr_calcvhash3(vaddr);
		}
	}

	/* Convert 64-bits hash value to range [0, nsec) */
	sec_idx = rehash_to_nsec(hash, nsec);
	return sec_idx;
}




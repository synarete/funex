/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_ADDR_H_
#define FUNEX_ADDR_H_

struct fx_bkref;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* V-element address as a tuple */
struct fx_vaddr {
	fx_vtype_e      vtype;
	fx_ino_t        ino;
	fx_xno_t        xno;
};
typedef struct fx_vaddr    fx_vaddr_t;


/* Logical bytes-range within regular-file + blocks reference */
struct fx_brange {
	fx_off_t       off;    /* Logical offset */
	fx_size_t      len;    /* Bytes length */
	fx_size_t      idx;    /* Internal block-index */
	fx_bkcnt_t     cnt;    /* Blocks count */
};
typedef struct fx_brange fx_brange_t;


/* Block-address tuple (vol.lba.frg) */
struct fx_bkaddr {
	fx_svol_t      svol;   /* Sub-volume identifier */
	fx_lba_t       lba;    /* LBA within sub-volume */
	signed short   frg;    /* Fragment-index from block's start */
};
typedef struct fx_bkaddr fx_bkaddr_t;


/* Mapping of regular-file's segment to multiple block-addresses */
struct fx_segmap {
	fx_brange_t    rng;
	fx_bkaddr_t    bka[FNX_SEGNBK];
};
typedef struct fx_segmap fx_segmap_t;



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Directory addressing */
int fx_doff_isvalid(fx_off_t doff);

fx_off_t fx_doff_to_dseg(fx_off_t);

fx_off_t fx_dseg_to_doff(fx_off_t);

fx_off_t fx_hash_to_dseg(fx_hash_t);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Addressing conversions */

fx_bkcnt_t fx_volsz_to_bkcnt(fx_off_t);

fx_bkcnt_t fx_bytes_to_nbk(fx_size_t);

fx_bkcnt_t fx_bytes_to_nfrg(fx_size_t);


int fx_isvalid_seg(fx_off_t, fx_size_t);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* V-address operations */
void fx_vaddr_init(fx_vaddr_t *, fx_vtype_e);

void fx_vaddr_destroy(fx_vaddr_t *);

int fx_vaddr_smap(const fx_vaddr_t *, fx_bkaddr_t *);

void fx_vaddr_hmap(const fx_vaddr_t *, const fx_extent_t *, fx_bkaddr_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Block-addressing */
void fx_bkaddr_setup(fx_bkaddr_t *, fx_svol_t, fx_lba_t, fx_off_t);

int fx_bkaddr_isequal(const fx_bkaddr_t *, const fx_bkaddr_t *);

int fx_bkaddr_compare(const fx_bkaddr_t *, const fx_bkaddr_t *);

int fx_bkaddr_compare2(const fx_bkaddr_t *, const fx_bkaddr_t *);

void fx_bkaddr_sfloor(const fx_bkaddr_t *, fx_bkaddr_t *);

fx_off_t fx_bkaddr_frgdist(const fx_bkaddr_t *, const fx_bkaddr_t *);

void fx_bkaddr_byref(fx_bkaddr_t *, const struct fx_bkref *, const fx_dfrg_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Bytes-range */
void fx_brange_init(fx_brange_t *);

void fx_brange_destroy(fx_brange_t *);

void fx_brange_copy(fx_brange_t *, const fx_brange_t *);

void fx_brange_setup(fx_brange_t *, fx_off_t, fx_size_t);

int fx_brange_issub(const fx_brange_t *, fx_off_t, fx_size_t);

int fx_brange_issub2(const fx_brange_t *, const fx_brange_t *);

fx_off_t fx_brange_end(const fx_brange_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Segment-mapping */
void fx_segmap_init(fx_segmap_t *);

void fx_segmap_destroy(fx_segmap_t *);

int fx_segmap_setup(fx_segmap_t *, fx_off_t, fx_size_t);

size_t fx_segmap_nmaps(const fx_segmap_t *);


#endif /* FUNEX_ADDR_H_ */



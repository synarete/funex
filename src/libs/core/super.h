/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_SUPER_H_
#define FUNEX_SUPER_H_



/* Super-Block */
fx_super_t *fx_vnode_to_super(const fx_vnode_t *);

void fx_super_init(fx_super_t *);

void fx_super_destroy(fx_super_t *);

void fx_super_setup(fx_super_t *, const char *, const char *);

void fx_super_settimes(fx_super_t *, const fx_times_t *, fx_flags_t);

void fx_super_htod(const fx_super_t *, fx_dsuper_t *);

void fx_super_dtoh(fx_super_t *, const fx_dsuper_t *);

int fx_super_dcheck(const fx_header_t *);

void fx_super_getfsinfo(const fx_super_t *, fx_fsinfo_t *);

void fx_super_fillnewi(const fx_super_t *, fx_inode_t *);


int fx_consume_ino(fx_super_t *, int, fx_ino_t *);

int fx_relinquish_ino(fx_super_t *, fx_ino_t);

int fx_consume_lba(fx_super_t *, fx_spmap_t *, int, fx_lba_t *);

int fx_relinquish_lba(fx_super_t *, fx_spmap_t *, fx_lba_t);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Space-block */
fx_spmap_t *fx_vnode_to_spmap(const fx_vnode_t *);

void fx_spmap_init(fx_spmap_t *);

void fx_spmap_destroy(fx_spmap_t *);

void fx_spmap_setup(fx_spmap_t *, fx_lba_t);

void fx_spmap_htod(const fx_spmap_t *, fx_dspmap_t *);

void fx_spmap_dtoh(fx_spmap_t *, const fx_dspmap_t *);

int fx_spmap_dcheck(const fx_header_t *);

int fx_spmap_hasfree(const fx_spmap_t *, fx_bkcnt_t);

int fx_spmap_isused(const fx_spmap_t *, fx_lba_t);

fx_bkdsc_t *fx_spmap_getdsc(fx_spmap_t *, fx_lba_t);

#endif /* FUNEX_SUPER_H_ */


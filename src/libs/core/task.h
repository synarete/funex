/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_TASK_H_
#define FUNEX_TASK_H_


/* Task's control flags */
#define FX_TASKF_PURIFY    FX_BITFLAG(1)
#define FX_TASKF_CLEARED   FX_BITFLAG(2)


/* FUSE types */
struct fuse_req;
struct fuse_chan;
typedef struct fuse_req  *fx_fuse_req_t;
typedef struct fuse_chan *fx_fuse_chan_t;


/* Tasks' queueing mechanism */
struct fx_task;
struct fx_fusei;
typedef struct fx_task fx_task_t;
typedef fx_execq_t fx_taskq_t;

/* Execution-context (task) */
struct fx_task {
	fx_xelem_t      tsk_xelem;
	fx_opcode_e     tsk_opcode;
	fx_timespec_t   tsk_start;
	fx_size_t       tsk_seqno;
	fx_flags_t      tsk_flags;
	fx_uctx_t       tsk_uctx;
	fx_request_t    tsk_request;
	fx_response_t   tsk_response;
	fx_magic_t      tsk_magic;
	fx_iobufs_t     tsk_iobufs;
	fx_fuse_req_t   tsk_fuse_req;
	fx_fuse_chan_t  tsk_fuse_chan;
	fx_fileref_t    *tsk_fref;
	struct fx_fusei *tsk_fusei;
};


/* Tasks' pool */
struct fx_task_pool {
	fx_mutex_t      mutex;      /* Protection mutex */
	fx_list_t       frees;      /* List of available (free) tasks */
	fx_task_t       pool[128];  /* Private task-entries */
};
typedef struct fx_task_pool fx_taskpool_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_task_init(fx_task_t *, fx_opcode_e);

void fx_task_destroy(fx_task_t *);


void fx_send_task(fx_taskq_t *, fx_task_t *, fx_flags_t);

void fx_send_utask(fx_taskq_t *, fx_task_t *);

fx_task_t *fx_recv_tasks(fx_taskq_t *, unsigned, unsigned);

fx_task_t *fx_link_to_task(const fx_link_t *);

fx_task_t *fx_xelem_to_task(const fx_xelem_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_taskpool_init(fx_taskpool_t *);

void fx_taskpool_destroy(fx_taskpool_t *);

fx_task_t *fx_taskpool_newtask(fx_taskpool_t *, fx_opcode_e);

void fx_taskpool_deltask(fx_taskpool_t *, fx_task_t *);

void fx_taskpool_cleanup(fx_taskpool_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline int task_status(const fx_task_t *task)
{
	return task->tsk_xelem.status;
}

#endif /* FUNEX_TASK_H_ */





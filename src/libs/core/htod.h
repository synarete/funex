/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_HTOD_H_
#define FUNEX_HTOD_H_

struct fx_uuid;
struct fx_dtimes;

/* Host --> External */
uint16_t fx_htod_u16(uint16_t);

uint32_t fx_htod_u32(uint32_t);

uint64_t fx_htod_u64(uint64_t);

uint32_t fx_htod_uid(fx_uid_t);

uint32_t fx_htod_gid(fx_gid_t);

uint64_t fx_htod_ino(fx_ino_t);

uint32_t fx_htod_mode(fx_mode_t);

uint32_t fx_htod_nlink(fx_nlink_t);

uint64_t fx_htod_dev(fx_dev_t);

uint64_t fx_htod_bkcnt(fx_bkcnt_t);

uint64_t fx_htod_filcnt(fx_filcnt_t);

uint64_t fx_htod_time(fx_time_t);

uint32_t fx_htod_versnum(unsigned);

uint32_t fx_htod_magic(fx_magic_t);

uint64_t fx_htod_flags(fx_flags_t);

uint64_t fx_htod_size(fx_size_t);

uint64_t fx_htod_hash(fx_hash_t);

uint64_t fx_htod_off(fx_off_t);

uint64_t fx_htod_lba(fx_lba_t);

uint8_t  fx_htod_vtype(fx_vtype_e);

uint16_t fx_htod_hfunc(fx_hfunc_e);

void fx_htod_uuid(uint8_t *, const struct fx_uuid *);

void fx_htod_timespec(uint64_t *, uint32_t *, const fx_timespec_t *);

void fx_htod_name(uint8_t *, uint8_t *, const fx_name_t *);

void fx_htod_path(uint8_t *, uint16_t *, const fx_path_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* External --> Host */
uint16_t fx_dtoh_u16(uint16_t);

uint32_t fx_dtoh_u32(uint32_t);

uint64_t fx_dtoh_u64(uint64_t);

fx_uid_t fx_dtoh_uid(uint32_t);

fx_gid_t fx_dtoh_gid(uint32_t);

fx_ino_t fx_dtoh_ino(uint64_t);

fx_mode_t fx_dtoh_mode(uint32_t);

fx_nlink_t fx_dtoh_nlink(uint32_t);

fx_dev_t fx_dtoh_dev(uint64_t);

fx_bkcnt_t fx_dtoh_bkcnt(uint64_t);

fx_filcnt_t fx_dtoh_filcnt(uint64_t);

fx_time_t fx_dtoh_time(uint64_t);

fx_version_t fx_dtoh_versnum(uint32_t);

fx_magic_t fx_dtoh_magic(uint32_t);

fx_flags_t fx_dtoh_flags(uint64_t);

fx_size_t fx_dtoh_size(uint64_t);

fx_hash_t fx_dtoh_hash(fx_hash_t);

fx_off_t fx_dtoh_off(uint64_t);

fx_lba_t fx_dtoh_lba(uint64_t);

fx_vtype_e fx_dtoh_vtype(uint8_t);

fx_hfunc_e fx_dtoh_hfunc(uint16_t);

void fx_dtoh_uuid(struct fx_uuid *, const uint8_t *);

void fx_dtoh_timespec(fx_timespec_t *, uint64_t, uint32_t);

void fx_dtoh_name(fx_name_t *, const uint8_t *, uint8_t);

void fx_dtoh_path(fx_path_t *, const uint8_t *, uint16_t);

#endif /* FUNEX_HTOD_H_ */


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_INODE_H_
#define FUNEX_INODE_H_

/* Inodes init nlink defs */
#define FX_INODE_INIT_NLINK     (0)
#define FX_DIR_INIT_NLINK       (1)



/* V-type */
fx_vtype_e fx_mode_to_vtype(fx_mode_t mode);

fx_mode_t fx_vtype_to_mode(fx_vtype_e);

fx_mode_t fx_vtype_to_crmode(fx_vtype_e, fx_mode_t, fx_mode_t);

fx_hash_t fx_calc_inamehash(const fx_name_t *name, fx_ino_t);


/* V-node */
void fx_vnode_init(fx_vnode_t *, fx_vtype_e);

void fx_vnode_destroy(fx_vnode_t *);

void fx_vnode_dexport(const fx_vnode_t *, fx_header_t *);

void fx_vnode_dimport(fx_vnode_t *, const fx_header_t *);

void fx_vnode_dverify(const fx_vnode_t *, const fx_header_t *);

void fx_vnode_setbkaddr(fx_vnode_t *, const fx_bkref_t *, const fx_dfrg_t *);


/* I-node */
fx_inode_t *fx_vnode_to_inode(const fx_vnode_t *);

void fx_inode_init(fx_inode_t *, fx_vtype_e);

void fx_inode_destroy(fx_inode_t *);

void fx_inode_setino(fx_inode_t *, fx_ino_t);

void fx_inode_setup(fx_inode_t *, const fx_uctx_t *, fx_mode_t , fx_mode_t);

void fx_inode_associate(fx_inode_t *, fx_ino_t, const fx_name_t *, fx_hash_t);


void fx_inode_clearsuid(fx_inode_t *);

void fx_inode_clearsgid(fx_inode_t *);


void fx_inode_htod(const fx_inode_t *, fx_dinode_t *);

void fx_inode_dtoh(fx_inode_t *, const fx_dinode_t *);

int fx_inode_dcheck(const fx_header_t *);


void fx_inode_setitimes(fx_inode_t *, fx_flags_t, const fx_times_t *);

void fx_inode_setitime(fx_inode_t *, fx_flags_t);

void fx_inode_getiattr(const fx_inode_t *, fx_iattr_t *);

int fx_inode_access(const fx_inode_t *, const fx_uctx_t *, fx_mode_t);

int fx_inode_isexec(const fx_inode_t *);

#endif /* FUNEX_INODE_H_ */




/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdint.h>
#include <errno.h>
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "proto.h"
#include "metai.h"
#include "index.h"
#include "htod.h"
#include "addr.h"
#include "addri.h"
#include "elems.h"
#include "inode.h"
#include "iobuf.h"
#include "super.h"

#define FX_BYTE_ORDER      __BYTE_ORDER

/*
 * Local functions declarations:
 */
static int spmap_ismember(const fx_spmap_t *, fx_lba_t);
static int spmap_alloc_lba(fx_spmap_t *, fx_lba_t, fx_lba_t *);
static int spmap_dealloc_lba(fx_spmap_t *, const fx_lba_t);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_super_t *fx_vnode_to_super(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_super_t, su_vnode);
}

void fx_super_init(fx_super_t *super)
{
	fx_vnode_init(&super->su_vnode, FNX_VTYPE_SUPER);
	fx_uuid_clear(&super->su_uuid);
	fx_name_init(&super->su_name);
	fx_name_init(&super->su_uref);
	fx_layout_init(&super->su_layout);
	fx_fsattr_init(&super->su_fsattr);
	fx_fsstat_init(&super->su_fsstat);
	fx_bzero_obj(&super->su_vstats);
	fx_bzero_obj(&super->su_iostat);
	fx_bzero_obj(&super->su_opstat);
	fx_times_init(&super->su_times);
	super->su_active    = FNX_TRUE;
	super->su_flags     = 0;
	super->su_magic     = FNX_SUPER_MAGIC;
	super->su_vstats.v_magic  = FNX_VSTAT_MAGIC;
}

void fx_super_destroy(fx_super_t *super)
{
	fx_assert(super->su_magic == FNX_SUPER_MAGIC);
	fx_vnode_destroy(&super->su_vnode);
	fx_layout_destroy(&super->su_layout);
	fx_bzero_obj(&super->su_fsattr);
	fx_bzero_obj(&super->su_fsstat);
	fx_bzero_obj(&super->su_vstats);
	fx_bzero_obj(&super->su_iostat);
	fx_bzero_obj(&super->su_opstat);
	super->su_magic         = 0;
}

void fx_super_setup(fx_super_t *super, const char *name, const char *uref)
{
	fx_name_setup(&super->su_name, name);
	fx_name_setup(&super->su_uref, uref);
	vaddr_for_super(&super->su_vnode.v_vaddr);
}

void fx_super_htod(const fx_super_t *super, fx_dsuper_t *dsuper)
{
	fx_header_t *hdr = &dsuper->su_hdr;

	/* Header & tail */
	fx_vnode_dexport(&super->su_vnode, hdr);
	fx_dobj_zpad(hdr, FNX_VTYPE_SUPER);

	/* R[0] */
	fx_htod_uuid(dsuper->su_uuid, &super->su_uuid);
	dsuper->su_flags    = fx_htod_flags(super->su_flags);
	dsuper->su_secsize  = fx_htod_u32(FNX_SECSIZE);
	dsuper->su_blksize  = fx_htod_u16(FNX_BLKSIZE);
	dsuper->su_frgsize  = fx_htod_u16(FNX_FRGSIZE);

	/* R[1] */
	dsuper->su_endian   = fx_htod_u16(FX_BYTE_ORDER);
	dsuper->su_ostype   = 0; /* XXX */

	/* R[2..3,4,5,6..7] */
	fx_layout_htod(&super->su_layout, &dsuper->su_layout);
	fx_fsattr_htod(&super->su_fsattr, &dsuper->su_fsattr);
	fx_fsstat_htod(&super->su_fsstat, &dsuper->su_fsstat);
	fx_vstats_htod(&super->su_vstats, &dsuper->su_vstats);

	/* R[8] */
	fx_times_htod(&super->su_times, &dsuper->su_times);
	dsuper->su_magic = fx_htod_magic(super->su_magic);

	/* R[16..23] */
	fx_htod_name(dsuper->su_name, &dsuper->su_namelen, &super->su_name);
	fx_htod_name(dsuper->su_uref, &dsuper->su_ureflen, &super->su_uref);
}


void fx_super_dtoh(fx_super_t *super, const fx_dsuper_t *dsuper)
{
	int rc;
	const fx_header_t *hdr = &dsuper->su_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_SUPER);
	fx_assert(rc == 0);
	fx_vnode_dimport(&super->su_vnode, hdr);

	/* R[0] */
	dsuper = header_to_dsuper(hdr);
	fx_dtoh_uuid(&super->su_uuid, dsuper->su_uuid);
	super->su_flags     = fx_dtoh_flags(dsuper->su_flags);

	/* R[2..3,4,5,6..7] */
	fx_layout_dtoh(&super->su_layout, &dsuper->su_layout);
	fx_fsattr_dtoh(&super->su_fsattr, &dsuper->su_fsattr);
	fx_fsstat_dtoh(&super->su_fsstat, &dsuper->su_fsstat);
	fx_vstats_dtoh(&super->su_vstats,  &dsuper->su_vstats);

	/* R[8] */
	fx_times_dtoh(&super->su_times, &dsuper->su_times);
	super->su_magic = fx_dtoh_magic(dsuper->su_magic);

	/* R[16..23] */
	fx_dtoh_name(&super->su_name, dsuper->su_name, dsuper->su_namelen);
	fx_dtoh_name(&super->su_uref, dsuper->su_uref, dsuper->su_ureflen);
}

int fx_super_dcheck(const fx_header_t *hdr)
{
	int rc;
	size_t size;
	fx_vtype_e vtype;
	fx_magic_t magic;
	const fx_dsuper_t *dsuper;

	vtype = fx_dobj_vtype(hdr);
	if (vtype != FNX_VTYPE_SUPER) {
		fx_warn("not-super-vtype: vtype=%d", (int)vtype);
		return -1;
	}
	rc = fx_dobj_check(hdr, vtype);
	if (rc != 0) {
		return rc;
	}
	dsuper = header_to_dsuper(hdr);
	magic  = fx_dtoh_magic(dsuper->su_magic);
	if (magic != FNX_SUPER_MAGIC) {
		fx_warn("not-super-magic: magic=%#x expected=%#x",
		        magic, FNX_SUPER_MAGIC);
		return -1;
	}
	magic = fx_dtoh_magic(dsuper->su_vstats.v_magic);
	if (magic != FNX_VSTAT_MAGIC) {
		fx_warn("not-vstat-magic: magic=%#x expected=%#x",
		        magic, FNX_VSTAT_MAGIC);
		return -1;
	}
	size = fx_dtoh_u16(dsuper->su_frgsize);
	if (size != FNX_FRGSIZE) {
		fx_warn("bad-frgsize: size=%d expected=%d", (int)size, FNX_FRGSIZE);
		return -1;
	}
	size = fx_dtoh_u16(dsuper->su_blksize);
	if (size != FNX_BLKSIZE) {
		fx_warn("bad-blksize: size=%d expected=%d", (int)size, FNX_BLKSIZE);
		return -1;
	}
	size = fx_dtoh_u32(dsuper->su_secsize);
	if (size != FNX_SECSIZE) {
		fx_warn("bad-secsize: size=%d expected=%d", (int)size, FNX_SECSIZE);
		return -1;
	}

	return 0;
}

void fx_super_settimes(fx_super_t *super,
                       const fx_times_t *tms, fx_flags_t flags)
{
	fx_times_fill(&super->su_times, flags, tms);
}

void fx_super_getfsinfo(const fx_super_t *super, fx_fsinfo_t *fsinfo)
{
	fx_fsattr_t *fsattr = &fsinfo->attr;
	fx_fsstat_t *fsstat = &fsinfo->fsst;
	fx_vstats_t *vstats = &fsinfo->vsts;
	fx_iostat_t *iostat = &fsinfo->iost;
	fx_opstat_t *opstat = &fsinfo->opst;

	fx_bzero(fsinfo, sizeof(*fsinfo));
	fx_bcopy(fsattr, &super->su_fsattr, sizeof(*fsattr));
	fx_bcopy(fsstat, &super->su_fsstat, sizeof(*fsstat));
	fx_bcopy(vstats, &super->su_vstats, sizeof(*vstats));
	fx_bcopy(iostat, &super->su_iostat, sizeof(*iostat));
	fx_bcopy(opstat, &super->su_opstat, sizeof(*opstat));
}

void fx_super_fillnewi(const fx_super_t *super, fx_inode_t *inode)
{
	inode->i_iattr.i_gen = super->su_fsattr.f_gen;
}

int fx_consume_ino(fx_super_t *super, int privileged, fx_ino_t *ino)
{
	int rc = -1;
	fx_fsstat_t *fsstat = &super->su_fsstat;

	if (privileged && (fsstat->f_ifree > 0)) {
		*ino = fsstat->f_iapex++;
		fsstat->f_ifree -= 1;
		if (fsstat->f_iavail > 0) {
			fsstat->f_iavail -= 1;
		}
		rc = 0;
	} else if (!privileged && (fsstat->f_iavail > 0)) {
		*ino = fsstat->f_iapex++;

		fsstat->f_iavail -= 1;
		if (fsstat->f_ifree > 0) {
			fsstat->f_ifree -= 1;
		}
		rc = 0;
	}
	return rc;
}

int fx_relinquish_ino(fx_super_t *super, fx_ino_t ino)
{
	int rc = -1;
	fx_filcnt_t iavail_max;
	fx_fsstat_t *fsstat = &super->su_fsstat;

	if (ino < fsstat->f_iapex) {
		/* TODO: FIXME (also in stats) */
		iavail_max = (fsstat->f_inodes * 255) / 256;

		if (fsstat->f_ifree < fsstat->f_inodes) {
			fsstat->f_ifree += 1;
		}
		if (fsstat->f_iavail < iavail_max) {
			fsstat->f_iavail += 1;
		}
		rc = 0;
	}
	return rc;
}

int fx_consume_lba(fx_super_t *super, fx_spmap_t *spmap,
                   int privileged, fx_lba_t *lba)
{
	int rc = -1;
	fx_fsstat_t *fsstat = &super->su_fsstat;

	if (privileged && (fsstat->f_bfree > 0)) {
		rc = spmap_alloc_lba(spmap, fsstat->f_bnext, lba);
		if (rc == 0) {
			fsstat->f_bfree -= 1;
			if (fsstat->f_bavail > 0) {
				fsstat->f_bavail -= 1;
			}
		}
	} else if (!privileged && (fsstat->f_bavail > 0)) {
		rc = spmap_alloc_lba(spmap, fsstat->f_bnext, lba);
		if (rc == 0) {
			fsstat->f_bavail -= 1;
			if (fsstat->f_bfree > 0) {
				fsstat->f_bfree -= 1;
			}
		}
	}
	if (rc == 0) {
		fsstat->f_bnext  += 1;
	}
	return rc;
}

int fx_relinquish_lba(fx_super_t *super, fx_spmap_t *spmap, fx_lba_t lba)
{
	int rc = -1;
	fx_bkcnt_t bavail_max;
	fx_fsstat_t *fsstat = &super->su_fsstat;

	rc = spmap_dealloc_lba(spmap, lba);
	if (rc == 0) {
		/* TODO: FIXME (also in stats) */
		bavail_max = (fsstat->f_blocks * 255) / 256;

		if (fsstat->f_bfree < fsstat->f_blocks) {
			fsstat->f_bfree += 1;
		}
		if (fsstat->f_bavail < bavail_max) {
			fsstat->f_bavail += 1;
		}
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void bkdsc_init(fx_bkdsc_t *bkdsc)
{
	bkdsc->refs    = 0;
	bkdsc->flags   = 0;
}

static void bkdsc_destroy(fx_bkdsc_t *bkdsc)
{
	bkdsc->refs    = 0;
	bkdsc->flags   = 0;
}

static void bkdsc_htod(const fx_bkdsc_t *bkdsc, fx_dbkdsc_t *dbkdsc)
{
	dbkdsc->refs   = fx_htod_u32((uint32_t)bkdsc->refs);
	dbkdsc->flags  = bkdsc->flags;
}

static void bkdsc_dtoh(fx_bkdsc_t *bkdsc, const fx_dbkdsc_t *dbkdsc)
{
	bkdsc->refs    = fx_dtoh_u32(dbkdsc->refs);
	bkdsc->flags   = dbkdsc->flags;
}

static int bkdsc_dcheck(const fx_dbkdsc_t *dbkdsc)
{
	uint32_t flags, mask;

	flags = fx_dtoh_u32(dbkdsc->flags);
	mask  = FNX_BLKF_FUZZY;
	return ((flags | mask) == mask) ? 0 : -1;
}

static int bkdsc_isused(const fx_bkdsc_t *bkdsc)
{
	return (bkdsc->refs > 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_spmap_t *fx_vnode_to_spmap(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_spmap_t, sp_vnode);
}

void fx_spmap_init(fx_spmap_t *spmap)
{
	fx_vnode_init(&spmap->sp_vnode, FNX_VTYPE_SPMAP);
	fx_foreach_arrelem(spmap->sp_bkd, bkdsc_init);
	spmap->sp_lba0 = FNX_LBA_NULL;
	spmap->sp_used = 0;
}

void fx_spmap_destroy(fx_spmap_t *spmap)
{
	fx_foreach_arrelem(spmap->sp_bkd, bkdsc_destroy);
	spmap->sp_lba0 = FNX_LBA_NULL;
	spmap->sp_used = 0;
}

void fx_spmap_setup(fx_spmap_t *spmap, fx_lba_t ulba)
{
	vaddr_for_spmap(&spmap->sp_vnode.v_vaddr, ulba);
	spmap->sp_lba0 = lba_floor_spseg(ulba);
}

void fx_spmap_htod(const fx_spmap_t *spmap, fx_dspmap_t *dspmap)
{
	fx_header_t *hdr = &dspmap->sp_hdr;

	/* Header & tail */
	fx_vnode_dexport(&spmap->sp_vnode, hdr);
	fx_dobj_zpad(hdr, FNX_VTYPE_SPMAP);

	/* F[0] */
	dspmap->sp_lba0 = fx_htod_lba(spmap->sp_lba0);
	dspmap->sp_used = fx_htod_u32((uint32_t)spmap->sp_used);
	fx_bzero(dspmap->sp_reserved0, sizeof(dspmap->sp_reserved0));

	/* F[1..96] */
	for (size_t i = 0; i < FX_NELEMS(dspmap->sp_bkd); ++i) {
		bkdsc_htod(&spmap->sp_bkd[i], &dspmap->sp_bkd[i]);
	}
}

void fx_spmap_dtoh(fx_spmap_t *spmap, const fx_dspmap_t *dspmap)
{
	int rc;
	const fx_header_t *hdr = &dspmap->sp_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_SPMAP);
	fx_assert(rc == 0);
	fx_vnode_dimport(&spmap->sp_vnode, hdr);

	/* F[0] */
	spmap->sp_lba0  = fx_dtoh_lba(dspmap->sp_lba0);
	spmap->sp_used  = fx_dtoh_u32(dspmap->sp_used);

	/* F[1..96] */
	for (size_t i = 0; i < FX_NELEMS(spmap->sp_bkd); ++i) {
		bkdsc_dtoh(&spmap->sp_bkd[i], &dspmap->sp_bkd[i]);
	}
}

int fx_spmap_dcheck(const fx_header_t *hdr)
{
	int rc = 0;
	const fx_dbkdsc_t *dbkdsc;
	const fx_dspmap_t *dspmap;

	rc = fx_dobj_check(hdr, FNX_VTYPE_SPMAP);
	if (rc != 0) {
		return rc;
	}

	dspmap = header_to_dspmap(hdr);
	for (size_t i = 0; i < FX_NELEMS(dspmap->sp_bkd); ++i) {
		dbkdsc = &dspmap->sp_bkd[i];
		if ((rc = bkdsc_dcheck(dbkdsc)) != 0) {
			return rc;
		}
	}

	/* TODO: check used, lba0 */

	return 0;
}

static fx_lba_t spmap_end(const fx_spmap_t *spmap)
{
	return lba_end(spmap->sp_lba0, FX_NELEMS(spmap->sp_bkd));
}

static size_t spmap_lba_to_pos(const fx_spmap_t *spmap, fx_lba_t lba)
{
	return lba - spmap->sp_lba0;
}

static fx_lba_t spmap_pos_to_lba(const fx_spmap_t *spmap, size_t pos)
{
	return spmap->sp_lba0 + pos;
}

static int spmap_ismember(const fx_spmap_t *spmap, fx_lba_t lba)
{
	return (lba >= spmap->sp_lba0) && (lba < spmap_end(spmap));
}

int fx_spmap_hasfree(const fx_spmap_t *spmap, fx_bkcnt_t cnt)
{
	return ((spmap->sp_used + cnt) <= FNX_SPCNBK);
}

static void spmap_allocat(fx_spmap_t *spmap, fx_bkdsc_t *bkdsc)
{
	if (bkdsc->refs == 0) {
		/* New block allocation: mark as fuzzy */
		bkdsc->refs  = 1;
		bkdsc->flags = FNX_BLKF_FUZZY;
		spmap->sp_used++;
	} else {
		/* Increase reference to existing allocation */
		bkdsc->refs++;
	}
}

static void spmap_deallocat(fx_spmap_t *spmap, fx_bkdsc_t *bkdsc)
{
	if (bkdsc->refs > 1) {
		bkdsc->refs--;
	} else {
		bkdsc->refs  = 0;
		bkdsc->flags = 0;
		spmap->sp_used--;
	}
}

static int spmap_alloc_lba_from(fx_spmap_t *spmap, fx_lba_t beg, fx_lba_t *lba)
{
	size_t i, pos, nelems = FX_NELEMS(spmap->sp_bkd);
	fx_bkdsc_t *bkdsc;

	if (!spmap_ismember(spmap, beg)) {
		return -1;
	}
	pos = spmap_lba_to_pos(spmap, beg);
	for (i = 0; i < nelems; ++i) {
		bkdsc = &spmap->sp_bkd[pos];
		if (!bkdsc_isused(bkdsc)) {
			spmap_allocat(spmap, bkdsc);
			*lba = spmap_pos_to_lba(spmap, pos);
			return 0;
		}
		if (++pos >= nelems) {
			pos = 0;
		}
	}
	return -1;
}

static int spmap_alloc_lba(fx_spmap_t *spmap, fx_lba_t hint, fx_lba_t *lba)
{
	int rc = -1;

	if (spmap_ismember(spmap, hint)) {
		rc = spmap_alloc_lba_from(spmap, hint, lba);
	}
	if (rc != 0) {
		rc = spmap_alloc_lba_from(spmap, spmap->sp_lba0, lba);
	}
	fx_assert(rc == 0);
	return rc;
}

static int spmap_dealloc_lba(fx_spmap_t *spmap, const fx_lba_t lba)
{
	int rc = -1;
	size_t pos;
	fx_bkdsc_t *bkdsc;

	fx_assert(lba >= spmap->sp_lba0);
	fx_assert(lba < spmap_end(spmap));
	fx_assert(spmap->sp_used > 0);

	pos = spmap_lba_to_pos(spmap, lba);
	if (pos < FX_NELEMS(spmap->sp_bkd)) {
		bkdsc = &spmap->sp_bkd[pos];
		spmap_deallocat(spmap, bkdsc);
		rc = 0;
	}
	return rc;
}

int fx_spmap_isused(const fx_spmap_t *spmap, fx_lba_t lba)
{
	int res = FNX_FALSE;
	size_t pos;
	const fx_bkdsc_t *bkdsc;

	fx_assert(lba >= spmap->sp_lba0);
	pos = lba - spmap->sp_lba0;
	if (pos < FX_NELEMS(spmap->sp_bkd)) {
		bkdsc = &spmap->sp_bkd[pos];
		res = (bkdsc->refs > 0);
	}
	return res;
}

fx_bkdsc_t *fx_spmap_getdsc(fx_spmap_t *spmap, fx_lba_t lba)
{
	size_t pos;
	fx_bkdsc_t *bkdsc = NULL;

	if (lba >= spmap->sp_lba0) {
		pos = lba - spmap->sp_lba0;
		if (pos < FX_NELEMS(spmap->sp_bkd)) {
			bkdsc = &spmap->sp_bkd[pos];
		}
	}
	return bkdsc;
}


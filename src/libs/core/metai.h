/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_METAI_H_
#define FUNEX_METAI_H_

/* Up-case dblk --> dseg */
static inline fx_dsec_t *dblk_to_dseg(const fx_dblk_t *dblk)
{
	return fx_container_of(dblk, fx_dsec_t, sec_blk0);
}

/* Up-case header --> dfrg/dblk */
static inline fx_dfrg_t *header_to_dfrg(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_dfrg_t, fr_hdr);
}

/* Up-cast header --> dvobject */
static inline fx_dsuper_t *header_to_dsuper(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_dsuper_t, su_hdr);
}

static inline fx_dspmap_t *header_to_dspmap(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_dspmap_t, sp_hdr);
}

static inline fx_dinode_t *header_to_dinode(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_dinode_t, i_hdr);
}

static inline fx_ddir_t *dinode_to_ddir(const fx_dinode_t *dinode)
{
	return fx_container_of(dinode, fx_ddir_t, d_inode);
}

static inline fx_ddir_t *header_to_ddir(const fx_header_t *hdr)
{
	return dinode_to_ddir(header_to_dinode(hdr));
}

static inline fx_ddirext_t *header_to_ddirext(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_ddirext_t, dx_hdr);
}

static inline fx_ddirseg_t *header_to_ddirseg(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_ddirseg_t, ds_hdr);
}

static inline fx_dssymlnk_t *dinode_to_dssymlnk(const fx_dinode_t *dinode)
{
	return fx_container_of(dinode, fx_dssymlnk_t, sl_inode);
}

static inline fx_dsymlnk_t *dinode_to_dsymlnk(const fx_dinode_t *dinode)
{
	return fx_container_of(dinode, fx_dsymlnk_t, sl_inode);
}

static inline fx_dssymlnk_t *header_to_dssymlnk(const fx_header_t *hdr)
{
	return dinode_to_dssymlnk(header_to_dinode(hdr));
}

static inline fx_dsymlnk_t *header_to_dsymlnk(const fx_header_t *hdr)
{
	return dinode_to_dsymlnk(header_to_dinode(hdr));
}

static inline fx_dslice_t *header_to_dslice(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_dslice_t, sc_hdr);
}

static inline fx_dsegmnt_t *header_to_dsegmnt(const fx_header_t *hdr)
{
	return fx_container_of(hdr, fx_dsegmnt_t, sg_hdr);
}

#endif /* FUNEX_METAI_H_ */



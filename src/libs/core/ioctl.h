/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_IOCTL_H_
#define FUNEX_IOCTL_H_


typedef union fx_iocargs {
	struct fx_xcopy_req xcopy_req;
	struct fx_fquery_req stats_req;
	struct fx_fquery_res stats_res;

} fx_iocargs_t;


struct fx_iocdef {
	unsigned nmbr;
	unsigned cmd;
	unsigned size;
};
typedef struct fx_iocdef fx_iocdef_t;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const fx_iocdef_t *fx_iocdef_by_opc(fx_opcode_e);

const fx_iocdef_t *fx_iocdef_by_cmd(int cmd);


#endif /* FUNEX_IOCTL_H_ */





/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <uuid/uuid.h>
#include <sys/stat.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "htod.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Copy helpers */
static void
copy_nchars(char *t, size_t lim, size_t *len, const char *s, size_t n)
{
	fx_assert(s != NULL);
	fx_assert(t != NULL);
	fx_assert(n < lim);

	fx_bcopy(t, s, n);
	t[n] = '\0';
	if (len != NULL) {
		*len = n;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_name_init(fx_name_t *name)
{
	name->len    = 0;
	name->str[0] = '\0';
}

void fx_name_setup(fx_name_t *name, const char *str)
{
	const char *str2 = str ? str : "";
	fx_name_nsetup(name, str2, strlen(str2));
}

void fx_name_nsetup(fx_name_t *name, const char *str, size_t len)
{
	copy_nchars(name->str, sizeof(name->str), &name->len, str, len);
}

void fx_name_clone(const fx_name_t *from, fx_name_t *to)
{
	fx_name_nsetup(to, from->str, from->len);
}

int fx_name_isequal(const fx_name_t *name, const char *str)
{
	return fx_name_isnequal(name, str, strlen(str));
}

int fx_name_isnequal(const fx_name_t *name, const char *str, size_t len)
{
	return ((len == name->len) && (strncmp(name->str, str, len)) == 0);
}


void fx_path_init(fx_path_t *path)
{
	path->len    = 0;
	path->str[0] = '\0';
}

void fx_path_setup(fx_path_t *path, const char *str)
{
	const char *str2 = str ? str : "";
	fx_path_nsetup(path, str2, strlen(str2));
}

void fx_path_nsetup(fx_path_t *path, const char *str, size_t len)
{
	copy_nchars(path->str, sizeof(path->str), &path->len, str, len);
}

void fx_path_clone(fx_path_t *path, const fx_path_t *buf)
{
	copy_nchars(path->str, sizeof(path->str), &path->len, buf->str, buf->len);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_isvmeta(fx_vtype_e vtype)
{
	int res;

	switch (vtype) {
		case FNX_VTYPE_SUPER:
		case FNX_VTYPE_CHRDEV:
		case FNX_VTYPE_BLKDEV:
		case FNX_VTYPE_SOCK:
		case FNX_VTYPE_FIFO:
		case FNX_VTYPE_SSYMLNK:
		case FNX_VTYPE_SYMLNK:
		case FNX_VTYPE_REFLNK:
		case FNX_VTYPE_DIR:
		case FNX_VTYPE_DIREXT:
		case FNX_VTYPE_DIRSEG:
		case FNX_VTYPE_REG:
		case FNX_VTYPE_SEGMNT:
		case FNX_VTYPE_SLICE:
		case FNX_VTYPE_SPMAP:
			res = FNX_TRUE;
			break;
		case FNX_VTYPE_BK:
		case FNX_VTYPE_SEC:
		case FNX_VTYPE_NONE:
		case FNX_VTYPE_EMPTY:
		case FNX_VTYPE_ANY:
		default:
			res = FNX_FALSE;
			break;
	}
	return res;
}

int fx_isitype(fx_vtype_e vtype)
{
	int res;

	switch (vtype) {
		case FNX_VTYPE_DIR:
		case FNX_VTYPE_REG:
		case FNX_VTYPE_CHRDEV:
		case FNX_VTYPE_BLKDEV:
		case FNX_VTYPE_SOCK:
		case FNX_VTYPE_FIFO:
		case FNX_VTYPE_SSYMLNK:
		case FNX_VTYPE_SYMLNK:
		case FNX_VTYPE_REFLNK:
			res = FNX_TRUE;
			break;
		case FNX_VTYPE_NONE:
		case FNX_VTYPE_SUPER:
		case FNX_VTYPE_DIREXT:
		case FNX_VTYPE_DIRSEG:
		case FNX_VTYPE_SLICE:
		case FNX_VTYPE_SEGMNT:
		case FNX_VTYPE_SPMAP:
		case FNX_VTYPE_BK:
		case FNX_VTYPE_SEC:
		case FNX_VTYPE_EMPTY:
		case FNX_VTYPE_ANY:
		default:
			res = FNX_FALSE;
			break;
	}
	return res;
}

int fx_ismetasec(fx_size_t sec_idx)
{
	return (sec_idx == FNX_SEC_SUPER);
}

/*
 * Ino number is valid as key to inode-object iff:
 * 1. Not equal to NULL (zero)
 * 2. Has value less then 2^56 (due to representation of on-disk dirent)
 */
int fx_check_ino(fx_ino_t ino)
{
	const fx_ino_t ino_max = FNX_INO_MAX;
	return ((ino != FNX_INO_NULL) && (ino <= ino_max)) ? 0 : -1;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void cred_init(fx_cred_t *cred)
{
	cred->cr_pid   = -1;
	cred->cr_uid   = (fx_uid_t)FNX_UID_NULL;
	cred->cr_gid   = (fx_gid_t)FNX_GID_NULL;
	cred->cr_umask = (fx_mode_t)(0);
	cred->cr_ngids = 0;
}

static void cred_destroy(fx_cred_t *cred)
{
	cred->cr_pid   = -1;
	cred->cr_uid   = (fx_uid_t)FNX_UID_NULL;
	cred->cr_gid   = (fx_gid_t)FNX_GID_NULL;
	cred->cr_umask = (fx_mode_t)(0);
	cred->cr_ngids = 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_userctx_init(fx_uctx_t *uctx)
{
	cred_init(&uctx->u_cred);
	uctx->u_capf = 0;
	uctx->u_root = FNX_FALSE;
}

void fx_userctx_destroy(fx_uctx_t *uctx)
{
	cred_destroy(&uctx->u_cred);
	uctx->u_capf = 0;
	uctx->u_root = FNX_FALSE;
}

int fx_userctx_iscap(const fx_uctx_t *uctx, fx_capf_t mask)
{
	return fx_testlf(uctx->u_capf, mask);
}

int fx_isprivileged(const fx_uctx_t *uctx)
{
	return (uctx->u_root || fx_userctx_iscap(uctx, FNX_CAPF_ADMIN));
}

int fx_isgroupmember(const fx_uctx_t *uctx, fx_gid_t gid)
{
	size_t i;
	const fx_cred_t *cred;

	cred = &uctx->u_cred;
	if (cred->cr_gid == gid) {
		return FNX_TRUE;
	}
	for (i = 0; i < cred->cr_ngids; ++i) {
		if (cred->cr_gids[i] == gid) {
			return FNX_TRUE;
		}
	}
	return FNX_FALSE;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_times_init(fx_times_t *tm)
{
	fx_timespec_init(&tm->btime);
	fx_timespec_init(&tm->atime);
	fx_timespec_init(&tm->ctime);
	fx_timespec_init(&tm->mtime);
}

void fx_times_copy(fx_times_t *tgt, const fx_times_t *src)
{
	fx_bcopy(tgt, src, sizeof(*tgt));
}

void fx_times_fill(fx_times_t *tm, fx_flags_t flags, const fx_times_t *tm_src)
{
	fx_timespec_t ts = { 0, 0 };
	const fx_timespec_t *ts_now = &ts;

	if (flags & FNX_BAMCTIME_NOW) {
		fx_timespec_gettimeofday(&ts);

		if (flags & FNX_BTIME_NOW) {
			fx_timespec_copy(&tm->btime, ts_now);
		}
		if (flags & FNX_ATIME_NOW) {
			fx_timespec_copy(&tm->atime, ts_now);
		}
		if (flags & FNX_MTIME_NOW) {
			fx_timespec_copy(&tm->mtime, ts_now);
		}
		if (flags & FNX_CTIME_NOW) {
			fx_timespec_copy(&tm->ctime, ts_now);
		}
	}

	if (tm_src != NULL) {
		if (flags & FNX_BTIME) {
			fx_timespec_copy(&tm->btime, &tm_src->btime);
		}
		if (flags & FNX_ATIME)  {
			fx_timespec_copy(&tm->atime, &tm_src->atime);
		}
		if (flags & FNX_MTIME) {
			fx_timespec_copy(&tm->mtime, &tm_src->mtime);
		}
		if (flags & FNX_CTIME)  {
			fx_timespec_copy(&tm->ctime, &tm_src->ctime);
		}
	}
}

void fx_times_dtoh(fx_times_t *tms, const fx_dtimes_t *dtms)
{
	fx_dtoh_timespec(&tms->btime, dtms->btime_sec, dtms->btime_nsec);
	fx_dtoh_timespec(&tms->atime, dtms->atime_sec, dtms->atime_nsec);
	fx_dtoh_timespec(&tms->mtime, dtms->mtime_sec, dtms->mtime_nsec);
	fx_dtoh_timespec(&tms->ctime, dtms->ctime_sec, dtms->ctime_nsec);
}

void fx_times_htod(const fx_times_t *tms, fx_dtimes_t *dtms)
{
	fx_htod_timespec(&dtms->btime_sec, &dtms->btime_nsec, &tms->btime);
	fx_htod_timespec(&dtms->atime_sec, &dtms->atime_nsec, &tms->atime);
	fx_htod_timespec(&dtms->mtime_sec, &dtms->mtime_nsec, &tms->mtime);
	fx_htod_timespec(&dtms->ctime_sec, &dtms->ctime_nsec, &tms->ctime);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void fx_uuid_setstr(fx_uuid_t *uu)
{
	fx_uuid_unparse(uu, uu->str, sizeof(uu->str));
}

static void fx_uuid_clearstr(fx_uuid_t *uu)
{
	uu->uu[0] = '\0';
}

void fx_uuid_clear(fx_uuid_t *uu)
{
	uuid_clear(uu->uu);
	fx_uuid_clearstr(uu);
}

void fx_uuid_copy(fx_uuid_t *dst_uu, const fx_uuid_t *src_uu)
{
	uuid_copy(dst_uu->uu, src_uu->uu);
	fx_uuid_setstr(dst_uu);
}

void fx_uuid_generate(fx_uuid_t *uu)
{
	uuid_generate(uu->uu);
	fx_uuid_setstr(uu);
}

int fx_uuid_isnull(const fx_uuid_t *uu)
{
	return uuid_is_null(uu->uu);
}

int fx_uuid_compare(const fx_uuid_t *uu1, const fx_uuid_t *uu2)
{
	return uuid_compare(uu1->uu, uu2->uu);
}

int fx_uuid_isequal(const fx_uuid_t *uu1, const fx_uuid_t *uu2)
{
	return (fx_uuid_compare(uu1, uu2) == 0);
}

int fx_uuid_parse(fx_uuid_t *uu, const char *s)
{
	int rc;
	size_t len, bsz;
	char buf[40];

	rc  = -1;
	bsz = sizeof(buf);
	len = strlen(s);
	if ((len > 0) && (len < bsz)) {
		strncpy(buf, s, len + 1);
		rc = uuid_parse(buf, uu->uu);
	}
	return rc;
}

int fx_uuid_unparse(const fx_uuid_t *uu, char *s, size_t sz)
{
	int rc;

	rc  = -1;
	if (sz >= 37) {
		uuid_unparse(uu->uu, s);
		rc = 0;
	}
	return rc;
}

void fx_uuid_assign(fx_uuid_t *uu, const unsigned char src_uu[16])
{
	FX_STATICASSERT_EQ(sizeof(uu->uu), 16);

	fx_bcopy(uu->uu, src_uu, sizeof(uu->uu));
	fx_uuid_setstr(uu);
}

void fx_uuid_copyto(const fx_uuid_t *uu, unsigned char tgt_uu[16])
{
	FX_STATICASSERT_EQ(sizeof(uu->uu), 16);

	fx_bcopy(tgt_uu, uu->uu, 16);
}

uint64_t fx_uuid_hi(fx_uuid_t *uu)
{
	size_t i;
	uint64_t hi;

	hi = 0;
	for (i = 0; i < 8; ++i) {
		hi = (hi << (i * 8)) | (uint64_t)(uu->uu[i]);
	}
	return hi;
}

uint64_t fx_uuid_lo(fx_uuid_t *uu)
{
	size_t i;
	uint64_t lo;

	lo = 0;
	for (i = 0; i < 8; ++i) {
		lo = (lo << (i * 8)) | (uint64_t)(uu->uu[i + 8]);
	}
	return lo;
}



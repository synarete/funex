/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <limits.h>
#include <sys/mount.h>
#include <sys/capability.h>
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "htod.h"
#include "addr.h"
#include "elems.h"
#include "blobs.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* On-disk meta-data objects bytes-sizes, repr in fragment (512) units */
#define FX_NRECS(n)             (FNX_RECORDSIZE * n)
#define FX_NFGRS(n)             (FNX_FRGSIZE * n)
#define FX_DDIRSIZE             FX_NFGRS(8)
#define FX_DINODESIZE           FX_NFGRS(1)


#define N2U(n, u)               ((n + u - 1) / u)
#define SIZE_IN_FRG(sz)         N2U(sz, FNX_FRGSIZE)
#define FRG_PER_BLK             SIZE_IN_FRG(FNX_BLKSIZE)
#define FRG_PER_DIRB            SIZE_IN_FRG(FX_DDIRSIZE)
#define FRG_PER_DSEC            SIZE_IN_FRG(FNX_DSECSIZE)

#define N2S(n)                  N2U(FNX_DSECSIZE, n)
#define INODEB_PER_SECTION      N2S(FX_DINODESIZE)
#define DIRB_PER_SECTION        N2S(FX_DDIRSIZE)

#define BLKS_PER_SECTION        (FNX_DSECSIZE / FNX_BLKSIZE)


#define staticassert_eq(a, b) \
	FX_STATICASSERT_EQ(a, b)
#define staticassert_sizeof(type, size) \
	staticassert_eq(sizeof(type), size)
#define staticassert_nfrgsz(type, nfrg) \
	staticassert_eq(sizeof(type), nfrg * FNX_FRGSIZE)
#define staticassert_eqmemsz(type, field, size) \
	staticassert_eq(FX_FIELD_SIZEOF(type, field), size)
#define staticassert_eqarrsz(type, field, size) \
	staticassert_eq(FX_ARRFIELD_NELEMS(type, field), size)

#define check_capf(f, c)    staticassert_eq(f, (1 << c))

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void verify_defs(void)
{
	staticassert_eq(FNX_BLKSIZE,    FNX_BLKNFRG * FNX_FRGSIZE);
	staticassert_eq(FNX_SECSIZE,    FNX_SECNFRG * FNX_FRGSIZE);
	staticassert_eq(FNX_SECSIZE,    FNX_SECNBK * FNX_BLKSIZE);
	staticassert_eq(FNX_SEGSIZE,    FNX_SEGNBK * FNX_BLKSIZE);
	staticassert_eq(FNX_SECSIZE,    FNX_BLKSIZE * FNX_SECNBK);
	staticassert_eq(FNX_SLICESIZE,  FNX_SEGSIZE * FNX_SLICENSEG);
	staticassert_eq(FNX_SPCSIZE,    FNX_SPCNBK * FNX_BLKSIZE);
	staticassert_eq(FNX_STUSIZE,    FNX_STUNBK * FNX_BLKSIZE);

	staticassert_eq(FNX_PATH_MAX,   PATH_MAX);
	staticassert_eq(FNX_NAME_MAX,   NAME_MAX);
}

static void verify_types(void)
{
	/* Funex requires 64-bits platform */
	staticassert_eq(sizeof(size_t),         sizeof(uint64_t));
	staticassert_eq(sizeof(off_t),          sizeof(int64_t));
	staticassert_eq(sizeof(ino_t),          sizeof(uint64_t));

	staticassert_eq(sizeof(fx_blk_t),       FNX_BLKSIZE);
	staticassert_eq(sizeof(fx_section_t),   FNX_SECSIZE);
	staticassert_eq(sizeof(fx_record_t),    FNX_RECORDSIZE);
}

static void verify_dobjs(void)
{
	staticassert_sizeof(fx_dsuper_t,    FNX_BLKSIZE);
	staticassert_sizeof(fx_dsuper_t,    FX_NFGRS(8));
	staticassert_sizeof(fx_dspmap_t,    FX_NFGRS(4));

	staticassert_sizeof(fx_header_t,    FNX_HDRSIZE);
	staticassert_sizeof(fx_dfrg_t,      FNX_FRGSIZE);
	staticassert_sizeof(fx_dblk_t,      FNX_BLKSIZE);
	staticassert_sizeof(fx_dsec_t,      FNX_DSECSIZE);
	staticassert_nfrgsz(fx_dsec_t,      FRG_PER_DSEC);

	staticassert_sizeof(fx_diattr_t,    FX_NRECS(2));
	staticassert_sizeof(fx_dfsattr_t,   FX_NRECS(1));
	staticassert_sizeof(fx_dfsstat_t,   FX_NRECS(2));
	staticassert_sizeof(fx_dvstats_t,   FX_NRECS(2));
	staticassert_sizeof(fx_dlayout_t,   FX_NRECS(2));

	staticassert_sizeof(fx_dinode_t,    FX_NFGRS(1));
	staticassert_sizeof(fx_dinode_t,    FX_NFGRS(1));
	staticassert_sizeof(fx_ddir_t,      FX_DDIRSIZE);
	staticassert_nfrgsz(fx_ddir_t,      FRG_PER_DIRB);
	staticassert_sizeof(fx_ddirext_t,   FX_NFGRS(8));
	staticassert_sizeof(fx_ddirseg_t,   FX_NFGRS(2));
	staticassert_sizeof(fx_dssymlnk_t,  FX_NFGRS(2));
	staticassert_sizeof(fx_dsymlnk_t,   FX_NFGRS(9));
	staticassert_sizeof(fx_dslice_t,    FX_NFGRS(1));
	staticassert_sizeof(fx_dsegmnt_t,   FX_NFGRS(1));

	staticassert_sizeof(fx_dxref_t,     FX_NRECS(1));
	staticassert_sizeof(fx_duber_t,     FNX_DSECSIZE);
}

static void verify_dmembers(void)
{
	staticassert_eqmemsz(fx_dfsattr_t,  f_uuid,     FX_UUIDSIZE);
	staticassert_eqarrsz(fx_dblk_t,     bk_frg,     FRG_PER_BLK);
	staticassert_eqarrsz(fx_dsec_t,     sec_blks,   BLKS_PER_SECTION);
	staticassert_eqarrsz(fx_dsec_t,     sec_dirs,   DIRB_PER_SECTION);
	staticassert_eqarrsz(fx_dsec_t,     sec_inodes, INODEB_PER_SECTION);
	staticassert_eqarrsz(fx_ddir_t,     d_dent,     FNX_DIRTOP_NDENT);
	staticassert_eqmemsz(fx_dssymlnk_t, sl_lnk,     FNX_SSYMLNK_MAX);
	staticassert_eqmemsz(fx_dsymlnk_t,  sl_lnk,     FNX_SYMLNK_MAX);
	staticassert_eqarrsz(fx_dsegmnt_t,  sg_bkaddr,  FNX_SEGNBK);
	staticassert_eqarrsz(fx_dslice_t,   sc_bitmap,  FNX_SLICENSEG / 32);
	staticassert_eqarrsz(fx_dspmap_t,   sp_bkd,     FNX_SPCNBK);
	staticassert_eqarrsz(fx_ddirext_t,  dx_dent,    FNX_DIREXT_NDENT);
	staticassert_eqarrsz(fx_ddirext_t,  dx_segs,    FNX_DIREXT_NSEGS / 32);
	staticassert_eqarrsz(fx_ddirseg_t,  ds_dent,    FNX_DIRSEG_NDENT);
	staticassert_eqarrsz(fx_duber_t,    u_xref,     FNX_STUENBK);
}

static void verify_mntf(void)
{
	staticassert_eq(FNX_MNTF_RDONLY,     MS_RDONLY);
	staticassert_eq(FNX_MNTF_NOSUID,     MS_NOSUID);
	staticassert_eq(FNX_MNTF_NODEV,      MS_NODEV);
	staticassert_eq(FNX_MNTF_NOEXEC,     MS_NOEXEC);
	staticassert_eq(FNX_MNTF_MANDLOCK,   MS_MANDLOCK);
	staticassert_eq(FNX_MNTF_DIRSYNC,    MS_DIRSYNC);
	staticassert_eq(FNX_MNTF_NOATIME,    MS_NOATIME);
	staticassert_eq(FNX_MNTF_NODIRATIME, MS_NODIRATIME);
	staticassert_eq(FNX_MNTF_RELATIME,   MS_RELATIME);
}

static void verify_capf(void)
{
	check_capf(FNX_CAPF_CHOWN,   CAP_CHOWN);
	check_capf(FNX_CAPF_FOWNER,  CAP_FOWNER);
	check_capf(FNX_CAPF_FSETID,  CAP_FSETID);
	check_capf(FNX_CAPF_ADMIN,   CAP_SYS_ADMIN);
	check_capf(FNX_CAPF_MKNOD,   CAP_MKNOD);
}

static void verify_blobs(void)
{
	staticassert_sizeof(fx_blob_t, FX_BLOBSIZE);
}

void fx_verify_core(void)
{
	verify_defs();
	verify_types();
	verify_dobjs();
	verify_dmembers();
	verify_mntf();
	verify_capf();
	verify_blobs();
}


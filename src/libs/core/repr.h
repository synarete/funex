/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_REPR_H_
#define FUNEX_REPR_H_


void fx_repr_intval(fx_substr_t *, int);

void fx_repr_uuid(fx_substr_t *, const fx_uuid_t *);

void fx_repr_times(fx_substr_t *, const fx_times_t *, const char *);

void fx_repr_fsattr(fx_substr_t *, const fx_fsattr_t *, const char *);

void fx_repr_fsstat(fx_substr_t *, const fx_fsstat_t *, const char *);

void fx_repr_vstats(fx_substr_t *, const fx_vstats_t *, const char *);

void fx_repr_iostat(fx_substr_t *, const fx_iostat_t *, const char *);

void fx_repr_opstat(fx_substr_t *, const fx_opstat_t *, const char *);

void fx_repr_layout(fx_substr_t *, const fx_layout_t *, const char *);

void fx_repr_super(fx_substr_t *, const fx_super_t *, const char *);

void fx_repr_cstats(fx_substr_t *, const fx_cstats_t *, const char *);

void fx_repr_climits(fx_substr_t *, const fx_climits_t *, const char *);

void fx_repr_iattr(fx_substr_t *, const fx_iattr_t *, const char *);

void fx_repr_inode(fx_substr_t *, const fx_inode_t *, const char *);

void fx_repr_dir(fx_substr_t *, const fx_dir_t *, const char *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_parse_param(const fx_substr_t *, int *);

#endif /* FUNEX_REPR_H_ */

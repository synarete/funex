/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "metai.h"
#include "index.h"
#include "addr.h"
#include "elems.h"
#include "super.h"
#include "inode.h"
#include "inodei.h"
#include "dir.h"
#include "file.h"
#include "iobuf.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_super(fx_vnode_t *vnode)
{
	fx_super_init(fx_vnode_to_super(vnode));
}

static void destroy_super(fx_vnode_t *vnode)
{
	fx_super_destroy(fx_vnode_to_super(vnode));
}

static void dexport_super(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_super_htod(fx_vnode_to_super(vnode), header_to_dsuper(hdr));
}

static void dimport_super(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_super_dtoh(fx_vnode_to_super(vnode), header_to_dsuper(hdr));
}

static int dcheck_super(const fx_header_t *hdr)
{
	return fx_super_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_spmap(fx_vnode_t *vnode)
{
	fx_spmap_init(fx_vnode_to_spmap(vnode));
}

static void destroy_spmap(fx_vnode_t *vnode)
{
	fx_spmap_destroy(fx_vnode_to_spmap(vnode));
}

static void dexport_spmap(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_spmap_htod(fx_vnode_to_spmap(vnode), header_to_dspmap(hdr));
}

static void dimport_spmap(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_spmap_dtoh(fx_vnode_to_spmap(vnode), header_to_dspmap(hdr));
}

static int dcheck_spmap(const fx_header_t *hdr)
{
	return fx_spmap_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_dir(fx_vnode_t *vnode)
{
	fx_dir_init(fx_vnode_to_dir(vnode));
}

static void destroy_dir(fx_vnode_t *vnode)
{
	fx_dir_destroy(fx_vnode_to_dir(vnode));
}

static void dexport_dir(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_dir_htod(fx_vnode_to_dir(vnode), header_to_ddir(hdr));
}

static void dimport_dir(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_dir_dtoh(fx_vnode_to_dir(vnode), header_to_ddir(hdr));
}

static int dcheck_dir(const fx_header_t *hdr)
{
	return fx_dir_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_dirext(fx_vnode_t *vnode)
{
	fx_dirext_init(fx_vnode_to_dirext(vnode));
}

static void destroy_dirext(fx_vnode_t *vnode)
{
	fx_dirext_destroy(fx_vnode_to_dirext(vnode));
}

static void dexport_dirext(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_dirext_htod(fx_vnode_to_dirext(vnode), header_to_ddirext(hdr));
}

static void dimport_dirext(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_dirext_dtoh(fx_vnode_to_dirext(vnode), header_to_ddirext(hdr));
}

static int dcheck_dirext(const fx_header_t *hdr)
{
	return fx_dirext_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_dirseg(fx_vnode_t *vnode)
{
	fx_dirseg_init(fx_vnode_to_dirseg(vnode));
}

static void destroy_dirseg(fx_vnode_t *vnode)
{
	fx_dirseg_destroy(fx_vnode_to_dirseg(vnode));
}

static void dexport_dirseg(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_dirseg_htod(fx_vnode_to_dirseg(vnode), header_to_ddirseg(hdr));
}

static void dimport_dirseg(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_dirseg_dtoh(fx_vnode_to_dirseg(vnode), header_to_ddirseg(hdr));
}

static int dcheck_dirseg(const fx_header_t *hdr)
{
	return fx_dirseg_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_chrdev(fx_vnode_t *vnode)
{
	fx_inode_init(vnode_to_inode(vnode), FNX_VTYPE_CHRDEV);
}

static void destroy_chrdev(fx_vnode_t *vnode)
{
	fx_inode_destroy(vnode_to_inode(vnode));
}

static void dexport_chrdev(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_inode_htod(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static void dimport_chrdev(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_inode_dtoh(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static int dcheck_chrdev(const fx_header_t *hdr)
{
	return fx_inode_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_blkdev(fx_vnode_t *vnode)
{
	fx_inode_init(vnode_to_inode(vnode), FNX_VTYPE_BLKDEV);
}

static void destroy_blkdev(fx_vnode_t *vnode)
{
	fx_inode_destroy(vnode_to_inode(vnode));
}

static void dexport_blkdev(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_inode_htod(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static void dimport_blkdev(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_inode_dtoh(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static int dcheck_blkdev(const fx_header_t *hdr)
{
	return fx_inode_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_sock(fx_vnode_t *vnode)
{
	fx_inode_init(vnode_to_inode(vnode), FNX_VTYPE_SOCK);
}

static void destroy_sock(fx_vnode_t *vnode)
{
	fx_inode_destroy(vnode_to_inode(vnode));
}

static void dexport_sock(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_inode_htod(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static void dimport_sock(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_inode_dtoh(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static int dcheck_sock(const fx_header_t *hdr)
{
	return fx_inode_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_fifo(fx_vnode_t *vnode)
{
	fx_inode_init(vnode_to_inode(vnode), FNX_VTYPE_FIFO);
}

static void destroy_fifo(fx_vnode_t *vnode)
{
	fx_inode_destroy(vnode_to_inode(vnode));
}

static void dexport_fifo(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_inode_htod(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static void dimport_fifo(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_inode_dtoh(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static int dcheck_fifo(const fx_header_t *hdr)
{
	return fx_inode_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_reflnk(fx_vnode_t *vnode)
{
	fx_inode_init(vnode_to_inode(vnode), FNX_VTYPE_REFLNK);
}

static void destroy_reflnk(fx_vnode_t *vnode)
{
	fx_inode_destroy(vnode_to_inode(vnode));
}

static void dexport_reflnk(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_inode_htod(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static void dimport_reflnk(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_inode_dtoh(vnode_to_inode(vnode), header_to_dinode(hdr));
}

static int dcheck_reflnk(const fx_header_t *hdr)
{
	return fx_inode_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_ssymlnk(fx_vnode_t *vnode)
{
	fx_symlnk_init(fx_vnode_to_symlnk(vnode), 0);
}

static void destroy_ssymlnk(fx_vnode_t *vnode)
{
	fx_symlnk_destroy(fx_vnode_to_symlnk(vnode));
}

static void dexport_ssymlnk(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_ssymlnk_htod(fx_vnode_to_symlnk(vnode), header_to_dssymlnk(hdr));
}

static void dimport_ssymlnk(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_ssymlnk_dtoh(fx_vnode_to_symlnk(vnode), header_to_dssymlnk(hdr));
}

static int dcheck_ssymlnk(const fx_header_t *hdr)
{
	return fx_symlnk_dcheck(hdr);
}

static void init_symlnk(fx_vnode_t *vnode)
{
	fx_symlnk_init(fx_vnode_to_symlnk(vnode), 1);
}

static void destroy_symlnk(fx_vnode_t *vnode)
{
	fx_symlnk_destroy(fx_vnode_to_symlnk(vnode));
}

static void dexport_symlnk(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_symlnk_htod(fx_vnode_to_symlnk(vnode), header_to_dsymlnk(hdr));
}

static void dimport_symlnk(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_symlnk_dtoh(fx_vnode_to_symlnk(vnode), header_to_dsymlnk(hdr));
}

static int dcheck_symlnk(const fx_header_t *hdr)
{
	return fx_symlnk_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_reg(fx_vnode_t *vnode)
{
	fx_regfile_init(fx_vnode_to_regfile(vnode));
}

static void destroy_reg(fx_vnode_t *vnode)
{
	fx_regfile_destroy(fx_vnode_to_regfile(vnode));
}

static void dexport_reg(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_regfile_htod(fx_vnode_to_regfile(vnode), header_to_dinode(hdr));
}

static void dimport_reg(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_regfile_dtoh(fx_vnode_to_regfile(vnode), header_to_dinode(hdr));
}

static int dcheck_reg(const fx_header_t *hdr)
{
	return fx_inode_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_slice(fx_vnode_t *vnode)
{
	fx_slice_init(fx_vnode_to_slice(vnode));
}

static void destroy_slice(fx_vnode_t *vnode)
{
	fx_slice_destroy(fx_vnode_to_slice(vnode));
}

static void dexport_slice(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_slice_htod(fx_vnode_to_slice(vnode), header_to_dslice(hdr));
}

static void dimport_slice(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_slice_dtoh(fx_vnode_to_slice(vnode), header_to_dslice(hdr));
}

static int dcheck_slice(const fx_header_t *hdr)
{
	return fx_slice_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void init_segmnt(fx_vnode_t *vnode)
{
	fx_segmnt_init(fx_vnode_to_segmnt(vnode));
}

static void destroy_segmnt(fx_vnode_t *vnode)
{
	fx_segmnt_destroy(fx_vnode_to_segmnt(vnode));
}

static void dexport_segmnt(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	fx_segmnt_htod(fx_vnode_to_segmnt(vnode), header_to_dsegmnt(hdr));
}

static void dimport_segmnt(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	fx_segmnt_dtoh(fx_vnode_to_segmnt(vnode), header_to_dsegmnt(hdr));
}

static int dcheck_segmnt(const fx_header_t *hdr)
{
	return fx_segmnt_dcheck(hdr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const fx_vinfo_t s_vinfo_none = {
	.name       = "NONE",
};

static const fx_vinfo_t s_vinfo_empty = {
	.name       = "EMPTY",
};

static const fx_vinfo_t s_vinfo_any = {
	.name       = "ANY",
};

static const fx_vinfo_t s_vinfo_super = {
	.name       = "SUPER",
	.size       = sizeof(fx_super_t),
	.dsize      = sizeof(fx_dsuper_t),
	.init       = init_super,
	.destroy    = destroy_super,
	.dexport    = dexport_super,
	.dimport    = dimport_super,
	.dcheck     = dcheck_super,
};

static const fx_vinfo_t s_vinfo_spmap = {
	.name       = "SPMAP",
	.size       = sizeof(fx_spmap_t),
	.dsize      = sizeof(fx_dspmap_t),
	.init       = init_spmap,
	.destroy    = destroy_spmap,
	.dexport    = dexport_spmap,
	.dimport    = dimport_spmap,
	.dcheck     = dcheck_spmap
};

static const fx_vinfo_t s_vinfo_dir = {
	.name       = "DIR",
	.size       = sizeof(fx_dir_t),
	.dsize      = sizeof(fx_ddir_t),
	.init       = init_dir,
	.destroy    = destroy_dir,
	.dexport    = dexport_dir,
	.dimport    = dimport_dir,
	.dcheck     = dcheck_dir
};

static const fx_vinfo_t s_vinfo_dirext = {
	.name       = "DIREXT",
	.size       = sizeof(fx_dirext_t),
	.dsize      = sizeof(fx_ddirext_t),
	.init       = init_dirext,
	.destroy    = destroy_dirext,
	.dexport    = dexport_dirext,
	.dimport    = dimport_dirext,
	.dcheck     = dcheck_dirext
};

static const fx_vinfo_t s_vinfo_dirseg = {
	.name       = "DIRSEG",
	.size       = sizeof(fx_dirseg_t),
	.dsize      = sizeof(fx_ddirseg_t),
	.init       = init_dirseg,
	.destroy    = destroy_dirseg,
	.dexport    = dexport_dirseg,
	.dimport    = dimport_dirseg,
	.dcheck     = dcheck_dirseg
};

static const fx_vinfo_t s_vinfo_chrdev = {
	.name       = "CHRDEV",
	.size       = sizeof(fx_inode_t),
	.dsize      = sizeof(fx_dinode_t),
	.init       = init_chrdev,
	.destroy    = destroy_chrdev,
	.dexport    = dexport_chrdev,
	.dimport    = dimport_chrdev,
	.dcheck     = dcheck_chrdev
};

static const fx_vinfo_t s_vinfo_blkdev = {
	.name       = "BLKDEV",
	.size       = sizeof(fx_inode_t),
	.dsize      = sizeof(fx_dinode_t),
	.init       = init_blkdev,
	.destroy    = destroy_blkdev,
	.dexport    = dexport_blkdev,
	.dimport    = dimport_blkdev,
	.dcheck     = dcheck_blkdev
};

static const fx_vinfo_t s_vinfo_sock = {
	.name       = "SOCK",
	.size       = sizeof(fx_inode_t),
	.dsize      = sizeof(fx_dinode_t),
	.init       = init_sock,
	.destroy    = destroy_sock,
	.dexport    = dexport_sock,
	.dimport    = dimport_sock,
	.dcheck     = dcheck_sock,
};

static const fx_vinfo_t s_vinfo_fifo = {
	.name       = "FIFO",
	.size       = sizeof(fx_inode_t),
	.dsize      = sizeof(fx_dinode_t),
	.init       = init_fifo,
	.destroy    = destroy_fifo,
	.dexport    = dexport_fifo,
	.dimport    = dimport_fifo,
	.dcheck     = dcheck_fifo
};

static const fx_vinfo_t s_vinfo_reflnk = {
	.name       = "REFLNK",
	.size       = sizeof(fx_inode_t),
	.dsize      = sizeof(fx_dinode_t),
	.init       = init_reflnk,
	.destroy    = destroy_reflnk,
	.dexport    = dexport_reflnk,
	.dimport    = dimport_reflnk,
	.dcheck     = dcheck_reflnk
};

static const fx_vinfo_t s_vinfo_ssymlnk = {
	.name       = "SSYMLNK",
	.size       = sizeof(fx_symlnk_t),
	.dsize      = sizeof(fx_dssymlnk_t),
	.init       = init_ssymlnk,
	.destroy    = destroy_ssymlnk,
	.dexport    = dexport_ssymlnk,
	.dimport    = dimport_ssymlnk,
	.dcheck     = dcheck_ssymlnk
};

static const fx_vinfo_t s_vinfo_symlnk = {
	.name       = "SYMLNK",
	.size       = sizeof(fx_symlnk_t),
	.dsize      = sizeof(fx_dsymlnk_t),
	.init       = init_symlnk,
	.destroy    = destroy_symlnk,
	.dexport    = dexport_symlnk,
	.dimport    = dimport_symlnk,
	.dcheck     = dcheck_symlnk
};

static const fx_vinfo_t s_vinfo_reg = {
	.name       = "REG",
	.size       = sizeof(fx_inode_t),
	.dsize      = sizeof(fx_dinode_t),
	.init       = init_reg,
	.destroy    = destroy_reg,
	.dexport    = dexport_reg,
	.dimport    = dimport_reg,
	.dcheck     = dcheck_reg
};

static const fx_vinfo_t s_vinfo_slice = {
	.name       = "SLICE",
	.size       = sizeof(fx_slice_t),
	.dsize      = sizeof(fx_dslice_t),
	.init       = init_slice,
	.destroy    = destroy_slice,
	.dexport    = dexport_slice,
	.dimport    = dimport_slice,
	.dcheck     = dcheck_slice
};

static const fx_vinfo_t s_vinfo_segmnt = {
	.name       = "SEGMNT",
	.size       = sizeof(fx_segmnt_t),
	.dsize      = sizeof(fx_dsegmnt_t),
	.init       = init_segmnt,
	.destroy    = destroy_segmnt,
	.dexport    = dexport_segmnt,
	.dimport    = dimport_segmnt,
	.dcheck     = dcheck_segmnt
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const fx_vinfo_t *s_vinfo_tbl[] = {
	[FNX_VTYPE_NONE]     = &s_vinfo_none,
	[FNX_VTYPE_SUPER]    = &s_vinfo_super,
	[FNX_VTYPE_DIR]      = &s_vinfo_dir,
	[FNX_VTYPE_DIREXT]   = &s_vinfo_dirext,
	[FNX_VTYPE_DIRSEG]   = &s_vinfo_dirseg,
	[FNX_VTYPE_SPMAP]    = &s_vinfo_spmap,
	[FNX_VTYPE_CHRDEV]   = &s_vinfo_chrdev,
	[FNX_VTYPE_BLKDEV]   = &s_vinfo_blkdev,
	[FNX_VTYPE_SOCK]     = &s_vinfo_sock,
	[FNX_VTYPE_FIFO]     = &s_vinfo_fifo,
	[FNX_VTYPE_SSYMLNK]  = &s_vinfo_ssymlnk,
	[FNX_VTYPE_SYMLNK]   = &s_vinfo_symlnk,
	[FNX_VTYPE_REFLNK]   = &s_vinfo_reflnk,
	[FNX_VTYPE_REG]      = &s_vinfo_reg,
	[FNX_VTYPE_SLICE]    = &s_vinfo_slice,
	[FNX_VTYPE_SEGMNT]   = &s_vinfo_segmnt,
	[FNX_VTYPE_EMPTY]    = &s_vinfo_empty,
	[FNX_VTYPE_ANY]      = &s_vinfo_any
};


const fx_vinfo_t *fx_get_vinfo(fx_vtype_e vtype)
{
	const size_t idx = (size_t)vtype;
	const fx_vinfo_t *vinfo = NULL;

	if (idx < FX_NELEMS(s_vinfo_tbl)) {
		vinfo = s_vinfo_tbl[idx];
	}
	return vinfo;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_init_vobj(fx_vnode_t *vnode, fx_vtype_e vtype)
{
	const fx_vinfo_t *vinfo;

	vinfo = fx_get_vinfo(vtype);
	vnode->v_vaddr.vtype = vtype;
	vinfo->init(vnode);
}

void fx_destroy_vobj(fx_vnode_t *vnode)
{
	const fx_vinfo_t *vinfo;

	vinfo = fx_get_vinfo(vnode->v_vaddr.vtype);
	vinfo->destroy(vnode);
}

void fx_dexport_vobj(const fx_vnode_t *vnode, fx_header_t *hdr)
{
	const fx_vinfo_t *vinfo;

	vinfo = fx_get_vinfo(vnode->v_vaddr.vtype);
	vinfo->dexport(vnode, hdr);
}

void fx_dimport_vobj(fx_vnode_t *vnode, const fx_header_t *hdr)
{
	int rc;
	const fx_vinfo_t *vinfo;

	vinfo = fx_get_vinfo(vnode->v_vaddr.vtype);
	rc = vinfo->dcheck(hdr);
	fx_assert(rc == 0);
	vinfo->dimport(vnode, hdr);
}

int fx_check_dobj(const fx_header_t *hdr, fx_vtype_e expected_vtype)
{
	int rc;
	fx_vtype_e vtype;
	const fx_vinfo_t *vinfo;

	vtype = fx_dobj_vtype(hdr);
	if ((expected_vtype != FNX_VTYPE_ANY) && (vtype != expected_vtype)) {
		fx_warn("unexpected-dobj-vtype: vtype=%d expected=%d",
		        (int)vtype, (int)expected_vtype);
		return -1;
	}
	vinfo = fx_get_vinfo(vtype);
	if (vinfo == NULL) {
		fx_error("unsupported-dobj: vtype=%d", (int)vtype);
		return -1;
	}
	rc = fx_dobj_check(hdr, FNX_VTYPE_ANY);
	if (rc != 0) {
		return rc;
	}
	return vinfo->dcheck(hdr);
}

fx_vnode_t *fx_new_vobj(fx_vtype_e vtype, fx_malloc_t *alloc)
{
	fx_vnode_t *vnode;
	const fx_vinfo_t *vinfo;

	alloc = (alloc != NULL) ? alloc : fx_mallocator;
	vinfo = fx_get_vinfo(vtype);
	vnode = (fx_vnode_t *)fx_allocate(alloc, vinfo->size);
	vinfo->init(vnode);
	return vnode;
}

void fx_del_vobj(fx_vnode_t *vnode, fx_malloc_t *alloc)
{
	fx_vtype_e vtype;
	const fx_vinfo_t *vinfo;

	alloc = (alloc != NULL) ? alloc : fx_mallocator;
	vtype = vnode->v_vaddr.vtype;
	vinfo = fx_get_vinfo(vtype);
	vinfo->destroy(vnode);
	fx_deallocate(alloc ? alloc : fx_mallocator, vnode, vinfo->size);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_size_t fx_metasize(fx_vtype_e vtype)
{
	const fx_vinfo_t *vinfo;

	vinfo = fx_get_vinfo(vtype);
	return vinfo ? vinfo->dsize : 0;
}

fx_size_t fx_dobjnfrg(fx_vtype_e vtype)
{
	return fx_bytes_to_nfrg(fx_metasize(vtype));
}

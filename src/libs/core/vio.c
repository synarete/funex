/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#include "fnxinfra.h"
#include "types.h"
#include "vio.h"

/* Control flags */
#define VIOF_RDWR       FX_VIOF_RDWR
#define VIOF_RDONLY     FX_VIOF_RDONLY
#define VIOF_LOCK       FX_VIOF_LOCK
#define VIOF_SYNC       FX_VIOF_SYNC
#define VIOF_REG        FX_BITFLAG(11)
#define VIOF_BLK        FX_BITFLAG(12)
#define VIOF_CREAT      FX_BITFLAG(13)
#define VIOF_FLOCKED    FX_BITFLAG(14)

/* Local functions declarations */
static int vio_guaranty_cap(const fx_vio_t *, size_t);
static int vio_close(fx_vio_t *);
static int vio_getcap(const fx_vio_t *, size_t *);
static int vio_isflocked(const fx_vio_t *);
static int vio_funlock(fx_vio_t *);
static int vio_ismmaped(const fx_vio_t *);
static int vio_munmap(fx_vio_t *);
static int vio_isinrange(const fx_vio_t *, fx_lba_t, fx_bkcnt_t);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int isblk(const char *path)
{
	int rc;
	struct stat st;

	rc = stat(path, &st);
	return ((rc == 0) && S_ISBLK(st.st_mode));
}

static int testf(int flags, int mask)
{
	return ((flags & mask) == mask);
}

static int vio_testf(const fx_vio_t *vio, int flags)
{
	return testf(vio->flags, flags);
}

static void vio_setf(fx_vio_t *vio, int flags)
{
	vio->flags |= flags;
}

static void vio_unsetf(fx_vio_t *vio, int flags)
{
	vio->flags &= ~(flags);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vio_isopen(const fx_vio_t *vio)
{
	return (vio->fd > 0);
}

static void vio_savepath(fx_vio_t *vio, const char *path)
{
	if (vio->path != NULL) {
		free(vio->path);
	}
	if (path != NULL) {
		vio->path = strdup(path);
	} else {
		vio->path = NULL;
	}
}

static void vio_setpath(fx_vio_t *vio, const char *path, int flags)
{
	if (flags & FX_VIOF_SAVEPATH) {
		vio_savepath(vio, path);
	} else {
		vio_savepath(vio, NULL);
	}
}

static fx_size_t vio_bytesize(const fx_vio_t *vio, fx_bkcnt_t lbcnt)
{
	return (lbcnt * vio->lbsz);
}

static fx_bkcnt_t vio_lbcount(const fx_vio_t *vio, fx_size_t bytes_sz)
{
	return (bytes_sz / vio->lbsz);
}

static fx_off_t vio_offmax(const fx_vio_t *vio)
{
	return (fx_off_t)vio_bytesize(vio, vio->bcap);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vio_stat(const fx_vio_t *vio, struct stat *st)
{
	int rc;

	rc = fstat(vio->fd, st);
	if (rc != 0) {
		rc = -errno;
		fx_trace1("fstat-failure: fd=%d err=%d", vio->fd, -rc);
	}
	return rc;
}

static int vflags_to_oflags(const char *path, int vflags)
{
	int oflags = 0, direct = 0; /* XXX */

	if (vflags & VIOF_RDWR) {
		oflags |= O_RDWR;
	} else if (vflags & VIOF_RDONLY) {
		oflags |= O_RDONLY;
	} else {
		oflags |= O_RDONLY; /* XXX */
	}
	if (vflags & VIOF_CREAT) {
		oflags |= O_CREAT | O_TRUNC;
	}
	if (vflags & VIOF_SYNC) {
		oflags |= O_SYNC;
		if (direct && !(vflags & VIOF_CREAT) && isblk(path)) {
			oflags |= O_DIRECT; /* NB: O_DIRECT causes open-failure if reg */
		}
	}
	return oflags;
}

static int vio_open_fd(fx_vio_t *vio, const char *path, int vflags)
{
	int fd, oflags, rc = 0;
	mode_t mode;

	mode    = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;
	if ((vflags & VIOF_CREAT) && (vflags & VIOF_LOCK)) {
		mode &= ~((mode_t)S_IXGRP);
		mode |= S_ISGID;
	}

	oflags  = vflags_to_oflags(path, vflags);
	fd = open(path, oflags, mode);
	if (fd > 0) {
		vio->fd = fd;
		vio_setf(vio, vflags);
	} else {
		rc = -errno;
		fx_warn("open-failure: path=%s oflags=%#x mode=%o err=%d",
		        path, oflags, mode, -rc);
	}

	return rc;
}

static int vio_probe_type(fx_vio_t *vio)
{
	int rc;
	struct stat st;

	rc = vio_stat(vio, &st);
	if (rc == 0) {
		if (S_ISREG(st.st_mode)) {
			vio_setf(vio, VIOF_REG);
		} else if (S_ISBLK(st.st_mode)) {
			vio_setf(vio, VIOF_BLK);
		} else {
			fx_warn("unsupported-vio-type: mode=%o", st.st_mode);
			rc = -EINVAL;
		}
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vio_create(fx_vio_t *vio, const char *path,
                      fx_bkcnt_t base, fx_bkcnt_t bcap, int flags)
{
	int rc;

	flags |= VIOF_CREAT;
	if (vio_isopen(vio)) {
		return -EINVAL;
	}
	rc = vio_open_fd(vio, path, flags);
	if (rc != 0) {
		return rc;
	}
	rc = vio_probe_type(vio);
	if (rc != 0) {
		vio_close(vio);
		return rc;
	}
	rc = vio_guaranty_cap(vio, base + bcap);
	if (rc != 0) {
		vio_close(vio);
		return rc;
	}

	vio->base = base;
	vio->bcap = bcap;
	vio_setpath(vio, path, flags);
	vio_setf(vio, flags);

	return 0;
}

static int vio_open(fx_vio_t *vio, const char *path,
                    fx_bkcnt_t base, fx_bkcnt_t bcap, int flags)
{
	int rc;
	fx_bkcnt_t rem, nbk = 0;

	if (vio_isopen(vio)) {
		return -EINVAL;
	}
	rc = vio_open_fd(vio, path, flags);
	if (rc != 0) {
		return rc;
	}
	rc = vio_probe_type(vio);
	if (rc != 0) {
		vio_close(vio);
		return rc;
	}
	rc = vio_guaranty_cap(vio, base + bcap);
	if (rc != 0) {
		vio_close(vio);
		return rc;
	}
	rc = vio_getcap(vio, &nbk);
	if (rc != 0) {
		vio_close(vio);
		return rc;
	}
	if (base > nbk) {
		vio_close(vio);
		return -EINVAL;
	}
	rem = nbk - base;

	if (bcap == 0) { /* Special case: use probed capacity */
		vio->base = base;
		vio->bcap = rem;
	} else {
		vio->base = base;
		vio->bcap = (bcap < rem) ? bcap : rem;
	}
	vio_setpath(vio, path, flags);
	vio_setf(vio, flags);

	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vio_getcap_reg(const fx_vio_t *vio, fx_bkcnt_t *nbk)
{
	int rc;
	struct stat st;

	rc = vio_stat(vio, &st);
	if (rc == 0) {
		*nbk = vio_lbcount(vio, (size_t)(st.st_size));
	}
	return rc;
}

static int vio_getcap_blk(const fx_vio_t *vio, fx_bkcnt_t *cnt)
{
	int rc;
	size_t sz;
	unsigned long cmd;

	cmd = BLKGETSIZE64;
	rc  = ioctl(vio->fd, cmd, &sz);
	if (rc == 0) {
		*cnt = vio_lbcount(vio, sz);
	} else {
		rc = -errno;
		fx_error("ioctl-blkgetsize64-failure: fd=%d cmd=%lx err=%d",
		         vio->fd, cmd, -rc);
	}
	return rc;
}

static int vio_getcap(const fx_vio_t *vio, fx_bkcnt_t *nbk)
{
	int rc = -EBADF;

	if (vio_isopen(vio)) {
		if (vio_testf(vio, VIOF_REG)) {
			rc = vio_getcap_reg(vio, nbk);
		} else if (vio_testf(vio, VIOF_BLK)) {
			rc = vio_getcap_blk(vio, nbk);
		}
	}
	return rc;
}

static int vio_off2lba(const fx_vio_t *vio, fx_off_t off, fx_lba_t *lba)
{
	*lba = (fx_lba_t)(off / (fx_off_t)vio->lbsz);
	return vio_isinrange(vio, *lba, 1) ? 0 : -ERANGE;
}

static int vio_guarantycap_reg(const fx_vio_t *vio, fx_bkcnt_t lbcnt)
{
	int rc;
	loff_t len, off = 0;
	fx_bkcnt_t cur = 0;

	rc = vio_getcap(vio, &cur);
	if ((rc == 0) && (lbcnt > cur)) {
		len = (loff_t)vio_bytesize(vio, lbcnt);
		rc  = -posix_fallocate(vio->fd, off, len);
		if (rc != 0) {
			fx_trace1("posix_fallocate-failure: fd=%d off=%jd len=%jd err=%d",
			          vio->fd, off, len, -rc);
		}
	}
	return rc;
}

static int vio_lseek(const fx_vio_t *vio, fx_lba_t lba)
{
	int rc = 0;
	loff_t off, res;

	off = (loff_t)vio_bytesize(vio, lba);
	res = lseek64(vio->fd, off, SEEK_SET);
	if (res == (off_t)(-1)) {
		rc = -errno;
		fx_error("lseek-failure fd=%d off=%jd err=%d", vio->fd, off, -rc);
	}
	return rc;
}

static int vio_guarantycap_blk(const fx_vio_t *vio, fx_bkcnt_t lbcnt)
{
	int rc = 0;
	fx_size_t sz;

	sz = vio_bytesize(vio, lbcnt);
	if (sz == 0) {
		goto out;
	}
	rc = vio_lseek(vio, lbcnt);
	if (rc != 0) {
		goto out;
	}
	rc = vio_lseek(vio, 0);
	if (rc != 0) {
		goto out;
	}
out:
	if (rc == -EINVAL) {
		rc = 0; /* Some block devices do-not support lseek */
	}
	return rc;
}

static int vio_guaranty_cap(const fx_vio_t *vio, fx_bkcnt_t cnt)
{
	int rc = -1;

	if (vio_testf(vio, VIOF_REG)) {
		rc = vio_guarantycap_reg(vio, cnt);
	} else if (vio_testf(vio, VIOF_BLK)) {
		rc = vio_guarantycap_blk(vio, cnt);
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void vio_close_fd(fx_vio_t *vio)
{
	int rc;

	errno = 0;
	rc = close(vio->fd);
	if ((rc != 0) && (errno != EINTR)) {
		/* http://lkml.org/lkml/2005/9/10/129 */
		fx_panic("close-failure: fd=%d rc=%d", vio->fd, rc);
	}
	vio->fd = -1;
}

static int vio_close(fx_vio_t *vio)
{
	if (vio_ismmaped(vio)) {
		vio_munmap(vio);
	}
	if (vio_isflocked(vio)) {
		vio_funlock(vio);
	}
	if (vio_isopen(vio)) {
		vio_close_fd(vio);
		vio->flags = 0;
	}
	vio_savepath(vio, NULL);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
vio_isinrange(const fx_vio_t *vio, fx_lba_t lba, fx_bkcnt_t cnt)
{
	int res = 1;
	fx_lba_t end;

	end = vio->base + vio->bcap;
	if (lba < vio->base) {
		res = 0;
	} else if (lba >= end) {
		res = 0;
	} else if (cnt > vio->bcap) {
		res = 0;
	} else if ((lba + cnt) > end) {
		res = 0;
	}
	return res;
}

static int
vio_read(const fx_vio_t *vio, void *buf, fx_lba_t lba, fx_bkcnt_t cnt)
{
	int rc;
	loff_t  off;
	size_t  len;
	ssize_t res;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_isinrange(vio, lba, cnt)) {
		return -EINVAL;
	}
	len = vio_bytesize(vio, cnt);
	off = (loff_t)vio_bytesize(vio, vio->base + lba);
	res = pread64(vio->fd, buf, len, off);
	if (res != (ssize_t)len) {
		rc = -errno;
		fx_error("pread-failure: fd=%d cnt=%zu off=%zu off-max=%zu err=%d",
		         vio->fd, len, off, vio_offmax(vio), -rc);
		return rc;
	}
	return 0;
}

static int
vio_write(const fx_vio_t *vio, const void *buf, fx_lba_t lba, fx_bkcnt_t cnt)
{
	int rc;
	loff_t  off;
	size_t  len;
	ssize_t res;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_isinrange(vio, lba, cnt)) {
		return -EINVAL;
	}
	len = vio_bytesize(vio, cnt);
	off = (off_t)vio_bytesize(vio, vio->base + lba);
	res = pwrite64(vio->fd, buf, len, off);
	if (res != (ssize_t)len) {
		rc = -errno;
		fx_error("pwrite-failure: fd=%d cnt=%zu off=%zu off-max=%zu err=%d",
		         vio->fd, len, off, vio_offmax(vio), -rc);
		return rc;
	}
	return 0;
}

static int vio_sync(const fx_vio_t *vio)
{
	int rc;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	/*rc = fsync(vio->fd);*/
	rc = fdatasync(vio->fd);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vio_ismmaped(const fx_vio_t *vio)
{
	return (vio->maddr != NULL);
}

static int vio_mmap(fx_vio_t *vio)
{
	int rc, prot, flags, rdwr;
	off_t off;
	size_t len;
	void *addr = NULL;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (vio_ismmaped(vio)) {
		return -EALREADY;
	}
	if (vio->bcap == 0) {
		return -ERANGE;
	}

	len     = vio_bytesize(vio, vio->bcap);
	rdwr    = vio_testf(vio, VIOF_RDWR);
	prot    = (rdwr ? (PROT_READ | PROT_WRITE) : PROT_READ);
	flags   = MAP_SHARED;
	off     = 0;
	addr = mmap(NULL, len, prot, flags, vio->fd, off);
	if (addr == MAP_FAILED) {
		rc = -errno;
		fx_trace1("no-mmap: len=%zu prot=%d flags=%d fd=%d off=%jd err=%d",
		          len, prot, flags, vio->fd, off, -rc);
		return rc;
	}

	rc = madvise(addr, len, MADV_RANDOM);
	if (rc != 0) {
		rc = -errno;
		fx_error("no-madvise: addr=%p len=%zu fd=%d err=%d",
		         addr, len, vio->fd, rc);
		munmap(addr, len);
		return rc;
	}

	vio->maddr = addr;
	return 0;
}

static int vio_munmap(fx_vio_t *vio)
{
	int rc;
	fx_size_t len;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_ismmaped(vio)) {
		return -ENOTSUP;
	}
	len = vio_bytesize(vio, vio->bcap);
	rc  = munmap(vio->maddr, len);
	if (rc != 0) {
		return -errno;
	}
	vio->maddr = NULL;
	return 0;
}

static int vio_mgetp(const fx_vio_t *vio, fx_lba_t lba, void **p)
{
	char *addr = NULL;
	fx_lba_t pos;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_ismmaped(vio)) {
		return -ENOTSUP;
	}
	if (!vio_isinrange(vio, lba, 1)) {
		return -ERANGE;
	}

	addr = (char *)vio->maddr;
	pos  = vio_bytesize(vio, lba);

	*p = (void *)(addr + pos);
	return 0;
}

static int
vio_msync(const fx_vio_t *vio, fx_lba_t lba, fx_bkcnt_t cnt, int now)
{
	int rc, flags;
	size_t pos, len;
	char *addr;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_ismmaped(vio)) {
		return -ENOTSUP;
	}
	if (!vio_isinrange(vio, lba, cnt)) {
		return -ERANGE;
	}

	flags   = now ? MS_SYNC : MS_ASYNC;
	pos     = vio_bytesize(vio, lba);
	len     = vio_bytesize(vio, cnt);
	addr    = ((char *)vio->maddr) + pos;
	rc = msync(addr, len, flags);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

static int vio_mflush(const fx_vio_t *bf, int now)
{
	return vio_msync(bf, 0, bf->bcap, now);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vio_isflocked(const fx_vio_t *vio)
{
	return vio_testf(vio, VIOF_FLOCKED);
}

static int vio_flock_op(const fx_vio_t *vio, int op)
{
	int rc;

	rc = flock(vio->fd, op);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

static int vio_flock(fx_vio_t *vio)
{
	int rc;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_testf(vio, VIOF_LOCK)) {
		return -ENOTSUP;
	}
	if (vio_isflocked(vio)) {
		return -EALREADY;
	}
	rc = vio_flock_op(vio, LOCK_EX);
	if (rc != 0) {
		return rc;
	}
	vio_setf(vio, VIOF_FLOCKED);
	return 0;
}

static int vio_tryflock(fx_vio_t *vio)
{
	int rc;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_testf(vio, VIOF_LOCK)) {
		return -ENOTSUP;
	}
	if (vio_isflocked(vio)) {
		return -EALREADY;
	}
	rc = vio_flock_op(vio, LOCK_EX | LOCK_NB);
	if (rc != 0) {
		return rc;
	}
	vio_setf(vio, VIOF_FLOCKED);
	return 0;
}

static int vio_funlock(fx_vio_t *vio)
{
	int rc;

	if (!vio_isopen(vio)) {
		return -EBADF;
	}
	if (!vio_testf(vio, VIOF_LOCK)) {
		return -ENOTSUP;
	}
	if (!vio_isflocked(vio)) {
		return -EINVAL;
	}
	rc = vio_flock_op(vio, LOCK_UN);
	if (rc != 0) {
		return rc;
	}
	vio_unsetf(vio, VIOF_FLOCKED);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void vio_bind_hooks(fx_vio_t *vio)
{
	vio->create     = vio_create;
	vio->open       = vio_open;
	vio->close      = vio_close;
	vio->read       = vio_read;
	vio->write      = vio_write;
	vio->sync       = vio_sync;
	vio->isopen     = vio_isopen;
	vio->getcap     = vio_getcap;
	vio->off2lba    = vio_off2lba;

	vio->mmap       = vio_mmap;
	vio->munmap     = vio_munmap;
	vio->msync      = vio_msync;
	vio->mflush     = vio_mflush;
	vio->mgetp      = vio_mgetp;

	vio->flock      = vio_flock;
	vio->tryflock   = vio_tryflock;
	vio->funlock    = vio_funlock;
}

void fx_vio_init(fx_vio_t *vio)
{
	vio->fd     = -1;
	vio->flags  = 0;
	vio->lbsz   = FNX_BLKSIZE;
	vio->bcap   = 0;
	vio->base   = 0;
	vio->maddr  = NULL;
	vio->path   = NULL;

	vio_bind_hooks(vio);
}

void fx_vio_destroy(fx_vio_t *vio)
{
	if (vio_isopen(vio)) {
		vio_close(vio);
	}
	vio->flags  = 0;
	vio->lbsz   = 0;
	vio->bcap   = 0;
	vio->maddr  = NULL;
}

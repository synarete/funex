/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_DIRI_H_
#define FUNEX_DIRI_H_


static inline fx_ino_t dir_getino(const fx_dir_t *dir)
{
	return inode_getino(&dir->d_inode);
}

static inline void dir_setitime(fx_dir_t *dir, unsigned flags)
{
	inode_setitime(&dir->d_inode, flags);
}

static inline int dir_ispseudo(const fx_dir_t *dir)
{
	return inode_ispseudo(&dir->d_inode);
}

static inline fx_nlink_t dir_getnlink(const fx_dir_t *dir)
{
	return inode_getnlink(&dir->d_inode);
}

static inline int dir_isempty(const fx_dir_t *dir)
{
	return (dir->d_nchilds == 0);
}

static inline int dir_hasspace(const fx_dir_t *dir)
{
	return (dir->d_nchilds < FNX_DIRNCHILDS_MAX);
}

static inline fx_dir_t *inode_to_dir(const fx_inode_t *inode)
{
	return fx_inode_to_dir(inode);
}

static inline fx_inode_t *dir_inode(const fx_dir_t *dir)
{
	const fx_inode_t *inode = &dir->d_inode;
	return (fx_inode_t *)inode;
}

static inline int dir_testf(const fx_dir_t *dir, fx_flags_t mask)
{
	return inode_testf(&dir->d_inode, mask);
}

static inline void dir_setf(fx_dir_t *dir, fx_flags_t mask)
{
	inode_setf(&dir->d_inode, mask);
}

static inline void dir_unsetf(fx_dir_t *dir, fx_flags_t mask)
{
	inode_unsetf(&dir->d_inode, mask);
}

static inline int dir_isroot(const fx_dir_t *dir)
{
	return (dir_getino(dir) == FNX_INO_ROOT);
}

static inline int dir_isvalidname(const fx_dir_t *dir, const fx_name_t *name)
{
	fx_unused(dir); /* TODO: Use dir-specific semantics */
	return (name->len <= FNX_NAME_MAX);
}

#endif /* FUNEX_DIRI_H_ */




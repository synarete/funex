/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_EXECQ_H_
#define FUNEX_EXECQ_H_


typedef fx_fifo_t fx_execq_t;


fx_xelem_t *fx_qlink_to_xelem(const fx_link_t *);

fx_bkref_t *fx_qlink_to_bkref(const fx_link_t *);

fx_bkref_t *fx_xelem_to_bkref(const fx_xelem_t *);


void fx_xelem_init(fx_xelem_t *);

void fx_xelem_destroy(fx_xelem_t *);


void fx_execq_init(fx_execq_t *);

void fx_execq_destroy(fx_execq_t *);

int fx_execq_isempty(const fx_execq_t *);

void fx_execq_enqueue(fx_execq_t *, fx_xelem_t *, int);

fx_xelem_t *fx_execq_dequeue(fx_execq_t *, unsigned, unsigned);


fx_bkref_t *fx_recv_bks(fx_execq_t *, unsigned, unsigned);

fx_bkref_t *fx_recv_bk(fx_execq_t *);

void fx_send_bk(fx_execq_t *, fx_bkref_t *);


#endif /* FUNEX_EXECQ_H_ */

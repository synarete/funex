/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "addr.h"
#include "elems.h"
#include "repr.h"

#define bool_tostr(ss, pref, ptr, field) \
	repr_bool(ss, prefstr(pref), fieldstr(#field), (fx_bool_t)((ptr)->field))

#define dint_tostr(ss, pref, ptr, field) \
	repr_dint(ss, prefstr(pref), fieldstr(#field), (long)((ptr)->field))

#define oint_tostr(ss, pref, ptr, field) \
	repr_oint(ss, prefstr(pref), fieldstr(#field), (long)((ptr)->field))

#define xint_tostr(ss, pref, ptr, field) \
	repr_xint(ss, prefstr(pref), fieldstr(#field), (long)((ptr)->field))

#define zint_tostr(ss, pref, ptr, field) \
	repr_zint(ss, prefstr(pref), fieldstr(#field), (long)((ptr)->field))

#define name_tostr(ss, pref, ptr, field) \
	repr_name(ss, prefstr(pref), fieldstr(#field), &((ptr)->field))

#define uuid_tostr(ss, pref, ptr, field) \
	repr_uuid(ss, prefstr(pref), fieldstr(#field), &((ptr)->field))

#define ts_tostr(ss, pref, ptr, field) \
	repr_ts(ss, prefstr(pref), fieldstr(#field), &((ptr)->field))

#define extent_tostr(ss, pref, ptr, field) \
	repr_extent(ss, prefstr(pref), fieldstr(#field), &((ptr)->field))


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const char *fieldstr(const char *s)
{
	const char *f;

	if ((s != NULL) && ((f = strchr(s, '_')) != NULL)) {
		f = f + 1;
	} else {
		f = s;
	}
	return f;
}

static const char *prefstr(const char *prefix)
{
	return prefix ? prefix : "";
}

static void appendf(fx_substr_t *ss, const char *fmt, ...)
{
	va_list ap;
	char buf[1024] = "";

	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf) - 1, fmt, ap);
	va_end(ap);

	fx_substr_append(ss, buf);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void repr_raw(fx_substr_t *ss, long n)
{
	appendf(ss, "%#ld\n", n);
}

static void
repr_bool(fx_substr_t *ss, const char *pref, const char *name, fx_bool_t b)
{
	appendf(ss, "%s%s: %d\n", pref, name, (int)b);
}

static void
repr_dint(fx_substr_t *ss, const char *pref, const char *name, long n)
{
	appendf(ss, "%s%s: %#ld\n", pref, name, n);
}

static void
repr_oint(fx_substr_t *ss, const char *pref, const char *name, long n)
{
	appendf(ss, "%s%s: %#lo\n", pref, name, n);
}

static void
repr_xint(fx_substr_t *ss, const char *pref, const char *name, long n)
{
	appendf(ss, "%s%s: %#lx\n", pref, name, n);
}

static void
repr_zint(fx_substr_t *ss, const char *pref, const char *name, long n)
{
	appendf(ss, "%s%s: %zd\n", pref, name, n);
}

static void repr_name(fx_substr_t *ss, const char *pref,
                      const char *name, const fx_name_t *val)
{
	appendf(ss, "%s%s: %s\n", pref, name, val->str);
}

static void repr_uuid(fx_substr_t *ss, const char *pref,
                      const char *name, const fx_uuid_t *uu)
{
	appendf(ss, "%s%s: %s\n", pref, name, uu->str);
}

static void repr_ts(fx_substr_t *ss, const char *pref,
                    const char *name, const fx_timespec_t *ts)
{
	appendf(ss, "%s%s: %ld.%ld\n", pref, name, ts->tv_sec, ts->tv_nsec);
}

static void repr_extent(fx_substr_t *ss, const char *pref,
                        const char *name, const fx_extent_t *ext)
{
	appendf(ss, "%s%s: %lu..%lu\n", pref, name, ext->lba, ext->lba + ext->cnt);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_repr_intval(fx_substr_t *ss, int nn)
{
	repr_raw(ss, nn);
}

void fx_repr_uuid(fx_substr_t *ss, const fx_uuid_t *uu)
{
	appendf(ss, "%s\n", uu->str);
}

void fx_repr_times(fx_substr_t *ss, const fx_times_t *times, const char *pref)
{
	ts_tostr(ss, pref, times, btime);
	ts_tostr(ss, pref, times, atime);
	ts_tostr(ss, pref, times, ctime);
	ts_tostr(ss, pref, times, mtime);
}


void fx_repr_fsattr(fx_substr_t *ss, const fx_fsattr_t *fsa, const char *pref)
{
	uuid_tostr(ss, pref, fsa, f_fsuuid);
	dint_tostr(ss, pref, fsa, f_fstype);
	dint_tostr(ss, pref, fsa, f_version);
	dint_tostr(ss, pref, fsa, f_uid);
	dint_tostr(ss, pref, fsa, f_gid);
	xint_tostr(ss, pref, fsa, f_rootino);
	xint_tostr(ss, pref, fsa, f_mntf);
}

void fx_repr_fsstat(fx_substr_t *ss, const fx_fsstat_t *fss, const char *pref)
{
	zint_tostr(ss, pref, fss, f_blocks);
	zint_tostr(ss, pref, fss, f_bfree);
	zint_tostr(ss, pref, fss, f_bavail);
	xint_tostr(ss, pref, fss, f_bnext);
	zint_tostr(ss, pref, fss, f_inodes);
	zint_tostr(ss, pref, fss, f_ifree);
	zint_tostr(ss, pref, fss, f_iavail);
	xint_tostr(ss, pref, fss, f_iapex);
}

void fx_repr_vstats(fx_substr_t *ss, const fx_vstats_t *vstat, const char *pref)
{
	zint_tostr(ss, pref, vstat, v_super);
	zint_tostr(ss, pref, vstat, v_spmap);
	zint_tostr(ss, pref, vstat, v_dir);
	zint_tostr(ss, pref, vstat, v_dirext);
	zint_tostr(ss, pref, vstat, v_dirseg);
	zint_tostr(ss, pref, vstat, v_symlnk);
	zint_tostr(ss, pref, vstat, v_reflnk);
	zint_tostr(ss, pref, vstat, v_special);
	zint_tostr(ss, pref, vstat, v_regfile);
	zint_tostr(ss, pref, vstat, v_slice);
	zint_tostr(ss, pref, vstat, v_segmnt);
}

void fx_repr_iostat(fx_substr_t *ss, const fx_iostat_t *ios, const char *pref)
{
	zint_tostr(ss, pref, ios, io_nread);
	zint_tostr(ss, pref, ios, io_nwrite);
}

void fx_repr_opstat(fx_substr_t *ss, const fx_opstat_t *ops, const char *pref)
{
	zint_tostr(ss, pref, ops, op_lookup);
	zint_tostr(ss, pref, ops, op_forget);
	zint_tostr(ss, pref, ops, op_getattr);
	zint_tostr(ss, pref, ops, op_setattr);
	zint_tostr(ss, pref, ops, op_readlink);
	zint_tostr(ss, pref, ops, op_mknod);
	zint_tostr(ss, pref, ops, op_mkdir);
	zint_tostr(ss, pref, ops, op_unlink);
	zint_tostr(ss, pref, ops, op_rmdir);
	zint_tostr(ss, pref, ops, op_symlink);
	zint_tostr(ss, pref, ops, op_rename);
	zint_tostr(ss, pref, ops, op_link);
	zint_tostr(ss, pref, ops, op_open);
	zint_tostr(ss, pref, ops, op_read);
	zint_tostr(ss, pref, ops, op_write);
	zint_tostr(ss, pref, ops, op_flush);
	zint_tostr(ss, pref, ops, op_release);
	zint_tostr(ss, pref, ops, op_fsync);
	zint_tostr(ss, pref, ops, op_opendir);
	zint_tostr(ss, pref, ops, op_readdir);
	zint_tostr(ss, pref, ops, op_releasedir);
	zint_tostr(ss, pref, ops, op_fsyncdir);
	zint_tostr(ss, pref, ops, op_statfs);
	zint_tostr(ss, pref, ops, op_access);
	zint_tostr(ss, pref, ops, op_create);
	zint_tostr(ss, pref, ops, op_bmap);
	zint_tostr(ss, pref, ops, op_interrupt);
	zint_tostr(ss, pref, ops, op_poll);
	zint_tostr(ss, pref, ops, op_truncate);
	zint_tostr(ss, pref, ops, op_fallocate);
	zint_tostr(ss, pref, ops, op_fpunch);
	zint_tostr(ss, pref, ops, op_stats);
	zint_tostr(ss, pref, ops, op_xcopy);
}

void fx_repr_layout(fx_substr_t *ss, const fx_layout_t *vd, const char *pref)
{
	extent_tostr(ss, pref, vd, l_super);
	extent_tostr(ss, pref, vd, l_index);
	extent_tostr(ss, pref, vd, l_extra);
	extent_tostr(ss, pref, vd, l_udata);
}

void fx_repr_super(fx_substr_t *ss, const fx_super_t *super, const char *pref)
{
	uuid_tostr(ss, pref, super, su_uuid);
	name_tostr(ss, pref, super, su_name);
	name_tostr(ss, pref, super, su_uref);
	xint_tostr(ss, pref, super, su_flags);
	bool_tostr(ss, pref, super, su_active);
	fx_repr_layout(ss, &super->su_layout, pref);
	fx_repr_fsattr(ss, &super->su_fsattr, pref);
	fx_repr_fsstat(ss, &super->su_fsstat, pref);
	fx_repr_vstats(ss, &super->su_vstats, pref);
	fx_repr_times(ss, &super->su_times, pref);
}

void fx_repr_cstats(fx_substr_t *ss,
                    const fx_cstats_t *cstats, const char *pref)
{
	zint_tostr(ss, pref, cstats, c_frefs);
	zint_tostr(ss, pref, cstats, c_inodes);
	zint_tostr(ss, pref, cstats, c_vnodes);
	zint_tostr(ss, pref, cstats, c_vdirty);
	zint_tostr(ss, pref, cstats, c_vlru);
	zint_tostr(ss, pref, cstats, c_sblocks);
	zint_tostr(ss, pref, cstats, c_sdirty);
	zint_tostr(ss, pref, cstats, c_slru);
	zint_tostr(ss, pref, cstats, c_ublocks);
	zint_tostr(ss, pref, cstats, c_udirty);
	zint_tostr(ss, pref, cstats, c_ulru);
	zint_tostr(ss, pref, cstats, c_ufree);
	zint_tostr(ss, pref, cstats, c_sfree);
}

void fx_repr_climits(fx_substr_t *ss,
                     const fx_climits_t *climits, const char *pref)
{
	zint_tostr(ss, pref, climits, c_pages_lim);
	zint_tostr(ss, pref, climits, c_frefs_lim);
	zint_tostr(ss, pref, climits, c_vinode_lim);
	zint_tostr(ss, pref, climits, c_vdirty_lim);
	zint_tostr(ss, pref, climits, c_sblock_lim);
	zint_tostr(ss, pref, climits, c_sdirty_lim);
	zint_tostr(ss, pref, climits, c_ublock_lim);
	zint_tostr(ss, pref, climits, c_udirty_lim);
}

void fx_repr_iattr(fx_substr_t *ss, const fx_iattr_t *iattr, const char *pref)
{
	xint_tostr(ss, pref, iattr, i_ino);
	dint_tostr(ss, pref, iattr, i_gen);
	oint_tostr(ss, pref, iattr, i_mode);
	dint_tostr(ss, pref, iattr, i_nlink);
	dint_tostr(ss, pref, iattr, i_uid);
	dint_tostr(ss, pref, iattr, i_gid);
	dint_tostr(ss, pref, iattr, i_rdev);
	zint_tostr(ss, pref, iattr, i_size);
	zint_tostr(ss, pref, iattr, i_bcap);
	zint_tostr(ss, pref, iattr, i_nblk);
	zint_tostr(ss, pref, iattr, i_wops);
	zint_tostr(ss, pref, iattr, i_wcnt);
	fx_repr_times(ss, &iattr->i_times, pref);
}

void fx_repr_inode(fx_substr_t *ss, const fx_inode_t *inode, const char *pref)
{
	fx_repr_iattr(ss, &inode->i_iattr, pref);
	xint_tostr(ss, pref, inode, i_flags);
	xint_tostr(ss, pref, inode, i_refino);
	xint_tostr(ss, pref, inode, i_parentd);
	name_tostr(ss, pref, inode, i_name);
}

void fx_repr_dir(fx_substr_t *ss, const fx_dir_t *dir, const char *pref)
{
	fx_repr_inode(ss, &dir->d_inode, pref);
	zint_tostr(ss, pref, dir, d_nchilds);
	zint_tostr(ss, pref, dir, d_ndents);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_parse_param(const fx_substr_t *ss, int *nn)
{
	int rc = -1;
	fx_substr_t s;
	char buf[128] = "";

	fx_substr_strip_ws(ss, &s);

	if (s.len && (s.len < sizeof(buf)) && fx_substr_isdigit(&s)) {
		fx_substr_copyto(ss, buf, sizeof(buf));
		*nn = atoi(buf);
		rc = 0;
	}
	return rc;
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_META_H_
#define FUNEX_META_H_

#include <stdlib.h>
#include <stdint.h>


/*
 * On-disk (or better said, on-device or on-volume) meta-data objects of the
 * Funex file-system. Objects begin with 32-bytes header:
 *
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |M|V|T|L| Resvd |     INO       |     XNO       | Reserved      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * M = Marker 0x46 (ASCII 'F')
 * V = On-disk format version
 * T = Objects type
 * L = Objects length in fragment-units (512 bytes)
 *
 * Where possible, fields are aligned to record-units (64 bytes) and fragment
 * units (512 bytes). The last 32bits of each section (64K) holds CRC checksum.
 *
 * Using little-endian for encoding of integer types. Unused fields (reserved)
 * are zeroed.
 */

typedef uint8_t fx_byte_t;


/* On-disk v-object's header */
typedef struct fx_packed fx_header {
	uint8_t         h_mark;
	uint8_t         h_vers;
	uint8_t         h_vtype;
	uint8_t         h_len;
	uint32_t        h_resrvd0;
	uint64_t        h_ino;
	uint64_t        h_xno;
	uint64_t        h_resrvd1;

} fx_header_t;


/* Block's logical-address, compact into 48-bits  */
typedef struct fx_packed fx_dbkaddr {
	uint8_t         lba[6];

} fx_dbkaddr_t;


/* Directory-entry on-disk repr */
typedef struct fx_packed fx_ddirent {
	uint64_t        de_namehash;
	uint64_t        de_ino_namelen;

} fx_ddirent_t;


/* Time-stamps (birth, access, modify & change) */
typedef struct fx_packed fx_dtimes {
	uint64_t        btime_sec;
	uint64_t        atime_sec;
	uint64_t        mtime_sec;
	uint64_t        ctime_sec;
	uint32_t        btime_nsec;
	uint32_t        atime_nsec;
	uint32_t        mtime_nsec;
	uint32_t        ctime_nsec;

} fx_dtimes_t;


/* Volume's blocks-extent */
typedef struct fx_packed fx_dextent {
	uint64_t        lba;
	uint64_t        cnt;

} fx_dextent_t;


/* Space layout definition */
typedef struct fx_packed fx_dlayout {
	/* R[0..1] */
	fx_dextent_t    l_super;
	fx_dextent_t    l_index;
	fx_dextent_t    l_extra;
	fx_dextent_t    l_udata;
	fx_dextent_t    l_trail;
	uint8_t         l_reserved[48];

} fx_dlayout_t;

/* File-system's global info */
typedef struct fx_packed fx_dfsattr {
	/* R[0] */
	uint8_t         f_uuid[16];
	uint32_t        f_fstype;
	uint32_t        f_version;
	uint32_t        f_gen;
	uint32_t        f_zid;
	uint32_t        f_uid;
	uint32_t        f_gid;
	uint64_t        f_rootino;
	uint8_t         f_reserved[16];

} fx_dfsattr_t;


/* File-system's blocks & inodes accounting */
typedef struct fx_packed fx_dfsstat {
	/* R[0] */
	uint64_t        f_inodes;
	uint64_t        f_ifree;
	uint64_t        f_iavail;
	uint64_t        f_iapex;
	uint8_t         f_reserved0[32];

	/* R[1] */
	uint64_t        f_blocks;
	uint64_t        f_bfree;
	uint64_t        f_bavail;
	uint64_t        f_bnext;
	uint64_t        f_pblocks;
	uint64_t        f_pfree;
	uint64_t        f_pavail;
	uint64_t        f_pnext;

} fx_dfsstat_t;


/* Metadata sub-elements accounting */
typedef struct fx_packed fx_dvstats {
	/* R[0] */
	uint64_t        v_super;
	uint64_t        v_spmap;
	uint64_t        v_dir;
	uint64_t        v_dirext;
	uint64_t        v_dirseg;
	uint64_t        v_symlnk;
	uint64_t        v_reflnk;
	uint64_t        v_special;

	/* R[1] */
	uint64_t        v_regfile;
	uint64_t        v_slice;
	uint64_t        v_segmnt;
	uint8_t         v_reserved1[36];
	uint32_t        v_magic;

} fx_dvstats_t;


/* I-node's attributes */
typedef struct fx_packed fx_diattr {
	/* R[0] */
	uint32_t        i_mode;
	uint32_t        i_nlink;
	uint32_t        i_uid;
	uint32_t        i_gid;
	uint64_t        i_rdev;
	uint64_t        i_size;
	uint64_t        i_bcap;
	uint64_t        i_nblk;
	uint64_t        i_wops;
	uint64_t        i_wcnt;

	/* R[1] */
	uint8_t         i_reserved1[16];
	fx_dtimes_t     i_times;

} fx_diattr_t;


/* Block-descriptor entry */
typedef struct fx_packed fx_dbkdsc {
	uint32_t        refs;
	uint8_t         flags;
	uint8_t         extra[2];

} fx_dbkdsc_t;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Super-block: file-system's root node */
typedef struct fx_packed fx_dsuper {
	/* R[0] */
	fx_header_t     su_hdr;
	uint8_t         su_uuid[16];
	uint64_t        su_flags;
	uint32_t        su_secsize;
	uint16_t        su_blksize;
	uint16_t        su_frgsize;

	/* R[1] */
	uint16_t        su_endian;
	uint16_t        su_ostype;
	uint8_t         su_reserved1[58];
	uint8_t         su_namelen;
	uint8_t         su_ureflen;

	/* R[2..3,4,5..6,7..8] */
	fx_dlayout_t    su_layout;
	fx_dfsattr_t    su_fsattr;
	fx_dfsstat_t    su_fsstat;
	fx_dvstats_t    su_vstats;

	/* R[9] */
	fx_dtimes_t     su_times;
	uint8_t         su_reserved8[12];
	uint32_t        su_magic;

	/* R[10..15] */
	uint8_t         su_reserved9[384];

	/* R[16..23] */
	uint8_t         su_name[256];
	uint8_t         su_uref[256];

	/* R[24..31] */
	uint8_t         su_reserved6[2556];
	uint32_t        su_checksum;

} fx_dsuper_t;


/* Space-map: reference to user's data blocks */
typedef struct fx_packed fx_dspmap {
	/* R[0] */
	fx_header_t     sp_hdr;
	uint64_t        sp_lba0;
	uint32_t        sp_used;
	uint8_t         sp_reserved0[20];

	/* R[1..31] */
	fx_dbkdsc_t     sp_bkd[256];
	uint8_t         sp_reserved1[188];
	uint32_t        sp_checksum;

} fx_dspmap_t;


/* I-node */
typedef struct fx_packed fx_dinode {
	/* R[0] */
	fx_header_t     i_hdr;
	uint64_t        i_flags;
	uint64_t        i_parentd;
	uint64_t        i_refino;
	uint8_t         i_reserved0[8];

	/* R[1..2] */
	fx_diattr_t     i_attr;

	/* R[3..6] */
	uint8_t         i_name[256];

	/* R[7] */
	uint8_t         i_namelen;
	uint8_t         i_pad;
	uint16_t        i_tlen;
	uint16_t        i_vhfunc;
	uint16_t        i_nhfunc;
	uint64_t        i_nhash;
	uint32_t        i_magic;
	uint8_t         i_reserved3[40];
	uint32_t        i_checksum;

} fx_dinode_t;


/* Directory head node */
typedef struct fx_packed fx_ddir {
	/* R[0..7] */
	fx_dinode_t     d_inode;

	/* R[8..63] */
	fx_ddirent_t    d_dent[223];
	uint32_t        d_ndents;
	uint32_t        d_nchilds;
	uint8_t         d_reserved[4];
	uint32_t        d_checksum;

} fx_ddir_t;


/* Directory extension */
typedef struct fx_packed fx_ddirext {
	/* R[0] */
	fx_header_t     dx_hdr;
	uint32_t        dx_ndents;
	uint32_t        dx_nsegs;
	uint8_t         dx_reserved0[20];
	uint32_t        dx_magic;

	/* R[1..9] */
	uint32_t        dx_segs[128];

	/* R[10..63] */
	fx_ddirent_t    dx_dent[211];
	uint8_t         dx_reserved1[140];
	uint32_t        dx_checksum;

} fx_ddirext_t;


/* Directory sub-segment */
typedef struct fx_packed fx_ddirseg {
	/* R[0] */
	fx_header_t     ds_hdr;
	uint32_t        ds_index;
	uint8_t         ds_ndents;
	uint8_t         ds_reserved0[23];
	uint32_t        ds_magic;

	/* R[1..15] */
	fx_ddirent_t    ds_dent[59];
	uint8_t         ds_reserved1[12];
	uint32_t        ds_checksum;

} fx_ddirseg_t;


/* Symbolic link (short) */
typedef struct fx_packed fx_dssymlnk {
	/* R[0..7] */
	fx_dinode_t     sl_inode;

	/* R[8..15] */
	uint8_t         sl_lnk[508];
	uint32_t        sl_checksum;

} fx_dssymlnk_t;


/* Symbolic link (long) */
typedef struct fx_packed fx_dsymlnk {
	/* R[0..7] */
	fx_dinode_t     sl_inode;

	/* R[8..71] */
	uint8_t         sl_lnk[4096];

} fx_dsymlnk_t;


/* Regular-file slice's bitmap */
typedef struct fx_packed fx_dslice {
	/* R[0] */
	fx_header_t     sc_hdr;
	uint32_t        sc_count;
	uint8_t         sc_reserved0[28];

	/* R[1..4] */
	uint32_t        sc_bitmap[64];

	/* R[5..7] */
	uint8_t         sc_reserved1[188];
	uint32_t        sc_checksum;

} fx_dslice_t;


/* Mapping of file's segment to sequence of blocks */
typedef struct fx_packed fx_dsegmnt {
	/* R[0] */
	fx_header_t     sg_hdr;
	uint64_t        sg_off;
	uint64_t        sg_bits;
	uint8_t         sg_reserved0[12];
	uint32_t        sg_magic;

	/* R[1..6] */
	fx_dbkaddr_t    sg_bkaddr[64];

	/* R[7] */
	uint8_t         sg_reserved7[60];
	uint32_t        sg_checksum;

} fx_dsegmnt_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Raw fragment */
typedef union fx_packed fx_dfrg {
	fx_header_t    fr_hdr;
	fx_byte_t      fr_dat[512];

} fx_dfrg_t;


/* Block + semantic "view" */
typedef union fx_packed fx_dblk {
	fx_header_t    bk_hdr;
	fx_dfrg_t      bk_frg[8];
	fx_byte_t      bk_data[4096];

} fx_dblk_t;


/* Section-block + semantic "view" */
typedef struct fx_packed fx_dsecbuf {
	fx_header_t     sec_hdr;
	fx_byte_t       sec_dat[65468];
	uint32_t        sec_checksum;

} fx_dsecbuf_t;

typedef union fx_packed fx_dsec {
	fx_header_t     sec_hdr;
	fx_dspmap_t     sec_spmap;
	fx_dsuper_t     sec_super;
	fx_dblk_t       sec_blk0;
	fx_dblk_t       sec_blks[16];
	fx_dfrg_t       sec_frgs[128];
	fx_ddir_t       sec_dirs[16];
	fx_dinode_t     sec_inodes[128];
	fx_dsecbuf_t    sec_buf;

} fx_dsec_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

typedef struct fx_packed fx_dxref {
	uint64_t        xr_fsid;
	uint64_t        xr_ino;
	uint64_t        xr_lba;
	uint64_t        xr_rev;
	uint8_t         xr_reserved[32];

} fx_dxref_t;

typedef struct fx_packed fx_duber {
	fx_header_t     u_hdr;
	uint8_t         u_reserved1[32];

	fx_dxref_t      u_xref[1008];
	uint8_t         u_reserved2[956];
	uint32_t        u_checksum;

} fx_duber_t;


#endif /* FUNEX_META_H_ */



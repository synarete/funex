/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_TYPES_H_
#define FUNEX_TYPES_H_

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "fnxdefs.h"

#define FX_UUIDSIZE            (16)
#define FX_UUIDSTRSIZE         (40) /* Include nul */


struct iovec;
struct stat;
struct statvfs;
struct timespec;
struct fx_dtimes;

/* Common types */
typedef _Bool           fx_bool_t;
typedef uint64_t        fx_lba_t;
typedef int64_t         fx_off_t;
typedef uint64_t        fx_size_t;
typedef uint32_t        fx_fstype_t;
typedef uint64_t        fx_ino_t;
typedef uint64_t        fx_xno_t;
typedef uint64_t        fx_dev_t;
typedef uint32_t        fx_uid_t;
typedef uint32_t        fx_gid_t;
typedef uint32_t        fx_mode_t;
typedef uint64_t        fx_nlink_t;
typedef uint64_t        fx_bkcnt_t;
typedef uint64_t        fx_filcnt_t;


/* Internal types */
typedef unsigned char   fx_octet_t;
typedef int             fx_status_t;
typedef unsigned int    fx_magic_t;
typedef unsigned int    fx_version_t;
typedef short           fx_svol_t;
typedef pid_t           fx_pid_t;
typedef time_t          fx_time_t;
typedef uint64_t        fx_hash_t;
typedef uint32_t        fx_zid_t;
typedef uint64_t        fx_flags_t;         /* Control-flags bit-mask */
typedef uint64_t        fx_mntf_t;          /* Mount-options bit-mask */
typedef uint64_t        fx_capf_t;          /* Capabilities bit-mask */
typedef uint32_t        fx_bits_t;          /* Bits-masks */
typedef struct iovec    fx_iovec_t;


/* Blobs' typedefs */
typedef fx_octet_t      fx_blk_t[FNX_BLKSIZE];
typedef fx_octet_t      fx_section_t[FNX_SECSIZE];
typedef fx_octet_t      fx_record_t[FNX_RECORDSIZE];


/* Enum typedefs */
typedef enum FNX_HFUNC  fx_hfunc_e;
typedef enum FNX_VTYPE  fx_vtype_e;


/* Name string-buffer */
struct fx_namebuf {
	size_t  len;
	char    str[FNX_NAME_MAX + 1];
};
typedef struct fx_namebuf  fx_name_t;


/* Path string-buffer (symbolic-link value) */
struct fx_pathbuf {
	size_t  len;
	char    str[FNX_PATH_MAX];
};
typedef struct fx_pathbuf  fx_path_t;


/* UUID wrapper */
struct fx_uuid {
	uint8_t uu[FX_UUIDSIZE];
	char    str[FX_UUIDSTRSIZE];
};
typedef struct fx_uuid      fx_uuid_t;


/* Time-stamps tuple */
struct fx_times {
	fx_timespec_t   btime;          /* Birth  */
	fx_timespec_t   atime;          /* Access */
	fx_timespec_t   mtime;          /* Modify */
	fx_timespec_t   ctime;          /* Change */
};
typedef struct fx_times   fx_times_t;


/* Credentials */
struct fx_cred {
	fx_pid_t        cr_pid;
	fx_uid_t        cr_uid;
	fx_gid_t        cr_gid;
	fx_mode_t       cr_umask;

	fx_size_t       cr_ngids;
	fx_gid_t        cr_gids[FNX_NGROUPS_MAX];
};
typedef struct fx_cred     fx_cred_t;


/* User's context (credentials & capabilities) */
struct fx_userctx {
	fx_cred_t       u_cred;
	fx_capf_t       u_capf;
	fx_bool_t       u_root;
};
typedef struct fx_userctx   fx_uctx_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_isitype(fx_vtype_e);

int fx_ismetasec(fx_size_t);

int fx_check_ino(fx_ino_t);

int fx_isvmeta(fx_vtype_e);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Name|Path string-buffer */
void fx_name_init(fx_name_t *);

void fx_name_setup(fx_name_t *, const char *);

void fx_name_nsetup(fx_name_t *, const char *, size_t);

void fx_name_clone(const fx_name_t *, fx_name_t *);

int fx_name_isequal(const fx_name_t *, const char *);

int fx_name_isnequal(const fx_name_t *, const char *, size_t);


void fx_path_init(fx_path_t *);

void fx_path_setup(fx_path_t *, const char *);

void fx_path_nsetup(fx_path_t *, const char *, size_t);

void fx_path_clone(fx_path_t *, const fx_path_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* UUID wrappers */
void fx_uuid_clear(fx_uuid_t *);

void fx_uuid_copy(fx_uuid_t *, const fx_uuid_t *);

void fx_uuid_generate(fx_uuid_t *);

int fx_uuid_isnull(const fx_uuid_t *);

int fx_uuid_compare(const fx_uuid_t *, const fx_uuid_t *);

int fx_uuid_isequal(const fx_uuid_t *, const fx_uuid_t *);

int fx_uuid_parse(fx_uuid_t *, const char *);

int fx_uuid_unparse(const fx_uuid_t *, char *, size_t);

void fx_uuid_assign(fx_uuid_t *, const unsigned char [16]);

void fx_uuid_copyto(const fx_uuid_t *, unsigned char [16]);

uint64_t fx_uuid_hi(fx_uuid_t *);

uint64_t fx_uuid_lo(fx_uuid_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* User's process' settings (credential & capacity) */
void fx_userctx_init(fx_uctx_t *);

void fx_userctx_destroy(fx_uctx_t *);

int fx_userctx_iscap(const fx_uctx_t *, fx_capf_t);


int fx_isprivileged(const fx_uctx_t *);

int fx_isgroupmember(const fx_uctx_t *, fx_gid_t gid);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Time & I-attr */
void fx_times_init(fx_times_t *);

void fx_times_fill(fx_times_t *, fx_flags_t flags, const fx_times_t *);

void fx_times_copy(fx_times_t *, const fx_times_t *);

void fx_times_dtoh(fx_times_t *, const struct fx_dtimes *);

void fx_times_htod(const fx_times_t *, struct fx_dtimes *);


#endif /* FUNEX_TYPES_H_ */


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "proto.h"
#include "metai.h"
#include "index.h"
#include "htod.h"
#include "addr.h"
#include "addri.h"
#include "elems.h"
#include "iobuf.h"
#include "super.h"
#include "inode.h"
#include "inodei.h"
#include "file.h"


#define FX_REG_INIT_MODE       \
	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define FX_SYMLNK_INIT_MODE      \
	(S_IFLNK | S_IRWXU | S_IRWXG | S_IRWXO)
#define FX_SYMLNK_INIT_XMSK      \
	(S_IRWXU | S_IRWXG | S_IRWXO)

#define FX_FREF_MAGIC       (0x2AF455EC)
#define FX_SEGMNT_MAGIC     (0x33AC112F)
#define FX_SYMLNK_MAGIC     (0x78AACE13)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fileref_check(const fx_fileref_t *fref)
{
	fx_assert(fref->f_magic == FX_FREF_MAGIC);
}

fx_fileref_t *fx_link_to_fileref(const fx_link_t *lnk)
{
	const fx_fileref_t *fref;

	fref = fx_container_of(lnk, fx_fileref_t, f_link);
	fx_fileref_check(fref);
	return (fx_fileref_t *)fref;
}

void fx_fileref_init(fx_fileref_t *fref)
{
	fx_bzero(fref, sizeof(*fref));

	link_init(&fref->f_link);
	fref->f_magic   = FX_FREF_MAGIC;
	fref->f_cached  = FNX_FALSE;
	fref->f_inode   = NULL;
}

void fx_fileref_destroy(fx_fileref_t *fref)
{
	fx_fileref_check(fref);

	link_destroy(&fref->f_link);
	fx_bff(fref, sizeof(*fref));
}

void fx_fileref_setup(fx_fileref_t *fref, fx_flags_t flags)
{
	if ((flags & O_RDONLY) == O_RDONLY) {
		fref->f_readable = FNX_TRUE;
	}
	if ((flags & O_WRONLY) == O_WRONLY) {
		fref->f_writeable = FNX_TRUE;
	}
	if ((flags & O_RDWR) == O_RDWR) {
		fref->f_readable  = FNX_TRUE;
		fref->f_writeable = FNX_TRUE;
	}

	if ((flags & O_APPEND) == O_APPEND) {
		/* FIXME */
	}
	if ((flags & O_NOATIME) == O_NOATIME) {
		fref->f_noatime = FNX_TRUE;
	}
	if ((flags & O_SYNC) == O_SYNC) {
		fref->f_sync = FNX_TRUE;
	}
}

static void fileref_set_unique_id(fx_fileref_t *fref)
{
	/* TODO: Make true unique identifier, crypto. Do not use uuid_generate; it
	 * is too-heavy.
	 */
	(void)fref;
}

void fx_fileref_attach(fx_fileref_t *fref, fx_inode_t *inode)
{
	fref->f_inode = inode;
	fileref_set_unique_id(fref);

	inode->i_vnode.v_refcnt += 1;
}

fx_inode_t *fx_fileref_detach(fx_fileref_t *fref)
{
	fx_inode_t *inode;

	inode = fref->f_inode;
	fref->f_inode = NULL;

	fx_assert(inode->i_vnode.v_refcnt > 0);
	inode->i_vnode.v_refcnt -= 1;
	return inode;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Regular-file
 */
fx_inode_t *fx_vnode_to_regfile(const fx_vnode_t *vnode)
{
	return vnode_to_inode(vnode);
}

void fx_regfile_init(fx_inode_t *regfile)
{
	fx_inode_init(regfile, FNX_VTYPE_REG);
	regfile->i_iattr.i_mode = FX_REG_INIT_MODE;
}

void fx_regfile_destroy(fx_inode_t *regfile)
{
	fx_inode_destroy(regfile);
}


void fx_regfile_htod(const fx_inode_t *regfile, fx_dinode_t *dinode)
{
	fx_inode_htod(regfile, dinode);
}

void fx_regfile_dtoh(fx_inode_t *regfile, const fx_dinode_t *dinode)
{
	int rc;

	/* Header */
	rc = fx_dobj_check(&dinode->i_hdr, FNX_VTYPE_REG);
	fx_assert(rc == 0);

	/* R[0..7] */
	fx_inode_dtoh(regfile, dinode);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


fx_slice_t *fx_vnode_to_slice(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_slice_t, sc_vnode);
}

void fx_slice_init(fx_slice_t *slice)
{
	fx_vnode_init(&slice->sc_vnode, FNX_VTYPE_SLICE);
	fx_bzero(slice->sc_bitmap, sizeof(slice->sc_bitmap));
	slice->sc_count = 0;
}

void fx_slice_destroy(fx_slice_t *slice)
{
	fx_vnode_destroy(&slice->sc_vnode);
	slice->sc_count = (fx_size_t)(-1);
}

void fx_slice_htod(const fx_slice_t *slice, fx_dslice_t *dslice)
{
	fx_header_t *hdr = &dslice->sc_hdr;

	/* Header & tail */
	fx_vnode_dexport(&slice->sc_vnode, hdr);
	fx_dobj_zpad(hdr, FNX_VTYPE_SLICE);

	/* R[0] */
	dslice->sc_count = fx_htod_u32((uint32_t)slice->sc_count);

	/* R[1..5] */
	for (size_t i = 0; i < FX_NELEMS(dslice->sc_bitmap); ++i) {
		dslice->sc_bitmap[i] = fx_htod_u32(slice->sc_bitmap[i]);
	}
}

void fx_slice_dtoh(fx_slice_t *slice, const fx_dslice_t *dslice)
{
	int rc;
	const fx_header_t *hdr = &dslice->sc_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_SLICE);
	fx_assert(rc == 0);
	fx_vnode_dimport(&slice->sc_vnode, hdr);

	/* R[0] */
	slice->sc_count = fx_dtoh_u32(dslice->sc_count);

	/* R[1..5] */
	for (size_t i = 0; i < FX_NELEMS(slice->sc_bitmap); ++i) {
		slice->sc_bitmap[i] = fx_dtoh_u32(dslice->sc_bitmap[i]);
	}
}

int fx_slice_dcheck(const fx_header_t *hdr)
{
	int rc;
	fx_vtype_e vtype;
	unsigned count, value, total;
	const fx_dslice_t *dslice;

	vtype = fx_dobj_vtype(hdr);
	if (vtype != FNX_VTYPE_SLICE) {
		return -1;
	}
	rc = fx_dobj_check(hdr, vtype);
	if (rc != 0) {
		return rc;
	}

	dslice = header_to_dslice(hdr);
	count  = fx_dtoh_u32(dslice->sc_count);
	total  = 0;
	for (size_t i = 0; i < FX_NELEMS(dslice->sc_bitmap); ++i) {
		value = fx_dtoh_u32(dslice->sc_bitmap[i]);
		total += (unsigned)fx_popcount(value);
	}

	fx_assert(count == total);
	if (count != total) {
		return -1;
	}
	return 0;
}

static fx_off_t off_to_seg(fx_off_t off)
{
	fx_assert(off >= FNX_SEGSIZE);
	return (off / FNX_SEGSIZE);
}

static uint32_t *slice_getslot(const fx_slice_t *slice, fx_off_t seg)
{
	size_t idx;
	const uint32_t *slot = NULL;

	fx_assert(seg < (FNX_NSLICES_MAX * FNX_SLICENSEG));
	fx_assert(seg >= 0);

	idx = (size_t)(seg % FNX_SLICENSEG) / 32;
	if (idx < FX_NELEMS(slice->sc_bitmap)) {
		slot = &slice->sc_bitmap[idx];
	}
	return (uint32_t *)slot;
}

static uint32_t seg_slot_mask(fx_off_t seg)
{
	fx_assert(seg >= 0);
	return (1u << ((size_t)(seg % 32)));
}

int fx_slice_testseg(const fx_slice_t *slice, fx_off_t off)
{
	int res;
	fx_off_t seg;
	uint32_t mask;
	const uint32_t *slot;

	seg  = off_to_seg(off);
	slot = slice_getslot(slice, seg);
	mask = seg_slot_mask(seg);
	res  = fx_testf(*slot, mask);

	return res;
}

void fx_slice_markseg(fx_slice_t *slice, fx_off_t off)
{
	fx_off_t seg;
	uint32_t mask;
	uint32_t *slot;

	seg  = off_to_seg(off);
	slot = slice_getslot(slice, seg);
	mask = seg_slot_mask(seg);

	fx_assert((*slot & mask) != mask);
	fx_assert(slice->sc_count < FNX_SLICENSEG);

	fx_setf(slot, mask);
	slice->sc_count += 1;
}

void fx_slice_unmarkseg(fx_slice_t *slice, fx_off_t off)
{
	fx_off_t seg;
	uint32_t mask;
	uint32_t *slot;

	seg  = off_to_seg(off);
	slot = slice_getslot(slice, seg);
	mask = seg_slot_mask(seg);

	fx_assert((*slot & mask) == mask);
	fx_assert(slice->sc_count > 0);

	fx_unsetf(slot, mask);
	slice->sc_count -= 1;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_segmnt_t *fx_vnode_to_segmnt(const fx_vnode_t *vnode)
{
	return fx_container_of(vnode, fx_segmnt_t, sg_vnode);
}

void fx_segmnt_init(fx_segmnt_t *segmnt)
{
	fx_vnode_init(&segmnt->sg_vnode, FNX_VTYPE_SEGMNT);
	fx_segmap_init(&segmnt->sg_segmap);
	segmnt->sg_magic = FX_SEGMNT_MAGIC;
}

void fx_segmnt_destroy(fx_segmnt_t *segmnt)
{
	fx_assert(segmnt->sg_magic == FX_SEGMNT_MAGIC);
	fx_segmap_destroy(&segmnt->sg_segmap);
	fx_vnode_destroy(&segmnt->sg_vnode);
	segmnt->sg_magic = 0;
}

int fx_segmnt_setup(fx_segmnt_t *segmnt, fx_ino_t ino, fx_off_t off)
{
	int rc;
	fx_off_t loff;
	fx_size_t len;
	fx_vaddr_t vaddr;

	vaddr_for_segmnt(&vaddr, ino, off);
	vnode_setvaddr(&segmnt->sg_vnode, &vaddr);

	loff = off_floor_seg(off);
	len  = FNX_SEGSIZE;
	rc   = fx_segmap_setup(&segmnt->sg_segmap, loff, len);
	fx_assert(rc == 0); /* FIXME */
	return rc;
}

static void bkaddr_htod(const fx_bkaddr_t *bkaddr, fx_dbkaddr_t *dbkaddr)
{
	const uint64_t lba = bkaddr->lba;

	dbkaddr->lba[0] = (uint8_t)(lba);
	dbkaddr->lba[1] = (uint8_t)(lba >> 8);
	dbkaddr->lba[2] = (uint8_t)(lba >> 16);
	dbkaddr->lba[3] = (uint8_t)(lba >> 24);
	dbkaddr->lba[4] = (uint8_t)(lba >> 32);
	dbkaddr->lba[5] = (uint8_t)(lba >> 40);
}

static void bkaddr_dtoh(fx_bkaddr_t *bkaddr, const fx_dbkaddr_t *dbkaddr)
{
	uint64_t lba = 0;

	lba |= ((uint64_t)dbkaddr->lba[5] << 40);
	lba |= ((uint64_t)dbkaddr->lba[4] << 32);
	lba |= ((uint64_t)dbkaddr->lba[3] << 24);
	lba |= ((uint64_t)dbkaddr->lba[2] << 16);
	lba |= ((uint64_t)dbkaddr->lba[1] << 8);
	lba |= ((uint64_t)dbkaddr->lba[0]);
	if (lba >= FNX_LBA_MAX) {
		lba = FNX_LBA_NULL;
	}

	bkaddr->lba = lba;
	bkaddr->svol = FNX_SVOL_DATA;
	bkaddr->frg = 0;
}

void fx_segmnt_htod(const fx_segmnt_t *segmnt, fx_dsegmnt_t *dsegmnt)
{
	fx_off_t off;
	fx_header_t *hdr = &dsegmnt->sg_hdr;

	/* Header & tail */
	fx_vnode_dexport(&segmnt->sg_vnode, hdr);
	fx_dobj_zpad(hdr, FNX_VTYPE_SEGMNT);

	/* R[0] */
	off = segmnt->sg_segmap.rng.off;
	fx_assert(off == off_floor_seg(off));
	fx_assert(off == (fx_off_t)(hdr->h_xno));
	dsegmnt->sg_off     = fx_htod_off(off);
	dsegmnt->sg_magic   = fx_htod_magic(segmnt->sg_magic);

	/* R[1..14] */
	fx_bzero(dsegmnt->sg_bkaddr, sizeof(dsegmnt->sg_bkaddr));
	for (size_t i = 0; i < FX_NELEMS(dsegmnt->sg_bkaddr); ++i) {
		bkaddr_htod(&segmnt->sg_segmap.bka[i], &dsegmnt->sg_bkaddr[i]);
	}
}

void fx_segmnt_dtoh(fx_segmnt_t *segmnt, const fx_dsegmnt_t *dsegmnt)
{
	int rc;
	fx_off_t off;
	const fx_header_t *hdr = &dsegmnt->sg_hdr;

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_SEGMNT);
	fx_assert(rc == 0);
	fx_vnode_dimport(&segmnt->sg_vnode, hdr);

	/* R[0] */
	off = fx_dtoh_off(dsegmnt->sg_off);
	segmnt->sg_segmap.rng.off = off;
	segmnt->sg_segmap.rng.len = FNX_SEGSIZE;
	segmnt->sg_segmap.rng.idx = 0;
	segmnt->sg_segmap.rng.cnt = FNX_SEGNBK;

	/* R[1..14] */
	for (size_t i = 0; i < FX_NELEMS(segmnt->sg_segmap.bka); ++i) {
		bkaddr_dtoh(&segmnt->sg_segmap.bka[i], &dsegmnt->sg_bkaddr[i]);
	}
}

int fx_segmnt_dcheck(const fx_header_t *hdr)
{
	int rc;
	fx_vtype_e vtype;
	fx_magic_t magic;
	const fx_dsegmnt_t *dsegmnt;

	vtype = fx_dobj_vtype(hdr);
	if (vtype != FNX_VTYPE_SEGMNT) {
		return -1;
	}
	rc = fx_dobj_check(hdr, vtype);
	if (rc != 0) {
		fx_assert(0); /* XXX */
		return -1;
	}
	dsegmnt = header_to_dsegmnt(hdr);
	magic = fx_dtoh_magic(dsegmnt->sg_magic);
	if (magic != FX_SEGMNT_MAGIC) {
		return -1;
	}

	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Symlink operations:
 */
fx_symlnk_t *fx_vnode_to_symlnk(const fx_vnode_t *vnode)
{
	return fx_inode_to_symlnk(vnode_to_inode(vnode));
}

fx_symlnk_t *fx_inode_to_symlnk(const fx_inode_t *inode)
{
	return fx_container_of(inode, fx_symlnk_t, sl_inode);
}

static void symlnk_init(fx_symlnk_t *symlnk, fx_vtype_e vtype)
{
	fx_iattr_t *iattr;

	fx_inode_init(&symlnk->sl_inode, vtype);
	fx_path_init(&symlnk->sl_value);

	iattr  = &symlnk->sl_inode.i_iattr;
	iattr->i_mode       = FX_SYMLNK_INIT_MODE;
	symlnk->sl_magic    = FX_SYMLNK_MAGIC;
}

void fx_symlnk_init(fx_symlnk_t *symlnk, int full)
{
	if (full) {
		symlnk_init(symlnk, FNX_VTYPE_SYMLNK);
	} else {
		symlnk_init(symlnk, FNX_VTYPE_SSYMLNK);
	}
}

void fx_symlnk_destroy(fx_symlnk_t *symlnk)
{
	fx_inode_destroy(&symlnk->sl_inode);
	symlnk->sl_magic    = 0;
}

static size_t symlnk_valbufsize(const fx_symlnk_t *symlnk)
{
	fx_vtype_e vtype;

	vtype = inode_vtype(&symlnk->sl_inode);
	return ((vtype == FNX_VTYPE_SYMLNK) ?
	        FNX_SYMLNK_MAX : FNX_SSYMLNK_MAX);
}

static void symlnk_assign(fx_symlnk_t *symlnk, const fx_path_t *slnk)
{
	size_t bsz, len;
	fx_iattr_t *iattr;

	len = slnk->len;
	bsz = symlnk_valbufsize(symlnk);

	fx_assert(symlnk->sl_magic == FX_SYMLNK_MAGIC);
	fx_assert(len < FNX_PATH_MAX);
	fx_assert(len < bsz);

	fx_path_clone(&symlnk->sl_value, slnk);
	symlnk->sl_inode.i_tlen   = len;
	iattr = &symlnk->sl_inode.i_iattr;
	iattr->i_size   = (fx_off_t)len;
}

void fx_symlnk_setup(fx_symlnk_t *symlnk,
                     const fx_uctx_t *uctx, const fx_path_t *slnk)
{
	mode_t mode, xmsk;

	mode = FX_SYMLNK_INIT_MODE;
	xmsk = FX_SYMLNK_INIT_XMSK;
	fx_inode_setup(&symlnk->sl_inode, uctx, mode, xmsk);
	symlnk_assign(symlnk, slnk);
}

void fx_ssymlnk_htod(const fx_symlnk_t *symlnk, fx_dssymlnk_t *dssymlnk)
{
	fx_dinode_t *dinode = &dssymlnk->sl_inode;

	fx_assert(inode_vtype(&symlnk->sl_inode) == FNX_VTYPE_SSYMLNK);

	/* R[0..7] */
	fx_inode_htod(&symlnk->sl_inode, dinode);

	/* R[8..15] */
	fx_htod_path(dssymlnk->sl_lnk, &dinode->i_tlen, &symlnk->sl_value);
}

void fx_ssymlnk_dtoh(fx_symlnk_t *symlnk, const fx_dssymlnk_t *dssymlnk)
{
	int rc;
	const fx_dinode_t *dinode = &dssymlnk->sl_inode;
	const fx_header_t *hdr    = &dinode->i_hdr;

	fx_assert(inode_vtype(&symlnk->sl_inode) == FNX_VTYPE_SSYMLNK);

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_SSYMLNK);
	fx_assert(rc == 0);

	/* R[0..7] */
	fx_inode_dtoh(&symlnk->sl_inode, dinode);

	/* R[8..15] */
	fx_dtoh_path(&symlnk->sl_value, dssymlnk->sl_lnk, dinode->i_tlen);
	symlnk->sl_inode.i_tlen = symlnk->sl_value.len;
}

void fx_symlnk_htod(const fx_symlnk_t *symlnk, fx_dsymlnk_t *dsymlnk)
{
	fx_dinode_t *dinode = &dsymlnk->sl_inode;

	fx_assert(inode_vtype(&symlnk->sl_inode) == FNX_VTYPE_SYMLNK);

	/* R[0..7] */
	fx_inode_htod(&symlnk->sl_inode, dinode);

	/* R[8..15] */
	fx_htod_path(dsymlnk->sl_lnk, &dinode->i_tlen, &symlnk->sl_value);
}

void fx_symlnk_dtoh(fx_symlnk_t *symlnk, const fx_dsymlnk_t *dsymlnk)
{
	int rc;
	const fx_dinode_t *dinode = &dsymlnk->sl_inode;
	const fx_header_t *hdr    = &dinode->i_hdr;

	fx_assert(inode_vtype(&symlnk->sl_inode) == FNX_VTYPE_SYMLNK);

	/* Header */
	rc = fx_dobj_check(hdr, FNX_VTYPE_SYMLNK);
	fx_assert(rc == 0);

	/* R[0..7] */
	fx_inode_dtoh(&symlnk->sl_inode, dinode);

	/* R[8..15] */
	fx_dtoh_path(&symlnk->sl_value, dsymlnk->sl_lnk, dinode->i_tlen);
	symlnk->sl_inode.i_tlen = symlnk->sl_value.len;
}

int fx_symlnk_dcheck(const fx_header_t *hdr)
{
	int rc;
	fx_vtype_e vtype;

	vtype = fx_dobj_vtype(hdr);
	if ((vtype != FNX_VTYPE_SYMLNK) &&
	    (vtype != FNX_VTYPE_SSYMLNK)) {
		return -1;
	}
	rc = fx_dobj_check(hdr, vtype);
	if (rc != 0) {
		fx_assert(0); /* XXX */
		return -1;
	}
	/* TODO: Extra checks */
	return 0;
}

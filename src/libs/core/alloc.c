/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "index.h"
#include "addr.h"
#include "elems.h"
#include "iobuf.h"
#include "blobs.h"
#include "alloc.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void *fx_mmap_bks(size_t count)
{
	int prot, flags;
	size_t size;
	void *ptr;

	size  = count * FNX_BLKSIZE;
	prot  = PROT_READ | PROT_WRITE;
	flags = MAP_PRIVATE | MAP_ANONYMOUS;
	ptr   = mmap(NULL, size, prot, flags, -1, 0);
	if (ptr == MAP_FAILED) {
		ptr = NULL;
	}
	return ptr;
}

void fx_munmap_bks(void *ptr, size_t count)
{
	size_t size;

	size = count * FNX_BLKSIZE;
	munmap(ptr, size);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void *popq(fx_slink_t *head)
{
	fx_slink_t *slnk;
	slnk = fx_slist_pop_front(head);
	return (void *)slnk;
}

static void pushq(fx_slink_t *head, void *ptr)
{
	fx_slink_t *slnk = (fx_slink_t *)ptr;
	fx_slist_push_front(head, slnk);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_size_t megas(fx_size_t n)
{
	return (n * FNX_MEGA);
}

static void alloc_setup_defaults(fx_balloc_t *alloc)
{
	alloc->blks.limit   = megas(1);
	alloc->blks.pivot   = FNX_SEGNBK;
	alloc->secs.limit   = megas(1);
	alloc->secs.pivot   = alloc->blks.pivot;
	alloc->bkrefs.limit = alloc->blks.limit + alloc->secs.limit;
	alloc->bkrefs.pivot = alloc->bkrefs.limit / 2;
}

void fx_balloc_init(fx_balloc_t *alloc)
{
	fx_bzero(alloc, sizeof(*alloc));
	fx_mutex_init(&alloc->mutex);
	fx_slist_init(&alloc->blks.head);
	fx_slist_init(&alloc->secs.head);
	fx_slist_init(&alloc->bkrefs.head);
	alloc->blks.esize   = sizeof(fx_blk_t);
	alloc->secs.esize   = sizeof(fx_section_t);
	alloc->bkrefs.esize = sizeof(fx_bkref_t);
	alloc_setup_defaults(alloc);
}

void fx_balloc_destroy(fx_balloc_t *alloc)
{
	fx_mutex_destroy(&alloc->mutex);
	fx_bzero(alloc, sizeof(*alloc));
}

static void alloc_lock(fx_balloc_t *alloc)
{
	fx_mutex_lock(&alloc->mutex);
}

static void alloc_unlock(fx_balloc_t *alloc)
{
	fx_mutex_unlock(&alloc->mutex);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static size_t getnbk(const fx_bucket_t *bucket)
{
	return fx_bytes_to_nbk(bucket->esize);
}

static void *new_mmapped(fx_bucket_t *bucket)
{
	void *ptr = NULL;

	fx_assert(bucket->inter <= bucket->total);
	if (bucket->total == bucket->limit) {
		return NULL;
	}

	if (bucket->inter > 0) {
		ptr = popq(&bucket->head);
		fx_bzero(ptr, bucket->esize);
		bucket->inter--;
	} else {
		ptr = fx_mmap_bks(getnbk(bucket));
		if (ptr != NULL) {
			bucket->total++;
		}
	}
	return ptr;
}

static void del_mmapped(fx_bucket_t *bucket, void *ptr)
{
	fx_assert(ptr != NULL);
	fx_assert(bucket->total > 0);
	fx_assert(bucket->inter <= bucket->total);

	if (bucket->inter < bucket->pivot) {
		pushq(&bucket->head, ptr);
		bucket->inter++;
	} else {
		fx_munmap_bks(ptr, getnbk(bucket));
		bucket->total--;
	}
}

static void clear_mmapped(fx_bucket_t *bucket)
{
	void *ptr;
	size_t esz, nbk;

	esz = bucket->esize;
	nbk = fx_bytes_to_nbk(esz);
	while ((ptr = popq(&bucket->head)) != NULL) {
		fx_munmap_bks(ptr, nbk);
		bucket->total--;
		bucket->inter--;
	}
}

static void *new_malloced(fx_bucket_t *bucket)
{
	size_t esz;
	void *ptr = NULL;

	esz = bucket->esize;
	if (bucket->total < bucket->limit) {
		if (bucket->inter > 0) {
			ptr = popq(&bucket->head);
			fx_assert(ptr != NULL);
			bucket->inter--;
		} else {
			ptr = fx_xmalloc(esz, FX_BZERO | FX_NOFAIL); /* XXX */
			if (ptr != NULL) {
				bucket->total++;
			}
		}
	}
	return ptr;
}

static void del_malloced(fx_bucket_t *bucket, void *ptr)
{
	size_t esz;

	fx_assert(ptr != NULL);
	fx_assert(bucket->total > 0);

	esz = bucket->esize;
	if (bucket->inter < bucket->pivot) {
		pushq(&bucket->head, ptr);
		bucket->inter++;
	} else {
		fx_xfree(ptr, esz, 0);
		bucket->total--;
	}
}

static void clear_malloced(fx_bucket_t *bucket)
{
	void *ptr;
	size_t esz;

	esz = bucket->esize;
	while ((ptr = popq(&bucket->head)) != NULL) {
		fx_xfree(ptr, esz, 0);
		bucket->total--;
		bucket->inter--;
	}
}

static void *alloc_new_blk(fx_balloc_t *alloc)
{
	return new_mmapped(&alloc->blks);
}

static void alloc_del_blk(fx_balloc_t *alloc, void *ptr)
{
	del_mmapped(&alloc->blks, ptr);
}

static void *alloc_new_sec(fx_balloc_t *alloc)
{
	return new_mmapped(&alloc->secs);
}

static void alloc_del_sec(fx_balloc_t *alloc, void *ptr)
{
	del_mmapped(&alloc->secs, ptr);
}

static fx_bkref_t *alloc_new_bkref(fx_balloc_t *alloc)
{
	fx_bkref_t *bkref = NULL;

	bkref = (fx_bkref_t *)new_malloced(&alloc->bkrefs);
	fx_assert(bkref != NULL);
	fx_bkref_init(bkref);

	return bkref;
}

static void alloc_del_bkref(fx_balloc_t *alloc, fx_bkref_t *bkref)
{
	fx_bkref_destroy(bkref);
	del_malloced(&alloc->bkrefs, bkref);
}


void fx_balloc_clear(fx_balloc_t *alloc)
{
	alloc_lock(alloc);
	clear_mmapped(&alloc->blks);
	clear_mmapped(&alloc->secs);
	clear_malloced(&alloc->bkrefs);
	alloc_unlock(alloc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_bkref_t *fx_balloc_new_sec(fx_balloc_t *alloc, const fx_bkaddr_t *bkaddr)
{
	void *blk;
	fx_bkref_t *bkref = NULL;

	alloc_lock(alloc);
	blk = alloc_new_sec(alloc);
	if (blk == NULL) {
		goto out;
	}
	bkref = alloc_new_bkref(alloc);
	if (bkref == NULL) {
		alloc_del_sec(alloc, blk);
		goto out;
	}
	fx_bkref_setup(bkref, bkaddr, blk);
	bkref->vtype = FNX_VTYPE_SEC;

out:
	alloc_unlock(alloc);
	return bkref;
}

fx_bkref_t *fx_balloc_new_bk(fx_balloc_t *alloc, const fx_bkaddr_t *bkaddr)
{
	void *blk;
	fx_bkref_t *bkref = NULL;

	alloc_lock(alloc);
	blk = alloc_new_blk(alloc);
	if (blk == NULL) {
		goto out;
	}
	bkref = alloc_new_bkref(alloc);
	if (bkref == NULL) {
		alloc_del_blk(alloc, blk);
		goto out;
	}
	fx_bkref_setup(bkref, bkaddr, blk);
	bkref->vtype = FNX_VTYPE_BK;

out:
	alloc_unlock(alloc);
	return bkref;
}

void fx_balloc_del_bk(fx_balloc_t *alloc, fx_bkref_t *bkref)
{
	void *blk;
	fx_vtype_e vtype;

	blk   = bkref->blk;
	vtype = bkref->vtype;

	alloc_lock(alloc);
	alloc_del_bkref(alloc, bkref);
	if (vtype == FNX_VTYPE_SEC) {
		alloc_del_sec(alloc, blk);
	} else if (vtype == FNX_VTYPE_BK) {
		alloc_del_blk(alloc, blk);
	} else {
		fx_panic("bad-bkref vtype=%d", (int)vtype);
	}
	alloc_unlock(alloc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#define VALLOC_MAGIC (0x93AABB7)

static void *valloc_malloc(size_t sz, void *hint)
{
	fx_valloc_t *valloc = (fx_valloc_t *)hint;

	fx_assert(valloc->magic == VALLOC_MAGIC);
	return fx_mallocator->malloc(sz, NULL);
}

static void valloc_free(void *ptr, size_t n, void *hint)
{
	fx_valloc_t *valloc = (fx_valloc_t *)hint;

	fx_assert(valloc->magic == VALLOC_MAGIC);
	fx_mallocator->free(ptr, n, NULL);
}

void fx_valloc_init(fx_valloc_t *valloc)
{
	fx_bzero(valloc, sizeof(*valloc));
	valloc->alloc.malloc    = valloc_malloc;
	valloc->alloc.free      = valloc_free;
	valloc->alloc.userp     = valloc;
	valloc->magic           = VALLOC_MAGIC;
}

void fx_valloc_destroy(fx_valloc_t *valloc)
{
	fx_bzero(valloc, sizeof(*valloc));
	valloc->magic       = 3;
}

void fx_valloc_clear(fx_valloc_t *valloc)
{
	fx_unused(valloc); /* XXX */
}

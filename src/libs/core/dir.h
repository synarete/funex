/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_DIR_H_
#define FUNEX_DIR_H_


fx_dir_t *fx_vnode_to_dir(const fx_vnode_t *);

fx_dir_t *fx_inode_to_dir(const fx_inode_t *);

void fx_dir_init(fx_dir_t *);

void fx_dir_destroy(fx_dir_t *);

void fx_dir_setup(fx_dir_t *, const fx_uctx_t *, fx_mode_t);

void fx_dir_htod(const fx_dir_t *, fx_ddir_t *);

void fx_dir_dtoh(fx_dir_t *, const fx_ddir_t *);

int fx_dir_dcheck(const fx_header_t *);


void fx_dir_iref_parentd(fx_dir_t *, const fx_dir_t *);

void fx_dir_unref_parentd(fx_dir_t *);


const fx_dirent_t *fx_dir_meta(const fx_dir_t *, const fx_name_t *);

fx_dirent_t *fx_dir_lookup(const fx_dir_t *, fx_hash_t, size_t);

const fx_dirent_t *fx_dir_search(const fx_dir_t *, fx_off_t);

fx_dirent_t *fx_dir_link(fx_dir_t *, const fx_inode_t *);

fx_dirent_t *fx_dir_unlink(fx_dir_t *, const fx_inode_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_dirext_t *fx_vnode_to_dirext(const fx_vnode_t *);

void fx_dirext_init(fx_dirext_t *);

void fx_dirext_destroy(fx_dirext_t *);

void fx_dirext_htod(const fx_dirext_t *, fx_ddirext_t *);

void fx_dirext_dtoh(fx_dirext_t *, const fx_ddirext_t *);

int fx_dirext_dcheck(const fx_header_t *);


fx_dirent_t *fx_dirext_lookup(const fx_dirext_t *, fx_hash_t, fx_size_t);

fx_dirent_t *fx_dirext_search(const fx_dirext_t *, fx_off_t);

fx_dirent_t *fx_dirext_link(fx_dirext_t *, const fx_inode_t *);

fx_dirent_t *fx_dirext_unlink(fx_dirext_t *, const fx_inode_t *);


int fx_dirext_isempty(const fx_dirext_t *);

int fx_dirext_hasseg(const fx_dirext_t *, fx_off_t);

void fx_dirext_setseg(fx_dirext_t *, fx_off_t);

void fx_dirext_unsetseg(fx_dirext_t *, fx_off_t);

fx_off_t fx_dirext_nextseg(const fx_dirext_t *, fx_off_t);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_dirseg_t *fx_vnode_to_dirseg(const fx_vnode_t *);

void fx_dirseg_init(fx_dirseg_t *);

void fx_dirseg_destroy(fx_dirseg_t *);

void fx_dirseg_setup(fx_dirseg_t *, fx_off_t);

int fx_dirseg_isempty(const fx_dirseg_t *);

void fx_dirseg_htod(const fx_dirseg_t *, fx_ddirseg_t *);

void fx_dirseg_dtoh(fx_dirseg_t *, const fx_ddirseg_t *);

int fx_dirseg_dcheck(const fx_header_t *);


fx_dirent_t *fx_dirseg_lookup(const fx_dirseg_t *, fx_hash_t, fx_size_t);

fx_dirent_t *fx_dirseg_search(const fx_dirseg_t *, fx_off_t);

fx_dirent_t *fx_dirseg_link(fx_dirseg_t *, const fx_inode_t *);

fx_dirent_t *fx_dirseg_unlink(fx_dirseg_t *, const fx_inode_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_hash_t fx_inamehash(const fx_name_t *, const fx_dir_t *);

#endif /* FUNEX_DIR_H_ */

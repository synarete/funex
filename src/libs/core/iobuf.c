/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/uio.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "addr.h"
#include "addri.h"
#include "alloc.h"
#include "elems.h"
#include "iobuf.h"
#include "execq.h"


/* Nil-block */
static const fx_blk_t s_nilbk;



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_off_t off_within_blk(fx_off_t off)
{
	return off % (fx_off_t)FNX_BLKSIZE;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void *getdptr(const void *blk, fx_off_t off)
{
	return ((char *)blk + (ptrdiff_t)off);
}

static void *getnptr(const void *buf, fx_size_t len)
{
	return getdptr(buf, (fx_off_t)len);
}

static void copy_data(void *tgtb, fx_off_t tgt_off,
                      const void *srcb, fx_off_t src_off, fx_size_t len)
{
	void *tgt;
	const void *src;
	fx_off_t end;

	end = off_end(tgt_off, len);
	fx_assert((end - tgt_off) <= FNX_BLKSIZE);
	end = off_end(src_off, len);
	fx_assert((end - src_off) <= FNX_BLKSIZE);

	tgt = getdptr(tgtb, tgt_off);
	src = getdptr(srcb, src_off);

	memcpy(tgt, src, len);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
bkref_assign(fx_bkref_t *bkref, fx_off_t off, fx_size_t len, const void *buf)
{
	fx_assert(bkref->vtype == FNX_VTYPE_BK);
	fx_assert(len <= FNX_BLKSIZE);
	fx_assert(((size_t)off_within_blk(off) + len) <= FNX_BLKSIZE);
	copy_data(bkref->blk, off, buf, 0, len);
}

static void
bkref_merge(fx_bkref_t *bkref,
            fx_off_t xoff, fx_size_t xlen, const fx_bkref_t *other)
{
	fx_off_t  off;
	fx_size_t len;

	fx_assert(bkref->vtype == FNX_VTYPE_BK);
	fx_assert(other->vtype == FNX_VTYPE_BK);
	fx_assert(xlen <= FNX_BLKSIZE);
	fx_assert(((size_t)off_within_blk(xoff) + xlen) <= FNX_BLKSIZE);

	off  = off_within_blk(xoff);
	len  = off_len(0, off);
	copy_data(bkref->blk, 0, other->blk, 0, len);

	off = off_end(off, xlen);
	len = off_len(off, FNX_BLKSIZE);
	copy_data(bkref->blk, off, other->blk, off, len);
}

static void
datab_to_iovec(const void *datab,
               fx_off_t off, fx_size_t len, fx_iovec_t *iov)
{
	fx_assert(datab != NULL);
	fx_assert(off < FNX_BLKSIZE);
	fx_assert((off + (int)len) <= FNX_BLKSIZE);

	iov->iov_base = getdptr(datab, off);
	iov->iov_len  = len;
}

static void
bkref_to_iovec(const fx_bkref_t *bkref,
               fx_off_t off, fx_size_t len, fx_iovec_t *iov)
{
	fx_assert(bkref != NULL);
	datab_to_iovec(bkref->blk, off, len, iov);
}

void fx_bkref_init(fx_bkref_t *bkref)
{
	fx_xelem_init(&bkref->xelem);
	link_init(&bkref->clink);
	bkaddr_reset(&bkref->addr);
	bkref->vtype    = FNX_VTYPE_NONE;
	bkref->ino      = FNX_INO_NULL;
	bkref->refcnt   = 0;
	bkref->stain    = FX_STAIN_NONE;
	bkref->blk      = NULL;
	bkref->next     = NULL;
	bkref->hlnk     = NULL;
	bkref->flags    = 0;
	bkref->magic    = FNX_MAGIC9;
}

void fx_bkref_destroy(fx_bkref_t *bkref)
{
	fx_xelem_destroy(&bkref->xelem);
	fx_link_destroy(&bkref->clink);
	bkaddr_reset(&bkref->addr);
	bkref->blk      = NULL;
	bkref->flags    = -1;
	bkref->magic    = 9;
}

void fx_bkref_setup(fx_bkref_t *bkref, const fx_bkaddr_t *addr, void *blk)
{
	bkref->blk = blk;
	fx_bkaddr_setup(&bkref->addr, addr->svol, addr->lba, 0);
}

fx_bkcnt_t fx_bkref_numbk(const fx_bkref_t *bkref)
{
	fx_assert((bkref->vtype == FNX_VTYPE_SEC) ||
	          (bkref->vtype == FNX_VTYPE_BK));
	return (bkref->vtype == FNX_VTYPE_SEC) ? FNX_SECNBK : 1;
}

void fx_bkref_merge(fx_bkref_t *bkref, fx_off_t xoff,
                    fx_size_t xlen, const fx_bkref_t *other)
{
	bkref_merge(bkref, xoff, xlen, other);
}

void fx_bkref_clone(const fx_bkref_t *bkref, fx_bkref_t *other)
{
	size_t len;

	fx_assert(other->vtype == bkref->vtype);
	len = fx_bkref_numbk(bkref) * FNX_BLKSIZE;
	memcpy(other->blk, bkref->blk, len);
	bkaddr_copy(&other->addr, &bkref->addr);
	other->ino      = bkref->ino;
}



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_bkref_t *malloc_bk(fx_balloc_t *alloc)
{
	const fx_bkaddr_t nil_bkaddr = { FNX_SVOL_NULL, FNX_LBA_NULL, 0 };
	return fx_balloc_new_bk(alloc, &nil_bkaddr);
}

static void free_bk(fx_balloc_t *alloc, fx_bkref_t *bkref)
{
	fx_balloc_del_bk(alloc, bkref);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void fx_iobuf_init(fx_iobuf_t *iobuf, fx_ino_t ino, fx_balloc_t *alloc)
{
	fx_segmap_init(&iobuf->sgm);
	fx_bzero(iobuf->bks, sizeof(iobuf->bks));
	iobuf->ino   = ino;
	iobuf->alloc = alloc;
}

static void fx_iobuf_destroy(fx_iobuf_t *iobuf)
{
	fx_segmap_destroy(&iobuf->sgm);
	fx_bzero(iobuf->bks, sizeof(iobuf->bks));
	iobuf->ino   = FNX_INO_NULL;
	iobuf->alloc = NULL;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void iobuf_dealloc_bks(fx_iobuf_t *iobuf)
{
	size_t i, pos;
	fx_bkref_t **bkref;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	for (i = 0; i < brange->cnt; ++i) {
		pos   = brange->idx + i;
		bkref = &iobuf->bks[pos];
		if (*bkref != NULL) {
			free_bk(iobuf->alloc, *bkref);
			*bkref = NULL;
		}
	}
}

static int iobuf_alloc_bks(fx_iobuf_t *iobuf)
{
	fx_bkref_t *bkref;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos = brange->idx + i;
		fx_assert(pos < FX_NELEMS(iobuf->bks));
		fx_assert(iobuf->bks[pos] == NULL);

		bkref = malloc_bk(iobuf->alloc);
		if (bkref == NULL) {
			iobuf_dealloc_bks(iobuf);
			return -ENOMEM;
		}
		iobuf->bks[pos] = bkref;
	}
	return 0;
}

static void
iobuf_assign_buf(const fx_iobuf_t *iobuf, const void *buf)
{
	fx_off_t off, off2, soff;
	fx_size_t len, len2, slen;
	const void *dat;
	fx_bkref_t *bkref;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	off = brange->off;
	len = brange->len;
	dat = buf;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos   = brange->idx + i;
		bkref = iobuf->bks[pos];

		soff  = off_within_blk(off);
		off2  = off_ceil_blk(off);
		len2  = off_len(off, off2);
		slen  = size_min(len, len2);
		bkref_assign(bkref, soff, slen, dat);

		dat = getnptr(dat, slen);
		off = off_end(off, slen);
		len -= slen;
	}
}

static int
iobuf_assign(fx_iobuf_t *iobuf, fx_off_t off, fx_size_t len, const void *buf)
{
	int rc;

	if ((iobuf == NULL) && (len == 0)) {
		return 0;
	}
	rc = fx_segmap_setup(&iobuf->sgm, off, len);
	if (rc != 0) {
		return -1;
	}
	if (buf == NULL) {
		return 0; /* OK, no-copy-data */
	}
	rc = iobuf_alloc_bks(iobuf);
	if (rc != 0) {
		return rc;
	}
	iobuf_assign_buf(iobuf, buf);
	return 0;
}

static int iobuf_mkiovec(const fx_iobuf_t *iobuf,
                         fx_iovec_t *iovec, size_t cnt, size_t *res)
{
	fx_off_t off, off2, soff;
	fx_size_t len, slen, len2;
	const fx_bkref_t  *bkref;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	if (cnt < brange->cnt) {
		return -1;
	}
	off = brange->off;
	len = brange->len;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos  = brange->idx + i;

		soff = off_within_blk(off);
		off2 = off_ceil_blk(off);
		len2 = off_len(off, off2);
		slen = size_min(len, len2);

		bkref = iobuf->bks[pos];
		if (bkref != NULL) {
			bkref_to_iovec(bkref, soff, slen, &iovec[i]);
		} else {
			datab_to_iovec(s_nilbk, soff, slen, &iovec[i]);
		}

		off = off_end(off, slen);
		len -= slen;
	}
	*res = brange->cnt;
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static size_t niob(const fx_iobufs_t *iob)
{
	return FX_NELEMS((iob)->iob);
}

void fx_iobufs_init(fx_iobufs_t *iobufs)
{
	for (size_t i = 0; i < niob(iobufs); ++i) {
		fx_iobuf_init(&iobufs->iob[i], FNX_INO_NULL, NULL);
	}
}

void fx_iobufs_destroy(fx_iobufs_t *iobufs)
{
	for (size_t i = 0; i < niob(iobufs); ++i) {
		fx_iobuf_destroy(&iobufs->iob[i]);
	}
}

void fx_iobufs_setup(fx_iobufs_t *iobufs, fx_ino_t ino, fx_balloc_t *alloc)
{
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobufs->iob[i].ino = ino;
		iobufs->iob[i].alloc = alloc;
	}
}

void fx_iobufs_release(fx_iobufs_t *iobufs)
{
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobuf_dealloc_bks(&iobufs->iob[i]);
	}
}

static fx_bkcnt_t iobufs_bkcnt(const fx_iobufs_t *iobufs)
{
	fx_bkcnt_t cnt = 0;

	for (size_t i = 0; i < niob(iobufs); ++i) {
		cnt += iobufs->iob[i].sgm.rng.cnt;
	}
	return cnt;
}

fx_off_t fx_iobufs_end(const fx_iobufs_t *iobufs)
{
	fx_off_t off, end = 0;

	for (size_t i = 0; i < niob(iobufs); ++i) {
		off = fx_brange_end(&iobufs->iob[i].sgm.rng);
		end = off_max(off, end);
	}
	return end;
}

int fx_iobufs_assign(fx_iobufs_t *iobufs,
                     fx_off_t off, fx_size_t len, const void *buf)
{
	int rc = -1;
	fx_off_t uoff;
	fx_size_t rlen, slen;

	if (!fx_isvalid_seg(off, len)) {
		return -1;
	}

	for (size_t i = 0; i < niob(iobufs); ++i) {
		uoff = off_ceil_seg(off);
		rlen = off_len(off, uoff);
		slen = size_min(len, rlen);
		if (slen == 0) {
			break;
		}
		rc = iobuf_assign(&iobufs->iob[i], off, slen, buf);
		if (rc != 0) {
			break;
		}

		off = off_end(off, slen);
		len -= slen;
		if (buf != NULL) {
			buf = getnptr(buf, slen);
		}
	}

	return rc;
}

int fx_iobufs_mkiovec(const fx_iobufs_t *iobufs,
                      fx_iovec_t *iovec, size_t cnt, size_t *res)
{
	int rc = -1;
	size_t nn, total, nset = 0;

	total = iobufs_bkcnt(iobufs);
	if (total > cnt) {
		return -1;
	}

	for (size_t i = 0; i < niob(iobufs); ++i) {
		nn = 0;
		rc = iobuf_mkiovec(&iobufs->iob[i], &iovec[nset], cnt - nset, &nn);
		if (rc != 0) {
			break;
		}
		nset += nn;
	}
	*res = nset;

	return rc;
}


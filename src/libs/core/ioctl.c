/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "proto.h"
#include "ioctl.h"

#define FX_IOCTL_MAGIC     (0xFE)
#define FX_IOWR(nr, t)     ((unsigned)(_IOWR(FX_IOCTL_MAGIC, nr, t)))

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const fx_iocdef_t s_ioctl_fquery = {
	.nmbr   = FX_OP_FQUERY,
	.cmd    = FX_IOWR(FX_OP_FQUERY, fx_iocargs_t),
	.size   = sizeof(fx_iocargs_t),
};

static const fx_iocdef_t s_ioctl_xcopy = {
	.nmbr   = FX_OP_XCOPY,
	.cmd    = FX_IOWR(FX_OP_XCOPY, fx_iocargs_t),
	.size   = sizeof(fx_iocargs_t),
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

const fx_iocdef_t *fx_iocdef_by_opc(fx_opcode_e opc)
{
	const fx_iocdef_t *ioc_info;

	switch ((int)opc) {
		case FX_OP_FQUERY:
			ioc_info = &s_ioctl_fquery;
			break;
		case FX_OP_XCOPY:
			ioc_info = &s_ioctl_xcopy;
			break;
		default:
			ioc_info = NULL;
			break;
	}
	return ioc_info;
}

const fx_iocdef_t *fx_iocdef_by_cmd(int cmd)
{
	unsigned ucmd, nmbr;

	ucmd = (unsigned)cmd;
	nmbr = ((unsigned)(_IOC_NR(ucmd)));
	return fx_iocdef_by_opc((fx_opcode_e)nmbr);
}


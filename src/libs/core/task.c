/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "proto.h"
#include "addr.h"
#include "elems.h"
#include "iobuf.h"
#include "execq.h"
#include "task.h"

#define TASK_MAGIC   (0xABCD)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_task_t *fx_xelem_to_task(const fx_xelem_t *xelem)
{
	return fx_container_of(xelem, fx_task_t, tsk_xelem);
}

fx_task_t *fx_link_to_task(const fx_link_t *qlnk)
{
	const fx_xelem_t *xelem;

	xelem = fx_qlink_to_xelem(qlnk);
	return fx_xelem_to_task(xelem);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_task_init(fx_task_t *task, fx_opcode_e opc)
{
	fx_bzero(task, sizeof(*task));

	fx_xelem_init(&task->tsk_xelem);
	fx_userctx_init(&task->tsk_uctx);
	fx_iobufs_init(&task->tsk_iobufs);
	task->tsk_xelem.xtype   = FX_XTYPE_TASK;
	task->tsk_opcode        = opc;
	task->tsk_seqno         = 0;
	task->tsk_flags         = 0;
	task->tsk_fuse_req      = NULL;
	task->tsk_fuse_chan     = NULL;
	task->tsk_fref          = NULL;
	task->tsk_fusei         = NULL;
	task->tsk_magic         = TASK_MAGIC;
}

void fx_task_destroy(fx_task_t *task)
{
	fx_iobufs_destroy(&task->tsk_iobufs);
	fx_userctx_destroy(&task->tsk_uctx);
	fx_xelem_destroy(&task->tsk_xelem);
	task->tsk_opcode        = FX_OP_NONE;
	task->tsk_fuse_req      = NULL;
	task->tsk_fuse_chan     = NULL;
	task->tsk_fref          = NULL;
	task->tsk_fusei         = NULL;
	task->tsk_magic         = 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Normal task: push to queue 1 */
void fx_send_task(fx_taskq_t *taskq, fx_task_t *task, fx_flags_t mask)
{
	task->tsk_flags |= mask;
	task->tsk_xelem.xtype = FX_XTYPE_TASK;
	fx_execq_enqueue(taskq, &task->tsk_xelem, 1);
}

/* Urgent (reply) task: push to queue 0 */
void fx_send_utask(fx_taskq_t *taskq, fx_task_t *task)
{
	task->tsk_xelem.xtype = FX_XTYPE_TASK;
	fx_execq_enqueue(taskq, &task->tsk_xelem, 0);
}

fx_task_t *fx_recv_tasks(fx_taskq_t *taskq, unsigned n, unsigned timeout)
{
	fx_xelem_t *xelem;
	fx_task_t  *task = NULL;

	xelem = fx_execq_dequeue(taskq, n, timeout);
	if (xelem != NULL) {
		fx_assert(xelem->xtype == FX_XTYPE_TASK);
		task = fx_xelem_to_task(xelem);
		fx_assert(task->tsk_magic == TASK_MAGIC);
	}
	return task;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void taskpool_init_privates(fx_taskpool_t *taskpool)
{
	fx_task_t *task;
	fx_link_t *qlnk;

	for (size_t i = 0; i < FX_NELEMS(taskpool->pool); ++i) {
		task = &taskpool->pool[i];
		qlnk = &task->tsk_xelem.qlink;
		fx_list_push_back(&taskpool->frees, qlnk);
	}
}

static int
taskpool_isprivate(const fx_taskpool_t *taskpool, const fx_task_t *task)
{
	const fx_task_t *beg;
	const fx_task_t *end;

	beg = taskpool->pool;
	end = beg + FX_NELEMS(taskpool->pool);
	return ((task >= beg) && (task < end));
}

void fx_taskpool_init(fx_taskpool_t *taskpool)
{
	fx_mutex_init(&taskpool->mutex);
	fx_list_init(&taskpool->frees);
	taskpool_init_privates(taskpool);
}

void fx_taskpool_destroy(fx_taskpool_t *taskpool)
{
	fx_list_destroy(&taskpool->frees);
	fx_mutex_destroy(&taskpool->mutex);
}

static void taskpool_lock(fx_taskpool_t *taskpool)
{
	fx_mutex_lock(&taskpool->mutex);
}

static void taskpool_unlock(fx_taskpool_t *taskpool)
{
	fx_mutex_unlock(&taskpool->mutex);
}

static fx_task_t *taskpool_popfree(fx_taskpool_t *taskpool)
{
	fx_link_t *qlnk;
	fx_task_t *task = NULL;

	taskpool_lock(taskpool);
	qlnk = fx_list_pop_front(&taskpool->frees);
	taskpool_unlock(taskpool);
	if (qlnk != NULL) {
		task = fx_link_to_task(qlnk);
	}
	return task;
}

static void taskpool_pushfree(fx_taskpool_t *taskpool, fx_task_t *task)
{
	fx_link_t *qlnk;

	qlnk = &task->tsk_xelem.qlink;
	taskpool_lock(taskpool);
	fx_list_push_back(&taskpool->frees, qlnk);
	taskpool_unlock(taskpool);
}

fx_task_t *fx_taskpool_newtask(fx_taskpool_t *taskpool, fx_opcode_e opc)
{
	const int flags = FX_BZERO | FX_NOFAIL;
	fx_task_t *task;

	task = taskpool_popfree(taskpool);
	if (task == NULL) {
		task = (fx_task_t *)fx_xmalloc(sizeof(*task), flags);
	}
	if (task != NULL) {
		fx_task_init(task, opc);
	}
	return task;
}

void fx_taskpool_deltask(fx_taskpool_t *taskpool, fx_task_t *task)
{
	fx_task_destroy(task);
	if (taskpool_isprivate(taskpool, task)) {
		taskpool_pushfree(taskpool, task);
	} else {
		fx_free(task, sizeof(*task));
	}
}

void fx_taskpool_cleanup(fx_taskpool_t *taskpool)
{
	fx_task_t *task;

	while ((task = taskpool_popfree(taskpool)) != NULL) {
		fx_task_destroy(task);
	}
}



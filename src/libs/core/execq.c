/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "addr.h"
#include "elems.h"
#include "execq.h"


fx_xelem_t *fx_qlink_to_xelem(const fx_link_t *qlink)
{
	return fx_container_of(qlink, fx_xelem_t, qlink);
}

void fx_xelem_init(fx_xelem_t *xelem)
{
	link_init(&xelem->qlink);
	link_init(&xelem->plink);
	xelem->xtype    = FX_XTYPE_NONE;
	xelem->status   = 0;
}

void fx_xelem_destroy(fx_xelem_t *xelem)
{
	link_destroy(&xelem->plink);
	link_destroy(&xelem->qlink);
	xelem->xtype    = FX_XTYPE_NONE;
	xelem->status   = -10000;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_execq_init(fx_execq_t *execq)
{
	fx_fifo_init(execq);
}

void fx_execq_destroy(fx_execq_t *execq)
{
	fx_fifo_destroy(execq);
}

int fx_execq_isempty(const fx_execq_t *execq)
{
	return fx_fifo_isempty(execq);
}

void fx_execq_enqueue(fx_execq_t *execq, fx_xelem_t *xelem, int q)
{
	fx_fifo_pushq(execq, &xelem->qlink, q);
}

fx_xelem_t *fx_execq_dequeue(fx_execq_t *execq, unsigned n, unsigned timeout)
{
	fx_link_t  *qlink;
	fx_xelem_t *xelem = NULL;

	qlink = fx_fifo_popn(execq, n, timeout);
	if (qlink != NULL) {
		xelem = fx_qlink_to_xelem(qlink);
	}
	return xelem;
}

static fx_xelem_t *fx_execq_trydequeue(fx_execq_t *execq)
{
	fx_link_t  *qlink;
	fx_xelem_t *xelem = NULL;

	qlink = fx_fifo_trypop(execq);
	if (qlink != NULL) {
		xelem = fx_qlink_to_xelem(qlink);
	}
	return xelem;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_bkref_t *fx_xelem_to_bkref(const fx_xelem_t *xelem)
{
	return fx_container_of(xelem, fx_bkref_t, xelem);
}

fx_bkref_t *fx_qlink_to_bkref(const fx_link_t *qlnk)
{
	const fx_xelem_t *xelem;

	xelem = fx_qlink_to_xelem(qlnk);
	return fx_xelem_to_bkref(xelem);
}

fx_bkref_t *fx_recv_bks(fx_execq_t *execq, unsigned n, unsigned timeout)
{
	fx_xelem_t *xelem;
	fx_bkref_t *bkref = NULL;

	xelem = fx_execq_dequeue(execq, n, timeout);
	if (xelem != NULL) {
		bkref = fx_xelem_to_bkref(xelem);
		fx_assert(bkref->magic == FNX_MAGIC9);
	}
	return bkref;
}

fx_bkref_t *fx_recv_bk(fx_execq_t *execq)
{
	fx_xelem_t *xelem;
	fx_bkref_t *bkref = NULL;

	xelem = fx_execq_trydequeue(execq);
	if (xelem != NULL) {
		bkref = fx_xelem_to_bkref(xelem);
		fx_assert(bkref->magic == FNX_MAGIC9);
	}
	return bkref;
}


void fx_send_bk(fx_execq_t *execq, fx_bkref_t *bkref)
{
	fx_execq_enqueue(execq, &bkref->xelem, 1);
}

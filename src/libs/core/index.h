/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_INDEX_H_
#define FUNEX_INDEX_H_

struct fx_bkref;
struct fx_bkaddr;
struct fx_vaddr;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* On-disk object sizes in bytes/fragments units */
fx_size_t fx_metasize(fx_vtype_e);

fx_size_t fx_dobjnfrg(fx_vtype_e);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Metadata on-disk-object operations */
int fx_dobj_check(const fx_header_t *, fx_vtype_e);

int fx_dobj_icheck(const fx_header_t *);

void fx_dobj_zpad(fx_header_t *, fx_vtype_e);

void fx_dobj_assign(fx_header_t *, fx_vtype_e, fx_ino_t, fx_ino_t, fx_size_t);

void fx_dobj_clear(fx_header_t *, size_t len);

fx_vtype_e fx_dobj_vtype(const fx_header_t *);

fx_size_t fx_dobj_getlen(const fx_header_t *);

fx_ino_t fx_dobj_getino(const fx_header_t *);

fx_ino_t fx_dobj_getxno(const fx_header_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Metadata dsection operations */
void fx_dsec_reset(fx_dsec_t *);

void fx_dsec_signsum(fx_dsec_t *);

int fx_dsec_checksum(const fx_dsec_t *);

fx_off_t fx_dsec_frgoff(const fx_dsec_t *, const fx_dfrg_t *);

fx_dfrg_t *fx_dsec_search_vnode(const fx_dsec_t *, const struct fx_vaddr *);

fx_dfrg_t *fx_dsec_search_inode(const fx_dsec_t *, fx_ino_t);

fx_dfrg_t *fx_dsec_search_free(fx_dsec_t *, fx_vtype_e);

void fx_dsec_chopat(fx_dsec_t *, fx_dfrg_t *, fx_vtype_e);

void fx_dsec_joinat(fx_dsec_t *, fx_dfrg_t *, fx_vtype_e);


fx_dsec_t *fx_get_dsec(const struct fx_bkref *);

fx_dfrg_t *fx_get_dfrg(const struct fx_bkref *, const struct fx_bkaddr *);

fx_header_t *fx_get_dobj(const struct fx_bkref *, const struct fx_bkaddr *);


#endif /* FUNEX_INDEX_H_ */



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_INODEI_H_
#define FUNEX_INODEI_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline fx_vtype_e vnode_vtype(const fx_vnode_t *vnode)
{
	return vnode->v_vaddr.vtype;
}

static inline void vnode_setvaddr(fx_vnode_t *vnode, const fx_vaddr_t *vaddr)
{
	vnode->v_vaddr.ino    = vaddr->ino;
	vnode->v_vaddr.xno    = vaddr->xno;
	vnode->v_vaddr.vtype  = vaddr->vtype;
}

static inline int vnode_isinode(const fx_vnode_t *vnode)
{
	return fx_isitype(vnode->v_vaddr.vtype);
}

static inline int vnode_ispseudo(const fx_vnode_t *vnode)
{
	return vnode->v_pseudo;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline fx_inode_t *vnode_to_inode(const fx_vnode_t *vnode)
{
	return fx_vnode_to_inode(vnode);
}

static inline fx_vtype_e inode_vtype(const fx_inode_t *inode)
{
	return vnode_vtype(&inode->i_vnode);
}

static inline fx_ino_t inode_getino(const fx_inode_t *inode)
{
	return inode->i_vnode.v_vaddr.ino;
}

static inline fx_ino_t inode_getrefino(const fx_inode_t *inode)
{
	return ((inode_vtype(inode) != FNX_VTYPE_REFLNK) ?
	        inode_getino(inode) : inode->i_refino);
}

static inline int inode_hasino(const fx_inode_t *inode, fx_ino_t ino)
{
	return (ino == inode_getino(inode));
}

static inline void inode_setitime(fx_inode_t *inode, fx_flags_t flags)
{
	fx_inode_setitime(inode, flags);
}

static inline int
inode_isname(const fx_inode_t *inode, const fx_name_t *name)
{
	return fx_name_isnequal(&inode->i_name, name->str, name->len);
}

static inline int
inode_isowner(const fx_inode_t *inode, const fx_uctx_t *uctx)
{
	return (inode->i_iattr.i_uid == uctx->u_cred.cr_uid);
}

static inline void
inode_getiattr(const fx_inode_t *inode, fx_iattr_t *iattr)
{
	fx_inode_getiattr(inode, iattr);
}

static inline fx_off_t inode_getsize(const fx_inode_t *inode)
{
	return inode->i_iattr.i_size;
}

static inline fx_mode_t inode_getmode(const fx_inode_t *inode)
{
	return inode->i_iattr.i_mode;
}

static inline fx_off_t inode_getbsize(const fx_inode_t *inode)
{
	return (fx_off_t)(inode->i_iattr.i_bcap * FNX_BLKSIZE);
}

static inline void inode_setsize(fx_inode_t *inode, fx_off_t size)
{
	inode->i_iattr.i_size = size;
}

static inline void inode_setbsize(fx_inode_t *inode, fx_off_t size)
{
	inode->i_iattr.i_bcap = fx_bytes_to_nbk((fx_size_t)size);
}

static inline fx_nlink_t inode_getnlink(const fx_inode_t *inode)
{
	return inode->i_iattr.i_nlink;
}

static inline int inode_ispseudo(const fx_inode_t *inode)
{
	return vnode_ispseudo(&inode->i_vnode);
}

static inline int inode_isdir(const fx_inode_t *inode)
{
	return (inode_vtype(inode) == FNX_VTYPE_DIR);
}

static inline int inode_isreg(const fx_inode_t *inode)
{
	return (inode_vtype(inode) == FNX_VTYPE_REG);
}

static inline int inode_issymlnk(const fx_inode_t *inode)
{
	const fx_vtype_e vtype = inode_vtype(inode);
	return ((vtype == FNX_VTYPE_SSYMLNK) || (vtype == FNX_VTYPE_SYMLNK));
}

static inline int inode_isreflnk(const fx_inode_t *inode)
{
	return (inode_vtype(inode) == FNX_VTYPE_REFLNK);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static inline int inode_testf(const fx_inode_t *inode, fx_flags_t mask)
{
	return fx_testlf(inode->i_flags, mask);
}

static inline void inode_setf(fx_inode_t *inode, fx_flags_t mask)
{
	fx_setlf(&inode->i_flags, mask);
}

static inline void inode_unsetf(fx_inode_t *inode, fx_flags_t mask)
{
	fx_unsetlf(&inode->i_flags, mask);
}

#endif /* FUNEX_INODEI_H_ */




/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_VIO_H_
#define FUNEX_VIO_H_


/* Flags */
#define FX_VIOF_SAVEPATH     FX_BITFLAG(1)
#define FX_VIOF_RDWR         FX_BITFLAG(2)
#define FX_VIOF_RDONLY       FX_BITFLAG(3)
#define FX_VIOF_LOCK         FX_BITFLAG(4)
#define FX_VIOF_SYNC         FX_BITFLAG(5)


struct fx_vio;
typedef struct fx_vio  fx_vio_t;


/*
 * Volume-I/O proxy over reg-file or raw-volume device. Operates via file
 * descriptor, using LBA + count addressing.
 *
 * May have mmap-ing of the whole volume address-space.
 */
struct fx_vio {
	/* Members */
	int         fd;             /* File-descriptor */
	int         flags;          /* Attribute flags */
	fx_size_t   lbsz;           /* Logical-block size */
	fx_bkcnt_t  base;           /* Base blocks-offset */
	fx_bkcnt_t  bcap;           /* Capacity in blocks */
	void       *maddr;          /* Memory mapped region */
	char       *path;           /* Pathname (optional) */

	/* Hooks */
	int (*create)(fx_vio_t *, const char *, fx_bkcnt_t, fx_bkcnt_t, int);
	int (*open)(fx_vio_t *, const char *, fx_bkcnt_t, fx_bkcnt_t, int);
	int (*close)(fx_vio_t *);
	int (*read)(const fx_vio_t *, void *, fx_lba_t, fx_bkcnt_t);
	int (*write)(const fx_vio_t *, const void *, fx_lba_t, fx_bkcnt_t);
	int (*sync)(const fx_vio_t *);
	int (*isopen)(const fx_vio_t *);
	int (*getcap)(const fx_vio_t *, fx_bkcnt_t *);
	int (*off2lba)(const fx_vio_t *, fx_off_t, fx_lba_t *);

	int (*flock)(fx_vio_t *);
	int (*tryflock)(fx_vio_t *);
	int (*funlock)(fx_vio_t *);

	int (*mmap)(fx_vio_t *);
	int (*munmap)(fx_vio_t *);
	int (*msync)(const fx_vio_t *, fx_lba_t, fx_bkcnt_t, int now);
	int (*mflush)(const fx_vio_t *, int now);
	int (*mgetp)(const fx_vio_t *, fx_lba_t, void **);
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_vio_init(fx_vio_t *);

void fx_vio_destroy(fx_vio_t *);


#endif /* FUNEX_VIO_H_ */


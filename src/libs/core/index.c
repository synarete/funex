/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

#include "balagan.h"
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "metai.h"
#include "addr.h"
#include "htod.h"
#include "elems.h"
#include "iobuf.h"
#include "index.h"

/* Local functions forward declarations */
static size_t dsec_resolve_frgi(const fx_dsec_t *, const fx_dfrg_t *);
static fx_header_t *dsec_get_frgi(const fx_dsec_t *, fx_size_t);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Calculates allocation sizes in frg-units per vtype */
static fx_size_t vtype_to_nfrg(fx_vtype_e vtype)
{
	size_t nfrg, size;

	size = fx_metasize(vtype);
	nfrg = fx_bytes_to_nfrg(size);
	return nfrg;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_size_t nfrg_to_bytes(fx_bkcnt_t nfrg)
{
	return nfrg * FNX_FRGSIZE;
}

void fx_dobj_zpad(fx_header_t *hdr, fx_vtype_e vtype)
{
	size_t hsz, bsz, nfrg;
	char *ptr;

	nfrg    = vtype_to_nfrg(vtype);
	hsz     = sizeof(*hdr);
	bsz     = nfrg_to_bytes(nfrg);
	if (bsz > hsz) {
		ptr = ((char *)hdr) + hsz;
		fx_bzero(ptr, bsz - hsz);
	}
}

void fx_dobj_assign(fx_header_t *hdr, fx_vtype_e vtype,
                    fx_ino_t ino, fx_ino_t xno, fx_size_t nfrg)
{
	fx_assert(nfrg > 0);
	fx_assert(nfrg <= (FNX_SECSIZE / FNX_FRGSIZE));

	hdr->h_mark     = (uint8_t)FNX_HDRMARK;
	hdr->h_vers     = (uint8_t)FNX_HDRVERS;
	hdr->h_vtype    = fx_htod_vtype(vtype);
	hdr->h_len      = (uint8_t)nfrg;
	hdr->h_ino      = fx_htod_ino(ino);
	hdr->h_xno      = fx_htod_ino(xno);
	hdr->h_resrvd0  = 0;
	hdr->h_resrvd1  = 0;
}

static size_t header_length(const fx_header_t *hdr)
{
	return hdr->h_len; /* In fragments units */
}

static void header_vreset(fx_header_t *hdr, fx_vtype_e vtype, size_t nfrg)
{
	fx_assert(nfrg <= FNX_SECNFRG);
	fx_assert(nfrg > 0);
	fx_dobj_assign(hdr, vtype, FNX_INO_NULL, FNX_INO_NULL, nfrg);
}

void fx_dobj_clear(fx_header_t *hdr, size_t bytes_len)
{
	const size_t nfrg = fx_bytes_to_nfrg(bytes_len);
	header_vreset(hdr, FNX_VTYPE_EMPTY, nfrg);
}

static size_t header_length_bytes(const fx_header_t *hdr)
{
	return nfrg_to_bytes(header_length(hdr));
}

fx_size_t fx_dobj_getlen(const fx_header_t *hdr)
{
	return header_length(hdr);
}

fx_vtype_e fx_dobj_vtype(const fx_header_t *hdr)
{
	return fx_dtoh_vtype(hdr->h_vtype);
}

fx_ino_t fx_dobj_getino(const fx_header_t *hdr)
{
	return fx_dtoh_ino(hdr->h_ino);
}

fx_ino_t fx_dobj_getxno(const fx_header_t *hdr)
{
	return fx_dtoh_ino(hdr->h_xno);
}

/*
 * On-disk checksum using fnv hash;
 *
 * Preferred over crc32c for speed and over adler32/fletcher32 for (allegedly)
 * low collision.
 */
static uint32_t calccsum(const void *buf, size_t len)
{
	uint32_t csum = 0;

	blgn_fnv32a(buf, len, &csum);
	return csum;
}

static size_t metalen(const fx_header_t *hdr, const uint32_t *csum)
{
	ptrdiff_t dif;
	const char *ptr1;
	const char *ptr2;

	ptr1  = (const char *)hdr;
	ptr2  = (const char *)csum;
	dif   = ptr2 - ptr1;
	return (size_t)dif;
}

static void dobj_signsum(const fx_header_t *hdr, uint32_t *csum)
{
	size_t len;
	uint32_t crc;

	len = metalen(hdr, csum);
	crc = calccsum(hdr, len);

	*csum = fx_htod_u32(crc);
}

static int dobj_checksum(const fx_header_t *hdr, const uint32_t *csum)
{
	size_t len;
	uint32_t crc1, crc2;

	len  = metalen(hdr, csum);
	crc1 = calccsum(hdr, len);
	crc2 = fx_htod_u32(*csum);
	return (!crc1 || (crc1 == crc2)) ? 0 : -1;
}

int fx_dobj_check(const fx_header_t *hdr, fx_vtype_e vtype)
{
	size_t dobj_size, bytes_size;
	fx_vtype_e h_vtype;

	if (hdr->h_mark != FNX_HDRMARK) {
		fx_warn("bad-hdr-mark: h_mark=%d expected=%d",
		        (int)hdr->h_mark, FNX_HDRMARK);
		return -1;
	}
	if (hdr->h_vers != FNX_HDRVERS) {
		fx_warn("bad-hdr-version: h_vers=%d expected=%d",
		        (int)hdr->h_vers, FNX_HDRVERS);
		return -1;
	}
	h_vtype = fx_dobj_vtype(hdr);
	if ((vtype != FNX_VTYPE_ANY) && (vtype != h_vtype)) {
		fx_warn("bad-hdr-vtype: h_vtype=%d expected=%d",
		        (int)h_vtype, vtype);
		return -1;
	}
	dobj_size  = fx_metasize(h_vtype);
	bytes_size = header_length_bytes(hdr);
	if (bytes_size != dobj_size) {
		fx_warn("bad-hdr-size: h_len=%d expected=%d",
		        (int)hdr->h_len, (int)(dobj_size / FNX_FRGSIZE));
		return -1;
	}
	return 0;
}

int fx_dobj_icheck(const fx_header_t *hdr)
{
	int rc;
	fx_vtype_e hdr_vtype;

	rc = fx_dobj_check(hdr, FNX_VTYPE_ANY);
	if (rc != 0) {
		return rc;
	}
	hdr_vtype = fx_dobj_vtype(hdr);
	if (!fx_isitype(hdr_vtype)) {
		return -1;
	}
	return 0;
}

static int header_has_vaddr(const fx_header_t *hdr, const fx_vaddr_t *vaddr)
{
	fx_vtype_e h_vtype;
	fx_ino_t h_ino;
	fx_xno_t h_xno;

	h_vtype = fx_dobj_vtype(hdr);
	h_ino   = fx_dobj_getino(hdr);
	h_xno   = fx_dobj_getxno(hdr);

	return ((vaddr->vtype == h_vtype) &&
	        (vaddr->ino == h_ino) &&
	        (vaddr->xno == h_xno));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_dsec_t *fx_get_dsec(const fx_bkref_t *sec)
{
	fx_dsec_t *dsec = NULL;

	fx_assert(sec->vtype == FNX_VTYPE_SEC);

	if (sec->vtype == FNX_VTYPE_SEC) {
		dsec = (fx_dsec_t *)(sec->blk);
	}
	return dsec;
}

fx_dfrg_t *fx_get_dfrg(const fx_bkref_t *sec, const fx_bkaddr_t *addr)
{
	fx_off_t frg, lim;
	const fx_dsec_t *dsec;
	const fx_dfrg_t *dfrg = NULL;

	dsec = fx_get_dsec(sec);
	if (dsec == NULL) {
		return NULL;
	}
	frg = fx_bkaddr_frgdist(&sec->addr, addr);
	lim = (fx_off_t)FX_NELEMS(dsec->sec_frgs);
	if ((frg >= 0) && (frg < lim)) {
		dfrg = &dsec->sec_frgs[frg];
	}
	return (fx_dfrg_t *)dfrg;
}

fx_header_t *fx_get_dobj(const fx_bkref_t *sec, const fx_bkaddr_t *addr)
{
	const fx_dfrg_t *dfrg;
	const fx_header_t *hdr = NULL;

	dfrg = fx_get_dfrg(sec, addr);
	if (dfrg != NULL) {
		hdr = &dfrg->fr_hdr;
	}
	return (fx_header_t *)hdr;
}

void fx_dsec_reset(fx_dsec_t *dsec)
{
	fx_dobj_clear(&dsec->sec_hdr, FNX_SECSIZE);
}

/* Returns the offset of fragment within its container section */
fx_off_t fx_dsec_frgoff(const fx_dsec_t *dsec, const fx_dfrg_t *dfrg)
{
	ptrdiff_t dif;
	const fx_dfrg_t *dfrg0;

	dfrg0 = &dsec->sec_frgs[0];
	dif   = dfrg - dfrg0;
	fx_assert(dif >= 0);
	fx_assert((size_t)dif < FX_NELEMS(dsec->sec_frgs));
	return (fx_off_t)dif;
}

/* Returns offset of sub-element with its container section (fragment units) */
static size_t dsec_resolve_frgi(const fx_dsec_t *dsec, const fx_dfrg_t *dfrg)
{
	ptrdiff_t dif;

	dif   = dfrg - dsec->sec_frgs;
	fx_assert(dif >= 0);
	fx_assert((size_t)dif <= FX_NELEMS(dsec->sec_frgs));
	return (size_t)dif;
}

/* Converts sub-element offset within section to object */
static fx_header_t *dsec_get_frgi(const fx_dsec_t *dsec, fx_size_t frgi)
{
	const fx_header_t  *hdr;

	hdr = &(dsec->sec_frgs[frgi].fr_hdr);
	return (fx_header_t *)hdr;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Find an existing allocated inode within section using ino as key:
 * Case 1: Normal: inode is fragment-aligned.
 * Case 2: Special: inode is on pre-defined block-offset within section.
 */
static const fx_header_t *
dsec_search_normal_inode(const fx_dsec_t *dsec, fx_ino_t ino)
{
	fx_vtype_e vtype;
	fx_ino_t h_ino, h_xno;
	size_t frgi, nfrg, len;
	const fx_header_t *hdr = NULL;

	nfrg = FX_NELEMS(dsec->sec_frgs);
	frgi = 0;
	while (frgi < nfrg) {
		hdr = dsec_get_frgi(dsec, frgi);
		len = header_length(hdr);
		vtype = fx_dobj_vtype(hdr);
		fx_assert(len > 0);

		if (fx_isitype(vtype)) {
			h_ino = fx_dobj_getino(hdr);
			h_xno = fx_dobj_getxno(hdr);
			if ((h_ino == ino) && (h_xno == FNX_XNO_NULL)) {
				break;
			}
		}
		frgi += len;
		hdr  = NULL;
	}
	return hdr;
}

static const fx_header_t *
dsec_search_special_inode(const fx_dsec_t *dsec, fx_ino_t ino)
{
	const fx_dfrg_t *dfrg;
	const fx_header_t *hdr = NULL;

	if (ino == FNX_INO_ROOT) {
		dfrg = &dsec->sec_frgs[FNX_FRG_ROOT];
		hdr  = &dfrg->fr_hdr;
	}
	return hdr;
}

fx_dfrg_t *fx_dsec_search_inode(const fx_dsec_t *dsec, fx_ino_t ino)
{
	fx_dfrg_t *dfrg = NULL;
	const fx_header_t *hdr;

	hdr = dsec_search_special_inode(dsec, ino);
	if (hdr == NULL) {
		hdr = dsec_search_normal_inode(dsec, ino);
	}

	if (hdr != NULL) {
		dfrg = header_to_dfrg(hdr);
	}
	return dfrg;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const fx_header_t *
dsec_search_special_vnode(const fx_dsec_t *dsec, const fx_vaddr_t *vaddr)
{
	const fx_dfrg_t *dfrg;
	const fx_header_t *hdr = NULL;

	if (vaddr->vtype == FNX_VTYPE_SUPER) {
		dfrg = &dsec->sec_frgs[FNX_FRG_SUPER];
		hdr  = &dfrg->fr_hdr;
	} else if ((vaddr->vtype == FNX_VTYPE_DIR) &&
	           (vaddr->ino == FNX_INO_ROOT) &&
	           (vaddr->xno == FNX_XNO_NULL)) {
		dfrg = &dsec->sec_frgs[FNX_FRG_ROOT];
		hdr  = &dfrg->fr_hdr;
	}
	if (hdr && !header_has_vaddr(hdr, vaddr)) {
		hdr = NULL;
	}
	return hdr;
}

static const fx_header_t *
dsec_search_normal_vnode(const fx_dsec_t *dsec, const fx_vaddr_t *vaddr)
{
	size_t frgi, nfrg, len;
	const fx_header_t *hdr;

	nfrg = FX_NELEMS(dsec->sec_frgs);
	frgi = 0;
	while (frgi < nfrg) {
		hdr = dsec_get_frgi(dsec, frgi);
		len = header_length(hdr);
		fx_assert(len > 0);

		if (header_has_vaddr(hdr, vaddr)) {
			return hdr;
		}
		frgi += len;
	}
	return NULL;
}

fx_dfrg_t *fx_dsec_search_vnode(const fx_dsec_t *dsec, const fx_vaddr_t *vaddr)
{
	fx_dfrg_t *dfrg = NULL;
	const fx_header_t *hdr;

	hdr = dsec_search_special_vnode(dsec, vaddr);
	if (hdr == NULL) {
		hdr = dsec_search_normal_vnode(dsec, vaddr);
	}
	if (hdr != NULL) {
		dfrg = header_to_dfrg(hdr);
	}
	return dfrg;
}


/* Generic search method of free fragments-range within on-disk section */
static const fx_header_t *
dsec_find_empty(const fx_dsec_t *dsec, fx_vtype_e vtype)
{
	fx_vtype_e frg_vtype;
	size_t frgi, nfrg, nelems, len;
	const fx_header_t *hdr = NULL;

	nfrg   = vtype_to_nfrg(vtype);
	nelems = FX_NELEMS(dsec->sec_frgs);
	frgi   = 0;
	while (frgi < nelems) {
		hdr = dsec_get_frgi(dsec, frgi);
		len = header_length(hdr);
		frg_vtype = fx_dobj_vtype(hdr);

		fx_assert(len > 0);

		if ((frg_vtype == FNX_VTYPE_EMPTY) && (len >= nfrg)) {
			break; /* Bingo! */
		}
		frgi += len;
		hdr  = NULL;
	}
	return (fx_header_t *)hdr;
}

/* Special search for long-symlnk: must no be at the end (no checksum) */
static const fx_header_t *
dsec_find_empty_symlnk(const fx_dsec_t *dsec)
{
	fx_vtype_e frg_vtype;
	size_t frgi, nfrg, nelems, len;
	const fx_header_t *hdr = NULL;

	nfrg   = vtype_to_nfrg(FNX_VTYPE_SYMLNK);
	nelems = FX_NELEMS(dsec->sec_frgs) - nfrg - 1;
	frgi   = 0;
	while (frgi < nelems) {
		hdr = dsec_get_frgi(dsec, frgi);
		len = header_length(hdr);
		frg_vtype = fx_dobj_vtype(hdr);

		fx_assert(len > 0);

		if ((frg_vtype == FNX_VTYPE_EMPTY) && (len >= nfrg)) {
			break; /* Bingo! */
		}
		frgi += len;
		hdr  = NULL;
	}
	return (fx_header_t *)hdr;
}

static void
dsec_chop_frgs(fx_dsec_t *dsec, fx_vtype_e vtype,
               size_t frg_idx, size_t frg_cnt)
{
	size_t nfrg_use, nfrg_rem;
	fx_header_t *hdr_begin, *hdr_end;

	hdr_begin   = dsec_get_frgi(dsec, frg_idx);
	hdr_end     = dsec_get_frgi(dsec, frg_idx + frg_cnt);

	nfrg_use    = header_length(hdr_begin);
	nfrg_rem    = nfrg_use - frg_cnt;
	fx_assert(frg_cnt <= nfrg_use);
	fx_assert(frg_cnt > 0);

	if (nfrg_rem > 0) {
		header_vreset(hdr_end, FNX_VTYPE_EMPTY, nfrg_rem);
	}
	header_vreset(hdr_begin, vtype, frg_cnt);
}

static size_t
dsec_chop_dobj(fx_dsec_t *dsec, fx_header_t *hdr, fx_vtype_e vtype)
{
	size_t frg_idx, frg_cnt;

	frg_idx = dsec_resolve_frgi(dsec, header_to_dfrg(hdr));
	frg_cnt = vtype_to_nfrg(vtype);
	fx_assert(frg_cnt > 0);
	fx_assert((frg_idx + frg_cnt) <= (FNX_SECSIZE / FNX_FRGSIZE));

	dsec_chop_frgs(dsec, vtype, frg_idx, frg_cnt);
	return frg_cnt;
}

void fx_dsec_chopat(fx_dsec_t *dsec, fx_dfrg_t *dfrg, fx_vtype_e vtype)
{
	size_t frg_cnt;
	fx_header_t *hdr = &dfrg->fr_hdr;

	fx_assert(dfrg >= &dsec->sec_frgs[0]);
	frg_cnt = dsec_chop_dobj(dsec, hdr, vtype);
	fx_assert(frg_cnt > 0);
}

fx_dfrg_t *fx_dsec_search_free(fx_dsec_t *dsec, fx_vtype_e vtype)
{
	fx_dfrg_t *dfrg = NULL;
	const fx_header_t *hdr;

	if (vtype == FNX_VTYPE_SYMLNK) {
		hdr = dsec_find_empty_symlnk(dsec);
	} else {
		hdr = dsec_find_empty(dsec, vtype);
	}
	if (hdr != NULL) {
		dfrg = header_to_dfrg(hdr);
	}
	return dfrg;
}



/* Merge-back a fragments-sequence within section into free chunk */
static fx_header_t *
dsec_get_prev_frg(const fx_dsec_t *dsec, size_t frg_idx)
{
	size_t frgi, len;
	const fx_header_t *hdr;

	frgi = 0;
	while (frgi < frg_idx) {
		hdr = dsec_get_frgi(dsec, frgi);
		len = header_length(hdr);
		if ((frgi + len) == frg_idx) {
			return (fx_header_t *)hdr;
		}
		frgi += len;
	}
	return NULL;
}

static fx_header_t *
dsec_get_post_frg(const fx_dsec_t *dsec, size_t frg_idx)
{
	size_t frgi, nfrg, len;
	const fx_header_t *hdr;

	frgi = frg_idx;
	nfrg = FX_NELEMS(dsec->sec_frgs);
	if (frgi < nfrg) {
		hdr = dsec_get_frgi(dsec, frgi);
		len = header_length(hdr);
		if ((frgi + len) < nfrg) {
			hdr = dsec_get_frgi(dsec, frgi + len);
			return (fx_header_t *)hdr;
		}
	}
	return NULL;
}

static fx_header_t *
dsec_join_frgs(fx_dsec_t *dsec, size_t frg_idx)
{
	fx_vtype_e vtype;
	size_t frg_cnt;
	fx_header_t *hdr_curr, *hdr_prev, *hdr_post;

	hdr_curr = dsec_get_frgi(dsec, frg_idx);
	frg_cnt = header_length(hdr_curr);

	hdr_prev = dsec_get_prev_frg(dsec, frg_idx);
	if (hdr_prev != NULL) {
		vtype = fx_dobj_vtype(hdr_prev);
		if (vtype == FNX_VTYPE_EMPTY) {
			frg_cnt += header_length(hdr_prev);
			hdr_curr = hdr_prev;
		}
	}

	hdr_post = dsec_get_post_frg(dsec, frg_idx);
	if (hdr_post != NULL) {
		vtype = fx_dobj_vtype(hdr_post);
		if (vtype == FNX_VTYPE_EMPTY) {
			frg_cnt += header_length(hdr_post);
		}
	}

	vtype = FNX_VTYPE_EMPTY;
	header_vreset(hdr_curr, vtype, frg_cnt);

	return hdr_curr;
}

static void dsec_rejoin(fx_dsec_t *dsec, fx_header_t *hdr, size_t len)
{
	size_t frg_idx, frg_cnt, frg_cnt2;

	frg_idx = dsec_resolve_frgi(dsec, header_to_dfrg(hdr));
	frg_cnt = header_length(hdr);

	fx_assert(frg_idx < FX_NELEMS(dsec->sec_frgs));
	fx_assert(frg_cnt <= FX_NELEMS(dsec->sec_frgs));
	fx_assert(frg_cnt > 0);
	fx_assert(len == frg_cnt);

	hdr = dsec_join_frgs(dsec, frg_idx);
	frg_cnt2 = header_length(hdr);
	fx_assert(frg_cnt2 >= frg_cnt);
}

void fx_dsec_joinat(fx_dsec_t *dsec, fx_dfrg_t *dfrg, fx_vtype_e vtype)
{
	size_t len;
	fx_header_t *hdr = &dfrg->fr_hdr;

	len = fx_dobjnfrg(vtype);
	fx_assert(fx_dobj_vtype(hdr) == vtype);
	fx_assert(header_length(hdr) == len);

	dsec_rejoin(dsec, hdr, len);
}


void fx_dsec_signsum(fx_dsec_t *dsec)
{
	fx_dsecbuf_t *secbuf = &dsec->sec_buf;
	dobj_signsum(&secbuf->sec_hdr, &secbuf->sec_checksum);
}

int fx_dsec_checksum(const fx_dsec_t *dsec)
{
	const fx_dsecbuf_t *secbuf = &dsec->sec_buf;
	return dobj_checksum(&secbuf->sec_hdr, &secbuf->sec_checksum);
}



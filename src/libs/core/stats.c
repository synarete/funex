/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "proto.h"
#include "htod.h"
#include "addr.h"
#include "addri.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void extent_init(fx_extent_t *extent)
{
	extent->lba = FNX_LBA_NULL;
	extent->cnt = 0;
}

static void extent_destroy(fx_extent_t *extent)
{
	extent->lba = FNX_LBA_MAX;
	extent->cnt = 0;
}

static void extent_htod(const fx_extent_t *extent, fx_dextent_t *dextent)
{
	dextent->lba = fx_htod_lba(extent->lba);
	dextent->cnt = fx_htod_bkcnt(extent->cnt);
}

static void extent_dtoh(fx_extent_t *extent, const fx_dextent_t *dextent)
{
	extent->lba = fx_dtoh_lba(dextent->lba);
	extent->cnt = fx_dtoh_bkcnt(dextent->cnt);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_layout_init(fx_layout_t *layout)
{
	extent_init(&layout->l_super);
	extent_init(&layout->l_index);
	extent_init(&layout->l_extra);
	extent_init(&layout->l_udata);
	extent_init(&layout->l_trail);
}

void fx_layout_destroy(fx_layout_t *layout)
{
	extent_destroy(&layout->l_super);
	extent_destroy(&layout->l_index);
	extent_destroy(&layout->l_extra);
	extent_destroy(&layout->l_udata);
	extent_destroy(&layout->l_trail);
}

void fx_layout_htod(const fx_layout_t *layout, fx_dlayout_t *dlayout)
{
	extent_htod(&layout->l_super, &dlayout->l_super);
	extent_htod(&layout->l_index, &dlayout->l_index);
	extent_htod(&layout->l_extra, &dlayout->l_extra);
	extent_htod(&layout->l_udata, &dlayout->l_udata);
	extent_htod(&layout->l_trail, &dlayout->l_trail);
}

void fx_layout_dtoh(fx_layout_t *layout, const fx_dlayout_t *dlayout)
{
	extent_dtoh(&layout->l_super, &dlayout->l_super);
	extent_dtoh(&layout->l_index, &dlayout->l_index);
	extent_dtoh(&layout->l_extra, &dlayout->l_extra);
	extent_dtoh(&layout->l_udata, &dlayout->l_udata);
	extent_dtoh(&layout->l_trail, &dlayout->l_trail);
}

static fx_bkcnt_t round_to_secnbk(fx_bkcnt_t n)
{
	return ((n / FNX_SECNBK) * FNX_SECNBK);
}

static fx_bkcnt_t round_to_spcnbk(fx_bkcnt_t n)
{
	return ((n / FNX_SPCNBK) * FNX_SPCNBK);
}

/*
 * Constructs data/meta-data blocks-layout for volume of nbk blocks.
 */
void fx_layout_devise(fx_layout_t *layout, fx_bkcnt_t nbk)
{
	fx_bkcnt_t sup, idx, ext, ubk, mbk, sbk, rem;

	/* TODO: Check valid */

	/* Total number of available blocks, aligned to section */
	nbk = round_to_secnbk(nbk);

	/* Number of meta-blocks needed for space-mapping of the entire volume */
	sbk = nbk / FNX_SPCNBK;

	/* Number of blocks allocated for users data */
	ubk = round_to_spcnbk((89 * (nbk - sbk)) / 100); /* XXX */

	/* Number of blocks allocated for meta-data */
	mbk = round_to_secnbk(nbk - ubk);

	/* Derived layout */
	sup = FNX_SECNBK;
	rem = mbk - sup;
	idx = round_to_secnbk(rem);
	ext = mbk - sup - idx;

	layout->l_super.lba    = 0;
	layout->l_super.cnt    = sup;
	layout->l_index.lba    = sup;
	layout->l_index.cnt    = idx;
	layout->l_extra.lba    = sup + idx;
	layout->l_extra.cnt    = ext;
	layout->l_udata.lba    = sup + idx + ext;
	layout->l_udata.cnt    = ubk;
	layout->l_trail.lba    = sup + idx + ext + ubk;
	layout->l_trail.cnt    = 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_iattr_init(fx_iattr_t *iattr)
{
	fx_bzero(iattr, sizeof(*iattr));
	fx_times_init(&iattr->i_times);
	iattr->i_gen        = 1;
	iattr->i_nlink      = 0;
	iattr->i_uid        = (fx_uid_t)FNX_UID_NULL;
	iattr->i_gid        = (fx_gid_t)FNX_GID_NULL;
	iattr->i_mode       = 0;
	iattr->i_size       = 0;
	iattr->i_bcap       = 0;
}

void fx_iattr_destroy(fx_iattr_t *iattr)
{
	fx_bff(iattr, sizeof(*iattr));
	iattr->i_uid        = (fx_uid_t)(FNX_UID_NULL);
	iattr->i_gid        = (fx_gid_t)(FNX_GID_NULL);
	iattr->i_mode       = 0;
}

void fx_iattr_copy(fx_iattr_t *iattr, const fx_iattr_t *other)
{
	memcpy(iattr, other, sizeof(*iattr));
}

void fx_iattr_htod(const fx_iattr_t *iattr, fx_diattr_t *diattr)
{
	/* R[0] */
	diattr->i_mode      = fx_htod_mode(iattr->i_mode);
	diattr->i_nlink     = fx_htod_nlink(iattr->i_nlink);
	diattr->i_uid       = fx_htod_uid(iattr->i_uid);
	diattr->i_gid       = fx_htod_gid(iattr->i_gid);
	diattr->i_rdev      = fx_htod_dev(iattr->i_rdev);
	diattr->i_size      = fx_htod_off(iattr->i_size);
	diattr->i_bcap      = fx_htod_bkcnt(iattr->i_bcap);
	diattr->i_nblk      = fx_htod_bkcnt(iattr->i_nblk);
	diattr->i_wops      = fx_htod_size(iattr->i_wops);
	diattr->i_wcnt      = fx_htod_size(iattr->i_wcnt);

	/* R[1] */
	fx_times_htod(&iattr->i_times, &diattr->i_times);
}

void fx_iattr_dtoh(fx_iattr_t *iattr, const fx_diattr_t *diattr)
{
	/* R[0] */
	iattr->i_mode       = fx_dtoh_mode(diattr->i_mode);
	iattr->i_nlink      = fx_dtoh_nlink(diattr->i_nlink);
	iattr->i_uid        = fx_dtoh_uid(diattr->i_uid);
	iattr->i_gid        = fx_dtoh_gid(diattr->i_gid);
	iattr->i_rdev       = fx_dtoh_dev(diattr->i_rdev);
	iattr->i_size       = fx_dtoh_off(diattr->i_size);
	iattr->i_bcap       = fx_dtoh_bkcnt(diattr->i_bcap);
	iattr->i_nblk       = fx_dtoh_bkcnt(diattr->i_nblk);
	iattr->i_wops       = fx_dtoh_size(diattr->i_wops);
	iattr->i_wcnt       = fx_dtoh_size(diattr->i_wcnt);

	/* R[1] */
	fx_times_dtoh(&iattr->i_times, &diattr->i_times);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fsattr_init(fx_fsattr_t *fsattr)
{
	fx_bzero(fsattr, sizeof(*fsattr));
	fsattr->f_fstype    = FNX_FSMAGIC;
	fsattr->f_version   = FNX_FSVERSION;
	fsattr->f_mntf      = FNX_MNTF_DEFAULT;
	fsattr->f_gen       = 0;
	fsattr->f_zid       = 0;
	fsattr->f_uid       = (fx_uid_t)(FNX_UID_NULL);
	fsattr->f_gid       = (fx_gid_t)(FNX_GID_NULL);
	fsattr->f_rootino   = FNX_INO_ROOT;
}

void fx_fsattr_setup(fx_fsattr_t *fsattr,
                     const fx_uuid_t *uuid, fx_uid_t uid, fx_gid_t gid)
{
	fx_uuid_copy(&fsattr->f_fsuuid, uuid);
	fsattr->f_uid       = uid;
	fsattr->f_gid       = gid;
	fsattr->f_rootino   = FNX_INO_ROOT;
}

void fx_fsattr_htod(const fx_fsattr_t *fsattr, fx_dfsattr_t *dfsattr)
{
	/* R[0] */
	fx_htod_uuid(dfsattr->f_uuid, &fsattr->f_fsuuid);
	dfsattr->f_fstype   = fx_htod_u32(fsattr->f_fstype);
	dfsattr->f_version  = fx_htod_versnum(fsattr->f_version);
	dfsattr->f_gen      = fx_htod_u32((uint32_t)fsattr->f_gen);
	dfsattr->f_uid      = fx_htod_uid(fsattr->f_uid);
	dfsattr->f_gid      = fx_htod_gid(fsattr->f_gid);
	dfsattr->f_rootino  = fx_htod_ino(fsattr->f_rootino);
}

void fx_fsattr_dtoh(fx_fsattr_t *fsattr, const fx_dfsattr_t *dfsattr)
{
	/* R[0] */
	fx_dtoh_uuid(&fsattr->f_fsuuid, dfsattr->f_uuid);
	fsattr->f_fstype    = fx_dtoh_u32(dfsattr->f_fstype);
	fsattr->f_version   = fx_dtoh_versnum(dfsattr->f_version);
	fsattr->f_gen       = fx_dtoh_u32(dfsattr->f_gen);
	fsattr->f_uid       = fx_dtoh_uid(dfsattr->f_uid);
	fsattr->f_gid       = fx_dtoh_gid(dfsattr->f_gid);
	fsattr->f_rootino   = fx_dtoh_ino(dfsattr->f_rootino);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fsstat_init(fx_fsstat_t *fsstat)
{
	fsstat->f_blocks    = 0;
	fsstat->f_bfree     = 0;
	fsstat->f_bavail    = 0;
	fsstat->f_bnext     = FNX_SECNBK;
	fsstat->f_inodes    = 0;
	fsstat->f_ifree     = 0;
	fsstat->f_iavail    = 0;
	fsstat->f_iapex     = FNX_INO_MAX;
}

static unsigned long unprivileged(unsigned long privileged)
{
	return (privileged * 17) / 19;
}

static unsigned long inodes_per_block(void)
{
	return sizeof(fx_dblk_t) / sizeof(fx_dinode_t);
}

void fx_fsstat_devise(fx_fsstat_t *fsstat, fx_bkcnt_t index, fx_bkcnt_t udata)
{
	const fx_filcnt_t inodes = (index * inodes_per_block()) / 2;

	fsstat->f_inodes    = inodes;
	fsstat->f_ifree     = inodes;
	fsstat->f_iavail    = unprivileged(inodes);
	fsstat->f_iapex     = FNX_INO_APEX0;

	fsstat->f_blocks    = udata;
	fsstat->f_bfree     = udata;
	fsstat->f_bavail    = unprivileged(udata);
	fsstat->f_bnext     = FNX_LBA_NULL;

	fsstat->f_pblocks   = 0;
	fsstat->f_pfree     = 0;
	fsstat->f_pavail    = 0;
	fsstat->f_pnext     = FNX_LBA_PSTART;
}

void fx_fsstat_htod(const fx_fsstat_t *fsstat, fx_dfsstat_t *dfsstat)
{
	/* R[0] */
	dfsstat->f_inodes   = fx_htod_filcnt(fsstat->f_inodes);
	dfsstat->f_ifree    = fx_htod_filcnt(fsstat->f_ifree);
	dfsstat->f_iavail   = fx_htod_filcnt(fsstat->f_iavail);
	dfsstat->f_iapex    = fx_htod_ino(fsstat->f_iapex);

	/* R[1] */
	dfsstat->f_blocks   = fx_htod_bkcnt(fsstat->f_blocks);
	dfsstat->f_bfree    = fx_htod_bkcnt(fsstat->f_bfree);
	dfsstat->f_bavail   = fx_htod_bkcnt(fsstat->f_bavail);
	dfsstat->f_bnext    = fx_htod_lba(fsstat->f_bnext);

	dfsstat->f_pblocks  = fx_htod_bkcnt(fsstat->f_pblocks);
	dfsstat->f_pfree    = fx_htod_bkcnt(fsstat->f_pfree);
	dfsstat->f_pavail   = fx_htod_bkcnt(fsstat->f_pavail);
	dfsstat->f_pnext    = fx_htod_lba(fsstat->f_pnext);
}

void fx_fsstat_dtoh(fx_fsstat_t *fsstat, const fx_dfsstat_t *dfsstat)
{
	/* R[0] */
	fsstat->f_inodes    = fx_dtoh_filcnt(dfsstat->f_inodes);
	fsstat->f_ifree     = fx_dtoh_filcnt(dfsstat->f_ifree);
	fsstat->f_iavail    = fx_dtoh_filcnt(dfsstat->f_iavail);
	fsstat->f_iapex     = fx_dtoh_ino(dfsstat->f_iapex);

	/* R[1] */
	fsstat->f_blocks    = fx_dtoh_bkcnt(dfsstat->f_blocks);
	fsstat->f_bfree     = fx_dtoh_bkcnt(dfsstat->f_bfree);
	fsstat->f_bavail    = fx_dtoh_bkcnt(dfsstat->f_bavail);
	fsstat->f_bnext     = fx_dtoh_lba(dfsstat->f_bnext);

	fsstat->f_pblocks   = fx_dtoh_bkcnt(dfsstat->f_pblocks);
	fsstat->f_pfree     = fx_dtoh_bkcnt(dfsstat->f_pfree);
	fsstat->f_pavail    = fx_dtoh_bkcnt(dfsstat->f_pavail);
	fsstat->f_pnext     = fx_dtoh_lba(dfsstat->f_pnext);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_vstats_htod(const fx_vstats_t *vstat, fx_dvstats_t *dvstat)
{
	/* R[0] */
	dvstat->v_super   = fx_htod_size(vstat->v_super);
	dvstat->v_spmap   = fx_htod_size(vstat->v_spmap);
	dvstat->v_dir     = fx_htod_size(vstat->v_dir);
	dvstat->v_dirext  = fx_htod_size(vstat->v_dirext);
	dvstat->v_dirseg  = fx_htod_size(vstat->v_dirseg);
	dvstat->v_symlnk  = fx_htod_size(vstat->v_symlnk);
	dvstat->v_reflnk  = fx_htod_size(vstat->v_reflnk);
	dvstat->v_special = fx_htod_size(vstat->v_special);

	/* R[1] */
	dvstat->v_regfile = fx_htod_size(vstat->v_regfile);
	dvstat->v_slice   = fx_htod_size(vstat->v_slice);
	dvstat->v_segmnt  = fx_htod_size(vstat->v_segmnt);
	dvstat->v_magic   = fx_htod_magic(vstat->v_magic);
}

void fx_vstats_dtoh(fx_vstats_t *vstats, const fx_dvstats_t *dvstats)
{
	/* R[0] */
	vstats->v_super   = fx_dtoh_size(dvstats->v_super);
	vstats->v_spmap   = fx_dtoh_size(dvstats->v_spmap);
	vstats->v_dir     = fx_dtoh_size(dvstats->v_dir);
	vstats->v_dirext  = fx_dtoh_size(dvstats->v_dirext);
	vstats->v_dirseg  = fx_dtoh_size(dvstats->v_dirseg);
	vstats->v_symlnk  = fx_dtoh_size(dvstats->v_symlnk);
	vstats->v_reflnk  = fx_dtoh_size(dvstats->v_reflnk);
	vstats->v_special = fx_dtoh_size(dvstats->v_special);

	/* R[1] */
	vstats->v_regfile = fx_dtoh_bkcnt(dvstats->v_regfile);
	vstats->v_slice   = fx_dtoh_bkcnt(dvstats->v_slice);
	vstats->v_segmnt  = fx_dtoh_bkcnt(dvstats->v_segmnt);
	vstats->v_magic   = fx_dtoh_magic(dvstats->v_magic);
}

#define VSTAT_OFF(vtype) offsetof(fx_vstats_t, vtype)

static const size_t s_vtype_to_vstat_off[] = {
	[FNX_VTYPE_SUPER]   = VSTAT_OFF(v_super),
	[FNX_VTYPE_SPMAP]   = VSTAT_OFF(v_spmap),
	[FNX_VTYPE_CHRDEV]  = VSTAT_OFF(v_special),
	[FNX_VTYPE_BLKDEV]  = VSTAT_OFF(v_special),
	[FNX_VTYPE_SOCK]    = VSTAT_OFF(v_special),
	[FNX_VTYPE_FIFO]    = VSTAT_OFF(v_special),
	[FNX_VTYPE_SSYMLNK] = VSTAT_OFF(v_symlnk),
	[FNX_VTYPE_SYMLNK]  = VSTAT_OFF(v_symlnk),
	[FNX_VTYPE_REFLNK]  = VSTAT_OFF(v_reflnk),
	[FNX_VTYPE_DIR]     = VSTAT_OFF(v_dir),
	[FNX_VTYPE_DIREXT]  = VSTAT_OFF(v_dirext),
	[FNX_VTYPE_DIRSEG]  = VSTAT_OFF(v_dirseg),
	[FNX_VTYPE_REG]     = VSTAT_OFF(v_regfile),
	[FNX_VTYPE_SLICE]   = VSTAT_OFF(v_slice),
	[FNX_VTYPE_SEGMNT]  = VSTAT_OFF(v_segmnt)

};

fx_size_t *fx_vstats_getcnt(fx_vstats_t *vstats, int vtype)
{
	size_t nelems, offset = 0;
	void *ptr = NULL;

	nelems = FX_NELEMS(s_vtype_to_vstat_off);
	if ((vtype > 0) && (vtype < (int)nelems)) {
		offset = s_vtype_to_vstat_off[vtype];
		if (offset > 0) {
			ptr = (void *)((char *)vstats + offset);
		}
	}
	return (fx_size_t *)ptr;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* XXX TODO Rethink */
void fx_fill_icounts(fx_fsstat_t *fsstat, fx_size_t nsec)
{
	fx_size_t ipersec, icount;

	ipersec = sizeof(fx_dsec_t) / sizeof(fx_dinode_t);
	icount  = nsec * ipersec;
	fsstat->f_inodes  = icount;
	fsstat->f_ifree   = icount;
	fsstat->f_iavail  = (icount * 255) / 256; /* XXX good? */
}

/* Update blocks-accounting for extra volume of nbks */
void fx_extend_bkcounts(fx_fsstat_t *fsstat, fx_bkcnt_t nbks)
{
	fsstat->f_blocks += nbks;
	fsstat->f_bfree  += nbks;
	fsstat->f_bavail += (nbks * 1023) / 1024; /* XXX good? */
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#define OPSTAT_OFF(opc) offsetof(fx_opstat_t, opc)

static const size_t s_opc_to_opstat_off[] = {
	[FX_OP_LOOKUP]      = OPSTAT_OFF(op_lookup),
	[FX_OP_FORGET]      = OPSTAT_OFF(op_forget),
	[FX_OP_GETATTR]     = OPSTAT_OFF(op_getattr),
	[FX_OP_SETATTR]     = OPSTAT_OFF(op_setattr),
	[FX_OP_READLINK]    = OPSTAT_OFF(op_readlink),
	[FX_OP_SYMLINK]     = OPSTAT_OFF(op_symlink),
	[FX_OP_MKNOD]       = OPSTAT_OFF(op_mknod),
	[FX_OP_MKDIR]       = OPSTAT_OFF(op_mkdir),
	[FX_OP_UNLINK]      = OPSTAT_OFF(op_unlink),
	[FX_OP_RMDIR]       = OPSTAT_OFF(op_rmdir),
	[FX_OP_RENAME]      = OPSTAT_OFF(op_rename),
	[FX_OP_LINK]        = OPSTAT_OFF(op_link),
	[FX_OP_OPEN]        = OPSTAT_OFF(op_open),
	[FX_OP_READ]        = OPSTAT_OFF(op_read),
	[FX_OP_WRITE]       = OPSTAT_OFF(op_write),
	[FX_OP_STATFS]      = OPSTAT_OFF(op_statfs),
	[FX_OP_RELEASE]     = OPSTAT_OFF(op_release),
	[FX_OP_FSYNC]       = OPSTAT_OFF(op_fsync),
	[FX_OP_FLUSH]       = OPSTAT_OFF(op_flush),
	[FX_OP_OPENDIR]     = OPSTAT_OFF(op_opendir),
	[FX_OP_READDIR]     = OPSTAT_OFF(op_readdir),
	[FX_OP_RELEASEDIR]  = OPSTAT_OFF(op_releasedir),
	[FX_OP_FSYNCDIR]    = OPSTAT_OFF(op_fsyncdir),
	[FX_OP_ACCESS]      = OPSTAT_OFF(op_access),
	[FX_OP_CREATE]      = OPSTAT_OFF(op_create),
	[FX_OP_INTERRUPT]   = OPSTAT_OFF(op_interrupt),
	[FX_OP_BMAP]        = OPSTAT_OFF(op_bmap),
	[FX_OP_POLL]        = OPSTAT_OFF(op_poll),
	[FX_OP_TRUNCATE]    = OPSTAT_OFF(op_truncate),
	[FX_OP_FALLOCATE]   = OPSTAT_OFF(op_fallocate),
	[FX_OP_FPUNCH]      = OPSTAT_OFF(op_fpunch),
	[FX_OP_FQUERY]       = OPSTAT_OFF(op_stats),
	[FX_OP_XCOPY]       = OPSTAT_OFF(op_xcopy),
	[FX_OP_LAST]        = 0
};

fx_size_t *fx_opstat_getcnt(fx_opstat_t *opstat, int opc)
{
	size_t nelems, offset = 0;
	void *ptr = NULL;

	nelems = FX_NELEMS(s_opc_to_opstat_off);
	if ((opc > 0) && (opc < (int)nelems)) {
		offset = s_opc_to_opstat_off[opc];
		if (offset > 0) {
			ptr = (void *)((char *)opstat + offset);
		}
	}
	return (fx_size_t *)ptr;
}


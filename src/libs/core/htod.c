/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <endian.h>

#include "fnxinfra.h"
#include "meta.h"
#include "types.h"
#include "stats.h"
#include "htod.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static uint16_t htod_u16(uint16_t n)
{
	return htole16(n);
}

static uint32_t htod_u32(uint32_t n)
{
	return htole32(n);
}

static uint64_t htod_u64(uint64_t n)
{
	return htole64(n);
}

static uint16_t dtoh_u16(uint16_t n)
{
	return le16toh(n);
}

static uint32_t dtoh_u32(uint32_t n)
{
	return le32toh(n);
}

static uint64_t dtoh_u64(uint64_t n)
{
	return le64toh(n);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

uint16_t fx_htod_u16(uint16_t n)
{
	return htod_u16(n);
}

uint32_t fx_htod_u32(uint32_t n)
{
	return htod_u32(n);
}

uint64_t fx_htod_u64(uint64_t n)
{
	return htod_u64(n);
}

uint32_t fx_htod_uid(fx_uid_t uid)
{
	return htod_u32((uint32_t)uid);
}

uint32_t fx_htod_gid(fx_gid_t gid)
{
	return htod_u32((uint32_t)gid);
}

uint64_t fx_htod_ino(fx_ino_t ino)
{
	return htod_u64(ino);
}

uint32_t fx_htod_mode(fx_mode_t mode)
{
	return htod_u32(mode);
}

uint32_t fx_htod_nlink(fx_nlink_t nlink)
{
	return htod_u32((uint32_t)nlink);
}

uint64_t fx_htod_dev(fx_dev_t dev)
{
	return htod_u64((uint64_t)dev);
}

uint64_t fx_htod_bkcnt(fx_bkcnt_t b)
{
	return htod_u64((uint64_t)b);
}

uint64_t fx_htod_filcnt(fx_filcnt_t c)
{
	return htod_u64((uint64_t)c);
}

uint64_t fx_htod_time(fx_time_t time)
{
	return htod_u64((uint64_t)time);
}

uint32_t fx_htod_versnum(fx_version_t v)
{
	return htod_u32(v);
}

uint32_t fx_htod_magic(fx_magic_t magic)
{
	return htod_u32(magic);
}

uint64_t fx_htod_flags(fx_flags_t flags)
{
	return htod_u64(flags);
}

uint64_t fx_htod_size(fx_size_t size)
{
	return htod_u64(size);
}

uint64_t fx_htod_hash(fx_hash_t hash)
{
	return htod_u64(hash);
}

uint64_t fx_htod_off(fx_off_t off)
{
	return htod_u64((uint64_t)off);
}

uint64_t fx_htod_lba(fx_lba_t lba)
{
	return htod_u64((uint64_t)lba);
}

uint8_t fx_htod_vtype(fx_vtype_e vtype)
{
	return (uint8_t)vtype;
}

uint16_t fx_htod_hfunc(fx_hfunc_e hfunc)
{
	return htod_u16((uint16_t)hfunc);
}

void fx_htod_uuid(uint8_t *d_uu, const fx_uuid_t *uu)
{
	fx_bcopy(d_uu, uu->uu, sizeof(uu->uu));
}

void fx_htod_timespec(uint64_t *sec, uint32_t *nsec, const fx_timespec_t *ts)
{
	*sec  = htod_u64((uint64_t)(ts->tv_sec));
	*nsec = htod_u32((uint32_t)(ts->tv_nsec));
}

void fx_htod_name(uint8_t *buf, uint8_t *psz, const fx_name_t *name)
{
	const size_t len = name->len;
	const char  *str = name->str;

	fx_assert(len <= FNX_NAME_MAX);
	fx_bcopy(buf, str, len);
	buf[len] = 0;
	*psz = (uint8_t)len;
}

void fx_htod_path(uint8_t *buf, uint16_t *psz, const fx_path_t *path)
{
	const size_t len = path->len;
	const char  *str = path->str;

	fx_assert(len < FNX_PATH_MAX);
	fx_bcopy(buf, str, len);
	*psz = htod_u16((uint16_t)len);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

uint16_t fx_dtoh_u16(uint16_t n)
{
	return dtoh_u16(n);
}

uint32_t fx_dtoh_u32(uint32_t n)
{
	return dtoh_u32(n);
}

uint64_t fx_dtoh_u64(uint64_t n)
{
	return dtoh_u64(n);
}

fx_uid_t fx_dtoh_uid(uint32_t uid)
{
	return (fx_uid_t)(dtoh_u32(uid));
}

fx_gid_t fx_dtoh_gid(uint32_t gid)
{
	return (fx_gid_t)(dtoh_u32(gid));
}

fx_ino_t fx_dtoh_ino(uint64_t ino)
{
	return (fx_ino_t)(dtoh_u64(ino));
}

fx_mode_t fx_dtoh_mode(uint32_t mode)
{
	return (fx_mode_t)(dtoh_u32(mode));
}

fx_nlink_t fx_dtoh_nlink(uint32_t nlink)
{
	return (fx_nlink_t)(dtoh_u32(nlink));
}

fx_dev_t fx_dtoh_dev(uint64_t dev)
{
	return (fx_dev_t)(dtoh_u64(dev));
}

fx_bkcnt_t fx_dtoh_bkcnt(uint64_t b)
{
	return (fx_bkcnt_t)(dtoh_u64(b));
}

fx_filcnt_t fx_dtoh_filcnt(uint64_t c)
{
	return (fx_filcnt_t)(dtoh_u64(c));
}

fx_time_t fx_dtoh_time(uint64_t time)
{
	return (fx_time_t)(dtoh_u64(time));
}

fx_version_t fx_dtoh_versnum(uint32_t vers)
{
	return dtoh_u32(vers);
}

fx_magic_t fx_dtoh_magic(uint32_t magic)
{
	return (fx_magic_t)(dtoh_u32(magic));
}

fx_flags_t fx_dtoh_flags(uint64_t flags)
{
	return dtoh_u64(flags);
}

fx_size_t fx_dtoh_size(uint64_t size)
{
	return dtoh_u64(size);
}

fx_hash_t fx_dtoh_hash(fx_hash_t hash)
{
	return dtoh_u64(hash);
}

fx_off_t fx_dtoh_off(uint64_t off)
{
	return (fx_off_t)(dtoh_u64(off));
}

fx_lba_t fx_dtoh_lba(uint64_t lba)
{
	return (fx_lba_t)(dtoh_u64(lba));
}

fx_vtype_e fx_dtoh_vtype(uint8_t t)
{
	return (fx_vtype_e)(t);
}

fx_hfunc_e fx_dtoh_hfunc(uint16_t h)
{
	const uint16_t u = dtoh_u16(h);
	return (fx_hfunc_e)(u);
}

void fx_dtoh_uuid(fx_uuid_t *uu, const uint8_t *d_uu)
{
	unsigned char uub[16];

	fx_bcopy(uub, d_uu, sizeof(uub));
	fx_uuid_assign(uu, uub);
}

void fx_dtoh_timespec(fx_timespec_t *ts, uint64_t sec, uint32_t nsec)
{
	ts->tv_sec  = fx_dtoh_time(sec);
	ts->tv_nsec = dtoh_u32(nsec);
}

void fx_dtoh_name(fx_name_t *name, const uint8_t *buf, uint8_t nbs)
{
	const size_t len = (size_t)nbs;
	fx_assert(len < sizeof(name->str));

	fx_bcopy(name->str, buf, len);
	name->str[len]  = '\0';
	name->len       = len;
}

void fx_dtoh_path(fx_path_t *path, const uint8_t *buf, uint16_t bsz)
{
	size_t len;

	len = dtoh_u16(bsz);
	fx_assert(len < sizeof(path->str));

	fx_bcopy(path->str, buf, len);
	path->str[len]  = '\0';
	path->len       = len;
}


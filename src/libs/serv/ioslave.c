/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"


#define watch_rc(rc, lba, cnt) \
	watch_rc_(rc, lba, cnt, FX_FUNCTION)

static void watch_rc_(int rc, fx_lba_t lba, fx_bkcnt_t cnt, const char *op)
{
	if (rc != 0) {
		if ((rc == -EAGAIN) || (rc == -EINTR) || (rc == -ENOSPC)) {
			fx_warn("%s: lba=%#lx cnt=%lu err=%d", op, lba, cnt, abs(rc));
			fx_usleep(100); /* XXX */
		} else {
			fx_panic("%s: lba=%#lx cnt=%lu err=%d", op, lba, cnt, abs(rc));
		}
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void verify_udata_lba(const fx_fscore_t *fscore, fx_lba_t lba)
{
	const fx_super_t  *super;
	const fx_layout_t *layout;
	const fx_extent_t *udata;

	super  = fscore->super;
	layout = &super->su_layout;
	udata  = &layout->l_udata;
	fx_assert(lba >= udata->lba);
	fx_assert(lba < (udata->lba + udata->cnt));
}

int fx_read_data_bk(fx_fscore_t *fscore, const fx_bkref_t *bkref)
{
	int rc, nn = 100;
	fx_bkcnt_t cnt;
	fx_lba_t beg, lba;
	const fx_vio_t *vio;
	const fx_layout_t *vol;

	vol = &fscore->super->su_layout;
	beg = vol->l_udata.lba;
	lba = beg + bkref->addr.lba;
	cnt = 1;
	vio = &fscore->vio;
	verify_udata_lba(fscore, lba);
	while (nn-- > 0) {
		rc = vio->read(vio, bkref->blk, lba, cnt);
		if (rc == 0) {
			break;
		}
		watch_rc(rc, lba, cnt);
	}
	return rc;
}

int fx_write_data_bk(fx_fscore_t *fscore, const fx_bkref_t *bkref)
{
	int rc, nn = 100;
	fx_bkcnt_t cnt;
	fx_lba_t beg, lba;
	const fx_vio_t *vio;
	const fx_layout_t *vol;

	vol = &fscore->super->su_layout;
	beg = vol->l_udata.lba;
	lba = beg + bkref->addr.lba;
	cnt = 1;
	vio = &fscore->vio;
	verify_udata_lba(fscore, lba);
	while (nn-- > 0) {
		rc = vio->write(vio, bkref->blk, lba, cnt);
		if (rc == 0) {
			break;
		}
		watch_rc(rc, lba, cnt);
		vio->sync(vio);
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int read_meta_bk(fx_fscore_t *fscore, const fx_bkref_t *bkref)
{
	int rc, nn = 100;
	fx_lba_t lba;
	fx_bkcnt_t cnt;
	fx_vio_t *vio;

	lba = bkref->addr.lba;
	cnt = FNX_SECNBK;
	vio = &fscore->vio;
	while (nn-- > 0) {
		rc  = vio->read(vio, bkref->blk, lba, cnt);
		if (rc == 0) {
			break;
		}
		watch_rc(rc, lba, cnt);
	}
	return rc;
}

static int checksign_meta_bk(const fx_bkref_t *sec)
{
	fx_dsec_t *dsec;

	dsec = fx_get_dsec(sec);
	return fx_dsec_checksum(dsec);
}

int fx_read_meta_bk(fx_fscore_t *fscore, const fx_bkref_t *sec)
{
	int rc;

	rc = read_meta_bk(fscore, sec);
	if (rc == 0) {
		rc = checksign_meta_bk(sec);
	}
	return rc;
}

static void sign_meta_bk(const fx_bkref_t *sec)
{
	fx_dsec_t *dsec;

	dsec = fx_get_dsec(sec);
	fx_dsec_signsum(dsec);
}

static int write_meta_bk(fx_fscore_t *fscore, const fx_bkref_t *sec)
{
	int rc, nn = 100;
	fx_lba_t lba;
	fx_bkcnt_t cnt;
	fx_vio_t *vio;

	lba = sec->addr.lba;
	cnt = FNX_SECNBK;
	vio = &fscore->vio;
	while (nn-- > 0) {
		rc  = vio->write(vio, sec->blk, lba, cnt);
		if (rc == 0) {
			break;
		}
		watch_rc(rc, lba, cnt);
		vio->sync(vio);
	}
	return rc;
}

int fx_write_meta_bk(fx_fscore_t *fscore, const fx_bkref_t *sec)
{
	int rc;

	sign_meta_bk(sec);
	rc = write_meta_bk(fscore, sec);
	return rc;
}

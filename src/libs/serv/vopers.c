/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"


#define trace_vop(corex, fmt, ...) \
	fx_trace1("%s: " fmt, FX_FUNCTION, __VA_ARGS__)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_count_vop(const fx_corex_t *corex)
{
	fx_size_t  *count;
	fx_super_t *super;
	const fx_task_t *task = corex->task;

	if (task != &corex->fscore->ptask) {
		super   = corex->get_super(corex);
		count   = fx_opstat_getcnt(&super->su_opstat, task->tsk_opcode);
		if (count != NULL) {
			(*count)++;
		}
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void inc_counter(fx_size_t *cnt, fx_size_t n)
{
	*cnt += n;
}

static void count_read(fx_corex_t *corex, fx_size_t nbytes)
{
	fx_super_t *super = corex->get_super(corex);
	inc_counter(&super->su_iostat.io_nread, nbytes);
}

static void count_write(fx_corex_t *corex, fx_size_t nbytes)
{
	fx_super_t *super = corex->get_super(corex);
	inc_counter(&super->su_iostat.io_nwrite, nbytes);
}

static const fx_request_t *get_request(fx_corex_t *corex)
{
	return &corex->task->tsk_request;
}

static fx_response_t *get_response(fx_corex_t *corex)
{
	return &corex->task->tsk_response;
}

static fx_fileref_t *get_fileref(fx_corex_t *corex)
{
	return corex->task->tsk_fref;
}

static fx_size_t sum_data(const fx_corex_t *corex)
{
	fx_size_t sum = 0;
	const fx_iobufs_t *iobufs;

	iobufs = corex->get_iobufs(corex);
	for (size_t i = 0; i < FX_NELEMS(iobufs->iob); ++i) {
		sum += iobufs->iob[i].sgm.rng.len;
	}
	return sum;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* LOOKUP */
static int vop_lookup(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t parent_ino;
	fx_hash_t nhash;
	fx_dir_t *dir;
	fx_inode_t *inode;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *name;

	rqst = get_request(corex);
	resp = get_response(corex);

	parent_ino  = rqst->req_lookup.parent;
	name        = &rqst->req_lookup.name;
	nhash       = rqst->req_lookup.hash;

	rc = fx_fetch_dir(corex, parent_ino, &dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_lookup(corex, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_inode(corex, dir, name, nhash, &inode);
	if (rc != 0) {
		return rc;
	}
	inode_getiattr(inode, &resp->res_lookup.iattr);
	trace_vop(corex, "parent_ino=%#jx name=%s", parent_ino, name->str);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FORGET */
static int vop_forget(fx_corex_t *corex)
{
	fx_ino_t ino;
	unsigned long nlookup;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	ino     = rqst->req_forget.ino;
	nlookup = rqst->req_forget.nlookup;

	/* Ignored operation */
	trace_vop(corex, "ino=%#jx nlookup=%lu", ino, nlookup);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* GETATTR */
static int vop_getattr(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_inode_t *inode = NULL;
	fx_response_t *resp;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	resp = get_response(corex);

	ino  = rqst->req_getattr.ino;
	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	inode_getiattr(inode, &resp->res_getattr.iattr);
	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* SETATTR */
static int vop_setattr(fx_corex_t *corex)
{
	int rc = 0;
	fx_flags_t flags;
	fx_ino_t ino;
	fx_off_t  size;
	fx_mode_t mode;
	fx_uid_t uid;
	fx_gid_t gid;
	fx_inode_t *inode;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_times_t   *itms;

	rqst    = get_request(corex);
	resp    = get_response(corex);
	flags   = rqst->req_setattr.flags;
	ino     = rqst->req_setattr.iattr.i_ino;
	mode    = rqst->req_setattr.iattr.i_mode;
	size    = rqst->req_setattr.iattr.i_size;
	uid     = rqst->req_setattr.iattr.i_uid;
	gid     = rqst->req_setattr.iattr.i_gid;
	itms    = &rqst->req_setattr.iattr.i_times;

	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_setattr(corex, inode, flags, mode, uid, gid, size);
	if (rc != 0) {
		return rc;
	}
	rc = fx_setattr_stat(corex, inode, flags, mode, uid, gid, size, itms);
	if (rc != 0) {
		return rc;
	}

	inode_getiattr(inode, &resp->res_setattr.iattr);
	trace_vop(corex, "ino=%#jx flags=%#lx mode=%o", ino, flags, mode);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* TRUNCATE */
static int vop_truncate(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_off_t size;
	fx_inode_t *reg;
	fx_fileref_t  *fref;
	fx_response_t *resp;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	resp = get_response(corex);
	fref = get_fileref(corex);

	ino  = rqst->req_truncate.ino;
	size = rqst->req_truncate.size;

	rc = fx_fetch_wreg(corex, fref, ino, &reg);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_setsize(corex, reg, size);
	if (rc != 0) {
		return rc;
	}
	if (size == inode_getsize(reg)) {
		goto out; /* OK, ignored case */
	}
	rc = fx_trunc_data(corex, reg, size);
	if (rc != 0) {
		return rc;
	}
	rc = fx_setattr_size(corex, reg, size);
	if (rc != 0) {
		return rc;
	}

out:
	inode_getiattr(reg, &resp->res_truncate.iattr);
	trace_vop(corex, "ino=%#jx size=%ju", ino, size);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* READLINK */
static int vop_readlink(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_symlnk_t *symlnk;
	fx_response_t *resp;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	resp = get_response(corex);
	ino  = rqst->req_readlink.ino;

	rc = fx_fetch_symlnk(corex, ino, &symlnk);
	if (rc != 0) {
		return rc;
	}
	rc = fx_read_symlnk(corex, symlnk, &resp->res_readlink.slnk);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* SYMLINK */
static int vop_symlink(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t parent_ino;
	fx_hash_t nhash;
	fx_super_t  *super   = NULL;
	fx_dir_t    *parentd = NULL;
	fx_symlnk_t *symlnk  = NULL;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *name;
	const fx_path_t *path;

	rqst = get_request(corex);
	resp = get_response(corex);

	parent_ino  = rqst->req_symlink.parent;
	name        = &rqst->req_symlink.name;
	nhash       = rqst->req_symlink.hash;
	path        = &rqst->req_symlink.slnk;

	rc = fx_fetch_wsuper(corex, &super);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_symlink(corex, parentd, name);
	if (rc != 0) {
		return rc;
	}
	rc = fx_acquire_symlnk(corex, path, &symlnk);
	if (rc != 0) {
		return rc;
	}
	rc = fx_link_child(corex, parentd, name, nhash, &symlnk->sl_inode);
	if (rc != 0) {
		return rc;
	}

	inode_getiattr(&symlnk->sl_inode, &resp->res_symlink.iattr);
	trace_vop(corex, "parent_ino=%#jx name=%s slnk=%s",
	          parent_ino, name->str, path->str);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* MKNOD */
static int vop_mknod(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t   parent_ino;
	fx_mode_t  mode;
	fx_dev_t   rdev;
	fx_hash_t  nhash;
	fx_super_t *super = NULL;
	fx_inode_t *inode = NULL;
	fx_dir_t *parentd;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *name;

	rqst = get_request(corex);
	resp = get_response(corex);

	parent_ino  = rqst->req_mknod.parent;
	name        = &rqst->req_mknod.name;
	nhash       = rqst->req_mknod.hash;
	mode        = rqst->req_mknod.mode;
	rdev        = rqst->req_mknod.rdev;

	rc = fx_fetch_wsuper(corex, &super);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_mknod(corex, parentd, name);
	if (rc != 0) {
		return rc;
	}
	rc = fx_acquire_special(corex, mode, &inode);
	if (rc != 0) {
		return rc;
	}
	inode->i_iattr.i_rdev = rdev; /* FIXME */
	rc = fx_link_child(corex, parentd, name, nhash, inode);
	if (rc != 0) {
		return rc;
	}

	inode_getiattr(inode, &resp->res_mknod.iattr);
	trace_vop(corex, "parent_ino=%#jx name=%s mode=%o rdev=%#jx",
	          parent_ino, name->str, mode, rdev);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* MKDIR */
static int vop_mkdir(fx_corex_t *corex)
{
	int rc = 0;
	fx_mode_t mode;
	fx_hash_t nhash;
	fx_ino_t parent_ino;
	fx_super_t *super;
	fx_dir_t *parentd;
	fx_dir_t *dir = NULL;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *name;

	rqst = get_request(corex);
	resp = get_response(corex);

	parent_ino  = rqst->req_mkdir.parent;
	name        = &rqst->req_mkdir.name;
	nhash       = rqst->req_mkdir.hash;
	mode        = rqst->req_mkdir.mode;

	rc = fx_fetch_wsuper(corex, &super);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_mkdir(corex, parentd, name);
	if (rc != 0) {
		return rc;
	}
	rc = fx_acquire_dir(corex, mode, &dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_link_child(corex, parentd, name, nhash, &dir->d_inode);
	if (rc != 0) {
		return rc;
	}

	inode_getiattr(&dir->d_inode, &resp->res_mkdir.iattr);
	trace_vop(corex, "parent_ino=%#jx name=%s mode=%o",
	          parent_ino, name->str, mode);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* UNLINK */
static int vop_unlink(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t parent_ino, ino = FNX_INO_NULL;
	fx_hash_t nhash;
	fx_inode_t *inode = NULL;
	fx_dir_t   *parentd;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *name;

	rqst = get_request(corex);
	resp = get_response(corex);
	parent_ino  = rqst->req_unlink.parent;
	name        = &rqst->req_unlink.name;
	nhash       = rqst->req_unlink.hash;

	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_link(corex, parentd, name, nhash, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_unlink(corex, parentd, inode);
	if (rc != 0) {
		return rc;
	}
	ino = inode_getrefino(inode);
	rc  = fx_unlink_child(corex, parentd, inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fix_unlinked(corex, inode);
	if (rc != 0) {
		return rc;
	}

	resp->res_unlink.ino = ino;
	trace_vop(corex, "parent_ino=%#jx name=%s ino=%#jx",
	          parent_ino, name->str, ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* RMDIR */
static int vop_rmdir(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t parent_ino;
	fx_hash_t nhash;
	fx_dir_t *parentd;
	fx_dir_t *dir;
	const fx_request_t *rqst;
	const fx_name_t *name;

	rqst = get_request(corex);

	parent_ino  = rqst->req_rmdir.parent;
	name        = &rqst->req_rmdir.name;
	nhash       = rqst->req_rmdir.hash;

	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_dir(corex, parentd, name, nhash, &dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_rmdir(corex, parentd, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_unlink_child(corex, parentd, &dir->d_inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fix_unlinked(corex, &dir->d_inode);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "parent_ino=%#jx name=%s", parent_ino, name->str);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* RENAME */
static int vop_rename(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t parent_ino, newparent_ino;
	fx_hash_t nhash, newnhash;
	fx_inode_t *inode, *curinode = NULL;
	fx_dir_t *parentd, *newparentd;
	const fx_request_t *rqst;
	const fx_name_t *name;
	const fx_name_t *newname;

	rqst = get_request(corex);

	parent_ino      = rqst->req_rename.parent;
	name            = &rqst->req_rename.name;
	nhash           = rqst->req_rename.hash;
	newparent_ino   = rqst->req_rename.newparent;
	newname         = &rqst->req_rename.newname;
	newnhash        = rqst->req_rename.newhash;

	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_dir(corex, newparent_ino, &newparentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_hinode(corex, parentd, name, nhash, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_ientry(corex, newparentd, newname, newnhash, &curinode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_rename_src(corex, parentd, newparentd, inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_rename_tgt(corex, newparentd, curinode);
	if (rc != 0) {
		return rc;
	}

	if (curinode == NULL) {
		/* Target does not exist */
		rc = fx_unlink_child(corex, parentd, inode);
		if (rc != 0) {
			return rc;
		}
		rc = fx_link_child(corex, newparentd, newname, newnhash, inode);
		if (rc != 0) {
			return rc;
		}
	} else {
		/* Replace existing inode */
		rc = fx_unlink_child(corex, newparentd, curinode);
		if (rc != 0) {
			return rc;
		}
		rc = fx_unlink_child(corex, parentd, inode);
		if (rc != 0) {
			return rc;
		}
		rc = fx_link_child(corex, newparentd, newname, newnhash, inode);
		if (rc != 0) {
			return rc;
		}
		rc = fx_fix_unlinked(corex, curinode);
		if (rc != 0) {
			return rc;
		}
	}

	trace_vop(corex, "parent_ino=%#jx name=%s newparent_ino=%#jx newname=%s",
	          parent_ino, name->str, newparent_ino, newname->str);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* LINK */
static int vop_link(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino, newparent_ino;
	fx_hash_t newnhash;
	fx_inode_t *inode, *hlnk = NULL;
	fx_dir_t *newparentd = NULL;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *newname;

	rqst = get_request(corex);
	resp = get_response(corex);
	ino             = rqst->req_link.ino;
	newparent_ino   = rqst->req_link.newparent;
	newname         = &rqst->req_link.newname;
	newnhash        = rqst->req_link.newhash;

	rc = fx_fetch_dir(corex, newparent_ino, &newparentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_link(corex, newparentd, inode, newname);
	if (rc != 0) {
		return rc;
	}
	rc = fx_acquire_reflnk(corex, inode, &hlnk);
	if (rc != 0) {
		return rc;
	}
	rc = fx_link_child(corex, newparentd, newname, newnhash, hlnk);
	if (rc != 0) {
		/* XXX del hlnk */
		fx_assert(0);
		return rc;
	}

	inode_getiattr(inode, &resp->res_link.iattr);
	trace_vop(corex, "ino=%#jx newparent_ino=%#jx newname=%s",
	          ino, newparent_ino, newname->str);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* OPEN */
static int vop_open(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_flags_t flags;
	fx_inode_t *inode;
	fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	ino     = rqst->req_open.ino;
	flags   = rqst->req_open.flags;

	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_open(corex, inode, flags);
	if (rc != 0) {
		return rc;
	}
	rc = fx_update_data(corex, inode, flags);
	if (rc != 0) {
		return rc;
	}
	rc = fx_attach_fref(corex, inode, flags, &fref);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx flags=%#lx", ino, flags);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* READ */
static int vop_read(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t  ino;
	fx_off_t  off;
	fx_size_t size, rsize = 0;
	const fx_fileref_t *fref;
	const fx_request_t *rqst;
	fx_response_t *resp;

	rqst = get_request(corex);
	resp = get_response(corex);
	fref = get_fileref(corex);

	ino     = rqst->req_read.ino;
	off     = rqst->req_read.off;
	size    = rqst->req_read.size;

	rc = fx_let_read(corex, fref, ino, off, size);
	if (rc != 0) {
		return rc;
	}
	rc = fx_read_data(corex, fref, off, size);
	if (rc != 0) {
		return rc;
	}
	rsize = sum_data(corex);
	resp->res_read.size = rsize;

	count_read(corex, rsize);
	trace_vop(corex, "ino=%#jx off=%ju rsize=%zu", ino, off, rsize);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* WRITE */
static int vop_write(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t  ino;
	fx_size_t size, wsize = 0;
	fx_off_t off;
	fx_fileref_t *fref;
	fx_iobufs_t  *iobufs;
	const fx_request_t *rqst;
	fx_response_t *resp;

	rqst = get_request(corex);
	resp = get_response(corex);
	fref = get_fileref(corex);

	ino     = rqst->req_write.ino;
	off     = rqst->req_write.off;
	size    = rqst->req_write.size;
	iobufs  = &corex->task->tsk_iobufs;

	rc = fx_let_write(corex, fref, ino, off, size);
	if (rc != 0) {
		fx_iobufs_release(iobufs); /* TODO elsewhere */
		return rc;
	}
	rc = fx_write_data(corex, fref, off, size);
	if (rc != 0) {
		return rc;
	}

	wsize = sum_data(corex);
	resp->res_write.size = wsize;

	count_write(corex, wsize);
	trace_vop(corex, "ino=%#jx off=%ju size=%zu", ino, off, wsize);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* STATFS */
static int vop_statfs(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_inode_t  *inode = NULL;
	fx_super_t  *super = NULL;
	fx_response_t *resp;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	resp = get_response(corex);
	ino  = rqst->req_statfs.ino;
	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_super(corex, &super);
	if (rc != 0) {
		return rc;
	}
	fx_super_getfsinfo(super, &resp->res_statfs.fsinfo);
	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* RELEASE */
static int vop_release(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_flags_t flags;
	fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	fref    = get_fileref(corex);
	ino     = rqst->req_release.ino;
	flags   = rqst->req_release.flags;

	rc = fx_let_release(corex, fref, ino);
	if (rc != 0) {
		return rc;
	}
	rc = fx_detach_fref(corex, fref);
	if (rc != 0) {
		return rc;
	}
	trace_vop(corex, "ino=%#jx flags=%#lx", ino, flags);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FSYNC */
static int vop_fsync(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_bool_t only_data;
	const fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	fref = get_fileref(corex);
	ino  = rqst->req_fsync.ino;
	only_data = rqst->req_fsync.datasync;

	rc = fx_let_fsync(corex, fref, ino);
	if (rc != 0) {
		return rc;
	}
	rc = fx_destage_dirty_of(corex, ino, !only_data);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx only-data=%d", ino, only_data);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FLUSH */
static int vop_flush(fx_corex_t *corex)
{
	int rc;
	fx_ino_t ino;
	const fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	fref    = get_fileref(corex);
	ino     = rqst->req_flush.ino;

	rc = fx_let_flush(corex, fref, ino);
	if (rc != 0) {
		return rc;
	}
	rc = fx_destage_dirty_of(corex, ino, 0);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* OPENDIR */
static int vop_opendir(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_dir_t *dir;
	fx_fileref_t *fref = NULL;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	ino  = rqst->req_opendir.ino;

	rc = fx_fetch_dir(corex, ino, &dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_opendir(corex, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_attach_fref(corex, &dir->d_inode, 0, &fref);
	if (rc != 0) {
		return rc;
	}
	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* READDIR */
static int vop_readdir(fx_corex_t *corex)
{
	int rc = 0;
	fx_off_t off, off_next  = FNX_DOFF_NONE; /* By default no next entry */
	fx_ino_t ino, child_ino = FNX_INO_NULL;
	fx_name_t *name     = NULL;
	fx_dir_t  *parentd  = NULL;
	fx_inode_t *child   = NULL;
	const fx_fileref_t *fref;
	const fx_request_t *rqst;
	fx_response_t *resp;

	rqst = get_request(corex);
	resp = get_response(corex);
	fref = get_fileref(corex);

	ino  = rqst->req_readdir.ino;
	off  = rqst->req_readdir.off;

	/* Fill response so that even if no next entry, output is valid */
	resp->res_readdir.name.len  = 0;
	resp->res_readdir.child = child_ino;
	resp->res_readdir.mode      = 0;
	resp->res_readdir.off_next  = off_next;

	/* May get -1 offset at end-of-stream. Still OK. */
	if (!fx_doff_isvalid(off)) {
		goto out; /* OK */
	}
	rc = fx_let_readdir(corex, fref, ino, off);
	if (rc != 0) {
		return rc;
	}
	parentd = fx_inode_to_dir(fref->f_inode);
	rc = fx_search_dent(corex, parentd, off, &name, &child, &off_next);
	if (rc == 0) {
		fx_name_clone(name, &resp->res_readdir.name);
		resp->res_readdir.child = child_ino = inode_getino(child);
		resp->res_readdir.mode      = child->i_iattr.i_mode;
		resp->res_readdir.off_next  = off_next;
	} else if (rc == FX_EEOS) {
		resp->res_readdir.child = child_ino = FNX_INO_NULL;
		resp->res_readdir.off_next  = off_next  = FNX_DOFF_NONE;
	} else {
		return rc;
	}

out:
	trace_vop(corex, "ino=%#jx off=%jd child_ino=%#jx off_next=%jd",
	          ino, off, child_ino, off_next);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* RELEASEDIR */
static int vop_releasedir(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst = get_request(corex);
	fref = get_fileref(corex);
	ino  = rqst->req_releasedir.ino;

	rc = fx_let_releasedir(corex, fref, ino);
	if (rc != 0) {
		return rc;
	}
	rc = fx_detach_fref(corex, fref);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FSYNCDIR */
static int vop_fsyncdir(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t ino;
	int datsync;
	fx_dir_t *dir;
	fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	fref    = get_fileref(corex);
	ino     = rqst->req_fsyncdir.ino;
	datsync = rqst->req_fsyncdir.datasync;

	rc = fx_fetch_dir(corex, ino, &dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_fsyncdir(corex, fref, ino);
	if (rc != 0) {
		return rc;
	}
	rc = fx_destage_dirty_of(corex, ino, 1);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx datsync=%d", ino, datsync);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* ACCESS */
static int vop_access(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t  ino;
	fx_mode_t mask;
	fx_inode_t *inode;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	ino     = rqst->req_access.ino;
	mask    = rqst->req_access.mask;

	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_access(corex, inode, mask);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx mask=%o", ino, mask);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* CREATE */
static int vop_create(fx_corex_t *corex)
{
	int rc = 0;
	fx_mode_t  mode;
	fx_flags_t flags;
	fx_hash_t  nhash;
	fx_ino_t   parent_ino;
	fx_super_t *super;
	fx_inode_t *reg;
	fx_dir_t   *parentd;
	fx_fileref_t  *fref;
	fx_response_t *resp;
	const fx_request_t *rqst;
	const fx_name_t *name;

	rqst = get_request(corex);
	resp = get_response(corex);

	parent_ino  = rqst->req_create.parent;
	name        = &rqst->req_create.name;
	nhash       = rqst->req_create.hash;
	mode        = rqst->req_create.mode;
	flags       = rqst->req_create.flags;

	rc = fx_fetch_wsuper(corex, &super);
	if (rc != 0) {
		return rc;
	}
	rc = fx_fetch_dir(corex, parent_ino, &parentd);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_create(corex, parentd, name, mode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_acquire_reg(corex, mode, &reg);
	if (rc != 0) {
		return rc;
	}
	rc = fx_link_child(corex, parentd, name, nhash, reg);
	if (rc != 0) {
		fx_assert(0);
		return rc;
	}
	rc = fx_attach_fref(corex, reg, flags, &fref);
	if (rc != 0) {
		/* XXX release inode */
		fx_assert(0);
		return rc;
	}

	inode_getiattr(reg, &resp->res_create.iattr);
	trace_vop(corex, "parent_ino=%#jx name=%s mode=%o flags=%#lx",
	          parent_ino, name->str, mode, flags);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FALLOCATE */
static int vop_fallocate(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t  ino;
	fx_off_t  off;
	fx_size_t len;
	fx_bool_t keepsz;
	fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	fref    = get_fileref(corex);

	ino     = rqst->req_fallocate.ino;
	off     = rqst->req_fallocate.off;
	len     = rqst->req_fallocate.len;
	keepsz  = rqst->req_fallocate.keep_size;

	rc = fx_let_fallocate(corex, fref, ino, off, len);
	if (rc != 0) {
		return rc;
	}
	rc = fx_falloc_data(corex, fref, off, len, keepsz);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx off=%ju len=%zu keep_size=%i",
	          ino, off, len, keepsz);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FPUNCH */
static int vop_fpunch(fx_corex_t *corex)
{
	int rc = 0;
	fx_ino_t  ino;
	fx_off_t  off;
	fx_size_t len;
	fx_fileref_t *fref;
	const fx_request_t *rqst;

	rqst    = get_request(corex);
	fref    = get_fileref(corex);

	ino     = rqst->req_fpunch.ino;
	off     = rqst->req_fpunch.off;
	len     = rqst->req_fpunch.len;

	rc = fx_let_fpunch(corex, fref, ino, off, len);
	if (rc != 0) {
		return rc;
	}
	rc = fx_punch_data(corex, fref->f_inode, off, len);
	if (rc != 0) {
		return rc;
	}

	trace_vop(corex, "ino=%#jx off=%ju len=%zu", ino, off, len);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* FQUERY */
static int vop_fquery(fx_corex_t *corex)
{
	int rc;
	fx_ino_t ino;
	fx_fileref_t *fref;
	const fx_request_t *rqst;
	fx_response_t *resp;

	rqst    = get_request(corex);
	resp    = get_response(corex);
	fref    = get_fileref(corex);
	ino     = rqst->req_fquery.ino;

	rc = fx_let_fquery(corex, fref, ino);
	if (rc != 0) {
		return rc;
	}

	inode_getiattr(fref->f_inode, &resp->res_fquery.iattr);
	trace_vop(corex, "ino=%#jx", ino);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_corex_fn fx_vops_hooks[] = {
	[FX_OP_NONE]       = NULL,
	[FX_OP_MKDIR]      = vop_mkdir,
	[FX_OP_RMDIR]      = vop_rmdir,
	[FX_OP_OPENDIR]    = vop_opendir,
	[FX_OP_READDIR]    = vop_readdir,
	[FX_OP_RELEASEDIR] = vop_releasedir,
	[FX_OP_FSYNCDIR]   = vop_fsyncdir,
	[FX_OP_STATFS]     = vop_statfs,
	[FX_OP_FORGET]     = vop_forget,
	[FX_OP_GETATTR]    = vop_getattr,
	[FX_OP_SETATTR]    = vop_setattr,
	[FX_OP_ACCESS]     = vop_access,
	[FX_OP_OPEN]       = vop_open,
	[FX_OP_READ]       = vop_read,
	[FX_OP_WRITE]      = vop_write,
	[FX_OP_RELEASE]    = vop_release,
	[FX_OP_FSYNC]      = vop_fsync,
	[FX_OP_FLUSH]      = vop_flush,
	[FX_OP_INTERRUPT]  = NULL,
	[FX_OP_BMAP]       = NULL,
	[FX_OP_POLL]       = NULL,
	[FX_OP_TRUNCATE]   = vop_truncate,
	[FX_OP_FALLOCATE]  = vop_fallocate,
	[FX_OP_FPUNCH]     = vop_fpunch,
	[FX_OP_LOOKUP]     = vop_lookup,
	[FX_OP_LINK]       = vop_link,
	[FX_OP_UNLINK]     = vop_unlink,
	[FX_OP_RENAME]     = vop_rename,
	[FX_OP_READLINK]   = vop_readlink,
	[FX_OP_SYMLINK]    = vop_symlink,
	[FX_OP_CREATE]     = vop_create,
	[FX_OP_MKNOD]      = vop_mknod,
	[FX_OP_FQUERY]     = vop_fquery,
	[FX_OP_XCOPY]      = NULL,
	[FX_OP_LAST]       = NULL,
};




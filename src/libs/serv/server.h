/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_SERVER_H_
#define FUNEX_SERVER_H_


/*
 * File-system server.
 */
struct fx_server {
	const char     *usock;
	fx_fusei_t      fusei;
	fx_fscore_t     fscore;
	fx_corex_t      corex;
	fx_cache_t      cache;

	signed short    active;
	fx_balloc_t     balloc;
	fx_valloc_t     valloc;
	fx_taskpool_t   taskpool;

	fx_taskq_t      fusei_taskq;
	fx_thread_t     fusei_rx_thread;
	fx_thread_t     fusei_tx_thread;

	fx_taskq_t      filter_taskq;
	fx_thread_t     filter_thread;

	fx_execq_t      core_execq;
	fx_thread_t     core_thread;
	fx_mutex_t      core_lock;

	fx_execq_t      slave_execq;
	fx_thread_t     slave_thread;
};
typedef struct fx_server   fx_server_t;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_server_init(fx_server_t *);

void fx_server_destroy(fx_server_t *);

int fx_server_setup(fx_server_t *, const fx_uctx_t *, const char *, fx_mntf_t);

int fx_server_open(fx_server_t *, const char *);

int fx_server_mount(fx_server_t *, const char *);

int fx_server_close(fx_server_t *);

int fx_server_start(fx_server_t *);

int fx_server_stop(fx_server_t *);

void fx_server_cleanup(fx_server_t *);

void fx_server_enslave_bk(fx_server_t *, fx_bkref_t *, int);


fx_task_t *fx_server_new_task(fx_server_t *, fx_opcode_e);

void fx_server_del_task(fx_server_t *, fx_task_t *);


#endif /* FUNEX_SERVER_H_ */

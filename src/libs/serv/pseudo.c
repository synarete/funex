/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxfusei.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"
#include "server.h"

static fx_server_t *corex_to_server(const fx_corex_t *corex)
{
	return fx_container_of(corex, fx_server_t, corex);
}


static const fx_imeta_t s_imeta_ioctl = {
	.name = FNX_PSIOCTLNAME,
	.mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP,
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int save_halt(fx_inode_t *inode, const void *blk, size_t sz)
{
	int rc, nn;
	fx_substr_t ss;
	fx_super_t *super = inode->i_super;

	fx_substr_init_rd(&ss, (const char *)blk, sz);
	rc = fx_parse_param(&ss, &nn);
	if ((rc == 0) && (nn == 0)) {
		super->su_active = FNX_FALSE;
	} else if ((rc == 0) && (nn == 1)) {
		super->su_active = FNX_TRUE;
	} else {
		rc = -EINVAL;
	}
	return rc;
}

static int show_halt(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_intval(&ss, inode->i_super->su_active);
	*sz = ss.len;
	return 0;
}

static const fx_imeta_t s_imeta_halt = {
	.name = FNX_PSHALTNAME,
	.mode = S_IRUSR | S_IWUSR | S_IRGRP,
	.show = show_halt,
	.save = save_halt
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int show_uuid(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_uuid(&ss, &inode->i_super->su_uuid);
	*sz = ss.len;
	return 0;
}

static int show_iostat(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_iostat(&ss, &inode->i_super->su_iostat, NULL);
	*sz = ss.len;
	return 0;
}

static int show_vstats(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_vstats(&ss, &inode->i_super->su_vstats, NULL);
	*sz = ss.len;
	return 0;
}

static int show_opstat(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_opstat(&ss, &inode->i_super->su_opstat, NULL);
	*sz = ss.len;
	return 0;
}

static int show_layout(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_layout(&ss, &inode->i_super->su_layout, NULL);
	*sz = ss.len;
	return 0;
}

static const fx_imeta_t s_imeta_uuid = {
	.name = "uuid",
	.mode = S_IRUSR | S_IRGRP,
	.show = show_uuid
};

static const fx_imeta_t s_imeta_iostat = {
	.name = FNX_PSIOSTATSNAME,
	.mode = S_IRUSR | S_IRGRP,
	.show = show_iostat
};

static const fx_imeta_t s_imeta_vstats = {
	.name = FNX_PSVSTATSNAME,
	.mode = S_IRUSR | S_IRGRP,
	.show = show_vstats
};

static const fx_imeta_t s_imeta_opstat = {
	.name = FNX_PSOPSTATSNAME,
	.mode = S_IRUSR | S_IRGRP,
	.show = show_opstat
};

static const fx_imeta_t s_imeta_layout = {
	.name = FNX_PSLAYOUTNAME,
	.mode = S_IRUSR | S_IRGRP,
	.show = show_layout
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int show_cstats(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;
	const fx_cache_t *cache  = &inode->i_server->cache;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_cstats(&ss, &cache->cstats, NULL);
	*sz = ss.len;
	return 0;
}

static int show_climits(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;
	const fx_cache_t *cache  = &inode->i_server->cache;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_climits(&ss, &cache->climits, NULL);
	*sz = ss.len;
	return 0;
}

static int show_cthreshold(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;
	const fx_cache_t *cache  = &inode->i_server->cache;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_intval(&ss, (int)cache->climits.c_vinode_lim);
	*sz = ss.len;
	return 0;
}

static int save_cthreshold(fx_inode_t *inode, const void *blk, size_t sz)
{
	int rc, nn;
	fx_substr_t ss;
	fx_cache_t *cache  = &inode->i_server->cache;

	fx_substr_init_rd(&ss, (const char *)blk, sz);
	rc = fx_parse_param(&ss, &nn);
	if ((rc == 0) && (nn > 0)) {
		fx_cache_set_limits(cache, (fx_size_t)nn);
	} else {
		rc = -EINVAL;
	}
	return rc;
}

static const fx_imeta_t s_imeta_cstats = {
	.name = FNX_PSCSTATSNAME,
	.mode = S_IRUSR | S_IRGRP,
	.show = show_cstats
};

static const fx_imeta_t s_imeta_climits = {
	.name = FNX_PSCLIMITSSNAME,
	.mode = S_IRUSR | S_IRGRP,
	.show = show_climits
};

static const fx_imeta_t s_imeta_cthreshold = {
	.name = "threshold",
	.mode = S_IRUSR | S_IWUSR | S_IRGRP,
	.show = show_cthreshold,
	.save = save_cthreshold
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int save_attr_timeout(fx_inode_t *inode, const void *blk, size_t sz)
{
	int rc, nn;
	fx_substr_t ss;
	fx_server_t *server = inode->i_server;
	fx_fusei_t  *fusei  = &server->fusei;

	fx_substr_init_rd(&ss, (const char *)blk, sz);
	rc = fx_parse_param(&ss, &nn);
	if ((rc == 0) && (nn >= 0) && (nn < 10)) {
		fusei->fi_info.attr_timeout = (unsigned)nn;
	} else {
		rc = -EINVAL;
	}
	return rc;
}

static int show_attr_timeout(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;
	const fx_server_t *server = inode->i_server;
	const fx_fusei_t  *fusei  = &server->fusei;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_intval(&ss, (int)fusei->fi_info.attr_timeout);
	*sz = ss.len;
	return 0;
}

static int save_entry_timeout(fx_inode_t *inode, const void *blk, size_t sz)
{
	int rc, nn;
	fx_substr_t ss;
	fx_server_t *server = inode->i_server;
	fx_fusei_t  *fusei  = &server->fusei;

	fx_substr_init_rd(&ss, (const char *)blk, sz);
	rc = fx_parse_param(&ss, &nn);
	if ((rc == 0) && (nn >= 0) && (nn < 10)) {
		fusei->fi_info.entry_timeout = (unsigned)nn;
	} else {
		rc = -EINVAL;
	}
	return rc;
}

static int show_entry_timeout(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;
	const fx_server_t *server = inode->i_server;
	const fx_fusei_t  *fusei  = &server->fusei;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_intval(&ss, (int)fusei->fi_info.entry_timeout);
	*sz = ss.len;
	return 0;
}

static const fx_imeta_t s_imeta_attr_timeout = {
	.name = "attr_timeout",
	.mode = S_IRUSR | S_IWUSR | S_IRGRP,
	.save = save_attr_timeout,
	.show = show_attr_timeout
};

static const fx_imeta_t s_imeta_entry_timeout = {
	.name = "entry_timeout",
	.mode = S_IRUSR | S_IWUSR | S_IRGRP,
	.save = save_entry_timeout,
	.show = show_entry_timeout
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int save_log_debug(fx_inode_t *inode, const void *blk, size_t sz)
{
	int rc, nn = 0;
	fx_substr_t ss;

	fx_substr_init_rd(&ss, (const char *)blk, sz);
	rc = fx_parse_param(&ss, &nn);
	nn = -abs(nn);
	if ((rc == 0) && (nn <= 0) && (nn >= FX_LL_DEBUG3)) {
		fx_default_logger->log_debug = nn;
	} else {
		rc = -EINVAL;
	}
	fx_unused(inode);
	return rc;
}

static int show_log_debug(fx_inode_t *inode, void *p, size_t n, size_t *sz)
{
	fx_substr_t ss;

	fx_substr_init_rw(&ss, (char *)p, 0, n);
	fx_repr_intval(&ss, abs(fx_default_logger->log_debug));
	*sz = ss.len;
	fx_unused(inode);
	return 0;
}



static const fx_imeta_t s_imeta_log_debug = {
	.name = "debug",
	.mode = S_IRUSR | S_IWUSR | S_IRGRP,
	.save = save_log_debug,
	.show = show_log_debug
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_ino_t next_pseudo_ino(fx_corex_t *corex)
{
	fx_ino_t pino;

	pino = corex->fscore->pino++;
	if ((pino >= FNX_INO_APEX0) || (pino < FNX_INO_PSROOT)) {
		fx_critical("no-next-pseudo-ino: pino=%#jx", pino);
	}
	return pino;
}

static fx_inode_t *new_pseudo_inode(fx_corex_t *corex, fx_vtype_e vtype)
{
	fx_ino_t pino;
	fx_vnode_t *vnode = NULL;
	fx_inode_t *inode = NULL;

	vnode = corex->cache->new_vnode(corex->cache, vtype);
	if (vnode == NULL) {
		fx_panic("no-new-pseudo-inode: vtype=%d", vtype);
	}
	pino  = next_pseudo_ino(corex);
	inode = vnode_to_inode(vnode);
	fx_inode_setino(inode, pino);
	inode->i_super  = corex->get_super(corex);
	inode->i_server = corex_to_server(corex);
	vnode->v_pseudo = FNX_TRUE;
	vnode->v_stable = FNX_TRUE;
	corex->cache->store_vnode(corex->cache, vnode);
	return inode;
}

static fx_dir_t *new_dir(fx_corex_t *corex)
{
	fx_mode_t mode;
	fx_dir_t   *dir;
	fx_inode_t *inode;
	const fx_vtype_e vtype = FNX_VTYPE_DIR;

	inode = new_pseudo_inode(corex, vtype);
	dir   = fx_inode_to_dir(inode);
	mode  = fx_vtype_to_crmode(vtype, inode->i_iattr.i_mode, 0);
	fx_dir_setup(dir, corex->get_uctx(corex), mode);
	inode->i_iattr.i_mode |= (S_IRGRP | S_IXGRP);

	return dir;
}

static fx_inode_t *new_reg(fx_corex_t *corex, const fx_imeta_t *meta)
{
	fx_mode_t mode;
	fx_inode_t *inode;
	const fx_vtype_e vtype = FNX_VTYPE_REG;

	inode   = new_pseudo_inode(corex, vtype);
	mode    = fx_vtype_to_crmode(vtype, meta->mode, 0);
	fx_inode_setup(inode, corex->get_uctx(corex), mode, 0);
	inode_setsize(inode, sizeof(corex->pblk));
	inode->i_meta = meta;

	return inode;
}

static void linkp(fx_corex_t *corex, fx_dir_t *parentd,
                  const char *str, fx_inode_t *inode)
{
	int rc;
	fx_name_t name;

	fx_name_setup(&name, str);
	if (parentd != NULL) {
		rc = fx_link_child(corex, parentd, &name, 0, inode);
		if (rc != 0) {
			fx_panic("link-pseudo-failed: name=%s", name.str);
		}
	} else {
		fx_inode_associate(inode, FNX_INO_ROOT, &name, 0);
	}
}

static void linkp2(fx_corex_t *corex, fx_dir_t *parentd, fx_inode_t *inode)
{
	linkp(corex, parentd, inode->i_meta->name, inode);
}

static fx_dir_t *mkpdir(fx_corex_t *corex, fx_dir_t *parentd, const char *name)
{
	fx_dir_t *dir;

	dir = new_dir(corex);
	linkp(corex, parentd, name, &dir->d_inode);
	return dir;
}

static void mkpreg(fx_corex_t *corex, fx_dir_t *parentd, const fx_imeta_t *meta)
{
	fx_inode_t *reg;

	reg = new_reg(corex, meta);
	linkp2(corex, parentd, reg);
}


static void genpns(fx_corex_t *corex)
{
	fx_dir_t *parentd, *rootd;
	fx_fscore_t *fscore;

	fscore = corex->fscore;
	fscore->proot = rootd = mkpdir(corex, NULL, FNX_PSROOTNAME);
	mkpreg(corex, rootd, &s_imeta_ioctl);
	mkpreg(corex, rootd, &s_imeta_halt);
	mkpreg(corex, rootd, &s_imeta_uuid);
	parentd = mkpdir(corex, rootd, FNX_PSSUPERDNAME);
	mkpreg(corex, parentd, &s_imeta_vstats);
	mkpreg(corex, parentd, &s_imeta_iostat);
	mkpreg(corex, parentd, &s_imeta_opstat);
	mkpreg(corex, parentd, &s_imeta_layout);
	parentd = mkpdir(corex, rootd, FNX_PSCACHEDNAME);
	mkpreg(corex, parentd, &s_imeta_cstats);
	mkpreg(corex, parentd, &s_imeta_climits);
	mkpreg(corex, parentd, &s_imeta_cthreshold);
	parentd = mkpdir(corex, rootd, "fusei");
	mkpreg(corex, parentd, &s_imeta_attr_timeout);
	mkpreg(corex, parentd, &s_imeta_entry_timeout);
	parentd = mkpdir(corex, rootd, "logger");
	mkpreg(corex, parentd, &s_imeta_log_debug);
}

static void tiepns(fx_corex_t *corex)
{
	fx_dir_t *root, *proot;
	fx_fscore_t *fscore;

	fscore  = corex->fscore;
	proot   = fscore->proot;
	root    = fscore->root;
	proot->d_dent[FNX_DOFF_PARENT].de_ino = dir_getino(root);
}

/* Pseudo-namespace operations */
int fx_bind_pseudo_namespace(fx_corex_t *corex)
{
	genpns(corex);
	tiepns(corex);

	return 0;
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"

/* Local functions */
static int retire_bk(fx_corex_t *, const fx_bkaddr_t *, fx_bkref_t *);

/* Local helpers */
static size_t niob(const fx_iobufs_t *iob)
{
	return FX_NELEMS((iob)->iob);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int fetch_slice(fx_corex_t *corex, const fx_inode_t *reg,
                       fx_off_t off, fx_slice_t **slice)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	fx_assert(off >= FNX_SEGSIZE);
	vaddr_for_slice(&vaddr, inode_getino(reg), off);
	rc = fx_fetch_vnode(corex, &vaddr, &vnode);
	if (rc == 0) {
		*slice = fx_vnode_to_slice(vnode);
	}
	return rc;
}

static int acquire_slice(fx_corex_t *corex, const fx_inode_t *reg,
                         fx_off_t off, fx_slice_t **slice)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	fx_assert(off >= FNX_SEGSIZE);
	vaddr_for_slice(&vaddr, inode_getino(reg), off);
	rc = fx_acquire_vnode(corex, &vaddr, &vnode);
	if (rc == 0) {
		*slice = fx_vnode_to_slice(vnode);
	}
	return rc;
}

static int yield_slice(fx_corex_t *corex, const fx_inode_t *reg,
                       fx_off_t off, fx_slice_t **slice)
{
	int rc;

	rc  = fetch_slice(corex, reg, off, slice);
	if (rc == -ENOENT) {
		rc = acquire_slice(corex, reg, off, slice);
	}
	return rc;
}

static int fetch_segmnt(fx_corex_t *corex, const fx_inode_t *reg,
                        fx_off_t off, fx_segmnt_t **segmnt)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	vaddr_for_segmnt(&vaddr, inode_getino(reg), off);
	rc = fx_fetch_vnode(corex, &vaddr, &vnode);
	if (rc == 0) {
		*segmnt = fx_vnode_to_segmnt(vnode);
	}
	return rc;
}

static int acquire_segmnt(fx_corex_t *corex, const fx_inode_t *reg,
                          fx_off_t off, fx_segmnt_t **segmnt)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	vaddr_for_segmnt(&vaddr, inode_getino(reg), off);
	rc = fx_acquire_vnode(corex, &vaddr, &vnode);
	if (rc == 0) {
		*segmnt = fx_vnode_to_segmnt(vnode);
		fx_segmnt_setup(*segmnt, inode_getino(reg), off);
	}
	return rc;
}

static int yield_segmnt(fx_corex_t *corex, const fx_inode_t *reg,
                        fx_off_t off, fx_segmnt_t **segmnt)
{
	int rc;

	rc = fetch_segmnt(corex, reg, off, segmnt);
	if ((rc == 0) || (rc != -ENOENT)) {
		return rc;
	}
	rc = acquire_segmnt(corex, reg, off, segmnt);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static int yield_ssegmnt(fx_corex_t *corex, const fx_inode_t *reg,
                         fx_slice_t *slice, fx_off_t off, fx_segmnt_t **segmnt)
{
	int rc;

	rc = fetch_segmnt(corex, reg, off, segmnt);
	if ((rc == 0) || (rc != -ENOENT)) {
		return rc;
	}
	rc = acquire_segmnt(corex, reg, off, segmnt);
	if (rc != 0) {
		return rc;
	}
	fx_slice_markseg(slice, off);
	corex->put_vnode(corex, &slice->sc_vnode);
	return 0;
}

static int
guarantee_space(fx_corex_t *corex, fx_bkcnt_t nbk, fx_spmap_t **spmap)
{
	int rc = 0;
	fx_lba_t ulba, uend;
	fx_super_t  *super;
	fx_fsstat_t *fsstat;
	const fx_extent_t *udata;
	const fx_bkcnt_t step = FNX_SPCNBK;

	super  = corex->get_super(corex);
	udata  = &super->su_layout.l_udata;
	fsstat = &super->su_fsstat;

	if (fsstat->f_bavail < nbk) { /* TODO: privileged */
		return -ENOSPC;
	}

	uend  = lba_end(0, udata->cnt);
	ulba  = fsstat->f_bnext;
	for (fx_bkcnt_t cnt = 0; cnt < udata->cnt; cnt += step) {
		if (ulba >= uend) {
			ulba = 0;
		}
		rc = fx_fetch_spmap(corex, ulba, spmap);
		if (rc != 0) {
			return rc;
		}
		if (fx_spmap_hasfree(*spmap, nbk)) {
			fsstat->f_bnext = ulba;
			return 0;
		}
		ulba = lba_floor_spseg(ulba + step);
	}

	*spmap = NULL;
	return -ENOSPC;
}

static int ispartial(fx_size_t len)
{
	return (len < FNX_BLKSIZE);
}

static int
stage_segbks(fx_corex_t  *corex, fx_segmnt_t *segmnt, const fx_brange_t *brange)
{
	int rc, pend = 0;
	fx_size_t len;
	fx_off_t off, cil, end;
	fx_spmap_t *spmap;
	fx_bkref_t *bkref;
	const fx_bkdsc_t  *bkdsc;
	const fx_bkaddr_t *bkaddr;

	off = brange->off;
	end = fx_brange_end(brange);
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		cil = off_ceil_blk(off);
		len = off_min_len(off, cil, end);
		off = cil;
		pos = brange->idx + i;
		bkaddr  = &segmnt->sg_segmap.bka[pos];
		if (bkaddr_isnull(bkaddr)) {
			continue;
		}
		rc = fx_fetch_spmap(corex, bkaddr->lba, &spmap);
		if (rc != 0) {
			return rc;
		}
		bkdsc = fx_spmap_getdsc(spmap, bkaddr->lba);
		fx_assert(bkdsc->refs > 0);
		if (ispartial(len)) {
			rc = corex->fetch_bk(corex, bkaddr, &bkref);
			if (rc == FX_ERPEND) {
				pend = 1;
			} else if (rc != 0) {
				return rc;
			}
		}
	}
	return (pend ? FX_ERPEND : 0);
}

static int
acquire_bkaddr(fx_corex_t *corex, fx_spmap_t *spmap, fx_bkaddr_t *bkaddr)
{
	int rc = -1;
	fx_super_t *super;
	const fx_uctx_t *uctx;

	super = corex->get_super(corex);
	uctx  = corex->get_uctx(corex);

	bkaddr->svol = FNX_SVOL_DATA;
	bkaddr->frg  = 0;
	rc = fx_consume_lba(super, spmap, fx_isprivileged(uctx), &bkaddr->lba);
	fx_assert(rc == 0);

	corex->put_vnode(corex, &spmap->sp_vnode);
	corex->put_super(corex, super, 0);
	return 0;
}

static int bind_newbks(fx_corex_t *corex, fx_spmap_t *spmap, fx_iobuf_t *iobuf)
{
	int rc = 0;
	fx_bkaddr_t *bkaddr;
	fx_bkref_t  *bkref;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos     = i + brange->idx;
		bkaddr  = &iobuf->sgm.bka[pos];
		bkref   = iobuf->bks[pos];
		rc = acquire_bkaddr(corex, spmap, bkaddr);
		if (rc != 0) {
			return rc;
		}
		bkaddr_copy(&bkref->addr, bkaddr);
	}
	return 0;
}

static int rewrite_segmnt(fx_corex_t  *corex, const fx_inode_t *reg,
                          fx_segmnt_t *segmnt, fx_iobuf_t *iobuf)
{
	int rc = 0;
	fx_ino_t ino;
	fx_size_t len;
	fx_off_t off, end, piv;
	fx_bkaddr_t *bkaddr;
	fx_bkref_t  *bkref, *bkref2;
	const fx_brange_t *rng;

	ino = inode_getino(reg);
	rng = &iobuf->sgm.rng;
	off = rng->off;
	end = off_end(rng->off, rng->len);
	for (size_t pos, i = 0; i < rng->cnt; ++i) {
		pos     = i + rng->idx;
		bkref   = iobuf->bks[pos];
		bkaddr  = &segmnt->sg_segmap.bka[pos];
		bkref2  = NULL;

		piv = off_ceil_blk(off);
		len = off_min_len(off, end, piv);

		if (!bkaddr_isnull(bkaddr)) {
			if (ispartial(len)) {
				rc = corex->fetch_bk(corex, bkaddr, &bkref2);
				fx_assert(rc == 0);
				fx_bkref_merge(bkref, off, len, bkref2);
			}
			rc = retire_bk(corex, bkaddr, bkref2);
			fx_assert(rc == 0);
		}

		bkref->ino = ino;
		bkaddr_copy(bkaddr, &bkref->addr);
		corex->put_bk(corex, bkref);

		off = off_end(off, len);
	}
	corex->put_vnode(corex, &segmnt->sg_vnode);

	return rc;
}

static int guarantee_segmnt(fx_corex_t *corex, const fx_inode_t *reg,
                            const fx_brange_t *brange, fx_segmnt_t **segmnt)
{
	int rc;
	fx_slice_t *slice = NULL;

	if (off_isseg0(brange->off)) {
		rc = yield_segmnt(corex, reg, brange->off, segmnt);
		if (rc != 0) {
			return rc;
		}
	} else {
		rc = yield_slice(corex, reg, brange->off, &slice);
		if (rc != 0) {
			return rc;
		}
		rc = yield_ssegmnt(corex, reg, slice, brange->off, segmnt);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

static int prepare_write(fx_corex_t *corex, fx_inode_t *reg,
                         const fx_iobuf_t *iobuf)
{
	int rc;
	fx_spmap_t  *spmap  = NULL;
	fx_segmnt_t *segmnt = NULL;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	rc = guarantee_space(corex, brange->cnt, &spmap);
	if (rc != 0) {
		return rc;
	}
	rc = guarantee_segmnt(corex, reg, brange, &segmnt);
	if (rc != 0) {
		return rc;
	}
	rc = stage_segbks(corex, segmnt, brange);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static int write_segmnt(fx_corex_t *corex, fx_inode_t *reg, fx_iobuf_t *iobuf)
{
	int rc;
	fx_spmap_t  *spmap  = NULL;
	fx_segmnt_t *segmnt = NULL;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	rc = guarantee_space(corex, brange->cnt, &spmap);
	if (rc != 0) {
		return rc;
	}
	rc = fetch_segmnt(corex, reg, brange->off, &segmnt);
	if (rc != 0) {
		return rc;
	}
	rc = bind_newbks(corex, spmap, iobuf);
	if (rc != 0) {
		return rc;
	}
	rc = rewrite_segmnt(corex, reg, segmnt, iobuf);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static void
update_capacity(fx_corex_t *corex, fx_inode_t *reg,
                fx_off_t off, fx_size_t len, fx_bkcnt_t nbks, fx_bool_t keepsz)
{
	fx_flags_t flags;
	fx_off_t   wsize, nsize;
	fx_bkcnt_t wbcap, nbcap;
	fx_iattr_t *iattr = &reg->i_iattr;

	wsize = off_end(off, len);
	nsize = off_max(iattr->i_size, wsize);
	wbcap = fx_bytes_to_nbk((fx_size_t)nsize);
	nbcap = bkcnt_max(iattr->i_bcap, wbcap);

	if (!keepsz) {
		iattr->i_size = nsize;
	}
	iattr->i_bcap = nbcap;
	iattr->i_nblk += nbks;

	flags = (len > 0) ? FNX_AMCTIME_NOW : FNX_ATIME_NOW;
	corex->put_inode(corex, reg, flags);
}

static int write_reg(fx_corex_t *corex, fx_inode_t *reg)
{
	int rc, pend = 0;
	fx_iobuf_t  *iobuf;
	fx_iobufs_t *iobufs;

	iobufs = corex->get_iobufs(corex);
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobuf = &iobufs->iob[i];
		if (iobuf->sgm.rng.len == 0) {
			break;
		}
		rc = prepare_write(corex, reg, iobuf);
		if (rc == FX_ERPEND) {
			pend = 1;
		} else if (rc != 0) {
			return rc;
		}
	}
	if (pend) {
		return FX_ERPEND;
	}
	for (size_t j = 0; j < niob(iobufs); ++j) {
		iobuf = &iobufs->iob[j];
		if (iobuf->sgm.rng.len == 0) {
			break;
		}
		rc = write_segmnt(corex, reg, iobuf);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

static int write_pseudo(fx_corex_t *corex, fx_inode_t *inode,
                        fx_off_t off, fx_size_t len)
{
	int rc = 0;
	fx_size_t sz = 0;
	fx_iovec_t iov[1];
	const fx_iobufs_t *iobufs;

	if (!inode->i_meta || !inode->i_meta->save) {
		return -ENOTSUP;
	}
	if ((off != 0) || (len > FNX_BLKSIZE)) {
		return -EINVAL; /* XXX */
	}
	iobufs = corex->get_iobufs(corex);
	rc = fx_iobufs_mkiovec(iobufs, iov, 1, &sz);
	if ((rc != 0) || (sz != 1)) {
		return -EINVAL;
	}
	rc  = inode->i_meta->save(inode, iov[0].iov_base, iov[0].iov_len);
	if (rc != 0) {
		return rc;
	}

	return 0;
}

static void release_data(fx_corex_t *corex)
{
	fx_bkref_t  *bkref;
	fx_iobuf_t  *iobuf;
	fx_iobufs_t *iobufs;
	const fx_brange_t *brange;

	iobufs = corex->get_iobufs(corex);
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobuf  = &iobufs->iob[i];
		brange = &iobuf->sgm.rng;
		if (brange->len == 0) {
			break;
		}
		for (size_t pos, j = 0; j < brange->cnt; ++j) {
			pos   = brange->idx + j;
			bkref = iobuf->bks[pos];
			if (bkref != NULL) {
				fx_assert(bkref->refcnt == 0);
				corex->discard_bk(corex, bkref);
				iobuf->bks[pos] = NULL;
			}
		}
	}
}

int fx_write_data(fx_corex_t *corex, const fx_fileref_t *fref,
                  fx_off_t off, fx_size_t len)
{
	int rc = 0;
	fx_bkcnt_t beg, end, nbk;
	const fx_super_t  *super;
	const fx_fsstat_t *fsstat;

	super  = corex->get_super(corex);
	fsstat = &super->su_fsstat;

	if (inode_ispseudo(fref->f_inode)) {
		rc = write_pseudo(corex, fref->f_inode, off, len);
		release_data(corex);
	} else {
		beg = fsstat->f_bfree;
		rc  = write_reg(corex, fref->f_inode);
		end = fsstat->f_bfree;
		fx_assert(beg >= end);
		nbk = beg - end;
		update_capacity(corex, fref->f_inode, off, len, nbk, FNX_FALSE);
	}
	return rc;
}

int fx_prepare_space(fx_corex_t *corex, fx_bkcnt_t nbk)
{
	int rc = -ENOSPC;
	fx_bkcnt_t rem;
	fx_lba_t ulba, uend;
	fx_super_t  *super;
	fx_fsstat_t *fsstat;
	const fx_extent_t *udata;
	const fx_bkcnt_t spcnbk = FNX_SPCNBK;
	fx_spmap_t *spmap = NULL;

	super  = corex->get_super(corex);
	udata  = &super->su_layout.l_udata;
	fsstat = &super->su_fsstat;

	if (fsstat->f_bavail < nbk) { /* TODO: privileged */
		goto out;
	}

	uend  = lba_end(0, udata->cnt);
	ulba  = fsstat->f_bnext;
	for (fx_bkcnt_t cnt = 0; (cnt < udata->cnt); cnt += spcnbk) {
		if (ulba >= uend) {
			ulba = 0;
		}
		rc = fx_fetch_spmap(corex, ulba, &spmap);
		if (rc != 0) {
			goto out;
		}
		rem = spcnbk - spmap->sp_used;
		if (rem > nbk) {
			goto out;
		}
		nbk -= rem;
		ulba = lba_floor_spseg(ulba + spcnbk);
	}

out:
	return rc;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int falloc_segmnt(fx_corex_t *corex, fx_segmnt_t *segmnt,
                         fx_off_t off, fx_size_t len)

{
	int rc;
	fx_brange_t brange;
	fx_segmap_t *segmap;
	fx_bkaddr_t *bkaddr;
	fx_spmap_t  *spmap = NULL;

	fx_brange_setup(&brange, off, len);
	rc = guarantee_space(corex, brange.cnt, &spmap);
	if (rc != 0) {
		return rc;
	}
	segmap = &segmnt->sg_segmap;
	for (size_t pos, i = 0; i < brange.cnt; ++i) {
		pos     = brange.idx + i;
		bkaddr  = &segmap->bka[pos];
		if (!bkaddr_isnull(bkaddr)) {
			continue;
		}
		rc = acquire_bkaddr(corex, spmap, bkaddr);
		if (rc != 0) {
			return rc;
		}
		corex->put_vnode(corex, &spmap->sp_vnode);
		corex->put_vnode(corex, &segmnt->sg_vnode);
	}
	return 0;
}

static int falloc_slice(fx_corex_t *corex, const fx_inode_t *reg,
                        fx_slice_t *slice, fx_off_t off, fx_size_t len)
{
	int rc;
	fx_size_t slen;
	fx_off_t beg, end, piv;
	fx_segmnt_t *segmnt = NULL;

	beg = off_floor_slice(off);
	end = off_end(off, len);
	fx_assert(off_len(beg, end) <= FNX_SLICESIZE);

	for (; off < end; off = piv) {
		piv = off_ceil_seg(off);
		rc  = yield_ssegmnt(corex, reg, slice, off, &segmnt);
		if (rc != 0) {
			return rc;
		}
		slen = off_min_len(off, piv, end);
		rc   = falloc_segmnt(corex, segmnt, off, slen);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

static int falloc_sliced_range(fx_corex_t *corex, const fx_inode_t *reg,
                               fx_off_t off, fx_size_t len)
{
	int rc;
	fx_size_t slen;
	fx_off_t end, piv;
	fx_slice_t  *slice  = NULL;

	end = off_end(off, len);
	for (; off < end; off = piv) {
		piv  = off_ceil_slice(off);
		slen = off_min_len(off, piv, end);
		rc = yield_slice(corex, reg, off, &slice);
		if (rc != 0) {
			return rc;
		}
		rc = falloc_slice(corex, reg, slice, off, slen);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

static int falloc_segmnt0(fx_corex_t *corex, const fx_inode_t *reg,
                          fx_off_t off, fx_size_t len)
{
	int rc;
	fx_segmnt_t *segmnt = NULL;

	rc = yield_segmnt(corex, reg, off, &segmnt);
	if (rc != 0) {
		return rc;
	}
	rc = falloc_segmnt(corex, segmnt, off, len);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static int falloc_range(fx_corex_t *corex, const fx_inode_t *reg,
                        fx_off_t off, fx_size_t len)
{
	int rc;
	fx_size_t slen;
	fx_off_t end, piv;

	/* Head case: falloc segment-0 which is unmapped by any slice */
	if (off_isseg0(off)) {
		end  = off_end(off, len);
		piv  = off_ceil_seg(off);
		slen = off_min_len(off, piv, end);
		rc = falloc_segmnt0(corex, reg, off, slen);
		if (rc != 0) {
			return rc;
		}
		off = off_end(off, slen);
		len -= slen;
	}
	/* Tail case: falloc segments-1..N which are mapped by slices */
	rc = falloc_sliced_range(corex, reg, off, len);
	return rc;
}

int fx_falloc_data(fx_corex_t *corex, const fx_fileref_t *fref,
                   fx_off_t off, fx_size_t len, fx_bool_t keep_size)
{
	int rc;
	fx_bkcnt_t beg, end, nbk;
	const fx_super_t  *super;
	const fx_fsstat_t *fsstat;

	super  = corex->get_super(corex);
	fsstat = &super->su_fsstat;

	beg = fsstat->f_bfree;
	rc  = falloc_range(corex, fref->f_inode, off, len);
	end = fsstat->f_bfree;
	fx_assert(rc == 0);
	if (rc == 0) {
		fx_assert(beg >= end);
		nbk = (beg - end);
		update_capacity(corex, fref->f_inode, off, len, nbk, keep_size);
	}

	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
retire_bk(fx_corex_t *corex, const fx_bkaddr_t *bkaddr, fx_bkref_t *bkref)
{
	int rc;
	fx_super_t *super;
	fx_spmap_t *spmap = NULL;

	fx_assert(bkaddr->svol == FNX_SVOL_DATA);
	fx_assert(bkaddr->frg == 0);

	super = corex->get_super(corex);

	/* Space-chunk for this block must exist! */
	rc  = fx_fetch_spmap(corex, bkaddr->lba, &spmap);
	if (rc != 0) {
		return rc;
	}
	rc = fx_relinquish_lba(super, spmap, bkaddr->lba);
	fx_assert(rc == 0);
	corex->put_vnode(corex, &spmap->sp_vnode);
	corex->put_super(corex, super, 0);

	/* If this block is currently cached, evict and retire */
	if (bkref == NULL) {
		bkref = corex->cache->search_bk(corex->cache, bkaddr);
	}
	if (bkref != NULL) {
		corex->withdraw_bk(corex, bkref);
	}
	return 0;
}

static int trim_segmnt(fx_corex_t *corex, fx_segmnt_t *segmnt,
                       fx_off_t off, fx_size_t len)
{
	int rc;
	fx_off_t beg, end;
	fx_brange_t brange;
	fx_segmap_t *segmap;
	fx_bkaddr_t *bkaddr;

	beg = off_floor_seg(off);
	end = off_end(off, len);
	fx_brange_setup(&brange, off, len);

	fx_assert(off_len(beg, end) <= FNX_SEGSIZE);
	fx_assert(fx_brange_issub2(&segmnt->sg_segmap.rng, &brange));

	segmap = &segmnt->sg_segmap;
	for (size_t pos, i = 0; i < brange.cnt; ++i) {
		pos     = brange.idx + i;
		bkaddr  = &segmap->bka[pos];
		if (!bkaddr_isnull(bkaddr)) {
			rc = retire_bk(corex, bkaddr, NULL);
			if (rc != 0) {
				fx_assert(rc == 0);
				return rc;
			}
			bkaddr_reset(bkaddr);
			corex->put_vnode(corex, &segmnt->sg_vnode);
		}
	}
	return 0;
}

static int fetch_trim_segmnt(fx_corex_t *corex, const fx_inode_t *reg,
                             fx_off_t off, fx_size_t len, fx_segmnt_t **segmnt)
{
	int rc;

	rc = fetch_segmnt(corex, reg, off, segmnt);
	if (rc == 0) {
		rc = trim_segmnt(corex, *segmnt, off, len);
	}
	return rc;
}

static int trim_slice(fx_corex_t *corex, const fx_inode_t *reg,
                      fx_slice_t *slice, fx_off_t off, fx_size_t len)
{
	int rc, set;
	size_t nsm;
	fx_size_t slen;
	fx_off_t beg, end, piv;
	fx_segmnt_t *segmnt = NULL;

	beg = off_floor_slice(off);
	end = off_end(off, len);
	fx_assert(off_len(beg, end) <= FNX_SLICESIZE);

	for (; off < end; off = piv) {
		piv  = off_ceil_seg(off);
		slen = off_min_len(off, piv, end);
		set  = fx_slice_testseg(slice, off);
		if (!set) {
			continue;
		}
		rc = fetch_trim_segmnt(corex, reg, off, slen, &segmnt);
		if (rc != 0) {
			return rc;
		}
		nsm = fx_segmap_nmaps(&segmnt->sg_segmap);
		if (nsm == 0) {
			fx_slice_unmarkseg(slice, off);
			corex->put_vnode(corex, &slice->sc_vnode);
			rc = fx_reclaim_vnode(corex, &segmnt->sg_vnode);
			fx_assert(rc == 0);
		}
	}
	return 0;
}

static int trim_sliced_range(fx_corex_t *corex, const fx_inode_t *reg,
                             fx_off_t off, fx_size_t len)
{
	int rc;
	fx_size_t slen;
	fx_off_t end, piv;
	fx_slice_t *slice = NULL;

	end = off_end(off, len);
	for (; off < end; off = piv, slice = NULL) {
		piv  = off_ceil_slice(off);
		slen = off_min_len(off, piv, end);
		rc   = fetch_slice(corex, reg, off, &slice);
		if (rc == -ENOENT) {
			continue;
		} else if (rc != 0) {
			return 0;
		}
		rc = trim_slice(corex, reg, slice, off, slen);
		if (rc != 0) {
			return 0;
		}
		if (slice->sc_count == 0) {
			rc = fx_reclaim_vnode(corex, &slice->sc_vnode);
			fx_assert(rc == 0);
		}
	}
	return 0;
}

static int trim_segmnt0(fx_corex_t *corex, const fx_inode_t *reg,
                        fx_off_t off, fx_size_t len)
{
	int rc;
	fx_size_t slen, nsm;
	fx_off_t end, piv;
	fx_segmnt_t *segmnt = NULL;

	end  = off_end(off, len);
	piv  = off_ceil_seg(off);
	slen = off_min_len(off, piv, end);

	rc = fetch_trim_segmnt(corex, reg, off, slen, &segmnt);
	if (rc == -ENOENT) {
		return 0;
	} else if (rc != 0) {
		return rc;
	}
	nsm = fx_segmap_nmaps(&segmnt->sg_segmap);
	if (nsm == 0) {
		rc = fx_reclaim_vnode(corex, &segmnt->sg_vnode);
		fx_assert(rc == 0);
	}
	return 0;
}

static int trim_data(fx_corex_t *corex, const fx_inode_t *reg,
                     fx_off_t off, fx_size_t len)
{
	int rc;
	fx_size_t slen;
	fx_off_t end, piv;

	/* Head case: trim segment-0 which is unmapped by any slice */
	if (off_isseg0(off)) {
		end  = off_end(off, len);
		piv  = off_ceil_seg(off);
		slen = off_min_len(off, piv, end);
		rc = trim_segmnt0(corex, reg, off, slen);
		if (rc != 0) {
			return rc;
		}
		off = off_end(off, slen);
		len -= slen;
	}
	/* Tail case: trim segments-1..N which are mapped by slices */
	rc = trim_sliced_range(corex, reg, off, len);
	return rc;
}

static fx_off_t off_xceil_blk(fx_off_t off)
{
	return off_floor_blk(off_end(off, FNX_BLKSIZE - 1));
}

int fx_punch_data(fx_corex_t *corex, fx_inode_t *reg,
                  fx_off_t off, fx_size_t len)
{
	int rc = 0;
	fx_size_t slen;
	fx_bkcnt_t beg, end, nbk;
	fx_off_t vsize, bsize, size, send, dend, boff, bend;
	const fx_super_t  *super;
	const fx_fsstat_t *fsstat;

	super  = corex->get_super(corex);
	fsstat = &super->su_fsstat;

	dend  = off_end(off, len);
	vsize = inode_getsize(reg);
	bsize = inode_getbsize(reg);
	size  = off_max(bsize, vsize);
	if (off >= size) {
		return 0; /* Punch past end-of-data; no-op */
	}

	send = off_min(size, dend);
	bend = off_floor_blk(send);
	boff = off_xceil_blk(off);
	slen = off_len(boff, bend);
	beg  = fsstat->f_bfree;
	rc   = trim_data(corex, reg, boff, slen);
	end  = fsstat->f_bfree;
	fx_assert(rc == 0);
	fx_assert(end >= beg);

	nbk = (end - beg);
	fx_assert(reg->i_iattr.i_nblk >= nbk);
	reg->i_iattr.i_nblk -= nbk;

	if (bend <= bsize) {
		inode_setbsize(reg, boff);
	}
	corex->put_inode(corex, reg, FNX_ACTIME_NOW);
	return 0;
}

int fx_trunc_data(fx_corex_t *corex, fx_inode_t *reg, fx_off_t off)
{
	int rc = 0;
	fx_size_t len;
	fx_off_t vsize, bsize, size;

	if (inode_ispseudo(reg)) {
		corex->put_inode(corex, reg, FNX_ACTIME_NOW);
		return 0;
	}

	vsize = inode_getsize(reg);
	bsize = inode_getbsize(reg);
	size  = off_max(bsize, vsize);
	if (size != off) {
		if (off < size) {
			len = off_len(off, size + FNX_BLKSIZE);
			rc  = fx_punch_data(corex, reg, off, len);
			fx_assert(rc == 0);
		}
		inode_setsize(reg, off);
		corex->put_inode(corex, reg, FNX_ACTIME_NOW);
	}
	return rc;
}

int fx_update_data(fx_corex_t *corex, fx_inode_t *reg, fx_flags_t o_flags)
{
	int rc = 0;
	fx_off_t size;
	fx_flags_t mask;

	mask = O_TRUNC;
	if (!(o_flags & mask)) {
		return 0;  /* OK, no trunc upon open */
	}
	mask = (O_RDWR | O_WRONLY);
	if (!(o_flags & mask)) {
		return 0; /* O_TRUNC requires O_RDWR or O_WRONLY */
	}
	size = inode_getsize(reg);
	if (size == 0) {
		return 0;  /* Nothing to do; already zero size */
	}
	rc = fx_trunc_data(corex, reg, 0);
	if (rc != 0) {
		return rc; /* Err */
	}
	corex->put_inode(corex, reg, FNX_AMCTIME_NOW);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void unroll_iobuf(fx_iobuf_t *iobuf)
{
	fx_bkref_t  *bkref;
	fx_bkaddr_t *bkaddr;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos    = brange->idx + i;
		bkref  = iobuf->bks[pos];
		bkaddr = &iobuf->sgm.bka[pos];
		if (bkref != NULL) {
			fx_assert(bkref->refcnt > 0);
			bkref->refcnt -= 1;
			bkaddr_reset(bkaddr);
			iobuf->bks[pos] = NULL;
		}
	}
}

static void unroll_iobufs(fx_corex_t *corex)
{
	fx_iobuf_t *iobuf;
	fx_iobufs_t *iobufs;

	iobufs = corex->get_iobufs(corex);
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobuf = &iobufs->iob[i];
		if (iobuf->sgm.rng.len == 0) {
			break;
		}
		unroll_iobuf(iobuf);
	}
}

static int stage_bks(fx_corex_t *corex, fx_segmnt_t *segmnt, fx_iobuf_t *iobuf)
{
	int rc;
	fx_bkref_t  *bkref;
	fx_bkaddr_t *bkaddr2;
	const fx_brange_t *brange;
	const fx_bkaddr_t *bkaddr;
	const fx_segmap_t *segmap;

	brange = &iobuf->sgm.rng;
	segmap = &segmnt->sg_segmap;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos     = brange->idx + i;
		bkaddr  = &segmap->bka[pos];
		bkaddr2 = &iobuf->sgm.bka[pos];

		if (!bkaddr_isnull(bkaddr)) {
			rc = corex->fetch_bk(corex, bkaddr, &bkref);
			if (rc != 0) {
				return rc;
			}
			iobuf->bks[pos] = bkref;
			bkref->refcnt += 1;
			bkaddr_copy(bkaddr2, bkaddr);
		}
	}
	return 0;
}

static int read_segmnt(fx_corex_t *corex,
                       const fx_inode_t *reg, fx_iobuf_t *iobuf)
{
	int rc;
	fx_off_t off;
	fx_segmnt_t *segmnt = NULL;

	off = iobuf->sgm.rng.off;
	rc  = fetch_segmnt(corex, reg, off, &segmnt);
	if (rc == -ENOENT) {
		return 0; /* No-mapping -- OK */
	}
	if (rc != 0) {
		return rc;
	}
	rc = stage_bks(corex, segmnt, iobuf);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static void update_read(fx_corex_t *corex, const fx_fileref_t *fref)
{
	fx_mntf_t mntf;
	fx_inode_t *inode;

	mntf  = corex->fscore->mntf;
	inode = fref->f_inode;

	if (fref->f_noatime) {
		return;
	}
	if (!(mntf & FNX_MNTF_NOATIME)) {
		corex->put_inode(corex, inode, FNX_ATIME_NOW); /* XXX */
	} else {
		corex->put_inode(corex, inode, FNX_ATIME_NOW);
	}
}

static int read_reg(fx_corex_t *corex, fx_inode_t *reg)
{
	int rc = 0;
	fx_iobuf_t  *iobuf;
	fx_iobufs_t *iobufs;

	iobufs = corex->get_iobufs(corex);
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobuf = &iobufs->iob[i];
		if (iobuf->sgm.rng.len == 0) {
			break;
		}
		rc = read_segmnt(corex, reg, iobuf);
		if (rc != 0) {
			break;
		}
	}
	return rc;
}

static void increfs(fx_iobufs_t *iobufs)
{
	fx_bkref_t *bkref;
	fx_iobuf_t *iobuf;

	for (size_t i = 0; i < FX_NELEMS(iobufs->iob); ++i) {
		iobuf = &iobufs->iob[i];
		for (size_t pos, j = 0; j < iobuf->sgm.rng.cnt; ++j) {
			pos = iobuf->sgm.rng.idx + j;
			bkref = iobuf->bks[pos];
			if (bkref != NULL) {
				bkref->refcnt += 1;
			}
		}
	}
}

static int read_pseudo(fx_corex_t *corex, fx_inode_t *inode,
                       fx_off_t off, fx_size_t len)
{
	int rc = 0;
	fx_size_t sz = 0;
	fx_blk_t *blk;
	fx_iobufs_t *iobufs;

	if (!inode->i_meta || !inode->i_meta->show) {
		return -ENOTSUP;
	}

	blk = &corex->pblk;
	rc  = inode->i_meta->show(inode, blk, sizeof(*blk), &sz);
	if ((rc != 0) || (off >= (fx_off_t)sz)) {
		return rc;
	}
	len = off_min_len(off, off_end(0, sz), off_end(off, len));
	iobufs = corex->get_iobufs(corex);
	rc = fx_iobufs_assign(iobufs, off, len, &blk[off]);
	if (rc != 0) {
		return -EINVAL;
	}
	increfs(iobufs);

	return 0;
}

int fx_read_data(fx_corex_t *corex,
                 const fx_fileref_t *fref, fx_off_t off, fx_size_t len)
{
	int rc = 0;
	fx_off_t size;
	fx_iobufs_t *iobufs;
	fx_inode_t  *regfile;

	regfile = fref->f_inode;
	if (inode_ispseudo(regfile)) {
		rc = read_pseudo(corex, regfile, off, len);
		if (rc != 0) {
			return rc;
		}
	} else {
		size = inode_getsize(regfile);
		if (off < size) {
			len = off_min_len(off, size, off_end(off, len));
			iobufs = corex->get_iobufs(corex);
			rc = fx_iobufs_assign(iobufs, off, len, NULL);
			if (rc != 0) {
				return -EINVAL;
			}
			rc = read_reg(corex, regfile);
			if (rc != 0) {
				fx_assert(rc != FX_EWPEND);
				if (rc == FX_ERPEND) {
					unroll_iobufs(corex);
				}
				return rc;
			}
		}
	}

	update_read(corex, fref);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void liberate_bks(fx_corex_t *corex, fx_iobuf_t *iobuf)
{
	fx_bkref_t *bkref;
	const fx_brange_t *brange;

	brange = &iobuf->sgm.rng;
	for (size_t pos, i = 0; i < brange->cnt; ++i) {
		pos   = brange->idx + i;
		bkref = iobuf->bks[pos];
		if (bkref != NULL) {
			fx_assert(bkref->refcnt > 0);
			bkref->refcnt -= 1;
			corex->discard_bk(corex, bkref);
			iobuf->bks[pos] = NULL;
		}
	}
}

int fx_relax_data(fx_corex_t *corex)
{
	fx_iobuf_t  *iobuf;
	fx_iobufs_t *iobufs;

	iobufs = corex->get_iobufs(corex);
	for (size_t i = 0; i < niob(iobufs); ++i) {
		iobuf = &iobufs->iob[i];
		if (iobuf->sgm.rng.len == 0) {
			break;
		}
		liberate_bks(corex, iobuf);
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_read_symlnk(fx_corex_t *corex, fx_symlnk_t *symlnk, fx_path_t *path)
{
	fx_path_clone(path, &symlnk->sl_value);
	corex->put_inode(corex, &symlnk->sl_inode, FNX_ATIME_NOW);

	return 0;
}


int fx_xcopy_data(fx_corex_t *corex,
                  const fx_fileref_t *tgt_fref,
                  const fx_fileref_t *src_fref,
                  fx_off_t off, fx_size_t len)
{
	/* TODO: FIXME */
	fx_unused(corex);
	fx_unused(tgt_fref);
	fx_unused(src_fref);
	fx_unused(off);
	fx_unused(len);
	return -ENOTSUP;
}



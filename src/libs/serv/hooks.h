/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_HOOKS_H_
#define FUNEX_HOOKS_H_



int fx_let_access(fx_corex_t *, const fx_inode_t *, fx_mode_t);

int fx_let_lookup(fx_corex_t *, const fx_dir_t *);

int fx_let_open(fx_corex_t *, const fx_inode_t *, fx_flags_t);

int fx_let_fsync(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_flush(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_fsyncdir(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_release(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_releasedir(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_fquery(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_opendir(fx_corex_t *, const fx_dir_t *);

int fx_let_readdir(fx_corex_t *, const fx_fileref_t *, fx_ino_t, fx_off_t);

int fx_let_rmdir(fx_corex_t *, const fx_dir_t *, const fx_dir_t *);

int fx_let_nolink(fx_corex_t *, const fx_dir_t *, const fx_name_t *);

int fx_let_symlink(fx_corex_t *, const fx_dir_t *, const fx_name_t *);

int fx_let_mknod(fx_corex_t *, const fx_dir_t *, const fx_name_t *);

int fx_let_mkdir(fx_corex_t *, const fx_dir_t *, const fx_name_t *);

int fx_let_unlink(fx_corex_t *, const fx_dir_t *, const fx_inode_t *);

int fx_let_link(fx_corex_t *, const fx_dir_t *,
                const fx_inode_t *, const fx_name_t *);

int fx_let_fallocate(fx_corex_t *, const fx_fileref_t *,
                     fx_ino_t, fx_off_t, fx_size_t);

int fx_let_write(fx_corex_t *, const fx_fileref_t *,
                 fx_ino_t, fx_off_t, fx_size_t);

int fx_let_read(fx_corex_t *, const fx_fileref_t *,
                fx_ino_t, fx_off_t, fx_size_t);

int fx_let_fpunch(fx_corex_t *, const fx_fileref_t *,
                  fx_ino_t, fx_off_t, fx_size_t);

int fx_let_setsize(fx_corex_t *, const fx_inode_t *, fx_off_t);

int fx_let_setattr(fx_corex_t *, const fx_inode_t *, fx_flags_t,
                   fx_mode_t, fx_uid_t, fx_gid_t, fx_off_t);

int fx_let_create(fx_corex_t *, const fx_dir_t *,
                  const fx_name_t *, fx_mode_t);

int fx_let_rename_src(fx_corex_t *, const fx_dir_t *,
                      const fx_dir_t *, const fx_inode_t *);

int fx_let_rename_tgt(fx_corex_t *, const fx_dir_t *, const fx_inode_t *);


int fx_let_xcopy_tgt(fx_corex_t *, const fx_fileref_t *, fx_ino_t);

int fx_let_xcopy_src(fx_corex_t *, const fx_fileref_t *,
                     fx_ino_t, fx_off_t, fx_size_t);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_lookup_iinode(fx_corex_t *, const fx_dir_t *,
                     const fx_name_t *, fx_hash_t, fx_inode_t **);

int fx_lookup_ientry(fx_corex_t *, const fx_dir_t *,
                     const fx_name_t *, fx_hash_t, fx_inode_t **);

int fx_lookup_hinode(fx_corex_t *, const fx_dir_t *,
                     const fx_name_t *, fx_hash_t, fx_inode_t **);

int fx_lookup_inode(fx_corex_t *, const fx_dir_t *,
                    const fx_name_t *, fx_hash_t, fx_inode_t **);

int fx_lookup_link(fx_corex_t *, const fx_dir_t *,
                   const fx_name_t *, fx_hash_t, fx_inode_t **);

int fx_lookup_dir(fx_corex_t *, const fx_dir_t *,
                  const fx_name_t *, fx_hash_t, fx_dir_t **);


int fx_link_child(fx_corex_t *, fx_dir_t *,
                  const fx_name_t *, fx_hash_t, fx_inode_t *);

int fx_unlink_child(fx_corex_t *, fx_dir_t *, fx_inode_t *);

int fx_fix_unlinked(fx_corex_t *, fx_inode_t *);

int fx_search_dent(fx_corex_t *, const fx_dir_t *, fx_off_t,
                   fx_name_t **, fx_inode_t **, fx_off_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_fetch_vnode(fx_corex_t *, const fx_vaddr_t *, fx_vnode_t **);

int fx_fetch_inode(fx_corex_t *, fx_ino_t, fx_inode_t **);

int fx_fetch_super(fx_corex_t *, fx_super_t **);

int fx_fetch_wsuper(fx_corex_t *, fx_super_t **);

int fx_fetch_spmap(fx_corex_t *, fx_lba_t, fx_spmap_t **);

int fx_fetch_dir(fx_corex_t *, fx_ino_t, fx_dir_t **);

int fx_fetch_symlnk(fx_corex_t *, fx_ino_t, fx_symlnk_t **);

int fx_fetch_wreg(fx_corex_t *,  const fx_fileref_t *, fx_ino_t, fx_inode_t **);


int fx_acquire_vnode(fx_corex_t *, const fx_vaddr_t *, fx_vnode_t **);

int fx_acquire_special(fx_corex_t *, fx_mode_t, fx_inode_t **);

int fx_acquire_symlnk(fx_corex_t *, const fx_path_t *, fx_symlnk_t **);

int fx_acquire_reflnk(fx_corex_t *, fx_inode_t *, fx_inode_t **);

int fx_acquire_dir(fx_corex_t *, fx_mode_t, fx_dir_t **);

int fx_acquire_reg(fx_corex_t *, fx_mode_t, fx_inode_t **);


int fx_reclaim_vnode(fx_corex_t *, fx_vnode_t *);

int fx_reclaim_inode(fx_corex_t *, fx_inode_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_attach_fref(fx_corex_t *, fx_inode_t *, fx_flags_t, fx_fileref_t **);

int fx_detach_fref(fx_corex_t *, fx_fileref_t *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_setattr_stat(fx_corex_t *, fx_inode_t *, fx_flags_t, fx_mode_t,
                    fx_uid_t, fx_gid_t, fx_off_t, const fx_times_t *);

int fx_setattr_size(fx_corex_t *, fx_inode_t *, fx_off_t);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


int fx_relax_data(fx_corex_t *);

int fx_prepare_space(fx_corex_t *, fx_bkcnt_t);

int fx_write_data(fx_corex_t *, const fx_fileref_t *, fx_off_t, fx_size_t);

int fx_read_data(fx_corex_t *, const fx_fileref_t *, fx_off_t, fx_size_t);

int fx_read_symlnk(fx_corex_t *, fx_symlnk_t *, fx_path_t *);

int fx_punch_data(fx_corex_t *, fx_inode_t *, fx_off_t, fx_size_t);

int fx_trunc_data(fx_corex_t *, fx_inode_t *, fx_off_t);

int fx_update_data(fx_corex_t *, fx_inode_t *, fx_flags_t);

int fx_falloc_data(fx_corex_t *, const fx_fileref_t *,
                   fx_off_t off, fx_size_t, fx_bool_t);

int fx_xcopy_data(fx_corex_t *, const fx_fileref_t *,
                  const fx_fileref_t *, fx_off_t, fx_size_t);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_destage_dirty(fx_corex_t *, fx_size_t);

int fx_destage_dirty_of(fx_corex_t *, fx_ino_t, fx_bool_t);

int fx_destage_udirty(fx_corex_t *, fx_ino_t, fx_size_t);

int fx_destage_sdirty(fx_corex_t *, fx_size_t);

int fx_destage_vdirty(fx_corex_t *, fx_size_t);


void fx_squeeze_vlru(fx_corex_t *, fx_size_t);

void fx_squeeze_ulru(fx_corex_t *, fx_size_t);

void fx_squeeze_slru(fx_corex_t *, fx_size_t);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_preprocess_task(fx_task_t *);

int fx_read_data_bk(fx_fscore_t *, const fx_bkref_t *);

int fx_write_data_bk(fx_fscore_t *, const fx_bkref_t *);

int fx_write_meta_bk(fx_fscore_t *, const fx_bkref_t *);

int fx_read_meta_bk(fx_fscore_t *, const fx_bkref_t *);


#endif /* FUNEX_HOOKS_H_ */

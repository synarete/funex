/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                         Funex File-System Library                           *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  The Funex file-system library is a free software; you can redistribute it  *
 *  and/or modify it under the terms of the GNU Lesser General Public License  *
 *  as published by the Free Software Foundation; either version 3 of the      *
 *  License, or (at your option) any later version.                            *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "fnxinfra.h"
#include "fnxcore.h"
#include "cache.h"

#define CACHE_MAGIC         (0x7CAC11E)
#define NOTCACHED           0
#define CACHED              1

/* Locals */
static void cache_bind_ops(fx_cache_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int isitype(fx_vtype_e vtype)
{
	return ((vtype == FNX_VTYPE_ANY) || fx_isitype(vtype));
}

static int isequal(const fx_vaddr_t *vaddr1, const fx_vaddr_t *vaddr2)
{
	int eq;

	if (isitype(vaddr1->vtype) || isitype(vaddr2->vtype)) {
		eq = (vaddr1->ino == vaddr2->ino);
	} else {
		eq = vaddr_isequal(vaddr1, vaddr2);
	}
	return eq;
}

static unsigned long vaddr_hkey(const fx_vaddr_t *vaddr)
{
	unsigned long hkey, base, vino, vxno;

	if (isitype(vaddr->vtype)) {
		hkey = (unsigned long)(vaddr->ino);
	} else {
		base = (unsigned long)(vaddr->vtype) * 12582917UL;
		vino = (unsigned long)(vaddr->ino);
		vxno = (unsigned long)(vaddr->xno);
		hkey = fx_rotleft(base, 17) ^ vino ^ ~vxno;
	}
	return hkey;
}

static void vihtbl_init(fx_vihtbl_t *hmap)
{
	fx_bzero(hmap, sizeof(*hmap));
	hmap->size  = 0;
}

static void vihtbl_destroy(fx_vihtbl_t *hmap)
{
	fx_bzero(hmap, sizeof(*hmap));
}

static size_t
vihtbl_gethpos(const fx_vihtbl_t *hmap, const fx_vaddr_t *vaddr)
{
	unsigned long hkey, hpos;

	hkey = vaddr_hkey(vaddr);
	hpos = hkey % FX_NELEMS(hmap->htbl);
	return hpos;
}

static fx_vnode_t *
vihtbl_lookup(const fx_vihtbl_t *hmap, const fx_vaddr_t *vaddr)
{
	size_t hpos;
	const fx_vnode_t *vnode;
	fx_vnode_t *const *hlnk;

	hpos = vihtbl_gethpos(hmap, vaddr);
	hlnk = &hmap->htbl[hpos];
	while ((vnode = *hlnk) != NULL) {
		if (isequal(&vnode->v_vaddr, vaddr)) {
			break;
		}
		hlnk = &vnode->v_hlnk;
	}
	return (fx_vnode_t *)vnode;
}

static void vihtbl_insert(fx_vihtbl_t *hmap, fx_vnode_t *vnode)
{
	size_t hpos;
	fx_vnode_t **hlnk;

	hpos = vihtbl_gethpos(hmap, &vnode->v_vaddr);
	hlnk = &hmap->htbl[hpos];
	vnode->v_hlnk = *hlnk;
	*hlnk = vnode;
	hmap->size += 1;
}

static void vihtbl_remove(fx_vihtbl_t *hmap, fx_vnode_t *vnode)
{
	size_t hpos;
	fx_vnode_t **hlnk;

	hpos = vihtbl_gethpos(hmap, &vnode->v_vaddr);
	hlnk = &hmap->htbl[hpos];
	while (*hlnk != vnode) {
		hlnk = &(*hlnk)->v_hlnk;
	}
	*hlnk = vnode->v_hlnk;
	vnode->v_hlnk = NULL;
	hmap->size -= 1;
}

static void append_vnode(fx_list_t *list, fx_vnode_t *vnode)
{
	fx_list_push_back(list, &vnode->v_clink);
}

static void vihtbl_remove_all(fx_vihtbl_t *hmap, fx_list_t *list)
{
	fx_vnode_t *vnode, **hlnk;

	for (size_t i = 0; i < FX_NELEMS(hmap->htbl); ++i) {
		hlnk = &hmap->htbl[i];
		while ((vnode = *hlnk) != NULL) {
			*hlnk = NULL;
			hlnk = &vnode->v_hlnk;
			append_vnode(list, vnode);
		}
	}
	hmap->size = 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_bkref_t *clink_to_bkref(const fx_link_t *lnk)
{
	return fx_container_of(lnk, fx_bkref_t, clink);
}

static void append_bkref(fx_list_t *list, fx_bkref_t *bkref)
{
	fx_list_push_back(list, &bkref->clink);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static unsigned long bkaddr_hkey(const fx_bkaddr_t *bkaddr)
{
	unsigned long lba, tmp;

	lba = ((unsigned long)bkaddr->lba);
	tmp = lba * 2654435761ULL;

	return (~lba ^ tmp) + lba;
}

static void bkhtbl_init(fx_bkhtbl_t *bkhtbl)
{
	fx_bzero(bkhtbl, sizeof(*bkhtbl));
	bkhtbl->size  = 0;
}

static void bkhtbl_destroy(fx_bkhtbl_t *bkhtbl)
{
	fx_bzero(bkhtbl, sizeof(*bkhtbl));
}

static size_t
bkhtbl_gethpos(const fx_bkhtbl_t *bkhtbl, const fx_bkaddr_t *bkaddr)
{
	unsigned long hkey, hpos;

	hkey = bkaddr_hkey(bkaddr);
	hpos = hkey % FX_NELEMS(bkhtbl->htbl);
	return hpos;
}

static fx_bkref_t *
bkhtbl_lookup(const fx_bkhtbl_t *bkhtbl, const fx_bkaddr_t *bkaddr)
{
	size_t hpos;
	fx_bkref_t *bkref;
	fx_bkref_t *const *hlnk;

	hpos = bkhtbl_gethpos(bkhtbl, bkaddr);
	hlnk = &bkhtbl->htbl[hpos];
	while ((bkref = *hlnk) != NULL) {
		if (bkaddr_isequal(&bkref->addr, bkaddr)) {
			break;
		}
		hlnk = &bkref->hlnk;
	}
	return bkref;
}

static void bkhtbl_insert(fx_bkhtbl_t *bkhtbl, fx_bkref_t *bkref)
{
	size_t hpos;
	fx_bkref_t **hlnk;

	hpos = bkhtbl_gethpos(bkhtbl, &bkref->addr);
	hlnk = &bkhtbl->htbl[hpos];
	bkref->hlnk = *hlnk;
	*hlnk = bkref;
	bkhtbl->size += 1;
}

static void bkhtbl_remove(fx_bkhtbl_t *bkhtbl, fx_bkref_t *bkref)
{
	size_t hpos;
	fx_bkref_t **hlnk;

	fx_assert(bkhtbl->size > 0);

	hpos = bkhtbl_gethpos(bkhtbl, &bkref->addr);
	hlnk = &bkhtbl->htbl[hpos];
	while (*hlnk != bkref) {
		fx_assert(*hlnk != NULL);
		hlnk = &(*hlnk)->hlnk;
	}
	*hlnk = bkref->hlnk;
	bkref->hlnk = NULL;
	bkhtbl->size -= 1;
}

static void bkhtbl_remove_all(fx_bkhtbl_t *bkhtbl, fx_list_t *list)
{
	fx_bkref_t *bkref, **hlnk;

	for (size_t i = 0; i < FX_NELEMS(bkhtbl->htbl); ++i) {
		hlnk = &bkhtbl->htbl[i];
		while ((bkref = *hlnk) != NULL) {
			*hlnk = NULL;
			hlnk = &bkref->hlnk;
			append_bkref(list, bkref);
		}
	}
	bkhtbl->size = 0;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void dehtbl_init(fx_dehtbl_t *dehtbl)
{
	fx_bzero(dehtbl, sizeof(*dehtbl));
}

static void dehtbl_destroy(fx_dehtbl_t *dehtbl)
{
	fx_bff(dehtbl, sizeof(*dehtbl));
}

static fx_hash_t dehash(fx_ino_t dino, fx_hash_t hash, fx_size_t nlen)
{
	fx_hash_t hval = 0;

	hval = hash ^ ~(fx_hash_t)dino;
	hval ^= (fx_hash_t)((nlen * 8648737) << 17);

	return hval;
}

static fx_dentry_t *
dehtbl_gethent(const fx_dehtbl_t *dehtbl, fx_ino_t dino,
               fx_hash_t hash, fx_size_t nlen)
{
	size_t hpos;
	fx_hash_t hval;
	const fx_dentry_t *dent;

	hval = dehash(dino, hash, nlen);
	hpos = hval % FX_NELEMS(dehtbl->htbl);
	dent = &dehtbl->htbl[hpos];

	return (fx_dentry_t *)dent;
}

static void
dehtbl_remap(fx_dehtbl_t *dehtbl, fx_ino_t dino,
             fx_hash_t hash, fx_size_t nlen, fx_ino_t ino)
{
	fx_dentry_t *dent;

	dent = dehtbl_gethent(dehtbl, dino, hash, nlen);
	dent->dino = dino;
	dent->hash = hash;
	dent->nlen = nlen;
	dent->ino  = ino;   /* New|Override */
}

static const fx_dentry_t *
dehtbl_lookup(const fx_dehtbl_t *dehtbl, fx_ino_t dino,
              fx_hash_t hash, fx_size_t nlen)
{
	fx_dentry_t *dent, *dent2 = NULL;

	dent = dehtbl_gethent(dehtbl, dino, hash, nlen);
	if ((dent->ino != FNX_INO_NULL) &&
	    (dent->hash == hash) &&
	    (dent->dino == dino) &&
	    (dent->nlen == nlen)) {
		dent2 = dent;
	}
	return dent2;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_bkhtbl_t *
cache_bkhtbl_of(const fx_cache_t *cache, const fx_bkaddr_t *bkaddr)
{
	const fx_bkhtbl_t *bkhtbl = NULL;

	if (bkaddr->svol == FNX_SVOL_META) {
		bkhtbl = &cache->sbhtbl;
	} else if (bkaddr->svol == FNX_SVOL_DATA) {
		bkhtbl = &cache->ubhtbl;
	} else {
		fx_panic("illegal-bkaddr: svol=%d lba=%lu", bkaddr->svol, bkaddr->lba);
	}
	return (fx_bkhtbl_t *)bkhtbl;
}

static fx_bkref_t *cache_new_sec(fx_cache_t *cache, const fx_bkaddr_t *bkaddr)
{
	return fx_balloc_new_sec(cache->balloc, bkaddr);
}

static fx_bkref_t *cache_new_bk(fx_cache_t *cache, const fx_bkaddr_t *bkaddr)
{
	return fx_balloc_new_bk(cache->balloc, bkaddr);
}

static void cache_del_bk(fx_cache_t *cache, fx_bkref_t *bkref)
{
	fx_balloc_del_bk(cache->balloc, bkref);
}

static void cache_store_bk(fx_cache_t *cache, fx_bkref_t *bkref)
{
	fx_bkhtbl_t *bkhtbl;

	fx_assert(!(bkref->flags & FX_BKREF_CACHED));
	bkhtbl = cache_bkhtbl_of(cache, &bkref->addr);
	bkhtbl_insert(bkhtbl, bkref);
	bkref->flags |= FX_BKREF_CACHED;
}

static fx_bkref_t *
cache_search_bk(const fx_cache_t *cache, const fx_bkaddr_t *bkaddr)
{
	fx_bkref_t *bkref;
	const fx_bkhtbl_t *bkhtbl;

	bkhtbl = cache_bkhtbl_of(cache, bkaddr);
	bkref  = bkhtbl_lookup(bkhtbl, bkaddr);
	return bkref;
}

static void cache_evict_bk(fx_cache_t *cache, fx_bkref_t *bkref)
{
	fx_bkhtbl_t *bkhtbl;

	fx_assert(bkref->flags & FX_BKREF_CACHED);
	bkhtbl = cache_bkhtbl_of(cache, &bkref->addr);
	bkhtbl_remove(bkhtbl, bkref);
	bkref->flags &= ~FX_BKREF_CACHED;
}

static void transfer(fx_link_t *lnk, fx_list_t *from, fx_list_t *to)
{
	if (from != NULL) {
		fx_assert(from->size > 0);
		fx_list_remove(from, lnk);
	}
	if (to != NULL) {
		fx_list_push_back(to, lnk);
	}
}

static fx_list_t *
cache_list_by_stain(fx_cache_t *cache, fx_vtype_e vtype, fx_stain_e stain)
{
	fx_list_t *list = NULL;

	if (stain == FX_STAIN_LRU) {
		if (vtype == FNX_VTYPE_BK) {
			list = &cache->ulru;
		} else if (vtype == FNX_VTYPE_SEC) {
			list = &cache->slru;
		} else {
			list = &cache->vlru;
		}
	} else if (stain == FX_STAIN_DIRTY) {
		if (vtype == FNX_VTYPE_BK) {
			list = &cache->udirty;
		} else if (vtype == FNX_VTYPE_SEC) {
			list = &cache->sdirty;
		} else {
			list = &cache->vdirty;
		}
	}
	return list;
}

static void
cache_stain_bk(fx_cache_t *cache, fx_bkref_t *bkref, fx_stain_e stain)
{
	fx_vtype_e vtype;
	fx_list_t *from = NULL, *to = NULL;

	vtype = bkref->vtype;
	from  = cache_list_by_stain(cache, vtype, bkref->stain);
	to    = cache_list_by_stain(cache, vtype, stain);
	transfer(&bkref->clink, from, to);
	bkref->stain = stain;
}

static fx_bkref_t *
cache_undirty_bks(fx_cache_t *cache, fx_ino_t ino, fx_size_t lim)
{
	fx_size_t cnt = 0;
	fx_link_t  *lnk, *itr, *end;
	fx_list_t  *lst;
	fx_bkref_t *bkref = NULL;
	fx_bkref_t *bklst = NULL;

	lst = &cache->udirty;
	itr = fx_list_begin(lst);
	end = fx_list_end(lst);
	while ((cnt < lim) && (itr != end)) {
		lnk = itr;
		itr = fx_list_next(lst, lnk);

		bkref = clink_to_bkref(lnk);
		if ((ino == FNX_INO_ANY) || (bkref->ino == ino)) {
			fx_list_remove(lst, lnk);
			bkref->stain = FX_STAIN_NONE;

			bkref->next = bklst;
			bklst = bkref;
			cnt++;
		}
	}
	return bklst;
}

static fx_bkref_t *cache_undirty_sec(fx_cache_t *cache)
{
	fx_link_t  *lnk;
	fx_bkref_t *bkref = NULL;

	lnk = fx_list_pop_front(&cache->sdirty);
	if (lnk != NULL) {
		bkref = clink_to_bkref(lnk);
		bkref->stain = FX_STAIN_NONE;
	}
	return bkref;
}

static fx_bkref_t *cache_getlru_bk(fx_cache_t *cache)
{
	fx_link_t  *lnk;
	fx_bkref_t *bkref = NULL;

	lnk = fx_list_pop_front(&cache->ulru);
	if (lnk != NULL) {
		bkref = clink_to_bkref(lnk);
		bkref->stain = FX_STAIN_NONE;
	}
	return bkref;
}

static fx_bkref_t *cache_getlru_sec(fx_cache_t *cache)
{
	fx_link_t  *lnk;
	fx_bkref_t *bkref = NULL;

	lnk = fx_list_pop_front(&cache->slru);
	if (lnk != NULL) {
		bkref = clink_to_bkref(lnk);
		bkref->stain = FX_STAIN_NONE;
	}
	return bkref;
}

static void clear_bkq(fx_list_t *bkq)
{
	fx_link_t  *lnk;
	fx_bkref_t *bkref;

	while ((lnk = fx_list_pop_front(bkq)) != NULL) {
		bkref = clink_to_bkref(lnk);
		bkref->stain = FX_STAIN_NONE;
	}
}

static void cache_clear_bks(fx_cache_t *cache)
{
	fx_list_t list;
	fx_link_t  *lnk;
	fx_bkref_t *bkref;

	clear_bkq(&cache->udirty);
	clear_bkq(&cache->sdirty);
	clear_bkq(&cache->ulru);
	clear_bkq(&cache->slru);

	fx_list_init(&list);
	bkhtbl_remove_all(&cache->sbhtbl, &list);
	bkhtbl_remove_all(&cache->ubhtbl, &list);

	lnk = fx_list_pop_front(&list);
	while (lnk != NULL) {
		bkref = clink_to_bkref(lnk);
		bkref->flags = 0;
		cache_del_bk(cache, bkref);
		lnk = fx_list_pop_front(&list);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_vnode_t *clink_to_vnode(const fx_link_t *lnk)
{
	return fx_container_of(lnk, fx_vnode_t, v_clink);
}

static void cache_check(const fx_cache_t *cache)
{
	fx_assert(cache->magic == CACHE_MAGIC);
}

static void
cache_check_vnode(const fx_cache_t *cache,
                  const fx_vnode_t *vnode, int cached)
{
	fx_assert(cache->magic == CACHE_MAGIC);
	fx_assert(vnode->v_cached == cached);
}

static fx_vnode_t *cache_new_vnode(fx_cache_t *cache, fx_vtype_e vtype)
{
	fx_vnode_t *vnode;

	vnode = fx_new_vobj(vtype, &cache->valloc->alloc);
	if (vnode == NULL) {
		fx_error("no-new-vnode vtype=%d", vtype);
	}
	fx_assert(vnode != NULL); /* XXX */
	return vnode;
}

static void cache_del_vnode(fx_cache_t *cache, fx_vnode_t *vnode)
{
	fx_del_vobj(vnode, &cache->valloc->alloc);
}

static void cache_clear_vnodes(fx_cache_t *cache)
{
	fx_link_t *lnk;
	fx_vnode_t *vnode;
	fx_list_t list;

	fx_list_clear(&cache->vdirty);
	fx_list_clear(&cache->vlru);

	fx_list_init(&list);
	vihtbl_remove_all(&cache->vhtbl, &list);
	vihtbl_remove_all(&cache->ihtbl, &list);

	while ((lnk = fx_list_pop_front(&list)) != NULL) {
		vnode = clink_to_vnode(lnk);
		vnode->v_cached = FNX_FALSE;
		cache_del_vnode(cache, vnode);
	}
}

static void cache_store_vnode(fx_cache_t *cache, fx_vnode_t *vnode)
{
	cache_check_vnode(cache, vnode, NOTCACHED);

	if (vnode_isinode(vnode)) {
		vihtbl_insert(&cache->ihtbl, vnode);
	} else {
		vihtbl_insert(&cache->vhtbl, vnode);
	}
	vnode->v_cached = FNX_TRUE;
}

static fx_vnode_t *
cache_search_vnode(const fx_cache_t *cache, const fx_vaddr_t *vaddr)
{
	fx_vnode_t *vnode;

	cache_check(cache);
	if (isitype(vaddr->vtype)) {
		vnode = vihtbl_lookup(&cache->ihtbl, vaddr);
	} else {
		vnode = vihtbl_lookup(&cache->vhtbl, vaddr);
	}
	return vnode;
}

static fx_inode_t *
cache_search_inode(const fx_cache_t *cache, fx_ino_t ino)
{
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode;
	fx_inode_t *inode = NULL;

	cache_check(cache);
	vaddr_for_anyino(&vaddr, ino);
	vnode = cache_search_vnode(cache, &vaddr);
	if (vnode != NULL) {
		inode = vnode_to_inode(vnode);
		fx_assert(inode_hasino(inode, ino));
	}
	return inode;
}

static void
cache_stain_vnode(fx_cache_t *cache, fx_vnode_t *vnode, fx_stain_e stain)
{
	fx_vtype_e vtype;
	fx_list_t *from = NULL, *to = NULL;

	vtype = vnode_vtype(vnode);
	from  = cache_list_by_stain(cache, vtype, vnode->v_stain);
	to    = cache_list_by_stain(cache, vtype, stain);
	transfer(&vnode->v_clink, from, to);
	vnode->v_stain = stain;
}

static fx_vnode_t *cache_undirty_vnode(fx_cache_t *cache)
{
	fx_link_t  *lnk;
	fx_vnode_t *vnode = NULL;

	lnk = fx_list_pop_front(&cache->vdirty);
	if (lnk != NULL) {
		vnode = clink_to_vnode(lnk);
		vnode->v_stain = FX_STAIN_NONE;
	}
	return vnode;
}

static fx_vnode_t *cache_getlru_vnode(fx_cache_t *cache)
{
	fx_link_t  *lnk;
	fx_vnode_t *vnode = NULL;

	lnk = fx_list_pop_front(&cache->vlru);
	if (lnk != NULL) {
		vnode = clink_to_vnode(lnk);
		vnode->v_stain = FX_STAIN_NONE;
	}
	return vnode;
}

static void cache_evict_vnode(fx_cache_t *cache, fx_vnode_t *vnode)
{
	cache_check_vnode(cache, vnode, CACHED);

	if (vnode_isinode(vnode)) {
		vihtbl_remove(&cache->ihtbl, vnode);
	} else {
		vihtbl_remove(&cache->vhtbl, vnode);
	}
	cache_stain_vnode(cache, vnode, FX_STAIN_NONE);
	vnode->v_cached = FNX_FALSE;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_fileref_t *cache_new_fileref(fx_cache_t *cache)
{
	fx_fileref_t *fref;

	fref = (fx_fileref_t *)fx_malloc(sizeof(*fref));
	if (fref != NULL) {
		fx_fileref_init(fref);
	}
	fx_unused(cache);
	return fref;
}

static void cache_store_fileref(fx_cache_t *cache, fx_fileref_t *fref)
{
	fx_list_push_back(&cache->frefs, &fref->f_link);
	fref->f_cached = FNX_TRUE;
}

static void cache_del_fileref(fx_cache_t *cache, fx_fileref_t *fref)
{
	fx_fileref_destroy(fref);
	fx_free(fref, sizeof(*fref)); /* XXX */
	fx_unused(cache);
}

static void cache_evict_fileref(fx_cache_t *cache, fx_fileref_t *fref)
{
	fx_list_remove(&cache->frefs, &fref->f_link);
	fref->f_cached = FNX_FALSE;
}


static void cache_clear_filerefs(fx_cache_t *cache)
{
	fx_link_t *lnk;
	fx_fileref_t *fref;

	lnk = fx_list_pop_front(&cache->frefs);
	while (lnk != NULL) {
		fref = fx_link_to_fileref(lnk);
		fref->f_cached = FNX_FALSE;
		cache_del_fileref(cache, fref);
		lnk = fx_list_pop_front(&cache->frefs);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void cache_remap_de(fx_cache_t *cache, fx_ino_t dino,
                           fx_hash_t hash, fx_size_t nlen, fx_ino_t ino)
{
	dehtbl_remap(&cache->dehtbl, dino, hash, nlen, ino);
}

static fx_ino_t cache_lookup_de(const fx_cache_t *cache, fx_ino_t dino,
                                fx_hash_t hash, fx_size_t nlen)
{
	const fx_dentry_t *dent;

	dent = dehtbl_lookup(&cache->dehtbl, dino, hash, nlen);
	return (dent != NULL) ? dent->ino : FNX_INO_NULL;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


void fx_cache_init(fx_cache_t *cache, fx_balloc_t *balloc, fx_valloc_t *valloc)
{
	fx_bzero(cache, sizeof(*cache));
	dehtbl_init(&cache->dehtbl);
	bkhtbl_init(&cache->ubhtbl);
	bkhtbl_init(&cache->sbhtbl);
	vihtbl_init(&cache->vhtbl);
	vihtbl_init(&cache->ihtbl);
	fx_list_init(&cache->vdirty);
	fx_list_init(&cache->vlru);
	fx_list_init(&cache->udirty);
	fx_list_init(&cache->ulru);
	fx_list_init(&cache->sdirty);
	fx_list_init(&cache->slru);
	fx_list_init(&cache->frefs);
	cache->balloc   = balloc;
	cache->valloc   = valloc;
	cache->magic    = CACHE_MAGIC;

	cache_bind_ops(cache);

	cache->climits.c_thresh_max = FX_CVINODE_MAX;
	fx_cache_set_limits(cache, FX_CVINODE_LIM);
}

void fx_cache_destroy(fx_cache_t *cache)
{
	dehtbl_destroy(&cache->dehtbl);
	bkhtbl_destroy(&cache->sbhtbl);
	bkhtbl_destroy(&cache->ubhtbl);
	vihtbl_destroy(&cache->vhtbl);
	vihtbl_destroy(&cache->ihtbl);
	fx_list_destroy(&cache->vdirty);
	fx_list_destroy(&cache->vlru);
	fx_list_destroy(&cache->udirty);
	fx_list_destroy(&cache->ulru);
	fx_list_destroy(&cache->sdirty);
	fx_list_destroy(&cache->slru);
	fx_list_destroy(&cache->frefs);
	fx_bff(cache, sizeof(*cache));
}

void fx_cache_set_limits(fx_cache_t *cache, fx_size_t thresh)
{
	fx_size_t val;
	fx_climits_t *climits = &cache->climits;

	val = fx_min(thresh, cache->climits.c_thresh_max);
	val = fx_max(val, FX_CVINODE_MIN);

	climits->c_frefs_lim    = FNX_OPENFH_MAX;
	climits->c_vinode_lim   = val;
	climits->c_vdirty_lim   = climits->c_vinode_lim / 2;
	climits->c_sblock_lim   = val;
	climits->c_sdirty_lim   = climits->c_sblock_lim / 2;
	climits->c_ublock_lim   = val;
	climits->c_udirty_lim   = climits->c_ublock_lim / 3;
}

void fx_cache_clearall(fx_cache_t *cache)
{
	cache_clear_filerefs(cache);
	cache_clear_vnodes(cache);
	cache_clear_bks(cache);
}

void fx_cache_updates(fx_cache_t *cache)
{
	fx_cstats_t *cstats = &cache->cstats;

	fx_bzero(cstats, sizeof(*cstats));
	cstats->c_inodes    = cache->ihtbl.size;
	cstats->c_vnodes    = cache->vhtbl.size;
	cstats->c_vdirty    = cache->vdirty.size;
	cstats->c_vlru      = cache->vlru.size;
	cstats->c_sblocks   = cache->sbhtbl.size;
	cstats->c_sdirty    = cache->sdirty.size;
	cstats->c_slru      = cache->slru.size;
	cstats->c_ublocks   = cache->ubhtbl.size;
	cstats->c_udirty    = cache->udirty.size;
	cstats->c_ulru      = cache->ulru.size;
	cstats->c_frefs     = cache->frefs.size;
	cstats->c_ufree     = cache->balloc->blks.inter;
	cstats->c_sfree     = cache->balloc->secs.inter;
}

static void cache_bind_ops(fx_cache_t *cache)
{
	cache->new_sec          = cache_new_sec;
	cache->new_bk           = cache_new_bk;
	cache->del_bk           = cache_del_bk;
	cache->store_bk         = cache_store_bk;
	cache->evict_bk         = cache_evict_bk;
	cache->search_bk        = cache_search_bk;
	cache->stain_bk         = cache_stain_bk;
	cache->undirty_bks      = cache_undirty_bks;
	cache->undirty_sec      = cache_undirty_sec;
	cache->getlru_bk        = cache_getlru_bk;
	cache->getlru_sec       = cache_getlru_sec;

	cache->new_vnode        = cache_new_vnode;
	cache->del_vnode        = cache_del_vnode;
	cache->store_vnode      = cache_store_vnode;
	cache->evict_vnode      = cache_evict_vnode;
	cache->search_inode     = cache_search_inode;
	cache->search_vnode     = cache_search_vnode;
	cache->stain_vnode      = cache_stain_vnode;
	cache->undirty_vnode    = cache_undirty_vnode;
	cache->getlru_vnode     = cache_getlru_vnode;

	cache->new_fileref      = cache_new_fileref;
	cache->del_fileref      = cache_del_fileref;
	cache->store_fileref    = cache_store_fileref;
	cache->evict_fileref    = cache_evict_fileref;

	cache->remap_de         = cache_remap_de;
	cache->lookup_de        = cache_lookup_de;
}


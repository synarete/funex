/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"

/* Locals */
static int lookup_special(fx_corex_t *, const fx_dir_t *,
                          const fx_name_t *, fx_inode_t **);

static int lookup_dirent(fx_corex_t *, const fx_dir_t *, fx_hash_t,
                         const fx_name_t *, fx_dirent_t **);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int verify_name(const fx_dir_t *dir, const fx_name_t *name)
{
	int rc = 0;

	if (!dir_isvalidname(dir, name)) {
		rc = -ENAMETOOLONG;
	}
	return rc;
}

static void
dir_update_linked(fx_dir_t *dir, fx_inode_t *inode, fx_inode_t *iref)
{
	if (inode_isreflnk(inode)) {
		fx_assert(iref != NULL);
		iref->i_iattr.i_nlink++;
	} else {
		inode->i_iattr.i_nlink++;
	}
	dir->d_nchilds++;
}

static void
dir_update_linkedd(fx_dir_t *parentd, fx_dir_t *childd)
{
	fx_inode_t *child = &childd->d_inode;

	fx_dir_iref_parentd(childd, parentd);
	child->i_iattr.i_nlink++;

	parentd->d_nchilds++;
	parentd->d_inode.i_iattr.i_nlink++;
}

static void
dir_update_unlinked(fx_dir_t *dir, fx_inode_t *child, fx_inode_t *iref)
{
	if (inode_isreflnk(child)) {
		fx_assert(iref != NULL);
		fx_assert(iref->i_iattr.i_nlink > 0);
		iref->i_iattr.i_nlink--;
	} else {
		fx_assert(child->i_iattr.i_nlink > 0);
		child->i_iattr.i_nlink--;
	}
	fx_assert(dir->d_nchilds > 0);
	dir->d_nchilds--;
}

static void
dir_update_unlinkedd(fx_dir_t *parentd, fx_dir_t *childd)
{
	fx_assert(childd->d_inode.i_iattr.i_nlink > 0);
	fx_dir_unref_parentd(childd);
	childd->d_inode.i_iattr.i_nlink--;

	fx_assert(parentd->d_nchilds > 0);
	fx_assert(parentd->d_inode.i_iattr.i_nlink > 0);
	parentd->d_nchilds--;
	parentd->d_inode.i_iattr.i_nlink--;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
lookup_special(fx_corex_t *corex, const fx_dir_t *dir,
               const fx_name_t *name, fx_inode_t **inode)
{
	int rc;
	fx_bool_t isroot;
	const fx_dirent_t *dirent;

	/* Case 1: pseudo-namespace's root */
	*inode = NULL;
	isroot = dir_isroot(dir);
	if (isroot && fx_name_isequal(name, FNX_PSROOTNAME)) {
		rc = fx_fetch_inode(corex, FNX_INO_PSROOT, inode);
		if ((rc == 0) && inode_isname(*inode, name)) {
			return 0;
		}
	}
	/* Case 2: dot or dot-dot */
	*inode = NULL;
	dirent = fx_dir_meta(dir, name);
	if (dirent == NULL) {
		return -ENOENT;
	}
	rc = fx_fetch_inode(corex, dirent->de_ino, inode);
	if (rc != 0) {
		return rc;
	}
	if (!inode_isname(*inode, name)) {
		return -ENOENT;
	}
	return 0;
}

static fx_inode_t *
dirent_to_inode(fx_corex_t *corex, const fx_dirent_t *dent,
                const fx_name_t *name)
{
	int rc;
	fx_inode_t *inode = NULL;

	rc = fx_fetch_inode(corex, dent->de_ino, &inode);
	fx_assert(rc == 0);
	fx_assert(inode != NULL);
	fx_assert(inode_isname(inode, name)); /* XXX remove this sanity-check */

	return inode;
}

static int
fetch_dirext(fx_corex_t *corex, const fx_dir_t *dir, fx_dirext_t **dirext)
{
	int rc = -ENOENT;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode;

	if (dir_testf(dir, FNX_INODEF_EXT)) {
		vaddr_for_dirext(&vaddr, dir_getino(dir));
		rc = fx_fetch_vnode(corex, &vaddr, &vnode);
		if (rc == 0) {
			*dirext = fx_vnode_to_dirext(vnode);
		}
	}
	return rc;
}

static int
fetch_dirseg(fx_corex_t *corex, const fx_dir_t *dir, const fx_dirext_t *dirext,
             fx_off_t dseg, fx_dirseg_t **dirseg)
{
	int rc = -ENOENT;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode;

	if (fx_dirext_hasseg(dirext, dseg)) {
		vaddr_for_dirseg(&vaddr, dir_getino(dir), dseg);
		rc = fx_fetch_vnode(corex, &vaddr, &vnode);
		fx_assert(rc != -ENOENT);
		if (rc == 0) {
			*dirseg = fx_vnode_to_dirseg(vnode);
		}
	}
	return rc;
}

static int
fetch_dirsegh(fx_corex_t *corex, const fx_dir_t *dir, const fx_dirext_t *dirext,
              fx_hash_t hash, fx_dirseg_t **dirseg)
{
	const fx_off_t dseg = fx_hash_to_dseg(hash);

	return fetch_dirseg(corex, dir, dirext, dseg, dirseg);
}

static int
lookup_dirent(fx_corex_t *corex, const fx_dir_t *dir, fx_hash_t hash,
              const fx_name_t *name, fx_dirent_t **dirent)
{
	int rc;
	fx_dirext_t *dirext = NULL;
	fx_dirseg_t *dirseg = NULL;

	/* Lookup within top-node */
	*dirent = fx_dir_lookup(dir, hash, name->len);
	if (*dirent != NULL) {
		return 0; /* OK, match at dir-top */
	}
	/* Lookup within dir-extension node */
	rc = fetch_dirext(corex, dir, &dirext);
	if (rc != 0) {
		return rc;
	}
	*dirent = fx_dirext_lookup(dirext, hash, name->len);
	if (*dirent != NULL) {
		return 0;
	}
	/* Lookup within dir sub-segment node */
	rc = fetch_dirsegh(corex, dir, dirext, hash, &dirseg);
	if (rc != 0) {
		return rc;
	}
	*dirent = fx_dirseg_lookup(dirseg, hash, name->len);
	if (*dirent != NULL) {
		return 0;
	}

	/* No entry in all levels */
	return -ENOENT;
}

static int
lookup_cached_de(fx_corex_t *corex, const fx_dir_t *dir, fx_hash_t hash,
                 const fx_name_t *name, fx_inode_t **inode)
{
	int rc;
	fx_ino_t dino, ino;

	dino = dir_getino(dir);
	ino  = corex->cache->lookup_de(corex->cache, dino, hash, name->len);
	if (ino == FNX_INO_NULL) {
		return -ENOENT;
	}
	rc = fx_fetch_inode(corex, ino, inode);
	if (rc != 0) {
		return rc;
	}
	if (!inode_isname(*inode, name)) {
		return -ENOENT;
	}
	return 0;
}

int fx_lookup_iinode(fx_corex_t *corex, const fx_dir_t *dir,
                     const fx_name_t *name, fx_hash_t nhash, fx_inode_t **inode)
{
	int rc;
	fx_hash_t hash = 0;
	fx_dirent_t *dirent = NULL;

	rc = verify_name(dir, name);
	if (rc != 0) {
		return rc;
	}
	/* Lookup for dot, dot-dot or pseudo */
	rc = lookup_special(corex, dir, name, inode);
	if (rc == 0) {
		return 0;
	}

	/* Do lookup-operations by name-hash + name-length */
	if (nhash == 0) {
		hash = fx_inamehash(name, dir);
	} else {
		hash = nhash;
	}

	/* Try dentries-cache */
	rc = lookup_cached_de(corex, dir, hash, name, inode);
	if (rc == 0) {
		return 0;
	}

	/* Lookup within dir-subs tree */
	rc = lookup_dirent(corex, dir, hash, name, &dirent);
	if (rc != 0) {
		return rc;
	}
	*inode = dirent_to_inode(corex, dirent, name);
	if (*inode == NULL) {
		return -ENOENT; /* TODO: keep on iter */
	}
	return 0;
}

int fx_lookup_ientry(fx_corex_t *corex, const fx_dir_t *dir,
                     const fx_name_t *name, fx_hash_t nhash, fx_inode_t **inode)
{
	int rc;

	*inode = NULL;
	rc = fx_lookup_iinode(corex, dir, name, nhash, inode);
	if ((rc != 0) && (rc != -ENOENT)) {
		return rc;
	}
	return 0;
}

int fx_lookup_hinode(fx_corex_t *corex, const fx_dir_t *dir,
                     const fx_name_t *name, fx_hash_t nhash, fx_inode_t **inode)
{
	int rc;

	*inode = NULL;
	rc = verify_name(dir, name);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_iinode(corex, dir, name, nhash, inode);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_lookup_inode(fx_corex_t *corex, const fx_dir_t *dir,
                    const fx_name_t *name, fx_hash_t nhash, fx_inode_t **inode)
{
	int rc;
	fx_inode_t *reflnk;

	rc = fx_lookup_hinode(corex, dir, name, nhash, inode);
	if (rc != 0) {
		return rc;
	}
	if (inode_isreflnk(*inode)) {
		/* Fetch and refer to h-linked inode */
		reflnk  = *inode;
		rc = fx_fetch_inode(corex, reflnk->i_refino, inode);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

int fx_lookup_link(fx_corex_t *corex, const fx_dir_t *dir,
                   const fx_name_t *name, fx_hash_t nhash, fx_inode_t **inode)
{
	int rc;
	fx_inode_t *hlnk = NULL;

	/* NB: Must check here for inode; we do lookup for the link itself! */
	rc = verify_name(dir, name);
	if (rc != 0) {
		return rc;
	}
	rc = fx_lookup_iinode(corex, dir, name, nhash, &hlnk);
	if (rc != 0) {
		return rc;
	}
	if (inode_isdir(hlnk)) {
		return -EISDIR;
	}
	*inode = hlnk;
	return 0;
}

int fx_lookup_dir(fx_corex_t *corex, const fx_dir_t *parentd,
                  const fx_name_t *name, fx_hash_t nhash, fx_dir_t **dir)
{
	int rc;
	fx_inode_t *inode = NULL;

	rc = fx_lookup_inode(corex, parentd, name, nhash, &inode);
	if (rc != 0) {
		return rc;
	}
	if (!inode_isdir(inode)) {
		return -ENOTDIR;
	}
	*dir = fx_inode_to_dir(inode);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_off_t doff_plus(const fx_dirent_t *dirent)
{
	/* TODO: Improve? */
	fx_off_t doff, doff_next = FNX_DOFF_NONE;

	doff = dirent->de_doff;
	if ((doff >= FNX_DOFF_SELF) && (doff < FNX_DOFF_END)) {
		doff_next = doff + 1;
	}
	return doff_next;
}

static int search_dirtop(const fx_dir_t *dir, fx_off_t doff,
                         fx_dirent_t **dirent, fx_off_t *doff_next)
{
	if ((doff < 0) || (FNX_DOFF_BEGINX <= doff)) {
		return FX_EEOS;
	}
	*dirent = (fx_dirent_t *)fx_dir_search(dir, doff);
	if (*dirent == NULL) {
		return FX_EEOS;
	}
	*doff_next = doff_plus(*dirent);
	return 0;
}

static int
search_dirext(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
              fx_dirent_t **dirent, fx_off_t *doff_next)
{
	int rc;
	fx_dirext_t *dirext = NULL;

	if (!dir_testf(dir, FNX_INODEF_EXT)) {
		return FX_EEOS;
	}
	if (doff >= FNX_DOFF_BEGINS) {
		return FX_EEOS;
	}
	rc = fetch_dirext(corex, dir, &dirext);
	if (rc != 0) {
		return rc;
	}

	doff = off_max(doff, FNX_DOFF_BEGINX);
	*dirent = fx_dirext_search(dirext, doff);
	if (*dirent != NULL) {
		*doff_next = doff_plus(*dirent);
		return 0;
	}
	return FX_EEOS;
}

static int
search_dirseg(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
              fx_dirent_t **dirent, fx_off_t *doff_next)
{
	int rc, cnt = 0;
	fx_off_t dseg, dlim;
	fx_dirext_t *dirext = NULL;
	fx_dirseg_t *dirseg = NULL;


	if (doff >= FNX_DOFF_END) {
		return FX_EEOS;
	}
	rc = fetch_dirext(corex, dir, &dirext);
	if (rc != 0) {
		return rc;
	}
	if (fx_dirext_isempty(dirext)) {
		return FX_EEOS;
	}

	doff = off_max(doff, FNX_DOFF_BEGINS);
	dseg = fx_doff_to_dseg(doff);
	dlim = fx_doff_to_dseg(FNX_DOFF_END);
	while ((dseg < dlim) && (dseg != FNX_DOFF_NONE)) {
		if (cnt++ > 0) {
			doff = fx_dseg_to_doff(dseg);
		}
		rc = fetch_dirseg(corex, dir, dirext, dseg, &dirseg);
		if (rc == 0) {
			*dirent = fx_dirseg_search(dirseg, doff);
			if (*dirent != NULL) {
				*doff_next = doff_plus(*dirent);
				return 0; /* Habemus dirent */
			}
		} else if (rc != -ENOENT) {
			return rc;
		}
		dseg = fx_dirext_nextseg(dirext, dseg + 1);
	}
	return FX_EEOS;
}

static int
search_dirent(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
              fx_dirent_t **dirent, fx_off_t *doff_next)
{
	int rc = FX_EEOS;

	fx_assert(dir->d_dent[FNX_DOFF_PARENT].de_ino != FNX_INO_NULL);

	/* Nothing to look for if non-valid dir-offset */
	if (!fx_doff_isvalid(doff)) {
		return FX_EEOS;
	}

	/* Search for top-node child */
	*dirent     = NULL;
	*doff_next  = FNX_DOFF_NONE;
	rc = search_dirtop(dir, doff, dirent, doff_next);
	if ((rc == 0) || (rc != FX_EEOS)) {
		return rc;
	}
	/* Search witin dir-extension */
	rc = search_dirext(corex, dir, doff, dirent, doff_next);
	if ((rc == 0) || (rc != FX_EEOS)) {
		return rc;
	}
	/* Search within sub-segments */
	rc = search_dirseg(corex, dir, doff, dirent, doff_next);
	if ((rc == 0) || (rc != FX_EEOS)) {
		return rc;
	}

	/* End-of-stream */
	return FX_EEOS;
}

static int
search_normal(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
              fx_inode_t **hlnk, fx_inode_t **child, fx_off_t *doff_next)
{
	int rc;
	fx_inode_t *reflnk, *inode = NULL;
	fx_dirent_t *dirent = NULL;

	/* Fast-lookup for self */
	if (doff == FNX_DOFF_SELF) {
		*child      = dir_inode(dir);
		*doff_next  = FNX_DOFF_PARENT;
		return 0;
	}

	/* Perform dir's multi-level search for child's entry */
	rc = search_dirent(corex, dir, doff, &dirent, doff_next);
	if (rc != 0) {
		return rc;
	}

	/* Resolve from dirent to actual inode */
	rc = fx_fetch_inode(corex, dirent->de_ino, &inode);
	fx_assert(rc == 0);
	fx_assert(inode != NULL);
	*hlnk = inode;

	/* May need to do extra resolve in case of hard-link */
	if (inode_isreflnk(inode)) {
		reflnk  = inode;
		rc = fx_fetch_inode(corex, reflnk->i_refino, &inode);
		fx_assert(rc == 0);
		fx_assert(inode != NULL);
	}
	*child = inode;
	return 0;
}

static int
search_pseudo(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
              fx_inode_t **hlnk, fx_inode_t **child, fx_off_t *doff_next)
{
	int rc;
	fx_inode_t *inode;

	if (!fx_doff_isvalid(doff)) {
		return FX_EEOS;
	}
	if (!dir_isroot(dir)) {
		return FX_EEOS;
	}
	rc = fx_fetch_inode(corex, FNX_INO_PSROOT, &inode);
	fx_assert(rc == 0);
	*hlnk = *child = inode;
	*doff_next = FNX_DOFF_NONE;
	return 0;
}

static int
search_dir(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
           fx_inode_t **hlnk, fx_inode_t **child, fx_off_t *doff_next)
{
	int rc = 0;

	/* By default, no next dir-offset */
	*doff_next = FNX_DOFF_NONE;

	/* Directory's two-level search for child's entry */
	rc = search_normal(corex, dir, doff, hlnk, child, doff_next);
	if (rc != 0) {
		/* Special case for root-dir with associated pseudo namespace */
		rc = search_pseudo(corex, dir, doff, hlnk, child, doff_next);
	}
	return rc;
}

/* Returns inode's name; for self and parent returns special string-names */
static fx_name_t *getname(fx_inode_t *inode, fx_off_t doff)
{
	static fx_name_t s_dot1 = { 1, "." };
	static fx_name_t s_dot2 = { 2, ".." };

	if (doff == FNX_DOFF_SELF) {
		return &s_dot1;
	}
	if (doff == FNX_DOFF_PARENT) {
		return &s_dot2;
	}
	return &inode->i_name;
}

int fx_search_dent(fx_corex_t *corex, const fx_dir_t *dir, fx_off_t doff,
                   fx_name_t **name, fx_inode_t **child, fx_off_t *next)
{
	int rc;
	fx_inode_t *hlnk = NULL;

	rc = search_dir(corex, dir, doff, &hlnk, child, next);
	if (rc == 0) {
		*name = getname(hlnk, doff);
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_attach_fref(fx_corex_t *corex, fx_inode_t *inode,
                   fx_flags_t flags, fx_fileref_t **fref)
{
	int rc;

	rc = corex->obtain_fref(corex, flags, fref);
	if (rc == 0) {
		fx_fileref_attach(*fref, inode);
		corex->task->tsk_fref = *fref;
	}
	return rc;
}

int fx_detach_fref(fx_corex_t *corex, fx_fileref_t *fref)
{
	int rc;
	fx_inode_t *inode;

	inode = fx_fileref_detach(fref);
	corex->discard_fref(corex, fref);
	corex->task->tsk_fref = NULL;

	if (inode->i_vnode.v_removed) {
		rc = fx_fix_unlinked(corex, inode);
		if (rc != 0) {
			return rc;/* TODO: reref? */
		}
	}
	return 0;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
acquire_dirext(fx_corex_t *corex, const fx_dir_t *dir, fx_dirext_t **dirext)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	vaddr_for_dirext(&vaddr, dir_getino(dir));
	rc = fx_acquire_vnode(corex, &vaddr, &vnode);
	if (rc == 0) {
		*dirext = fx_vnode_to_dirext(vnode);
	}
	return rc;
}

static int yield_dirext(fx_corex_t *corex, fx_dir_t *dir, fx_dirext_t **dirext)
{
	int rc;

	if (dir_testf(dir, FNX_INODEF_EXT)) {
		rc = fetch_dirext(corex, dir, dirext);
	} else {
		rc = acquire_dirext(corex, dir, dirext);
		if (rc == 0) {
			dir_setf(dir, FNX_INODEF_EXT);
			corex->put_dir(corex, dir, 0);
		}
	}
	return rc;
}


static int
acquire_dirseg(fx_corex_t *corex, const fx_dir_t *dir,
               fx_off_t dseg, fx_dirseg_t **dirseg)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	vaddr_for_dirseg(&vaddr, dir_getino(dir), dseg);
	rc = fx_acquire_vnode(corex, &vaddr, &vnode);
	if (rc == 0) {
		*dirseg = fx_vnode_to_dirseg(vnode);
		fx_dirseg_setup(*dirseg, dseg);
	}
	return rc;
}

static int
yield_dirseg(fx_corex_t *corex, const fx_dir_t *dir,
             fx_dirext_t *dirext, fx_hash_t nhash, fx_dirseg_t **dirseg)
{
	int rc;
	fx_off_t dseg;

	dseg = fx_hash_to_dseg(nhash);
	rc = fetch_dirseg(corex, dir, dirext, dseg, dirseg);
	if (rc == -ENOENT) {
		rc = acquire_dirseg(corex, dir, dseg, dirseg);
		if (rc == 0) {
			fx_dirext_setseg(dirext, dseg);
			corex->put_vnode(corex, &dirext->dx_vnode);
		}
	}
	return rc;
}

static int
link_child(fx_corex_t *corex, fx_dir_t *parentd,
           const fx_name_t *name, fx_hash_t nhash, fx_inode_t *inode)
{
	int rc;
	fx_dirent_t *dirent = NULL;
	fx_dirext_t *dirext = NULL;
	fx_dirseg_t *dirseg = NULL;

	/* Set namespace mapping: (dir-ino, name) --> inode */
	fx_inode_associate(inode, dir_getino(parentd), name, nhash);

	/* Try to link top-level child */
	dirent = fx_dir_link(parentd, inode);
	if (dirent != NULL) {
		return 0; /* Linked OK at dir's head */
	}

	/* Try to link extension child */
	rc = yield_dirext(corex, parentd, &dirext);
	if (rc != 0) {
		return rc;
	}
	dirent = fx_dirext_link(dirext, inode);
	if (dirent != NULL) {
		corex->put_vnode(corex, &dirext->dx_vnode);
		return 0;
	}

	/* Try to at sub-segment */
	rc = yield_dirseg(corex, parentd, dirext, inode->i_nhash, &dirseg);
	if (rc != 0) {
		return rc;
	}
	dirent = fx_dirseg_link(dirseg, inode);
	if (dirent != NULL) {
		corex->put_vnode(corex, &dirseg->ds_vnode);
		return 0;
	}

	/* Unable to add child */
	return -ENOSPC;
}

static void update_linked(fx_corex_t *corex, fx_dir_t *parentd,
                          fx_inode_t *child, fx_inode_t *iref)
{
	fx_hash_t hash;
	fx_size_t nlen;
	fx_ino_t  dino, ino;
	fx_dir_t *childd;

	/* Post-op: fix namespace linkage */
	if (inode_isdir(child)) {
		childd = inode_to_dir(child);
		dir_update_linkedd(parentd, childd);
		corex->put_dir(corex, parentd, FNX_AMCTIME_NOW);
		corex->put_dir(corex, childd, FNX_AMCTIME_NOW);
	} else if (inode_isreflnk(child)) {
		fx_assert(iref != NULL);
		dir_update_linked(parentd, child, iref);
		corex->put_dir(corex, parentd, FNX_MCTIME_NOW);
		corex->put_inode(corex, child, FNX_MCTIME_NOW);
		corex->put_inode(corex, iref, FNX_MCTIME_NOW);
	} else {
		fx_assert(iref == NULL);
		dir_update_linked(parentd, child, NULL);
		corex->put_dir(corex, parentd, FNX_MCTIME_NOW);
		corex->put_inode(corex, child, FNX_AMCTIME_NOW);
	}

	/* If possible, add dentry-cache */
	dino = dir_getino(parentd);
	hash = child->i_nhash;
	nlen = child->i_name.len;
	ino  = inode_getrefino(child);
	corex->cache->remap_de(corex->cache, dino, hash, nlen, ino);
}

static int
prepare_xxlink(fx_corex_t *corex, fx_inode_t *inode, fx_inode_t **iref)
{
	int rc = 0;

	if (inode_vtype(inode) == FNX_VTYPE_REFLNK) {
		fx_assert(inode->i_refino != FNX_INO_NULL);
		rc = fx_fetch_inode(corex, inode->i_refino, iref);
		fx_assert(rc == 0);
	} else {
		*iref = NULL;
	}
	return rc;
}

int fx_link_child(fx_corex_t *corex, fx_dir_t *parentd,
                  const fx_name_t *name, fx_hash_t nhash, fx_inode_t *inode)
{
	int rc;
	fx_inode_t *iref = NULL;

	if (!dir_hasspace(parentd)) {
		return -ENOSPC;
	}
	rc = prepare_xxlink(corex, inode, &iref);
	if (rc != 0) {
		return rc;
	}
	rc = link_child(corex, parentd, name, nhash, inode);
	if (rc != 0) {
		return rc;
	}
	update_linked(corex, parentd, inode, iref);
	return 0;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void settle_unlinked(fx_corex_t *corex, fx_dir_t *dir,
                            fx_dirext_t *dirext, fx_dirseg_t *dirseg)
{
	if (dirseg != NULL) {
		if (fx_dirseg_isempty(dirseg)) {
			fx_dirext_unsetseg(dirext, dirseg->ds_index);
			fx_reclaim_vnode(corex, &dirseg->ds_vnode);
			dirseg = NULL;
		}
	}
	if (dirext != NULL) {
		if (fx_dirext_isempty(dirext)) {
			dir_unsetf(dir, FNX_INODEF_EXT);
			fx_reclaim_vnode(corex, &dirext->dx_vnode);
			dirext = NULL;
		}
	}
	if (dir != NULL) {
		if (dirext != NULL) {
			if (dirseg != NULL) {
				corex->put_vnode(corex, &dirseg->ds_vnode);
			}
			corex->put_vnode(corex, &dirext->dx_vnode);
		}
		corex->put_dir(corex, dir, 0);
	}
}

static int
unlink_child(fx_corex_t *corex, fx_dir_t *parentd, fx_inode_t *inode)
{
	int rc;
	fx_dirent_t *dirent;
	fx_dirext_t *dirext = NULL;
	fx_dirseg_t *dirseg = NULL;

	/* Try to unlink top-node child */
	dirent = fx_dir_unlink(parentd, inode);
	if (dirent != NULL) {
		return 0; /* Unlinked OK from dir's head */
	}
	/* Try to unlink dir-extension child */
	rc = fetch_dirext(corex, parentd, &dirext);
	if (rc != 0) {
		return rc;
	}
	dirent = fx_dirext_unlink(dirext, inode);
	if (dirent != NULL) {
		settle_unlinked(corex, parentd, dirext, NULL);
		return 0;
	}
	/* Try to unlink dir sub-segment child */
	rc = fetch_dirsegh(corex, parentd, dirext, inode->i_nhash, &dirseg);
	if (rc != 0) {
		return rc;
	}
	dirent = fx_dirseg_unlink(dirseg, inode);
	if (dirent != NULL) {
		settle_unlinked(corex, parentd, dirext, dirseg);
		return 0;
	}

	/* Could-not unlink: internal error! */
	fx_assert(0);
	return -EIO;
}

static void update_unlinked(fx_corex_t *corex, fx_dir_t *parentd,
                            fx_inode_t *child, fx_inode_t *iref)
{
	fx_hash_t hash;
	fx_size_t nlen;
	fx_ino_t  dino;
	fx_dir_t *childd;

	/* Remove from dentry-cache */
	dino = dir_getino(parentd);
	hash = child->i_nhash;
	nlen = child->i_name.len;
	corex->cache->remap_de(corex->cache, dino, hash, nlen, FNX_INO_NULL);

	/* Dissociate from namespace mapping */
	fx_inode_associate(child, FNX_INO_NULL, NULL, 0);

	/* Post-op: fix dir linkage */
	if (inode_isdir(child)) {
		childd = inode_to_dir(child);
		dir_update_unlinkedd(parentd, childd);
		corex->put_dir(corex, parentd, FNX_MCTIME_NOW);
		corex->put_dir(corex, childd, FNX_CTIME_NOW);
	} else if (inode_isreflnk(child)) {
		fx_assert(iref != NULL);
		dir_update_unlinked(parentd, child, iref);
		corex->put_dir(corex, parentd, FNX_MCTIME_NOW);
		corex->put_inode(corex, iref, FNX_CTIME_NOW);
	} else {
		fx_assert(iref == NULL);
		dir_update_unlinked(parentd, child, NULL);
		corex->put_dir(corex, parentd, FNX_MCTIME_NOW);
		corex->put_inode(corex, child, FNX_CTIME_NOW);
	}
}

int fx_unlink_child(fx_corex_t *corex, fx_dir_t *parentd, fx_inode_t *inode)
{
	int rc;
	fx_inode_t *iref = NULL;

	rc = prepare_xxlink(corex, inode, &iref);
	if (rc != 0) {
		return rc;
	}
	rc = unlink_child(corex, parentd, inode);
	if (rc != 0) {
		return rc;
	}
	update_unlinked(corex, parentd, inode, iref);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int settle_unlinked_dir(fx_corex_t *corex, fx_dir_t *dir)
{
	int rc;
	fx_nlink_t nlink;
	fx_vnode_t *vnode;

	vnode = &dir->d_inode.i_vnode;
	nlink = dir_getnlink(dir);
	if (nlink > FX_DIR_INIT_NLINK) {
		return 0;
	}
	vnode->v_removed = FNX_TRUE;
	if (vnode->v_refcnt > 0) {
		return 0;
	}
	fx_assert(dir->d_nchilds == 0);
	rc = fx_reclaim_inode(corex, &dir->d_inode);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static int settle_unlinked_subs(fx_corex_t *corex, fx_inode_t *inode)
{
	int rc = 0;

	if (inode_isreg(inode)) {
		rc = fx_trunc_data(corex, inode, 0);
	}
	return rc;
}

static int settle_unlinked_inode(fx_corex_t *corex, fx_inode_t *inode)
{
	int rc;
	fx_nlink_t nlink;
	fx_vnode_t *vnode;

	vnode = &inode->i_vnode;
	nlink = inode_getnlink(inode);
	if (nlink > FX_INODE_INIT_NLINK) {
		return 0;
	}
	vnode->v_removed = FNX_TRUE;
	if (vnode->v_refcnt > 0) {
		return 0;
	}
	rc = settle_unlinked_subs(corex, inode);
	if (rc != 0) {
		return rc;
	}
	rc = fx_reclaim_inode(corex, inode);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_fix_unlinked(fx_corex_t *corex, fx_inode_t *inode)
{
	int rc;
	fx_dir_t *dir;
	fx_inode_t *iref = NULL;

	rc = prepare_xxlink(corex, inode, &iref);
	if (rc != 0) {
		return rc;
	}
	if (inode_isdir(inode)) {
		dir = inode_to_dir(inode);
		rc  = settle_unlinked_dir(corex, dir);
	} else if (inode_isreflnk(inode)) {
		fx_assert(iref != NULL);
		rc = settle_unlinked_inode(corex, iref);
		if (rc == 0) {
			rc = settle_unlinked_inode(corex, inode);
		}
	} else {
		fx_assert(iref == NULL);
		rc = settle_unlinked_inode(corex, inode);
	}

	return rc;
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"


static size_t num_openfh(const fx_corex_t *corex)
{
	return list_size(&corex->cache->frefs);
}

static int check_openfh_limit(fx_corex_t *corex)
{
	int rc = 0;
	size_t cur, lim;
	const fx_cache_t *cache = corex->cache;

	cur = num_openfh(corex);
	lim = cache->climits.c_frefs_lim;
	if (cur >= lim) {
		fx_trace1("fref-limit: cur=%zu lim=%zu", cur, lim);
		rc = -ENFILE;
	}
	return rc;
}

static int hascap(const fx_uctx_t *uctx, fx_capf_t mask)
{
	return (uctx->u_root || fx_userctx_iscap(uctx, mask));
}

static int isvtx(const fx_dir_t *dir)
{
	const fx_mode_t mode = inode_getmode(&dir->d_inode);
	return ((mode & S_ISVTX) != 0);
}

static int
verify_sticky(fx_corex_t *corex, const fx_dir_t *dir, const fx_inode_t *inode)
{
	int rc;
	const fx_uctx_t *uctx = corex->get_uctx(corex);

	if (!isvtx(dir)) {
		rc = 0; /* No sticky-bit, we're fine */
	} else if (inode_isowner(&dir->d_inode, uctx)) {
		rc = 0;
	} else if (inode_isowner(inode, uctx)) {
		rc = 0;
	} else if (hascap(uctx, FNX_CAPF_FOWNER)) {
		rc = 0;
	} else {
		rc = -EPERM;
	}
	return rc;
}

int fx_let_access(fx_corex_t *corex, const fx_inode_t *inode, fx_mode_t mask)
{
	int rc;

	rc = fx_inode_access(inode, corex->get_uctx(corex), mask);
	return (rc != 0) ? -EACCES : 0;
}

static int let_access(fx_corex_t *corex, const fx_dir_t *dir, fx_mode_t mask)
{
	return fx_let_access(corex, dir_inode(dir), mask);
}

static int let_waccess(fx_corex_t *corex, const fx_dir_t *dir)
{
	return let_access(corex, dir, W_OK);
}

int fx_let_lookup(fx_corex_t *corex, const fx_dir_t *dir)
{
	return let_access(corex, dir, X_OK);
}

int fx_let_opendir(fx_corex_t *corex, const fx_dir_t *dir)
{
	int rc;

	rc = let_access(corex, dir, R_OK);
	if (rc != 0) {
		return rc;
	}
	rc = check_openfh_limit(corex);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static int verify_fileref(const fx_fileref_t *fref, fx_ino_t ino)
{
	if ((fref == NULL) || (fref->f_inode == NULL)) {
		return -EBADF;
	}
	if (!fref->f_cached) {
		fx_warn("fileref-not-cached: ino=%#jx", ino);
		return -EBADF;
	}
	if (inode_getino(fref->f_inode) != ino) {
		fx_error("fileref-inconsistency: ino=%#jx fref-ino=%#jx vtype=%d",
		         ino, inode_getino(fref->f_inode), inode_vtype(fref->f_inode));
		return -EBADF;
	}
	return 0;
}

int fx_let_fsyncdir(fx_corex_t *corex, const fx_fileref_t *fref, fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!inode_isdir(fref->f_inode)) {
		return -ENOTDIR;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_fsync(fx_corex_t *corex, const fx_fileref_t *fref, fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!inode_isreg(fref->f_inode)) {
		return -EINVAL;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_flush(fx_corex_t *corex, const fx_fileref_t *fref, fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!inode_isreg(fref->f_inode)) {
		return -EINVAL;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_release(fx_corex_t *corex, const fx_fileref_t *fref, fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!inode_isreg(fref->f_inode)) {
		return -EINVAL;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_releasedir(fx_corex_t *corex, const fx_fileref_t *fref, fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!inode_isdir(fref->f_inode)) {
		return -ENOTDIR;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_fquery(fx_corex_t *corex, const fx_fileref_t *fref, fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_readable) {
		return -EPERM;
	}
	if (!inode_isreg(fref->f_inode) ||
	    inode_ispseudo(fref->f_inode)) {
		return -EPERM;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_readdir(fx_corex_t *corex, const fx_fileref_t *fref,
                   fx_ino_t ino, fx_off_t off)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!inode_isdir(fref->f_inode)) {
		return -ENOTDIR;
	}
	if (!fx_doff_isvalid(off)) {
		return -EINVAL;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_rmdir(fx_corex_t *corex,
                 const fx_dir_t *parentd, const fx_dir_t *dir)
{
	int rc;

	if (dir_ispseudo(parentd)) {
		return -EPERM;
	}
	rc = let_waccess(corex, parentd);
	if (rc != 0) {
		return rc;
	}
	if (!dir_isempty(dir)) {
		return -ENOTEMPTY;
	}
	if (dir_ispseudo(dir)) {
		return -EPERM;
	}
	if (dir_isroot(dir)) {
		return -EBUSY;
	}
	if (dir_testf(dir, FNX_INODEF_EXT)) {
		return -EBUSY; /* Should never happen! */
	}
	rc = verify_sticky(corex, parentd, &dir->d_inode);
	if (rc != 0) {
		return rc;
	}

	return 0;
}

int fx_let_unlink(fx_corex_t *corex,
                  const fx_dir_t *parentd, const fx_inode_t *inode)
{
	int rc;

	if (inode_isdir(inode)) {
		return -EISDIR;
	}
	if (inode_ispseudo(inode)) {
		return -EPERM;
	}
	rc = let_waccess(corex, parentd);
	if (rc != 0) {
		return rc;
	}
	rc = verify_sticky(corex, parentd, inode);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_let_link(fx_corex_t *corex, const fx_dir_t *dir,
                const fx_inode_t *inode, const fx_name_t *name)
{
	int rc;

	if (inode_isdir(inode)) {
		return -EISDIR;
	}
	if (dir_ispseudo(dir)) {
		return -EPERM;
	}
	if (!dir_hasspace(dir)) {
		return -EMLINK;
	}
	if (inode_getnlink(inode) >= FNX_LINK_MAX) {
		return -EMLINK;
	}
	rc = let_waccess(corex, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_nolink(corex, dir, name);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

static int verify_iorange(const fx_inode_t *inode, fx_off_t off, fx_size_t len)
{
	fx_off_t max, end;

	if (!inode_isreg(inode)) {
		return -EPERM;
	}
	end = off_end(off, len);
	max = off_end(0, FNX_REGSIZE_MAX);
	if (inode_ispseudo(inode)) {
		max = off_end(off, FNX_BLKSIZE);
	}
	if (off < 0) {
		return -EINVAL;
	}
	if ((off >= max) || (end > max)) {
		return -EFBIG;
	}
	return 0;
}

int fx_let_fallocate(fx_corex_t *corex, const fx_fileref_t *fref,
                     fx_ino_t ino, fx_off_t off, fx_size_t len)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_writeable) {
		return -EPERM;
	}
	if (!inode_isreg(fref->f_inode)) {
		return -ENODEV;
	}
	rc = verify_iorange(fref->f_inode, off, len);
	if (rc != 0) {
		return rc;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_write(fx_corex_t *corex, const fx_fileref_t *fref,
                 fx_ino_t ino, fx_off_t off, fx_size_t len)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_writeable) {
		return -EPERM;
	}
	rc = verify_iorange(fref->f_inode, off, len);
	if (rc != 0) {
		return rc;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_read(fx_corex_t *corex, const fx_fileref_t *fref,
                fx_ino_t ino, fx_off_t off, fx_size_t len)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_readable) {
		return -EBADF;
	}
	rc = verify_iorange(fref->f_inode, off, len);
	if (rc != 0) {
		return rc;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_fpunch(fx_corex_t *corex, const fx_fileref_t *fref,
                  fx_ino_t ino, fx_off_t off, fx_size_t len)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_writeable) {
		return -EPERM;
	}
	if (inode_ispseudo(fref->f_inode)) {
		return -EPERM;
	}
	rc = verify_iorange(fref->f_inode, off, len);
	if (rc != 0) {
		return rc;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_xcopy_tgt(fx_corex_t *corex, const fx_fileref_t *fref,
                     fx_ino_t ino)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_writeable) {
		return -EPERM;
	}
	if (inode_ispseudo(fref->f_inode)) {
		return -EPERM;
	}
	rc = verify_iorange(fref->f_inode, 0, 0);
	if (rc != 0) {
		return rc;
	}
	fx_unused(corex);
	return 0;
}

int fx_let_xcopy_src(fx_corex_t *corex, const fx_fileref_t *fref,
                     fx_ino_t ino,
                     fx_off_t off, fx_size_t len)
{
	int rc;

	if ((rc = verify_fileref(fref, ino)) != 0) {
		return rc;
	}
	if (!fref->f_readable) {
		return -EPERM;
	}
	if (inode_ispseudo(fref->f_inode)) {
		return -EPERM;
	}
	rc = verify_iorange(fref->f_inode, off, len);
	if (rc != 0) {
		return rc;
	}
	fx_unused(corex);
	return 0;
}

static int
verify_oaccess(fx_corex_t *corex, const fx_inode_t *inode, fx_flags_t flags)
{
	fx_mode_t mask = 0;

	if ((flags & O_RDWR) == O_RDWR) {
		mask = R_OK | W_OK;
	} else if ((flags & O_WRONLY) == O_WRONLY) {
		mask = W_OK;
	} else if ((flags & O_RDONLY) == O_RDONLY) {
		mask = R_OK;
	}

	if ((flags & O_TRUNC) == O_TRUNC) {
		mask |= W_OK;
	}
	if ((flags & O_APPEND) == O_APPEND) {
		mask |= W_OK;
	}
	return fx_let_access(corex, inode, mask);
}

static int
verify_opseudo(fx_corex_t *corex, const fx_inode_t *inode, fx_flags_t flags)
{
	fx_flags_t mask = O_WRONLY | O_RDWR;

	if (!inode_ispseudo(inode)) {
		return 0;
	}
	if (flags & mask) {
		if ((inode->i_meta == NULL) || (inode->i_meta->save == NULL)) {
			return -ENOTSUP;
		}
	}
	fx_unused(corex);
	return 0;
}

int fx_let_open(fx_corex_t *corex, const fx_inode_t *inode, fx_flags_t flags)
{
	int rc;
	fx_flags_t mask;

	if (inode_isdir(inode)) {
		return -EISDIR;
	}
	if (flags & O_DIRECTORY) {
		return -EISDIR;
	}
	mask = O_CREAT | O_EXCL;
	if (flags & mask) {
		return -EEXIST; /* XXX ? */
	}
	if (!inode_isreg(inode)) {
		return -EINVAL;
	}
	rc = check_openfh_limit(corex);
	if (rc != 0) {
		return rc;
	}
	rc = verify_oaccess(corex, inode, flags);
	if (rc != 0) {
		return rc;
	}
	rc = verify_opseudo(corex, inode, flags);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_let_nolink(fx_corex_t *corex,
                  const fx_dir_t  *dir, const fx_name_t *name)
{
	int rc;
	fx_inode_t *inode;

	rc = fx_lookup_iinode(corex, dir, name, 0, &inode);
	if (rc == 0) {
		return -EEXIST;
	}
	if (rc != -ENOENT) {
		return rc;
	}
	return 0;
}

int fx_let_symlink(fx_corex_t *corex,
                   const fx_dir_t *dir, const fx_name_t *name)
{
	int rc;

	if (dir_ispseudo(dir)) {
		return -EPERM;
	}
	if (!dir_hasspace(dir)) {
		return -EMLINK;
	}
	rc = let_waccess(corex, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_nolink(corex, dir, name);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_let_mknod(fx_corex_t *corex,
                 const fx_dir_t *dir, const fx_name_t *name)
{
	int rc;

	if (dir_ispseudo(dir)) {
		return -EPERM;
	}
	if (!dir_hasspace(dir)) {
		return -EMLINK;
	}
	rc = let_waccess(corex, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_nolink(corex, dir, name);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_let_mkdir(fx_corex_t *corex,
                 const fx_dir_t *dir, const fx_name_t *name)
{
	int rc;

	if (dir_ispseudo(dir)) {
		return -EPERM;
	}
	if (!dir_hasspace(dir)) {
		return -EMLINK;
	}
	rc = let_waccess(corex, dir);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_nolink(corex, dir, name);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_let_create(fx_corex_t *corex, const fx_dir_t *dir,
                  const fx_name_t *name, fx_mode_t mode)
{
	int rc;
	fx_vtype_e vtype;

	if (dir_ispseudo(dir)) {
		return -EPERM;
	}
	if (!dir_hasspace(dir)) {
		return -EMLINK;
	}
	rc = let_waccess(corex, dir);
	if (rc != 0) {
		return rc;
	}
	vtype = fx_mode_to_vtype(mode);
	if (vtype != FNX_VTYPE_REG) {
		return -EINVAL;
	}
	rc = check_openfh_limit(corex);
	if (rc != 0) {
		return rc;
	}
	rc = fx_let_nolink(corex, dir, name);
	if (rc != 0) {
		return rc;
	}
	return 0;
}


static int
verify_nocycles(fx_corex_t *corex, const fx_dir_t *dir, const fx_dir_t *childd)
{
	int rc = 0;
	fx_ino_t parent_ino;
	fx_dir_t *parentd;
	const fx_dir_t *ditr;

	ditr = childd;
	while (ditr && !dir_isroot(ditr)) {
		if (ditr == dir) {
			rc = -EINVAL;
			break;
		}
		parent_ino = ditr->d_inode.i_parentd;
		if (parent_ino == FNX_INO_NULL) {
			break;
		}
		rc = fx_fetch_dir(corex, parent_ino, &parentd);
		if (rc != 0) {
			break;
		}
		if (ditr == parentd) {
			break;
		}
		ditr = parentd;
	}
	return rc;
}

int fx_let_rename_src(fx_corex_t *corex, const fx_dir_t *parentd,
                      const fx_dir_t *newparentd, const fx_inode_t *inode)
{
	int rc;
	const fx_dir_t *dir;

	if (dir_ispseudo(parentd)) {
		return -EPERM;
	}
	rc = let_waccess(corex, parentd);
	if (rc != 0) {
		return rc;
	}
	if (inode_ispseudo(inode)) {
		return -EPERM;
	}
	rc = verify_sticky(corex, parentd, inode);
	if (rc != 0) {
		return rc;
	}
	if (inode_isdir(inode)) {
		dir = fx_inode_to_dir(inode);
		if (dir_isroot(dir)) {
			return -EBUSY;
		}
		rc = verify_nocycles(corex, dir, newparentd);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

int fx_let_rename_tgt(fx_corex_t *corex, const fx_dir_t *newparentd,
                      const fx_inode_t *curinode)
{
	int rc;
	const fx_dir_t *dir;

	if (dir_ispseudo(newparentd)) {
		return -EPERM;
	}
	if (!dir_hasspace(newparentd)) {
		return -EMLINK;
	}
	rc = let_waccess(corex, newparentd);
	if (rc != 0) {
		return rc;
	}
	if (curinode == NULL) {
		return 0; /* OK, no target */
	}
	if (inode_ispseudo(curinode)) {
		return -EPERM;
	}
	rc = verify_sticky(corex, newparentd, curinode);
	if (rc != 0) {
		return rc;
	}
	if (inode_isdir(curinode)) {
		dir = fx_inode_to_dir(curinode);
		rc  = fx_let_rmdir(corex, newparentd, dir);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}


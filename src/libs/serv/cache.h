/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                         Funex File-System Library                           *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  The Funex file-system library is a free software; you can redistribute it  *
 *  and/or modify it under the terms of the GNU Lesser General Public License  *
 *  as published by the Free Software Foundation; either version 3 of the      *
 *  License, or (at your option) any later version.                            *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_CACHE_H_
#define FUNEX_CACHE_H_

/* Cache threshold defs */
#define FX_CVINODE_LIM      (FNX_DIREXT_NSEGS + FNX_LINK_MAX)
#define FX_CVINODE_MAX      (8 * FX_CVINODE_LIM)
#define FX_CVINODE_MIN      (FX_CVINODE_LIM / 32)


/* Forward declarations */
struct fx_cache;
typedef struct fx_cache fx_cache_t;


/* I/V-nodes hash-table */
struct fx_vihtbl {
	fx_size_t       size;
	fx_vnode_t     *htbl[196613];
};
typedef struct fx_vihtbl fx_vihtbl_t;


/* Blocks hash-table */
struct fx_bkhtbl {
	fx_size_t       size;
	fx_bkref_t     *htbl[393241];
};
typedef struct fx_bkhtbl fx_bkhtbl_t;


/* Dir-entries hash */
struct fx_dentry {
	fx_ino_t        dino;
	fx_hash_t       hash;
	fx_size_t       nlen;
	fx_ino_t        ino;
};
typedef struct fx_dentry fx_dentry_t;

struct fx_dehtbl {
	fx_dentry_t     htbl[196613];
};
typedef struct fx_dehtbl fx_dehtbl_t;


/* V/I-nodes and file-references caching */
struct fx_cache {
	fx_magic_t      magic;          /* Debug checker */
	fx_cstats_t     cstats;         /* Internals counters */
	fx_climits_t    climits;        /* Thresholds */
	fx_dehtbl_t     dehtbl;         /* Dir-entries hash-table */
	fx_vihtbl_t     vhtbl;          /* V-nodes' hash-table */
	fx_vihtbl_t     ihtbl;          /* I-nodes' hash-table */
	fx_bkhtbl_t     ubhtbl;         /* User-blocks hash-table */
	fx_bkhtbl_t     sbhtbl;         /* Meta-blocks hash-table */
	fx_list_t       vdirty;         /* Dirty vnodes */
	fx_list_t       vlru;           /* V/I-nodes LRU queue */
	fx_list_t       udirty;         /* Dirty user-blocks */
	fx_list_t       ulru;           /* User-blocks LRU */
	fx_list_t       sdirty;         /* Dirty meta-blocks */
	fx_list_t       slru;           /* Meta-blocks LRU */
	fx_list_t       frefs;          /* Open-files refs */
	fx_balloc_t    *balloc;         /* Blocks allocator */
	fx_valloc_t    *valloc;         /* V-Elements allocator */

	/* Operations */
	fx_bkref_t *(*new_sec)(fx_cache_t *, const fx_bkaddr_t *);
	fx_bkref_t *(*new_bk)(fx_cache_t *, const fx_bkaddr_t *);
	void (*del_bk)(fx_cache_t *, fx_bkref_t *);
	void (*store_bk)(fx_cache_t *, fx_bkref_t *);
	void (*evict_bk)(fx_cache_t *, fx_bkref_t *);
	fx_bkref_t *(*search_bk)(const fx_cache_t *, const fx_bkaddr_t *);
	void (*stain_bk)(fx_cache_t *, fx_bkref_t *, fx_stain_e);
	fx_bkref_t *(*undirty_bks)(fx_cache_t *, fx_ino_t, fx_size_t);
	fx_bkref_t *(*undirty_sec)(fx_cache_t *);
	fx_bkref_t *(*getlru_bk)(fx_cache_t *);
	fx_bkref_t *(*getlru_sec)(fx_cache_t *);

	fx_vnode_t *(*new_vnode)(fx_cache_t *, fx_vtype_e);
	void (*del_vnode)(fx_cache_t *, fx_vnode_t *);
	void (*store_vnode)(fx_cache_t *, fx_vnode_t *);
	void (*evict_vnode)(fx_cache_t *, fx_vnode_t *);
	fx_inode_t *(*search_inode)(const fx_cache_t *, fx_ino_t);
	fx_vnode_t *(*search_vnode)(const fx_cache_t *, const fx_vaddr_t *);
	void (*stain_vnode)(fx_cache_t *, fx_vnode_t *, fx_stain_e);
	fx_vnode_t *(*undirty_vnode)(fx_cache_t *);
	void (*update_vnode)(fx_cache_t *, fx_vnode_t *);
	fx_vnode_t *(*getlru_vnode)(fx_cache_t *);

	fx_fileref_t *(*new_fileref)(fx_cache_t *);
	void (*del_fileref)(fx_cache_t *, fx_fileref_t *);
	void (*store_fileref)(fx_cache_t *, fx_fileref_t *);
	void (*evict_fileref)(fx_cache_t *, fx_fileref_t *);

	void (*remap_de)(fx_cache_t *, fx_ino_t, fx_hash_t, fx_size_t, fx_ino_t);
	fx_ino_t (*lookup_de)(const fx_cache_t *, fx_ino_t, fx_hash_t, fx_size_t);
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_cache_init(fx_cache_t *, fx_balloc_t *, fx_valloc_t *);

void fx_cache_destroy(fx_cache_t *);

void fx_cache_set_limits(fx_cache_t *, fx_size_t);

void fx_cache_clearall(fx_cache_t *);

void fx_cache_updates(fx_cache_t *);

#endif /* FUNEX_CACHE_H_ */



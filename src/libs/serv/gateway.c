/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"


static void prepare_inamehash(fx_task_t *task)
{
	fx_hash_t *hash;
	fx_ino_t parent;
	const fx_name_t *name;
	fx_request_t *rqst;

	rqst = &task->tsk_request;
	switch ((int)task->tsk_opcode) {
		case FX_OP_LOOKUP:
			parent  = rqst->req_lookup.parent;
			name    = &rqst->req_lookup.name;
			hash    = &rqst->req_lookup.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_MKNOD:
			parent  = rqst->req_mknod.parent;
			name    = &rqst->req_mknod.name;
			hash    = &rqst->req_mknod.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_MKDIR:
			parent  = rqst->req_mkdir.parent;
			name    = &rqst->req_mkdir.name;
			hash    = &rqst->req_mkdir.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_UNLINK:
			parent  = rqst->req_unlink.parent;
			name    = &rqst->req_unlink.name;
			hash    = &rqst->req_unlink.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_RMDIR:
			parent  = rqst->req_rmdir.parent;
			name    = &rqst->req_rmdir.name;
			hash    = &rqst->req_rmdir.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_SYMLINK:
			parent  = rqst->req_symlink.parent;
			name    = &rqst->req_symlink.name;
			hash    = &rqst->req_symlink.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_LINK:
			parent  = rqst->req_link.newparent;
			name    = &rqst->req_link.newname;
			hash    = &rqst->req_link.newhash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_CREATE:
			parent  = rqst->req_create.parent;
			name    = &rqst->req_create.name;
			hash    = &rqst->req_create.hash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		case FX_OP_RENAME:
			parent  = rqst->req_rename.parent;
			name    = &rqst->req_rename.name;
			hash    = &rqst->req_rename.hash;
			*hash   = fx_calc_inamehash(name, parent);
			parent  = rqst->req_rename.newparent;
			name    = &rqst->req_rename.newname;
			hash    = &rqst->req_rename.newhash;
			*hash   = fx_calc_inamehash(name, parent);
			break;
		default:
			break;
	}
}


int fx_preprocess_task(fx_task_t *task)
{
	int rc = 0;
	fx_corex_fn vop;

	if (task->tsk_flags == 0) {
		vop = fx_get_vop(task->tsk_opcode);
		if (vop != NULL) {
			prepare_inamehash(task);
		} else {
			rc = -ENOTSUP;
		}
	}
	return rc;
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxfusei.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"
#include "server.h"


/* Forward declarations */
static void corex_bind_ops(fx_corex_t *corex);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fscore_init(fx_fscore_t *fscore, fx_balloc_t *alloc)
{
	fx_vio_init(&fscore->vio);
	fx_task_init(&fscore->ptask, FX_OP_NONE);
	fscore->balloc   = alloc;
	fscore->super   = NULL;
	fscore->root    = NULL;
	fscore->proot   = NULL;
	fscore->pino    = FNX_INO_PSROOT;
	fscore->mntf    = FNX_MNTF_DEFAULT;
	fscore->uctx    = &fscore->ptask.tsk_uctx;

	fx_iobufs_setup(&fscore->ptask.tsk_iobufs, FNX_INO_NULL, alloc);
}

void fx_fscore_destroy(fx_fscore_t *fscore)
{
	fx_vio_destroy(&fscore->vio);
	fx_task_destroy(&fscore->ptask);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_corex_init(fx_corex_t *corex, fx_fscore_t *fscore, fx_cache_t *cache)
{
	fx_bzero(corex->pblk, sizeof(corex->pblk));
	fx_list_init(&corex->vmods);
	fx_list_init(&corex->pendq);
	corex->fscore   = fscore;
	corex->cache    = cache;
	corex->task     = &fscore->ptask;
	corex->bmark    = 0;
	corex->ntout    = 0;
	corex->nexec    = 0;

	corex_bind_ops(corex);
}

void fx_corex_destroy(fx_corex_t *corex)
{
	fx_list_destroy(&corex->vmods);
	fx_list_destroy(&corex->pendq);
	corex->fscore   = NULL;
	corex->cache    = NULL;
	corex->task     = NULL;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_corex_fn fx_get_vop(fx_opcode_e opc)
{
	unsigned indx, nops = FX_OP_LAST;
	fx_corex_fn hook = NULL;

	indx = (unsigned)opc;
	if (indx < nops) {
		hook = fx_vops_hooks[indx];
	}
	return hook;
}

static const fx_uctx_t *get_uctx(const fx_corex_t *corex)
{
	return &corex->task->tsk_uctx;
}

static fx_super_t *get_super(const fx_corex_t *corex)
{
	return corex->fscore->super;
}

static fx_iobufs_t *get_iobufs(const fx_corex_t *corex)
{
	return &corex->task->tsk_iobufs;
}

static fx_vnode_t *mlink_to_vnode(const fx_link_t *mlnk)
{
	const fx_vnode_t *vnode;

	vnode = fx_container_of(mlnk, fx_vnode_t, v_mlink);
	return (fx_vnode_t *)vnode;
}


static fx_vnode_t *pop_vnode(fx_corex_t *corex)
{
	fx_link_t  *mlink;
	fx_vnode_t *vnode = NULL;

	mlink = fx_list_pop_front(&corex->vmods);
	if (mlink != NULL) {
		vnode = mlink_to_vnode(mlink);
	}
	return vnode;
}

static fx_vnode_t *pop_fuzzy(fx_corex_t *corex)
{
	fx_link_t  *mlink;
	fx_vnode_t *vnode = NULL;

	mlink = fx_list_front(&corex->vmods);
	if (mlink != NULL) {
		vnode = mlink_to_vnode(mlink);
		if (!vnode->v_stable) {
			fx_list_pop_front(&corex->vmods);
		} else {
			vnode = NULL;
		}
	}
	return vnode;
}

static void put_bk(fx_corex_t *corex, fx_bkref_t *bkref)
{
	if (!(bkref->flags & FX_BKREF_CACHED)) {
		corex->cache->store_bk(corex->cache, bkref);
	}
	corex->cache->stain_bk(corex->cache, bkref, FX_STAIN_DIRTY);
}

static void put_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	fx_link_t *mlink;

	mlink = &vnode->v_mlink;
	if (!fx_link_islinked(mlink)) {
		if (vnode->v_stable) {
			fx_list_push_back(&corex->vmods, mlink);
		} else {
			fx_list_push_front(&corex->vmods, mlink);
		}
	}
}

static void put_super(fx_corex_t *corex, fx_super_t *su, fx_flags_t flags)
{
	fx_super_settimes(su, NULL, flags);
	put_vnode(corex, &su->su_vnode);
}

static void put_inode(fx_corex_t *corex, fx_inode_t *inode,
                      fx_flags_t flags)
{
	inode_setitime(inode, flags);
	put_vnode(corex, &inode->i_vnode);
}

static void put_dir(fx_corex_t *corex, fx_dir_t *dir, fx_flags_t flags)
{
	put_inode(corex, &dir->d_inode, flags);
}

static int iscached(const fx_bkref_t *bkref)
{
	return ((bkref->flags & FX_BKREF_CACHED) ||
	        (bkref->stain != FX_STAIN_NONE));
}

static int istrans(const fx_bkref_t *bkref)
{
	return (bkref->flags & (FX_BKREF_RDTRANS | FX_BKREF_WRTRANS));
}

static void discard_bk(fx_corex_t *corex, fx_bkref_t *bkref)
{
	if (!istrans(bkref) && !iscached(bkref) && !bkref->refcnt) {
		corex->cache->del_bk(corex->cache, bkref);
	}
}

static void withdraw_bk(fx_corex_t *corex, fx_bkref_t *bkref)
{
	corex->cache->stain_bk(corex->cache, bkref, FX_STAIN_NONE);
	corex->cache->evict_bk(corex->cache, bkref);
	corex->discard_bk(corex, bkref);
}

static int
fetch_bk(fx_corex_t *corex, const fx_bkaddr_t *bkaddr, fx_bkref_t **p_bkref)
{
	int rc;
	fx_bkref_t *bkref;
	fx_cache_t *cache;

	cache = corex->cache;
	bkref = cache->search_bk(cache, bkaddr);
	if (bkref != NULL) {
		if (!(bkref->flags & FX_BKREF_RDTRANS)) {
			/* Cache-hit, not in read-transition -- all is good */
			*p_bkref = bkref;
			rc = 0;
		} else {
			/* Cache-hit, but currently in read-transition */
			*p_bkref = NULL;
			rc = FX_ERPEND;
		}
	} else {
		/* Cache-miss; create entry, pend on it */
		bkref = cache->new_bk(cache, bkaddr);
		if (bkref != NULL) {
			cache->store_bk(cache, bkref);
			corex->enslave_bk(corex, bkref, FX_XTYPE_RDBK);
			rc = FX_ERPEND;
		} else {
			rc = -ENOMEM;
		}
	}
	return rc;
}

static int
retrieve_sec(fx_corex_t *corex, const fx_bkaddr_t *saddr, fx_bkref_t **p_sec)
{
	int rc;
	fx_bkref_t *sec;
	fx_cache_t *cache = corex->cache;

	sec = cache->new_sec(cache, saddr);
	if (sec == NULL) {
		return -ENOMEM;
	}
	rc = fx_read_meta_bk(corex->fscore, sec);
	if (rc != 0) {
		cache->del_bk(cache, sec);
		return rc;
	}
	cache->store_bk(cache, sec);
	cache->stain_bk(cache, sec, FX_STAIN_LRU);
	*p_sec = sec;
	return 0;
}

static int
replace_sec(fx_corex_t *corex, fx_bkref_t *sec, fx_bkref_t **p_sec)
{
	fx_bkref_t *sec2;
	fx_cache_t *cache = corex->cache;

	sec2 = cache->new_sec(cache, &sec->addr);
	if (sec2 == NULL) {
		return -ENOMEM;
	}
	fx_bkref_clone(sec, sec2);
	cache->evict_bk(cache, sec);
	cache->store_bk(cache, sec2);
	*p_sec = sec2;
	return 0;
}

static int
fetch_sec(fx_corex_t *corex, const fx_bkaddr_t *bkaddr, fx_bkref_t **sec)
{
	int rc = 0;
	fx_bkaddr_t saddr;
	fx_cache_t *cache = corex->cache;

	fx_bkaddr_sfloor(bkaddr, &saddr);
	*sec = cache->search_bk(cache, &saddr);
	if (*sec == NULL) {
		rc = retrieve_sec(corex, &saddr, sec);
	} else if ((*sec)->flags & FX_BKREF_WRTRANS) {
		rc = replace_sec(corex, *sec, sec);
	}
	return rc;
}

static void withdraw_sec(fx_corex_t *corex, fx_bkref_t *sec)
{
	fx_cache_t *cache = corex->cache;

	cache->stain_bk(cache, sec, FX_STAIN_NONE);
	cache->evict_bk(cache, sec);
	cache->del_bk(cache, sec);
}

static int obtain_fref(fx_corex_t *corex, fx_flags_t flags, fx_fileref_t **fref)
{
	*fref = corex->cache->new_fileref(corex->cache);
	if (*fref == NULL) {
		return -ENOMEM;
	}

	fx_fileref_setup(*fref, flags);
	corex->cache->store_fileref(corex->cache, *fref);
	return 0;
}

static void discard_fref(fx_corex_t *corex, fx_fileref_t *fref)
{
	corex->cache->evict_fileref(corex->cache, fref);
	corex->cache->del_fileref(corex->cache, fref);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void pend_bk(fx_corex_t *corex, fx_bkref_t *bkref)
{
	fx_list_push_back(&corex->pendq, &bkref->xelem.plink);
}

static void unpend_bk(fx_corex_t *corex, fx_bkref_t *bkref)
{
	fx_list_remove(&corex->pendq, &bkref->xelem.plink);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_server_t *corex_to_server(const fx_corex_t *corex)
{
	return fx_container_of(corex, fx_server_t, corex);
}

static void enslave_bk(fx_corex_t *corex, fx_bkref_t *bkref, int xtype)
{
	fx_server_enslave_bk(corex_to_server(corex), bkref, xtype);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void corex_bind_ops(fx_corex_t *corex)
{
	corex->put_bk       = put_bk;
	corex->put_vnode    = put_vnode;
	corex->put_super    = put_super;
	corex->put_inode    = put_inode;
	corex->put_dir      = put_dir;
	corex->pop_vnode    = pop_vnode;
	corex->pop_fuzzy    = pop_fuzzy;
	corex->get_uctx     = get_uctx;
	corex->get_super    = get_super;
	corex->get_iobufs   = get_iobufs;
	corex->discard_bk   = discard_bk;
	corex->withdraw_bk  = withdraw_bk;
	corex->fetch_bk     = fetch_bk;
	corex->fetch_sec    = fetch_sec;
	corex->withdraw_sec = withdraw_sec;
	corex->obtain_fref  = obtain_fref;
	corex->discard_fref = discard_fref;
	corex->pend_bk      = pend_bk;
	corex->unpend_bk    = unpend_bk;
	corex->enslave_bk   = enslave_bk;
}



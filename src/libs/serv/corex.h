/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_COREX_H_
#define FUNEX_COREX_H_


/* Internal status-codes to indicate task is pending for read/write */
#define FX_ERPEND           (-11001)
#define FX_EWPEND           (-11002)

/* Internal status-code to indicate end-of-stream upon readdir */
#define FX_EEOS             (-11003)


struct fx_fscore;
struct fx_corex;

typedef struct fx_corex  fx_corex_t;
typedef struct fx_fscore fx_fscore_t;
typedef int (*fx_corex_fn)(fx_corex_t *);

extern fx_corex_fn fx_vops_hooks[];


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * File-system's core module.
 */
struct fx_fscore {
	fx_super_t     *super;          /* Super-block */
	fx_dir_t       *root;           /* Root directory */
	fx_dir_t       *proot;          /* Pseudo root */
	fx_ino_t        pino;           /* Pseudo-ino generator */
	fx_mntf_t       mntf;           /* Mount options bit-flags */
	fx_uctx_t      *uctx;           /* Internal user context */
	fx_balloc_t    *balloc;         /* Blocks allocator */
	fx_task_t       ptask;          /* Private task */
	fx_vio_t        vio;            /* Block-volume I/O interface */
};


/*
 * Core-executor: execute file-system task over fscore module.
 */
struct fx_corex {
	fx_fscore_t    *fscore;         /* Reference to fscore module */
	fx_cache_t     *cache;          /* Objects/blocks cache */
	fx_task_t      *task;           /* Current task of execution */
	fx_blk_t        pblk;           /* Pseudo-info block */
	fx_list_t       vmods;          /* Modified vnodes */
	fx_list_t       pendq;          /* Blocks/tasks pending queue */
	fx_bkcnt_t      bmark;          /* Blocks-allocation water-mark */
	fx_size_t       ntout;          /* Number of consecutive timeouts */
	fx_size_t       nexec;          /* Number of consecutive executes */

	/* Hooks */
	fx_super_t  *(*get_super)(const fx_corex_t *);
	fx_iobufs_t *(*get_iobufs)(const fx_corex_t *);
	const fx_uctx_t *(*get_uctx)(const fx_corex_t *);

	fx_vnode_t  *(*pop_vnode)(fx_corex_t *);
	fx_vnode_t  *(*pop_fuzzy)(fx_corex_t *);
	void (*put_bk)(fx_corex_t *, fx_bkref_t *);
	void (*put_vnode)(fx_corex_t *, fx_vnode_t *);
	void (*put_super)(fx_corex_t *, fx_super_t *, fx_flags_t);
	void (*put_inode)(fx_corex_t *, fx_inode_t *, fx_flags_t);
	void (*put_dir)(fx_corex_t *, fx_dir_t *, fx_flags_t);

	int (*fetch_bk)(fx_corex_t *, const fx_bkaddr_t *, fx_bkref_t **);
	void (*discard_bk)(fx_corex_t *, fx_bkref_t *);
	void (*withdraw_bk)(fx_corex_t *, fx_bkref_t *);
	void (*pend_bk)(fx_corex_t *, fx_bkref_t *);
	void (*unpend_bk)(fx_corex_t *, fx_bkref_t *);
	int (*fetch_sec)(fx_corex_t *, const fx_bkaddr_t *, fx_bkref_t **);
	void (*withdraw_sec)(fx_corex_t *, fx_bkref_t *);
	void (*enslave_bk)(fx_corex_t *, fx_bkref_t *, int);

	int (*obtain_fref)(fx_corex_t *, fx_flags_t, fx_fileref_t **);
	void (*discard_fref)(fx_corex_t *, fx_fileref_t *);
};


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fscore_init(fx_fscore_t *, fx_balloc_t *);

void fx_fscore_destroy(fx_fscore_t *);


void fx_corex_init(fx_corex_t *, fx_fscore_t *, fx_cache_t *);

void fx_corex_destroy(fx_corex_t *);


fx_corex_fn fx_get_vop(fx_opcode_e);

void fx_count_vop(const fx_corex_t *);

int fx_bind_pseudo_namespace(fx_corex_t *);


#endif /* FUNEX_COREX_H_ */




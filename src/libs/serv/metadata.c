/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_vnode_t *new_vnode(fx_corex_t *corex, fx_vtype_e vtype)
{
	return corex->cache->new_vnode(corex->cache, vtype);
}

static fx_vnode_t *new_vvnode(fx_corex_t *corex, const fx_vaddr_t *vaddr)
{
	fx_vnode_t *vnode;

	if ((vnode = new_vnode(corex, vaddr->vtype)) != NULL) {
		vnode_setvaddr(vnode, vaddr);
	}
	return vnode;
}

static fx_inode_t *new_inode(fx_corex_t *corex, fx_vtype_e vtype, fx_ino_t ino)
{
	fx_vnode_t *vnode = NULL;
	fx_inode_t *inode = NULL;

	vnode = new_vnode(corex, vtype);
	if (vnode != NULL) {
		inode = vnode_to_inode(vnode);
		inode->i_super = corex->get_super(corex);
		fx_inode_setino(inode, ino);
	}
	return inode;
}

static void del_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	corex->cache->del_vnode(corex->cache, vnode);
}

static void del_inode(fx_corex_t *corex, fx_inode_t *inode)
{
	del_vnode(corex, &inode->i_vnode);
}

static void store_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	corex->cache->store_vnode(corex->cache, vnode);
}

static void store_inode(fx_corex_t *corex, fx_inode_t *inode)
{
	store_vnode(corex, &inode->i_vnode);
}

static void evict_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	if (vnode->v_cached) {
		corex->cache->evict_vnode(corex->cache, vnode);
	}
}

static fx_vnode_t *search_vnode(fx_corex_t *corex, const fx_vaddr_t *vaddr)
{
	return corex->cache->search_vnode(corex->cache, vaddr);
}

static fx_inode_t *search_inode(fx_corex_t *corex, fx_ino_t ino)
{
	return corex->cache->search_inode(corex->cache, ino);
}

static fx_spmap_t *search_spmap(fx_corex_t *corex, fx_lba_t ulba)
{
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode;

	vaddr_for_spmap(&vaddr, ulba);
	vnode = search_vnode(corex, &vaddr);
	return (vnode != NULL) ? fx_vnode_to_spmap(vnode) : NULL;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int stage_susec(fx_corex_t *corex, fx_bkref_t **bkref)
{
	fx_lba_t lba;
	fx_bkaddr_t bkaddr;

	lba = sec_to_lba(FNX_SEC_SUPER);
	fx_bkaddr_setup(&bkaddr, FNX_SVOL_META, lba, FNX_FRG_SUPER);
	return corex->fetch_sec(corex, &bkaddr, bkref);
}

static int
stage_visec(fx_corex_t *corex, const fx_vaddr_t *vaddr, fx_bkref_t **bkref)
{
	fx_bkaddr_t bkaddr;
	const fx_super_t  *super;
	const fx_extent_t *index;

	super = corex->get_super(corex);
	index = &super->su_layout.l_index;
	if (fx_vaddr_smap(vaddr, &bkaddr) != 0) {
		fx_vaddr_hmap(vaddr, index, &bkaddr);
	}
	return corex->fetch_sec(corex, &bkaddr, bkref);
}

static int stage_isec(fx_corex_t *corex, fx_ino_t ino, fx_bkref_t **bkref)
{
	int rc;
	fx_vaddr_t vaddr;

	/* Special case for root-inode */
	if (ino == FNX_INO_ROOT) {
		rc = stage_susec(corex, bkref);
	} else {
		vaddr_for_anyino(&vaddr, ino);
		rc = stage_visec(corex, &vaddr, bkref);
	}
	return rc;
}

static void
load_vnode(fx_vnode_t *vnode, const fx_bkref_t *sec, const fx_header_t *hdr)
{
	fx_vtype_e vtype;
	const fx_dfrg_t *dfrg;

	vtype = fx_dobj_vtype(hdr);
	dfrg  = header_to_dfrg(hdr);

	fx_dimport_vobj(vnode, hdr);
	fx_assert(vnode->v_vaddr.vtype == vtype);
	fx_vnode_setbkaddr(vnode, sec, dfrg);
	vnode->v_stable = FNX_TRUE;
}

static void
load_inode(fx_inode_t *inode, const fx_bkref_t *sec, const fx_header_t *hdr)
{
	load_vnode(&inode->i_vnode, sec, hdr);
}

static fx_header_t *
find_dvnode(fx_corex_t *corex, const fx_bkref_t *sec, const fx_vaddr_t *vaddr)
{
	int rc;
	fx_dfrg_t *dfrg;
	fx_header_t *hdr = NULL;
	const fx_dsec_t *dsec;

	dsec = fx_get_dsec(sec);
	dfrg = fx_dsec_search_vnode(dsec, vaddr);
	if (dfrg != NULL) {
		hdr = &dfrg->fr_hdr;
		rc = fx_dobj_check(hdr, FNX_VTYPE_ANY);
		fx_assert(rc == 0); /* XXX */
	} else {
		/* May be OK */
		fx_unused(corex);
	}
	return hdr;
}

static int stage_super(fx_corex_t *corex, fx_super_t **super)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_header_t *hdr    = NULL;
	fx_bkref_t  *sec    = NULL;
	fx_vnode_t  *vnode  = NULL;

	rc = stage_susec(corex, &sec);
	if (rc != 0) {
		return rc;
	}
	vaddr_for_super(&vaddr);
	hdr = find_dvnode(corex, sec, &vaddr);
	if (hdr == NULL) {
		return -ENOENT; /* Always error in case of super-section */
	}
	vnode = new_vnode(corex, vaddr.vtype);
	if (vnode == NULL) {
		return -ENOMEM;
	}
	load_vnode(vnode, sec, hdr);
	store_vnode(corex, vnode);

	*super = fx_vnode_to_super(vnode);
	return 0;
}

static int
stage_vnode(fx_corex_t *corex, const fx_vaddr_t *vaddr, fx_vnode_t **p_vnode)
{
	int rc;
	fx_vnode_t *vnode;
	fx_header_t *hdr = NULL;
	fx_bkref_t  *sec = NULL;

	rc = stage_visec(corex, vaddr, &sec);
	if (rc != 0) {
		return rc;
	}
	hdr = find_dvnode(corex, sec, vaddr);
	if (hdr == NULL) {
		return -ENOENT; /* May be OK */
	}
	vnode = new_vnode(corex, vaddr->vtype);
	if (vnode == NULL) {
		return -ENOMEM;
	}
	load_vnode(vnode, sec, hdr);
	store_vnode(corex, vnode);
	*p_vnode = vnode;
	return 0;
}

static int
stage_spmap(fx_corex_t *corex, fx_lba_t ulba, fx_spmap_t **spmap)
{
	int rc;
	fx_vaddr_t vaddr;
	fx_vnode_t *vnode = NULL;

	vaddr_for_spmap(&vaddr, ulba);
	rc = stage_vnode(corex, &vaddr, &vnode);
	if (rc != 0) {
		return rc;
	}
	*spmap = fx_vnode_to_spmap(vnode);
	return 0;
}

static int search_dinode(const fx_bkref_t *sec, fx_ino_t ino, fx_header_t **hdr)
{
	int rc;
	fx_dsec_t *dsec;
	fx_dfrg_t *dfrg;

	dsec = fx_get_dsec(sec);
	dfrg = fx_dsec_search_inode(dsec, ino);
	if (dfrg == NULL) {
		return -ENOENT;
	}
	*hdr = &dfrg->fr_hdr;
	rc = fx_dobj_icheck(*hdr);
	fx_assert(rc == 0); /* XXX */
	return rc;
}

static int stage_inode(fx_corex_t *corex, fx_ino_t ino, fx_inode_t **p_inode)
{
	int rc;
	fx_vtype_e vtype;
	fx_inode_t  *inode;
	fx_bkref_t  *sec = NULL;
	fx_header_t *hdr = NULL;

	rc = stage_isec(corex, ino, &sec);
	if (rc != 0) {
		return rc;
	}
	rc = search_dinode(sec, ino, &hdr);
	if (rc != 0) {
		return rc;
	}
	vtype = fx_dobj_vtype(hdr);
	inode = new_inode(corex, vtype, ino);
	if (inode == NULL) {
		return -ENOMEM;
	}
	load_inode(inode, sec, hdr);
	store_inode(corex, inode);
	*p_inode = inode;
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_fetch_vnode(fx_corex_t *corex,
                   const fx_vaddr_t *vaddr, fx_vnode_t **vnode)
{
	int rc = 0;

	*vnode = search_vnode(corex, vaddr);
	if (*vnode == NULL) {
		rc = stage_vnode(corex, vaddr, vnode);
	}
	return rc;
}

int fx_fetch_spmap(fx_corex_t *corex, fx_lba_t ulba, fx_spmap_t **spmap)
{
	int rc = 0;

	*spmap = search_spmap(corex, ulba);
	if (*spmap == NULL) {
		rc = stage_spmap(corex, ulba, spmap);
	}
	return rc;
}

int fx_fetch_super(fx_corex_t *corex, fx_super_t **super)
{
	int rc;

	/* Normal case: direct access */
	*super = corex->fscore->super;
	if (*super != NULL) {
		return 0;
	}

	/* Bootstrap case: stage */
	rc = stage_super(corex, super);
	fx_assert(rc == 0); /* XXX */
	return rc;
}

int fx_fetch_wsuper(fx_corex_t *corex, fx_super_t **super)
{
	int rc;
	const fx_mntf_t mntf = corex->fscore->mntf;

	if (mntf & FNX_MNTF_RDONLY) {
		rc = -EROFS;
	} else {
		rc = fx_fetch_super(corex, super);
	}
	return rc;
}

static int stage_iref(fx_corex_t *corex, fx_inode_t *inode)
{
	int rc;
	fx_ino_t refino;
	fx_vtype_e vtype;
	fx_inode_t *iref = NULL;

	refino  = inode->i_refino;
	vtype   = inode_vtype(inode);
	if (vtype != FNX_VTYPE_REFLNK) {
		return 0; /* No-op, OK */
	}
	rc = fx_check_ino(refino);
	if (rc != 0) {
		return -EINVAL; /* XXX Need other error: emetadata? */
	}
	iref = search_inode(corex, refino);
	if (iref != NULL) {
		return 0; /* Cache hit */
	}
	rc = stage_inode(corex, refino, &iref);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_fetch_inode(fx_corex_t *corex, fx_ino_t ino, fx_inode_t **inode)
{
	int rc;

	rc = fx_check_ino(ino);
	if (rc != 0) {
		return -EINVAL;
	}
	*inode = search_inode(corex, ino);
	if (*inode != NULL) {
		return 0; /* Cache hit */
	}
	rc = stage_inode(corex, ino, inode);
	if (rc != 0) {
		return rc;
	}
	rc = stage_iref(corex, *inode);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fx_fetch_dir(fx_corex_t *corex, fx_ino_t ino, fx_dir_t **dir)
{
	int rc;
	fx_inode_t *inode = NULL;

	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc != 0) {
		return rc;
	}
	if (!inode_isdir(inode)) {
		return -ENOTDIR;
	}
	*dir = fx_inode_to_dir(inode);
	return 0;
}

static int
wregfile_by_fileref(const fx_fileref_t *fref, fx_ino_t ino, fx_inode_t **inode)
{
	if (fref->f_inode == NULL) {
		return -EINVAL;
	}
	if (inode_getino(fref->f_inode) != ino) {
		return -EINVAL;
	}
	if (!inode_isreg(fref->f_inode)) {
		return -EINVAL;
	}
	if (!fref->f_writeable) {
		return -EACCES;
	}
	*inode = fref->f_inode;
	return 0;
}

int fx_fetch_wreg(fx_corex_t *corex, const fx_fileref_t *fref,
                  fx_ino_t ino, fx_inode_t **reg)
{
	int rc;

	if (fref != NULL) {
		rc = wregfile_by_fileref(fref, ino, reg);
		if (rc != 0) {
			return rc;
		}
	} else {
		rc = fx_fetch_inode(corex, ino, reg);
		if (rc != 0) {
			return rc;
		}
		if (!inode_isreg(*reg)) {
			return -EINVAL;
		}
		rc = fx_let_access(corex, *reg, W_OK);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

int fx_fetch_symlnk(fx_corex_t *corex, fx_ino_t ino, fx_symlnk_t **symlnk)
{
	int rc;
	fx_inode_t *inode = NULL;

	rc = fx_fetch_inode(corex, ino, &inode);
	if (rc == 0) {
		if (inode_issymlnk(inode)) {
			*symlnk = fx_inode_to_symlnk(inode);
		} else {
			rc = -EINVAL;
		}
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int chop_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	int rc;
	fx_vtype_e vtype;
	fx_bkref_t *sec  = NULL;
	fx_dsec_t  *dsec = NULL;
	fx_dfrg_t  *dfrg = NULL;
	const fx_vaddr_t *vaddr;

	vaddr = &vnode->v_vaddr;
	vtype = vaddr->vtype;
	rc = stage_visec(corex, vaddr, &sec);
	fx_assert(rc == 0); /* FIXME */
	fx_assert(sec != NULL); /* FIXME */

	dsec = fx_get_dsec(sec);
	dfrg = fx_dsec_search_free(dsec, vtype);
	fx_assert(dfrg != NULL); /* XXX */
	if (dfrg == NULL) {
		return -ENOSPC;
	}
	fx_dsec_chopat(dsec, dfrg, vtype);
	fx_vnode_setbkaddr(vnode, sec, dfrg);
	corex->put_bk(corex, sec);
	return 0;
}

static void account_vnode(fx_corex_t *corex, const fx_vnode_t *vnode, int n)
{
	fx_size_t  *cnt;
	fx_vtype_e  vtype;
	fx_super_t *super;

	super = corex->get_super(corex);
	vtype = vnode_vtype(vnode);
	cnt = fx_vstats_getcnt(&super->su_vstats, vtype);
	fx_assert(cnt != NULL);
	if (n > 0) {
		*cnt += (fx_size_t)n;
	} else {
		*cnt -= (fx_size_t)(-n);
	}
	corex->put_super(corex, super, 0);
}

int fx_acquire_vnode(fx_corex_t *corex,
                     const fx_vaddr_t *vaddr, fx_vnode_t **p_vnode)
{
	int rc;
	fx_vnode_t *vnode;

	vnode = new_vvnode(corex, vaddr);
	if (vnode == NULL) {
		return -ENOMEM;
	}
	rc = chop_vnode(corex, vnode);
	if (rc != 0) {
		del_vnode(corex, vnode);
		return rc;
	}
	account_vnode(corex, vnode, 1);
	store_vnode(corex, vnode);
	corex->put_vnode(corex, vnode);
	*p_vnode = vnode;
	return 0;
}

static int alloc_ino(fx_corex_t *corex, fx_ino_t *ino)
{
	int rc;
	fx_super_t *super;
	const fx_uctx_t *uctx;

	super = corex->get_super(corex);
	uctx  = &corex->task->tsk_uctx;
	rc = fx_consume_ino(super, fx_isprivileged(uctx), ino);
	if (rc != 0) {
		return -ENOSPC;
	}
	corex->put_super(corex, super, FNX_MTIME_NOW);
	return 0;
}

static int
acquire_inode(fx_corex_t *corex, fx_vtype_e vtype, fx_inode_t **p_inode)
{
	int rc;
	fx_ino_t ino;
	fx_inode_t *inode;

	fx_assert(fx_isitype(vtype));

	rc = alloc_ino(corex, &ino);
	if (rc != 0) {
		return rc;
	}
	inode = new_inode(corex, vtype, ino);
	if (inode == NULL) {
		return -ENOMEM;
	}
	rc = chop_vnode(corex, &inode->i_vnode);
	if (rc != 0) {
		del_inode(corex, inode);
		return rc;
	}
	account_vnode(corex, &inode->i_vnode, 1);
	fx_super_fillnewi(inode->i_super, inode);
	store_inode(corex, inode);

	corex->put_inode(corex, inode, FNX_BTIME_NOW);
	*p_inode = inode;
	return 0;
}

int fx_acquire_special(fx_corex_t *corex, fx_mode_t mode, fx_inode_t **inode)
{
	int rc;
	fx_vtype_e vtype;

	vtype = fx_mode_to_vtype(mode);
	if ((vtype != FNX_VTYPE_CHRDEV) &&
	    (vtype != FNX_VTYPE_BLKDEV) &&
	    (vtype != FNX_VTYPE_SOCK) &&
	    (vtype != FNX_VTYPE_FIFO)) {
		return -EINVAL;
	}
	rc = acquire_inode(corex, vtype, inode);
	if (rc != 0) {
		return rc;
	}
	fx_inode_setup(*inode, corex->get_uctx(corex), mode, 0);
	return 0;
}

static fx_vtype_e slnk_to_vtype(const fx_path_t *path)
{
	fx_vtype_e vtype = FNX_VTYPE_NONE;

	if (path->len < FNX_SSYMLNK_MAX) {
		vtype = FNX_VTYPE_SSYMLNK;
	} else if (path->len < FNX_SYMLNK_MAX) {
		vtype = FNX_VTYPE_SYMLNK;
	}
	return vtype;
}

int fx_acquire_symlnk(fx_corex_t *corex, const fx_path_t *path,
                      fx_symlnk_t **symlnk)
{
	int rc;
	fx_vtype_e   vtype;
	fx_inode_t  *inode = NULL;

	vtype = slnk_to_vtype(path);
	if (vtype == FNX_VTYPE_NONE) {
		return -EINVAL;
	}
	rc = acquire_inode(corex, vtype, &inode);
	if (rc != 0) {
		return rc;
	}
	*symlnk = fx_inode_to_symlnk(inode);
	fx_symlnk_setup(*symlnk, corex->get_uctx(corex), path);
	return 0;
}

int fx_acquire_reflnk(fx_corex_t *corex, fx_inode_t *iref, fx_inode_t **inode)
{
	int rc;

	rc = acquire_inode(corex, FNX_VTYPE_REFLNK, inode);
	if (rc == 0) {
		fx_inode_setup(*inode, corex->get_uctx(corex), 0, 0);

		(*inode)->i_refino = inode_getino(iref);
	}
	return rc;
}

static int isnotdir(fx_mode_t mode)
{
	fx_mode_t mask;

	mask = (S_IFMT & ~S_IFDIR);
	return ((mode & mask) != 0);
}

int fx_acquire_dir(fx_corex_t *corex, fx_mode_t mode, fx_dir_t **dir)
{
	int rc;
	fx_inode_t *inode = NULL;

	if (isnotdir(mode)) {
		/* NB: Negative check here due to FUSE mode without dir mode bits on */
		return -EINVAL;
	}
	rc = acquire_inode(corex, FNX_VTYPE_DIR, &inode);
	if (rc != 0) {
		return rc;
	}
	*dir = fx_inode_to_dir(inode);
	fx_dir_setup(*dir, corex->get_uctx(corex), mode);
	return 0;
}

int fx_acquire_reg(fx_corex_t *corex, fx_mode_t mode, fx_inode_t **inode)
{
	int rc;

	rc = acquire_inode(corex, FNX_VTYPE_REG, inode);
	if (rc != 0) {
		return rc;
	}
	fx_inode_setup(*inode, corex->get_uctx(corex), mode, 0);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void undo_modv(fx_corex_t *corex, fx_vnode_t *vnode)
{
	fx_link_t *mlink;

	mlink = &vnode->v_mlink;
	if (fx_link_islinked(mlink)) {
		fx_list_remove(&corex->vmods, mlink);
	}
}

static int rejoin_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	int rc;
	fx_dsec_t *dsec;
	fx_dfrg_t *dfrg;
	fx_bkref_t *sec = NULL;
	const fx_bkaddr_t *bkaddr   = &vnode->v_bkaddr;

	rc = corex->fetch_sec(corex, bkaddr, &sec);
	if (rc != 0) {
		return rc;
	}
	dfrg = fx_get_dfrg(sec, bkaddr);
	fx_assert(dfrg != NULL);
	if (dfrg == NULL) {
		return -EIO;
	}
	dsec = fx_get_dsec(sec);
	fx_dsec_joinat(dsec, dfrg, vnode_vtype(vnode));
	corex->put_bk(corex, sec);
	return 0;
}

int fx_reclaim_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	int rc;

	/* Revert from staging-list */
	undo_modv(corex, vnode);

	/* Evict if cached */
	evict_vnode(corex, vnode);

	/* Reclaim space only if last ref */
	if (vnode->v_refcnt != 0) { /* XXX */
		return 0;
	}
	rc = rejoin_vnode(corex, vnode);
	if (rc != 0) {
		return rc;
	}
	account_vnode(corex, vnode, -1);
	del_vnode(corex, vnode);
	return 0;
}

int fx_reclaim_inode(fx_corex_t *corex, fx_inode_t *inode)
{
	int rc;
	fx_ino_t ino;
	fx_super_t *super;

	super = corex->get_super(corex);
	ino = inode_getino(inode);
	rc  = fx_relinquish_ino(super, ino);
	fx_assert(rc == 0);

	rc = fx_reclaim_vnode(corex, &inode->i_vnode);
	if (rc != 0) {
		return rc;
	}
	return 0;
}




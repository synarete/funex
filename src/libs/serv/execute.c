/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <limits.h>

#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxfusei.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"
#include "server.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const char *mntpoint(const fx_server_t *server)
{
	return server->fusei.fi_mntpoint;
}

static int isemptyq(const fx_execq_t *execq)
{
	return fx_execq_isempty(execq);
}

static int hasdirty(const fx_server_t *server)
{
	const fx_cache_t *cache = &server->cache;

	return (!list_isempty(&cache->sdirty) ||
	        !list_isempty(&cache->vdirty) ||
	        !list_isempty(&cache->udirty));
}

static int haspending(const fx_server_t *server)
{
	return !list_isempty(&server->corex.pendq);
}

static int haspendirty(const fx_server_t *server)
{
	return haspending(server) || hasdirty(server);
}

static int hasqueued(const fx_server_t *server)
{
	if (haspending(server)) {
		return FNX_TRUE;
	}
	if (!isemptyq(&server->fusei_taskq)) {
		return FNX_TRUE;
	}
	if (!isemptyq(&server->filter_taskq)) {
		return FNX_TRUE;
	}
	if (!isemptyq(&server->core_execq)) {
		return FNX_TRUE;
	}
	if (!isemptyq(&server->slave_execq)) {
		return FNX_TRUE;
	}
	return FNX_FALSE;
}

static int isactive(const fx_server_t *server)
{
	/* Normal case (run-time common) */
	if (server->active > 0) {
		return FNX_TRUE;
	}
	/* Tear-down case; make sure all queues are flushed */
	if (hasqueued(server)) {
		return FNX_TRUE;
	}
	return FNX_FALSE;
}

static fx_task_t *getnexttask(fx_task_t *tlist)
{
	fx_link_t *next;
	fx_task_t *task = NULL;

	next = tlist->tsk_xelem.qlink.next;
	if (next != NULL) {
		task = fx_link_to_task(next);
	}
	return task;
}

static fx_bkref_t *getnextbk(fx_bkref_t *bklst)
{
	fx_link_t *next;
	fx_bkref_t *bkref = NULL;

	next = bklst->xelem.qlink.next;
	if (next != NULL) {
		bkref = fx_qlink_to_bkref(next);
	}
	return bkref;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void execute_ioslaveq(fx_server_t *server)
{
	int rc = 0;
	fx_xtype_e xtype;
	fx_bkref_t *bkref, *bklst;
	fx_fscore_t *fscore = &server->fscore;

	bklst  = fx_recv_bks(&server->slave_execq, 100, 100000);
	while ((bkref = bklst) != NULL) {
		bklst = getnextbk(bklst);
		xtype = bkref->xelem.xtype;
		switch (xtype) {
			case FX_XTYPE_RDBK:
				rc = fx_read_data_bk(fscore, bkref);
				fx_assert(rc == 0);
				break;
			case FX_XTYPE_WRBK:
				rc = fx_write_data_bk(fscore, bkref);
				fx_assert(rc == 0);
				break;
			case FX_XTYPE_WSEC:
				rc = fx_write_meta_bk(fscore, bkref);
				fx_assert(rc == 0);
				break;
			case FX_XTYPE_NONE:
			case FX_XTYPE_TASK:
			default:
				fx_assert(0);
				break;
		}
		bkref->xelem.status = rc;
		fx_send_bk(&server->core_execq, bkref);
	}
}

static void run_slave(void *p)
{
	fx_server_t *server = (fx_server_t *)p;

	fx_info("start-execute-io-slave: mntpoint=%s", mntpoint(server));
	while (isactive(server)) {
		execute_ioslaveq(server);
	}
	fx_info("done-execute-io-slave: mntpoint=%s", mntpoint(server));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_server_enslave_bk(fx_server_t *server, fx_bkref_t *bkref, int xtype)
{
	fx_corex_t *corex = &server->corex;

	bkref->xelem.xtype  = xtype;
	if (xtype == FX_XTYPE_RDBK) {
		if (!(bkref->flags & FX_BKREF_RDTRANS)) {
			bkref->flags |= FX_BKREF_RDTRANS;
			fx_send_bk(&server->slave_execq, bkref);
			corex->pend_bk(corex, bkref);
		}
	} else if (xtype == FX_XTYPE_WRBK) {
		bkref->flags |= FX_BKREF_WRTRANS;
		fx_send_bk(&server->slave_execq, bkref);
		corex->pend_bk(corex, bkref);
	} else if (xtype == FX_XTYPE_WSEC) {
		if (!(bkref->flags & FX_BKREF_WRTRANS)) {
			bkref->flags |= FX_BKREF_WRTRANS;
			fx_send_bk(&server->slave_execq, bkref);
			corex->pend_bk(corex, bkref);
		}
	} else {
		fx_assert(0);
	}
}

/*
 * Re-export node to its associated meta-data section. If a node is marked as
 * 'removed', no need to re-export. Nothing to do here for pseudo inodes.
 */
static int isunstable(const fx_vnode_t *vnode)
{
	int res;

	res = (vnode->v_cached && !vnode->v_stable);
	if (res) {
		fx_assert(vnode->v_vaddr.vtype != FNX_VTYPE_REFLNK);
		fx_assert(vnode->v_vaddr.vtype != FNX_VTYPE_SEGMNT);
		fx_assert(vnode->v_vaddr.vtype != FNX_VTYPE_SLICE);
		fx_assert(vnode->v_vaddr.vtype != FNX_VTYPE_DIREXT);
		fx_assert(vnode->v_vaddr.vtype != FNX_VTYPE_DIRSEG);
		fx_assert(vnode->v_vaddr.vtype != FNX_VTYPE_SPMAP);
	}
	return res;
}

static void collect_garbage(fx_corex_t *corex)
{
	fx_vnode_t *vnode;

	if (task_status(corex->task) != 0) {
		while ((vnode = corex->pop_fuzzy(corex)) != NULL) {
			if (vnode_ispseudo(vnode)) {
				continue;
			}
			fx_assert(isunstable(vnode));
			fx_reclaim_vnode(corex, vnode);
		}
	}
}

static void update_changes(fx_corex_t *corex)
{
	fx_vnode_t *vnode;
	fx_cache_t *cache = corex->cache;

	while ((vnode = corex->pop_vnode(corex)) != NULL) {
		if (!vnode->v_pseudo && !vnode->v_removed) {
			vnode->v_stable = FNX_TRUE;
			cache->stain_vnode(cache, vnode, FX_STAIN_DIRTY);
		}
	}
}

static void complete_vop(fx_corex_t *corex)
{
	collect_garbage(corex);
	update_changes(corex);
}

static fx_xelem_t *plink_to_xelem(const fx_link_t *plink)
{
	return fx_container_of(plink, fx_xelem_t, plink);
}

static void pend_task(fx_corex_t *corex, fx_task_t *task, int st)
{
	fx_assert((st == FX_ERPEND) || (st == FX_EWPEND));

	task->tsk_xelem.status = st;
	fx_list_push_back(&corex->pendq, &task->tsk_xelem.plink);
}

static fx_task_t *unpend_front_task(fx_corex_t *corex)
{
	fx_link_t  *plnk;
	fx_xelem_t *xelem;
	fx_task_t  *task = NULL;

	plnk = fx_list_front(&corex->pendq);
	if (plnk != NULL) {
		xelem = plink_to_xelem(plnk);
		if (xelem->xtype == FX_XTYPE_TASK) {
			fx_list_pop_front(&corex->pendq);
			task = fx_xelem_to_task(xelem);
		}
	}
	return task;
}

static int execute_vop(fx_corex_t *corex, fx_task_t *task)
{
	int rc;
	fx_corex_fn vop;

	/* Call hook */
	vop = fx_get_vop(task->tsk_opcode);
	if ((rc = vop(corex)) == 0) {
		fx_count_vop(corex);
	}

	/* Post-op closing */
	complete_vop(corex);

	return rc;
}

static void reply_cleared_task(fx_server_t *server, fx_task_t *task)
{
	task->tsk_xelem.status = 0;
	fx_send_task(&server->fusei_taskq, task, FX_TASKF_CLEARED);
}

static void reply_normal_task(fx_server_t *server, fx_task_t *task, int st)
{
	task->tsk_xelem.status = st;
	fx_send_task(&server->fusei_taskq, task, 0);
}

static void
execute_task(fx_server_t *server, fx_corex_t *corex, fx_task_t *task)
{
	int rc;

	corex->task = task;
	if (task->tsk_flags & FX_TASKF_PURIFY) {
		fx_relax_data(corex);
		reply_cleared_task(server, task);
	} else {
		task->tsk_xelem.status = 0;
		rc = execute_vop(corex, task);
		if ((rc == FX_ERPEND) || (rc == FX_EWPEND)) {
			pend_task(corex, task, rc);
		} else {
			reply_normal_task(server, task, rc);
		}
	}
	corex->task = NULL;
}

static void execute_ioreply(fx_corex_t *corex, fx_bkref_t *bkref)
{
	corex->unpend_bk(corex, bkref);
	bkref->xelem.xtype = FX_XTYPE_NONE;
	bkref->flags &= ~(FX_BKREF_WRTRANS | FX_BKREF_RDTRANS);
	if (!(bkref->flags & FX_BKREF_CACHED)) {
		corex->discard_bk(corex, bkref);
	} else {
		fx_assert(bkref->stain != FX_STAIN_DIRTY);
		corex->cache->stain_bk(corex->cache, bkref, FX_STAIN_LRU);
	}
}

static void execute_coreq(fx_server_t *server, fx_corex_t *corex)
{
	fx_xelem_t *xelem;
	fx_bkref_t *bkref = NULL;
	fx_task_t  *task  = NULL;
	const unsigned usec_timeout = 100000;

	xelem = fx_execq_dequeue(&server->core_execq, 1, usec_timeout);
	if (xelem != NULL) {
		corex->nexec += 1;
		corex->ntout = 0;
		switch (xelem->xtype) {
			case FX_XTYPE_TASK:
				task  = fx_xelem_to_task(xelem);
				execute_task(server, corex, task);
				break;
			case FX_XTYPE_RDBK:
			case FX_XTYPE_WRBK:
			case FX_XTYPE_WSEC:
				bkref = fx_xelem_to_bkref(xelem);
				execute_ioreply(corex, bkref);
				break;
			case FX_XTYPE_NONE:
			default:
				fx_assert(0);
				break;
		}
	} else {
		corex->nexec = 0;
		corex->ntout += 1;
	}
}

static void execute_pendq(fx_server_t *server, fx_corex_t *corex)
{
	int status;
	fx_task_t *task;

	while ((task = unpend_front_task(corex)) != NULL) {
		status = task_status(task);
		if (status == FX_ERPEND) {
			execute_task(server, corex, task);
		} else {
			fx_assert(status == FX_EWPEND);
			reply_normal_task(server, task, 0); /* TODO: Case -EIO */
		}
	}
}

static void adjust_space(fx_corex_t *corex)
{
	fx_lba_t bnext, jump;
	fx_bkcnt_t bfree, diff;
	fx_super_t  *super;
	fx_fsstat_t *fsstat;
	const fx_size_t  segnbk = FNX_SEGNBK;
	const fx_bkcnt_t spcnbk = FNX_SPCNBK;

	super   = corex->get_super(corex);
	fsstat  = &super->su_fsstat;
	bnext   = fsstat->f_bnext;
	bfree   = fsstat->f_bfree;
	if (corex->ntout) {
		if (bfree < corex->bmark) {
			diff = (corex->bmark - bfree);
			if (diff > (spcnbk / 3)) {
				jump = lba_end(bnext, spcnbk);
				fsstat->f_bnext = lba_floor(jump, segnbk);
				corex->bmark    = bfree;
			}
		} else if (bfree > corex->bmark) {
			diff = (bfree - corex->bmark);
			if (diff > (spcnbk / 3)) {
				corex->bmark = bfree;
			}
		}
		fx_prepare_space(corex, spcnbk);
	}
}

static void adjust_vdirty(fx_corex_t *corex)
{
	size_t cur, bar, nn = 0;
	const fx_cache_t *cache = corex->cache;

	bar = cache->climits.c_vdirty_lim;
	cur = list_size(&cache->vdirty);
	if (cur > bar) {
		nn = (cur - bar) / 4;
	} else if (cur > (bar / 4)) {
		nn += 4 * (corex->ntout & 1);
	}
	fx_destage_vdirty(corex, nn);
}

static void adjust_vlru(fx_corex_t *corex)
{
	size_t cur, lim, nn = 0;
	const fx_cache_t *cache = corex->cache;

	lim = cache->climits.c_vinode_lim;
	cur = cache->vhtbl.size + cache->ihtbl.size;
	if (cur > (2 * lim)) {
		nn = (cur - lim) / 16;
	} else if (cur > lim) {
		nn = ((cur - lim) / 16) + 1;
		nn = fx_min(nn, 16);
	} else if (cur > (lim / 4)) {
		nn = (corex->ntout & 1);
	}
	fx_squeeze_vlru(corex, nn);
}

static void adjust_udirty(fx_corex_t *corex)
{
	size_t cur, bar, ref, nn = 0;
	const fx_cache_t *cache = corex->cache;

	bar = cache->climits.c_udirty_lim;
	ref = list_size(&cache->ulru);
	cur = list_size(&cache->udirty);
	if (cur > bar) {
		nn = ((cur - bar) / 8) + 1;
	} else if (cur > ref) {
		nn = (cur - ref) / 2;
	}
	nn += (corex->ntout & 1);
	fx_destage_udirty(corex, FNX_INO_ANY, nn);
}

static void adjust_ulru(fx_corex_t *corex)
{
	size_t cur, bar, nn = 0;
	const fx_cache_t *cache = corex->cache;

	bar = cache->climits.c_ublock_lim;
	cur = cache->ubhtbl.size;
	if (cur > bar) {
		nn = ((cur - bar) / 8) + 1;
	} else if (cur > (bar / 4)) {
		nn += (corex->ntout & 1);
	}
	fx_squeeze_ulru(corex, nn);
}

static void adjust_sdirty(fx_corex_t *corex)
{
	size_t cur, bar, lru, nn = 0;
	const fx_cache_t *cache = corex->cache;

	bar = cache->climits.c_sdirty_lim;
	lru = list_size(&cache->slru);
	cur = list_size(&cache->sdirty);
	if (cur > bar) {
		nn = ((cur - bar) / 8) + 1;
	} else if (cur > (2 * lru)) {
		nn = 2;
	}
	nn += (corex->ntout & 1);
	fx_destage_sdirty(corex, nn);
}

static void adjust_slru(fx_corex_t *corex)
{
	size_t cur, lim, nn = 0;
	const fx_cache_t *cache = corex->cache;

	lim = cache->climits.c_sblock_lim;
	cur = cache->sbhtbl.size;
	if (cur > lim) {
		nn = ((cur - lim) / 8) + 1;
	} else if (cur > (lim / 2)) {
		nn = 2;
	} else if (cur > (lim / 8)) {
		nn = 4 * (corex->ntout & 1);
	}
	fx_squeeze_slru(corex, nn);
}

static void adjust_cache(fx_corex_t *corex)
{
	adjust_udirty(corex);
	adjust_ulru(corex);
	adjust_vdirty(corex);
	adjust_vlru(corex);
	adjust_sdirty(corex);
	adjust_slru(corex);
	fx_cache_updates(corex->cache);
}

/*
 * If super has been deactivated, force flush of all-dirty and then trigger
 * halt via fusei when no more requests and no open files left.
 * Flush-dirty threshold is determined but currently open files.
 */
static void probe_triggers(fx_server_t *server, fx_corex_t *corex)
{
	size_t nopenfh;
	const fx_super_t *super;

	super   = corex->get_super(corex);
	nopenfh = list_size(&corex->cache->frefs);
	if (!super->su_active && !nopenfh) {
		fx_destage_dirty(corex, UINT_MAX);
		if (!haspendirty(server)) {
			server->fusei.fi_active = FNX_FALSE;
		}
		server->active = 2;
	}
	if (server->fusei.fi_rx_done) {
		server->active = 0;
	}
}

static void execute_corex(fx_server_t *server, fx_corex_t *corex)
{
	while (isactive(server)) {
		/* Process next in-coming task/bk */
		execute_coreq(server, corex);

		/* Re-execute tasks revoked by reply-bk */
		execute_pendq(server, corex);

		/* Do space adjustments */
		adjust_space(corex);

		/* Do caching adjustments */
		adjust_cache(corex);

		/* Reorder execution state */
		probe_triggers(server, corex);
	}
}

static void finalize_corex(fx_server_t *server, fx_corex_t *corex)
{
	do {
		fx_destage_dirty(corex, UINT_MAX);
		execute_coreq(server, corex);
	} while (haspendirty(server));
}

static void run_fscore(void *p)
{
	fx_server_t *server  = (fx_server_t *)p;
	fx_corex_t  *corex   = &server->corex;

	fx_info("start-execute-fscore: mntpoint=%s", mntpoint(server));
	fx_mutex_lock(&server->core_lock);
	execute_corex(server, corex);
	finalize_corex(server, corex);
	fx_mutex_unlock(&server->core_lock);
	fx_info("done-execute-fscore: mntpoint=%s", mntpoint(server));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void execute_filter(fx_server_t *server)
{
	int rc;
	fx_task_t *task, *tlist;

	tlist = fx_recv_tasks(&server->filter_taskq, 100, 10000);
	while ((task = tlist) != NULL) {
		tlist = getnexttask(tlist);
		rc = fx_preprocess_task(task);
		if (fx_likely(rc == 0)) {
			fx_send_task(&server->core_execq, task, 0);
		} else {
			reply_normal_task(server, task, rc);
		}
	}
}

static void run_filter(void *p)
{
	fx_server_t *server = (fx_server_t *)p;

	fx_info("start-execute-filter: mntpoint=%s", mntpoint(server));
	while (isactive(server)) {
		execute_filter(server);
	}
	fx_info("done-execute-filter: mntpoint=%s", mntpoint(server));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void execute_fusei_tx(fx_server_t *server)
{
	fx_task_t *task, *tlist;
	fx_fusei_t *fusei = &server->fusei;

	tlist = fx_recv_tasks(&server->fusei_taskq, 11, 10000);
	while ((task = tlist) != NULL) {
		tlist = getnexttask(tlist);

		if (task->tsk_flags & FX_TASKF_CLEARED) {
			/* Reply task after purification; delete it */
			fx_server_del_task(server, task);
		} else {
			/* Normal response execution */
			fx_fusei_execute_tx(fusei, task);

			if (task->tsk_opcode == FX_OP_READ) {
				/* Read-response; need to release blocks via resend */
				fx_send_task(&server->core_execq, task, FX_TASKF_PURIFY);
			} else {
				/* Done with this task, delete it */
				fx_server_del_task(server, task);
			}
		}
	}
}

static void run_fusei_tx(void *p)
{
	fx_server_t *server = (fx_server_t *)p;

	fx_info("start-execute-fusei-tx: mntpoint=%s", mntpoint(server));
	while (isactive(server)) {
		execute_fusei_tx(server);
	}
	fx_info("done-execute-fusei-tx: mntpoint=%s", mntpoint(server));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void run_fusei_rx(void *p)
{
	fx_server_t *server = (fx_server_t *)p;

	fx_info("start-execute-fusei-rx: mntpoint=%s", mntpoint(server));
	fx_fusei_process_rx(&server->fusei);
	fx_info("done-execute-fusei-rx: mntpoint=%s", mntpoint(server));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void start_thread(fx_server_t *server, fx_thread_t *thread,
                         fx_thread_fn hook, const char *name)
{
	fx_thread_setname(thread, name);
	fx_thread_start(thread, hook, server);
}

static void join_thread(fx_thread_t *thread)
{
	fx_join_thread(thread, FX_NOFAIL);
}

int fx_server_start(fx_server_t *server)
{
	server->active = 1;
	start_thread(server, &server->slave_thread, run_slave, "fx-slave");
	start_thread(server, &server->core_thread, run_fscore, "fx-core");
	start_thread(server, &server->filter_thread, run_filter, "fx-filter");
	start_thread(server, &server->fusei_tx_thread, run_fusei_tx, "fx-fusei-tx");
	start_thread(server, &server->fusei_rx_thread, run_fusei_rx, "fx-fusei-rx");
	return 0;
}

int fx_server_stop(fx_server_t *server)
{
	server->active = 0;
	join_thread(&server->fusei_rx_thread);
	join_thread(&server->fusei_tx_thread);
	join_thread(&server->filter_thread);
	join_thread(&server->core_thread);
	join_thread(&server->slave_thread);
	fx_fusei_term(&server->fusei);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_task_t *fx_server_new_task(fx_server_t *server, fx_opcode_e opc)
{
	fx_task_t *task;

	task = fx_taskpool_newtask(&server->taskpool, opc);
	task->tsk_fusei = &server->fusei;
	return task;
}

void fx_server_del_task(fx_server_t *server, fx_task_t *task)
{
	fx_taskpool_deltask(&server->taskpool, task);
}


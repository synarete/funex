/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"

static int destage_vnode(fx_corex_t *corex, fx_vnode_t *vnode)
{
	int rc = 0;
	fx_bkref_t  *sec  = NULL;
	fx_dsec_t   *dsec = NULL;
	fx_header_t *hdr  = NULL;
	const fx_bkaddr_t *bkaddr = &vnode->v_bkaddr;

	if (vnode_ispseudo(vnode)) {
		return 0; /* OK, ignored */
	}
	rc = corex->fetch_sec(corex, bkaddr, &sec);
	if (rc != 0) {
		return rc;
	}
	hdr = fx_get_dobj(sec, bkaddr);
	fx_assert(hdr != NULL);
	if (hdr == NULL) {
		return -EIO;
	}
	dsec = fx_get_dsec(sec);
	fx_assert(hdr >= &dsec->sec_hdr);
	fx_dexport_vobj(vnode, hdr);
	corex->put_bk(corex, sec);
	return 0;
}

int fx_destage_vdirty(fx_corex_t *corex, fx_size_t lim)
{
	int rc;
	fx_vnode_t *vnode;
	fx_cache_t *cache = corex->cache;

	while (lim-- > 0) {
		vnode = cache->undirty_vnode(cache);
		if (vnode == NULL) {
			break;
		}
		rc = destage_vnode(corex, vnode);
		fx_assert(rc == 0);
		cache->stain_vnode(cache, vnode, FX_STAIN_LRU);
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_destage_udirty(fx_corex_t *corex, fx_ino_t ino, fx_size_t lim)
{
	int rc = 0;
	fx_cache_t *cache;
	fx_bkref_t *bkref, *bklst;

	cache = corex->cache;
	bklst = cache->undirty_bks(cache, ino, lim);
	while ((bkref = bklst) != NULL) {
		bklst = bklst->next;
		bkref->next = NULL;
		corex->enslave_bk(corex, bkref, FX_XTYPE_WRBK);
		rc = FX_EWPEND;
	}
	return rc;
}

int fx_destage_sdirty(fx_corex_t *corex, fx_size_t lim)
{
	int rc = 0;
	fx_bkref_t *sec;
	fx_cache_t *cache = corex->cache;

	while (lim-- > 0) {
		sec = cache->undirty_sec(cache);
		if (sec == NULL) {
			break;
		}
		corex->enslave_bk(corex, sec, FX_XTYPE_WSEC);
		rc = FX_EWPEND;
	}
	return rc;
}

int fx_destage_dirty_of(fx_corex_t *corex, fx_ino_t ino, fx_bool_t all)
{
	int rc;
	const fx_size_t lim = ULONG_MAX;

	if (all) {
		rc = fx_destage_udirty(corex, ino, lim);
		fx_destage_vdirty(corex, lim);
		fx_destage_sdirty(corex, lim);
	} else {
		rc = fx_destage_udirty(corex, ino, lim);
	}

	return rc;
}

int fx_destage_dirty(fx_corex_t *corex, fx_size_t lim)
{
	int rc;

	fx_destage_vdirty(corex, lim);
	rc = fx_destage_udirty(corex, FNX_INO_ANY, lim);
	fx_destage_sdirty(corex, lim);

	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_squeeze_vlru(fx_corex_t *corex, fx_size_t lim)
{
	fx_vnode_t *vnode = NULL;
	fx_cache_t *cache = corex->cache;

	while (lim-- > 0) {
		if ((vnode = cache->getlru_vnode(cache)) == NULL) {
			break;
		}
		if (vnode_vtype(vnode) == FNX_VTYPE_SUPER) {
			continue;
		}
		if (vnode->v_refcnt == 0) {
			cache->evict_vnode(cache, vnode);
			cache->del_vnode(cache, vnode);
		} else {
			cache->stain_vnode(cache, vnode, FX_STAIN_LRU);
		}
	}
}

void fx_squeeze_ulru(fx_corex_t *corex, fx_size_t lim)
{
	fx_bkref_t *bkref = NULL;
	fx_cache_t *cache = corex->cache;

	while (lim-- > 0) {
		if ((bkref = cache->getlru_bk(cache)) == NULL) {
			break;
		}
		if (bkref->refcnt == 0) {
			cache->evict_bk(cache, bkref);
			cache->del_bk(cache, bkref);
		} else {
			cache->stain_bk(cache, bkref, FX_STAIN_LRU);
		}
	}
}

void fx_squeeze_slru(fx_corex_t *corex, fx_size_t lim)
{
	fx_bkref_t *sec = NULL;
	fx_cache_t *cache = corex->cache;

	while (lim-- > 0) {
		if ((sec = cache->getlru_sec(cache)) == NULL) {
			break;
		}
		if (bkaddr_issuper(&sec->addr)) {
			continue;
		}
		corex->withdraw_sec(corex, sec);
	}
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <errno.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "cache.h"
#include "corex.h"
#include "hooks.h"


static int gid_isnull(fx_gid_t gid)
{
	return (gid == ((fx_gid_t)FNX_GID_NULL));
}

static int uid_isnull(fx_uid_t uid)
{
	return (uid == ((fx_uid_t)FNX_UID_NULL));
}

static int uid_isvalid(fx_uid_t uid)
{
	return (uid < FNX_UID_MAX);
}


static int isowner(const fx_uctx_t *uctx, const fx_inode_t *inode)
{
	return inode_isowner(inode, uctx);
}

static int isingroup(const fx_uctx_t *uctx, fx_gid_t gid)
{
	return fx_isgroupmember(uctx, gid);
}

static int hascap(const fx_uctx_t *uctx, fx_capf_t mask)
{
	return uctx->u_root || fx_userctx_iscap(uctx, mask);
}

static int issize(const fx_inode_t *inode, fx_off_t size)
{
	return (inode_getsize(inode) == size);
}

static int
verify_chmod(fx_corex_t *corex, const fx_inode_t *inode, fx_mode_t mode)
{
	fx_vtype_e vtype;
	const fx_uctx_t *uctx = corex->get_uctx(corex);

	vtype = fx_mode_to_vtype(mode);
	if (inode_vtype(inode) != vtype) {
		/* Can not change vtype upon chmod */
		return -EPERM;
	}
	if (inode_ispseudo(inode)) {
		return -EPERM;
	}

	if (!isowner(uctx, inode) && !hascap(uctx, FNX_CAPF_FOWNER)) {
		return -EPERM;
	}
	return 0;
}

static int
verify_chown_gid(fx_corex_t *corex, const fx_inode_t *inode, fx_gid_t gid)
{
	const fx_uctx_t *uctx = corex->get_uctx(corex);

	if (inode_ispseudo(inode)) {
		return -EPERM;
	}
	if (!hascap(uctx, FNX_CAPF_CHOWN)) {
		if (!isowner(uctx, inode)) {
			return -EPERM;
		}
		if (!isingroup(uctx, gid)) {
			return -EPERM;
		}
	}
	return 0;
}

static int
verify_chown_uid(fx_corex_t *corex, const fx_inode_t *inode, fx_uid_t uid)
{
	const fx_uctx_t *uctx = corex->get_uctx(corex);

	if (!uid_isvalid(uid)) {
		return -EINVAL;
	}
	if (inode_ispseudo(inode)) {
		return -EPERM;
	}
	if (!isowner(uctx, inode) && !hascap(uctx, FNX_CAPF_CHOWN)) {
		return -EPERM;
	}
	return 0;
}

static int verify_utimes(fx_corex_t *corex, const fx_inode_t *inode)
{
	const fx_uctx_t *uctx = corex->get_uctx(corex);

	if (inode_ispseudo(inode)) {
		return -EPERM;
	}
	if (!isowner(uctx, inode) && !hascap(uctx, FNX_CAPF_FOWNER)) {
		return -EPERM;
	}
	return 0;
}

int fx_let_setattr(fx_corex_t *corex, const fx_inode_t *inode,
                   fx_flags_t flags, fx_mode_t mode, fx_uid_t uid,
                   fx_gid_t gid, fx_off_t size)

{
	int rc = 0;

	if (flags & FNX_SETATTR_MODE) {
		rc = verify_chmod(corex, inode, mode);
		if (rc != 0) {
			return rc;
		}
	}
	if ((flags & FNX_SETATTR_GID) && !gid_isnull(gid)) {
		rc = verify_chown_gid(corex, inode, gid);
		if (rc != 0) {
			return rc;
		}
	}
	if ((flags & FNX_SETATTR_UID) && !uid_isnull(uid)) {
		rc = verify_chown_uid(corex, inode, uid);
		if (rc != 0) {
			return rc;
		}
	}
	if ((flags & FNX_SETATTR_SIZE) && !issize(inode, size)) {
		rc = fx_let_setsize(corex, inode, size);
		if (rc != 0) {
			return rc;
		}
	}
	if (flags & FNX_SETATTR_AMTIME_ANY) {
		rc = verify_utimes(corex, inode);
		if (rc != 0) {
			return rc;
		}
	}
	return 0;
}

int fx_let_setsize(fx_corex_t *corex, const fx_inode_t *inode, fx_off_t size)
{
	const fx_uctx_t *uctx = corex->get_uctx(corex);

	if (size > (fx_off_t)FNX_REGSIZE_MAX) {
		return -EFBIG;
	} else if (size < 0) {
		return -EINVAL;
	}
	if (inode_ispseudo(inode)) {
		return -EPERM;
	}
	if (inode_isdir(inode)) {
		return -EISDIR;
	}
	if (!inode_isreg(inode)) { /* TODO: Is it? */
		return -EINVAL;
	}
	if (!inode_isowner(inode, uctx) && !hascap(uctx, FNX_CAPF_ADMIN)) {
		return -EPERM;
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
inode_is_clearsgid_needed(const fx_inode_t *inode, const fx_uctx_t *uctx)
{
	const int cap_fsetid = fx_userctx_iscap(uctx, FNX_CAPF_FSETID);
	return (!fx_isgroupmember(uctx, inode->i_iattr.i_gid) && !cap_fsetid);
}

static int
inode_is_clearsuid_needed(const fx_inode_t *inode, const fx_uctx_t *uctx)
{
	const int cap_chown = fx_userctx_iscap(uctx, FNX_CAPF_CHOWN);
	return (inode_isreg(inode) && fx_inode_isexec(inode) && !cap_chown);
}

static void
inode_refresh_sgid(fx_inode_t *inode, const fx_uctx_t *uctx)
{
	if (inode_is_clearsgid_needed(inode, uctx)) {
		fx_inode_clearsgid(inode);
	}
}

static void
inode_refresh_suid(fx_inode_t *inode, const fx_uctx_t *uctx)
{
	if (inode_is_clearsuid_needed(inode, uctx)) {
		fx_inode_clearsuid(inode);
	}
}

static void setattr_chmod(fx_corex_t *corex, fx_inode_t *inode, fx_mode_t mode)
{
	fx_iattr_t *iattr;
	const fx_uctx_t *uctx;

	uctx  = &corex->task->tsk_uctx;
	iattr = &inode->i_iattr;

	iattr->i_mode = mode;
	inode_refresh_sgid(inode, uctx);
}

static void
setattr_chown_uid(fx_corex_t *corex, fx_inode_t *inode, fx_uid_t uid)
{
	fx_iattr_t *iattr;
	const fx_uctx_t *uctx;

	uctx  = &corex->task->tsk_uctx;
	iattr = &inode->i_iattr;

	iattr->i_uid = uid;
	inode_refresh_suid(inode, uctx);
}

static void
setattr_chown_gid(fx_corex_t *corex, fx_inode_t *inode, fx_gid_t gid)
{
	fx_iattr_t *iattr;
	const fx_uctx_t *uctx;

	uctx  = &corex->task->tsk_uctx;
	iattr = &inode->i_iattr;

	iattr->i_gid = gid;
	inode_refresh_sgid(inode, uctx);
}

static void
setattr_utimes(fx_corex_t *corex, fx_inode_t *inode,
               fx_flags_t flags, const fx_times_t *tms)
{
	fx_inode_setitimes(inode, flags, tms);
	fx_unused(corex);
}

int fx_setattr_size(fx_corex_t *corex, fx_inode_t *inode, fx_off_t size)
{
	const fx_uctx_t *uctx = &corex->task->tsk_uctx;

	inode_setsize(inode, size);
	inode_refresh_suid(inode, uctx);
	inode_refresh_sgid(inode, uctx);
	return 0;
}


int fx_setattr_stat(fx_corex_t *corex, fx_inode_t *inode, fx_flags_t flags,
                    fx_mode_t mode, fx_uid_t uid, fx_gid_t gid, fx_off_t size,
                    const fx_times_t *itms)
{
	fx_flags_t tmflags = 0;

	if (flags & FNX_SETATTR_MODE) {
		setattr_chmod(corex, inode, mode);
		tmflags |= FNX_CTIME_NOW;
	}
	if ((flags & FNX_SETATTR_GID) && !gid_isnull(gid)) {
		setattr_chown_gid(corex, inode, gid);
		tmflags |= FNX_CTIME_NOW;
	}
	if ((flags & FNX_SETATTR_UID) && !uid_isnull(uid)) {
		setattr_chown_uid(corex, inode, uid);
		tmflags |= FNX_CTIME_NOW;
	}
	if ((flags & FNX_SETATTR_SIZE) && !issize(inode, size)) {
		fx_setattr_size(corex, inode, size);
		tmflags |= FNX_ACTIME_NOW;
	}
	if (flags & FNX_SETATTR_AMTIME_ANY) {
		setattr_utimes(corex, inode, flags, itms);
	}

	corex->put_inode(corex, inode, tmflags);
	return 0;
}


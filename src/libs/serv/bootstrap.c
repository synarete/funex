/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxfusei.h"

#include "cache.h"
#include "corex.h"
#include "server.h"
#include "hooks.h"


/* Locals */
static void server_fusei_sendreq(fx_fusei_t *, fx_task_t *);
static fx_task_t *server_fusei_newtask(struct fx_fusei *, fx_opcode_e);
static void server_fusei_deltask(struct fx_fusei *, fx_task_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_server_init(fx_server_t *server)
{
	fx_bzero(server, sizeof(*server));
	server->usock   = NULL;
	server->active  = 0;

	fx_balloc_init(&server->balloc);
	fx_valloc_init(&server->valloc);
	fx_cache_init(&server->cache, &server->balloc, &server->valloc);
	fx_taskpool_init(&server->taskpool);
	fx_fscore_init(&server->fscore, &server->balloc);
	fx_corex_init(&server->corex, &server->fscore, &server->cache);
	fx_fusei_init(&server->fusei, &server->balloc);
	server->fusei.fi_sendreq_fn = server_fusei_sendreq;
	server->fusei.fi_newtask_fn = server_fusei_newtask;
	server->fusei.fi_deltask_fn = server_fusei_deltask;

	fx_execq_init(&server->core_execq);
	fx_execq_init(&server->slave_execq);
	fx_execq_init(&server->filter_taskq);
	fx_execq_init(&server->fusei_taskq);
	fx_mutex_init(&server->core_lock);

	fx_thread_init(&server->core_thread);
	fx_thread_init(&server->slave_thread);
	fx_thread_init(&server->filter_thread);
	fx_thread_init(&server->fusei_tx_thread);
	fx_thread_init(&server->fusei_rx_thread);
}

void fx_server_destroy(fx_server_t *server)
{
	fx_thread_destroy(&server->fusei_rx_thread);
	fx_thread_destroy(&server->fusei_tx_thread);
	fx_thread_destroy(&server->filter_thread);
	fx_thread_destroy(&server->core_thread);
	fx_thread_destroy(&server->slave_thread);

	fx_mutex_destroy(&server->core_lock);
	fx_execq_destroy(&server->fusei_taskq);
	fx_execq_destroy(&server->filter_taskq);
	fx_execq_destroy(&server->core_execq);
	fx_execq_destroy(&server->slave_execq);

	fx_fusei_destroy(&server->fusei);
	fx_fscore_destroy(&server->fscore);
	fx_corex_destroy(&server->corex);
	fx_taskpool_destroy(&server->taskpool);
	fx_cache_destroy(&server->cache);
	fx_balloc_destroy(&server->balloc);
	fx_valloc_destroy(&server->valloc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void report_mntf(fx_mntf_t mntf)
{
	fx_info("mnt_rdonly=%d",       fx_testlf(mntf, FNX_MNTF_RDONLY));
	fx_info("mnt_nosuid=%d",       fx_testlf(mntf, FNX_MNTF_NOSUID));
	fx_info("mnt_nodev=%d",        fx_testlf(mntf, FNX_MNTF_NODEV));
	fx_info("mnt_noexec=%d",       fx_testlf(mntf, FNX_MNTF_NOEXEC));
	fx_info("mnt_mandlock=%d",     fx_testlf(mntf, FNX_MNTF_MANDLOCK));
	fx_info("mnt_dirsync=%d",      fx_testlf(mntf, FNX_MNTF_DIRSYNC));
	fx_info("mnt_noatime=%d",      fx_testlf(mntf, FNX_MNTF_NOATIME));
	fx_info("mnt_nodiratime=%d",   fx_testlf(mntf, FNX_MNTF_NODIRATIME));
	fx_info("mnt_relatime=%d",     fx_testlf(mntf, FNX_MNTF_RELATIME));
	fx_info("mnt_strict=%d",       fx_testlf(mntf, FNX_MNTF_STRICT));
}

int fx_server_setup(fx_server_t *server, const fx_uctx_t *uctx,
                    const char *usock, fx_mntf_t mntf)
{
	int rc, strict;
	fx_uctx_t *suctx = NULL;

	server->usock       = usock;
	server->fscore.mntf = mntf;
	suctx = &server->fscore.ptask.tsk_uctx;
	memcpy(suctx, uctx, sizeof(*suctx));

	strict = fx_testlf(mntf, FNX_MNTF_STRICT);
	rc = fx_fusei_setup(&server->fusei, strict);
	if (rc != 0) {
		return rc;
	}
	report_mntf(mntf);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int open_vio(fx_server_t *server, const char *path)
{
	int rc, flags;
	size_t nbk = 0;
	fx_vio_t *vio;

	/* XXX FX_VIOF_RDONLY? FX_VIOF_SYNC? */
	flags = FX_VIOF_RDWR | FX_VIOF_LOCK | FX_VIOF_SAVEPATH;
	fx_trace1("open-meta: path=%s flags=%#x", path, flags);

	vio = &server->fscore.vio;
	rc = vio->open(vio, path, 0, 0, flags);
	if (rc != 0) {
		fx_error("no-open: path=%s flags=%#x err=%d", path, flags, -rc);
		return rc;
	}
	rc = vio->tryflock(vio);
	if (rc != 0) {
		vio->close(vio);
		fx_error("no-flock: path=%s err=%d", path, -rc);
		return rc;
	}
	rc = vio->getcap(vio, &nbk);
	if (rc != 0) {
		vio->close(vio);
		fx_error("no-getcap: path=%s err=%d", path, -rc);
		return rc;
	}
	if ((nbk < FNX_VOLNBK_MIN) || (nbk > FNX_VOLNBK_MAX)) {
		vio->close(vio);
		fx_error("illegal-volume-size: path=%s nbk=%lu", path, nbk);
		return rc;
	}
	return 0;
}

static void report_super(const fx_super_t *super)
{
	const char *prefix = "super";
	const fx_fsattr_t *fsattr = &super->su_fsattr;
	const fx_fsstat_t *fsstat = &super->su_fsstat;

	fx_info("%s: fs-name=%s", prefix, super->su_name.str);
	fx_info("%s: vol-uuid=%s flags=%#lx", prefix,
	        super->su_uuid.str, super->su_flags);
	fx_info("%s: fs-uuid=%s", prefix, fsattr->f_fsuuid.str);
	fx_info("%s: uid=%d gid=%d", prefix, fsattr->f_uid, fsattr->f_gid);
	fx_info("%s: blocks=%zu bnext=%zu iapex=%#lx", prefix,
	        fsstat->f_blocks, fsstat->f_bnext, fsstat->f_iapex);
	fx_info("%s: inodes=%zu iapex=%#lx", prefix,
	        fsstat->f_inodes, fsstat->f_iapex);
}

static int load_super(fx_server_t *server)
{
	int rc;
	fx_uctx_t   *uctx;
	fx_super_t  *super;
	fx_fsattr_t *fsattr;
	fx_corex_t  *corex  = &server->corex;
	fx_fscore_t *fscore = &server->fscore;

	/* Bootstrap fetch */
	rc = fx_fetch_super(corex, &super);
	if (rc != 0) {
		return rc;
	}
	fscore->super = super;

	/* Refresh mount-bits */
	fsattr = &super->su_fsattr;
	fsattr->f_mntf = fscore->mntf;

	/* Check uid/gid match current run, update if needed */
	uctx = fscore->uctx;
	if (fsattr->f_uid != uctx->u_cred.cr_uid) {
		fx_warn("super-uid-mismatch: f_uid=%u uid=%u",
		        fsattr->f_uid, uctx->u_cred.cr_uid);
	}
	if (fsattr->f_gid != uctx->u_cred.cr_gid) {
		fx_warn("super-gid-mismatch: f_gid=%u gid=%u",
		        fsattr->f_gid, uctx->u_cred.cr_gid);
	}

	/* Logging info */
	report_super(super);

	return 0;
}

static int expect_noinode(fx_server_t *server, fx_ino_t ino)
{
	int rc;
	fx_inode_t *inode  = NULL;

	rc = fx_fetch_inode(&server->corex, ino, &inode);
	return ((rc == -ENOENT) ? 0 : -EINVAL);
}

static int verify_fsinfo(fx_server_t *server)
{
	int rc = 0;
	fx_ino_t ino, apex_ino, root_ino;
	const fx_super_t  *super  = server->fscore.super;
	const fx_fsattr_t *fsattr = &super->su_fsattr;
	const fx_fsstat_t *fsstat = &super->su_fsstat;

	root_ino = fsattr->f_rootino;
	apex_ino = fsstat->f_iapex;
	if (fx_check_ino(root_ino) != 0) {
		fx_error("non-valid-ino: root_ino=%#jx", root_ino);
		return -EINVAL;
	}
	for (size_t i = 0; i < 128; ++i) {
		ino = ((fx_ino_t)i + apex_ino);
		rc = expect_noinode(server, ino);
		if (rc != 0) {
			fx_error("unexpected-ino: ino=%#jx apex_ino=%#jx ", ino, apex_ino);
			return -EINVAL;
		}
	}
	return 0;
}

static int verify_layout(fx_server_t *server)
{
	int rc;
	fx_lba_t beg, end, fin;
	fx_bkcnt_t cap = 0;
	const fx_fscore_t *fscore = &server->fscore;
	const fx_super_t  *super  = fscore->super;
	const fx_layout_t *layout = &super->su_layout;
	const fx_vio_t *vio = &fscore->vio;

	rc = vio->getcap(vio, &cap);
	if ((rc != 0) || (cap < FNX_VOLNBK_MIN)) {
		fx_error("no-getcap: path=%s cap=%lu", vio->path, cap);
		return rc;
	}
	fin = lba_end(0, cap);
	end = lba_end(layout->l_udata.lba, layout->l_udata.cnt);
	if (end > fin) {
		fx_error("illegal-layout: cap=%lu udata_lba=%#lx udata_cnt=%lu",
		         cap, layout->l_udata.lba, layout->l_udata.cnt);
		return -EINVAL;
	}
	end = lba_end(layout->l_super.lba, layout->l_super.cnt);
	beg = layout->l_index.lba;
	if (end > beg) {
		fx_error("illegal-layout: super_lba=%#lx super_cnt=%lu",
		         layout->l_super.lba, layout->l_super.cnt);
		return -EINVAL;
	}
	end = lba_end(layout->l_index.lba, layout->l_index.cnt);
	beg = layout->l_udata.lba;
	if (end > beg) {
		fx_error("illegal-layout: index_lba=%#lx index_cnt=%lu",
		         layout->l_index.lba, layout->l_index.cnt);
		return -EINVAL;
	}
	return 0;
}

static int load_rootd(fx_server_t *server)
{
	int rc;
	fx_ino_t root_ino;
	fx_dir_t *rootd  = NULL;
	fx_corex_t  *corex  = &server->corex;
	fx_fscore_t *fscore = &server->fscore;

	root_ino = fscore->super->su_fsattr.f_rootino;
	rc = fx_fetch_dir(corex, root_ino, &rootd);
	if (rc == 0) {
		inode_setitime(&rootd->d_inode, FNX_AMCTIME_NOW);
		fscore->root = rootd;
		fx_info("load-rootd: root_ino=%#jx", root_ino);
	} else {
		fx_error("no-load-rootd: root_ino=%#jx rc=%d", root_ino, rc);
	}
	return rc;
}

/* TODO: clean & close on errors */
static int open_fscore(fx_server_t *server, const char *volume)
{
	int rc;

	rc = open_vio(server, volume);
	if (rc != 0) {
		goto out;
	}
	rc = load_super(server);
	if (rc != 0) {
		goto out;
	}
	rc = verify_fsinfo(server);
	if (rc != 0) {
		goto out;
	}
	rc = verify_layout(server);
	if (rc != 0) {
		goto out;
	}
	rc = load_rootd(server);
	if (rc != 0) {
		goto out;
	}

	/* TODO: Check volume's layout consistency with super */
	/* TODO: Load space-maps once */

out:
	/* TODO: close on error */
	return rc;
}

/* For-each new mount, update refcount for LBA-0 */
static int update_bkzero(fx_server_t *server)
{
	int rc;
	fx_spmap_t  *spmap  = NULL;
	fx_bkdsc_t  *bkdsc  = NULL;
	fx_corex_t  *corex  = &server->corex;

	rc = fx_fetch_spmap(corex, FNX_LBA_ZERO, &spmap);
	if (rc != 0) {
		return rc;
	}
	bkdsc = fx_spmap_getdsc(spmap, FNX_LBA_ZERO);
	fx_assert(bkdsc != NULL);
	bkdsc->refs += 1;

	if (spmap->sp_used == 0) {
		spmap->sp_used = 1; /* First-time mount */
	}

	corex->put_vnode(corex, &spmap->sp_vnode);
	return 0;
}

int fx_server_open(fx_server_t *server, const char *volume)
{
	int rc;

	rc = open_fscore(server, volume);
	if (rc != 0) {
		fx_critical("open-fscore-failed: volume=%s", volume);
		return rc;
	}
	rc = update_bkzero(server);
	if (rc != 0) {
		fx_critical("update-bkzero-failed: volume=%s", volume);
		return rc;
	}
	rc = fx_bind_pseudo_namespace(&server->corex);
	if (rc != 0) {
		fx_critical("bind-pseudo-namespace-failed: volume=%s", volume);
		return rc;
	}

	fx_info("opened-server: volume=%s", volume);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static char *resolve_mntpoint(const char *path)
{
	int rc;
	char *mntpoint = NULL;

	rc = access(path, R_OK | W_OK | X_OK);
	if (rc != 0) {
		fx_warn("no-rwx-access path=%s", path);
		return NULL;
	}
	mntpoint = realpath(path, NULL);
	if (mntpoint == NULL) {
		fx_error("could-not-resolve path=%s", path);
		return NULL;
	}
	return mntpoint;
}

int fx_server_mount(fx_server_t *server, const char *path)
{
	int rc, fd;
	char *mntpoint;
	const fx_mntf_t mntflags = server->fscore.mntf;

	rc = fd = -1;
	mntpoint = resolve_mntpoint(path);
	if (mntpoint == NULL) {
		goto out;
	}
	/* XXX For now, let libfuse do the mount and do not relay on funex-mntsrv
	rc = fx_mount(usock, path, mntflags, &fd);
	if (rc != 0) {
	    goto out;
	}
	fx_info("mount usock=%s mntpoint=%s fd=%d", usock, mntpoint, fd);
	*/
	rc = fx_fusei_open(&server->fusei, fd, mntpoint);
	if (rc != 0) {
		fx_error("open-fusei-failed: mntpoint=%s", mntpoint);
		goto out;
	}
	fx_info("server-mount mntpoint=%s fd=%d", mntpoint, fd);
	fx_unused(mntflags);
out:
	if (mntpoint != NULL) {
		free(mntpoint);
	}
	if ((rc != 0) && (fd > 0)) {
		close(fd);
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void close_vio(fx_vio_t *vio)
{
	int rc;
	const char *path = vio->path;

	if (vio->isopen(vio)) {
		rc = vio->sync(vio);
		if (rc != 0) {
			fx_error("no-sync: path=%s err=%d", path, -rc);
		}
		vio->close(vio);
	}
}

static void close_fscore(fx_server_t *server)
{
	fx_fscore_t *fscore = &server->fscore;
	close_vio(&fscore->vio);
}

int fx_server_close(fx_server_t *server)
{
	fx_fusei_close(&server->fusei);
	close_fscore(server);
	return 0;
}

void fx_server_cleanup(fx_server_t *server)
{
	/* TODO: improve */
	fx_mutex_lock(&server->core_lock);
	fx_cache_clearall(&server->cache);
	fx_taskpool_cleanup(&server->taskpool);
	fx_balloc_clear(&server->balloc);
	fx_valloc_clear(&server->valloc);
	fx_mutex_unlock(&server->core_lock);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_server_t *server_from_fusei(fx_fusei_t *fusei)
{
	return fx_container_of(fusei, fx_server_t, fusei);
}

static void server_fusei_sendreq(fx_fusei_t *fusei, fx_task_t *task)
{
	fx_server_t *server = server_from_fusei(fusei);
	fx_send_task(&server->filter_taskq, task, 0);
}

static fx_task_t *
server_fusei_newtask(struct fx_fusei *fusei, fx_opcode_e opc)
{
	fx_server_t *server = server_from_fusei(fusei);
	return fx_server_new_task(server, opc);
}

static void
server_fusei_deltask(struct fx_fusei *fusei, fx_task_t *task)
{
	fx_server_t *server = server_from_fusei(fusei);
	fx_server_del_task(server, task);
}



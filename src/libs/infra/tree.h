/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_TREE_H_
#define FUNEX_TREE_H_


struct fx_keyref;
struct fx_treelink;
struct fx_tree;

typedef struct fx_tree fx_avl_t;
typedef struct fx_tree fx_rb_t;
typedef struct fx_tree fx_treap_t;



enum FX_TREE_TYPE {
	FX_TREE_AVL    = 1,
	FX_TREE_RB     = 2,
	FX_TREE_TREAP  = 4
};
typedef enum FX_TREE_TYPE    fx_treetype_e;


/* Get key-ref of tree-node */
typedef const void *(*fx_getkey_fn)(const struct fx_treelink *);

/* 3-way compare function-pointer */
typedef int (*fx_keycmp_fn)(const void *, const void *);



/* Node operator */
typedef void (*fx_tnode_fn)(struct fx_treelink *, void *);

/* User-provided tree-node operations-hooks */
struct fx_treehooks {
	fx_getkey_fn   getkey_hook;
	fx_keycmp_fn   keycmp_hook;
};
typedef struct fx_treehooks    fx_treehooks_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * "Base" for specific self-balancing binary-search-tree nodes.
 */
struct fx_treelink {
	struct fx_treelink *parent;
	struct fx_treelink *left;
	struct fx_treelink *right;

	union {
		int avl_balance;
		int rb_color;
		unsigned long treap_priority;
		const void *alignment;
	} d;

} FX_ATTR_ALIGNED;
typedef struct fx_treelink fx_tlink_t;


/*
 * "Iterators" range a-la STL pair.
 */
struct fx_tree_range {
	fx_tlink_t *first;
	fx_tlink_t *second;
};
typedef struct fx_tree_range fx_tree_range_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Binary-search-tree (self-balancing). Holds reference to user-provided nodes
 * (intrusive container). Uses user-provided hooks for resolving node-to-key
 * and key-compare. Head stores leftmost, rightmost and root pointers.
 *
 * NB: It is up to the user to release any associated resources, including the
 * possibly still-linked nodes upon destruction.
 */
struct fx_tree {
	size_t tr_nodecount;            /* Size (num nodes) */
	unsigned int    tr_pad_;        /* Alignment */
	fx_treetype_e   tr_type;        /* Sub-type (AVL,RB,Treap) */
	fx_tlink_t      tr_head;        /* Left, right, root */
	const fx_treehooks_t *tr_uops; /* User provided oper-hooks */
};
typedef struct fx_tree  fx_tree_t;



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/


/*
 * Set all links to NULL.
 */
FX_ATTR_NONNULL
void fx_tlink_init(fx_tlink_t *x);

FX_ATTR_NONNULL
void fx_tlink_destroy(fx_tlink_t *x);



/*
 * Construct empty tree (AVL|RB|Treap), using user-provided hooks.
 */
FX_ATTR_NONNULL
void fx_avl_init(fx_avl_t *avl, const fx_treehooks_t *uops);

FX_ATTR_NONNULL
void fx_rb_init(fx_rb_t *rb, const fx_treehooks_t *uops);

FX_ATTR_NONNULL
void fx_treap_init(fx_treap_t *treap, const fx_treehooks_t *uops);

FX_ATTR_NONNULL
void fx_tree_init(fx_tree_t *tree,
                  fx_treetype_e tree_type, const fx_treehooks_t *uops);


/*
 * Destructor: clear & zero.
 */
FX_ATTR_NONNULL
void fx_tree_destroy(fx_tree_t *tree);

/*
 * Returns True is no elements in tree.
 */
FX_ATTR_NONNULL
int fx_tree_isempty(const fx_tree_t *);

/*
 * Returns the number elements stored in tree.
 */
FX_ATTR_NONNULL
size_t fx_tree_size(const fx_tree_t *);

/*
 * Returns the number of elements with key.
 */
FX_ATTR_NONNULL
size_t fx_tree_count(const fx_tree_t *, const void *);

/*
 * Returns pointer with iterator-semantics to the first element.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_begin(const fx_tree_t *);

/*
 * Returns pointer with iterator-semantics to one past the last element.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_end(const fx_tree_t *);

/*
 * Returns TRUE is an iterator is referring to end-of-sequence (or NULL).
 */
int fx_tree_iseoseq(const fx_tree_t *, const fx_tlink_t *);

/*
 * Enables iterator operator++
 * Returns fx_tree_end at the end of sequence.
 */
fx_tlink_t *fx_tree_next(const fx_tree_t *, const fx_tlink_t *);

/*
 * Enables iterator operator--
 */
fx_tlink_t *fx_tree_prev(const fx_tree_t *, const fx_tlink_t *);

/*
 * Searches for an element by key. Returns NULL if not exist.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_find(const fx_tree_t *, const void *);

/*
 * Searches for the first element by key. Returns NULL if not exist.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_find_first(const fx_tree_t *, const void *);

/*
 * Finds the first element whose key is not less than. Returns NULL if not
 * exist.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_lower_bound(const fx_tree_t *, const void *);

/*
 * Finds the first element whose key greater than. Returns NULL if not exist.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_upper_bound(const fx_tree_t *, const void *);

/*
 * Finds a range containing all elements with key.
 */
FX_ATTR_NONNULL
void fx_tree_equal_range(const fx_tree_t *,
                         const void *, fx_tree_range_t *);

/*
 * Inserts new element. Possibly multiple elements with same key.
 */
FX_ATTR_NONNULL
void fx_tree_insert(fx_tree_t *, fx_tlink_t *);

/*
 * Inserts new unique element. Returns -1 if key already exists.
 */
FX_ATTR_NONNULL
int fx_tree_insert_unique(fx_tree_t *, fx_tlink_t *);

/*
 * Inserts new element or replace existing one. Returns previous element if key
 * already exists, NULL otherwise.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_insert_replace(fx_tree_t *, fx_tlink_t *);

/*
 * Erases an existing element.
 */
FX_ATTR_NONNULL
void fx_tree_remove(fx_tree_t *, fx_tlink_t *);

/*
 * Erases iterators range [first, last).
 */
FX_ATTR_NONNULL
void fx_tree_remove_range(fx_tree_t *, fx_tlink_t *, fx_tlink_t *);

/*
 * Do tree-walk, remove each node and apply user's hook. User provided pointer
 * ptr is passed as hint to fn.
 */
FX_ATTR_NONNULL1
void fx_tree_clear(fx_tree_t *, fx_tnode_fn fn, void *ptr);


/*
 * Removes all the elements from the tree. Returns a linked list of previously
 * stored elements (linked by ->right).
 *
 * NB: It is up to the user to release any resources associated with the tree's
 *     nodes.
 */
FX_ATTR_NONNULL
fx_tlink_t *fx_tree_reset(fx_tree_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Common 3-way compare functions when using (unsigned) long-integers as keys.
 */
int fx_compare3(long x, long y);

int fx_ucompare3(unsigned long x, unsigned long y);


#endif /* FUNEX_TREE_H_ */





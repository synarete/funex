/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"

#include "compiler.h"
#include "macros.h"
#include "bitops.h"


void fx_setf(unsigned int *flags, unsigned int mask)
{
	*flags |= mask;
}

void fx_unsetf(unsigned int *flags, unsigned int mask)
{
	*flags &= ~mask;
}

int fx_testf(unsigned int flags, unsigned int mask)
{
	return ((flags & mask) == mask);
}


void fx_setlf(unsigned long *flags, unsigned long mask)
{
	*flags |= mask;
}

void fx_unsetlf(unsigned long *flags, unsigned long mask)
{
	*flags &= ~mask;
}

int fx_testlf(unsigned long flags, unsigned long mask)
{
	return ((flags & mask) == mask);
}


unsigned long fx_rotleft(unsigned long word, unsigned int shift)
{
	return (word << shift) | (word >> (64 - shift));
}

unsigned long fx_rotright(unsigned long word, unsigned int shift)
{
	return (word >> shift) | (word << (64 - shift));
}

/* Returns the number of 1-bits in x */
int fx_popcount(unsigned int x)
{
	return __builtin_popcount(x);
}



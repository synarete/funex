/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "compiler.h"
#include "macros.h"
#include "panic.h"
#include "list.h"
#include "listi.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Link:
 */
void fx_link_init(fx_link_t *lnk)
{
	link_init(lnk);
}

void fx_link_destroy(fx_link_t *lnk)
{
	lnk->next = lnk->prev = NULL;
}

void fx_link_set(fx_link_t *lnk, fx_link_t *prev, fx_link_t *next)
{
	lnk->next = next;
	lnk->prev = prev;
}

void fx_link_insert(fx_link_t *lnk, fx_link_t *prev, fx_link_t *next)
{
	fx_link_set(lnk, prev, next);

	next->prev = lnk;
	prev->next = lnk;
}

void fx_link_remove(fx_link_t *lnk)
{
	fx_link_t *next, *prev;

	next = lnk->next;
	prev = lnk->prev;

	next->prev = prev;
	prev->next = next;

	fx_link_set(lnk, lnk, lnk);
}

int fx_link_isunlinked(const fx_link_t *lnk)
{
	return (lnk->next == lnk) && (lnk->prev == lnk);
}

int fx_link_islinked(const fx_link_t *lnk)
{
	return ((lnk->next != NULL) && (lnk->prev != NULL) &&
	        !fx_link_isunlinked(lnk));
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * List:
 */
static void list_inc_size(fx_list_t *ls)
{
	ls->size += 1;
}

static void list_dec_size(fx_list_t *ls)
{
	ls->size -= 1;
}

static void list_tie(fx_list_t *ls, fx_link_t *first, fx_link_t *last)
{
	ls->head.next = first;
	ls->head.prev = last;
	first->prev = &ls->head;
	last->next  = &ls->head;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_list_init(fx_list_t *ls)
{
	list_tie(ls, &ls->head, &ls->head);
	ls->size = 0;
}

void fx_list_destroy(fx_list_t *ls)
{
	fx_link_set(&ls->head, NULL, NULL);
	ls->size = 0;
}

size_t fx_list_size(const fx_list_t *ls)
{
	return list_size(ls);
}

int fx_list_isempty(const fx_list_t *ls)
{
	return (list_size(ls) == 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_list_clear(fx_list_t *ls)
{
	/* Enusre elements are not linked */
	while (list_size(ls) > 0) {
		fx_list_pop_front(ls);
	}

	link_init(&ls->head);  /* Relink cyclic */
	ls->size = 0;
}

void fx_list_push_front(fx_list_t *ls, fx_link_t *lnk)
{
	fx_list_insert_after(ls, &ls->head, lnk);
}

void fx_list_push_back(fx_list_t *ls, fx_link_t *lnk)
{
	fx_list_insert_before(ls, lnk, &ls->head);
}

void fx_list_insert_before(fx_list_t *ls,
                           fx_link_t *lnk, fx_link_t *next_lnk)
{
	fx_link_insert(lnk, next_lnk->prev, next_lnk);
	list_inc_size(ls);
}

void fx_list_insert_after(fx_list_t *ls,
                          fx_link_t *prev_lnk, fx_link_t *lnk)
{
	fx_link_insert(lnk, prev_lnk, prev_lnk->next);
	list_inc_size(ls);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_list_remove(fx_list_t *ls, fx_link_t *lnk)
{
	if (list_size(ls) > 0) {
		fx_link_remove(lnk);
		list_dec_size(ls);
	}
}

fx_link_t *fx_list_pop_front(fx_list_t *ls)
{
	fx_link_t *lnk = NULL;

	if (list_size(ls) > 0) {
		lnk = ls->head.next;
		fx_list_remove(ls, lnk);
	}
	return lnk;
}

fx_link_t *fx_list_pop_back(fx_list_t *ls)
{
	fx_link_t *lnk = NULL;

	if (list_size(ls) > 0) {
		lnk = ls->head.prev;
		fx_list_remove(ls, lnk);
	}
	return lnk;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_link_t *fx_list_begin(const fx_list_t *ls)
{
	const fx_link_t *lnk;

	lnk = &ls->head;
	return (fx_link_t *)lnk->next;
}

fx_link_t *fx_list_end(const fx_list_t *ls)
{
	const fx_link_t *lnk;

	lnk = &ls->head;
	return (fx_link_t *)lnk;
}

fx_link_t *fx_list_next(const fx_list_t *ls, const fx_link_t *lnk)
{
	(void)ls;
	return lnk->next;
}

fx_link_t *fx_list_prev(const fx_list_t *ls, const fx_link_t *lnk)
{
	(void)ls;
	return lnk->prev;
}

fx_link_t *fx_list_front(const fx_list_t *ls)
{
	const fx_link_t *lnk = NULL;

	if (list_size(ls) > 0) {
		lnk = ls->head.next;
	}
	return (fx_link_t *)lnk;
}

fx_link_t *fx_list_back(const fx_list_t *ls)
{
	const fx_link_t *lnk = NULL;

	if (list_size(ls) > 0) {
		lnk = ls->head.prev;
	}
	return (fx_link_t *)lnk;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

fx_link_t *fx_list_find(const fx_list_t *ls, fx_link_unary_pred fn)
{
	const fx_link_t *res;
	const fx_link_t *itr;
	const fx_link_t *end;

	res = NULL;
	itr = fx_list_begin(ls);
	end = fx_list_end(ls);
	while (itr != end) {
		if (fn(itr)) {
			res = itr;
			break;
		}
		itr = itr->next;
	}
	return (fx_link_t *)res;
}

fx_link_t *fx_list_vfind(const fx_list_t *ls,
                         fx_link_vbinary_pred fn, const void *v)
{
	const fx_link_t *res;
	const fx_link_t *itr;
	const fx_link_t *end;

	res = NULL;
	itr = fx_list_begin(ls);
	end = fx_list_end(ls);
	while (itr != end) {
		if (fn(itr, v)) {
			res = itr;
			break;
		}
		itr = itr->next;
	}
	return (fx_link_t *)res;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_list_foreach(const fx_list_t *ls, fx_link_unary_fn fn)
{
	fx_link_t *itr, *end;

	itr = fx_list_begin(ls);
	end = fx_list_end(ls);
	while (itr != end) {
		if (fn(itr)) {
			break;
		}
		itr = itr->next;
	}
}

void fx_list_vforeach(const fx_list_t *ls,
                      fx_link_vbinary_fn fn, const void *v)
{
	fx_link_t *itr, *end;

	itr = fx_list_begin(ls);
	end = fx_list_end(ls);
	while (itr != end) {
		if (fn(itr, v)) {
			break;
		}
		itr = itr->next;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_list_append(fx_list_t *ls1, fx_list_t *ls2)
{
	size_t sz;
	fx_link_t *head, *lnk;

	sz = list_size(ls2);
	if (sz > 0) {
		/* Link content of list2 into list1 */
		head = &ls1->head;

		lnk = ls2->head.next;
		lnk->prev = head->prev;
		lnk->prev->next = lnk;

		lnk = ls2->head.prev;
		lnk->next  = head;
		lnk->next->prev = lnk;

		ls1->size += sz;

		/* Reset list2 to zero-size */
		fx_list_init(ls2);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void list_swap_sizes(fx_list_t *ls1, fx_list_t *ls2)
{
	const size_t sz1 = ls1->size;

	ls1->size = ls2->size;
	ls2->size = sz1;
}

static void list_copy_head(const fx_list_t *ls, fx_link_t *head)
{
	head->next = ls->head.next;
	head->prev = ls->head.prev;
}

void fx_list_swap(fx_list_t *ls1, fx_list_t *ls2)
{
	fx_link_t  head1, head2;

	list_copy_head(ls1, &head1);
	list_copy_head(ls2, &head2);

	if (list_size(ls2) > 0) {
		list_tie(ls1, head2.next, head2.prev);
	} else {
		list_tie(ls1, &ls1->head, &ls1->head);
	}

	if (list_size(ls1) > 0) {
		list_tie(ls2, head1.next, head1.prev);
	} else {
		list_tie(ls2, &ls2->head, &ls2->head);
	}

	list_swap_sizes(ls1, ls2);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_slist_init(fx_slink_t *head)
{
	head->next = NULL;
}

void fx_slist_destroy(fx_slink_t *head)
{
	fx_slink_t *itr;
	fx_slink_t *cur;

	itr = head;
	while (itr != NULL) {
		cur = itr;
		itr = itr->next;
		cur->next = NULL;
	}
}

size_t fx_slist_length(const fx_slink_t *head)
{
	size_t cnt;
	const fx_slink_t *itr;

	cnt = 0;
	itr = head->next;
	while (itr != NULL) {
		cnt++;
		itr = itr->next;
	}
	return cnt;
}

int fx_slist_isempty(const fx_slink_t *head)
{
	return (head->next == NULL);
}

void fx_slist_push_front(fx_slink_t *head, fx_slink_t *lnk)
{
	fx_slist_insert(head, head->next, lnk);
}

fx_slink_t *fx_slist_pop_front(fx_slink_t *head)
{
	return fx_slist_remove(head, head->next);
}

fx_slink_t *fx_slist_insert(fx_slink_t *head, fx_slink_t *pos, fx_slink_t *lnk)
{
	fx_slink_t *prv;
	fx_slink_t *itr;

	prv = head;
	itr = head->next;
	while ((itr != NULL) && (itr != pos)) {
		prv = itr;
		itr = itr->next;
	}

	if (itr == pos) {
		prv->next  = lnk;
		lnk->next = pos;
	}
	return itr;
}

fx_slink_t *fx_slist_remove(fx_slink_t *head, fx_slink_t *lnk)
{
	fx_slink_t *prv;
	fx_slink_t *itr;

	prv = head;
	itr = head->next;
	while ((itr != NULL) && (itr != lnk)) {
		prv = itr;
		itr = itr->next;
	}

	if (itr != NULL) {
		prv->next = itr->next;
		itr->next = NULL;
	}
	return itr;
}

fx_slink_t *fx_slist_find(const fx_slink_t *head,
                          fx_slink_test_fn fn, const void *v)
{
	fx_slink_t *itr;

	itr = head->next;
	while (itr != NULL) {
		if (fn(itr, v)) {
			break;
		}
		itr = itr->next;
	}
	return (fx_slink_t *)itr;
}

fx_slink_t *fx_slist_findremove(fx_slink_t *head,
                                fx_slink_test_fn fn, const void *v)
{
	fx_slink_t *prv;
	fx_slink_t *itr;

	prv = head;
	itr = head->next;
	while (itr != NULL) {
		if (fn(itr, v)) {
			break;
		}
		prv = itr;
		itr = itr->next;
	}

	if (itr != NULL) {
		prv->next = itr->next;
		itr->next = NULL;
	}
	return (fx_slink_t *)itr;
}

void fx_slist_foreach(const fx_slink_t *head,
                      fx_slink_foreach_fn fn, const void *v)
{
	fx_slink_t *itr;

	itr = head->next;
	while (itr != NULL) {
		fn(itr, v);
		itr = itr->next;
	}
}





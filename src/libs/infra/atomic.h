/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_ATOMIC_H_
#define FUNEX_ATOMIC_H_

#if (__WORDSIZE == 64)
#define FX_HAVE_ATOMIC64     (1)
#endif

/* Type-safe wrappers over compiler-specific atomic extension */
size_t fx_atomic_size_read(volatile const size_t *p);

size_t fx_atomic_size_test_and_set(volatile size_t *p, size_t n);

size_t fx_atomic_size_add_and_fetch(volatile size_t *p, size_t n);

size_t fx_atomic_size_sub_and_fetch(volatile size_t *p, size_t n);

void fx_atomic_size_add(volatile size_t *p, size_t n);

void fx_atomic_size_inc(volatile size_t *p);


int32_t fx_atomic_i32_read(volatile const int32_t *p);

int32_t fx_atomic_i32_test_and_set(volatile int32_t *p, int32_t i);

int32_t fx_atomic_i32_add_and_fetch(volatile int32_t *p, int32_t i);

int32_t fx_atomic_i32_sub_and_fetch(volatile int32_t *p, int32_t i);


uint32_t fx_atomic_u32_read(volatile const uint32_t *p);

uint32_t fx_atomic_u32_test_and_set(volatile uint32_t *p, uint32_t u);

uint32_t fx_atomic_u32_add_and_fetch(volatile uint32_t *p, uint32_t u);

uint32_t fx_atomic_u32_sub_and_fetch(volatile uint32_t *p, uint32_t u);


uint32_t fx_atomic_get(volatile const uint32_t *p);

void fx_atomic_set(volatile uint32_t *p, uint32_t);

void fx_atomic_add(volatile uint32_t *p, uint32_t u);

void fx_atomic_sub(volatile uint32_t *p, uint32_t u);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#if defined(FX_HAVE_ATOMIC64)

int64_t fx_atomic_i64_read(volatile const int64_t *p);

int64_t fx_atomic_i64_test_and_set(volatile int64_t *p, int64_t i);

int64_t fx_atomic_i64_add_and_fetch(volatile int64_t *p, int64_t i);

int64_t fx_atomic_i64_sub_and_fetch(volatile int64_t *p, int64_t i);


uint64_t fx_atomic_u64_read(volatile const uint64_t *p);

uint64_t fx_atomic_u64_test_and_set(volatile uint64_t *p, uint64_t u);

uint64_t fx_atomic_u64_add_and_fetch(volatile uint64_t *p, uint64_t u);

uint64_t fx_atomic_u64_sub_and_fetch(volatile uint64_t *p, uint64_t u);


uint64_t fx_atomic_get64(volatile const uint64_t *p);

void fx_atomic_set64(volatile uint64_t *p, uint64_t u);

void fx_atomic_add64(volatile uint64_t *p, uint64_t u);

void fx_atomic_sub64(volatile uint64_t *p, uint64_t u);

#endif /* FX_HAVE_ATOMIC64 */

#endif /* FUNEX_ATOMIC_H_ */


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_TIMEX_H_
#define FUNEX_TIMEX_H_

/* Make standrad timespec part of fnx namespace */
struct timeval;
struct timespec;
typedef struct timespec fx_timespec_t;


/* Wrappers over _sleep */
void fx_usleep(unsigned int usec);
void fx_msleep(unsigned int usec);

/* Assign nil values */
void fx_timespec_init(fx_timespec_t *);

/* Timespecs copy helper */
void fx_timespec_copy(fx_timespec_t *, const fx_timespec_t *);

/* Fill timespec with current time using 'gettimeofday' */
void fx_timespec_gettimeofday(fx_timespec_t *);

/* Fill timespec with current MONOTONIC time using 'clock_gettime' */
void fx_timespec_getmonotime(fx_timespec_t *);

/* Adds n usec to tp value's, protect against possible overlap */
void fx_timespec_usecadd(fx_timespec_t *, long n);

/* Adds n msec (millisec) to tp value's, protect against possible overlap. */
void fx_timespec_msecadd(fx_timespec_t *, long n);

/* Returns the diff in usecs between two timespecs */
long fx_timespec_usecdiff(const fx_timespec_t *, const fx_timespec_t *);

/* Returns the diff in msecs between two timespecs */
long fx_timespec_msecdiff(const fx_timespec_t *, const fx_timespec_t *);

/* Convert timespec to millisec integer */
long fx_timespec_millisec(const fx_timespec_t *);

/* Convert timespec to microsec integer */
long fx_timespec_microsec(const fx_timespec_t *);

/* Fill timespec from millisec|microsec integer values */
void fx_ts_from_millisec(fx_timespec_t *, long millisec_value);
void fx_ts_from_microsec(fx_timespec_t *, long microsec_value);


/* Convert timespec <--> timeval */
void fx_timespec_to_timeval(const fx_timespec_t *, struct timeval *);

void fx_timespec_from_timeval(fx_timespec_t *, const struct timeval *);

#endif /* FUNEX_TIMEX_H_ */




/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_FIFO_H_
#define FUNEX_FIFO_H_


/*
 * Linked-queue over user-provided (intrusive) elements, with first-in-first-out
 * semantics. Thread safe.
 *
 * Supports two-levels priority. Underneath, the object maintains two linked
 * lists (queues): one for normal objects and one for high-priority objects.
 * Insertion with priority will cause more urgent elements will to be removed
 * first.
 */
struct fx_fifo {
	size_t     ff_npend;
	fx_lock_t  ff_lock;
	fx_list_t  ff_list[2];
};
typedef struct fx_fifo fx_fifo_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Construct an an empty object.
 */
void fx_fifo_init(fx_fifo_t *);

/*
 * Destroy existing object. It is up to the user to release any user-provided
 * resources still referenced by the internal list-queue.
 */
void fx_fifo_destroy(fx_fifo_t *);


/*
 * Returns the total number of currently stored elements in queue.
 * NB: Not thread safe (no locking).
 */
size_t fx_fifo_size(const fx_fifo_t *);

/*
 * Return TRUE if size is zero.
 */
int fx_fifo_isempty(const fx_fifo_t *);

/*
 * Push single user-provided element to end-of-queue. For the second call, q may
 * be either 0 for high-priority elements, 1 for normal elements.
 */
void fx_fifo_push(fx_fifo_t *, fx_link_t *);

void fx_fifo_pushq(fx_fifo_t *, fx_link_t *, int q);

/*
 * Pops single element from front-of-queue without any wait on condition. If
 * queue is empty, returns NULL.
 */
fx_link_t *fx_fifo_trypop(fx_fifo_t *fifo);

/*
 * Pops single element from front-of-queue. Returns the removed element, or
 * NULL pointer if timed-out.
 */
fx_link_t *fx_fifo_pop(fx_fifo_t *fifo, size_t usec_timeout);

/*
 * Pops multiple elements from front-of-queue. Returns a chain of up to n
 * removed element, or NULL pointer if timed-out. Does not pend more then usec
 * timeout until the first element is available. Thus, it may return a list of
 * less then n elements.
 */
fx_link_t *fx_fifo_popn(fx_fifo_t *fifo, size_t n, size_t usec_timeout);


#endif /* FUNEX_FIFO_H_ */





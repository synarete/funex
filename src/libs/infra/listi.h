/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_LISTI_H_
#define FUNEX_LISTI_H_


static inline void link_init(fx_link_t *lnk)
{
	lnk->next = lnk->prev = lnk;
}

static inline void link_destroy(fx_link_t *lnk)
{
	lnk->next = lnk->prev = NULL;
}


static inline size_t list_size(const fx_list_t *ls)
{
	return ls->size;
}

static inline int list_isempty(const fx_list_t *ls)
{
	return (list_size(ls) == 0);
}

#endif /* FUNEX_LISTI_H_ */

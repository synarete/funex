/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_LIST_H_
#define FUNEX_LIST_H_


/*
 * Forward declarations:
 */
struct fx_link;
struct fx_list;


/*
 * List-link element within bidirectional linked-list. Typically, part (at the
 * beginning) of user's object.
 */
struct fx_link {
	struct fx_link *next;
	struct fx_link *prev;

} FX_ATTR_ALIGNED;
typedef struct fx_link     fx_link_t;


/*
 * Bidirectional cyclic linked-list. The actual type of objects is defined
 * by the user.
 *
 * NB: It is up to the user to ensure that the linked elements remain valid
 * throughout the time they are members in the list.
 *
 */
struct fx_list {
	fx_link_t head;    /* "Pseudo" node; Always be cyclic   */
	size_t    size;    /* Number of elements (without head) */
};
typedef struct fx_list         fx_list_t;


/*
 * Unidirectional linked-list: each element is linked only to the next element.
 * That is, a sequence-container that supports forward traversal only, and
 * (amortized) constant time insertion of elements. The actual type of objects
 * is defined by the user.
 *
 * NB: It is up to the user to ensure that the linked elements remain valid
 * throughout the time they are members os a list.
 */
struct fx_slink {
	struct fx_slink *next;
};
typedef struct fx_slink    fx_slink_t;


typedef int (*fx_slink_test_fn)(const fx_slink_t *, const void *);
typedef void (*fx_slink_foreach_fn)(fx_slink_t *, const void *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Compare/Apply functions:
 */
typedef int (*fx_link_unary_pred)(const fx_link_t *);

typedef int (*fx_link_binary_pred)(const fx_link_t *,
                                   const fx_link_t *);

typedef int (*fx_link_vbinary_pred)(const fx_link_t *, const void *);

typedef int (*fx_link_unary_fn)(fx_link_t *);

typedef int (*fx_link_binary_fn)(fx_link_t *, fx_link_t *);

typedef int (*fx_link_vbinary_fn)(fx_link_t *, const void *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Create cyclic (unlinked, selfish) link */
FX_ATTR_NONNULL
void fx_link_init(fx_link_t *);

/* Have NULL links */
FX_ATTR_NONNULL
void fx_link_destroy(fx_link_t *);


/* Set link within linked-list */
void fx_link_set(fx_link_t *, fx_link_t *, fx_link_t *);

/* Link into existing list, keep it in order */
void fx_link_insert(fx_link_t *, fx_link_t *, fx_link_t *);

/* Remove link from cyclic-linked list, keep it in order */
FX_ATTR_NONNULL
void fx_link_remove(fx_link_t *);


/* Returns TRUE if link is not associated with any list (selfish links) */
FX_ATTR_NONNULL
int fx_link_isunlinked(const fx_link_t *);

/* Returns TRUE if link is associated with a list (non-selfish no-null links) */
FX_ATTR_NONNULL
int fx_link_islinked(const fx_link_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Constructor: initializes an empty, cyclic list.
 */
FX_ATTR_NONNULL
void fx_list_init(fx_list_t *ls);

/*
 * Destroys list object. NB: It is up to the user to release any linked
 * resources associated with the list.
 */
FX_ATTR_NONNULL
void fx_list_destroy(fx_list_t *ls);


/*
 * Returns the number of elements currently linked in list.
 */
FX_ATTR_NONNULL
size_t fx_list_size(const fx_list_t *ls);


/*
 * Returns TRUE if no elements are associated with the list.
 */
FX_ATTR_NONNULL
int fx_list_isempty(const fx_list_t *ls);



/*
 * Link user-provided node into the beginning of list (intrusive).
 */
FX_ATTR_NONNULL
void fx_list_push_front(fx_list_t *ls, fx_link_t *lnk);


/*
 * Link node to end of list
 */
FX_ATTR_NONNULL
void fx_list_push_back(fx_list_t *ls, fx_link_t *lnk);


/*
 * Insert node before existing node in list
 */
FX_ATTR_NONNULL
void fx_list_insert_before(fx_list_t *ls,
                           fx_link_t *lnk, fx_link_t *next_lnk);

/*
 * Insert node after existing node in list
 */
FX_ATTR_NONNULL
void fx_list_insert_after(fx_list_t *ls,
                          fx_link_t *prev_lnk, fx_link_t *lnk);

/*
 * Unlink existing node.
 */
FX_ATTR_NONNULL
void fx_list_remove(fx_list_t *ls, fx_link_t *lnk);


/*
 * Removes the first node.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_pop_front(fx_list_t *ls);

/*
 * Removes the last node.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_pop_back(fx_list_t *ls);



/*
 * Returns the first node, or end() if empty.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_begin(const fx_list_t *ls);

/*
 * Returns one "past" last valid node.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_end(const fx_list_t *ls);


/*
 * Iterators semantics (bidirectional).
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_next(const fx_list_t *ls, const fx_link_t *lnk);

FX_ATTR_NONNULL
fx_link_t *fx_list_prev(const fx_list_t *ls, const fx_link_t *lnk);


/*
 * Returns the first/last element, or NULL if empty.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_front(const fx_list_t *ls);

FX_ATTR_NONNULL
fx_link_t *fx_list_back(const fx_list_t *ls);


/*
 * Removes all elements (reduces size to zero).
 *
 * NB: It is up to the user to release any resource associate with the list's
 * elements, prior to clear.
 */
FX_ATTR_NONNULL
void fx_list_clear(fx_list_t *ls);


/*
 * Searches for the first element for which fn returns TRUE. If none exists in
 * the list, returns NULL.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_find(const fx_list_t *ls, fx_link_unary_pred fn);

/*
 * Searches for the first element for which fn with v returns TRUE. If none
 * exists in the list, returns NULL.
 */
FX_ATTR_NONNULL
fx_link_t *fx_list_vfind(const fx_list_t *ls,
                         fx_link_vbinary_pred fn, const void *v);


/*
 * Apply unary function fn on every element in the list.
 */
FX_ATTR_NONNULL
void fx_list_foreach(const fx_list_t *ls, fx_link_unary_fn fn);


/*
 * Apply unary function fn on every element in the list with v.
 */
FX_ATTR_NONNULL
void fx_list_vforeach(const fx_list_t *ls,
                      fx_link_vbinary_fn fn, const void *v);



/*
 * Swaps the content of two lists.
 */
FX_ATTR_NONNULL
void fx_list_swap(fx_list_t *ls1, fx_list_t *ls2);

/*
 * Appends the content of list2 to list1. Post-op, the size of list2 is reduced
 * to zero.
 */
FX_ATTR_NONNULL
void fx_list_append(fx_list_t *ls1, fx_list_t *ls2);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Constructor: initializes an empty, null-terminated, single-linked list.
 */
FX_ATTR_NONNULL
void fx_slist_init(fx_slink_t *head);

/*
 * Destroys list object: iterates on list's elements and break their links.
 *
 * NB: It is up to the user to release any linked resources associated with
 * the list.
 */
FX_ATTR_NONNULL
void fx_slist_destroy(fx_slink_t *head);

/*
 * Iterates and counts the number of linked-elements in list.
 */
FX_ATTR_NONNULL
size_t fx_slist_length(const fx_slink_t *head);

/*
 * Returns TRUE if no elements are associated with the list.
 */
FX_ATTR_NONNULL
int fx_slist_isempty(const fx_slink_t *head);


/*
 * Link user-provided element into the beginning of list (intrusive).
 */
FX_ATTR_NONNULL
void fx_slist_push_front(fx_slink_t *head, fx_slink_t *lnk);

/*
 * Removes the first element.
 */
FX_ATTR_NONNULL
fx_slink_t *fx_slist_pop_front(fx_slink_t *head);


/*
 * Inserts an element previous to existing element in the list. Returns NULL if
 * pos element was not found in list.
 */
FX_ATTR_NONNULL
fx_slink_t *fx_slist_insert(fx_slink_t *head,
                            fx_slink_t *pos, fx_slink_t *lnk);

/*
 * Removes an element which is linked on list. Returns NULL if the element was
 * not found in list.
 */
FX_ATTR_NONNULL
fx_slink_t *fx_slist_remove(fx_slink_t *head, fx_slink_t *lnk);


/*
 * Searches for the first element for which fn with v returns TRUE. If none
 * exists in the list, returns NULL.
 */
fx_slink_t *fx_slist_find(const fx_slink_t *head,
                          fx_slink_test_fn fn, const void *v);

/*
 * Searches for the first element for which fn with v returns TRUE and remove it
 * from the list. If exists returns the removed element; otherwise returns NULL.
 */
fx_slink_t *fx_slist_findremove(fx_slink_t *head,
                                fx_slink_test_fn fn, const void *v);

/*
 * Apply user-provided hook fn with v on every element in the list.
 */
void fx_slist_foreach(const fx_slink_t *head,
                      fx_slink_foreach_fn fn, const void *v);


#endif /* FUNEX_LIST_H_ */



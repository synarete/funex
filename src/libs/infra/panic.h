/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_PANIC_H_
#define FUNEX_PANIC_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Handle fatal errors which prevents further execution of the application. By
 * default, prints error strings and abort. The user may customize with his own
 * panic-hook call-back.
 */
#define FX_ASSERT(expr) \
	do { if (fx_unlikely(!(expr))) fx_assertion_failure( \
			        __FILE__, __LINE__, NULL, /* FX_FUNCTION */ \
			        FX_MAKESTR(expr)); } while(0)

#define FX_PANIC(fmt, ...)                                 \
	fx_panicf(__FILE__, __LINE__, FX_FUNCTION, fmt, __VA_ARGS__)


#define fx_assert(expr)        FX_ASSERT(expr)
#define fx_assert_ok(rc)       fx_assert((rc) == 0)
#define fx_panic(fmt, ...)     FX_PANIC(fmt, __VA_ARGS__)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * End process with abort.
 */
FX_ATTR_NORETURN
void fx_die(void);

/*
 * Panic -- halt process execution.
 */
FX_ATTR_NORETURN
FX_ATTR_FMTPRINTF(4, 5)
void fx_panicf(const char *, unsigned, const char *, const char *, ...);

/*
 * Assertion failure, which calls panic.
 */
FX_ATTR_NORETURN
void fx_assertion_failure(const char *, unsigned, const char *, const char *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

typedef void (*fx_error_cb)(int, const char *);

/*
 * Set user's hook for run-time fatal errors
 */
void fx_set_panic_callback(fx_error_cb fn);


/*
 * Prefer command line utility 'addr2line' with addresses for back-tracing over
 * 'backtrace_symbols' which gives poor results. User will have a string ready
 * for copy-paste into command line.
 */
int fx_addr2line_backtrace(char *buf, size_t buf_sz);

#endif /* FUNEX_PANIC_H_ */





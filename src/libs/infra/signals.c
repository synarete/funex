/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#include "compiler.h"
#include "macros.h"
#include "panic.h"
#include "timex.h"
#include "signals.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* User-provided halt-cleanup hook */
static fx_sigaction_cb s_halt_callback = NULL;

/*
 * Signal-action which simply terminates the process. See example in GNU libc
 * documentations (24.4.2).
 */
static void fx_sigaction_halt_handler(int signum, siginfo_t *si, void *p)
{
	int reraise = 1;
	fx_sigaction_cb user_hook = s_halt_callback;
	static volatile sig_atomic_t s_halt_in_progress = 0;

	/*
	 * Since this handler is established for more than one kind of signal, it
	 * might still get invoked recursively by delivery of some other kind of
	 * signal. Using a static variable to keep track of that.
	 */
	if (s_halt_in_progress) {
		raise(signum);
		return;
	}

	/* Let user do cleanups via call-back hook */
	s_halt_in_progress = 1;
	if (user_hook != NULL) {
		reraise = user_hook(signum, si, p);
	}
	s_halt_in_progress = 0;

	/*
	 * Re-raise the signal. We reactivate the signal's default handling, which
	 * is to terminate the process. We could just call exit or abort, but
	 * re-raising the signal sets the return status from the process correctly.
	 */
	if (reraise) {
		signal(signum, SIG_DFL);
		raise(signum);
	}
}

/*
 * Signal-action which forces abort.
 */
static void fx_sigaction_abort_handler(int signum, siginfo_t *si, void *p)
{
	(void)si;
	(void)signum;
	(void)p;
	abort();
}


static struct sigaction fx_s_sigaction_ign = {
	.sa_handler     = SIG_IGN
};

static struct sigaction fx_s_sigaction_halt = {
	.sa_sigaction   = fx_sigaction_halt_handler,
	.sa_flags       = SA_SIGINFO
};

static struct sigaction fx_s_sigaction_abort = {
	.sa_sigaction   = fx_sigaction_abort_handler,
	.sa_flags       = SA_SIGINFO
};



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Register signal-action handlers.
 */
static void fx_register_sigaction(int signum, const struct sigaction *act)
{
	int rc;

	errno = 0;
	rc = sigaction(signum, act, NULL);
	if (rc != 0) {
		fx_panic("sigaction-failed signum=%d rc=%d", signum, rc);
	}
}

void fx_sigaction_ignore(int signum)
{
	fx_register_sigaction(signum,  &fx_s_sigaction_ign);
}

void fx_sigaction_halt(int signum)
{
	fx_register_sigaction(signum,  &fx_s_sigaction_halt);
}

void fx_sigaction_abort(int signum)
{
	fx_register_sigaction(signum,  &fx_s_sigaction_abort);
}

void fx_sigaction_nocatch(int signum)
{
	(void)signum;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_set_default_sigactions(void)
{
	fx_sigaction_ignore(SIGHUP);   /* 1    Hangup (POSIX)                  */
	fx_sigaction_halt(SIGINT);     /* 2    Interrupt (ANSI)                */
	fx_sigaction_halt(SIGQUIT);    /* 3    Quit (POSIX)                    */
	fx_sigaction_abort(SIGILL);    /* 4    Illegal instruction (ANSI)      */
	fx_sigaction_ignore(SIGTRAP);  /* 5    Trace trap (POSIX)              */
	fx_sigaction_nocatch(SIGABRT); /* 6    Abort (ANSI)                    */
	fx_sigaction_nocatch(SIGBUS);  /* 7    BUS error (4.2 BSD)             */
	fx_sigaction_nocatch(SIGFPE);  /* 8    Floating-point exception (ANSI) */
	fx_sigaction_nocatch(SIGKILL); /* 9    Kill, unblockable (POSIX)       */
	fx_sigaction_ignore(SIGUSR1);  /* 10   User-defined signal 1 (POSIX)   */
	fx_sigaction_abort(SIGSEGV);   /* 11   Segmentation violation (ANSI)   */
	fx_sigaction_ignore(SIGUSR2);  /* 12   User-defined signal 2 (POSIX)   */
	fx_sigaction_ignore(SIGPIPE);  /* 13   Broken pipe (POSIX)             */
	fx_sigaction_ignore(SIGALRM);  /* 14   Alarm clock (POSIX)             */
	fx_sigaction_halt(SIGTERM);    /* 15   Termination (ANSI)              */
	fx_sigaction_abort(SIGSTKFLT); /* 16   Stack fault                     */
	fx_sigaction_ignore(SIGCHLD);  /* 17   Child status has changed (POSIX)*/
	fx_sigaction_ignore(SIGCONT);  /* 18   Continue (POSIX)                */
	fx_sigaction_nocatch(SIGSTOP); /* 19   Stop, unblockable (POSIX)       */
	fx_sigaction_halt(SIGTSTP);    /* 20   Keyboard stop (POSIX)           */
	fx_sigaction_halt(SIGTTIN);    /* 21   Background read from tty (POSIX)*/
	fx_sigaction_halt(SIGTTOU);    /* 22   Background write to tty (POSIX) */
	fx_sigaction_ignore(SIGURG);   /* 23   Urgent condition on socket */
	fx_sigaction_halt(SIGXCPU);    /* 24   CPU limit exceeded (4.2 BSD)    */
	fx_sigaction_halt(SIGXFSZ);    /* 25   File size limit exceeded        */
	fx_sigaction_halt(SIGVTALRM);  /* 26   Virtual alarm clock (4.2 BSD)   */
	fx_sigaction_nocatch(SIGPROF); /* 27   Profiling alarm clock (4.2 BSD) */
	fx_sigaction_ignore(SIGWINCH); /* 28   Window size change (4.3 BSD, Sun) */
	fx_sigaction_ignore(SIGIO);    /* 29   I/O now possible (4.2 BSD)     */
	fx_sigaction_halt(SIGPWR);     /* 30   Power failure restart (System V) */
	fx_sigaction_halt(SIGSYS);     /* 31   Bad system call  */
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_set_sighalt_callback(fx_sigaction_cb cb)
{
	s_halt_callback = cb;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Converts signal-number to human readable string:
 */
static const char *s_signum_str[] = {
	[SIGHUP]        = "SIGHUP",
	[SIGINT]        = "SIGINT",
	[SIGQUIT]       = "SIGQUIT",
	[SIGILL]        = "SIGILL",
	[SIGTRAP]       = "SIGTRAP",
	[SIGABRT]       = "SIGABRT",
	[SIGBUS]        = "SIGBUS",
	[SIGFPE]        = "SIGFPE",
	[SIGKILL]       = "SIGKILL",
	[SIGUSR1]       = "SIGUSR1",
	[SIGSEGV]       = "SIGSEGV",
	[SIGUSR2]       = "SIGUSR2",
	[SIGPIPE]       = "SIGPIPE",
	[SIGALRM]       = "SIGALRM",
	[SIGTERM]       = "SIGTERM",
	[SIGSTKFLT]     = "SIGSTKFLT",
	[SIGCHLD]       = "SIGCHLD",
	[SIGCONT]       = "SIGCONT",
	[SIGSTOP]       = "SIGSTOP",
	[SIGTSTP]       = "SIGTSTP",
	[SIGTTIN]       = "SIGTTIN",
	[SIGTTOU]       = "SIGTTOU",
	[SIGURG]        = "SIGURG",
	[SIGXCPU]       = "SIGXCPU",
	[SIGXFSZ]       = "SIGXFSZ",
	[SIGVTALRM]     = "SIGVTALRM",
	[SIGPROF]       = "SIGPROF",
	[SIGWINCH]      = "SIGWINCH",
	[SIGIO]         = "SIGIO",
	[SIGPWR]        = "SIGPWR",
	[SIGSYS]        = "SIGSYS",
};

const char *fx_str_signum(int signum)
{
	int nelems;
	const char *s;

	nelems = (int)FX_ARRAYSIZE(s_signum_str);
	if ((signum > 0) && (signum < nelems)) {
		s = s_signum_str[signum];
	} else {
		s = "SIGXXX";
	}
	return s;
}



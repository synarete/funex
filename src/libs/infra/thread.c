/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <syscall.h>
#include <sys/prctl.h>

#include "compiler.h"
#include "macros.h"
#include "utility.h"
#include "panic.h"
#include "timex.h"
#include "thread.h"


/*
 * Controls mutex type: for development ('-g -O0') use non-portable error
 * checking, otherwise use default.
 */
#if (FX_DEBUG > 0) && (FX_OPTIMIZE == 0)
#define FX_MUTEX_KIND PTHREAD_MUTEX_ERRORCHECK_NP
#else
#define FX_MUTEX_KIND PTHREAD_MUTEX_DEFAULT
#endif

/*
 * Panic in cases of failure, where rc is returned value from one of
 * pthread's functions
 */
#define fx_check_pthread_rc(rc) \
	do { if (rc != 0) \
			fx_panic("rc=%d", rc); } while (0)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Mutex */

#define FX_MUTEX_MAGIC (0xAA01BB13)

#define fx_mutex_check(mutex) \
	do { if (fx_unlikely(mutex->magic != mutex_magic(mutex))) \
			fx_panic("mutex=%p magic=%lx",  \
			         (void*)mutex, (long)(mutex->magic)); \
	} while (0)


static unsigned mutex_magic(const fx_mutex_t *mutex)
{
	fx_unused(mutex);
	return FX_MUTEX_MAGIC;
}

void fx_mutex_init(fx_mutex_t *mutex)
{
	int rc, kind = FX_MUTEX_KIND;
	pthread_mutexattr_t attr;

	pthread_mutexattr_init(&attr);
	rc = pthread_mutexattr_settype(&attr, kind);
	fx_check_pthread_rc(rc);

	pthread_mutex_init(&mutex->mutex, &attr);
	pthread_mutexattr_destroy(&attr);

	mutex->magic = mutex_magic(mutex);
}

void fx_mutex_destroy(fx_mutex_t *mutex)
{
	int rc;

	fx_mutex_check(mutex);
	rc = pthread_mutex_destroy(&mutex->mutex);
	fx_check_pthread_rc(rc);

	memset(mutex, 0xEE, sizeof(*mutex));
}

void fx_mutex_lock(fx_mutex_t *mutex)
{
	int rc;

	fx_mutex_check(mutex);
	rc = pthread_mutex_lock(&mutex->mutex);
	fx_check_pthread_rc(rc);
}

int fx_mutex_trylock(fx_mutex_t *mutex)
{
	int rc, status;

	rc = pthread_mutex_trylock(&mutex->mutex);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == EBUSY) {
			errno  = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}
	return status;
}

void fx_mutex_unlock(fx_mutex_t *mutex)
{
	int rc;

	fx_mutex_check(mutex);
	rc = pthread_mutex_unlock(&mutex->mutex);
	fx_check_pthread_rc(rc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Condition variable */

#define FX_COND_MAGIC (0xCC01FF87)

#define fx_cond_check(cond) \
	do { if (fx_unlikely(cond->magic != cond_magic(cond))) \
			fx_panic("cond=%p magic=%lx", \
			         (void*)cond, (long)(cond->magic));\
	} while (0)


static unsigned cond_magic(const fx_cond_t *cond)
{
	fx_unused(cond);
	return FX_COND_MAGIC;
}

void fx_cond_init(fx_cond_t *cond)
{
	int rc;
	pthread_condattr_t attr;

	rc = pthread_condattr_init(&attr);
	fx_check_pthread_rc(rc);

	/* NB: CLOCK_MONOTONIC_RAW still not supported for pthread on F19 */
	rc = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
	fx_check_pthread_rc(rc);

	rc = pthread_cond_init(&cond->cond, &attr);
	fx_check_pthread_rc(rc);

	rc = pthread_condattr_destroy(&attr);
	fx_check_pthread_rc(rc);

	cond->magic = cond_magic(cond);
}

void fx_cond_destroy(fx_cond_t *cond)
{
	int rc;

	rc = pthread_cond_destroy(&cond->cond);
	fx_check_pthread_rc(rc);

	memset(cond, 0xC0, sizeof(*cond));
}

void fx_cond_wait(fx_cond_t *cond, fx_mutex_t *mutex)
{
	int rc;

	fx_cond_check(cond);
	rc = pthread_cond_wait(&cond->cond, &mutex->mutex);
	fx_check_pthread_rc(rc);
}

int fx_cond_timedwait(fx_cond_t *cond, fx_mutex_t *mutex,
                      const fx_timespec_t *abstime)
{
	int rc, status;

	fx_mutex_check(mutex);
	fx_cond_check(cond);
	rc = pthread_cond_timedwait(&cond->cond, &mutex->mutex, abstime);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == ETIMEDOUT) {
			errno = rc;
		} else if (rc == EINTR) {
			errno = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}
	return status;
}

void fx_cond_signal(fx_cond_t *cond)
{
	int rc;

	fx_cond_check(cond);
	rc = pthread_cond_signal(&cond->cond);
	fx_check_pthread_rc(rc);
}

void fx_cond_broadcast(fx_cond_t *cond)
{
	int rc;

	fx_cond_check(cond);
	rc = pthread_cond_broadcast(&cond->cond);
	fx_check_pthread_rc(rc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Lock (Mutex + Condition):
 */
#define FX_LOCK_MAGIC  (0x217AAC)

#define fx_lock_check(lk) \
	do { if (fx_unlikely(lk->magic != FX_LOCK_MAGIC)) \
			fx_panic("lk=%p lk_magic=%lx", \
			         (void*)lk, (long)(lk->magic)); \
	} while (0)

void fx_lock_init(fx_lock_t *lock)
{
	fx_mutex_init(&lock->mutex);
	fx_cond_init(&lock->cond);
	lock->magic = FX_LOCK_MAGIC;
}

void fx_lock_destroy(fx_lock_t *lock)
{
	fx_mutex_destroy(&lock->mutex);
	fx_cond_destroy(&lock->cond);
	lock->magic = 1;
}

void fx_lock_acquire(fx_lock_t *lock)
{
	fx_lock_check(lock);
	fx_mutex_lock(&lock->mutex);
}

void fx_lock_release(fx_lock_t *lock)
{
	fx_lock_check(lock);
	fx_mutex_unlock(&lock->mutex);
}

void fx_lock_wait(fx_lock_t *lock)
{
	fx_lock_check(lock);
	fx_cond_wait(&lock->cond, &lock->mutex);
}

int fx_lock_timedwait(fx_lock_t *lock, const fx_timespec_t *ts)
{
	fx_lock_check(lock);
	return fx_cond_timedwait(&lock->cond, &lock->mutex, ts);
}

void fx_lock_signal(fx_lock_t *lock)
{
	fx_lock_check(lock);
	fx_cond_signal(&lock->cond);
}

void fx_lock_broadcast(fx_lock_t *lock)
{
	fx_lock_check(lock);
	fx_cond_broadcast(&lock->cond);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Read-Write Lock:
 */
void fx_rwlock_init(fx_rwlock_t *rw)
{
	int rc;
	pthread_rwlockattr_t attr;

	rc = pthread_rwlockattr_init(&attr);
	fx_check_pthread_rc(rc);

	rc = pthread_rwlock_init(&rw->rwlock, &attr);
	fx_check_pthread_rc(rc);

	rc = pthread_rwlockattr_destroy(&attr);
	fx_check_pthread_rc(rc);
}

void fx_rwlock_destroy(fx_rwlock_t *rw)
{
	int rc;

	rc = pthread_rwlock_destroy(&rw->rwlock);
	fx_check_pthread_rc(rc);

	memset(rw, 0xFE, sizeof(*rw));
}

void fx_rwlock_rdlock(fx_rwlock_t *rw)
{
	int rc;

	rc = pthread_rwlock_rdlock(&rw->rwlock);
	fx_check_pthread_rc(rc);
}

void fx_rwlock_wrlock(fx_rwlock_t *rw)
{
	int rc;

	rc = pthread_rwlock_wrlock(&rw->rwlock);
	fx_check_pthread_rc(rc);
}

int fx_rwlock_tryrdlock(fx_rwlock_t *rw)
{
	int rc, status;

	rc = pthread_rwlock_tryrdlock(&rw->rwlock);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == EBUSY) {
			errno = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}

	return status;
}

int fx_rwlock_trywrlock(fx_rwlock_t *rw)
{
	int rc, status;

	rc = pthread_rwlock_trywrlock(&rw->rwlock);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == EBUSY) {
			errno  = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}
	return status;
}

int fx_rwlock_timedrdlock(fx_rwlock_t *rw, size_t usec_timeout)
{
	int rc, status;
	fx_timespec_t ts;

	fx_timespec_getmonotime(&ts);
	fx_timespec_usecadd(&ts, (long)usec_timeout);

	rc = pthread_rwlock_timedrdlock(&rw->rwlock, &ts);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == ETIMEDOUT) {
			errno  = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}
	return status;
}

int fx_rwlock_timedwrlock(fx_rwlock_t *rw, size_t usec_timeout)
{
	int rc, status = 0;
	fx_timespec_t ts;

	fx_timespec_getmonotime(&ts);
	fx_timespec_usecadd(&ts, (long)usec_timeout);

	rc = pthread_rwlock_timedwrlock(&rw->rwlock, &ts);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == ETIMEDOUT) {
			errno = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}
	return status;
}

void fx_rwlock_unlock(fx_rwlock_t *rw)
{
	int rc;

	rc = pthread_rwlock_unlock(&rw->rwlock);
	fx_check_pthread_rc(rc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * SpinLock:
 */
void fx_spinlock_init(fx_spinlock_t *s)
{
	int rc, pshared = 0;

	rc = pthread_spin_init(&s->spinlock, pshared);
	fx_check_pthread_rc(rc);
}

void fx_spinlock_destroy(fx_spinlock_t *s)
{
	int rc;

	rc = pthread_spin_destroy(&s->spinlock);
	fx_check_pthread_rc(rc);

	memset(s, 0x77, sizeof(*s));
}

void fx_spinlock_lock(fx_spinlock_t *s)
{
	int rc;

	rc = pthread_spin_lock(&s->spinlock);
	fx_check_pthread_rc(rc);
}

int fx_spinlock_trylock(fx_spinlock_t *s)
{
	int rc, status = 0;

	rc = pthread_spin_trylock(&s->spinlock);
	if (rc == 0) {
		status = 0;
	} else {
		status = -1;
		if (rc == EBUSY) {
			errno = rc;
		} else {
			fx_check_pthread_rc(rc);
		}
	}
	return status;
}

void fx_spinlock_unlock(fx_spinlock_t *s)
{
	int rc;

	rc = pthread_spin_unlock(&s->spinlock);
	fx_check_pthread_rc(rc);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Threads:
 */
void fx_thread_init(fx_thread_t *th)
{
	fx_bzero(th, sizeof(*th));

	th->th_exec = NULL;
	th->th_arg  = NULL;
	th->th_pid  = (pid_t)(-1);
	th->th_blocksig = 1;
}

void fx_thread_destroy(fx_thread_t *th)
{
	fx_bzero(th, sizeof(*th));
	th->th_pid = (pid_t)(-1);
}

/* N.B. Must be calles before start */
void fx_thread_setname(fx_thread_t *th, const char *name)
{
	size_t sz;

	sz = sizeof(th->th_name);
	fx_bzero(th->th_name, sz);
	if (name != NULL) {
		strncpy(th->th_name, name, sz - 1);
	}
}


/* Wrapper for gettid (Linux specific) */
static pid_t fx_gettid(void)
{
#if defined(SYS_gettid)
	return (pid_t)syscall(SYS_gettid);
#else
	return getpid();
#endif
}

static void thread_settid(fx_thread_t *th)
{
	th->th_pid = fx_gettid();
}

/* Export thread's name to the system (Linux specific, for 'top') */
static void thread_settitle(const fx_thread_t *th)
{
	int rc;

	if (strlen(th->th_name)) {
		rc = pthread_setname_np(th->th_id, th->th_name);
		fx_check_pthread_rc(rc);
	}
	/* fx_setproctitle(th->th_name); */
}

/* Disable some signals to thread */
static void thread_block_signals(const fx_thread_t *th)
{
	sigset_t sigset_th;

	if (th->th_blocksig) {
		sigemptyset(&sigset_th);
		sigaddset(&sigset_th, SIGHUP);
		sigaddset(&sigset_th, SIGINT);
		sigaddset(&sigset_th, SIGQUIT);
		sigaddset(&sigset_th, SIGTERM);
		sigaddset(&sigset_th, SIGTRAP);
		sigaddset(&sigset_th, SIGUSR1);
		sigaddset(&sigset_th, SIGUSR2);
		sigaddset(&sigset_th, SIGPIPE);
		sigaddset(&sigset_th, SIGALRM);
		sigaddset(&sigset_th, SIGCHLD);
		sigaddset(&sigset_th, SIGCONT);
		sigaddset(&sigset_th, SIGWINCH);
		sigaddset(&sigset_th, SIGIO);

		pthread_sigmask(SIG_BLOCK, &sigset_th, NULL);
	}
}

/* Disable/Enable current thread cancellation */
static void thread_disable_cancel(const fx_thread_t *th)
{
	int rc;
	long long thread_pid;

	rc = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	if (rc != 0) {
		thread_pid = (long long)th->th_pid;
		fx_panic("rc=%d thread_pid=%#llx", rc, thread_pid);
	}
}

static void thread_enable_cancel(const fx_thread_t *th)
{
	int rc;
	long long thread_pid;

	rc = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	if (rc != 0) {
		thread_pid = (long long)th->th_pid;
		fx_panic("rc=%d thread_pid=%#llx", rc, thread_pid);
	}
}


/*
 * Entry point for Funex threads. Disable cancellation while executing user's
 * call. The thread_name and thread_pid args are used just for debugger
 * back-tarcing readability.
 */
static void fx_thread_exec(const fx_thread_t *th,
                           const char *thread_name, pid_t thread_pid)
{
	void *arg;

	arg = th->th_arg;

	thread_block_signals(th);

	thread_disable_cancel(th);
	th->th_exec(arg);   /* User's execution */
	thread_enable_cancel(th);

	(void)thread_name;
	(void)thread_pid;
}

static void *fx_start_thread(void *p)
{
	fx_thread_t *th;

	th = (fx_thread_t *)p;

	/* First, do some thread-specific settings */
	thread_settitle(th);
	thread_settid(th);

	/* Execute user's call. */
	errno = 0;
	fx_thread_exec(th, th->th_name, th->th_pid);

	/* Done */
	pthread_exit(NULL);
	return NULL;
}

/*
 * Creates new thread (start execution). Will not return until sub-thread has
 * start running.
 */
void fx_thread_start(fx_thread_t *th, fx_thread_fn fn, void *arg)
{
	int rc;
	pthread_attr_t attr;
	const volatile pid_t *p_pid;

	th->th_exec = fn;
	th->th_arg  = arg;
	th->th_pid  = (pid_t)(-1);

	p_pid = &th->th_pid;

	errno = 0;
	rc = pthread_attr_init(&attr);
	fx_check_pthread_rc(rc);

	rc = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	fx_check_pthread_rc(rc);

	rc = pthread_create(&th->th_id, &attr, fx_start_thread, th);
	fx_check_pthread_rc(rc);

	while (*p_pid == (pid_t)(-1)) {
		fx_msleep(1);
	}
	pthread_attr_destroy(&attr);
}

/*
 * Threads-join wrapper
 */
int fx_join_thread(const fx_thread_t *other, int flags)
{
	int rc;
	void *retval = NULL;
	const char *name;

	errno  = 0;
	name = other->th_name;
	rc = pthread_join(other->th_id, &retval);
	if (rc != 0) {
		if ((rc == ESRCH) && !(flags & FX_NOFAIL)) {
			return -1;
		}

		switch (rc) {
			case ESRCH:
			case EDEADLK:
			case EINVAL:
			default:
				fx_panic("no-pthread-join name=%s th_id=%#lx rc=%d",
				         name, (unsigned long)other->th_id, rc);
				return -1;
		}
	}
	return 0;
}

void fx_setproctitle(const char *name)
{
	if (name && strlen(name)) {
		prctl(PR_SET_NAME, name, 0, 0, 0);
	}
}


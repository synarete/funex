/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_SIGNALS_H_
#define FUNEX_SIGNALS_H_

#include <unistd.h>
#include <signal.h>

/*
 * User's call-back in case of cleanup upon termination signal. To be executed
 * from the context of signal handler.
 *
 * Return non-zero value in order to re-raise the signal.
 */
typedef int (*fx_sigaction_cb)(int, const siginfo_t *, void *);


/*
 * Divides signals into 4 groups: ignore, term, abort and no-catch:
 *
 * ignore:   catch and do nothing
 * term:     terminate process (cleanup user's hook)
 * abort:    abort
 * no-catch: nothing
 *
 * In case of term signals, user's call-back may be called (if set) for proper
 * cleanup.
 */
void fx_sigaction_ignore(int signum);

void fx_sigaction_halt(int signum);

void fx_sigaction_abort(int signum);

void fx_sigaction_nocatch(int signum);

/*
 * Sets default signal handlers:
 */
void fx_set_default_sigactions(void);

/*
 * Sets user-provided call-back hook for handling cases of process termination.
 */
void fx_set_sighalt_callback(fx_sigaction_cb cb);

/*
 * Converts signal-number to human readable string.
 */
const char *fx_str_signum(int signum);

#endif /* FUNEX_SIGNALS_H_ */


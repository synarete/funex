/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_MACROS_H_
#define FUNEX_MACROS_H_

#ifdef NDEBUG
#define FX_DEBUG   0
#else
#if (defined(DEBUG) && (DEBUG > 0))
#define FX_DEBUG   DEBUG
#elif (defined(_DEBUG) && (_DEBUG > 0))
#define FX_DEBUG   _DEBUG
#else
#define FX_DEBUG   0
#endif
#endif

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Converts the parameter x_ to a string after macro replacement on it has
 * been performed.
 */
#define FX_MAKESTR(x_)                  FX_MAKESTR2_(x_)
#define FX_MAKESTR2_(x_)                #x_


/*
 * The following piece of macro magic joins the two arguments together, even
 * when one of the arguments is itself a macro. The key is that macro
 * expansion of macro arguments does not occur in FX_JOIN2B_ but does in
 * FX_JOIN2_.
 */
#define FX_JOIN(x_,y_)                  FX_JOIN2_(x_,y_)
#define FX_JOIN2_(x_,y_)                FX_JOIN2B_(x_,y_)
#define FX_JOIN2B_(x_,y_)               x_##y_


/*
 * Evaluates to the number of elements in static array.
 *
 * NB: The size of the array must be known at compile time; *do not* try to
 * use this macro for dynamic-allocated arrays!
 */
#define FX_NELEMS(X)                    ( sizeof(X) / sizeof(X[0]) )
#define FX_ARRAYSIZE(X)                 FX_NELEMS(X)

#define fx_nelems(x)                    FX_NELEMS(x)

/*
 * Sizeof field within struct
 */
#define FX_FIELD_SIZEOF(T, F)           ( sizeof(( (T*)NULL)->F) )
#define FX_ARRFIELD_NELEMS(T, F)        FX_ARRAYSIZE( ((T*)(NULL))->F )


/* Common MIN/MAX */
#define FX_MIN(x,y)                     ( ( (x) < (y) ) ? (x) : (y) )
#define FX_MAX(x,y)                     ( ( (x) > (y) ) ? (x) : (y) )

/* Bit-masking helpers */
#define FX_BITFLAG(n)                   (1 << n)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Compile-time assertion.
 *
 * The macros FX_STATICASSERT(x) and fx_staticassert(x), generates a compile
 * time error message if the integral-constant-expression 'expr' is not true.
 * In other words, it is the compile time equivalent of the assert macro.
 * Note that if the condition is true, then the macro will generate neither
 * code nor data, and it may be used within function scope.
 *
 * See:
 * GNULIB-manual, 12.4 Compile-time Assertions (verify)
 * http://www.gnu.org/software/gnulib/manual/gnulib.html
 *
 * Linux Kernel uses BUILD_BUG_ON macro.
 *
 * /http://www.jaggersoft.com/pubs/CVu11_3.html
 *
 * Boost's C++ version:
 *  http://www.boost.org/doc/libs/1_46_1/doc/html/boost_staticassert.html
 */
#define FX_STATICASSERT_1(expr)             \
	do { switch (0) { case 0: case expr: default: break; } } while (0)

#define FX_STATICASSERT_LABEL_(tag)         \
	FX_JOIN(fx_static_assert_, FX_JOIN(tag, __LINE__))

#define FX_STATICASSERT_(tag, expr)         \
	typedef char FX_STATICASSERT_LABEL_(tag) [ 1 - 2*(!(expr)) ]

#define FX_STATICASSERT(expr)               \
	FX_STATICASSERT_(_, expr)

#define FX_STATICASSERT_EQ(a, b)            \
	FX_STATICASSERT_(not_equal_, ((a) == (b)))

#define FX_STATICASSERT_EQSIZEOF(a, b)      \
	FX_STATICASSERT_(not_equal_sizeof_, (sizeof(a) == sizeof(b)))

#define FX_STATICASSERT_EQPSIZE(a, b)       \
	FX_STATICASSERT_(not_equal_pointers_, (sizeof(*a) == sizeof(*b)))


#define fx_staticassert(expr)      FX_STATICASSERT(expr)
#define fx_staticassert_eq(a, b)   FX_STATICASSERT_EQ(a, b)


/* Compiler-specific support for static-assert */
#if defined(FX_GCC)
#if (FX_GCC_VERSION >= 40700)
#undef FX_STATICASSERT_
#define FX_STATICASSERT_(tag, expr)         \
	_Static_assert(expr, #expr)
#endif
#elif defined(FX_CLANG)
#if __has_feature(c_static_assert)
#undef FX_STATICASSERT_
#define FX_STATICASSERT_(tag, expr)         \
	_Static_assert(expr, #expr)
#endif
#endif


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Verify two pointers are of equal size
 */
#define FX_ARGI(i)   FX_JOIN(FX_JOIN(_fx_arg_, __LINE__), i)
#define FX_EQPTRSIZE(x, y) \
	do { __extension__ __typeof__ ((x)) FX_ARGI(1) = (x); \
		__extension__ __typeof__ ((y)) FX_ARGI(2) = (y); \
		FX_STATICASSERT_EQPSIZE(FX_ARGI(1), FX_ARGI(2)); \
	} while(0)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Cast a member of a structure out to the containing structure:
 * NB: Linux kernel use GCC extension to check pointer-types equality.
 */
#define FX_CONTAINER_OF(ptr, type, member) \
	(type*)((void*)((char*)ptr - offsetof(type, member)))

#define fx_container_of(ptr, type, member) \
	FX_CONTAINER_OF(ptr, type, member)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Suppress compiler warns about unused parameter
 */
#define FX_UNUSED(x)       ((void)(x))
#define fx_unused(x)       FX_UNUSED(x)
#define fx_unused2(x, y)   do { fx_unused(x); fx_unused(y); } while (0)


/*
 * Array-elements iteration: apply function 'fn' on each member of the array.
 */
#define fx_foreach_arrelem(arr, fn) \
	do { for (size_t _i = 0; _i < FX_NELEMS(arr); ++_i) \
			fn(&(arr)[_i]); } while (0)


#endif /* FUNEX_MACROS_H_ */


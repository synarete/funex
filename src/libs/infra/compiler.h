/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_COMPILER_H_
#define FUNEX_COMPILER_H_


/* Just find out if using GCC */
#if defined(__GNUC__) || defined(__clang__)
#define FX_GCC
#define FX_CLANG
#elif defined(__INTEL_COMPILER)
#define FX_ICC
#error "ICC -- Untested"
#else
#error "Unknown compiler"
#endif


#if defined(FX_GCC)
#define FX_GCC_VERSION \
	((__GNUC__ * 10000) + (__GNUC_MINOR__ * 100) + __GNUC_PATCHLEVEL__)
#endif

#if defined(FX_GCC) || defined(FX_CLANG)

#define FX_FUNCTION                 __func__ /* __FUNCTION__ */
#define FX_WORDSIZE                 __WORDSIZE

#if defined(__OPTIMIZE__)
#define FX_OPTIMIZE                 __OPTIMIZE__
#else
#define FX_OPTIMIZE                 0
#endif

#define FX_ATTR_ALIGNED             __attribute__ ((__aligned__))
#define FX_ATTR_PACKED              __attribute__ ((__packed__))
#define FX_ATTR_PURE                __attribute__ ((__pure__))
#define FX_ATTR_NONNULL             __attribute__ ((__nonnull__))
#define FX_ATTR_NONNULL1            __attribute__ ((__nonnull__ (1)))
#define FX_ATTR_NORETURN            __attribute__ ((__noreturn__))
#define FX_ATTR_FMTPRINTF(i, j)     __attribute__ ((format (printf, i, j)))


#define fx_likely(x)                __builtin_expect(!!(x), 1)
#define fx_unlikely(x)              __builtin_expect(!!(x), 0)

#define fx_packed                   FX_ATTR_PACKED

#endif

#endif /* FUNEX_COMPILER_H_ */





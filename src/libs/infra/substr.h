/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_SUBSTR_H_
#define FUNEX_SUBSTR_H_


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Sub-string: reference to characters-array. When nwr is zero, referring to
 * immutable (read-only) string. In all cases, never overlap writable region.
 * All possible dynamic-allocation must be made explicitly by the user.
 */
struct fx_substr {
	char  *str; /* Beginning of characters-array (rd & wr)    */
	size_t len; /* Number of readable chars (string's length) */
	size_t nwr; /* Number of writable chars from beginning    */
};
typedef struct fx_substr    fx_substr_t;


struct fx_substr_pair {
	fx_substr_t first, second;
};
typedef struct fx_substr_pair  fx_substr_pair_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Returns the largest possible size a sub-string may have.
 */
size_t fx_substr_max_size(void);

/*
 * "Not-a-pos" (synonym to fx_substr_max_size())
 */
size_t fx_substr_npos(void);


/*
 * Constructors:
 * The first two create read-only substrings, the next two creates a mutable
 * (for write) substring. The last one creates read-only empty string.
 */
void fx_substr_init(fx_substr_t *ss, const char *str);
void fx_substr_init_rd(fx_substr_t *ss, const char *s, size_t n);
void fx_substr_init_rwa(fx_substr_t *ss, char *);
void fx_substr_init_rw(fx_substr_t *ss, char *, size_t nrd, size_t nwr);
void fx_substr_inits(fx_substr_t *ss);

/*
 * Shallow-copy constructor (without deep copy).
 */
void fx_substr_clone(fx_substr_t *ss, const fx_substr_t *other);

/*
 * Destructor: zero all
 */
void fx_substr_destroy(fx_substr_t *ss);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Returns the string's read-length. Synonym to ss->len.
 */
size_t fx_substr_size(const fx_substr_t *ss);

/*
 * Returns the writable-size of sub-string.
 */
size_t fx_substr_wrsize(const fx_substr_t *ss);

/*
 * Returns TRUE if sub-string's length is zero.
 */
int fx_substr_isempty(const fx_substr_t *ss);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns an iterator pointing to the beginning of the characters sequence.
 */
const char *fx_substr_begin(const fx_substr_t *ss);

/*
 * Returns an iterator pointing to the end of the characters sequence.
 */
const char *fx_substr_end(const fx_substr_t *ss);

/*
 * Returns the number of elements between begin() and p. If p is out-of-range,
 * returns npos.
 */
size_t fx_substr_offset(const fx_substr_t *ss, const char *p);

/*
 * Returns pointer to the n'th character. Performs out-of-range check:
 * panics in case n is out of range.
 */
const char *fx_substr_at(const fx_substr_t *substr, size_t n);

/*
 * Returns TRUE if ss->ss_str[i] is a valid substring-index (i < s->ss_len).
 */
int fx_substr_isvalid_index(const fx_substr_t *ss, size_t i);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 *  Copy data into buffer, return number of characters assigned. Assign
 *  no more than min(n, size()) bytes.
 *
 *  Returns the number of characters copied to buf, not including possible null
 *  termination character.
 *
 *  NB: Result buf will be null-terminated only if there is enough
 *          room (i.e. n > size()).
 */
size_t fx_substr_copyto(const fx_substr_t *ss, char *buf, size_t n);

/*
 * Three-way lexicographical comparison
 */
int fx_substr_compare(const fx_substr_t *ss, const char *s);
int fx_substr_ncompare(const fx_substr_t *ss, const char *s, size_t n);

/*
 * Returns TRUE in case of equal size and equal data
 */
int fx_substr_isequal(const fx_substr_t *ss, const char *s);
int fx_substr_nisequal(const fx_substr_t *ss, const char *s, size_t n);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns the number of (non-overlapping) occurrences of s (or c) as a
 * substring of ss.
 */
size_t fx_substr_count(const fx_substr_t *ss, const char *s);
size_t fx_substr_ncount(const fx_substr_t *ss,
                        const char *s, size_t n);
size_t fx_substr_count_chr(const fx_substr_t *ss, char c);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Searches ss, beginning at position pos, for the first occurrence of s (or
 * single-character c). If search fails, returns npos.
 */
size_t fx_substr_find(const fx_substr_t *ss, const char *str);
size_t fx_substr_nfind(const fx_substr_t *ss,
                       size_t pos, const char *s, size_t n);
size_t fx_substr_find_chr(const fx_substr_t *ss, size_t pos, char c);


/*
 * Searches ss backwards, beginning at position pos, for the last occurrence of
 * s (or single-character c). If search fails, returns npos.
 */
size_t fx_substr_rfind(const fx_substr_t *ss, const char *s);
size_t fx_substr_nrfind(const fx_substr_t *ss,
                        size_t pos, const char *s, size_t n);
size_t fx_substr_rfind_chr(const fx_substr_t *ss, size_t pos, char c);


/*
 * Searches ss, beginning at position pos, for the first character that is
 * equal to any one of the characters of s.
 */
size_t fx_substr_find_first_of(const fx_substr_t *ss, const char *s);
size_t fx_substr_nfind_first_of(const fx_substr_t *ss,
                                size_t pos, const char *s, size_t n);


/*
 * Searches ss backwards, beginning at position pos, for the last character
 * that is equal to any of the characters of s.
 */
size_t fx_substr_find_last_of(const fx_substr_t *ss, const char *s);
size_t fx_substr_nfind_last_of(const fx_substr_t *ss,
                               size_t pos, const char *s, size_t n);


/*
 * Searches ss, beginning at position pos, for the first character that is not
 * equal to any of the characters of s.
 */
size_t fx_substr_find_first_not_of(const fx_substr_t *ss,
                                   const char *s);
size_t fx_substr_nfind_first_not_of(const fx_substr_t *ss,
                                    size_t pos, const char *s, size_t n);
size_t fx_substr_find_first_not(const fx_substr_t *ss,
                                size_t pos, char c);



/*
 * Searches ss backwards, beginning at position pos, for the last character
 * that is not equal to any of the characters of s (or c).
 */
size_t fx_substr_find_last_not_of(const fx_substr_t *ss,
                                  const char *s);
size_t fx_substr_nfind_last_not_of(const fx_substr_t *ss,
                                   size_t pos, const char *s, size_t n);
size_t fx_substr_find_last_not(const fx_substr_t *ss,
                               size_t pos, char c);



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Creates a substring of ss, which refers to n characters after position i.
 * If i is an invalid index, the result substring is empty. If there are less
 * then n characters after position i, the result substring will refer only to
 * the elements which are members of ss.
 */
void fx_substr_sub(const fx_substr_t *ss,
                   size_t i, size_t n,  fx_substr_t *result);

/*
 * Creates a substring of ss, which refers to the last n chars. The result
 * substring will not refer to more then ss->ss_len elements.
 */
void fx_substr_rsub(const fx_substr_t *ss,
                    size_t n, fx_substr_t *result);

/*
 * Creates a substring with all the characters that are in the range of
 * both s1 and s2. That is, all elements within the range
 * [s1.begin(),s1.end()) which are also in the range
 * [s2.begin(), s2.end()) (i.e. have the same address).
 */
void fx_substr_intersection(const fx_substr_t *s1,
                            const fx_substr_t *s2,
                            fx_substr_t *result);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Creates a pair of substrings of ss, which are divided by any of the first
 * characters of seps. If none of the characters of seps is in ss, the first
 * first element of the result-pair is equal to ss and the second element is an
 * empty substring.
 *
 *  Examples:
 *  split("root@foo//bar", "/@:")  --> "root", "foo//bar"
 *  split("foo///:bar::zoo", ":/") --> "foo", "bar:zoo"
 *  split("root@foo.bar", ":/")    --> "root@foo.bar", ""
 */
void fx_substr_split(const fx_substr_t *ss,
                     const char *seps, fx_substr_pair_t *);

void fx_substr_nsplit(const fx_substr_t *ss,
                      const char *seps, size_t n, fx_substr_pair_t *);

void fx_substr_split_chr(const fx_substr_t *ss, char sep,
                         fx_substr_pair_t *result);



/*
 * Creates a pair of substrings of ss, which are divided by any of the first n
 * characters of seps, while searching ss backwards. If none of the characters
 * of seps is in ss, the first element of the pair equal to ss and the second
 * element is an empty substring.
 */
void fx_substr_rsplit(const fx_substr_t *ss,
                      const char *seps, fx_substr_pair_t *result);

void fx_substr_nrsplit(const fx_substr_t *ss,
                       const char *seps, size_t, fx_substr_pair_t *);

void fx_substr_rsplit_chr(const fx_substr_t *ss, char sep,
                          fx_substr_pair_t *result);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Creates a substring of ss without the first n leading characters.
 */
void fx_substr_trim(const fx_substr_t *ss, size_t n, fx_substr_t *);


/*
 * Creates a substring of ss without any leading characters which are members
 * of set.
 */
void fx_substr_trim_any_of(const fx_substr_t *ss, const char *set,
                           fx_substr_t *result);

void fx_substr_ntrim_any_of(const fx_substr_t *ss,
                            const char *set, size_t n, fx_substr_t *);

void fx_substr_trim_chr(const fx_substr_t *substr, char c,
                        fx_substr_t *result);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Creates a substring of ss without the last n trailing characters.
 * If n >= ss->ss_len the result substring is empty.
 */
void fx_substr_chop(const fx_substr_t *ss, size_t n, fx_substr_t *);

/*
 * Creates a substring of ss without any trailing characters which are members
 * of set.
 */
void fx_substr_chop_any_of(const fx_substr_t *ss,
                           const char *set, fx_substr_t *result);

void fx_substr_nchop_any_of(const fx_substr_t *ss,
                            const char *set, size_t n, fx_substr_t *);

/*
 * Creates a substring of ss without any trailing characters that equals c.
 */
void fx_substr_chop_chr(const fx_substr_t *ss, char c, fx_substr_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Creates a substring of ss without any leading and trailing characters which
 * are members of set.
 */
void fx_substr_strip_any_of(const fx_substr_t *ss,
                            const char *set, fx_substr_t *result);

void fx_substr_nstrip_any_of(const fx_substr_t *ss,
                             const char *set, size_t n,
                             fx_substr_t *result);

/*
 * Creates a substring of substr without any leading and trailing
 * characters which are equal to c.
 */
void fx_substr_strip_chr(const fx_substr_t *ss, char c,
                         fx_substr_t *result);


/*
 * Strip white-spaces
 */
void fx_substr_strip_ws(const fx_substr_t *ss, fx_substr_t *result);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Finds the first substring of ss that is a token delimited by any of the
 * characters of sep(s).
 */
void fx_substr_find_token(const fx_substr_t *ss,
                          const char *seps, fx_substr_t *result);

void fx_substr_nfind_token(const fx_substr_t *ss,
                           const char *seps, size_t n,
                           fx_substr_t *result);

void fx_substr_find_token_chr(const fx_substr_t *ss, char sep,
                              fx_substr_t *result);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Finds the next token in ss after tok, which is delimeted by any of the
 * characters of sep(s).
 */

void fx_substr_find_next_token(const fx_substr_t *ss,
                               const fx_substr_t *tok,
                               const char *seps,
                               fx_substr_t *result);

void fx_substr_nfind_next_token(const fx_substr_t *ss,
                                const fx_substr_t *tok,
                                const char *seps, size_t n,
                                fx_substr_t *result);

void fx_substr_find_next_token_chr(const fx_substr_t *substr,
                                   const fx_substr_t *tok, char sep,
                                   fx_substr_t *result);

/*
 * Parses the substring ss into tokens, delimited by separators seps and inserts
 * them into tok_list. Inserts no more then max_sz tokens.
 *
 * Returns 0 if all tokens assigned to tok_list, or -1 in case of insufficient
 * space. If p_ntok is not NULL it is set to the number of parsed tokens.
 */
int fx_substr_tokenize(const fx_substr_t *ss, const char *seps,
                       fx_substr_t tok_list[], size_t list_size,
                       size_t *p_ntok);

int fx_substr_ntokenize(const fx_substr_t *ss,
                        const char *seps, size_t n,
                        fx_substr_t tok_list[], size_t list_size,
                        size_t *p_ntok);

int fx_substr_tokenize_chr(const fx_substr_t *ss, char sep,
                           fx_substr_t tok_list[], size_t list_size,
                           size_t *p_ntok);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Scans ss for the longest common prefix with s.
 */
size_t fx_substr_common_prefix(const fx_substr_t *ss, const char *str);
size_t fx_substr_ncommon_prefix(const fx_substr_t *ss,
                                const char *s, size_t n);

/*
 * Return TRUE if the first character of ss equals c.
 */
int fx_substr_starts_with(const fx_substr_t *ss, char c);


/*
 * Scans ss backwards for the longest common suffix with s.
 */
size_t fx_substr_common_suffix(const fx_substr_t *ss,
                               const char *s);
size_t fx_substr_ncommon_suffix(const fx_substr_t *ss,
                                const char *s, size_t n);

/*
 * Return TRUE if the last character of ss equals c.
 */
int fx_substr_ends_with(const fx_substr_t *ss, char c);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 *  Returns pointer to beginning of characters sequence.
 */
char *fx_substr_data(const fx_substr_t *ss);

/*
 * Assigns s, truncates result in case of insufficient room.
 */
void fx_substr_assign(fx_substr_t *ss, const char *s);
void fx_substr_nassign(fx_substr_t *ss, const char *s, size_t len);

/*
 * Assigns n copies of c.
 */
void fx_substr_assign_chr(fx_substr_t *ss, size_t n, char c);


/*
 * Appends s.
 */
void fx_substr_append(fx_substr_t *ss, const char *s);
void fx_substr_nappend(fx_substr_t *ss, const char *s, size_t len);

/*
 * Appends n copies of c.
 */
void fx_substr_append_chr(fx_substr_t *ss, size_t n, char c);

/*
 * Appends single char.
 */
void fx_substr_push_back(fx_substr_t *ss, char c);

/*
 * Inserts s before position pos.
 */
void fx_substr_insert(fx_substr_t *ss, size_t pos, const char *s);
void fx_substr_ninsert(fx_substr_t *ss,
                       size_t pos, const char *s, size_t len);

/*
 * Inserts n copies of c before position pos.
 */
void fx_substr_insert_chr(fx_substr_t *ss,
                          size_t pos, size_t n, char c);

/*
 * Replaces a part of sub-string with the string s.
 */
void fx_substr_replace(fx_substr_t *ss,
                       size_t pos, size_t n, const char *s);
void fx_substr_nreplace(fx_substr_t *ss,
                        size_t pos, size_t n, const char *s, size_t len);

/*
 * Replaces part of sub-string with n2 copies of c.
 */
void fx_substr_replace_chr(fx_substr_t *ss,
                           size_t pos, size_t n1, size_t n2, char c);


/*
 * Erases part of sub-string.
 */
void fx_substr_erase(fx_substr_t *ss, size_t pos, size_t n);

/*
 * Reverse the writable portion of sub-string.
 */
void fx_substr_reverse(fx_substr_t *ss);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Generic:
 */

/*
 * Returns the index of the first element of ss that satisfy the unary
 * predicate fn (or !fn), or npos if no such element exist.
 */
size_t fx_substr_find_if(const fx_substr_t *ss, fx_chr_pred fn);
size_t fx_substr_find_if_not(const fx_substr_t *ss, fx_chr_pred fn);


/*
 * Returns the index of the last element of ss that satisfy the unary
 * predicate fn (or !fn), or npos if no such element exist.
 */
size_t fx_substr_rfind_if(const fx_substr_t *ss, fx_chr_pred fn);
size_t fx_substr_rfind_if_not(const fx_substr_t *ss, fx_chr_pred fn);

/*
 * Returns the number of elements in ss that satisfy the unary predicate fn.
 */
size_t fx_substr_count_if(const fx_substr_t *ss, fx_chr_pred fn);

/*
 * Returns TRUE if all characters of ss satisfy unary predicate fn.
 */
int fx_substr_test_if(const fx_substr_t *ss, fx_chr_pred fn);


/*
 * Creates a substring of ss without leading characters that satisfy unary
 * predicate fn.
 */
void fx_substr_trim_if(const fx_substr_t *ss,
                       fx_chr_pred fn, fx_substr_t *result);

/*
 * Creates a substring of ss without trailing characters that satisfy unary
 * predicate fn.
 */
void fx_substr_chop_if(const fx_substr_t *ss,
                       fx_chr_pred fn, fx_substr_t *result);

/*
 * Creates a substring of ss without any leading and trailing characters that
 * satisfy unary predicate fn.
 */
void fx_substr_strip_if(const fx_substr_t *ss,
                        fx_chr_pred fn, fx_substr_t *result);


/*
 * Apply fn for every element in sub-string.
 */
void fx_substr_foreach(fx_substr_t *ss, fx_chr_modify_fn fn);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns TRUE if all characters of ss satisfy ctype predicate.
 */
int fx_substr_isalnum(const fx_substr_t *ss);
int fx_substr_isalpha(const fx_substr_t *ss);
int fx_substr_isascii(const fx_substr_t *ss);
int fx_substr_isblank(const fx_substr_t *ss);
int fx_substr_iscntrl(const fx_substr_t *ss);
int fx_substr_isdigit(const fx_substr_t *ss);
int fx_substr_isgraph(const fx_substr_t *ss);
int fx_substr_islower(const fx_substr_t *ss);
int fx_substr_isprint(const fx_substr_t *ss);
int fx_substr_ispunct(const fx_substr_t *ss);
int fx_substr_isspace(const fx_substr_t *ss);
int fx_substr_isupper(const fx_substr_t *ss);
int fx_substr_isxdigit(const fx_substr_t *ss);

/*
 * Case sensitive operations:
 */
void fx_substr_toupper(fx_substr_t *substr);
void fx_substr_tolower(fx_substr_t *substr);
void fx_substr_capitalize(fx_substr_t *substr);

#endif /* FUNEX_SUBSTR_H_ */



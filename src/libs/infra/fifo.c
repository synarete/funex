/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>

#include "compiler.h"
#include "panic.h"
#include "timex.h"
#include "utility.h"
#include "atomic.h"
#include "thread.h"
#include "list.h"
#include "listi.h"
#include "fifo.h"

static void fifo_acquire_lock(fx_fifo_t *fifo)
{
	fx_lock_acquire(&fifo->ff_lock);
}

static void fifo_release_lock(fx_fifo_t *fifo)
{
	fx_lock_release(&fifo->ff_lock);
}

static void fifo_signal_lock(fx_fifo_t *fifo)
{
	fx_lock_signal(&fifo->ff_lock);
}

static int fifo_timedwait_lock(fx_fifo_t *fifo, const fx_timespec_t *ts)
{
	return fx_lock_timedwait(&fifo->ff_lock, ts);
}

static fx_link_t *fifo_popq(fx_fifo_t *fifo)
{
	fx_link_t *lnk;

	lnk = fx_list_pop_front(&fifo->ff_list[0]);
	if (lnk == NULL) {
		lnk = fx_list_pop_front(&fifo->ff_list[1]);
	}

	if (lnk != NULL) {
		lnk->next = lnk->prev = NULL;
	}
	return lnk;
}

static void fifo_pushq(fx_fifo_t *fifo, fx_link_t *lnk, int q)
{
	fx_list_t *list;

	list = q ? &fifo->ff_list[1] : &fifo->ff_list[0];
	fx_list_push_back(list, lnk);
}

static size_t fifo_qsize(const fx_fifo_t *fifo)
{
	size_t sz0, sz1;

	sz0 = list_size(&fifo->ff_list[0]);
	sz1 = list_size(&fifo->ff_list[1]);

	return (sz0 + sz1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fifo_init(fx_fifo_t *fifo)
{
	fx_lock_init(&fifo->ff_lock);
	fx_list_init(&fifo->ff_list[0]);
	fx_list_init(&fifo->ff_list[1]);
	fifo->ff_npend = 0;
}

void fx_fifo_destroy(fx_fifo_t *fifo)
{
	fx_lock_destroy(&fifo->ff_lock);
	fx_list_destroy(&fifo->ff_list[0]);
	fx_list_destroy(&fifo->ff_list[1]);
}

size_t fx_fifo_size(const fx_fifo_t *fifo)
{
	return fifo_qsize(fifo);
}

int fx_fifo_isempty(const fx_fifo_t *fifo)
{
	return (fifo_qsize(fifo) == 0);
}
void fx_fifo_push(fx_fifo_t *fifo, fx_link_t *lnk)
{
	fx_fifo_pushq(fifo, lnk, 0);
}

void fx_fifo_pushq(fx_fifo_t *fifo, fx_link_t *lnk, int q)
{
	fifo_acquire_lock(fifo);
	fifo_pushq(fifo, lnk, q);
	if (fifo->ff_npend > 0) {
		fifo_signal_lock(fifo);
	}
	fifo_release_lock(fifo);
}

fx_link_t *fx_fifo_trypop(fx_fifo_t *fifo)
{
	fx_link_t *lnk;

	fifo_acquire_lock(fifo);
	lnk = fifo_popq(fifo);  /* NULL if empty */
	fifo_release_lock(fifo);

	return lnk;
}

fx_link_t *fx_fifo_pop(fx_fifo_t *fifo, size_t usec_timeout)
{
	return fx_fifo_popn(fifo, 1, usec_timeout);
}

fx_link_t *fx_fifo_popn(fx_fifo_t *fifo, size_t n, size_t usec_timeout)
{
	int rc, err;
	fx_timespec_t ts;
	fx_link_t lst;
	fx_link_t *lnk;
	fx_link_t **ppl;

	lst.next = lst.prev = NULL;
	ppl = &lst.next;

	if (fx_unlikely(n == 0)) {
		return NULL;
	}

	fifo_acquire_lock(fifo);
	fx_timespec_getmonotime(&ts);
	fx_timespec_usecadd(&ts, (long)usec_timeout);

	err = 0;
	lnk = fifo_popq(fifo);
	if (lnk == NULL) {
		fifo->ff_npend++;
		while (!fifo_qsize(fifo) && !err) {
			rc  = fifo_timedwait_lock(fifo, &ts);
			err = (rc != 0); /* ETIMEDOUT  | EINTR */
		}
		fifo->ff_npend--;
		if (!err) {
			lnk = fifo_popq(fifo);
		}
	}
	if (!err && lnk) {
		*ppl = lnk;
		ppl  = &lnk->next;
		while (--n && lnk) {
			lnk  = fifo_popq(fifo);
			if (lnk != NULL) {
				*ppl = lnk;
				ppl  = &lnk->next;
			}
		}
	}
	fifo_release_lock(fifo);

	return lst.next;
}


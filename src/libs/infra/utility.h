/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_UTILITY_H_
#define FUNEX_UTILITY_H_

/*
 * Allocations flags:
 */
#define FX_BZERO   0x01
#define FX_NOFAIL  0x02



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Allocation hooks & allocator-encapsulation:
 */
typedef void *(*fx_malloc_fn)(size_t, void *);
typedef void (*fx_free_fn)(void *, size_t, void *);

struct fx_memory_allocator {
	fx_malloc_fn malloc;
	fx_free_fn   free;
	void *userp;
};
typedef struct fx_memory_allocator fx_malloc_t;

/* Default allocator (std) */
extern fx_malloc_t *fx_mallocator;

/*
 * Memory allocation/deallocation via allocator object.
 */
void *fx_allocate(const fx_malloc_t *alloc, size_t sz);
void fx_deallocate(const fx_malloc_t *alloc, void *p, size_t n);

/*
 * Wrappers over malloc/free via Funex global allocator. By default, useS
 * standard malloc/free.
 */
void *fx_malloc(size_t sz);
void fx_free(void *p, size_t sz);
void *fx_xmalloc(size_t sz, int flags);
void fx_xfree(void *p, size_t sz, int flags);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Wrappers over memset with zeros or 0xff
 */
#define fx_bzero_obj(ptr) fx_bzero((ptr), sizeof(*(ptr)))

FX_ATTR_NONNULL
void fx_bzero(void *p, size_t n);

FX_ATTR_NONNULL
void fx_bff(void *p, size_t n);

FX_ATTR_NONNULL
void fx_bcopy(void *t, const void *s, size_t n);


/*
 * Recursive stack coloring.
 */
void fx_burnstack(int n);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Min/Max as functions for size_t
 */
FX_ATTR_PURE
size_t fx_min(size_t x, size_t y);

FX_ATTR_PURE
size_t fx_max(size_t x, size_t y);


/*
 * Chooses a "good" prime-value for hash-table of n-elements
 */
size_t fx_good_hprime(size_t n);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Global pointer to program's execution name or path.
 */
extern const char *fx_execname;

/*
 * Assign process's path-to-executable, or invocation-name into global pointer
 * fx_execname (derived from Solaris' getexecname(3C)).
 */
void fx_initexecname(void);

/*
 * Converts errno value to human-readable string (strerror return description
 * message).
 */
const char *fx_errno_to_string(int errnum);


#endif /* FUNEX_UTILITY_H_ */



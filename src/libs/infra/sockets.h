/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_SOCKETS_H_
#define FUNEX_SOCKETS_H_

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

typedef in_port_t       fx_inport_t;
typedef struct ucred    fx_ucred_t;

union fx_sockaddr {
	struct sockaddr     addr;
	struct sockaddr_in  in;
	struct sockaddr_in6 in6;
	struct sockaddr_un  un;
};
typedef union fx_sockaddr  fx_sockaddr_t;

struct fx_socket {
	int     fd;
	short   family;
	short   type;
	short   proto;
};
typedef struct fx_socket    fx_socket_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

uint16_t fx_htons(uint16_t n);

uint32_t fx_htonl(uint32_t n);

uint16_t fx_ntohs(uint16_t n);

uint32_t fx_ntohl(uint32_t n);

void fx_ucred_self(struct ucred *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_msghdr_setaddr(struct msghdr *, const fx_sockaddr_t *);

struct cmsghdr *fx_cmsg_firsthdr(struct msghdr *);

struct cmsghdr *fx_cmsg_nexthdr(struct msghdr *, struct cmsghdr *);

size_t fx_cmsg_align(size_t);

size_t fx_cmsg_space(size_t);

size_t fx_cmsg_len(size_t);

void *fx_cmsg_data(const struct cmsghdr *);

void fx_cmsg_pack_fd(struct cmsghdr *, int);

int fx_cmsg_unpack_fd(const struct cmsghdr *, int *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_isvalid_portnum(long);

void fx_sockaddr_any(fx_sockaddr_t *);

void fx_sockaddr_any6(fx_sockaddr_t *);

void fx_sockaddr_loopback(fx_sockaddr_t *, in_port_t);

void fx_sockaddr_setport(fx_sockaddr_t *, in_port_t);

int fx_sockaddr_setunix(fx_sockaddr_t *, const char *);

int fx_sockaddr_pton(fx_sockaddr_t *, const char *);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_udpsock_init(fx_socket_t *);

void fx_udpsock_init6(fx_socket_t *);

void fx_udpsock_initu(fx_socket_t *);

void fx_udpsock_destroy(fx_socket_t *);

int fx_udpsock_open(fx_socket_t *);

void fx_udpsock_close(fx_socket_t *);

int fx_udpsock_isopen(const fx_socket_t *);

int fx_udpsock_bind(fx_socket_t *, const fx_sockaddr_t *);

int fx_udpsock_rselect(const fx_socket_t *, const fx_timespec_t *);

int fx_udpsock_sendto(const fx_socket_t *, const void *, size_t,
                      const fx_sockaddr_t *, size_t *);

int fx_udpsock_recvfrom(const fx_socket_t *, void *, size_t,
                        size_t *, fx_sockaddr_t *);


int fx_udpsock_sendmsg(const fx_socket_t *, const struct msghdr *, int);

int fx_udpsock_recvmsg(const fx_socket_t *, struct msghdr *, int);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_tcpsock_init(fx_socket_t *);

void fx_tcpsock_init6(fx_socket_t *);

void fx_tcpsock_initu(fx_socket_t *);

void fx_tcpsock_destroy(fx_socket_t *);

int fx_tcpsock_open(fx_socket_t *);

void fx_tcpsock_close(fx_socket_t *);

int fx_tcpsock_bind(fx_socket_t *, const fx_sockaddr_t *);

int fx_tcpsock_listen(const fx_socket_t *, int);

int fx_tcpsock_accept(const fx_socket_t *, fx_socket_t *, fx_sockaddr_t *);

int fx_tcpsock_connect(fx_socket_t *, const fx_sockaddr_t *);

int fx_tcpsock_shutdown(const fx_socket_t *);

int fx_tcpsock_rselect(const fx_socket_t *, const fx_timespec_t *);

void fx_tcpsock_setnodelay(const fx_socket_t *);

void fx_tcpsock_setkeepalive(const fx_socket_t *);

void fx_tcpsock_setreuseaddr(const fx_socket_t *);

void fx_tcpsock_setnonblock(const fx_socket_t *);

int fx_tcpsock_recv(const fx_socket_t *, void *, size_t, size_t *);

int fx_tcpsock_send(const fx_socket_t *, const void *, size_t, size_t *);

int fx_tcpsock_sendmsg(const fx_socket_t *, const struct msghdr *, int);

int fx_tcpsock_recvmsg(const fx_socket_t *, struct msghdr *, int);

int fx_tcpsock_peercred(const fx_socket_t *, fx_ucred_t *);


#endif /* FUNEX_SOCKETS_H_ */


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_THREAD_H_
#define FUNEX_THREAD_H_


/*
 * Max thread name. See man PRCTL(2), PR_SET_NAME
 */
#define FX_THREAD_NAME_MAXLEN     15


/*
 * Error-checking wapperes over POSIX-threads (pthread) functionality;
 * Panic in case lib-pthread returns error.
 */
struct fx_mutex {
	unsigned int    magic;
	pthread_mutex_t mutex;
};
typedef struct fx_mutex        fx_mutex_t;


struct fx_cond {
	unsigned int    magic;
	pthread_cond_t  cond;
};
typedef struct fx_cond         fx_cond_t;


struct fx_rwlock {
	pthread_rwlock_t rwlock;
};
typedef struct fx_rwlock       fx_rwlock_t;


struct fx_spinlock {
	pthread_spinlock_t spinlock;
};
typedef struct fx_spinlock     fx_spinlock_t;


struct fx_lock {
	fx_mutex_t  mutex;
	fx_cond_t   cond;
	unsigned    magic;
};
typedef struct fx_lock fx_lock_t;


/*
 * Wrapper over pthread threading. Should not exit in the middle, may not be
 * cancelated.
 */
typedef void (*fx_thread_fn)(void *);

struct fx_thread {
	fx_thread_fn th_exec;  /* Entry point          */
	pthread_t th_id;        /* PThread ID           */
	pid_t     th_pid;       /* Linux gettid         */
	void     *th_arg;       /* Execution arg        */
	char th_blocksig;       /* Bool: block signals  */
	char th_name[16];       /* Optional name        */
};
typedef struct fx_thread    fx_thread_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Mutex wrapper. Always using error-checking mutex. Lock is blocking.
 * Trylock returns 0 upon success, or -1 upon failure.
 */
void fx_mutex_init(fx_mutex_t *m);
void fx_mutex_destroy(fx_mutex_t *m);
void fx_mutex_lock(fx_mutex_t *m);
int  fx_mutex_trylock(fx_mutex_t *m);
void fx_mutex_unlock(fx_mutex_t *m);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Condition wrapper. In case of cond-wait and cond-timedwait, it is up to the
 * user to explicitly lock/unlock the associated mutex. The returned value of
 * cond-timedwait is 0 upon success; non-zero value (-ETIMEDOUT) upon
 * timeout.
 */
void fx_cond_init(fx_cond_t *c);
void fx_cond_destroy(fx_cond_t *c);
void fx_cond_wait(fx_cond_t *c, fx_mutex_t *);
int  fx_cond_timedwait(fx_cond_t *c, fx_mutex_t *, const fx_timespec_t *);
void fx_cond_signal(fx_cond_t *c);
void fx_cond_broadcast(fx_cond_t *c);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Lock wrapper: a condition + mutex pair.
 */
void fx_lock_init(fx_lock_t *lk);
void fx_lock_destroy(fx_lock_t *lk);
void fx_lock_acquire(fx_lock_t *lk);
void fx_lock_release(fx_lock_t *lk);
void fx_lock_wait(fx_lock_t *lk);
int  fx_lock_timedwait(fx_lock_t *lk, const fx_timespec_t *);
void fx_lock_signal(fx_lock_t *lk);
void fx_lock_broadcast(fx_lock_t *lk);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Read-Write lock wrapper. Try-rdlock returns 0 upon success, -1
 * upon failure. Timed-rdlock and Timed-wrlock returns 0 upon success,
 * non-zero value (-ETIMEDOUT) upon timeout.
 */
void fx_rwlock_init(fx_rwlock_t *rw);
void fx_rwlock_destroy(fx_rwlock_t *rw);
void fx_rwlock_rdlock(fx_rwlock_t *rw);
void fx_rwlock_wrlock(fx_rwlock_t *rw);
int  fx_rwlock_tryrdlock(fx_rwlock_t *rw);
int  fx_rwlock_trywrlock(fx_rwlock_t *rw);
int  fx_rwlock_timedrdlock(fx_rwlock_t *rw, size_t usec_timeout);
int  fx_rwlock_timedwrlock(fx_rwlock_t *rw, size_t usec_timeout);
void fx_rwlock_unlock(fx_rwlock_t *rw);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Spinlock wrapper. Try-lock returns 0 upon success, and -1 upon
 * failure.
 */
void fx_spinlock_init(fx_spinlock_t *s);
void fx_spinlock_destroy(fx_spinlock_t *s);
void fx_spinlock_lock(fx_spinlock_t *s);
int  fx_spinlock_trylock(fx_spinlock_t *s);
void fx_spinlock_unlock(fx_spinlock_t *s);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Initialize or destroy thread-container object with defaults.
 */
void fx_thread_init(fx_thread_t *);

void fx_thread_destroy(fx_thread_t *);

/*
 * Associate short-name string with thread's execution context.
 * N.B. Must be calles before start.
 */
void fx_thread_setname(fx_thread_t *, const char *);

/*
 * Creates new thread and starts execution. Will not return until sub-thread
 * has start running
 */
void fx_thread_start(fx_thread_t *, void (*fn)(void *), void *arg);

/*
 * Join a thread. Returns 0 upon success, -1 if no thread-ID was found and flags
 * is zero. If flags is FX_NOFAIL, panics upon failure.
 */
int fx_join_thread(const fx_thread_t *, int flags);

/*
 * Wrapper over 'prctl' to implement BSD-like 'setproctitle'. Linux specific.
 */
void fx_setproctitle(const char *);


#endif /* FUNEX_THREAD_H_ */


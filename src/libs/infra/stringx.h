/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_STRINGX_H_
#define FUNEX_STRINGX_H_


typedef int (*fx_chr_pred)(char);
typedef void (*fx_chr_modify_fn)(char *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * String Operations:
 */
/*
 * Returns the number of characters in s before the first null character.
 */
size_t fx_str_length(const char *s);

/*
 * Three way lexicographic compare of two characters-arrays.
 */
int fx_str_compare(const char *s1, const char *s2, size_t n);
int fx_str_ncompare(const char *s1, size_t n1,
                    const char *s2, size_t n2);

/*
 * Returns the first occurrence of s2 as a substring of s1, or null if no such
 * substring.
 */
const char *fx_str_find(const char *s1, size_t n1,
                        const char *s2, size_t n2);


/*
 * Returns the last occurrence of s2 as substring of s1.
 */
const char *fx_str_rfind(const char *s1, size_t n1,
                         const char *s2, size_t n2);


const char *fx_str_find_chr(const char *s, size_t n, char a);


/*
 * Returns the last occurrence of c within the first n characters of s
 */
const char *fx_str_rfind_chr(const char *s, size_t n, char c);

/*
 * Returns the first occurrence of any of the characters of s2 in s1.
 */
const char *fx_str_find_first_of(const char *s1, size_t n1,
                                 const char *s2, size_t n2);

/*
 * Returns the first occurrence of any of the char of s2 which is not in s1.
 */
const char *fx_str_find_first_not_of(const char *s1, size_t n1,
                                     const char *s2, size_t n2);

/*
 * Returns the first character in s which is not equal to c.
 */
const char *fx_str_find_first_not_eq(const char *s, size_t n, char c);

/*
 * Returns the last occurrence of any of the characters of s2 within the first
 * n1 characters of s1.
 */
const char *fx_str_find_last_of(const char *s1, size_t n1,
                                const char *s2, size_t n2);

/*
 * Returns the last occurrence of any of the characters of s2 which is not in
 * the first n1 characters of s1.
 */
const char *
fx_str_find_last_not_of(const char *s1, size_t n1,
                        const char *s2, size_t n2);

/*
 * Returns the last character within the first n characters of s which is not
 * equal to c.
 */
const char *
fx_str_find_last_not_eq(const char *s, size_t n, char c);

/*
 * Returns the number of matching equal characters from the first n characters
 * of s1 and s2.
 */
size_t fx_str_common_prefix(const char *s1, const char *s2, size_t n);

/*
 * Returns the number of matching equal characters from the last n characters
 * of s1 and s2.
 */
size_t fx_str_common_suffix(const char *s1, const char *s2, size_t n);


/*
 * Returns the number of of characters from the first n1 elements of s1 that
 * overlaps any of the characters of the first n2 elements of s2.
 */
size_t fx_str_overlaps(const char *s1, size_t n1,
                       const char *s2, size_t n2);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Copy the first n characters of s into p. In case of overlap, uses safe copy.
 */
void fx_str_copy(char *p, const char *s, size_t n);


/*
 * Assigns n copies of c to the first n elements of s.
 */
void fx_str_fill(char *s, size_t n, char c);

/*
 * Assign EOS character ('\0') in s[n]
 */
void fx_str_terminate(char *s, size_t n);

/*
 * Revere the order of characters in s.
 */
void fx_str_reverse(char *s, size_t n);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Inserts the first n2 characters of s to in front of the first n1 characters
 * of p. In case of insufficient buffer-size, the result string is truncated.
 *
 * p   Target buffer
 * sz  Size of buffer: number of writable elements after p.
 * n1  Number of chars already inp (must be less or equal to sz)
 * s   Source string (may overlap any of the characters of p)
 * n2  Number of chars in s
 *
 * Returns the number of characters in p after insertion (always less or equal
 * to sz).
 */
size_t fx_str_insert(char *p, size_t sz, size_t n1,
                     const char *s, size_t n2);


/*
 * Inserts n2 copies of c to the front of p. Tries to insert as many characters
 * as possible, but does not insert more then available writable characters
 * in the buffer.
 *
 * Makes room at the beginning of the buffer: move the current string m steps
 * forward, then fill k c-characters into p.
 *
 * p   Target buffer
 * sz  Size of buffer: number of writable elements after p.
 * n1  Number of chars already in p (must be less or equal to sz)
 * n2  Number of copies of c to insert.
 * c   Fill character.
 *
 * Returns the number of characters in p after insertion (always less or equal
 * to sz).
 */
size_t fx_str_insert_chr(char *p, size_t sz,
                         size_t n1, size_t n2, char c);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Replaces the first n1 characters of p with the first n2 characters of s.
 *
 * p   Target buffer
 * sz  Size of buffer: number of writable elements after p.
 * len Length of current string in p.
 * n1  Number of chars to replace (must be less or equal to len).
 * s   Source string (may overlap any of the characters of p)
 * n2  Number of chars in s
 *
 * Returns the number of characters in p after replacement (always less or
 * equal to sz).
 */
size_t fx_str_replace(char *p, size_t sz, size_t len, size_t n1,
                      const char *s, size_t n2);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Replace the first n2 characters of p with n2 copies of c.
 *
 * Returns the number of characters in p after replacement (always less or
 * equal to sz).
 */
size_t fx_str_replace_chr(char *p, size_t sz, size_t len,
                          size_t n1, size_t n2, char c);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Char-Traits Functions:
 */
int fx_chr_isalnum(char c);
int fx_chr_isalpha(char c);
int fx_chr_isascii(char c);
int fx_chr_isblank(char c);
int fx_chr_iscntrl(char c);
int fx_chr_isdigit(char c);
int fx_chr_isgraph(char c);
int fx_chr_islower(char c);
int fx_chr_isprint(char c);
int fx_chr_ispunct(char c);
int fx_chr_isspace(char c);
int fx_chr_isupper(char c);
int fx_chr_isxdigit(char c);

int fx_chr_toupper(char c);
int fx_chr_tolower(char c);

#endif /* FUNEX_STRINGX_H_ */


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "compiler.h"
#include "macros.h"
#include "panic.h"
#include "utility.h"


const char *fx_execname = NULL;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
FX_ATTR_PURE
size_t fx_min(size_t x, size_t y)
{
	return FX_MIN(x, y);
}

FX_ATTR_PURE
size_t fx_max(size_t x, size_t y)
{
	return FX_MAX(x, y);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Allocations wrappers:
 */
static void *stdlib_malloc(size_t n, void *hint)
{
	fx_unused(hint);
	return malloc(n);
}

static void stdlib_free(void *p, size_t n, void *hint)
{
	free(p);
	fx_unused(hint);
	fx_unused(n);
}

/* Default allocator, mapped to std allocation functions. */
static fx_malloc_t s_default_allocator = {
	.malloc = stdlib_malloc,
	.free   = stdlib_free,
	.userp  = NULL
};
fx_malloc_t *fx_mallocator = &s_default_allocator;


void *fx_allocate(const fx_malloc_t *alloc, size_t n)
{
	return alloc->malloc(n, alloc->userp);
}

void fx_deallocate(const fx_malloc_t *alloc, void *p, size_t n)
{
	alloc->free(p, n, alloc->userp);
}

/*
 * Wrappers over STD malloc/free
 */
void *fx_malloc(size_t n)
{
	const fx_malloc_t *alloc = &s_default_allocator;
	return fx_allocate(alloc, n);
}

void fx_free(void *p, size_t n)
{
	const fx_malloc_t *alloc = &s_default_allocator;
	fx_deallocate(alloc, p, n);
}

void *fx_xmalloc(size_t n, int flags)
{
	void *p;

	p = fx_malloc(n);
	if (p != NULL) {
		if (flags & FX_BZERO) {
			fx_bzero(p, n);
		}
	} else {
		if (flags & FX_NOFAIL) {
			fx_panic("malloc-failure n=%ld", (long)n);
		}
	}
	return p;
}


void fx_xfree(void *p, size_t n, int flags)
{
	if (flags & FX_BZERO) {
		fx_bzero(p, n);
	}
	fx_free(p, n);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

FX_ATTR_NONNULL
void fx_bzero(void *p, size_t n)
{
	memset(p, 0x00, n);
}

FX_ATTR_NONNULL
void fx_bff(void *p, size_t n)
{
	memset(p, 0xFF, n);
}

FX_ATTR_NONNULL
void fx_bcopy(void *t, const void *s, size_t n)
{
	memcpy(t, s, n);
}

void fx_burnstack(int n)
{
	char buf[512];

	if (n > 0) {
		fx_burnstack(n - 1);
	}
	fx_bff(buf, sizeof(buf));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int fx_getexecname(char *buf, size_t bufsz)
{
	int rc = 0;
	ssize_t res;
	const char *prog;

	fx_bzero(buf, bufsz);

	/* Linux specific */
	res = readlink("/proc/self/exe", buf, bufsz - 1);
	if (res > 0) {
		goto out;
	}

	/* BASH specific */
	prog = getenv("_");
	if ((prog != NULL) && (strlen(prog) < bufsz)) {
		strncpy(buf, prog, bufsz);
		goto out;
	}

	/* GNU */
	prog = program_invocation_name;
	if ((prog != NULL) && (strlen(prog) < bufsz)) {
		strncpy(buf, prog, bufsz);
		goto out;
	}
	prog = program_invocation_short_name;
	if ((prog != NULL) && (strlen(prog) < bufsz)) {
		strncpy(buf, prog, bufsz);
		goto out;
	}

	/* No good */
	if (bufsz > 0) {
		buf[0] = '\0';
	}
	rc = -1;

out:
	return rc;
}


void fx_initexecname(void)
{
	int rc;
	static char s_execname[1024];

	rc = fx_getexecname(s_execname, sizeof(s_execname));
	if (rc == 0) {
		fx_execname = s_execname;
	} else {
		fx_execname = program_invocation_name;
	}
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#define E2S(s)    [s] = "" #s ""
static const char *s_errno_string[] = {
	E2S(EPERM),       E2S(ENOENT),      E2S(ESRCH),
	E2S(EINTR),       E2S(EIO),         E2S(ENXIO),
	E2S(E2BIG),       E2S(ENOEXEC),     E2S(EBADF),
	E2S(ECHILD),      E2S(EAGAIN),      E2S(ENOMEM),
	E2S(EACCES),      E2S(EFAULT),      E2S(ENOTBLK),
	E2S(EBUSY),       E2S(EEXIST),      E2S(EXDEV),
	E2S(ENODEV),      E2S(ENOTDIR),     E2S(EISDIR),
	E2S(EINVAL),      E2S(ENFILE),      E2S(EMFILE),
	E2S(ENOTTY),      E2S(ETXTBSY),     E2S(EFBIG),
	E2S(ENOSPC),      E2S(ESPIPE),      E2S(EROFS),
	E2S(EMLINK),      E2S(EPIPE),       E2S(EDOM),
	E2S(ERANGE),      E2S(EDEADLK),     E2S(ENAMETOOLONG),
	E2S(ENOLCK),      E2S(ENOSYS),      E2S(ENOTEMPTY),
	E2S(ELOOP),       E2S(ENOMSG),      E2S(EIDRM),
	E2S(ECHRNG),      E2S(EL2NSYNC),    E2S(EL3HLT),
	E2S(EL3RST),      E2S(ELNRNG),      E2S(EUNATCH),
	E2S(ENOCSI),      E2S(EL2HLT),      E2S(EBADE),
	E2S(EBADR),       E2S(EXFULL),      E2S(ENOANO),
	E2S(EBADRQC),     E2S(EBADSLT),     E2S(EBFONT),
	E2S(ENOSTR),      E2S(ENODATA),     E2S(ETIME),
	E2S(ENOSR),       E2S(ENONET),      E2S(ENOPKG),
	E2S(EREMOTE),     E2S(ENOLINK),     E2S(EADV),
	E2S(ESRMNT),      E2S(ECOMM),       E2S(EPROTO),
	E2S(EMULTIHOP),   E2S(EDOTDOT),     E2S(EBADMSG),
	E2S(EOVERFLOW),   E2S(ENOTUNIQ),    E2S(EBADFD),
	E2S(EREMCHG),     E2S(ELIBACC),     E2S(ELIBBAD),
	E2S(ELIBSCN),     E2S(ELIBMAX),     E2S(ELIBEXEC),
	E2S(EILSEQ),      E2S(ERESTART),    E2S(ESTRPIPE),
	E2S(EUSERS),      E2S(ENOTSOCK),    E2S(EDESTADDRREQ),
	E2S(EMSGSIZE),    E2S(EPROTOTYPE),  E2S(ENOPROTOOPT),
	E2S(EISCONN),     E2S(EOPNOTSUPP),  E2S(EPROTONOSUPPORT),
	E2S(ENOBUFS),     E2S(ENETRESET),   E2S(ESOCKTNOSUPPORT),
	E2S(EUCLEAN),     E2S(EHWPOISON),   E2S(EPFNOSUPPORT),
	E2S(ESHUTDOWN),   E2S(ENOTCONN),    E2S(EAFNOSUPPORT),
	E2S(EADDRINUSE),  E2S(ESTALE),      E2S(EADDRNOTAVAIL),
	E2S(ENETDOWN),    E2S(ENETUNREACH), E2S(ENOTRECOVERABLE),
	E2S(ENOTNAM),     E2S(ENOKEY),      E2S(ECONNABORTED),
	E2S(ECONNRESET),  E2S(ENAVAIL),     E2S(ETOOMANYREFS),
	E2S(ETIMEDOUT),   E2S(EALREADY),    E2S(ECONNREFUSED),
	E2S(EHOSTDOWN),   E2S(EISNAM),      E2S(EHOSTUNREACH),
	E2S(EINPROGRESS), E2S(EREMOTEIO),   E2S(EDQUOT),
	E2S(ENOMEDIUM),   E2S(EMEDIUMTYPE), E2S(ECANCELED),
	E2S(ERFKILL),     E2S(EKEYEXPIRED), E2S(EKEYREVOKED),
	E2S(EOWNERDEAD),  E2S(EKEYREJECTED),
};

/*
* Converts erron value to human-readable string.
* NB: strerror return errno description message.
*/
const char *fx_errno_to_string(int errnum)
{
	const char *str = NULL;
	if ((errnum > 0) && (errnum < (int)FX_NELEMS(s_errno_string))) {
		str = s_errno_string[errnum];
	}
	return str;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Prime numbers used by SGI STL C++ hashtable. See also:
 * http://planetmath.org/encyclopedia/GoodHashTablePrimes.html
 */
static const size_t s_good_hashtable_primes[] = {
	13UL,
	53UL,
	97UL,
	193UL,
	389UL,
	769UL,
	1543UL,
	3079UL,
	6151UL,
	12289UL,
	24593UL,
	49157UL,
	98317UL,
	196613UL,
	393241UL,
	786433UL,
	1572869UL,
	3145739UL,
	6291469UL,
	12582917UL,
	25165843UL,
	50331653UL,
	100663319UL,
	201326611UL,
	402653189UL,
	805306457UL,
	1610612741UL,
	3221225473UL,
	4294967291UL
};

size_t fx_good_hprime(size_t n)
{
	size_t i, nelems, v;

	nelems = FX_ARRAYSIZE(s_good_hashtable_primes);
	for (i = 0; i < nelems; ++i) {
		v = s_good_hashtable_primes[i];
		if (v >= n) {
			break;
		}
	}
	return v;
}



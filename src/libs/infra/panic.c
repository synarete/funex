/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <errno.h>
#include <error.h>
#include <signal.h>
#include <unistd.h>
#include <libunwind.h>

#include "compiler.h"
#include "macros.h"
#include "utility.h"
#include "panic.h"

static void fx_fatal_error(int errnum, const char *msg);


/*
 * User-provided hook for handling of fatal errors:
 */
static fx_error_cb s_panic_error_fn = fx_fatal_error;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* End process now! */
FX_ATTR_NORETURN
void fx_die(void)
{
	fflush(stdout);
	fflush(stderr);
	sync();
	abort();
}


/* Default call-back: print error and abort. */
static void fx_fatal_error(int errnum, const char *msg)
{
	char buf[1024];

	fx_bzero(buf, sizeof(buf));
	fx_addr2line_backtrace(buf, sizeof(buf));

	fprintf(stderr, "%s errno=%d\n", msg, errnum);
	fprintf(stderr, "%s\n", buf);
	fx_die();
}

/* Let user's call-back do all the hassle; if none set, use default + die */
static void fx_do_panic(int errnum, const char *msg)
{
	fx_error_cb error_callback;

	error_callback = fx_fatal_error;
	if (s_panic_error_fn != NULL) {
		error_callback = s_panic_error_fn;
	}

	error_callback(errnum, msg);
	fx_die();
}

/* Panic -- halt process execution. */
FX_ATTR_NORETURN
static void fx_vpanic(int errnum,
                      const char *file, unsigned line,
                      const char *func, const char *prefix,
                      const char *fmt, va_list ap)
{
	int n = 0;
	size_t sz;
	char msg[2048];

	sz = sizeof(msg);
	fx_bzero(msg, sz);

	if ((file != NULL) && (line > 0) && (strlen(file) < sz / 2)) {
		n += snprintf(msg, sz - 1, "%s:%u: ", file, line);
	}
	if ((func != NULL) && (n < (int)sz)) {
		n += snprintf(msg + n, sz - (size_t)n - 1, "%s: ", func);
	}
	if ((prefix != NULL) && (n < (int)sz)) {
		n += snprintf(msg + n, sz - (size_t)n - 1, "%s: ", prefix);
	}

	if (n < (int)sz) {
		vsnprintf(msg + n, sz - (size_t)n - 1, fmt, ap);
	} else {
		vsnprintf(msg, sz - 1, fmt, ap);
	}

	fx_do_panic(errnum, msg);
	fx_die();
}

FX_ATTR_NORETURN
FX_ATTR_FMTPRINTF(4, 5)
void fx_panicf(const char *file, unsigned line, const char *func,
               const char *fmt, ...)
{
	int errnum;
	va_list ap;

	errnum = errno;

	va_start(ap, fmt);
	fx_vpanic(errnum, file, line, func, "*panic*", fmt, ap);
	va_end(ap);
}

/* Assertion failure call. */
FX_ATTR_NORETURN
void fx_assertion_failure(const char *file, unsigned line,
                          const char *func, const char *expr)
{
	fx_panicf(file, line, func, "Assertion fail `%s'", expr);
}


void fx_set_panic_callback(fx_error_cb fn)
{
	s_panic_error_fn = fn;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Backtracing:
 *
 * No good results with backtrace_symbols, event when compiled with -rdynamic
 * to ld. Prefer having back-trace via 'addr2line' command line utility.
 */
int fx_addr2line_backtrace(char *buf, size_t buf_sz)
{
	int i, n, rc;
	size_t sz, len;
	void *bt_arr[64];
	char addr_list[512];
	const char *progname;
	char *ptr;

	sz = sizeof(bt_arr);
	fx_bzero(bt_arr, sz);

	sz  = sizeof(addr_list);
	ptr = addr_list;
	fx_bzero(ptr, sz);

	progname = fx_execname;
	if (progname == NULL) {
		progname = program_invocation_short_name;
	}

	n = unw_backtrace(bt_arr, (int)(FX_ARRAYSIZE(bt_arr)));
	for (i = 1; i < n - 2; ++i) {
		sz  = sizeof(addr_list);
		len = strlen(addr_list);
		ptr = addr_list + len;
		snprintf(ptr, sz - len - 1, "%p ", bt_arr[i]);
	}

	rc = snprintf(buf, buf_sz, "addr2line -a -C -e %s -f -p -s %s",
	              progname, addr_list);
	return rc;
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_LOGGER_H_
#define FUNEX_LOGGER_H_

/*
 * Logger macros API;
 * Always prefer this macro interface over direct function-call.
 */
#define FX_LL_(LEVEL)          FX_JOIN(FX_LL_, LEVEL)
#define FX_CHECKF(LOGGER, FMT, ...) \
	do { if (fx_unlikely(fx_check_format) && \
		         fx_unlikely(LOGGER == NULL)) \
			fprintf(stdout, FMT "\n", __VA_ARGS__); } while (0)

#define FX_LOGF2(LOGGER, LEVEL, FUNC, FMT, ...) \
	do { fx_logf(LOGGER, FX_LL_(LEVEL), \
		             __FILE__, __LINE__, FUNC, FMT, __VA_ARGS__); } while (0)

#define FX_LOGF(LOGGER, LEVEL, FMT, ...) \
	do { FX_CHECKF(LOGGER, FMT, __VA_ARGS__);\
		FX_LOGF2(LOGGER, LEVEL, NULL, \
		         FMT, __VA_ARGS__); } while (0)

#define FX_DBGLOGF(LOGGER, LEVEL, FMT, ...) \
	do { FX_CHECKF(LOGGER, FMT, __VA_ARGS__);\
		FX_LOGF2(LOGGER, LEVEL, FX_FUNCTION, \
		         FMT, __VA_ARGS__); } while (0)


#define fx_log_debug3(lg, fmt, ...) \
	FX_DBGLOGF(lg, DEBUG3, fmt, __VA_ARGS__)
#define fx_log_debug2(lg, fmt, ...) \
	FX_DBGLOGF(lg, DEBUG2, fmt, __VA_ARGS__)
#define fx_log_debug(lg, fmt, ...) \
	FX_DBGLOGF(lg, DEBUG1, fmt, __VA_ARGS__)
#define fx_log_info(lg, fmt, ...) \
	FX_LOGF(lg, INFO,  fmt, __VA_ARGS__)
#define fx_log_warn(lg, fmt, ...) \
	FX_LOGF(lg, WARN,fmt, __VA_ARGS__)
#define fx_log_error(lg, fmt, ...) \
	FX_LOGF(lg, ERROR, fmt, __VA_ARGS__)
#define fx_log_critical(lg, fmt, ...) \
	FX_LOGF(lg, CRIT, fmt, __VA_ARGS__)

#define fx_trace3(fmt, ...) \
	fx_log_debug3(fx_default_logger, fmt, __VA_ARGS__)
#define fx_trace2(fmt, ...) \
	fx_log_debug2(fx_default_logger, fmt, __VA_ARGS__)
#define fx_trace1(fmt, ...) \
	fx_log_debug(fx_default_logger, fmt, __VA_ARGS__)
#define fx_info(fmt, ...) \
	fx_log_info(fx_default_logger, fmt, __VA_ARGS__)
#define fx_warn(fmt, ...) \
	fx_log_warn(fx_default_logger, fmt, __VA_ARGS__)
#define fx_error(fmt, ...) \
	fx_log_error(fx_default_logger, fmt, __VA_ARGS__)
#define fx_critical(fmt, ...) \
	fx_log_critical(fx_default_logger, fmt, __VA_ARGS__)

/*
 * Log Levels.
 */
#define FX_LL_DEBUG3           (-3)
#define FX_LL_DEBUG2           (-2)
#define FX_LL_DEBUG1           (-1)
#define FX_LL_NONE             (0)
#define FX_LL_INFO             (2)
#define FX_LL_WARN             (3)
#define FX_LL_ERROR            (4)
#define FX_LL_CRIT             (5)

/*
 * Log control flags:
 */
#define FX_LF_STREAM           FX_BITFLAG(0)
#define FX_LF_STDOUT           FX_BITFLAG(1)
#define FX_LF_SYSLOG           FX_BITFLAG(2)
#define FX_LF_PROGNAME         FX_BITFLAG(3)
#define FX_LF_SEVERITY         FX_BITFLAG(4)
#define FX_LF_FILINE           FX_BITFLAG(5)
#define FX_LF_FUNC             FX_BITFLAG(6)
#define FX_LF_TIMESTAMP        FX_BITFLAG(7)
#define FX_LF_ERRNOSTR         FX_BITFLAG(8)

#define FX_LF_DEFAULT_         (FX_LF_STDOUT      |   \
                                FX_LF_SYSLOG      |   \
                                FX_LF_FILINE      |   \
                                FX_LF_FUNC        |   \
                                FX_LF_TIMESTAMP)

#define FX_LL_DEFAULT          (FX_LL_INFO)
#define FX_LF_DEFAULT          \
	(FX_LF_STDOUT | FX_LF_PROGNAME | FX_LF_SEVERITY)

/* Default log-rate (messages per second) */
#define FX_LR_DEFAULT          (1024)
#define FX_LR_UNLIMITED        (0)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Interface log-message.
 */
struct fx_logmsg {
	int lm_level;
	int lm_errno;
	int lm_line;
	int lm_pad;
	const char *lm_file;
	const char *lm_func;
	const char *lm_ident;
	const char *lm_msgstr;
};
typedef struct fx_logmsg   fx_logmsg_t;

/*
 * Logging interface.
 */
struct fx_logger {
	/* Control flags */
	int log_flags;

	/* Normal/debug threshold level */
	int log_level;
	int log_debug;

	/*
	 * Basic rate-limit mechanism: drop non-critical messages in case of
	 * overflow. Should be effective mainly in cases if logging diarrhea.
	 * Zero log_rate indicates disabling; log_count and log_last are used by
	 * internal logger implementation.
	 */
	size_t log_rate;
	size_t log_count;
	time_t log_last;

	/* Optional ident-prefix */
	const char *log_ident;

	/*
	 * Test-message hook. Returns status code: 0 where message should be
	 * sent, non-zero where message should be discarded.
	 */
	int (*log_testmsg)(struct fx_logger *, const struct fx_logmsg *);

	/*
	 * Send-message hook.
	 */
	void (*log_sendmsg)(struct fx_logger *, const struct fx_logmsg *);
};
typedef struct fx_logger   fx_logger_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Initialize logger with default hooks & flags. */
void fx_logger_init(fx_logger_t *logger);

/* Destructor */
void fx_logger_destroy(fx_logger_t *logger);

/*
 * Initialize logging facility: Limits the number of logging-messages to @rate
 * per sec, or don't use rate-limit mechanism if @rate is zero. Sets @flags
 * for control, allow only messages above @level.
 */
void fx_logger_setup(fx_logger_t *logger, const char *ident,
                     int level, int debug, int flags, size_t rate);


/* Logging facility API. Prefer the above macros. */
void fx_logf(fx_logger_t *logger, int level,
             const char *file, int line, const char *func,
             const char *fmt, ...);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Default logger instance: */
extern fx_logger_t *fx_default_logger;

/* Foramt checker flags -- keep zero */
extern short fx_check_format;


#endif /* FUNEX_LOGGER_H_ */





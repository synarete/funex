/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_ASCII_H_
#define FUNEX_ASCII_H_


/* Flags for case-sensitivity. */
enum FX_CASE {
	FX_UPPERCASE        = 0,
	FX_LOWERCASE        = 1
};


/*
 * Returns the null terminated string of lowercase ascii letters a-z.
 */
const char *fx_ascii_lowercase_letters(void);

/*
    Returns a (null terminated) string of uppercase letters
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
*/
const char *fx_ascii_uppercase_letters(void);

/*
    Returns a (null terminated) string of "abc...z" or
    "ABC...Z" letters.
*/
const char *fx_ascii_abc(int icase);

/*
    Returns concatenation of lowercase_letters and
    uppercase_letters.
*/
const char *fx_ascii_letters(void);

/*
    Returns a (null terminated) string of decimal digits
    ("0123456789").
*/
const char *fx_ascii_digits(void);


/*
    Returns a (null terminated) string of octal digits ("01234567").
*/
const char *fx_ascii_odigits(void);

/*
    Returns the string "0123456789ABCDEF".
*/
const char *fx_ascii_xdigits_uppercase(void);

/*
    Returns the (null terminated) string "0123456789abcdef".
*/
const char *fx_ascii_xdigits_lowercase(void);

/*
    Returns a (null terminated) string of hexadecimal digits.
*/
const char *fx_ascii_xdigits(int icase);


/*
    Returns a (null terminated) string of punctuation characters:
    !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~.
*/
const char *fx_ascii_punctuation(void);

/*
    Returns a (null terminated) string of space, tab, return, linefeed,
    formfeed, and vertical-tab.
*/
const char *fx_ascii_whitespaces(void);

/*
    Returns a (null terminated) string with single space character.
*/
const char *fx_ascii_space(void);


#endif /* FUNEX_ASCII_H_ */



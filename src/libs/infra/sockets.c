/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "compiler.h"
#include "macros.h"
#include "panic.h"
#include "utility.h"
#include "logger.h"
#include "timex.h"
#include "sockets.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

uint16_t fx_htons(uint16_t n)
{
	return htons(n);
}

uint32_t fx_htonl(uint32_t n)
{
	return htonl(n);
}


uint16_t fx_ntohs(uint16_t n)
{
	return ntohs(n);
}

uint32_t fx_ntohl(uint32_t n)
{
	return ntohl(n);
}

void fx_ucred_self(struct ucred *cred)
{
	cred->uid = getuid();
	cred->gid = getgid();
	cred->pid = getpid();
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_isvalid_portnum(long portnum)
{
	return ((portnum > 0) && (portnum < 65536));
}


static void saddr_clear(fx_sockaddr_t *saddr)
{
	fx_bzero(saddr, sizeof(*saddr));
}

static sa_family_t saddr_family(const fx_sockaddr_t *saddr)
{
	return saddr->addr.sa_family;
}

static socklen_t saddr_length(const fx_sockaddr_t *saddr)
{
	socklen_t len = 0;
	const sa_family_t family = saddr_family(saddr);

	if (family == AF_INET) {
		len = sizeof(saddr->in);
	} else if (family == AF_INET6) {
		len = sizeof(saddr->in6);
	} else if (family == AF_UNIX) {
		len = sizeof(saddr->un);
	}
	return len;
}

void fx_sockaddr_any(fx_sockaddr_t *saddr)
{
	struct sockaddr_in *in = &saddr->in;

	saddr_clear(saddr);
	in->sin_family      = AF_INET;
	in->sin_port        = 0;
	in->sin_addr.s_addr = fx_htonl(INADDR_ANY);
}

void fx_sockaddr_any6(fx_sockaddr_t *saddr)
{
	struct sockaddr_in6 *in6 = &saddr->in6;

	saddr_clear(saddr);
	in6->sin6_family    = AF_INET6;
	in6->sin6_port      = fx_htons(0);
	memcpy(&in6->sin6_addr, &in6addr_any, sizeof(in6->sin6_addr));
}

void fx_sockaddr_loopback(fx_sockaddr_t *saddr, in_port_t port)
{
	struct sockaddr_in *in = &saddr->in;

	saddr_clear(saddr);
	in->sin_family      = AF_INET;
	in->sin_port        = fx_htons(port);
	in->sin_addr.s_addr = fx_htonl(INADDR_LOOPBACK);
}

void fx_sockaddr_setport(fx_sockaddr_t *saddr, in_port_t port)
{
	sa_family_t family = saddr_family(saddr);

	if (family == AF_INET6) {
		saddr->in6.sin6_port = fx_htons(port);
	} else if (family == AF_INET) {
		saddr->in.sin_port = fx_htons(port);
	} else {
		fx_panic("illegal-sa-family: family=%ld", (long)family);
	}
}

int fx_sockaddr_setunix(fx_sockaddr_t *saddr, const char *path)
{
	size_t lim, len;
	struct sockaddr_un *un = &saddr->un;

	lim = sizeof(un->sun_path);
	len = strlen(path);
	if (len >= lim) {
		return -1;
	}

	saddr_clear(saddr);
	un->sun_family      = AF_UNIX;
	strncpy(un->sun_path, path, lim);
	return 0;
}


int fx_sockaddr_pton(fx_sockaddr_t *saddr, const char *str)
{
	int res, rc = -1;
	struct sockaddr_in *in  = &saddr->in;
	struct sockaddr_in6 *in6 = &saddr->in6;

	saddr_clear(saddr);
	if (strchr(str, ':') != NULL) {
		in6->sin6_family = AF_INET6;
		res = inet_pton(AF_INET6, str, &in6->sin6_addr);
		rc  = (res == 1) ? 0 : -1;
	} else {
		in->sin_family = AF_INET;
		res = inet_aton(str, &in->sin_addr);
		rc  = (res != 0) ? 0 : -1;
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_msghdr_setaddr(struct msghdr *msgh, const fx_sockaddr_t *saddr)
{
	msgh->msg_namelen   = saddr_length(saddr);
	msgh->msg_name      = (void *)(&saddr->addr);
}

struct cmsghdr *fx_cmsg_firsthdr(struct msghdr *msgh)
{
	return CMSG_FIRSTHDR(msgh);
}

struct cmsghdr *fx_cmsg_nexthdr(struct msghdr *msgh, struct cmsghdr *cmsg)
{
	return CMSG_NXTHDR(msgh, cmsg);
}

size_t fx_cmsg_align(size_t length)
{
	return CMSG_ALIGN(length);
}

size_t fx_cmsg_space(size_t length)
{
	return CMSG_SPACE(length);
}

size_t fx_cmsg_len(size_t length)
{
	return CMSG_LEN(length);
}

void *fx_cmsg_data(const struct cmsghdr *cmsg)
{
	return (void *)CMSG_DATA(cmsg);
}

void fx_cmsg_pack_fd(struct cmsghdr *cmsg, int fd)
{
	void *data;

	cmsg->cmsg_len      = fx_cmsg_len(sizeof(fd));
	cmsg->cmsg_level    = SOL_SOCKET;
	cmsg->cmsg_type     = SCM_RIGHTS;
	data = fx_cmsg_data(cmsg);
	memmove(data, &fd, sizeof(fd));
}

int fx_cmsg_unpack_fd(const struct cmsghdr *cmsg, int *fd)
{
	size_t size;
	const void *data;

	if (cmsg->cmsg_type != SCM_RIGHTS) {
		return -1;
	}
	data = fx_cmsg_data(cmsg);
	size = cmsg->cmsg_len - sizeof(*cmsg);
	if (size != sizeof(*fd)) {
		return -1;
	}
	memmove(fd, data, sizeof(*fd));
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void socket_init(fx_socket_t *sock, short pf, short type)
{
	sock->fd        = -1;
	sock->family    = pf;
	sock->type      = type;
	sock->proto     = IPPROTO_IP;
}

static void
socket_setup(fx_socket_t *sock, int fd, short pf, short type, short proto)
{
	sock->fd        = fd;
	sock->family    = pf;
	sock->type      = type;
	sock->proto     = proto;
}

static void socket_destroy(fx_socket_t *sock)
{
	sock->fd        = -1;
	sock->family    = 0;
	sock->type      = 0;
}

static int socket_checkaddr(const fx_socket_t *sock, const fx_sockaddr_t *saddr)
{
	const sa_family_t family = saddr_family(saddr);
	return (sock->family == (int)(family)) ? 0 : -1;
}

static void socket_checkerrno(const fx_socket_t *sock)
{
	const int errnum = errno;

	switch (errnum) {
		case EBADF:
		case EINVAL:
		case EFAULT:
		case EISCONN:
		case ENOTSOCK:
		case EOPNOTSUPP:
		case EAFNOSUPPORT:
		case ENOPROTOOPT:
			fx_panic("socket-error fd=%d errno=%d", sock->fd, errnum);
			break;
		case EINTR:
		default:
			break;
	}
}

static int socket_isopen(const fx_socket_t *sock)
{
	return (sock->fd > 0);
}

static int socket_open(fx_socket_t *sock)
{
	int fd, rc = 0;

	errno = 0;
	fd = socket(sock->family, sock->type, sock->proto);
	if (fd > 0) {
		sock->fd = fd;
		rc = 0;
	} else {
		fx_error("no-socket: family=%d type=%d proto=%d errno=%d",
		         sock->family, sock->type, sock->proto, errno);
		rc = -1;
	}
	return rc;
}

static void socket_close(fx_socket_t *sock)
{
	int rc;

	errno = 0;
	rc = close(sock->fd);
	if (rc != 0) {
		fx_panic("socket-close: fd=%d family=%d", sock->fd, sock->family);
	}
	sock->fd = -1;
}

static int socket_rselect(const fx_socket_t *sock, const fx_timespec_t *ts)
{
	int rc, res, fd;
	fd_set rfds;

	fd = sock->fd;
	FD_ZERO(&rfds);
	FD_SET(fd, &rfds);

	errno = 0;
	rc = -1;
	res = pselect(fd + 1, &rfds, NULL, NULL, ts, NULL);
	if (res == 1) {
		if (FD_ISSET(fd, &rfds)) {
			rc = 0;
		} else {
			errno = ETIMEDOUT; /* XXX ? */
		}
	}
	return rc;
}

static int socket_bind(fx_socket_t *sock, const fx_sockaddr_t *saddr)
{
	int rc;

	rc = socket_checkaddr(sock, saddr);
	if (rc != 0) {
		return -1;
	}

	errno = 0;
	rc = bind(sock->fd, &saddr->addr, saddr_length(saddr));
	if (rc != 0) {
		socket_checkerrno(sock);
		return -1;
	}

	return 0;
}

static int
socket_sendto(const fx_socket_t *sock, const void *buf, size_t len,
              const fx_sockaddr_t *saddr, size_t *p_sent)
{
	ssize_t res;
	socklen_t addrlen;

	errno = 0;
	addrlen = saddr_length(saddr);
	res = sendto(sock->fd, buf, len, 0, &saddr->addr, addrlen);
	if (res < 0) {
		socket_checkerrno(sock);
		return -1;
	}
	*p_sent = (size_t)res;
	return 0;
}

static int
socket_recvfrom(const fx_socket_t *sock, void *buf, size_t len,
                size_t *p_recv, fx_sockaddr_t *saddr)
{
	int rc;
	ssize_t res;
	socklen_t addrlen = sizeof(*saddr);

	errno = 0;
	saddr_clear(saddr);
	res = recvfrom(sock->fd, buf, len, 0, &saddr->addr, &addrlen);
	if (res >= 0) {
		*p_recv = (size_t)(res);
		rc = 0;
	} else {
		socket_checkerrno(sock);
		rc = -1;
	}
	return rc;
}

static int socket_listen(const fx_socket_t *sock, int backlog)
{
	int rc;

	errno = 0;
	rc = listen(sock->fd, backlog);
	if (rc != 0) {
		socket_checkerrno(sock);
		return -1;
	}
	return 0;
}

static int
socket_accept(const fx_socket_t *sock, fx_socket_t *acsock, fx_sockaddr_t *peer)
{
	int rc;
	socklen_t addrlen = sizeof(*peer);

	saddr_clear(peer);
	errno = 0;
	rc = accept(sock->fd, &peer->addr, &addrlen);
	if (rc < 0) {
		socket_checkerrno(sock);
		return -1;
	}

	socket_setup(acsock, rc, sock->family, sock->type, sock->proto);
	return 0;
}

static int socket_connect(const fx_socket_t *sock, const fx_sockaddr_t *saddr)
{
	int rc;

	rc = socket_checkaddr(sock, saddr);
	if (rc != 0) {
		return -1;
	}

	errno = 0;
	rc = connect(sock->fd, &saddr->addr, saddr_length(saddr));
	if (rc != 0) {
		socket_checkerrno(sock);
		return -1;
	}
	return 0;
}

static void socket_shutdown(const fx_socket_t *sock)
{
	int rc;

	errno = 0;
	rc = shutdown(sock->fd, SHUT_RDWR);
	if (rc != 0) {
		socket_checkerrno(sock);
	}
}

static void socket_setnodelay(const fx_socket_t *sock)
{
	int rc, nodelay = 1;
	const socklen_t optlen = sizeof(nodelay);

	errno = 0;
	rc = setsockopt(sock->fd, sock->proto, TCP_NODELAY, &nodelay, optlen);
	if (rc != 0) {
		socket_checkerrno(sock);
	}
}

static void socket_setkeepalive(const fx_socket_t *sock)
{
	int rc, keepalive = 1;
	const socklen_t optlen = sizeof(keepalive);

	errno = 0;
	rc = setsockopt(sock->fd, SOL_SOCKET, SO_KEEPALIVE, &keepalive, optlen);
	if (rc != 0) {
		socket_checkerrno(sock);
	}
}

static void socket_setreuseaddr(const fx_socket_t *sock)
{
	int rc, reuse = 1;
	const socklen_t optlen = sizeof(reuse);

	errno = 0;
	rc = setsockopt(sock->fd, SOL_SOCKET, SO_REUSEADDR, &reuse, optlen);
	if (rc != 0) {
		socket_checkerrno(sock);
	}
}

static int socket_peercred(const fx_socket_t *sock, struct ucred *cred)
{
	int rc;
	socklen_t cred_len = sizeof(*cred);

	errno = 0;
	rc = getsockopt(sock->fd, SOL_SOCKET, SO_PEERCRED, cred, &cred_len);
	if (rc != 0) {
		socket_checkerrno(sock);
	}
	return rc;
}


static void socket_setnonblock(const fx_socket_t *sock)
{
	int rc, opts;

	errno = 0;
	opts = fcntl(sock->fd, F_GETFL);
	if (opts < 0) {
		fx_panic("no-fcntl(F_GETFL) fd=%d", sock->fd);
	}
	opts = (opts | O_NONBLOCK);
	rc   = fcntl(sock->fd, F_SETFL, opts);
	if (rc < 0) {
		fx_panic("no-fcntl(F_SETFL) fd=%d opts=%#x", sock->fd, opts);
	}
}

static int socket_recv(const fx_socket_t *sock,
                       void *buf, size_t len, size_t *p_recv)
{
	ssize_t res;

	res = recv(sock->fd, buf, len, 0);
	if (res == (ssize_t)(-1)) {
		socket_checkerrno(sock);
		return -1;
	}
	if (res == 0) {
		return -1; /* peer shutdown */
	}

	*p_recv = (size_t)res;
	return 0;
}

static int socket_send(const fx_socket_t *sock,
                       const void *buf, size_t len, size_t *p_sent)
{
	ssize_t res = 0;

	res = send(sock->fd, buf, len, 0);
	if (res == (ssize_t)(-1)) {
		socket_checkerrno(sock);
		return -1;
	}

	*p_sent = (size_t)res;
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int
socket_sendmsg(const fx_socket_t *sock, const struct msghdr *msgh, int flags)
{
	ssize_t res;

	errno = 0;
	res = sendmsg(sock->fd, msgh, flags);
	if (res == (ssize_t)(-1)) {
		socket_checkerrno(sock);
		return -1;
	}
	return 0;
}

static int
socket_recvmsg(const fx_socket_t *sock, struct msghdr *msgh, int flags)
{
	ssize_t res;

	errno = 0;
	res = recvmsg(sock->fd, msgh, flags);
	if (res == (ssize_t)(-1)) {
		socket_checkerrno(sock);
		return -1;
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_udpsock_init(fx_socket_t *sock)
{
	socket_init(sock, PF_INET, SOCK_DGRAM);
	sock->proto = IPPROTO_UDP;
}

void fx_udpsock_init6(fx_socket_t *sock)
{
	socket_init(sock, PF_INET6, SOCK_DGRAM);
	sock->proto = IPPROTO_UDP;
}

void fx_udpsock_initu(fx_socket_t *sock)
{
	socket_init(sock, AF_UNIX, SOCK_DGRAM);
}

void fx_udpsock_destroy(fx_socket_t *sock)
{
	fx_udpsock_close(sock);
	socket_destroy(sock);
}

int fx_udpsock_open(fx_socket_t *sock)
{
	int rc;

	if (!socket_isopen(sock)) {
		rc = socket_open(sock);
	} else {
		fx_error("udpsock: already-open fd=%d", sock->fd);
		rc = -1;
	}
	return rc;
}

void fx_udpsock_close(fx_socket_t *sock)
{
	if (socket_isopen(sock)) {
		socket_close(sock);
	}
}

int fx_udpsock_isopen(const fx_socket_t *sock)
{
	return socket_isopen(sock);
}

int fx_udpsock_bind(fx_socket_t *sock, const fx_sockaddr_t *saddr)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_bind(sock, saddr);
	}
	return rc;
}

int fx_udpsock_rselect(const fx_socket_t *sock, const fx_timespec_t *ts)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_rselect(sock, ts);
	}
	return rc;
}

int fx_udpsock_sendto(const fx_socket_t *sock, const void *buf, size_t len,
                      const fx_sockaddr_t *saddr, size_t *p_sent)
{
	int rc = -1;
	size_t sent = 0;

	if (socket_isopen(sock)) {
		p_sent = p_sent ? p_sent : &sent;
		rc = socket_sendto(sock, buf, len, saddr, p_sent);
	}
	return rc;
}

int fx_udpsock_recvfrom(const fx_socket_t *sock, void *buf, size_t len,
                        size_t *p_recv, fx_sockaddr_t *src_addr)
{
	int rc = -1;
	size_t recv = 0;

	if (socket_isopen(sock)) {
		p_recv = p_recv ? p_recv : &recv;
		rc = socket_recvfrom(sock, buf, len, p_recv, src_addr);
	}
	return rc;
}


int fx_udpsock_sendmsg(const fx_socket_t *sock,
                       const struct msghdr *msgh, int flags)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_sendmsg(sock, msgh, flags);
	}
	return rc;
}

int fx_udpsock_recvmsg(const fx_socket_t *sock,
                       struct msghdr *msgh, int flags)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_recvmsg(sock, msgh, flags);
	}
	return rc;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_tcpsock_init(fx_socket_t *sock)
{
	socket_init(sock, PF_INET, SOCK_STREAM);
	sock->proto = IPPROTO_TCP;
}

void fx_tcpsock_init6(fx_socket_t *sock)
{
	socket_init(sock, PF_INET6, SOCK_STREAM);
	sock->proto = IPPROTO_TCP;
}

void fx_tcpsock_initu(fx_socket_t *sock)
{
	socket_init(sock, AF_UNIX, SOCK_STREAM);
}

void fx_tcpsock_destroy(fx_socket_t *sock)
{
	fx_tcpsock_close(sock);
	socket_destroy(sock);
}

int fx_tcpsock_open(fx_socket_t *sock)
{
	int rc;

	if (!socket_isopen(sock)) {
		rc = socket_open(sock);
	} else {
		fx_error("tcpsock: already-open fd=%d", sock->fd);
		rc = -1;
	}
	return rc;
}

void fx_tcpsock_close(fx_socket_t *sock)
{
	if (socket_isopen(sock)) {
		socket_close(sock);
	}
}

int fx_tcpsock_bind(fx_socket_t *sock, const fx_sockaddr_t *saddr)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_bind(sock, saddr);
	}
	return rc;
}

int fx_tcpsock_listen(const fx_socket_t *sock, int backlog)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_listen(sock, backlog);
	}
	return rc;
}

int fx_tcpsock_accept(const fx_socket_t *sock,
                      fx_socket_t *acsock, fx_sockaddr_t *peeraddr)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_accept(sock, acsock, peeraddr);
	}
	return rc;
}

int fx_tcpsock_connect(fx_socket_t *sock, const fx_sockaddr_t *saddr)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_connect(sock, saddr);
	}
	return rc;
}

int fx_tcpsock_shutdown(const fx_socket_t *sock)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		socket_shutdown(sock);
		rc = 0;
	}
	return rc;
}

int fx_tcpsock_rselect(const fx_socket_t *sock, const fx_timespec_t *ts)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_rselect(sock, ts);
	}
	return rc;
}

void fx_tcpsock_setnodelay(const fx_socket_t *sock)
{
	if (socket_isopen(sock)) {
		socket_setnodelay(sock);
	}
}

void fx_tcpsock_setkeepalive(const fx_socket_t *sock)
{
	if (socket_isopen(sock)) {
		socket_setkeepalive(sock);
	}
}

void fx_tcpsock_setreuseaddr(const fx_socket_t *sock)
{
	if (socket_isopen(sock)) {
		socket_setreuseaddr(sock);
	}
}

void fx_tcpsock_setnonblock(const fx_socket_t *sock)
{
	if (socket_isopen(sock)) {
		socket_setnonblock(sock);
	}
}

int fx_tcpsock_recv(const fx_socket_t *sock,
                    void *buf, size_t len, size_t *p_recv)
{
	int rc = -1;
	size_t recv = 0;

	if (socket_isopen(sock)) {
		p_recv = p_recv ? p_recv : &recv;
		rc = socket_recv(sock, buf, len, p_recv);
	}
	return rc;
}

int fx_tcpsock_send(const fx_socket_t *sock,
                    const void *buf, size_t len, size_t *p_sent)
{
	int rc = -1;
	size_t sent = 0;

	if (socket_isopen(sock)) {
		p_sent = p_sent ? p_sent : &sent;
		rc = socket_send(sock, buf, len, p_sent);
	}
	return rc;
}

int fx_tcpsock_sendmsg(const fx_socket_t *sock,
                       const struct msghdr *msgh, int flags)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_sendmsg(sock, msgh, flags);
	}
	return rc;
}

int fx_tcpsock_recvmsg(const fx_socket_t *sock,
                       struct msghdr *msgh, int flags)
{
	int rc = -1;

	if (socket_isopen(sock)) {
		rc = socket_recvmsg(sock, msgh, flags);
	}
	return rc;
}

int fx_tcpsock_peercred(const fx_socket_t *sock, fx_ucred_t *cred)
{
	int rc;

	if (sock->family != AF_UNIX) {
		return -1;
	}
	if (!socket_isopen(sock)) {
		return -1;
	}
	rc = socket_peercred(sock, cred);
	return rc;
}

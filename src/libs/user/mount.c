/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <libmount/libmount.h>

#include "fnxinfra.h"
#include "fnxuser.h"
#include "fnxnames.h"

/*
 * Traverse dir-path upward until reached first file-system boundary.
 */
int fnx_fsboundary(const char *path, char **fsb)
{
	int rc = 0;
	size_t size, len, rem;
	char *rpath, *rpath2, *eos;
	dev_t dev_cur, dev_prnt;
	struct stat st;

	size  = PATH_MAX + 4;
	len   = strlen(path);
	rpath = NULL;
	if (len >= PATH_MAX) {
		rc = -EINVAL;
		goto out_err;
	}
	rpath = (char *)malloc(size);
	if (rpath == NULL) {
		rc = -errno;
		goto out_err;
	}

	/* Resolve realpath */
	if (realpath(path, rpath) != rpath) {
		rc = (errno != 0) ? -errno : -EINVAL;
		goto out_err;
	}
	if ((rc = fnx_statdev(rpath, &st, &dev_cur)) != 0) {
		goto out_err;
	}
	/* If not dir, force dir-path */
	if (!S_ISDIR(st.st_mode)) {
		eos = strrchr(rpath, '/');
		if (eos != NULL) {
			*eos = '\0';
		}
	}

	/* Traverse-up */
	while (strcmp(rpath, "/") != 0) {
		len = strlen(rpath);
		eos = rpath + len;
		rem = size - len;
		strncpy(eos, "/..", rem);
		rc = fnx_statdev(rpath, &st, &dev_prnt);
		if (rc != 0) {
			goto out_err;
		}
		if (dev_cur != dev_prnt) {
			*eos = '\0';
			break;
		}
		rpath2 = realpath(rpath, NULL);
		if (rpath2 == NULL) {
			rc = (errno != 0) ? -errno : -EINVAL;
			goto out_err;
		}
		strncpy(rpath, rpath2, size);
		free(rpath2);
	}

	*fsb = strdup(rpath);
	free(rpath);
	return 0;

out_err:
	if (rpath != NULL) {
		free(rpath);
	}
	return rc;
}


/*
 * FUSE mount-point must be an empty-dir, which does not overlay other FUSE FS
 */
#define FS_MAGIC_FUSEBLK 0x65735546
#define FS_MAGIC_FUSECTL 0x65735543

static int statvfs_isfuse(const struct statvfs *stv)
{
	return ((stv->f_fsid == FS_MAGIC_FUSEBLK) ||
	        (stv->f_fsid == FS_MAGIC_FUSECTL));
}

int fnx_checkfusemnt(const char *path)
{
	int rc;
	struct stat st;
	struct statvfs stv;

	rc = fnx_statedir(path, &st);
	if (rc != 0) {
		return rc;
	}
	rc = fnx_statvfs(path, &stv);
	if (rc != 0) {
		return rc;
	}
	if (statvfs_isfuse(&stv)) {
		return -EOPNOTSUPP;
	}
	return 0;
}


int fnx_funexmnts(fnx_mntent_t mntent[], size_t nent, size_t *nfill)
{
	int rc;
	size_t cnt;
	fnx_mntent_t *ment;
	char const *target, *options, *mntfile;
	struct libmnt_table *tb;
	struct libmnt_iter *itr;
	struct libmnt_fs *fs;

	errno = 0;
	mntfile = "/proc/self/mountinfo";
	tb = mnt_new_table_from_file(mntfile);
	if (tb == NULL) {
		if (errno != 0) {
			rc = -errno;
		} else {
			rc = -ENOMEM;
		}
		return rc;
	}

	cnt = 0;
	itr = mnt_new_iter(MNT_ITER_FORWARD);
	while (cnt < nent) {
		rc = mnt_table_next_fs(tb, itr, &fs);
		if (rc != 0) {
			break;
		}
		if (mnt_fs_match_fstype(fs, FNX_MNTFSTYPE)) {
			target  = mnt_fs_get_target(fs);
			options = mnt_fs_get_options(fs);

			ment = &mntent[cnt++];
			ment->target  = strdup(target);
			ment->options = strdup(options);
		}
	}
	mnt_free_iter(itr);
	mnt_free_table(tb);

	*nfill = cnt;
	return 0;
}

int fnx_freemnts(fnx_mntent_t mntent[], size_t nent)
{
	fnx_mntent_t *ment;

	for (size_t i = 0; i < nent; ++i) {
		ment = &mntent[i];
		if (ment->target != NULL) {
			free(ment->target);
			ment->target = NULL;
		}
		if (ment->options != NULL) {
			free(ment->options);
			ment->options = NULL;
		}
	}
	return 0;
}





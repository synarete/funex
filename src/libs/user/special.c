/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "fnxinfra.h"
#include "fnxuser.h"


int fnx_symlink(const char *oldpath, const char *newpath)
{
	int rc;

	rc = symlink(oldpath, newpath);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_mkfifo(const char *path, mode_t mode)
{
	int rc;

	rc = mkfifo(path, mode);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_readlink(const char *path, char *buf, size_t bsz, size_t *nwr)
{
	int rc = 0;
	ssize_t res;

	res = readlink(path, buf, bsz);
	if (res == -1) {
		rc = -errno;
		*nwr = 0;
	} else {
		*nwr = (size_t)res;
	}
	return rc;
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include "fnxdefs.h"
#include "fnxinfra.h"
#include "fnxcore.h"
#include "fnxuser.h"


static int
prep_xcopy(int tgt_fd, int src_fd, size_t *src_sz, unsigned long *src_fid)
{
	int rc;
	struct stat src_st, tgt_st;
	struct statvfs src_stv, tgt_stv;

	rc = fnx_fstatvfs(src_fd, &src_stv);
	if (rc != 0) {
		return rc;
	}
	rc = fnx_fstat(src_fd, &src_st);
	if (rc != 0) {
		return rc;
	}
	rc = fnx_fstatvfs(tgt_fd, &tgt_stv);
	if (rc != 0) {
		return rc;
	}
	rc = fnx_fstat(tgt_fd, &tgt_st);
	if (rc != 0) {
		return rc;
	}
	rc = fnx_fquery(src_fd, src_fid);
	if (rc != 0) {
		return rc;
	}

	if (src_stv.f_fsid != tgt_stv.f_fsid) {
		return -EXDEV;
	}
	if (src_st.st_dev != tgt_st.st_dev) {
		return -EXDEV;
	}
	/* Hmmm.. No good with FUSE (TODO: why?)
	if (src_stv.f_fsid != FNX_FSMAGIC) {
	    return -ENOTSUP;
	}
	*/
	*src_sz  = (size_t)src_st.st_size;
	return 0;
}

static int
ioctl_xcopy(int tgt_fd, loff_t off, size_t len)
{
	int rc;
	fx_iocargs_t args;
	const fx_iocdef_t *ioc;

	ioc = fx_iocdef_by_opc(FX_OP_XCOPY);
	if ((ioc == NULL) || (ioc->size != sizeof(args))) {
		return -ENOTSUP;
	}
	memset(&args, 0, sizeof(args));
	args.xcopy_req.off  = off;
	args.xcopy_req.len  = len;

	rc = ioctl(tgt_fd, ioc->cmd, &args);
	if (rc != 0) {
		if (rc == -1) {
			rc = -errno;
		}
	}
	return rc;
}

/* XXX -- Not impl */
int fnx_fclone(int tgt_fd, int src_fd)
{
	int rc;
	size_t src_sz;
	unsigned long src_fid = 0;

	rc = prep_xcopy(tgt_fd, src_fd, &src_sz, &src_fid);
	if (rc != 0) {
		return rc;
	}
	rc = ioctl_xcopy(tgt_fd, 0, src_sz);
	if (rc != 0) {
		return rc;
	}
	return 0;
}

int fnx_fquery(int fd, unsigned long *fid)
{
	int rc;
	fx_iocargs_t args;
	const fx_iocdef_t *ioc;

	*fid = 0; /* XXX */
	ioc = fx_iocdef_by_opc(FX_OP_FQUERY);
	if (!ioc || (ioc->size != sizeof(args))) {
		return -ENOTSUP;
	}
	memset(&args, 0, sizeof(args));
	rc = ioctl(fd, ioc->cmd, &args);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>

#include "fnxinfra.h"
#include "fnxuser.h"


int fnx_access(const char *path, int mode)
{
	int rc;

	rc = access(path, mode);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_link(const char *path1, const char *path2)
{
	int rc;

	rc = link(path1, path2);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_unlink(const char *path)
{
	int rc;

	rc = unlink(path);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_rename(const char *oldpath, const char *newpath)
{
	int rc;

	rc = rename(oldpath, newpath);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_realpath(const char *path, char **resp)
{
	int rc = 0;
	char *real;

	real = realpath(path, NULL);
	if (real != NULL) {
		*resp = real;
	} else {
		rc = -errno;
	}
	return rc;
}

static void join(char *buf, const char *comps[], size_t ncomp)
{
	int sep, last;
	char *tgt;
	const char *src;

	sep = 0;
	tgt = buf;
	for (size_t i = 0; i < ncomp; ++i) {
		if ((src = comps[i]) != NULL) {
			while (*src != '\0') {
				if (*src == '/') {
					if (!sep) {
						*tgt++ = *src++;
						sep = 1;
					} else {
						src++;
					}
				} else {
					*tgt++ = *src++;
					sep = 0;
				}
			}

			last = ((i + 1) == ncomp);
			if (!sep && !last) {
				*tgt++ = '/';
				sep = 1;
			}
		}
	}
	*tgt = '\0';
}

char *fnx_makepath(const char *base, ...)
{
	va_list ap;
	char *path;
	const char *comp;
	const char *comps[256];
	size_t ncomp, length, total;
	const size_t nelem = FX_NELEMS(comps);

	ncomp = 0;
	comps[ncomp++] = base;
	total = strlen(base);

	va_start(ap, base);
	while (1) {
		comp = va_arg(ap, char *);
		if (comp == NULL) {
			break; /* OK, end-of-list */
		}
		if (ncomp == nelem) {
			errno = EINVAL;
			return NULL; /* Too many comps */
		}
		length = strlen(comp);
		if (length == 0) {
			break;
		}
		comps[ncomp++] = comp;
		total += (length + 1);
	}
	va_end(ap);

	if (total >= PATH_MAX) {
		errno = EOVERFLOW;
		return NULL;
	}

	path = (char *)malloc(total + 1);
	if (path != NULL) {
		join(path, comps, ncomp);
	}
	return path;
}

char *fnx_joinpath(const char *base, const char *comp)
{
	return fnx_makepath(base, comp, NULL);
}

void fnx_freepath(char *path)
{
	free(path);
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "fnxdefs.h"
#include "fnxuser.h"

int fnx_create(const char *path, mode_t mode, int *fd)
{
	int rc;

	rc = creat(path, mode);
	if (rc < 0) {
		*fd = -1;
		rc = -errno;
	} else {
		*fd = rc;
		rc = 0;
	}
	return rc;
}

int fnx_open(const char *path, int flags, mode_t mode, int *fd)
{
	int rc;

	rc = open(path, flags, mode);
	if (rc < 0) {
		*fd = -1;
		rc = -errno;
	} else {
		*fd = rc;
		rc = 0;
	}
	return rc;
}

int fnx_close(int fd)
{
	int rc;

	errno = 0;
	rc = close(fd);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include "fnxdefs.h"
#include "fnxuser.h"
#include "fnxinfra.h"



struct linux_dirent {
	unsigned long  d_ino;
	unsigned long  d_off;
	unsigned short d_reclen;
	char           d_name[1];
};


int fnx_getdent(int fd, loff_t off, fnx_dirent_t *dent)
{
	long rc, dt, nread;
	size_t len;
	char buf[1024];
	void *ptr = buf;
	const struct linux_dirent *d = NULL;

	memset(dent, 0, sizeof(*dent));

	if (off >= 0) {
		rc = lseek(fd, off, SEEK_SET);
		if (rc == -1) {
			return -errno;
		}
	}

	/* Read dir-entry using low-level syscall */
	nread = syscall(SYS_getdents, fd, ptr, sizeof(buf));
	if (nread == -1) {
		return -errno;
	}
	if (nread == 0) {
		return 0; /* End-of-stream */
	}
	d = (const struct linux_dirent *)ptr;
	if (d->d_reclen >= sizeof(buf)) {
		return -EINVAL;
	}
	len = strlen(d->d_name);
	if (len >= sizeof(dent->d_name)) {
		return -ENAMETOOLONG;
	}
	dt = (long)buf[d->d_reclen - 1];

	/* Convert to fnx_dirent format */
	dent->d_ino     = d->d_ino;
	dent->d_off     = (loff_t)d->d_off;
	dent->d_type    = (mode_t)DTTOIF(dt);
	memcpy(dent->d_name, d->d_name, len);

	return 0;
}

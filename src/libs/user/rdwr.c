/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/mount.h>
#include <fcntl.h>
#include <unistd.h>
#include "fnxdefs.h"
#include "fnxuser.h"


int fnx_read(int fd, void *buf, size_t cnt, size_t *nrd)
{
	int rc;
	ssize_t res;

	res = read(fd, buf, cnt);
	if (res < 0) {
		rc = -errno;
		*nrd = 0;
	} else {
		rc = 0;
		*nrd = (size_t)res;
	}
	return rc;
}

int fnx_pread(int fd, void *buf, size_t cnt, loff_t off, size_t *nrd)
{
	int rc;
	ssize_t res;

	res = pread(fd, buf, cnt, off);
	if (res < 0) {
		rc = -errno;
		*nrd = 0;
	} else {
		rc = 0;
		*nrd = (size_t)res;
	}
	return rc;
}


int fnx_write(int fd, const void *buf, size_t cnt, size_t *nwr)
{
	int rc;
	ssize_t res;

	res = write(fd, buf, cnt);
	if (res < 0) {
		rc = -errno;
		*nwr = 0;
	} else {
		rc = 0;
		*nwr = (size_t)res;
	}
	return rc;
}

int fnx_pwrite(int fd, const void *buf, size_t cnt, loff_t off, size_t *nwr)
{
	int rc;
	ssize_t res;

	res = pwrite(fd, buf, cnt, off);
	if (res < 0) {
		rc = -errno;
		*nwr = 0;
	} else {
		rc = 0;
		*nwr = (size_t)res;
	}
	return rc;
}


int fnx_lseek(int fd, loff_t off, int whence, loff_t *pos)
{
	int rc;
	loff_t res;

	res = lseek64(fd, off, whence);
	if (res == (off_t)(-1)) {
		rc = -errno;
	} else {
		rc = 0;
		*pos = res;
	}
	return rc;
}

int fnx_fsync(int fd)
{
	int rc;

	rc = fsync(fd);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_fdatasync(int fd)
{
	int rc;

	rc = fdatasync(fd);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_fallocate(int fd, int mode, loff_t off, loff_t len)
{
	int rc;

	rc = fallocate(fd, mode, off, len);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_truncate(const char *path, loff_t len)
{
	int rc;

	rc = truncate(path, len);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_ftruncate(int fd, loff_t len)
{
	int rc;

	rc = ftruncate(fd, len);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}


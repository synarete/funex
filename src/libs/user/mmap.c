/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>

#include "fnxdefs.h"
#include "fnxuser.h"


int fnx_mmap(int fd, loff_t off, size_t len, int prot, int flags, void **addr)
{
	int rc;
	void *ptr = NULL;

	ptr = mmap(NULL, len, prot, flags, fd, off);
	if (ptr == MAP_FAILED) {
		rc = -errno;
		*addr = NULL;
	} else {
		rc = 0;
		*addr = ptr;
	}
	return rc;
}

int fnx_munmap(void *addr, size_t len)
{
	int rc;

	rc = munmap(addr, len);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_msync(void *addr, size_t len, int flags)
{
	int rc;

	rc = msync(addr, len, flags);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

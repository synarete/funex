/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/mount.h>
#include <fcntl.h>
#include <unistd.h>
#include "fnxdefs.h"
#include "fnxuser.h"


static int stat_isdir(const struct stat *st)
{
	return S_ISDIR(st->st_mode);
}

static int stat_isreg(const struct stat *st)
{
	return S_ISREG(st->st_mode);
}

static int stat_isblk(const struct stat *st)
{
	return S_ISBLK(st->st_mode);
}

static int stat_isvol(const struct stat *st)
{
	return S_ISREG(st->st_mode) || S_ISBLK(st->st_mode);
}

static int stat_access(const struct stat *st, mode_t mode)
{
	mode_t mask = 0;

	if (mode & R_OK) {
		mask |= S_IRUSR | S_IRGRP | S_IROTH;
	}
	if (mode & W_OK) {
		mask |= S_IWUSR | S_IWGRP | S_IWOTH;
	}
	return ((st->st_mode & mask) != 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fnx_fstat(int fd, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fstat(fd, st);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_stat(const char *path, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = stat(path, st);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_lstat(const char *path, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = lstat(path, st);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}


int fnx_statdev(const char *path, struct stat *st, dev_t *dev)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if (rc == 0) {
		*dev = st->st_dev;
	}
	return rc;
}

int fnx_statdir(const char *path, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if ((rc == 0) && !stat_isdir(st)) {
		rc = -ENOTDIR;
	}
	return rc;
}

int fnx_statedir(const char *path, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if (rc == 0) {
		if (!stat_isdir(st)) {
			rc = -ENOTDIR;
		} else if (st->st_nlink > 2) {
			rc = -ENOTEMPTY;
		}
	}
	return rc;
}



int fnx_statreg(const char *path, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if ((rc == 0) && !stat_isreg(st)) {
		rc = -EINVAL;
	}
	return rc;
}

int fnx_statblk(const char *path, struct stat *st)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if ((rc == 0) && !stat_isblk(st)) {
		rc = -ENOTBLK;
	}
	return rc;
}

int fnx_statvol(const char *path, struct stat *st, mode_t mode)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if (rc != 0) {
		return rc;
	}
	if (!stat_isvol(st)) {
		return -ENOTBLK;
	}
	if (mode && !stat_access(st, mode)) {
		return -EACCES;
	}
	return 0;
}

static int blkgetsize(const char *path, loff_t *sz)
{
	int fd, rc = -1;
	size_t blksz = 0;

	errno = 0;
	fd = open(path, O_RDONLY, S_IRUSR);
	if (fd > 0) {
		errno = 0;
		rc = ioctl(fd, BLKGETSIZE64, &blksz);
		if (rc == 0) {
			*sz = (loff_t)blksz;
		} else {
			rc = -errno;
		}
		close(fd);
	} else {
		rc = -errno;
	}
	return rc;
}

int fnx_statsz(const char *path, struct stat *st, loff_t *sz)
{
	int rc;
	struct stat buf;

	if (st == NULL) {
		st = &buf;
	}
	rc = fnx_stat(path, st);
	if (rc != 0) {
		return rc;
	}

	*sz = st->st_size;
	if (stat_isblk(st)) {
		/* Special handling for block-device via ioctl */
		rc = blkgetsize(path, sz);
	}
	return rc;
}

int fnx_fstatvfs(int fd, struct statvfs *stv)
{
	int rc;
	struct statvfs buf;

	if (stv == NULL) {
		stv = &buf;
	}
	errno = 0;
	rc = fstatvfs(fd, stv);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}

int fnx_statvfs(const char *path, struct statvfs *stv)
{
	int rc;
	struct statvfs buf;

	if (stv == NULL) {
		stv = &buf;
	}
	errno = 0;
	rc = statvfs(path, stv);
	if (rc != 0) {
		rc = -errno;
	}
	return rc;
}




/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                       Balagan -- Hash functions library                     *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Balagan hash-functions library is a free software; you can redistribute it *
 *  and/or modify it under the terms of the GNU Lesser General Public License  *
 *  as published by the Free Software Foundation; either version 3 of the      *
 *  License, or (at your option) any later version.                            *
 *                                                                             *
 *  Balagan is distributed in the hope that it will be useful, but WITHOUT ANY *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "bconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "balagan.h"


static uint32_t rotate(uint32_t x, uint32_t r)
{
	return (x << r) | (x >> (32 - r));
}

static uint64_t fmix(uint64_t k)
{
	k ^= k >> 33;
	k *= 0xff51afd7ed558ccdULL;
	k ^= k >> 33;
	k *= 0xc4ceb9fe1a85ec53ULL;
	k ^= k >> 33;

	return k;
}

void blgn_bardak64(uint64_t in, uint64_t *out)
{
	uint64_t nn, hh;
	uint32_t k1, h1, h2;
	const uint32_t c1 = 0xcc9e2d51;
	const uint32_t c2 = 0x1b873593;

	nn = in ^ 0xc949d7c7509e6557ULL;
	h1 = h2 = 0;
	k1 = (uint32_t)nn;
	k1 *= c1;
	k1  = rotate(k1, 13);
	k1 *= c2;

	h1 ^= k1;
	h1 = rotate(h1, (nn & 0xF));
	h1 = h1 * 5 + 0xe6546b64;

	k1 = (uint32_t)(nn >> 32);
	k1 *= c1;
	k1  = rotate(k1, 23);
	k1 *= c2;

	h2 ^= k1;
	h2 = rotate(h2, (h1 & 0xF));
	h2 = h2 * 5 + 0xe6546b64;

	hh = h2;
	hh |= ((uint64_t)h1 << 32);
	*out = fmix(hh);
}

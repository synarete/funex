/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_HTBL_H_
#define FUNEX_HTBL_H_       (1)

#include <stdlib.h>

struct fx_hlink;
struct fx_htbl;

typedef size_t fx_hkey_t;
typedef struct fx_hlink fx_hlink_t;

/* Hash-function hook: converts user's key-object to unsigned hash value */
typedef fx_hkey_t (*fx_gethkey_fn)(const void *, size_t);

/* Compare hash-link with hash-obj reference */
typedef int (*fx_hcompare_fn)(const fx_hlink_t *, const void *, size_t);

/*
 * User-provided hashing element. It is up to the user to ensure this object
 * remains valid through-out the entire life-cycle of the hash-table.
 */
struct fx_hlink {
	fx_hlink_t *h_next;
	fx_hkey_t   h_key;
};


/*
 * An open addressing hash-table of user-provided linkable elements, where the
 * hash-key is long unsigned integer type.
 */
struct fx_htbl {
	size_t ht_nelems;                   /* Stored elements count */
	size_t ht_tblsize;                  /* Actual table size     */
	size_t ht_memsize;                  /* Allocated memory size */
	void  *ht_memory;                   /* Allocated memory      */
	fx_hlink_t   **ht_slots;           /* Buckets' array        */
	fx_gethkey_fn  ht_gethkey_fn;      /* Calc hash-key hook    */
	fx_hcompare_fn ht_compare_fn;      /* Compare-to hook       */
	unsigned int    ht_magic;        /* Debug checker         */
	fx_hlink_t    *ht_slots_arr[1];
};
typedef struct fx_htbl   fx_htbl_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Initialize hash-table object, using private allocated memory.
 * Return 0 in case of success, -1 in case of memory allocation failure.
 */
int fx_htbl_init(fx_htbl_t *htbl, size_t nelems, fx_hcompare_fn hcompare);

/*
 * Destroy hash-table object and release all prive allocated memory.
 * NB: It is up to the user to release elements still linked in hash-table.
 */
void fx_htbl_destroy(fx_htbl_t *htbl);


/*
 * Allocates and constructs new hash-table.
 */
fx_htbl_t *fx_htbl_new(size_t nelems, fx_hcompare_fn);

/*
 * Destruct and release resources associated with hash-table.
 * NB: It is up to the user to release elements still linked in hash-table.
 */
void fx_htbl_del(fx_htbl_t *);


/*
 * Returns the number of elements currently stored in hash-table.
 */
size_t fx_htbl_size(const fx_htbl_t *);

/*
 * Returns TRUE if no elemets in hash-table.
 */
int fx_htbl_isempty(const fx_htbl_t *);

/*
 * Insert the element x to hash-table. Hash-key for x is calculated by applying
 * hash-function on user-provided data-ref.
 */
void fx_htbl_insert(fx_htbl_t *, fx_hlink_t *, const void *, size_t);

/*
 * Search by refernce. Returns the first element whose hash-key equals given
 * data-ref. If no element found, returns NULL.
 */
fx_hlink_t *fx_htbl_lookup(const fx_htbl_t *ht, const void *, size_t);

/*
 * Removes a previously looked-up element, which is still a member of the
 * hash-table.
 */
void fx_htbl_remove(fx_htbl_t *ht, fx_hlink_t *x);


#endif /* FUNEX_HTBL_H_ */



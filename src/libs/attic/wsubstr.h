/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_WSUBSTR_H_
#define FUNEX_WSUBSTR_H_


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Sub-string: reference to characters-array. When nwr is zero, referring to
 * immutable (read-only) string. In all cases, never overlap writable region.
 * All possible dynamic-allocation must be made explicitly by the user.
 */
struct fx_wsubstr {
	wchar_t *str;  /* Beginning of characters-array (rd & wr)    */
	size_t len;    /* Number of readable chars (string's length) */
	size_t nwr;    /* Number of writable chars from beginning    */
};
typedef struct fx_wsubstr    fx_wsubstr_t;


struct fx_wsubstr_pair {
	fx_wsubstr_t first, second;
};
typedef struct fx_wsubstr_pair  fx_wsubstr_pair_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Returns the largest possible size a sub-string may have.
 */
size_t fx_wsubstr_max_size(void);

/*
 * "Not-a-pos" (synonym to fx_wsubstr_max_size())
 */
size_t fx_wsubstr_npos(void);


/*
 * Constructors:
 * The first two create read-only substrings, the next two creates a mutable
 * (for write) substring. The last one creates read-only empty string.
 */
void fx_wsubstr_init(fx_wsubstr_t *ss, const wchar_t *str);
void fx_wsubstr_init_rd(fx_wsubstr_t *ss, const wchar_t *s, size_t n);
void fx_wsubstr_init_rwa(fx_wsubstr_t *ss, wchar_t *);
void fx_wsubstr_init_rw(fx_wsubstr_t *ss, wchar_t *, size_t nrd, size_t nwr);
void fx_wsubstr_inits(fx_wsubstr_t *ss);

/*
 * Shallow-copy constructor (without deep copy).
 */
void fx_wsubstr_clone(fx_wsubstr_t *ss, const fx_wsubstr_t *other);

/*
 * Destructor: zero all
 */
void fx_wsubstr_destroy(fx_wsubstr_t *ss);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Returns the string's read-length. Synonym to ss->len.
 */
size_t fx_wsubstr_size(const fx_wsubstr_t *ss);

/*
 * Returns the writable-size of sub-string.
 */
size_t fx_wsubstr_wrsize(const fx_wsubstr_t *ss);

/*
 * Returns TRUE if sub-string's length is zero.
 */
int fx_wsubstr_isempty(const fx_wsubstr_t *ss);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns an iterator pointing to the beginning of the characters sequence.
 */
const wchar_t *fx_wsubstr_begin(const fx_wsubstr_t *ss);

/*
 * Returns an iterator pointing to the end of the characters sequence.
 */
const wchar_t *fx_wsubstr_end(const fx_wsubstr_t *ss);

/*
 * Returns the number of elements between begin() and p. If p is out-of-range,
 * returns npos.
 */
size_t fx_wsubstr_offset(const fx_wsubstr_t *ss, const wchar_t *p);

/*
 * Returns pointer to the n'th character. Performs out-of-range check:
 * panics in case n is out of range.
 */
const wchar_t *fx_wsubstr_at(const fx_wsubstr_t *substr, size_t n);

/*
 * Returns TRUE if ss->ss_str[i] is a valid substring-index (i < s->ss_len).
 */
int fx_wsubstr_isvalid_index(const fx_wsubstr_t *ss, size_t i);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 *  Copy data into buffer, return number of characters assigned. Assign
 *  no more than min(n, size()) bytes.
 *
 *  Returns the number of characters copied to buf, not including possible null
 *  termination character.
 *
 *  NB: Result buf will be null-terminated only if there is enough
 *          room (i.e. n > size()).
 */
size_t fx_wsubstr_copyto(const fx_wsubstr_t *ss, wchar_t *buf, size_t n);

/*
 * Three-way lexicographical comparison
 */
int fx_wsubstr_compare(const fx_wsubstr_t *ss, const wchar_t *s);
int fx_wsubstr_ncompare(const fx_wsubstr_t *ss, const wchar_t *s, size_t n);

/*
 * Returns TRUE in case of equal size and equal data
 */
int fx_wsubstr_isequal(const fx_wsubstr_t *ss, const wchar_t *s);
int fx_wsubstr_nisequal(const fx_wsubstr_t *ss, const wchar_t *s, size_t n);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns the number of (non-overlapping) occurrences of s (or c) as a
 * substring of ss.
 */
size_t fx_wsubstr_count(const fx_wsubstr_t *ss, const wchar_t *s);
size_t fx_wsubstr_ncount(const fx_wsubstr_t *ss,
                         const wchar_t *s, size_t n);
size_t fx_wsubstr_count_chr(const fx_wsubstr_t *ss, wchar_t c);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Searches ss, beginning at position pos, for the first occurrence of s (or
 * single-character c). If search fails, returns npos.
 */
size_t fx_wsubstr_find(const fx_wsubstr_t *ss, const wchar_t *str);
size_t fx_wsubstr_nfind(const fx_wsubstr_t *ss,
                        size_t pos, const wchar_t *s, size_t n);
size_t fx_wsubstr_find_chr(const fx_wsubstr_t *ss, size_t pos, wchar_t c);


/*
 * Searches ss backwards, beginning at position pos, for the last occurrence of
 * s (or single-character c). If search fails, returns npos.
 */
size_t fx_wsubstr_rfind(const fx_wsubstr_t *ss, const wchar_t *s);
size_t fx_wsubstr_nrfind(const fx_wsubstr_t *ss,
                         size_t pos, const wchar_t *s, size_t n);
size_t fx_wsubstr_rfind_chr(const fx_wsubstr_t *ss, size_t pos, wchar_t c);


/*
 * Searches ss, beginning at position pos, for the first character that is
 * equal to any one of the characters of s.
 */
size_t fx_wsubstr_find_first_of(const fx_wsubstr_t *ss, const wchar_t *s);
size_t fx_wsubstr_nfind_first_of(const fx_wsubstr_t *ss,
                                 size_t pos, const wchar_t *s, size_t n);


/*
 * Searches ss backwards, beginning at position pos, for the last character
 * that is equal to any of the characters of s.
 */
size_t fx_wsubstr_find_last_of(const fx_wsubstr_t *ss, const wchar_t *s);
size_t fx_wsubstr_nfind_last_of(const fx_wsubstr_t *ss,
                                size_t pos, const wchar_t *s, size_t n);


/*
 * Searches ss, beginning at position pos, for the first character that is not
 * equal to any of the characters of s.
 */
size_t fx_wsubstr_find_first_not_of(const fx_wsubstr_t *ss,
                                    const wchar_t *s);
size_t fx_wsubstr_nfind_first_not_of(const fx_wsubstr_t *ss,
                                     size_t pos, const wchar_t *s, size_t n);
size_t fx_wsubstr_find_first_not(const fx_wsubstr_t *ss,
                                 size_t pos, wchar_t c);



/*
 * Searches ss backwards, beginning at position pos, for the last character
 * that is not equal to any of the characters of s (or c).
 */
size_t fx_wsubstr_find_last_not_of(const fx_wsubstr_t *ss,
                                   const wchar_t *s);
size_t fx_wsubstr_nfind_last_not_of(const fx_wsubstr_t *ss,
                                    size_t pos, const wchar_t *s, size_t n);
size_t fx_wsubstr_find_last_not(const fx_wsubstr_t *ss,
                                size_t pos, wchar_t c);



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Creates a substring of ss, which refers to n characters after position i.
 * If i is an invalid index, the result substring is empty. If there are less
 * then n characters after position i, the result substring will refer only to
 * the elements which are members of ss.
 */
void fx_wsubstr_sub(const fx_wsubstr_t *ss,
                    size_t i, size_t n,  fx_wsubstr_t *result);

/*
 * Creates a substring of ss, which refers to the last n chars. The result
 * substring will not refer to more then ss->ss_len elements.
 */
void fx_wsubstr_rsub(const fx_wsubstr_t *ss,
                     size_t n, fx_wsubstr_t *result);

/*
 * Creates a substring with all the characters that are in the range of
 * both s1 and s2. That is, all elements within the range
 * [s1.begin(),s1.end()) which are also in the range
 * [s2.begin(), s2.end()) (i.e. have the same address).
 */
void fx_wsubstr_intersection(const fx_wsubstr_t *s1,
                             const fx_wsubstr_t *s2,
                             fx_wsubstr_t *result);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Creates a pair of substrings of ss, which are divided by any of the first
 * characters of seps. If none of the characters of seps is in ss, the first
 * first element of the result-pair is equal to ss and the second element is an
 * empty substring.
 *
 *  Examples:
 *  split("root@foo//bar", "/@:")  --> "root", "foo//bar"
 *  split("foo///:bar::zoo", ":/") --> "foo", "bar:zoo"
 *  split("root@foo.bar", ":/")    --> "root@foo.bar", ""
 */
void fx_wsubstr_split(const fx_wsubstr_t *ss,
                      const wchar_t *seps, fx_wsubstr_pair_t *);

void fx_wsubstr_nsplit(const fx_wsubstr_t *ss,
                       const wchar_t *seps, size_t n, fx_wsubstr_pair_t *);

void fx_wsubstr_split_chr(const fx_wsubstr_t *ss, wchar_t sep,
                          fx_wsubstr_pair_t *result);



/*
 * Creates a pair of substrings of ss, which are divided by any of the first n
 * characters of seps, while searching ss backwards. If none of the characters
 * of seps is in ss, the first element of the pair equal to ss and the second
 * element is an empty substring.
 */
void fx_wsubstr_rsplit(const fx_wsubstr_t *ss,
                       const wchar_t *seps, fx_wsubstr_pair_t *result);

void fx_wsubstr_nrsplit(const fx_wsubstr_t *ss,
                        const wchar_t *seps, size_t, fx_wsubstr_pair_t *);

void fx_wsubstr_rsplit_chr(const fx_wsubstr_t *ss, wchar_t sep,
                           fx_wsubstr_pair_t *result);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Creates a substring of ss without the first n leading characters.
 */
void fx_wsubstr_trim(const fx_wsubstr_t *ss, size_t n, fx_wsubstr_t *);


/*
 * Creates a substring of ss without any leading characters which are members
 * of set.
 */
void fx_wsubstr_trim_any_of(const fx_wsubstr_t *ss, const wchar_t *set,
                            fx_wsubstr_t *result);

void fx_wsubstr_ntrim_any_of(const fx_wsubstr_t *ss,
                             const wchar_t *set, size_t n, fx_wsubstr_t *);

void fx_wsubstr_trim_chr(const fx_wsubstr_t *substr, wchar_t c,
                         fx_wsubstr_t *result);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Creates a substring of ss without the last n trailing characters.
 * If n >= ss->ss_len the result substring is empty.
 */
void fx_wsubstr_chop(const fx_wsubstr_t *ss, size_t n, fx_wsubstr_t *);

/*
 * Creates a substring of ss without any trailing characters which are members
 * of set.
 */
void fx_wsubstr_chop_any_of(const fx_wsubstr_t *ss,
                            const wchar_t *set, fx_wsubstr_t *result);

void fx_wsubstr_nchop_any_of(const fx_wsubstr_t *ss,
                             const wchar_t *set, size_t n, fx_wsubstr_t *);

/*
 * Creates a substring of ss without any trailing characters that equals c.
 */
void fx_wsubstr_chop_chr(const fx_wsubstr_t *ss, wchar_t c, fx_wsubstr_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Creates a substring of ss without any leading and trailing characters which
 * are members of set.
 */
void fx_wsubstr_strip_any_of(const fx_wsubstr_t *ss,
                             const wchar_t *set, fx_wsubstr_t *result);

void fx_wsubstr_nstrip_any_of(const fx_wsubstr_t *ss,
                              const wchar_t *set, size_t n,
                              fx_wsubstr_t *result);

/*
 * Creates a substring of substr without any leading and trailing
 * characters which are equal to c.
 */
void fx_wsubstr_strip_chr(const fx_wsubstr_t *ss, wchar_t c,
                          fx_wsubstr_t *result);


/*
 * Strip white-spaces
 */
void fx_wsubstr_strip_ws(const fx_wsubstr_t *ss, fx_wsubstr_t *result);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/*
 * Finds the first substring of ss that is a token delimited by any of the
 * characters of sep(s).
 */
void fx_wsubstr_find_token(const fx_wsubstr_t *ss,
                           const wchar_t *seps, fx_wsubstr_t *result);

void fx_wsubstr_nfind_token(const fx_wsubstr_t *ss,
                            const wchar_t *seps, size_t n,
                            fx_wsubstr_t *result);

void fx_wsubstr_find_token_chr(const fx_wsubstr_t *ss, wchar_t sep,
                               fx_wsubstr_t *result);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Finds the next token in ss after tok, which is delimeted by any of the
 * characters of sep(s).
 */

void fx_wsubstr_find_next_token(const fx_wsubstr_t *ss,
                                const fx_wsubstr_t *tok,
                                const wchar_t *seps,
                                fx_wsubstr_t *result);

void fx_wsubstr_nfind_next_token(const fx_wsubstr_t *ss,
                                 const fx_wsubstr_t *tok,
                                 const wchar_t *seps, size_t n,
                                 fx_wsubstr_t *result);

void fx_wsubstr_find_next_token_chr(const fx_wsubstr_t *substr,
                                    const fx_wsubstr_t *tok, wchar_t sep,
                                    fx_wsubstr_t *result);

/*
 * Parses the substring ss into tokens, delimited by separators seps and inserts
 * them into tok_list. Inserts no more then max_sz tokens.
 *
 * Returns 0 if all tokens assigned to tok_list, or -1 in case of insufficient
 * space. If p_ntok is not NULL it is set to the number of parsed tokens.
 */
int fx_wsubstr_tokenize(const fx_wsubstr_t *ss, const wchar_t *seps,
                        fx_wsubstr_t tok_list[], size_t list_size,
                        size_t *p_ntok);

int fx_wsubstr_ntokenize(const fx_wsubstr_t *ss,
                         const wchar_t *seps, size_t n,
                         fx_wsubstr_t tok_list[], size_t list_size,
                         size_t *p_ntok);

int fx_wsubstr_tokenize_chr(const fx_wsubstr_t *ss, wchar_t sep,
                            fx_wsubstr_t tok_list[], size_t list_size,
                            size_t *p_ntok);

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Scans ss for the longest common prefix with s.
 */
size_t fx_wsubstr_common_prefix(const fx_wsubstr_t *ss, const wchar_t *str);
size_t fx_wsubstr_ncommon_prefix(const fx_wsubstr_t *ss,
                                 const wchar_t *s, size_t n);

/*
 * Return TRUE if the first character of ss equals c.
 */
int fx_wsubstr_starts_with(const fx_wsubstr_t *ss, wchar_t c);


/*
 * Scans ss backwards for the longest common suffix with s.
 */
size_t fx_wsubstr_common_suffix(const fx_wsubstr_t *ss,
                                const wchar_t *s);
size_t fx_wsubstr_ncommon_suffix(const fx_wsubstr_t *ss,
                                 const wchar_t *s, size_t n);

/*
 * Return TRUE if the last character of ss equals c.
 */
int fx_wsubstr_ends_with(const fx_wsubstr_t *ss, wchar_t c);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns pointer to beginning of characters sequence.
 */
wchar_t *fx_wsubstr_data(const fx_wsubstr_t *ss);

/*
 * Assigns s, truncates result in case of insufficient room.
 */
void fx_wsubstr_assign(fx_wsubstr_t *ss, const wchar_t *s);
void fx_wsubstr_nassign(fx_wsubstr_t *ss, const wchar_t *s, size_t len);

/*
 * Assigns n copies of c.
 */
void fx_wsubstr_assign_chr(fx_wsubstr_t *ss, size_t n, wchar_t c);


/*
 * Appends s.
 */
void fx_wsubstr_append(fx_wsubstr_t *ss, const wchar_t *s);
void fx_wsubstr_nappend(fx_wsubstr_t *ss, const wchar_t *s, size_t len);

/*
 * Appends n copies of c.
 */
void fx_wsubstr_append_chr(fx_wsubstr_t *ss, size_t n, wchar_t c);

/*
 * Appends single char.
 */
void fx_wsubstr_push_back(fx_wsubstr_t *ss, wchar_t c);

/**
 * Inserts s before position pos.
 */
void fx_wsubstr_insert(fx_wsubstr_t *ss, size_t pos, const wchar_t *s);
void fx_wsubstr_ninsert(fx_wsubstr_t *ss,
                        size_t pos, const wchar_t *s, size_t len);

/*
 * Inserts n copies of c before position pos.
 */
void fx_wsubstr_insert_chr(fx_wsubstr_t *ss,
                           size_t pos, size_t n, wchar_t c);

/*
 * Replaces a part of sub-string with the string s.
 */
void fx_wsubstr_replace(fx_wsubstr_t *ss,
                        size_t pos, size_t n, const wchar_t *s);
void fx_wsubstr_nreplace(fx_wsubstr_t *ss,
                         size_t pos, size_t n, const wchar_t *s, size_t len);

/*
 * Replaces part of sub-string with n2 copies of c.
 */
void fx_wsubstr_replace_chr(fx_wsubstr_t *ss,
                            size_t pos, size_t n1, size_t n2, wchar_t c);


/*
 * Erases part of sub-string.
 */
void fx_wsubstr_erase(fx_wsubstr_t *ss, size_t pos, size_t n);

/*
 * Reverse the writable portion of sub-string.
 */
void fx_wsubstr_reverse(fx_wsubstr_t *ss);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Generic:
 */

/*
 * Returns the index of the first element of ss that satisfy the unary
 * predicate fn (or !fn), or npos if no such element exist.
 */
size_t fx_wsubstr_find_if(const fx_wsubstr_t *ss, fx_wchr_pred fn);
size_t fx_wsubstr_find_if_not(const fx_wsubstr_t *ss, fx_wchr_pred fn);


/*
 * Returns the index of the last element of ss that satisfy the unary
 * predicate fn (or !fn), or npos if no such element exist.
 */
size_t fx_wsubstr_rfind_if(const fx_wsubstr_t *ss, fx_wchr_pred fn);
size_t fx_wsubstr_rfind_if_not(const fx_wsubstr_t *ss, fx_wchr_pred fn);

/*
 * Returns the number of elements in ss that satisfy the unary predicate fn.
 */
size_t fx_wsubstr_count_if(const fx_wsubstr_t *ss, fx_wchr_pred fn);

/*
 * Returns TRUE if all characters of ss satisfy unary predicate fn.
 */
int fx_wsubstr_test_if(const fx_wsubstr_t *ss, fx_wchr_pred fn);


/*
 * Creates a substring of ss without leading characters that satisfy unary
 * predicate fn.
 */
void fx_wsubstr_trim_if(const fx_wsubstr_t *ss,
                        fx_wchr_pred fn, fx_wsubstr_t *result);

/*
 * Creates a substring of ss without trailing characters that satisfy unary
 * predicate fn.
 */
void fx_wsubstr_chop_if(const fx_wsubstr_t *ss,
                        fx_wchr_pred fn, fx_wsubstr_t *result);

/*
 * Creates a substring of ss without any leading and trailing characters that
 * satisfy unary predicate fn.
 */
void fx_wsubstr_strip_if(const fx_wsubstr_t *ss,
                         fx_wchr_pred fn, fx_wsubstr_t *result);


/*
 * Apply fn for every element in sub-string.
 */
void fx_wsubstr_foreach(fx_wsubstr_t *ss, fx_wchr_modify_fn fn);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Returns TRUE if all characters of ss satisfy ctype predicate.
 */
int fx_wsubstr_isalnum(const fx_wsubstr_t *ss);
int fx_wsubstr_isalpha(const fx_wsubstr_t *ss);
int fx_wsubstr_isascii(const fx_wsubstr_t *ss);
int fx_wsubstr_isblank(const fx_wsubstr_t *ss);
int fx_wsubstr_iscntrl(const fx_wsubstr_t *ss);
int fx_wsubstr_isdigit(const fx_wsubstr_t *ss);
int fx_wsubstr_isgraph(const fx_wsubstr_t *ss);
int fx_wsubstr_islower(const fx_wsubstr_t *ss);
int fx_wsubstr_isprint(const fx_wsubstr_t *ss);
int fx_wsubstr_ispunct(const fx_wsubstr_t *ss);
int fx_wsubstr_isspace(const fx_wsubstr_t *ss);
int fx_wsubstr_isupper(const fx_wsubstr_t *ss);
int fx_wsubstr_isxdigit(const fx_wsubstr_t *ss);

/*
 * Case sensitive operations:
 */
void fx_wsubstr_toupper(fx_wsubstr_t *substr);
void fx_wsubstr_tolower(fx_wsubstr_t *substr);
void fx_wsubstr_capitalize(fx_wsubstr_t *substr);

#endif /* FUNEX_WSUBSTR_H_ */



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_VECTOR_H_
#define FUNEX_VECTOR_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * A sequence-container that supports random access to elements, constant time
 * insertion and removal of elements at the end, and linear time insertion and
 * removal of elements at the beginning or in the middle. Holds fixed-size
 * elements, which are a copy of user-inserted objects, stored in dynamic
 * allocated array.
 */
struct fx_vector {
	fx_malloc_t vec_alloc;   /* Memory allocator           */
	size_t vec_elemsize;     /* Size of single user object */
	size_t vec_size;         /* Current number of elements */
	size_t vec_capacity;     /* Max number of elements     */
	void *vec_data;          /* Elements-array             */
};
typedef struct fx_vector        fx_vector_t;


/*: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : :*/
/*
 * Creates empty vector object, associated with user's allocator. If alloc is
 * NULL, using default allocator.
 */
void fx_vector_init(fx_vector_t *vec, size_t elem_sz,
                    const fx_malloc_t *alloc);

/*
 * Releases any resources associated with vector object.
 */
void fx_vector_destroy(fx_vector_t *vec);


/*
 * Returns the number of elements stored in vec.
 */
size_t fx_vector_size(const fx_vector_t *vec);


/*
 * Number of elements for which memory has been allocated (always greater
 * than or equal to size).
 */
size_t fx_vector_capacity(const fx_vector_t *vec);


/*
 * Returns TRUE is vec size is zero.
 */
int fx_vector_isempty(const fx_vector_t *vec);


/*
 * Returns the i-th element, or NULL if out of range.
 */
void *fx_vector_at(const fx_vector_t *vec, size_t i);


/*
 * Returns the underlying memory region associated with vec.
 */
void *fx_vector_data(const fx_vector_t *vec);


/*
 * Set size to zero (but without deallocation).
 */
void fx_vector_clear(fx_vector_t *vec);


/*
 * Copy the content of vec to mem. Copies no more then len bytes.
 *
 * Returns the number of bytes copied to mem.
 */
size_t fx_vector_copyto(const fx_vector_t *vec,
                        void *mem, size_t len);

/**
 * Forces capacity of at-least n elements (manual reallocation).
 *
 * Returns pointer to beginning of data, or NULL pointer in case of
 * allocation failure.
 */
void *fx_vector_reserve(fx_vector_t *vec, size_t n);


/*
 * Inserts a copy of element at the end.
 *
 * Returns pointer to the newly inserted element, or NULL if full (in which
 * user need to do explicit reallocation with reserve).
 */
void *fx_vector_push_back(fx_vector_t *vec, const void *element);


/*
 * Inserts a copy of element at the end, do reallocation if full.
 *
 * Returns pointer to the newly inserted element, or NULL in case of
 * allocation failure.
 */
void *fx_vector_xpush_back(fx_vector_t *vec, const void *element);

/*
 * Removes the last element, if any.
 */
void fx_vector_pop_back(fx_vector_t *vec);


/*
 * Inserts element before position pos.
 *
 * Returns pointer to the newly inserted element, or NULL if full.
 */
void *fx_vector_insert(fx_vector_t *vec,
                       size_t pos, const void *element);

/*
 * Inserts element before position pos.
 *
 * Returns pointer to the newly inserted element, or NULL in case of
 * allocation failure.
 */
void *fx_vector_xinsert(fx_vector_t *vec,
                        size_t pos, const void *element);

/*
 * Erases the element at position pos.
 */
void fx_vector_erase(fx_vector_t *vec, size_t pos);


#endif /* FUNEX_VECTOR_H_ */

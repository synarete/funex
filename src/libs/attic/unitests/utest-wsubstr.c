/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include "fnxinfra.h"

#define NELEMS(x)       FX_NELEMS(x)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_compare(fx_wsubstr_t *ss)
{
	int cmp, eq, emp;
	size_t sz;

	fx_wsubstr_init(ss, L"123456789");
	sz = fx_wsubstr_size(ss);
	fx_assert(sz == 9);

	emp = fx_wsubstr_isempty(ss);
	fx_assert(!emp);

	cmp = fx_wsubstr_compare(ss, L"123");
	fx_assert(cmp > 0);

	cmp = fx_wsubstr_compare(ss, L"9");
	fx_assert(cmp < 0);

	cmp = fx_wsubstr_compare(ss, L"123456789");
	fx_assert(cmp == 0);

	eq = fx_wsubstr_isequal(ss, L"123456789");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find(fx_wsubstr_t *ss)
{
	size_t n, pos, len;

	fx_wsubstr_init(ss, L"ABCDEF abcdef ABCDEF");

	n = fx_wsubstr_find(ss, L"A");
	fx_assert(n == 0);

	n = fx_wsubstr_find(ss, L"EF");
	fx_assert(n == 4);

	pos = 10;
	len = 2;
	n = fx_wsubstr_nfind(ss, pos, L"EF", len);
	fx_assert(n == 18);

	pos = 1;
	n = fx_wsubstr_find_chr(ss, pos, 'A');
	fx_assert(n == 14);

	n = fx_wsubstr_find(ss, L"XXX");
	fx_assert(n > fx_wsubstr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_rfind(fx_wsubstr_t *ss)
{
	size_t n, pos, len;

	fx_wsubstr_init(ss, L"ABRACADABRA");


	n = fx_wsubstr_rfind(ss, L"A");
	fx_assert(n == 10);

	n = fx_wsubstr_rfind(ss, L"BR");
	fx_assert(n == 8);

	pos = fx_wsubstr_size(ss) / 2;
	len = 2;
	n = fx_wsubstr_nrfind(ss, pos, L"BR", len);
	fx_assert(n == 1);

	pos = 1;
	n = fx_wsubstr_rfind_chr(ss, pos, 'B');
	fx_assert(n == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_first_of(fx_wsubstr_t *ss)
{
	size_t n, pos;

	fx_wsubstr_init(ss, L"012x456x89z");

	n = fx_wsubstr_find_first_of(ss, L"xyz");
	fx_assert(n == 3);

	pos = 5;
	n = fx_wsubstr_nfind_first_of(ss, pos, L"x..z", 4);
	fx_assert(n == 7);

	n = fx_wsubstr_find_first_of(ss, L"XYZ");
	fx_assert(n > fx_wsubstr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_last_of(fx_wsubstr_t *ss)
{
	size_t n, pos;

	fx_wsubstr_init(ss, L"AAAAA-BBBBB");

	n = fx_wsubstr_find_last_of(ss, L"xyzAzyx");
	fx_assert(n == 4);

	pos = 9;
	n = fx_wsubstr_nfind_last_of(ss, pos, L"X-Y", 3);
	fx_assert(n == 5);

	n = fx_wsubstr_find_last_of(ss, L"BBBBBBBBBBBBBBBBBBBBB");
	fx_assert(n == (fx_wsubstr_size(ss) - 1));

	n = fx_wsubstr_find_last_of(ss, L"...");
	fx_assert(n > fx_wsubstr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_first_not_of(fx_wsubstr_t *ss)
{
	size_t n, pos;

	fx_wsubstr_init(ss, L"aaa bbb ccc * ddd + eee");

	n = fx_wsubstr_find_first_not_of(ss, L"a b c d e");
	fx_assert(n == 12);

	pos = 14;
	n = fx_wsubstr_nfind_first_not_of(ss, pos, L"d e", 3);
	fx_assert(n == 18);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_last_not_of(fx_wsubstr_t *ss)
{
	size_t n, pos;

	fx_wsubstr_init(ss, L"-..3456.--");

	n = fx_wsubstr_find_last_not_of(ss, L".-");
	fx_assert(n == 6);

	pos = 1;
	n = fx_wsubstr_nfind_last_not_of(ss, pos, L"*", 1);
	fx_assert(n == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_sub(fx_wsubstr_t *ss)
{
	int rc;
	fx_wsubstr_t sub;
	const wchar_t *abc;

	abc = L"abcdefghijklmnopqrstuvwxyz";

	fx_wsubstr_init_rd(ss, abc, 10);    /* L"abcdefghij" */
	fx_wsubstr_sub(ss, 2, 4, &sub);
	rc  = fx_wsubstr_isequal(&sub, L"cdef");
	fx_assert(rc == 1);

	fx_wsubstr_rsub(ss, 3, &sub);
	rc  = fx_wsubstr_isequal(&sub, L"hij");
	fx_assert(rc == 1);

	fx_wsubstr_chop(ss, 8, &sub);
	rc  = fx_wsubstr_isequal(&sub, L"ab");
	fx_assert(rc == 1);

	fx_wsubstr_clone(&sub, ss);
	rc  = fx_wsubstr_nisequal(&sub, ss->str, ss->len);
	fx_assert(rc == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_count(fx_wsubstr_t *ss)
{
	size_t n;

	fx_wsubstr_init(ss, L"xxx-xxx-xxx-xxx");

	n = fx_wsubstr_count(ss, L"xxx");
	fx_assert(n == 4);

	n = fx_wsubstr_count_chr(ss, '-');
	fx_assert(n == 3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_split(fx_wsubstr_t *ss)
{
	int eq;
	fx_wsubstr_pair_t split;

	fx_wsubstr_init(ss, L"ABC-DEF+123");
	fx_wsubstr_split(ss, L"-", &split);

	eq = fx_wsubstr_isequal(&split.first, L"ABC");
	fx_assert(eq);

	eq = fx_wsubstr_isequal(&split.second, L"DEF+123");
	fx_assert(eq);

	fx_wsubstr_split(ss, L" + * L", &split);
	eq = fx_wsubstr_isequal(&split.first, L"ABC-DEF");
	fx_assert(eq);

	eq = fx_wsubstr_isequal(&split.second, L"123");
	fx_assert(eq);


	fx_wsubstr_split_chr(ss, 'B', &split);
	eq = fx_wsubstr_isequal(&split.first, L"A");
	fx_assert(eq);

	eq = fx_wsubstr_isequal(&split.second, L"C-DEF+123");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_rsplit(fx_wsubstr_t *ss)
{
	int eq;
	fx_wsubstr_pair_t split;

	fx_wsubstr_init(ss, L"XXX--YYY--ZZZ");
	fx_wsubstr_rsplit(ss, L"-.", &split);

	eq = fx_wsubstr_isequal(&split.first, L"XXX--YYY");
	fx_assert(eq);

	eq = fx_wsubstr_isequal(&split.second, L"ZZZ");
	fx_assert(eq);


	fx_wsubstr_rsplit(ss, L"+", &split);

	eq = fx_wsubstr_nisequal(&split.first, ss->str, ss->len);
	fx_assert(eq);

	eq = fx_wsubstr_isequal(&split.second, L"ZZZ");
	fx_assert(!eq);


	fx_wsubstr_init(ss, L"1.2.3.4.5");
	fx_wsubstr_rsplit_chr(ss, '.', &split);

	eq = fx_wsubstr_isequal(&split.first, L"1.2.3.4");
	fx_assert(eq);

	eq = fx_wsubstr_isequal(&split.second, L"5");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_trim(fx_wsubstr_t *ss)
{
	int eq;
	fx_wsubstr_t sub;

	fx_wsubstr_init(ss, L".:ABCD");

	fx_wsubstr_trim_any_of(ss, L":,.%^", &sub);
	eq  = fx_wsubstr_isequal(&sub, L"ABCD");
	fx_assert(eq);

	fx_wsubstr_ntrim_any_of(ss,
	                        fx_wsubstr_data(ss),
	                        fx_wsubstr_size(ss),
	                        &sub);
	eq  = fx_wsubstr_size(&sub) == 0;
	fx_assert(eq);

	fx_wsubstr_trim_chr(ss, '.', &sub);
	eq  = fx_wsubstr_isequal(&sub, L":ABCD");
	fx_assert(eq);

	fx_wsubstr_trim(ss, 4, &sub);
	eq  = fx_wsubstr_isequal(&sub, L"CD");
	fx_assert(eq);

	fx_wsubstr_trim_if(ss, fx_wchr_ispunct, &sub);
	eq  = fx_wsubstr_isequal(&sub, L"ABCD");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_chop(fx_wsubstr_t *ss)
{
	int eq;
	fx_wsubstr_t sub;

	fx_wsubstr_init(ss, L"123....");

	fx_wsubstr_chop_any_of(ss, L"+*&^%$.", &sub);
	eq  = fx_wsubstr_isequal(&sub, L"123");
	fx_assert(eq);

	fx_wsubstr_nchop_any_of(ss,
	                        fx_wsubstr_data(ss),
	                        fx_wsubstr_size(ss),
	                        &sub);
	eq  = fx_wsubstr_isequal(&sub, L"");
	fx_assert(eq);

	fx_wsubstr_chop(ss, 6, &sub);
	eq  = fx_wsubstr_isequal(&sub, L"1");
	fx_assert(eq);

	fx_wsubstr_chop_chr(ss, '.', &sub);
	eq  = fx_wsubstr_isequal(&sub, L"123");
	fx_assert(eq);

	fx_wsubstr_chop_if(ss, fx_wchr_ispunct, &sub);
	eq  = fx_wsubstr_isequal(&sub, L"123");
	fx_assert(eq);

	fx_wsubstr_chop_if(ss, fx_wchr_isprint, &sub);
	eq  = fx_wsubstr_size(&sub) == 0;
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_strip(fx_wsubstr_t *ss)
{
	int eq;
	size_t sz;
	const wchar_t *s;
	fx_wsubstr_t sub;

	fx_wsubstr_init(ss, L".....#XYZ#.........");

	fx_wsubstr_strip_any_of(ss, L"-._#", &sub);
	eq  = fx_wsubstr_isequal(&sub, L"XYZ");
	fx_assert(eq);

	fx_wsubstr_strip_chr(ss, '.', &sub);
	eq  = fx_wsubstr_isequal(&sub, L"#XYZ#");
	fx_assert(eq);

	fx_wsubstr_strip_if(ss, fx_wchr_ispunct, &sub);
	eq  = fx_wsubstr_isequal(&sub, L"XYZ");
	fx_assert(eq);

	s  = fx_wsubstr_data(ss);
	sz = fx_wsubstr_size(ss);
	fx_wsubstr_nstrip_any_of(ss, s, sz, &sub);
	eq  = fx_wsubstr_isequal(&sub, L"");
	fx_assert(eq);

	fx_wsubstr_init(ss, L" \t ABC\n\r\v");
	fx_wsubstr_strip_ws(ss, &sub);
	eq = fx_wsubstr_isequal(&sub, L"ABC");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_token(fx_wsubstr_t *ss)
{
	int eq;
	fx_wsubstr_t tok;
	const wchar_t *seps;

	seps   = L" \t\n\v\r";
	fx_wsubstr_init(ss, L" A BB \t  CCC    DDDD  \n");

	fx_wsubstr_find_token(ss, seps, &tok);
	eq  = fx_wsubstr_isequal(&tok, L"A");
	fx_assert(eq);

	fx_wsubstr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_wsubstr_isequal(&tok, L"BB");
	fx_assert(eq);

	fx_wsubstr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_wsubstr_isequal(&tok, L"CCC");
	fx_assert(eq);

	fx_wsubstr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_wsubstr_isequal(&tok, L"DDDD");
	fx_assert(eq);

	fx_wsubstr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_wsubstr_isequal(&tok, L"");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tokenize(fx_wsubstr_t *ss)
{
	int rc, eq;
	size_t n_toks;
	fx_wsubstr_t toks_list[7];
	const wchar_t *seps = L" /:;.| L" ;
	const wchar_t *line;

	line = L"    /Ant:::Bee;:Cat:Dog;...Elephant.../Frog:/Giraffe///    L";
	fx_wsubstr_init(ss, line);

	rc = fx_wsubstr_tokenize(ss, seps, toks_list, 7, &n_toks);
	fx_assert(rc == 0);
	fx_assert(n_toks == 7);

	eq  = fx_wsubstr_isequal(&toks_list[0], L"Ant");
	fx_assert(eq);

	eq  = fx_wsubstr_isequal(&toks_list[4], L"Elephant");
	fx_assert(eq);

	eq  = fx_wsubstr_isequal(&toks_list[6], L"Giraffe");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_case(fx_wsubstr_t *ss)
{
	int eq;
	wchar_t buf[20] = L"0123456789abcdef";

	fx_wsubstr_init_rwa(ss, buf);

	fx_wsubstr_toupper(ss);
	eq  = fx_wsubstr_isequal(ss, L"0123456789ABCDEF");
	fx_assert(eq);

	fx_wsubstr_tolower(ss);
	eq  = fx_wsubstr_isequal(ss, L"0123456789abcdef");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_common_prefix(fx_wsubstr_t *ss)
{
	int eq, n;
	wchar_t buf1[] = L"0123456789abcdef";

	fx_wsubstr_init(ss, buf1);

	n = (int) fx_wsubstr_common_prefix(ss, L"0123456789ABCDEF");
	eq = (n == 10);
	fx_assert(eq);

	n = (int) fx_wsubstr_common_prefix(ss, buf1);
	eq = (n == 16);
	fx_assert(eq);

	n = (int) fx_wsubstr_common_prefix(ss, L"XYZ");
	eq = (n == 0);
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_common_suffix(fx_wsubstr_t *ss)
{
	int eq, n;
	wchar_t buf1[] = L"abcdef0123456789";

	fx_wsubstr_init(ss, buf1);

	n = (int) fx_wsubstr_common_suffix(ss, L"ABCDEF0123456789");
	eq = (n == 10);
	fx_assert(eq);

	n = (int) fx_wsubstr_common_suffix(ss, buf1);
	eq = (n == 16);
	fx_assert(eq);

	n = (int) fx_wsubstr_common_suffix(ss, L"XYZ");
	eq = (n == 0);
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_assign(fx_wsubstr_t *ss)
{
	int eq, n;
	fx_wsubstr_t sub;
	wchar_t buf1[] = L"0123456789......";
	const wchar_t *s;

	fx_wsubstr_init_rw(ss, buf1, 10, 16);
	fx_wsubstr_sub(ss, 10, 6, &sub);

	n = (int)fx_wsubstr_size(&sub);
	eq = (n == 0);
	fx_assert(eq);

	n = (int)fx_wsubstr_wrsize(&sub);
	eq = (n == 6);
	fx_assert(eq);

	s = L"ABC";
	fx_wsubstr_assign(ss, s);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 3);
	n = (int)fx_wsubstr_wrsize(ss);
	fx_assert(n == 16);

	eq = fx_wsubstr_isequal(ss, s);
	fx_assert(eq);

	s = L"ABCDEF";
	fx_wsubstr_assign(&sub, s);
	eq = fx_wsubstr_isequal(&sub, s);
	fx_assert(eq);

	s = L"ABCDEF$$$";
	fx_wsubstr_assign(&sub, s);
	n = (int)fx_wsubstr_size(&sub);
	fx_assert(n == 6);
	n = (int)fx_wsubstr_wrsize(&sub);
	fx_assert(n == 6);
	eq = fx_wsubstr_isequal(&sub, s);
	fx_assert(!eq);

	fx_wsubstr_sub(&sub, 5, 100, &sub);
	s = L"XYZ";
	fx_wsubstr_assign(&sub, s);
	n = (int)fx_wsubstr_size(&sub);
	fx_assert(n == 1);
	n = (int)fx_wsubstr_wrsize(&sub);
	fx_assert(n == 1);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_append(fx_wsubstr_t *ss)
{
	int eq, n;
	wchar_t buf[20];
	const wchar_t *s;

	fx_wsubstr_init_rw(ss, buf, 0, NELEMS(buf));
	s = L"0123456789abcdef";

	fx_wsubstr_append(ss, s);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 16);
	n = (int)fx_wsubstr_wrsize(ss);
	fx_assert(n == NELEMS(buf));
	eq = fx_wsubstr_isequal(ss, s);
	fx_assert(eq);

	fx_wsubstr_append(ss, s);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == NELEMS(buf));
	n = (int)fx_wsubstr_wrsize(ss);
	fx_assert(n == NELEMS(buf));

	fx_wsubstr_init_rw(ss, buf, 0, NELEMS(buf));
	s = L"01";
	fx_wsubstr_nappend(ss, s, 1);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 1);
	fx_wsubstr_nappend(ss, s + 1, 1);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 2);
	eq = fx_wsubstr_isequal(ss, s);
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_insert(fx_wsubstr_t *ss)
{
	int eq, n;
	const wchar_t *s;
	wchar_t buf[20];

	fx_wsubstr_init_rw(ss, buf, 0, NELEMS(buf));

	s = L"0123456789";
	fx_wsubstr_insert(ss, 0, s);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 10);
	eq = fx_wsubstr_isequal(ss, s);
	fx_assert(eq);

	fx_wsubstr_insert(ss, 10, s);
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 20);
	eq = fx_wsubstr_isequal(ss, L"01234567890123456789");
	fx_assert(eq);

	fx_wsubstr_insert(ss, 1, L"....");
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 20);
	eq = fx_wsubstr_isequal(ss, L"0....123456789012345");
	fx_assert(eq);

	fx_wsubstr_insert(ss, 16, L"%%%");
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 20);
	eq = fx_wsubstr_isequal(ss, L"0....12345678901%%%2");
	fx_assert(eq);

	fx_wsubstr_insert_chr(ss, 1, 20, '$');
	n = (int)fx_wsubstr_size(ss);
	fx_assert(n == 20);
	eq = fx_wsubstr_isequal(ss, L"0$$$$$$$$$$$$$$$$$$$");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_replace(fx_wsubstr_t *ss)
{
	int eq, n;
	const wchar_t *s;
	wchar_t buf[10];

	fx_wsubstr_init_rw(ss, buf, 0, NELEMS(buf));

	s = L"ABCDEF";
	fx_wsubstr_replace(ss, 0, 2, s);
	n = (int) fx_wsubstr_size(ss);
	fx_assert(n == 6);
	eq = fx_wsubstr_isequal(ss, s);
	fx_assert(eq);

	fx_wsubstr_replace(ss, 1, 2, s);
	eq = fx_wsubstr_isequal(ss, L"AABCDEFDEF");
	fx_assert(eq);

	fx_wsubstr_replace(ss, 6, 3, s);
	eq = fx_wsubstr_isequal(ss, L"AABCDEABCD");
	fx_assert(eq);

	fx_wsubstr_replace_chr(ss, 0, 10, 30, '.');
	eq = fx_wsubstr_isequal(ss, L"..........");
	fx_assert(eq);

	fx_wsubstr_replace_chr(ss, 1, 8, 4, 'A');
	eq = fx_wsubstr_isequal(ss, L".AAAA.");
	fx_assert(eq);

	fx_wsubstr_nreplace(ss, 2, 80,
	                    fx_wsubstr_data(ss),
	                    fx_wsubstr_size(ss));
	eq = fx_wsubstr_isequal(ss, L".A.AAAA.");
	fx_assert(eq);

	fx_wsubstr_nreplace(ss, 4, 80,
	                    fx_wsubstr_data(ss),
	                    fx_wsubstr_size(ss));
	eq = fx_wsubstr_isequal(ss, L".A.A.A.AAA");  /* Truncated */
	fx_assert(eq);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_erase(fx_wsubstr_t *ss)
{
	int n, eq;
	wchar_t buf[5];

	fx_wsubstr_init_rw(ss, buf, 0, NELEMS(buf));

	fx_wsubstr_assign(ss, L"ABCDEF");
	eq = fx_wsubstr_isequal(ss, L"ABCDE");
	fx_assert(eq);

	fx_wsubstr_erase(ss, 1, 2);
	eq = fx_wsubstr_isequal(ss, L"ADE");
	fx_assert(eq);

	fx_wsubstr_erase(ss, 0, 100);
	eq = fx_wsubstr_isequal(ss, L"");
	fx_assert(eq);

	n = (int) fx_wsubstr_wrsize(ss);
	fx_assert(n == NELEMS(buf));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_reverse(fx_wsubstr_t *ss)
{
	int eq;
	wchar_t buf[40];

	fx_wsubstr_init_rw(ss, buf, 0, NELEMS(buf));
	fx_wsubstr_assign(ss, L"abracadabra");
	fx_wsubstr_reverse(ss);

	eq = fx_wsubstr_isequal(ss, L"arbadacarba");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_copyto(fx_wsubstr_t *ss)
{
	int eq;
	wchar_t buf[10];
	wchar_t pad = '@';
	size_t sz;

	fx_wsubstr_init(ss, L"123456789");
	sz = fx_wsubstr_size(ss);
	fx_assert(sz == 9);

	sz = fx_wsubstr_copyto(ss, buf, sizeof(buf));
	fx_assert(sz == 9);

	eq = !wcscmp(buf, L"123456789");
	fx_assert(eq);
	fx_assert(pad == '@');
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
int main(void)
{
	fx_wsubstr_t wsubstr_obj;
	fx_wsubstr_t *ss = &wsubstr_obj;

	test_compare(ss);
	test_find(ss);
	test_rfind(ss);
	test_find_first_of(ss);
	test_find_last_of(ss);
	test_find_first_not_of(ss);
	test_find_last_not_of(ss);
	test_sub(ss);
	test_count(ss);
	test_split(ss);
	test_rsplit(ss);
	test_trim(ss);
	test_chop(ss);
	test_strip(ss);
	test_find_token(ss);
	test_tokenize(ss);
	test_case(ss);
	test_common_prefix(ss);
	test_common_suffix(ss);

	test_assign(ss);
	test_append(ss);
	test_insert(ss);
	test_replace(ss);
	test_erase(ss);
	test_reverse(ss);
	test_copyto(ss);

	fx_wsubstr_destroy(ss);

	return EXIT_SUCCESS;
}



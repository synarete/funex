/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "fnxinfra.h"
#include "randutil.h"

struct hash_elem {
	fx_hlink_t hlink;
	int key, value;
};
typedef struct hash_elem        hash_elem_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int key_to_value(int key)
{
	return (key * key);
}

static hash_elem_t *make_elem(int key)
{
	hash_elem_t *elem = NULL;

	elem = (hash_elem_t *)fx_malloc(sizeof(*elem));
	fx_assert(elem != NULL);

	elem->key   = key;
	elem->value = key_to_value(key);
	return elem;
}

static void check_elem(const hash_elem_t *elem)
{
	fx_assert(elem != NULL);
	fx_assert(elem->value == key_to_value(elem->key));
}

static void free_elem(hash_elem_t *elem)
{
	check_elem(elem);
	fx_free(elem, sizeof(*elem));
}

static fx_hlink_t *elem_to_hlink(const hash_elem_t *elem)
{
	const fx_hlink_t *lnk = &elem->hlink;
	return (fx_hlink_t *)lnk;
}

static hash_elem_t *hlink_to_elem(const fx_hlink_t *lnk)
{
	const hash_elem_t *elem = fx_container_of(lnk, hash_elem_t, hlink);
	return (hash_elem_t *)elem;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int elem_hcompare(const fx_hlink_t *hlnk, const void *ref, size_t len)
{
	int key;
	const hash_elem_t *elem;

	elem = hlink_to_elem(hlnk);
	key  = *(const int *)ref;

	return ((elem->key == key) && (len == sizeof(key))) ? 0 : -1;
}

static void
test_hashtable_insert(fx_htbl_t *htbl, const int *keys, size_t nelems)
{
	size_t i, sz, len;
	int key;
	const void *ref;
	hash_elem_t *elem  = NULL;
	fx_hlink_t *hlnk  = NULL;
	hash_elem_t *elem2 = NULL;

	for (i = 0; i < nelems; ++i) {
		key = keys[i];
		ref = &key;
		len = sizeof(key);

		elem = make_elem(key);
		check_elem(elem);

		hlnk = elem_to_hlink(elem);
		fx_htbl_insert(htbl, hlnk, &elem->key, sizeof(elem->key));

		hlnk = fx_htbl_lookup(htbl, ref, len);
		fx_assert(hlnk != NULL);
		elem2 = hlink_to_elem(hlnk);
		check_elem(elem2);
		fx_assert(elem == elem2);

		sz = fx_htbl_size(htbl);
		fx_assert(sz == (i + 1));
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
test_hashtable_lookup(const fx_htbl_t *htbl, const int *keys, size_t nelems)
{
	size_t i;
	int key;
	const hash_elem_t *elem = NULL;
	const fx_hlink_t *hlnk  = NULL;

	for (i = 0; i < nelems; ++i) {
		key  = keys[i];

		hlnk  = fx_htbl_lookup(htbl, &key, sizeof(key));
		fx_assert(hlnk != NULL);
		elem = hlink_to_elem(hlnk);
		check_elem(elem);
		fx_assert(elem->key == key);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
test_hashtable_remove(fx_htbl_t *ht, const int *keys, size_t nelems)
{
	size_t i, sz;
	int key;
	hash_elem_t *elem  = NULL;
	fx_hlink_t *hlnk  = NULL;

	for (i = 0; i < nelems; ++i) {
		key = keys[i];

		hlnk = fx_htbl_lookup(ht, &key, sizeof(key));
		fx_assert(hlnk != NULL);

		elem  = hlink_to_elem(hlnk);
		check_elem(elem);
		fx_assert(elem->key == key);

		fx_htbl_remove(ht, hlnk);
		free_elem(elem);

		sz = fx_htbl_size(ht);
		fx_assert(sz == (nelems - i - 1));
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void
test_hashtable(int *keys, size_t nelems, struct random_data *rnd_dat)
{
	int rc;
	fx_htbl_t htbl_obj;
	fx_htbl_t *ht = &htbl_obj;

	rc = fx_htbl_init(ht, nelems, elem_hcompare);
	fx_assert(rc == 0);

	test_hashtable_insert(ht, keys, nelems);
	test_hashtable_lookup(ht, keys, nelems);
	random_shuffle(keys, nelems, rnd_dat);
	test_hashtable_lookup(ht, keys, nelems);
	random_shuffle(keys, nelems, rnd_dat);
	test_hashtable_remove(ht, keys, nelems);

	fx_htbl_destroy(ht);
}

int main(int argc, char *argv[])
{
	int rc;
	size_t size, nkeys, nn;
	int *keys;
	struct random_data *rnd_dat;

	rnd_dat = create_randomizer();

	nkeys = 601;
	if (argc > 1) {
		rc = sscanf(argv[1], "%zu", &nn);
		if ((rc == 1) && (nn > 0)) {
			nkeys = nn;
		}
	}

	size  = nkeys * sizeof(keys[0]);
	keys  = (int *)malloc(size);
	fx_assert(keys != NULL);

	generate_keys(keys, nkeys, 1); /* Unique keys */
	random_shuffle(keys, nkeys, rnd_dat);
	test_hashtable(keys, nkeys, rnd_dat);

	generate_keys(keys, nkeys, 7); /* Non-unique */
	random_shuffle(keys, nkeys, rnd_dat);
	test_hashtable(keys, nkeys, rnd_dat);

	free(keys);

	return EXIT_SUCCESS;
}



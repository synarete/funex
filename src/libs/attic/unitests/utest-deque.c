/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include "fnxinfra.h"


#define MAGIC           (2011UL)
#define MAX_SIZE        (1024)

struct msg_ {
	size_t key;
	size_t magic;
	char value[51];
};
typedef struct msg_ msg_t;


static void msg_init(msg_t *msg, size_t key)
{
	fx_bzero(msg, sizeof(*msg));

	msg->key    = key;
	msg->magic  = MAGIC + key;
	snprintf(msg->value, sizeof(msg->value), "%ld", (long)key);
}

static void msg_check(const msg_t *msg, size_t key)
{
	int cmp;
	char buf[100];

	snprintf(buf, sizeof(buf), "%ld", (long)key);

	fx_assert(msg->key == key);
	fx_assert(msg->magic == (MAGIC + key));

	cmp = strcmp(buf, msg->value);
	fx_assert(cmp == 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_deque_ops1(fx_deque_t *deque)
{
	int rc;
	size_t i, key, nelems, sz;
	msg_t msg;
	const msg_t *p_msg = NULL;

	nelems = fx_deque_maxsize(deque);
	fx_assert((nelems % 2) == 0);

	for (i = 0; i < nelems; ++i) {
		key = i;

		rc = fx_deque_isfull(deque);
		fx_assert(!rc);

		msg_init(&msg, key);
		fx_deque_push_back(deque, &msg);
		p_msg = (const msg_t *)fx_deque_back(deque);
		fx_assert(p_msg != NULL);

		msg_check(p_msg, key);
	}

	for (i = 0; i < nelems; ++i) {
		key = i;
		p_msg = (const msg_t *)fx_deque_at(deque, i);
		msg_check(p_msg, key);
	}

	sz = fx_deque_size(deque);
	fx_assert(sz == nelems);

	key = 0;
	while (sz >= 2) {
		p_msg = (const msg_t *)fx_deque_front(deque);
		fx_assert(p_msg != NULL);
		msg_check(p_msg, key);

		fx_deque_pop_back(deque);
		fx_deque_pop_front(deque);
		sz = fx_deque_size(deque);

		++key;
	}

	fx_assert(fx_deque_isempty(deque));
}



static void test_deque_ops2(fx_deque_t *deque)
{
	int rc;
	size_t i, key, nelems;
	msg_t msg;
	const msg_t *p_msg = NULL;

	nelems = fx_deque_maxsize(deque);
	fx_assert((nelems % 2) == 0);

	for (i = 0; i < nelems / 2; ++i) {
		key = i;

		rc = fx_deque_isfull(deque);
		fx_assert(!rc);

		msg_init(&msg, key);
		fx_deque_push_back(deque, &msg);
		p_msg = (const msg_t *)fx_deque_back(deque);
		msg_check(p_msg, key);

		msg_init(&msg, key);
		fx_deque_push_back(deque, &msg);
		p_msg = (const msg_t *)fx_deque_back(deque);
		msg_check(p_msg, key);

		fx_deque_pop_front(deque);

		key = i + 1;
		msg_init(&msg, key);
		fx_deque_push_front(deque, &msg);
		p_msg = (const msg_t *)fx_deque_front(deque);
		msg_check(p_msg, key);
	}

	while (!fx_deque_isempty(deque)) {
		fx_deque_pop_back(deque);
	}
}

static void test_deque_ops(fx_deque_t *deque)
{
	fx_assert(fx_deque_isempty(deque));
	test_deque_ops1(deque);

	fx_assert(fx_deque_isempty(deque));
	test_deque_ops1(deque);

	fx_assert(fx_deque_isempty(deque));
	test_deque_ops1(deque);

	fx_assert(fx_deque_isempty(deque));
	test_deque_ops2(deque);
}


static void test_deque(void)
{
	size_t nelems, max_sz;
	fx_deque_t *deque;

	nelems = MAX_SIZE;
	deque = fx_deque_new(sizeof(msg_t), nelems);

	max_sz = fx_deque_maxsize(deque);
	fx_assert(max_sz == nelems);

	test_deque_ops(deque);

	fx_deque_delete(deque);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int main(void)
{
	test_deque();

	return EXIT_SUCCESS;
}




/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>

#include "compiler.h"
#include "utility.h"
#include "buffer.h"
#include "timex.h"
#include "thread.h"
#include "recordq.h"

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_queue_init(fx_queue_t *q, void *mem, size_t sz)
{
	fx_bzero(q, sizeof(*q));

	fx_lock_init(&q->q_lock);
	fx_buffer_init(&q->q_buf, mem, sz);
}

void fx_queue_destroy(fx_queue_t *q)
{
	fx_buffer_destroy(&q->q_buf);
	fx_lock_destroy(&q->q_lock);
	fx_bff(q, sizeof(*q));
}

int fx_queue_push_back(fx_queue_t *q, const void *p, size_t n)
{
	size_t sz, maxsz;
	fx_lock_t *lock;

	lock = &q->q_lock;
	fx_lock_acquire(lock);

	sz      = fx_buffer_size(&q->q_buf);
	maxsz   = fx_buffer_maxsize(&q->q_buf);

	if (n > maxsz) {
		fx_lock_release(lock);
		return -1;
	}

	if ((sz + n) > maxsz) {
		fx_lock_release(lock);
		return -1;
	}

	fx_buffer_push_back(&q->q_buf, p, n);

	fx_lock_signal(lock);
	fx_lock_release(lock);

	return 0;
}

int fx_queue_pop_front(fx_queue_t *q, void *p, size_t nbytes,
                       size_t usec_timeout, size_t *n_poped)
{
	int rc, n = 0;
	size_t sz;
	fx_timespec_t ts;
	fx_lock_t *lock;

	lock = &q->q_lock;
	fx_lock_acquire(lock);

	fx_timespec_getmonotime(&ts);
	fx_timespec_usecadd(&ts, (long)usec_timeout);

	sz = fx_buffer_size(&q->q_buf);
	while (sz == 0) {
		rc = fx_lock_timedwait(lock, &ts);
		if (rc != 0) {
			fx_lock_release(lock);
			return -1;
		}
		sz = fx_buffer_size(&q->q_buf);
	}

	sz = fx_buffer_size(&q->q_buf);
	if (sz > 0) {
		n = fx_buffer_pop_front(&q->q_buf, p, nbytes);
	}
	fx_lock_release(lock);

	if (n_poped != NULL) {
		*n_poped = (size_t) n;
	}

	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_recordq_init(fx_recordq_t *rq,
                     size_t rec_size, void *mem, size_t mem_sz)
{
	fx_queue_init(&rq->rq_queue, mem, mem_sz);

	rq->rq_recsz = rec_size;
	rq->rq_maxsz = mem_sz / rec_size;
	rq->rq_size  = 0;
}


void fx_recordq_destroy(fx_recordq_t *rq)
{
	const size_t zero = 0;

	rq->rq_recsz  = ~zero;
	rq->rq_maxsz  = ~zero;
	rq->rq_size   = ~zero;

	fx_queue_destroy(&rq->rq_queue);
}


fx_recordq_t *fx_recordq_new(size_t rec_size, size_t max_recs)
{
	size_t sz1, sz2, align;
	fx_recordq_t *rq;
	void *p;

	align = sizeof(void *);
	sz1   = ((sizeof(*rq) + align - 1) / align) * align;
	sz2   = rec_size * max_recs;

	rq = NULL;
	p  = fx_malloc(sz1 + sz2);
	if (p != NULL) {
		rq = (fx_recordq_t *)p;
		fx_recordq_init(rq, rec_size, ((char *)p) + sz1, sz2);
	}

	return rq;
}


void fx_recordq_del(fx_recordq_t *rq)
{
	size_t sz1, sz2, align;
	uint8_t *p;

	align = sizeof(void *);
	sz1   = ((sizeof(*rq) + align - 1) / align) * align;
	sz2   = rq->rq_recsz * rq->rq_maxsz;

	p = (uint8_t *)fx_buffer_umemory(&rq->rq_queue.q_buf);
	p -= sz1;

	fx_recordq_destroy(rq);
	fx_free(p, sz1 + sz2);
}


size_t fx_recordq_size(const fx_recordq_t *rq)
{
	return rq->rq_size;
}


int fx_recordq_push(fx_recordq_t *rq, const void *p_msg)
{
	int rc;
	fx_queue_t *queue;

	queue = &rq->rq_queue;
	rc = fx_queue_push_back(queue, p_msg, rq->rq_recsz);
	if (rc == 0) {
		rq->rq_size += 1;
	}

	return rc;
}

int fx_recordq_pop(fx_recordq_t *rq, void *p, size_t usec_timeout)
{
	int rc;
	size_t n = 0;
	fx_queue_t *queue;

	queue = &rq->rq_queue;
	rc = fx_queue_pop_front(queue, p, rq->rq_recsz, usec_timeout, &n);
	if (rc == 0) {
		rq->rq_size -= 1;
	}

	return rc;
}



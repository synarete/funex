/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>

#include "compiler.h"
#include "utility.h"
#include "panic.h"
#include "wstringx.h"
#include "wsubstr.h"


#define wsubstr_out_of_range(ss, pos, sz)               \
	fx_panic("out-of-range pos=%ld sz=%ld ss=%s",      \
	         (long)pos, (long)sz, ((const char*)ss->str))


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static size_t wsubstr_max_size(void)
{
	return (size_t)(-1);
}
size_t fx_wsubstr_max_size(void)
{
	return wsubstr_max_size();
}

static size_t wsubstr_npos(void)
{
	return wsubstr_max_size();
}
size_t fx_wsubstr_npos(void)
{
	return wsubstr_npos();
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Immutable String Operations:
 */

/* Returns the offset of p within substr */
static size_t wsubstr_offset(const fx_wsubstr_t *ss, const wchar_t *p)
{
	size_t off;

	off = wsubstr_npos();
	if (p != NULL) {
		if ((p >= ss->str) && (p < (ss->str + ss->len))) {
			off = (size_t)(p - ss->str);
		}
	}
	return off;
}

void fx_wsubstr_init(fx_wsubstr_t *ss, const wchar_t *s)
{
	fx_wsubstr_init_rd(ss, s, fx_wstr_length(s));
}

void fx_wsubstr_init_rd(fx_wsubstr_t *ss, const wchar_t *s, size_t n)
{
	fx_wsubstr_init_rw(ss, (wchar_t *)s, n, 0UL);
}

void fx_wsubstr_init_rwa(fx_wsubstr_t *ss, wchar_t *s)
{
	size_t len;

	len = fx_wstr_length(s);
	fx_wsubstr_init_rw(ss, s, len, len);
}

void fx_wsubstr_init_rw(fx_wsubstr_t *ss,
                        wchar_t *s, size_t nrd, size_t nwr)
{
	ss->str  = s;
	ss->len  = nrd;
	ss->nwr  = nwr;
}

void fx_wsubstr_inits(fx_wsubstr_t *ss)
{
	static const wchar_t *es = L"";
	fx_wsubstr_init(ss, es);
}

void fx_wsubstr_clone(fx_wsubstr_t *ss, const fx_wsubstr_t *other)
{
	ss->str  = other->str;
	ss->len  = other->len;
	ss->nwr  = other->nwr;
}

void fx_wsubstr_destroy(fx_wsubstr_t *ss)
{
	ss->str  = NULL;
	ss->len  = 0;
	ss->nwr  = 0;
}


static const wchar_t *wsubstr_data(const fx_wsubstr_t *ss)
{
	return ss->str;
}

static wchar_t *wsubstr_mutable_data(const fx_wsubstr_t *ss)
{
	return (wchar_t *)(ss->str);
}

static size_t wsubstr_size(const fx_wsubstr_t *ss)
{
	return ss->len;
}

size_t fx_wsubstr_size(const fx_wsubstr_t *ss)
{
	return wsubstr_size(ss);
}

static size_t wsubstr_wrsize(const fx_wsubstr_t *ss)
{
	return ss->nwr;
}

size_t fx_wsubstr_wrsize(const fx_wsubstr_t *ss)
{
	return wsubstr_wrsize(ss);
}

static int wsubstr_isempty(const fx_wsubstr_t *ss)
{
	return (wsubstr_size(ss) == 0);
}

int fx_wsubstr_isempty(const fx_wsubstr_t *ss)
{
	return wsubstr_isempty(ss);
}

static const wchar_t *wsubstr_begin(const fx_wsubstr_t *ss)
{
	return wsubstr_data(ss);
}

const wchar_t *fx_wsubstr_begin(const fx_wsubstr_t *ss)
{
	return wsubstr_begin(ss);
}

static const wchar_t *wsubstr_end(const fx_wsubstr_t *ss)
{
	return (wsubstr_data(ss) + wsubstr_size(ss));
}

const wchar_t *fx_wsubstr_end(const fx_wsubstr_t *ss)
{
	return wsubstr_end(ss);
}

size_t fx_wsubstr_offset(const fx_wsubstr_t *ss, const wchar_t *p)
{
	return wsubstr_offset(ss, p);
}

const wchar_t *fx_wsubstr_at(const fx_wsubstr_t *ss, size_t n)
{
	size_t sz;

	sz = wsubstr_size(ss);
	if (!(n < sz)) {
		wsubstr_out_of_range(ss, n, sz);
	}
	return wsubstr_data(ss) + n;
}

int fx_wsubstr_isvalid_index(const fx_wsubstr_t *ss, size_t i)
{
	return (i < wsubstr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_copyto(const fx_wsubstr_t *ss, wchar_t *buf, size_t n)
{
	size_t n1;

	n1 = fx_min(n, ss->len);
	fx_wstr_copy(buf, ss->str, n1);

	if (n1 < n) { /* If possible, terminate with EOS. */
		fx_wstr_terminate(buf, n1);
	}

	return n1;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_wsubstr_compare(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_ncompare(ss, s, fx_wstr_length(s));
}

int fx_wsubstr_ncompare(const fx_wsubstr_t *ss, const wchar_t *s, size_t n)
{
	int res = 0;

	if ((ss->str != s) || (ss->len != n)) {
		res = fx_wstr_ncompare(ss->str, ss->len, s, n);
	}
	return res;
}

int fx_wsubstr_isequal(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_nisequal(ss, s, fx_wstr_length(s));
}

int fx_wsubstr_nisequal(const fx_wsubstr_t *ss, const wchar_t *s, size_t n)
{
	int res;
	const wchar_t *str;

	res = 0;
	if (wsubstr_size(ss) == n) {
		str = wsubstr_data(ss);
		res = ((str == s) || (fx_wstr_compare(str, s, n) == 0));
	}
	return res;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_count(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_ncount(ss, s, fx_wstr_length(s));
}

size_t fx_wsubstr_ncount(const fx_wsubstr_t *ss, const wchar_t *s, size_t n)
{
	size_t i, sz, pos, cnt;

	sz  = wsubstr_size(ss);
	pos = 0;
	cnt = 0;
	i   = fx_wsubstr_nfind(ss, pos, s, n);
	while (i < sz) {
		++cnt;

		pos = i + n;
		i   = fx_wsubstr_nfind(ss, pos, s, n);
	}

	return cnt;
}

size_t fx_wsubstr_count_chr(const fx_wsubstr_t *ss, wchar_t c)
{
	size_t i, sz, pos, cnt;

	sz  = wsubstr_size(ss);
	pos = 0;
	cnt = 0;
	i   = fx_wsubstr_find_chr(ss, pos, c);
	while (i < sz) {
		++cnt;

		pos = i + 1;
		i   = fx_wsubstr_find_chr(ss, pos, c);
	}

	return cnt;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_find(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_nfind(ss, 0UL, s, fx_wstr_length(s));
}

size_t fx_wsubstr_nfind(const fx_wsubstr_t *ss,
                        size_t pos, const wchar_t *s, size_t n)
{
	size_t sz;
	const wchar_t *p;
	const wchar_t *dat;

	p   = NULL;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);

	if (pos < sz) {
		if (n > 1) {
			p = fx_wstr_find(dat + pos, sz - pos, s, n);
		} else if (n == 1) {
			p = fx_wstr_find_chr(dat + pos, sz - pos, s[0]);
		} else { /* n == 0 */
			/*
			 * Stay compatible with STL: empty string always matches (if inside
			 * string).
			 */
			p = dat + pos;
		}
	}
	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_find_chr(const fx_wsubstr_t *ss, size_t pos, wchar_t c)
{
	size_t sz;
	const wchar_t *p;
	const wchar_t *dat;

	p   = NULL;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);

	if (pos < sz) {
		p = fx_wstr_find_chr(dat + pos, sz - pos, c);
	}
	return wsubstr_offset(ss, p);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_rfind(const fx_wsubstr_t *ss, const wchar_t *s)
{
	size_t pos;

	pos = wsubstr_size(ss);
	return fx_wsubstr_nrfind(ss, pos, s, fx_wstr_length(s));
}

size_t fx_wsubstr_nrfind(const fx_wsubstr_t *ss,
                         size_t pos, const wchar_t *s, size_t n)
{
	size_t k, sz;
	const wchar_t *p;
	const wchar_t *q;
	const wchar_t *dat;

	p   = NULL;
	q   = s;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);
	k   = (pos < sz) ? pos + 1 : sz;

	if (n == 0) {
		/* STL compatible: empty string always matches */
		p = dat + k;
	} else if (n == 1) {
		p = fx_wstr_rfind_chr(dat, k, *q);
	} else {
		p = fx_wstr_rfind(dat, k, q, n);
	}

	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_rfind_chr(const fx_wsubstr_t *ss, size_t pos, wchar_t c)
{
	size_t sz, k;
	const wchar_t *p;
	const wchar_t *dat;

	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);
	k   = (pos < sz) ? pos + 1 : sz;

	p = fx_wstr_rfind_chr(dat, k, c);
	return wsubstr_offset(ss, p);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_find_first_of(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_nfind_first_of(ss, 0UL, s, fx_wstr_length(s));
}

size_t fx_wsubstr_nfind_first_of(const fx_wsubstr_t *ss,
                                 size_t pos, const wchar_t *s, size_t n)
{
	size_t sz;
	const wchar_t *p;
	const wchar_t *q;
	const wchar_t *dat;

	p   = NULL;
	q   = s;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);

	if ((n != 0) && (pos < sz)) {
		if (n == 1) {
			p = fx_wstr_find_chr(dat + pos, sz - pos, *q);
		} else {
			p = fx_wstr_find_first_of(dat + pos, sz - pos, q, n);
		}
	}

	return wsubstr_offset(ss, p);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_find_last_of(const fx_wsubstr_t *ss, const wchar_t *s)
{
	size_t pos;

	pos = wsubstr_size(ss);
	return fx_wsubstr_nfind_last_of(ss, pos, s, fx_wstr_length(s));
}

size_t fx_wsubstr_nfind_last_of(const fx_wsubstr_t *ss, size_t pos,
                                const wchar_t *s, size_t n)
{
	size_t sz;
	const wchar_t *p, *q, *dat;

	p   = NULL;
	q   = s;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);
	if (n != 0) {
		const size_t k = (pos < sz) ? pos + 1 : sz;

		if (n == 1) {
			p = fx_wstr_rfind_chr(dat, k, *q);
		} else {
			p = fx_wstr_find_last_of(dat, k, q, n);
		}
	}
	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_find_first_not_of(const fx_wsubstr_t *ss,
                                    const wchar_t *s)
{
	return fx_wsubstr_nfind_first_not_of(ss, 0UL, s, fx_wstr_length(s));
}

size_t fx_wsubstr_nfind_first_not_of(const fx_wsubstr_t *ss,
                                     size_t pos, const wchar_t *s, size_t n)
{
	size_t sz;
	const wchar_t *p, *q, *dat;

	p   = NULL;
	q   = s;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);

	if (pos < sz) {
		if (n == 0) {
			p = dat + pos;
		} else if (n == 1) {
			p = fx_wstr_find_first_not_eq(dat + pos, sz - pos, *q);
		} else {
			p = fx_wstr_find_first_not_of(dat + pos, sz - pos, q, n);
		}
	}

	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_find_first_not(const fx_wsubstr_t *ss,
                                 size_t pos, wchar_t c)
{
	size_t sz;
	const wchar_t *p, *dat;

	p   = NULL;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);
	if (pos < sz) {
		p = fx_wstr_find_first_not_eq(dat + pos, sz - pos, c);
	}
	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_find_last_not_of(const fx_wsubstr_t *ss, const wchar_t *s)
{
	size_t pos;

	pos = wsubstr_size(ss);
	return fx_wsubstr_nfind_last_not_of(ss, pos, s, fx_wstr_length(s));
}

size_t fx_wsubstr_nfind_last_not_of(const fx_wsubstr_t *ss,
                                    size_t pos, const wchar_t *s, size_t n)
{
	size_t k, sz;
	const wchar_t *p, *q, *dat;

	p   = NULL;
	q   = s;
	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);

	if (sz != 0) {
		k = (pos < sz) ? pos + 1 : sz;
		if (n == 0) {
			/* Stay compatible with STL. */
			p = dat + k - 1;
		} else if (n == 1) {
			p = fx_wstr_find_last_not_eq(dat, k, *q);
		} else {
			p = fx_wstr_find_last_not_of(dat, k, q, n);
		}
	}

	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_find_last_not(const fx_wsubstr_t *ss,
                                size_t pos, wchar_t c)
{
	size_t k, sz;
	const wchar_t *p, *dat;

	dat = wsubstr_data(ss);
	sz  = wsubstr_size(ss);
	k   = (pos < sz) ? pos + 1 : sz;
	p   = fx_wstr_find_last_not_eq(dat, k, c);

	return wsubstr_offset(ss, p);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_sub(const fx_wsubstr_t *ss,
                    size_t i, size_t n, fx_wsubstr_t *result)
{
	size_t j, k, n1, n2, sz, wr;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	j   = fx_min(i, sz);
	n1  = fx_min(n, sz - j);

	wr  = wsubstr_wrsize(ss);
	k   = fx_min(i, wr);
	n2  = fx_min(n, wr - k);
	dat = wsubstr_mutable_data(ss);

	fx_wsubstr_init_rw(result, dat + j, n1, n2);
}

void fx_wsubstr_rsub(const fx_wsubstr_t *ss,
                     size_t n, fx_wsubstr_t *result)
{
	size_t j, k, n1, n2, sz, wr;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	n1  = fx_min(n, sz);
	j   = sz - n1;

	wr  = wsubstr_wrsize(ss);
	k   = fx_min(j, wr);
	n2  = wr - k;
	dat = wsubstr_mutable_data(ss);

	fx_wsubstr_init_rw(result, dat + j, n1, n2);
}

void fx_wsubstr_intersection(const fx_wsubstr_t *s1,
                             const fx_wsubstr_t *s2,
                             fx_wsubstr_t *result)
{
	size_t i, n;
	const wchar_t *s1_begin;
	const wchar_t *s1_end;
	const wchar_t *s2_begin;
	const wchar_t *s2_end;

	s1_begin = wsubstr_begin(s1);
	s2_begin = wsubstr_begin(s2);
	if (s1_begin <= s2_begin) {
		i = n = 0;

		s1_end = wsubstr_end(s1);
		s2_end = wsubstr_end(s2);

		/* Case 1:  [.s1...)  [..s2.....) -- Returns empty substring. */
		if (s1_end <= s2_begin) {
			i = wsubstr_size(s2);
		}
		/* Case 2: [.s1........)
		                [.s2..) */
		else if (s2_end <= s1_end) {
			n = wsubstr_size(s2);
		}
		/* Case 3: [.s1.....)
		               [.s2......) */
		else {
			n = (size_t)(s1_end - s2_begin);
		}
		fx_wsubstr_sub(s2, i, n, result);
	} else {
		/* One step recursion -- its ok */
		fx_wsubstr_intersection(s2, s1, result);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Helper function to create split-of-substrings */
static void wsubstr_make_split_pair(const fx_wsubstr_t *ss,
                                    size_t i1, size_t n1,
                                    size_t i2, size_t n2,
                                    fx_wsubstr_pair_t *result)
{
	fx_wsubstr_sub(ss, i1, n1, &result->first);
	fx_wsubstr_sub(ss, i2, n2, &result->second);
}

void fx_wsubstr_split(const fx_wsubstr_t *ss,
                      const wchar_t *seps, fx_wsubstr_pair_t *result)
{

	fx_wsubstr_nsplit(ss, seps, fx_wstr_length(seps), result);
}

void fx_wsubstr_nsplit(const fx_wsubstr_t *ss,
                       const wchar_t *seps, size_t n,
                       fx_wsubstr_pair_t *result)
{
	size_t i, j, sz;

	sz = wsubstr_size(ss);
	i  = fx_wsubstr_nfind_first_of(ss, 0UL, seps, n);
	j  = sz;
	if (i < sz) {
		j = fx_wsubstr_nfind_first_not_of(ss, i, seps, n);
	}

	wsubstr_make_split_pair(ss, 0UL, i, j, sz, result);
}

void fx_wsubstr_split_chr(const fx_wsubstr_t *ss, wchar_t sep,
                          fx_wsubstr_pair_t *result)
{
	size_t i, j, sz;

	sz  = wsubstr_size(ss);
	i   = fx_wsubstr_find_chr(ss, 0UL, sep);
	j   = (i < sz) ? i + 1 : sz;

	wsubstr_make_split_pair(ss, 0UL, i, j, sz, result);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_rsplit(const fx_wsubstr_t *ss,
                       const wchar_t *seps, fx_wsubstr_pair_t *result)
{

	fx_wsubstr_nrsplit(ss, seps, fx_wstr_length(seps), result);
}

void fx_wsubstr_nrsplit(const fx_wsubstr_t *ss,
                        const wchar_t *seps, size_t n,
                        fx_wsubstr_pair_t *result)
{
	size_t i, j, sz;

	sz = wsubstr_size(ss);
	i  = fx_wsubstr_nfind_last_of(ss, sz, seps, n);
	j  = sz;
	if (i < sz) {
		j = fx_wsubstr_nfind_last_not_of(ss, i, seps, n);

		if (j < sz) {
			++i;
			++j;
		} else {
			i = j = sz;
		}
	}
	wsubstr_make_split_pair(ss, 0UL, j, i, sz, result);
}

void fx_wsubstr_rsplit_chr(const fx_wsubstr_t *ss, wchar_t sep,
                           fx_wsubstr_pair_t *result)
{
	size_t i, j, sz;

	sz  = wsubstr_size(ss);
	i   = fx_wsubstr_rfind_chr(ss, sz, sep);
	j   = (i < sz) ? i + 1 : sz;

	wsubstr_make_split_pair(ss, 0UL, i, j, sz, result);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_trim(const fx_wsubstr_t *substr, size_t n,
                     fx_wsubstr_t *result)
{
	size_t sz;

	sz = wsubstr_size(substr);
	fx_wsubstr_sub(substr, n, sz, result);
}

void fx_wsubstr_trim_any_of(const fx_wsubstr_t *ss,
                            const wchar_t *set, fx_wsubstr_t *result)
{
	fx_wsubstr_ntrim_any_of(ss, set, fx_wstr_length(set), result);
}

void fx_wsubstr_ntrim_any_of(const fx_wsubstr_t *ss,
                             const wchar_t *set, size_t n,
                             fx_wsubstr_t *result)
{
	size_t i, sz;

	sz = wsubstr_size(ss);
	i  = fx_wsubstr_nfind_first_not_of(ss, 0UL, set, n);

	fx_wsubstr_sub(ss, i, sz, result);
}

void fx_wsubstr_trim_chr(const fx_wsubstr_t *ss, wchar_t c,
                         fx_wsubstr_t *result)
{
	size_t i, sz;

	sz = wsubstr_size(ss);
	i  = fx_wsubstr_find_first_not(ss, 0UL, c);

	fx_wsubstr_sub(ss, i, sz, result);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_chop(const fx_wsubstr_t *ss,
                     size_t n, fx_wsubstr_t *result)
{
	size_t k, sz, wr;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);
	k   = fx_min(sz, n);
	dat = wsubstr_mutable_data(ss);

	fx_wsubstr_init_rw(result, dat, sz - k, wr);
}

void fx_wsubstr_chop_any_of(const fx_wsubstr_t *ss,
                            const wchar_t *set, fx_wsubstr_t *result)
{
	fx_wsubstr_nchop_any_of(ss, set, fx_wstr_length(set), result);
}

void fx_wsubstr_nchop_any_of(const fx_wsubstr_t *ss,
                             const wchar_t *set, size_t n,
                             fx_wsubstr_t *result)
{
	size_t j, sz;

	sz = wsubstr_size(ss);
	j  = fx_wsubstr_nfind_last_not_of(ss, sz, set, n);

	fx_wsubstr_sub(ss, 0UL, ((j < sz) ? j + 1 : 0), result);
}

void fx_wsubstr_chop_chr(const fx_wsubstr_t *substr, wchar_t c,
                         fx_wsubstr_t *result)
{
	size_t j, sz;

	sz = wsubstr_size(substr);
	j  = fx_wsubstr_find_last_not(substr, sz, c);

	fx_wsubstr_sub(substr, 0UL, ((j < sz) ? j + 1 : 0), result);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_strip_any_of(const fx_wsubstr_t *ss,
                             const wchar_t *set, fx_wsubstr_t *result)
{
	fx_wsubstr_nstrip_any_of(ss, set, fx_wstr_length(set), result);
}

void fx_wsubstr_nstrip_any_of(const fx_wsubstr_t *ss,
                              const wchar_t *set, size_t n,
                              fx_wsubstr_t *result)
{
	fx_wsubstr_t sub;

	fx_wsubstr_ntrim_any_of(ss, set, n, &sub);
	fx_wsubstr_nchop_any_of(&sub, set, n, result);
}

void fx_wsubstr_strip_chr(const fx_wsubstr_t *ss, wchar_t c,
                          fx_wsubstr_t *result)
{
	fx_wsubstr_t sub;

	fx_wsubstr_trim_chr(ss, c, &sub);
	fx_wsubstr_chop_chr(&sub, c, result);
}

void fx_wsubstr_strip_ws(const fx_wsubstr_t *ss, fx_wsubstr_t *result)
{
	const wchar_t *spaces = L" \n\t\r\v\f";
	fx_wsubstr_strip_any_of(ss, spaces, result);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_find_token(const fx_wsubstr_t *ss,
                           const wchar_t *seps, fx_wsubstr_t *result)
{
	fx_wsubstr_nfind_token(ss, seps, fx_wstr_length(seps), result);
}

void fx_wsubstr_nfind_token(const fx_wsubstr_t *ss,
                            const wchar_t *seps, size_t n,
                            fx_wsubstr_t *result)
{
	size_t i, j, sz;

	sz = wsubstr_size(ss);
	i  = fx_min(fx_wsubstr_nfind_first_not_of(ss, 0UL, seps, n), sz);
	j  = fx_min(fx_wsubstr_nfind_first_of(ss, i, seps, n), sz);

	fx_wsubstr_sub(ss, i, j - i, result);
}

void fx_wsubstr_find_token_chr(const fx_wsubstr_t *ss, wchar_t sep,
                               fx_wsubstr_t *result)
{
	size_t i, j, sz;

	sz = wsubstr_size(ss);
	i  = fx_min(fx_wsubstr_find_first_not(ss, 0UL, sep), sz);
	j  = fx_min(fx_wsubstr_find_chr(ss, i, sep), sz);

	fx_wsubstr_sub(ss, i, j - i, result);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_find_next_token(const fx_wsubstr_t *ss,
                                const fx_wsubstr_t *tok,
                                const wchar_t *seps,
                                fx_wsubstr_t *result)
{
	fx_wsubstr_nfind_next_token(ss, tok, seps, fx_wstr_length(seps), result);
}

void fx_wsubstr_nfind_next_token(const fx_wsubstr_t *ss,
                                 const fx_wsubstr_t *tok,
                                 const wchar_t *seps, size_t n,
                                 fx_wsubstr_t *result)
{
	size_t i, sz;
	const wchar_t *p;
	fx_wsubstr_t sub;

	sz  = wsubstr_size(ss);
	p   = wsubstr_end(tok);
	i   = wsubstr_offset(ss, p);

	fx_wsubstr_sub(ss, i, sz, &sub);
	fx_wsubstr_nfind_token(&sub, seps, n, result);
}

void fx_wsubstr_find_next_token_chr(const fx_wsubstr_t *ss,
                                    const fx_wsubstr_t *tok, wchar_t sep,
                                    fx_wsubstr_t *result)
{
	size_t i, sz;
	const wchar_t *p;
	fx_wsubstr_t sub;

	sz  = wsubstr_size(ss);
	p   = wsubstr_end(tok);
	i   = wsubstr_offset(ss, p);

	fx_wsubstr_sub(ss, i, sz, &sub);
	fx_wsubstr_find_token_chr(&sub, sep, result);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int fx_wsubstr_tokenize(const fx_wsubstr_t *ss,
                        const wchar_t *seps,
                        fx_wsubstr_t tok_list[],
                        size_t list_size, size_t *p_ntok)
{
	return fx_wsubstr_ntokenize(ss, seps, fx_wstr_length(seps),
	                            tok_list, list_size, p_ntok);
}

int fx_wsubstr_ntokenize(const fx_wsubstr_t *ss,
                         const wchar_t *seps, size_t n,
                         fx_wsubstr_t tok_list[],
                         size_t list_size, size_t *p_ntok)
{
	int rc;
	size_t ntok;
	fx_wsubstr_t tok_obj;
	fx_wsubstr_t *tok = &tok_obj;
	fx_wsubstr_t *tgt = NULL;

	rc   = 0;
	ntok = 0;

	fx_wsubstr_nfind_token(ss, seps, n, tok);
	while (!fx_wsubstr_isempty(tok)) {
		if (ntok == list_size) {
			rc = -1; /* Insufficient room */
			goto out;
		}
		tgt = &tok_list[ntok++];
		fx_wsubstr_clone(tgt, tok);

		fx_wsubstr_nfind_next_token(ss, tok, seps, n, tok);
	}

out:
	if (p_ntok != NULL) {
		*p_ntok = ntok;
	}
	return rc;
}

int fx_wsubstr_tokenize_chr(const fx_wsubstr_t *ss, wchar_t sep,
                            fx_wsubstr_t tok_list[],
                            size_t list_size, size_t *p_ntok)
{
	int rc;
	size_t ntok;
	fx_wsubstr_t tok_obj;
	fx_wsubstr_t *tok = &tok_obj;
	fx_wsubstr_t *tgt = NULL;

	rc   = 0;
	ntok = 0;

	fx_wsubstr_find_token_chr(ss, sep, tok);
	while (!fx_wsubstr_isempty(tok)) {
		if (ntok == list_size) {
			rc = -1; /* Insufficient room */
			goto out;
		}
		tgt = &tok_list[ntok++];
		fx_wsubstr_clone(tgt, tok);

		fx_wsubstr_find_next_token_chr(ss, tok, sep, tok);
	}

out:
	if (p_ntok != NULL) {
		*p_ntok = ntok;
	}
	return rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_common_prefix(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_ncommon_prefix(ss, s, fx_wstr_length(s));
}

size_t fx_wsubstr_ncommon_prefix(const fx_wsubstr_t *ss,
                                 const wchar_t *s, size_t n)
{
	size_t sz;
	const wchar_t *p;

	p  = wsubstr_data(ss);
	sz = wsubstr_size(ss);
	return fx_wstr_common_prefix(p, s, fx_min(n, sz));
}

int fx_wsubstr_starts_with(const fx_wsubstr_t *ss, wchar_t c)
{
	const wchar_t s[] = { c };
	return (fx_wsubstr_ncommon_prefix(ss, s, 1) == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_common_suffix(const fx_wsubstr_t *ss, const wchar_t *s)
{
	return fx_wsubstr_ncommon_suffix(ss, s, fx_wstr_length(s));
}

size_t fx_wsubstr_ncommon_suffix(const fx_wsubstr_t *ss,
                                 const wchar_t *s, size_t n)
{
	size_t sz, k;
	const wchar_t *p;

	p  = wsubstr_data(ss);
	sz = wsubstr_size(ss);
	if (n > sz) {
		k = fx_wstr_common_suffix(p, s + (n - sz), sz);
	} else {
		k = fx_wstr_common_suffix(p + (sz - n), s, n);
	}
	return k;
}

int fx_wsubstr_ends_with(const fx_wsubstr_t *ss, wchar_t c)
{
	const wchar_t s[] = { c };
	return (fx_wsubstr_ncommon_suffix(ss, s, 1) == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Mutable String Opeartions:
 */
wchar_t *fx_wsubstr_data(const fx_wsubstr_t *ss)
{
	return wsubstr_mutable_data(ss);
}

/* Set EOS characters at the end of characters array (if possible) */
static void wsubstr_terminate(fx_wsubstr_t *ss)
{
	wchar_t *dat;
	size_t sz, wr;

	dat = wsubstr_mutable_data(ss);
	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);

	if (sz < wr) {
		fx_wstr_terminate(dat, sz);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Inserts a copy of s before position pos. */
static void wsubstr_insert(fx_wsubstr_t *ss,
                           size_t pos, const wchar_t *s, size_t n)
{
	size_t sz, wr, j, k, rem, m;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);
	dat = fx_wsubstr_data(ss);

	/* Start insertion before position j. */
	j   = fx_min(pos, sz);

	/* Number of writable elements after j. */
	rem = (j < wr) ? (wr - j) : 0;

	/* Number of elements of substr after j (to be moved fwd). */
	k   = sz - j;

	/* Insert n elements of p: try as many as possible, truncate tail in
	    case of insufficient buffer capacity. */
	m = fx_wstr_insert(dat + j, rem, k, s, n);
	ss->len = j + m;

	wsubstr_terminate(ss);
}

/* Inserts n copies of c before position pos. */
static void wsubstr_insert_fill(fx_wsubstr_t *ss,
                                size_t pos, size_t n, wchar_t c)
{
	size_t sz, wr, j, k, rem, m;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);
	dat = fx_wsubstr_data(ss);

	/* Start insertion before position j. */
	j   = fx_min(pos, sz);

	/* Number of writable elements after j. */
	rem = (j < wr) ? (wr - j) : 0;

	/* Number of elements of substr after j (to be moved fwd). */
	k   = sz - j;

	/* Insert n copies of c: try as many as possible; truncate tail in
	    case of insufficient buffer capacity. */
	m = fx_wstr_insert_chr(dat + j, rem, k, n, c);
	ss->len = j + m;

	wsubstr_terminate(ss);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Replaces a substring of *this with a copy of s. */
static void wsubstr_replace(fx_wsubstr_t *ss, size_t pos, size_t n1,
                            const wchar_t *s, size_t n)
{
	size_t sz, wr, k, m;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);
	dat = wsubstr_mutable_data(ss);

	/* Number of elements to replace (assuming pos <= size). */
	k = fx_min(sz - pos, n1);

	/* Replace k elements after pos with s; truncate tail in case of
	    insufficient buffer capacity. */
	m = fx_wstr_replace(dat + pos, wr - pos, sz - pos, k, s, n);
	ss->len = pos + m;

	wsubstr_terminate(ss);
}

/* Replaces a substring of *this with n2 copies of c. */
static void wsubstr_replace_fill(fx_wsubstr_t *ss,
                                 size_t pos, size_t n1, size_t n2, wchar_t c)
{
	size_t sz, wr, k, m;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);
	dat = wsubstr_mutable_data(ss);

	/* Number of elements to replace (assuming pos <= size). */
	k = fx_min(sz - pos, n1);

	/* Replace k elements after pos with n2 copies of c; truncate tail in
	    case of insufficient buffer capacity. */
	m = fx_wstr_replace_chr(dat + pos, wr - pos, sz - pos, k, n2, c);
	ss->len = pos + m;

	wsubstr_terminate(ss);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_assign(fx_wsubstr_t *ss, const wchar_t *s)
{
	fx_wsubstr_nassign(ss, s, fx_wstr_length(s));
}

void fx_wsubstr_nassign(fx_wsubstr_t *ss, const wchar_t *s, size_t len)
{
	size_t sz;

	sz = wsubstr_size(ss);
	fx_wsubstr_nreplace(ss, 0, sz, s, len);
}

void fx_wsubstr_assign_chr(fx_wsubstr_t *ss, size_t n, wchar_t c)
{
	size_t sz;

	sz = wsubstr_size(ss);
	fx_wsubstr_replace_chr(ss, 0, sz, n, c);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_push_back(fx_wsubstr_t *ss, wchar_t c)
{
	fx_wsubstr_append_chr(ss, 1, c);
}

void fx_wsubstr_append(fx_wsubstr_t *ss, const wchar_t *s)
{
	fx_wsubstr_nappend(ss, s, fx_wstr_length(s));
}

void fx_wsubstr_nappend(fx_wsubstr_t *ss, const wchar_t *s, size_t len)
{
	size_t sz;

	sz = wsubstr_size(ss);
	fx_wsubstr_ninsert(ss, sz, s, len);
}

void fx_wsubstr_append_chr(fx_wsubstr_t *ss, size_t n, wchar_t c)
{
	size_t sz;

	sz = wsubstr_size(ss);
	fx_wsubstr_insert_chr(ss, sz, n, c);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_insert(fx_wsubstr_t *ss, size_t pos, const wchar_t *s)
{
	fx_wsubstr_ninsert(ss, pos, s, fx_wstr_length(s));
}

void fx_wsubstr_ninsert(fx_wsubstr_t *ss, size_t pos,
                        const wchar_t *s, size_t len)
{
	size_t sz;

	sz = wsubstr_size(ss);
	if (pos <= sz) {
		wsubstr_insert(ss, pos, s, len);
	} else {
		wsubstr_out_of_range(ss, pos, sz);
	}
}

void fx_wsubstr_insert_chr(fx_wsubstr_t *ss, size_t pos, size_t n, wchar_t c)
{
	size_t sz;

	sz = wsubstr_size(ss);
	if (pos <= sz) {
		wsubstr_insert_fill(ss, pos, n, c);
	} else {
		wsubstr_out_of_range(ss, pos, sz);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_wsubstr_replace(fx_wsubstr_t *ss,
                        size_t pos, size_t n, const wchar_t *s)
{
	fx_wsubstr_nreplace(ss, pos, n, s, fx_wstr_length(s));
}

void fx_wsubstr_nreplace(fx_wsubstr_t *ss,
                         size_t pos, size_t n,  const wchar_t *s, size_t len)
{
	size_t sz;

	sz = wsubstr_size(ss);
	if (pos < sz) {
		wsubstr_replace(ss, pos, n, s, len);
	} else if (pos == sz) {
		wsubstr_insert(ss, pos, s, len);
	} else {
		wsubstr_out_of_range(ss, pos, sz);
	}
}

void fx_wsubstr_replace_chr(fx_wsubstr_t *ss,
                            size_t pos, size_t n1, size_t n2, wchar_t c)
{
	size_t sz;

	sz = wsubstr_size(ss);
	if (pos < sz) {
		wsubstr_replace_fill(ss, pos, n1, n2, c);
	} else if (pos == sz) {
		wsubstr_insert_fill(ss, pos, n2, c);
	} else {
		wsubstr_out_of_range(ss, pos, sz);
	}
}

void fx_wsubstr_erase(fx_wsubstr_t *ss, size_t pos, size_t n)
{
	wchar_t c = '\0';
	fx_wsubstr_replace_chr(ss, pos, n, 0, c);
}

void fx_wsubstr_reverse(fx_wsubstr_t *ss)
{
	size_t n, sz, wr;
	wchar_t *dat;

	sz  = wsubstr_size(ss);
	wr  = wsubstr_wrsize(ss);
	n   = fx_min(sz, wr);
	dat = fx_wsubstr_data(ss);

	fx_wstr_reverse(dat, n);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Generic Operations:
 */
static size_t wsubstr_find_if(const fx_wsubstr_t *ss, fx_wchr_pred fn, int u)
{
	int v;
	const wchar_t *p, *s, *t;

	p = NULL;
	s = wsubstr_begin(ss);
	t = wsubstr_end(ss);
	while (s < t) {
		v = fn(*s) ? 1 : 0;
		if (v == u) {
			p = s;
			break;
		}
		++s;
	}
	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_find_if(const fx_wsubstr_t *ss, fx_wchr_pred fn)
{
	return wsubstr_find_if(ss, fn, 1);
}

size_t fx_wsubstr_find_if_not(const fx_wsubstr_t *ss, fx_wchr_pred fn)
{
	return wsubstr_find_if(ss, fn, 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static size_t
wsubstr_rfind_if(const fx_wsubstr_t *ss, fx_wchr_pred fn, int u)
{
	int v;
	const wchar_t *p, *s, *t;

	p = NULL;
	s = wsubstr_end(ss);
	t = wsubstr_begin(ss);
	while (s > t) {
		v = fn(*--s) ? 1 : 0;
		if (v == u) {
			p = s;
			break;
		}
	}
	return wsubstr_offset(ss, p);
}

size_t fx_wsubstr_rfind_if(const fx_wsubstr_t *ss, fx_wchr_pred fn)
{
	return wsubstr_rfind_if(ss, fn, 1);
}

size_t fx_wsubstr_rfind_if_not(const fx_wsubstr_t *ss, fx_wchr_pred fn)
{
	return wsubstr_rfind_if(ss, fn, 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

size_t fx_wsubstr_count_if(const fx_wsubstr_t *ss, fx_wchr_pred fn)
{
	size_t cnt;
	const wchar_t *s, *t;

	cnt = 0;
	s = wsubstr_begin(ss);
	t = wsubstr_end(ss);
	while (s < t) {
		if (fn(*s++)) {
			++cnt;
		}
	}
	return cnt;
}


int fx_wsubstr_test_if(const fx_wsubstr_t *substr, fx_wchr_pred fn)
{
	const wchar_t *s, *t;

	s = wsubstr_begin(substr);
	t = wsubstr_end(substr);
	while (s < t) {
		if (!fn(*s++)) {
			return 0;
		}
	}
	return 1;
}

void fx_wsubstr_trim_if(const fx_wsubstr_t *ss,
                        fx_wchr_pred fn, fx_wsubstr_t   *result)
{
	size_t i, sz;

	sz = wsubstr_size(ss);
	i  = fx_wsubstr_find_if_not(ss, fn);

	fx_wsubstr_sub(ss, i, sz, result);
}

void fx_wsubstr_chop_if(const fx_wsubstr_t *ss,
                        fx_wchr_pred fn, fx_wsubstr_t *result)
{
	size_t j, sz;

	sz = wsubstr_size(ss);
	j  = fx_wsubstr_rfind_if_not(ss, fn);

	fx_wsubstr_sub(ss, 0UL, ((j < sz) ? j + 1 : 0), result);
}

void fx_wsubstr_strip_if(const fx_wsubstr_t *ss,
                         fx_wchr_pred fn, fx_wsubstr_t *result)
{
	fx_wsubstr_t sub;

	fx_wsubstr_trim_if(ss, fn, &sub);
	fx_wsubstr_chop_if(&sub, fn, result);
}

void fx_wsubstr_foreach(fx_wsubstr_t *ss, fx_wchr_modify_fn fn)
{
	size_t i, sz;
	wchar_t *p;

	sz = wsubstr_wrsize(ss);
	p  = wsubstr_mutable_data(ss);
	for (i = 0; i < sz; ++i) {
		fn(p++);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Ctype Operations:
 */
int fx_wsubstr_isalnum(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isalnum);
}

int fx_wsubstr_isalpha(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isalpha);
}

int fx_wsubstr_isblank(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isblank);
}

int fx_wsubstr_iscntrl(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_iscntrl);
}

int fx_wsubstr_isdigit(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isdigit);
}

int fx_wsubstr_isgraph(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isgraph);
}

int fx_wsubstr_islower(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_islower);
}

int fx_wsubstr_isprint(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isprint);
}

int fx_wsubstr_ispunct(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_ispunct);
}

int fx_wsubstr_isspace(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isspace);
}

int fx_wsubstr_isupper(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isupper);
}

int fx_wsubstr_isxdigit(const fx_wsubstr_t *ss)
{
	return fx_wsubstr_test_if(ss, fx_wchr_isxdigit);
}

static void wchr_toupper(wchar_t *c)
{
	*c = (wchar_t)fx_wchr_toupper(*c);
}

static void wchr_tolower(wchar_t *c)
{
	*c = (wchar_t)fx_wchr_tolower(*c);
}

void fx_wsubstr_toupper(fx_wsubstr_t *ss)
{
	fx_wsubstr_foreach(ss, wchr_toupper);
}

void fx_wsubstr_tolower(fx_wsubstr_t *ss)
{
	fx_wsubstr_foreach(ss, wchr_tolower);
}

void fx_wsubstr_capitalize(fx_wsubstr_t *ss)
{
	if (ss->len) {
		wchr_toupper(ss->str);
	}
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>

#define FUSE_USE_VERSION    28
#include <fuse/fuse_lowlevel.h>
#include <fuse/fuse_common.h>
#include <fuse/fuse_opt.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "users.h"
#include "fusei.h"
#include "reply.h"


#define IOBUFS_NBK(z) (FX_NELEMS(z->iob[0].bks) * FX_NELEMS(z->iob))


static fx_fuse_req_t getreq(const fx_task_t *task)
{
	return task->tsk_fuse_req;
}

static fuse_ino_t ino_to_fuse_ino(fx_ino_t ino)
{
	/*
	 * NB: Reserve the high 8 bits of ino for fsid so the each 64-bits ino
	 * if globaly unique.
	 *
	 * TODO: needed?
	 */
	const fuse_ino_t fuse_ino = (fuse_ino_t)ino;

	fx_assert((((uint64_t)ino) >> 56) == 0);
	return fuse_ino;
}

static int isfatalerr(int err)
{
	if (err < 0) {
		err = -err;
	}
	return ((err != 0) && (err != ENOENT) && (err != EPIPE));
}

static void fx_fuse_check_rc(int rc)
{
	if (isfatalerr(rc)) {
		fx_panic("fuse-reply-error rc=%d", rc);
	}
}

static void iattr_to_stat(const fx_iattr_t *iattr, struct stat *st)
{
	fx_bzero(st, sizeof(*st));

	st->st_ino          = iattr->i_ino;
	st->st_dev          = iattr->i_dev;
	st->st_mode         = iattr->i_mode;
	st->st_nlink        = iattr->i_nlink;
	st->st_uid          = iattr->i_uid;
	st->st_gid          = iattr->i_gid;
	st->st_rdev         = iattr->i_rdev;
	st->st_size         = iattr->i_size;
	st->st_blksize      = FNX_BLKSIZE;
	st->st_blocks       = (blkcnt_t)(iattr->i_nblk * FNX_BLKNFRG);

	fx_timespec_copy(&st->st_atim, &iattr->i_times.atime);
	fx_timespec_copy(&st->st_ctim, &iattr->i_times.ctime);
	fx_timespec_copy(&st->st_mtim, &iattr->i_times.mtime);
}


static fsblkcnt_t fx_bkcnt_to_fsblkcnt(fx_bkcnt_t nbk)
{
	return nbk * FNX_BLKNFRG;
}

static void fsinfo_to_statvfs(const fx_fsinfo_t *stats, struct statvfs *stvfs)
{
	const fx_fsattr_t *fsattr = &stats->attr;
	const fx_fsstat_t *fsstat = &stats->fsst;

	fx_bzero(stvfs, sizeof(*stvfs));
	stvfs->f_bsize    = (long unsigned int)(FNX_BLKSIZE);
	stvfs->f_frsize   = (long unsigned int)(FNX_FRGSIZE);
	stvfs->f_blocks   = fx_bkcnt_to_fsblkcnt(fsstat->f_blocks);
	stvfs->f_bfree    = fx_bkcnt_to_fsblkcnt(fsstat->f_bfree);
	stvfs->f_bavail   = fx_bkcnt_to_fsblkcnt(fsstat->f_bavail);
	stvfs->f_files    = fsstat->f_inodes;
	stvfs->f_ffree    = fsstat->f_ifree;
	stvfs->f_favail   = fsstat->f_iavail;
	stvfs->f_namemax  = FNX_NAME_MAX;
	stvfs->f_flag     = fsattr->f_mntf;

	/*
	 * In 'man fstatfs(2)' we find the statment that "Nobody knows what f_fsid
	 * is supposed to contain (but see below)", yet that "the general idea is
	 * that f_fsid contains some random stuff such that the pair (f_fsid,ino)
	 * uniquely determines a file".
	 *
	 * NB: Trying to do our best; multiple funex mounts will have same fsid.
	 */
	stvfs->f_fsid = FNX_FSMAGIC;
}


static void
fill_entry_param(struct fuse_entry_param *entry, const fx_iattr_t *iattr,
                 fx_size_t attr_timeout, fx_size_t entry_timeout)
{
	fuse_ino_t  ino;

	fx_bzero(entry, sizeof(*entry));
	ino = ino_to_fuse_ino(iattr->i_ino);
	entry->ino           = ino;
	entry->generation    = iattr->i_gen;
	if ((attr_timeout > 0) && (entry_timeout > 0)) {
		entry->attr_timeout  = (float)attr_timeout + 0.05f;
		entry->entry_timeout = (float)entry_timeout + 0.05f;
	}

	iattr_to_stat(iattr, &entry->attr);
	entry->attr.st_ino = ino;
}

static void
fill_file_info(struct fuse_file_info *file_info, const fx_fileref_t *fref)
{
	size_t sz;

	fx_assert(fref != NULL);
	fx_bzero(file_info, sizeof(*file_info));

	sz = fx_min(sizeof(file_info->fh), sizeof(void *));
	memcpy(&file_info->fh, &fref, sz);

	/* NB: direct_io=1 causes mmap to fail */
	file_info->direct_io    = fref->f_direct_io;
	file_info->keep_cache   = fref->f_keep_cache;
	file_info->nonseekable  = fref->f_nonseekable;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fuse_reply_none(fx_fuse_req_t req)
{
	fuse_reply_none(req);
}

static void fx_fuse_reply_req(fx_fuse_req_t req, int st)
{
	int rc;

	if (st < 0) {
		st = -st;
	}
	rc = fuse_reply_err(req, st);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_invalid(fx_fuse_req_t req)
{
	fx_fuse_reply_req(req, EINVAL);
}

void fx_fuse_reply_badname(fx_fuse_req_t req)
{
	fx_fuse_reply_req(req, ENAMETOOLONG);
}

void fx_fuse_reply_nosys(fx_fuse_req_t req)
{
	fx_fuse_reply_req(req, ENOSYS);
}

void fx_fuse_reply_notsupp(fx_fuse_req_t req)
{
	fx_fuse_reply_req(req, EOPNOTSUPP);
}

void fx_fuse_reply_nomem(fx_fuse_req_t req)
{
	fx_fuse_reply_req(req, ENOMEM);
}

void fx_fuse_reply_status(const fx_task_t *task)
{
	fx_fuse_reply_req(getreq(task), task_status(task));
}

void fx_fuse_reply_nwrite(const fx_task_t *task, size_t count)
{
	int rc;

	rc = fuse_reply_write(getreq(task), count);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_iattr(const fx_task_t *task, const fx_iattr_t *iattr)
{
	int rc;
	double attr_timeout;
	struct stat st;

	/* Validity timeout (in seconds) for the attributes; */
	/* TODO: Fix? */
	attr_timeout = 1.0f;

	iattr_to_stat(iattr, &st);
	st.st_ino = ino_to_fuse_ino(iattr->i_ino);

	rc = fuse_reply_attr(getreq(task), &st, attr_timeout);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_entry(const fx_task_t *task, const fx_iattr_t *iattr)
{
	int rc;
	struct fuse_entry_param entry_param;
	const fx_fuseinfo_t *info = &task->tsk_fusei->fi_info;

	fill_entry_param(&entry_param, iattr,
	                 info->attr_timeout, info->entry_timeout);
	rc = fuse_reply_entry(getreq(task), &entry_param);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_create(const fx_task_t *task, const fx_fileref_t *fref,
                          const fx_iattr_t *iattr)
{
	int rc;
	struct fuse_file_info file_info;
	struct fuse_entry_param entry_param;
	const fx_fuseinfo_t *info = &task->tsk_fusei->fi_info;

	fill_entry_param(&entry_param, iattr,
	                 info->attr_timeout, info->entry_timeout);
	fill_file_info(&file_info, fref);

	rc = fuse_reply_create(getreq(task), &entry_param, &file_info);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_readlink(const fx_task_t *task, const char *lnkval)
{
	int rc;

	rc = fuse_reply_readlink(getreq(task), lnkval);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_open(const fx_task_t *task, const fx_fileref_t *fref)
{
	int rc;
	struct fuse_file_info file_info;

	fill_file_info(&file_info, fref);

	rc = fuse_reply_open(getreq(task), &file_info);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_statvfs(const fx_task_t *task, const fx_fsinfo_t *fsinfo)
{
	int rc;
	struct statvfs stvfs;

	fsinfo_to_statvfs(fsinfo, &stvfs);
	rc = fuse_reply_statfs(getreq(task), &stvfs);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_buf(const fx_task_t *task, const void *buf, size_t bsz)
{
	int rc;
	const char *pb;

	pb = (const char *)buf;
	rc = fuse_reply_buf(getreq(task), pb, bsz);
	fx_fuse_check_rc(rc);
}

void fx_fuse_reply_zbuf(const fx_task_t *task)
{
	fx_fuse_reply_buf(task, NULL, 0);
}

void fx_fuse_reply_iobufs(const fx_task_t *task, const fx_iobufs_t *iobufs)
{
	int rc;
	size_t cnt = 0;
	fx_iovec_t iov[2 * FNX_SEGNBK];

	fx_staticassert(FX_NELEMS(iov) >= IOBUFS_NBK(iobufs));

	rc = fx_iobufs_mkiovec(iobufs, iov, fx_nelems(iov), &cnt);
	fx_assert(rc == 0);

	if (cnt == 1) {
		/* Optimizatoin for short reads */
		fx_fuse_reply_buf(task, iov[0].iov_base, iov[0].iov_len);
	} else {
		rc = fuse_reply_iov(getreq(task), iov, (int)cnt);
		fx_fuse_check_rc(rc);
	}
}

void fx_fuse_reply_ioctl(const fx_task_t *task, const void *buf, size_t bufsz)
{
	int rc, status;

	status = 0; /* FIXME */
	rc = fuse_reply_ioctl(getreq(task), status, buf, bufsz);
	fx_fuse_check_rc(rc);
}

static size_t
add_direntry(fx_fuse_req_t req, fx_ino_t child_ino, const char *name,
             fx_mode_t mode, fx_off_t off_next, void *buf, size_t bsz)
{
	size_t sz;
	struct stat st;

	fx_bzero(&st, sizeof(st));
	st.st_ino  = ino_to_fuse_ino(child_ino);
	st.st_mode = mode;

	sz = fuse_add_direntry(req, (char *)buf, bsz, name, &st, off_next);
	if (sz > bsz) {
		fx_panic("fuse_add_direntry name=%s sz=%lu", name, sz);
	}
	return sz;
}

void fx_fuse_reply_readdir(const fx_task_t *task, size_t isz,
                           fx_ino_t child_ino, const char *name,
                           fx_mode_t mode, fx_off_t off_next)
{
	size_t bsz, osz;
	char buf[2 * (FNX_NAME_MAX + 1)];
	fx_fuse_req_t req = getreq(task);

	bsz  = fx_min(sizeof(buf), isz);
	osz  = add_direntry(req, child_ino, name, mode, off_next, buf, bsz);
	fx_fuse_reply_buf(task, buf, osz);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_fuse_notify_inval_iattr(const fx_task_t *task, const fx_iattr_t *iattr)
{
	int rc;
	fx_ino_t ino;
	struct fuse_chan *ch;

	ch  = task->tsk_fuse_chan;
	ino = iattr->i_ino;
	rc  = fuse_lowlevel_notify_inval_inode(ch, ino, -1, 0);
	if (rc != 0) {
		fx_error("fuse_lowlevel_notify_inval_inode: ino=%#jx rc=%d", ino, rc);
	}
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_FUSEI_H_
#define FUNEX_FUSEI_H_


/* See 'fuse_kern_chan_new' in fuse_kern_chan.c (MIN_BUFSIZE = 0x21000) */
#define FX_FUSE_CHAN_BUFSZ   (0x1000 + FNX_SEGSIZE)

/* Debug checker */
#define FX_FUSEI_MAGIC      FNX_MAGIC9


/* Forward declarations for FUSE tpyes */
struct fuse_args;
struct fuse_chan;
struct fuse_session;
struct fuse_conn_info;
struct fuse_lowlevel_ops;

/* Hooks */
struct fx_fusei;
typedef void (*fx_fusei_sentask_fn)(struct fx_fusei *, fx_task_t *);
typedef void (*fx_fusei_deltask_fn)(struct fx_fusei *, fx_task_t *);
typedef fx_task_t *(*fx_fusei_newtask_fn)(struct fx_fusei *, fx_opcode_e);



/*
 * Parameters & Capabilities set/get upon init with FUSE
 */
struct fx_fuseinfo {
	fx_size_t   proto_major;
	fx_size_t   proto_minor;
	fx_size_t   async_read;
	fx_size_t   max_write;
	fx_size_t   max_readahead;
	fx_size_t   attr_timeout;
	fx_size_t   entry_timeout;

	fx_bool_t   cap_async_read;
	fx_bool_t   cap_posix_locks;
	fx_bool_t   cap_atomic_o_trunk;
	fx_bool_t   cap_export_support;
	fx_bool_t   cap_big_writes;
	fx_bool_t   cap_dont_mask;
};
typedef struct fx_fuseinfo fx_fuseinfo_t;


/*
 * FUSE Interface, via asynchronous tasks.
 */
struct fx_fusei {
	char                *fi_mntpoint;   /* Mount-point directory */
	int                  fi_fd;         /* Fuse dev file-descriptor */
	fx_mutex_t           fi_mutex;      /* Mount/unmount sync lock */
	fx_balloc_t         *fi_balloc;      /* Blocks allocator */
	fx_size_t            fi_seqno;      /* Requests running-sequence counter */
	fx_bool_t            fi_mounted;    /* Are we mounted? */
	fx_bool_t            fi_closed;     /* Have been closed? */
	fx_bool_t            fi_rx_done;    /* Completed Rx thread? */
	fx_bool_t            fi_active;     /* Activity flag */
	fx_magic_t           fi_magic;      /* Debug checker */
	fx_size_t            fi_chbufsz;    /* Input data buffer size*/
	void                *fi_chanbuf;    /* Data buffer (mmapped) */
	fx_usersdb_t         fi_usersdb;    /* Sys-users caching */
	fx_fuseinfo_t        fi_info;       /* FUSE params/capabilities */
	struct fuse_args    *fi_mntargs;
	struct fuse_chan    *fi_channel;
	struct fuse_session *fi_session;
	fx_fusei_sentask_fn  fi_sendreq_fn; /* Send-request hook */
	fx_fusei_newtask_fn  fi_newtask_fn; /* New-task hook */
	fx_fusei_deltask_fn  fi_deltask_fn; /* Delete-task hook */
};
typedef struct fx_fusei fx_fusei_t;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

extern const struct fuse_lowlevel_ops fx_fusei_ll_ops;


void fx_fusei_init(fx_fusei_t *, fx_balloc_t *);

void fx_fusei_destroy(fx_fusei_t *);

int fx_fusei_setup(fx_fusei_t *, int);

int fx_fusei_open(fx_fusei_t *, int, const char *);

int fx_fusei_close(fx_fusei_t *);

void fx_fusei_term(fx_fusei_t *);


void fx_fusei_process_rx(fx_fusei_t *);

void fx_fusei_execute_tx(fx_fusei_t *, fx_task_t *);


#endif /* FUNEX_FUSEI_H_ */

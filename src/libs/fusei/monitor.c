/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libmount/libmount.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "mounter.h"

/* Require mount-points to be an empty rwx dir */
static int isdir(const struct stat *st)
{
	return S_ISDIR(st->st_mode);
}

static int isemptydir(const struct stat *st)
{
	return isdir(st) && (st->st_nlink <= 2);
}

static int isrwx(const struct stat *st)
{
	const mode_t mask = S_IRWXU;

	return ((st->st_mode & mask) == mask);
}

/* Check new mount-point */
int fx_check_mntpoint(const char *mntpoint)
{
	int rc;
	struct stat st;

	errno = 0;
	rc = stat(mntpoint, &st);
	if (rc != 0) {
		fx_error("no-stat: mntpoint=%s", mntpoint);
		return -errno;
	}
	if (!isdir(&st)) {
		fx_error("not-a-directory: mntpoint=%s", mntpoint);
		return -ENOTDIR;
	}
	if (!isrwx(&st)) {
		fx_error("not-rwx: mntpoint=%s", mntpoint);
		return -EINVAL;
	}
	if (!isemptydir(&st)) {
		fx_error("not-empty: mntpoint=%s", mntpoint);
		return -ENOTEMPTY;
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void monitor_and_repair(const char *mntpoint)
{
	int rc, err = 0, flags = MNT_DETACH;
	struct stat st;

	errno = 0;
	rc = stat(mntpoint, &st);
	if (rc != 0) {
		err = -errno;
	} else if (!S_ISDIR(st.st_mode)) {
		err = -ENOTDIR;
	}

	if ((err == -ENOTCONN) || (err == -ENOTDIR)) {
		err = umount2(mntpoint, flags);
		if (err == 0) {
			fx_info("umount: mntpoint=%s flags=%#x", mntpoint, flags);
		} else {
			fx_error("no-umount: mntpoint=%s flags=%x errno=%d",
			         mntpoint, flags, errno);
		}
	}
}

void fx_monitor_mounts(void)
{
	char const *target, *mntfile;
	struct libmnt_table *tb;
	struct libmnt_iter *itr;
	struct libmnt_fs *fs;

	mntfile = "/proc/self/mountinfo";
	tb = mnt_new_table_from_file(mntfile);
	if (tb == NULL) {
		fx_error("no-new-table: mntfile=%s", mntfile);
	}

	itr = mnt_new_iter(MNT_ITER_FORWARD);
	while (mnt_table_next_fs(tb, itr, &fs) == 0) {
		if (mnt_fs_match_fstype(fs, FNX_MNTFSTYPE)) {
			target  = mnt_fs_get_target(fs);
			monitor_and_repair(target);
		}
	}
	mnt_free_iter(itr);
	mnt_free_table(tb);
}

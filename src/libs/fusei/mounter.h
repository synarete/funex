/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_MOUNTER_H_
#define FUNEX_MOUNTER_H_

enum FNX_MNTCMD_TYPE {
	FNX_MNTCMD_NONE,
	FNX_MNTCMD_STATUS,
	FNX_MNTCMD_MOUNT,
	FNX_MNTCMD_UMOUNT,
};
typedef enum FNX_MNTCMD_TYPE fx_mntcmd_e;


struct fx_mntmsg {
	fx_magic_t      magic;
	fx_version_t    version;
	fx_status_t     status;
	fx_mntcmd_e     cmd;
	fx_mntf_t       mntf;
	fx_size_t       plen;
	char            path[FNX_PATH_MAX];
};
typedef struct fx_mntmsg fx_mntmsg_t;


/* Mounting server */
struct fx_mntsrv {
	fx_bool_t       active;         /* Activation flag */
	fx_thread_t     thread;         /* Executer-thread */
	fx_sockaddr_t   addr;           /* UNIX-domain socket address */
	fx_socket_t     sock;           /* Listen socket */
	fx_socket_t     conn;           /* Currnet connection */
	fx_ucred_t      ucred;          /* Peer's credentials */
	fx_mntmsg_t     msgbuf;         /* Message buffer */
	int             mntfd;          /* Ancillary mount file-descriptor */
};
typedef struct fx_mntsrv fx_mntsrv_t;



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Server-size functions
 */
void fx_mntsrv_init(fx_mntsrv_t *);

void fx_mntsrv_destroy(fx_mntsrv_t *);

int fx_mntsrv_open(fx_mntsrv_t *, const char *);

int fx_mntsrv_close(fx_mntsrv_t *);

int fx_mntsrv_start(fx_mntsrv_t *);

int fx_mntsrv_stop(fx_mntsrv_t *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Client-side functions: RPC-like communication with extranal privileged
 * mounting dameon. Enables mount/umount operations via sendmsg/recvmsg over
 * UNIX-domain sockets.
 */
int fx_rpc_mstat(const char *, const char *, int *);

int fx_rpc_mount(const char *, const char *, fx_mntf_t, int *);

int fx_rpc_umount(const char *, const char *);


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Monitor mount-points helpers.
 */
int fx_check_mntpoint(const char *);

void fx_monitor_mounts(void);


#endif /* FUNEX_MOUNTER_H_ */


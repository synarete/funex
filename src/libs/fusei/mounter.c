/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "mounter.h"

/* Locals */
static void execute_mntsrv(void *p);


/* See: linux/Documentation/filesystems/fuse.txt for mount-data format */
static int do_mount(const char *target, const fx_ucred_t *cred , int *pfd)
{
	int rc, fd;
	size_t sz;
	char data[512] = "";
	const char fstype[]     = FNX_MNTFSTYPE;
	const mode_t rootmode   = S_IFDIR;
	const unsigned long flags = MS_NOSUID | MS_NODEV; /* TODO: Use mntf */

	fd = open("/dev/fuse", O_RDWR);
	if (fd < 0) {
		return -errno;
	}

	sz = sizeof(data) - 1;
	snprintf(data, sz, "allow_other,fd=%d,rootmode=%o,user_id=%d,group_id=%d",
	         fd, rootmode, cred->uid, cred->gid);

	rc = mount("funex", target, fstype, flags, data);
	if (rc != 0) {
		fx_warn("no-mount: target=%s flags=%#lx data=%s errno=%d",
		        target, flags, data, errno);
		close(fd);
		return -errno;
	}

	*pfd = fd;
	fx_info("mount: pid=%d uid=%d target=%s flags=%lx data=%s",
	        cred->pid, cred->uid, target, flags, data);
	return 0;
}

static int do_umount(const char *target, const fx_ucred_t *cred)
{
	int rc;

	rc = umount2(target, MNT_DETACH);
	if (rc != 0) {
		fx_warn("no-umount: pid=%d uid=%d target=%s",
		        cred->pid, cred->uid, target);
		return -errno;
	}
	fx_warn("umount: pid=%d uid=%d target=%s", cred->pid, cred->uid, target);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void mntmsg_setup(fx_mntmsg_t *mntmsg, fx_mntcmd_e cmd, int status)
{
	fx_bzero(mntmsg, sizeof(*mntmsg));
	mntmsg->version = FNX_FSVERSION;
	mntmsg->cmd     = cmd;
	mntmsg->status  = status;
}

static void mntmsg_reset(fx_mntmsg_t *mntmsg)
{
	mntmsg_setup(mntmsg, FNX_MNTCMD_NONE, 0);
}


static void
fillmsg2(struct msghdr *msgh, struct iovec *iov,
         size_t iov_len, void *ctl, size_t clen)
{
	fx_bzero(msgh, sizeof(*msgh));
	msgh->msg_iov        = iov;
	msgh->msg_iovlen     = iov_len;
	msgh->msg_control    = ctl;
	msgh->msg_controllen = clen;
}

static void
fillmsg(struct msghdr *msgh, struct iovec *iov, size_t iov_len)
{
	fillmsg2(msgh, iov, iov_len, NULL, 0);
}

static void filliov(struct iovec *iov, void *ptr, size_t len)
{
	iov[0].iov_base = ptr;
	iov[0].iov_len  = len;
}

static int geterr(int rc)
{
	return ((rc != 0) && (errno != 0)) ? -errno : rc;
}

static int sendxmsg(const fx_socket_t *sock, struct msghdr *msgh)
{
	int rc;

	rc = fx_tcpsock_sendmsg(sock, msgh, 0);
	return geterr(rc);
}

static int recvxmsg(const fx_socket_t *sock, struct msghdr *msgh)
{
	int rc;

	rc = fx_tcpsock_recvmsg(sock, msgh, 0);
	return geterr(rc);
}

/*: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : :*/
/* Server: */

void fx_mntsrv_init(fx_mntsrv_t *mntsrv)
{
	fx_bzero(mntsrv, sizeof(*mntsrv));
	fx_thread_init(&mntsrv->thread);
	fx_tcpsock_initu(&mntsrv->sock);
	fx_tcpsock_initu(&mntsrv->conn);
	mntsrv->active  = FNX_FALSE;
	mntsrv->mntfd   = -1;
}

void fx_mntsrv_destroy(fx_mntsrv_t *mntsrv)
{
	fx_tcpsock_destroy(&mntsrv->conn);
	fx_tcpsock_destroy(&mntsrv->sock);
	fx_thread_destroy(&mntsrv->thread);
}

int fx_mntsrv_open(fx_mntsrv_t *mntsrv, const char *path)
{
	int rc, backlog = FNX_MOUNT_MAX;
	fx_socket_t *sock;
	fx_sockaddr_t *addr;

	sock = &mntsrv->sock;
	addr = &mntsrv->addr;

	rc = fx_sockaddr_setunix(addr, path);
	if (rc != 0) {
		fx_error("illegal-unix-sock: path=%s", path);
		return -1;
	}
	rc = fx_tcpsock_open(sock);
	if (rc != 0) {
		fx_error("no-sock-open: addr=%s errno=%d", path, errno);
		return -1;
	}
	rc = fx_tcpsock_bind(sock, addr);
	if (rc != 0) {
		fx_error("no-sock-bind: addr=%s errno=%d", path, errno);
		fx_tcpsock_close(sock);
		return -1;
	}
	rc = fx_tcpsock_listen(sock, backlog);
	if (rc != 0) {
		fx_error("listen-failure: addr=%s errno=%d", path, errno);
		fx_tcpsock_close(sock);
		return -1;
	}
	return 0;
}

int fx_mntsrv_close(fx_mntsrv_t *mntsrv)
{
	fx_socket_t *sock;
	fx_sockaddr_t *addr;

	sock = &mntsrv->sock;
	addr = &mntsrv->addr;

	if (sock->fd > 0) {
		fx_tcpsock_close(sock);
	}
	if (strlen(addr->un.sun_path)) {
		unlink(addr->un.sun_path);
	}
	return 0;
}

int fx_mntsrv_start(fx_mntsrv_t *mntsrv)
{
	fx_thread_t *thread = &mntsrv->thread;

	mntsrv->active = FNX_TRUE;
	fx_thread_setname(thread, "fx-mntsrv");
	fx_thread_start(thread, execute_mntsrv, mntsrv);
	return 0;
}

int fx_mntsrv_stop(fx_mntsrv_t *mntsrv)
{
	mntsrv->active = FNX_FALSE;
	fx_join_thread(&mntsrv->thread, FX_NOFAIL);
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void exec_mount(fx_mntsrv_t *mntsrv)
{
	int rc, fd = -1;
	const char *path;
	const fx_ucred_t *cred;

	path = mntsrv->msgbuf.path;
	cred = &mntsrv->ucred;

	rc = fx_check_mntpoint(path);
	if (rc != 0) {
		mntsrv->msgbuf.status = rc;
		return;
	}
	/* TODO: Extra checks here */
	rc = do_mount(path, cred, &fd);
	if (rc != 0) {
		mntsrv->msgbuf.status = rc;
		return;
	}
	mntsrv->msgbuf.status = 0;
	mntsrv->mntfd = fd;
}

static void exec_umount(fx_mntsrv_t *mntsrv)
{
	int rc;
	const char *path;
	const fx_ucred_t *cred;

	path = mntsrv->msgbuf.path;
	cred = &mntsrv->ucred;

	rc = do_umount(path, cred);
	if (rc != 0) {
		mntsrv->msgbuf.status = rc;
		return;
	}
	mntsrv->msgbuf.status = 0;
	mntsrv->mntfd = -1;
}

static void exec_status(fx_mntsrv_t *mntsrv)
{
	int rc = 0;
	const char *path;
	struct stat st;

	path = mntsrv->msgbuf.path;
	if (strlen(path)) {
		rc = stat(path, &st);
		if (rc != 0) {
			rc = -errno;
		} else if (!S_ISDIR(st.st_mode)) {
			rc = -ENOTDIR;
		}
	}

	mntsrv->msgbuf.status = rc;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int recv_request(fx_mntsrv_t *mntsrv)
{
	int rc;
	struct iovec iov[1];
	struct msghdr msgh;
	fx_mntmsg_t *reqmsg;

	reqmsg = &mntsrv->msgbuf;
	mntmsg_reset(reqmsg);
	filliov(iov, reqmsg, sizeof(*reqmsg));
	fillmsg(&msgh, iov, 1);

	rc = recvxmsg(&mntsrv->conn, &msgh);
	if (rc != 0) {
		fx_trace1("no-recvmsg rc=%d", rc);
	}
	/* TODO: Check recv length */
	return rc;
}

static int accept_request(fx_mntsrv_t *mntsrv)
{
	int rc;
	fx_ucred_t  *cred;
	fx_socket_t *conn;
	fx_sockaddr_t peer;
	const fx_socket_t *sock;
	const fx_timespec_t timeout = { 2, 0 };

	sock = &mntsrv->sock;
	conn = &mntsrv->conn;
	cred = &mntsrv->ucred;

	rc = fx_tcpsock_accept(sock, conn, &peer);
	if (rc != 0) {
		fx_error("accept-error: sockfd=%d errno=%d", sock->fd, errno);
		return -1;
	}
	rc = fx_tcpsock_peercred(conn, cred);
	if (rc != 0) {
		fx_error("no-peercred: sockfd=%d errno=%d", sock->fd, errno);
		fx_tcpsock_close(conn);
		return -1;
	}
	rc = fx_tcpsock_rselect(conn, &timeout);
	if (rc != 0) {
		fx_error("no-data: pid=%d uid=%d timeout={%ld,%ld} errno=%d",
		         cred->pid, cred->uid, timeout.tv_sec, timeout.tv_nsec, errno);
		fx_tcpsock_close(conn);
		return -1;
	}

	fx_info("accepted: pid=%d uid=%d gid=%d", cred->pid, cred->uid, cred->gid);
	return 0;
}

static int exec_request(fx_mntsrv_t *mntsrv)
{
	int rc = 0;
	const fx_mntmsg_t *reqmsg = &mntsrv->msgbuf;

	switch (reqmsg->cmd) {
		case FNX_MNTCMD_STATUS:
			exec_status(mntsrv);
			break;
		case FNX_MNTCMD_MOUNT:
			exec_mount(mntsrv);
			break;
		case FNX_MNTCMD_UMOUNT:
			exec_umount(mntsrv);
			break;
		case FNX_MNTCMD_NONE:
		default:
			rc = -1;
			break;
	}
	return rc;
}

static int send_response(fx_mntsrv_t *mntsrv)
{
	int rc;
	struct iovec iov[1];
	struct msghdr msgh;
	struct cmsghdr *cmsg;
	char cms[CMSG_SPACE(sizeof(int))];
	fx_mntmsg_t *resmsg = &mntsrv->msgbuf;

	filliov(iov, resmsg, sizeof(*resmsg));
	if (resmsg->status == 0) {
		if (mntsrv->mntfd > 0) {
			fillmsg2(&msgh, iov, 1, cms, sizeof(cms));
			cmsg = fx_cmsg_firsthdr(&msgh);
			fx_cmsg_pack_fd(cmsg, mntsrv->mntfd);
			msgh.msg_controllen = cmsg->cmsg_len;
		} else {
			fillmsg(&msgh, iov, 1);
			msgh.msg_controllen = 0;
		}
	} else {
		fillmsg(&msgh, iov, 1);
		msgh.msg_controllen = 0;
	}
	rc = sendxmsg(&mntsrv->conn, &msgh);
	if (rc != 0) {
		fx_warn("no-sendmsg rc=%d", rc);
	}
	mntsrv->mntfd = 0;

	return rc;
}


static void finalize_rpc(fx_mntsrv_t *mntsrv)
{
	fx_socket_t *conn;
	fx_ucred_t  *cred;
	fx_mntmsg_t *msgbuf;

	conn    = &mntsrv->conn;
	msgbuf  = &mntsrv->msgbuf;
	cred    = &mntsrv->ucred;
	if (conn->fd < 0) {
		return;
	}
	fx_info("finalize: cmd=%d pid=%d uid=%d gid=%d status=%d",
	        msgbuf->cmd, cred->pid, cred->uid, cred->gid, msgbuf->status);

	fx_tcpsock_close(conn);
	if (mntsrv->mntfd > 0) {
		close(mntsrv->mntfd);
	}

	fx_bff(cred, sizeof(*cred));
	mntmsg_reset(msgbuf);
}


static int serv_request(fx_mntsrv_t *mntsrv)
{
	int rc = 0;
	const fx_timespec_t ts  = { 1, 0 };
	const fx_socket_t *sock = &mntsrv->sock;

	rc = fx_tcpsock_rselect(sock, &ts);
	if (rc != 0) {
		goto out;
	}
	rc = accept_request(mntsrv);
	if (rc != 0) {
		goto out;
	}
	rc = recv_request(mntsrv);
	if (rc != 0) {
		goto out;
	}
	rc = exec_request(mntsrv);
	if (rc != 0) {
		goto out;
	}
	rc = send_response(mntsrv);
	if (rc != 0) {
		goto out;
	}
out:
	finalize_rpc(mntsrv);
	return rc;
}

static void execute_mntsrv(void *p)
{
	int cnt = 0, rc = 0;
	fx_mntsrv_t *mntsrv = (fx_mntsrv_t *)p;

	while (mntsrv->active) {
		cnt++;
		rc = serv_request(mntsrv);
		if ((rc != 0) && (cnt > 30)) {
			fx_monitor_mounts();
			cnt = 0;
		}
	}
}

/*: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : :*/
/* Client: */

/* Context for client-size rpc ops */
struct fx_mntclnt {
	fx_socket_t     sock;
	const char     *upath;
	fx_mntcmd_e     cmd;
	fx_mntmsg_t     msg;
	const char     *mpath;
	fx_mntf_t       mflags;
	int             err;
	int             fd;
};
typedef struct fx_mntclnt fx_mntclnt_t;


static fx_mntmsg_t *new_mntmsg(fx_mntcmd_e cmd)
{
	fx_mntmsg_t *mntmsg;

	mntmsg = (fx_mntmsg_t *)fx_malloc(sizeof(*mntmsg));
	if (mntmsg != NULL) {
		mntmsg_setup(mntmsg, cmd, 0);
	}
	return mntmsg;
}

static void del_mntmsg(fx_mntmsg_t *mntmsg)
{
	fx_free(mntmsg, sizeof(*mntmsg));
}

static int connect_mntd(fx_socket_t *sock, const char *path)
{
	int rc;
	fx_sockaddr_t addr;

	fx_tcpsock_initu(sock);
	rc = fx_sockaddr_setunix(&addr, path);
	if (rc != 0) {
		fx_error("illegal-unix-sock: path=%s", path);
		return -1;
	}
	rc = fx_tcpsock_open(sock);
	if (rc != 0) {
		fx_error("no-sock-open: path=%s errno=%d", path, errno);
		return -1;
	}
	rc = fx_tcpsock_connect(sock, &addr);
	if (rc != 0) {
		fx_error("no-connect: path=%s errno=%d", path, errno);
		fx_tcpsock_close(sock);
		return -1;
	}
	return 0;
}

static void disconnect_mntd(fx_socket_t *sock)
{
	fx_tcpsock_shutdown(sock);
	fx_tcpsock_close(sock);
	fx_tcpsock_destroy(sock);
}

static int
rpc_mount(const fx_socket_t *sock, fx_mntmsg_t *msgbuf,
          const char *path, fx_mntf_t mntf, int *fd)
{
	int rc = 0, status = 0;
	struct iovec iov[1];
	struct msghdr msgh;
	char cms[CMSG_SPACE(sizeof(int))];
	struct cmsghdr *cmsg;

	msgbuf->mntf = mntf;
	strncpy(msgbuf->path, path, sizeof(msgbuf->path));

	filliov(iov, msgbuf, sizeof(*msgbuf));
	fillmsg(&msgh, iov, 1);
	if ((rc = sendxmsg(sock, &msgh)) != 0) {
		return rc;
	}
	fillmsg2(&msgh, iov, 1, cms, sizeof(cms));
	if ((rc = recvxmsg(sock, &msgh)) != 0) {
		return rc;
	}
	status = msgbuf->status;
	if (status != 0) {
		rc = status;
		return rc;
	}
	cmsg = fx_cmsg_firsthdr(&msgh);
	fx_assert(cmsg != NULL);
	rc = fx_cmsg_unpack_fd(cmsg, fd);
	fx_assert(rc == 0);

	/* TODO: Check resmsg (version ,cmd..) */

	return rc;
}

int fx_rpc_mount(const char *usock, const char *mntpath, fx_mntf_t mntf,
                 int *fd)
{
	int rc;
	fx_socket_t sock;
	fx_mntmsg_t *msgbuf;

	msgbuf = new_mntmsg(FNX_MNTCMD_MOUNT);
	if (msgbuf == NULL) {
		return -ENOMEM;
	}
	rc = connect_mntd(&sock, usock);
	if (rc != 0) {
		del_mntmsg(msgbuf);
		return rc;
	}
	rc = rpc_mount(&sock, msgbuf, mntpath, mntf, fd);
	if (rc != 0) {
		disconnect_mntd(&sock);
		del_mntmsg(msgbuf);
		return rc;
	}
	disconnect_mntd(&sock);
	del_mntmsg(msgbuf);
	fx_info("rpc-mount: mntpath=%s fd=%d", mntpath, *fd);
	return 0;
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_REPLY_H_
#define FUNEX_REPLY_H_


void fx_fuse_reply_none(fx_fuse_req_t);

void fx_fuse_reply_invalid(fx_fuse_req_t);

void fx_fuse_reply_badname(fx_fuse_req_t);

void fx_fuse_reply_nosys(fx_fuse_req_t);

void fx_fuse_reply_notsupp(fx_fuse_req_t);

void fx_fuse_reply_nomem(fx_fuse_req_t);


void fx_fuse_reply_status(const fx_task_t *);

void fx_fuse_reply_nwrite(const fx_task_t *, size_t);

void fx_fuse_reply_iattr(const fx_task_t *, const fx_iattr_t *);

void fx_fuse_reply_entry(const fx_task_t *, const fx_iattr_t *);

void fx_fuse_reply_readlink(const fx_task_t *, const char *);

void fx_fuse_reply_open(const fx_task_t *, const fx_fileref_t *);

void fx_fuse_reply_statvfs(const fx_task_t *, const fx_fsinfo_t *);

void fx_fuse_reply_buf(const fx_task_t *, const void *, size_t);

void fx_fuse_reply_zbuf(const fx_task_t *);

void fx_fuse_reply_iobufs(const fx_task_t *, const fx_iobufs_t *);

void fx_fuse_reply_ioctl(const fx_task_t *, const void *, size_t);

void fx_fuse_reply_fsinfo(const fx_task_t *, const fx_fsinfo_t *);

void fx_fuse_reply_create(const fx_task_t *, const fx_fileref_t *,
                          const fx_iattr_t *);

void fx_fuse_reply_readdir(const fx_task_t *, size_t, fx_ino_t,
                           const char *, fx_mode_t, fx_off_t);


void fx_fuse_notify_inval_iattr(const fx_task_t *, const fx_iattr_t *);


#endif /* FUNEX_REPLY_H_ */





/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FUNEX_USERS_H_
#define FUNEX_USERS_H_


/* User's entry for fusei-layer caching */
struct fx_user {
	fx_link_t   u_link;                 /* Free-pool link */
	fx_tlink_t  u_tlnk;                 /* Mapping link */
	fx_time_t   u_time;                 /* Setup time (0=empty) */
	fx_uid_t    u_uid;                  /* Effective system UID */
	fx_capf_t   u_capf;                 /* FS capabilities mask */
	fx_size_t   u_ngids;                /* Supplementary groups */
	fx_gid_t    u_gids[FNX_NGROUPS_MAX];
};
typedef struct fx_user fx_user_t;


/* In-memory cache ("database") of currently active users */
struct fx_usersdb {
	fx_tree_t   users;                  /* Users' map */
	fx_list_t   frees;                  /* Free-pool */
	fx_user_t  *last;                   /* Last-accessed entry */
	fx_user_t   pool[FNX_NUSERS_MAX];   /* User-entries pool */
};
typedef struct fx_usersdb fx_usersdb_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_user_update(fx_user_t *, fx_pid_t, fx_fuse_req_t);

void fx_user_setctx(const fx_user_t *, fx_uctx_t *);


void fx_usersdb_init(fx_usersdb_t *);

void fx_usersdb_destroy(fx_usersdb_t *);

fx_user_t *fx_usersdb_lookup(fx_usersdb_t *, fx_uid_t);

fx_user_t *fx_usersdb_insert(fx_usersdb_t *, fx_uid_t);


#endif /* FUNEX_USERS_H_ */





/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/capability.h>

#define FUSE_USE_VERSION    29
#include <fuse/fuse_lowlevel.h>
#include <fuse/fuse_common.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#include "users.h"


static int cap_iseset(cap_t cap_p, cap_value_t value)
{
	int rc, res;
	cap_flag_value_t flag = 0;

	rc  = cap_get_flag(cap_p, value, CAP_EFFECTIVE, &flag);
	res = ((rc == 0) && (flag == CAP_SET));
	return res;
}

static int capf_by_pid(fx_pid_t pid, fx_capf_t *capf)
{
	cap_t cap;

	*capf = 0;
	cap = cap_get_pid(pid);
	if (cap == NULL) {
		return -1;
	}

	if (cap_iseset(cap, CAP_CHOWN)) {
		fx_setlf(capf, FNX_CAPF_CHOWN);
	}
	if (cap_iseset(cap, CAP_FOWNER)) {
		fx_setlf(capf, FNX_CAPF_FOWNER);
	}
	if (cap_iseset(cap, CAP_FSETID)) {
		fx_setlf(capf, FNX_CAPF_FSETID);
	}
	if (cap_iseset(cap, CAP_SYS_ADMIN)) {
		fx_setlf(capf, FNX_CAPF_ADMIN);
	}
	if (cap_iseset(cap, CAP_MKNOD)) {
		fx_setlf(capf, FNX_CAPF_MKNOD);
	}

	if (cap_free(cap) != 0) {
		fx_panic("cap_free cap=%p", (void *)cap);
	}
	return 0;
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void user_init(fx_user_t *user)
{
	fx_bzero(user, sizeof(*user));
	fx_link_init(&user->u_link);
	fx_tlink_init(&user->u_tlnk);
	user->u_time    = 0;
	user->u_uid     = (fx_uid_t)FNX_UID_NULL;
	user->u_capf    = 0;
	user->u_ngids   = 0;
}

static void user_destroy(fx_user_t *user)
{
	fx_link_destroy(&user->u_link);
	fx_tlink_destroy(&user->u_tlnk);
	user->u_time    = 0;
	user->u_uid     = (fx_uid_t)FNX_UID_NULL;
	user->u_capf    = 0;
	user->u_ngids   = 0;
}

static void user_setup(fx_user_t *user, fx_uid_t uid)
{
	user->u_uid     = uid;
	user->u_time    = time(NULL);
	user->u_capf    = 0;
	user->u_ngids   = 0;
}

static fx_user_t *link_to_user(const fx_link_t *lnk)
{
	const fx_user_t *user;

	user = fx_container_of(lnk, fx_user_t, u_link);
	return (fx_user_t *)user;
}

static fx_user_t *tlink_to_user(const fx_tlink_t *tlnk)
{
	return fx_container_of(tlnk, fx_user_t, u_tlnk);
}

static void user_getgroups(fx_user_t *user, fx_fuse_req_t req)
{
	int ngid, cnt;
	gid_t gids[FNX_NGROUPS_MAX];

	fx_bff(gids, sizeof(gids));
	ngid = (int)FX_NELEMS(gids);
	cnt  = fuse_req_getgroups(req, ngid, gids);
	if (cnt < 0) {
		fx_error("fuse_req_getgroups-failed: uid=%d rc=%d", user->u_uid, cnt);
		return;
	}

	user->u_ngids = fx_min((size_t)cnt, FX_NELEMS(user->u_gids));
	for (size_t i = 0; i < user->u_ngids; ++i) {
		user->u_gids[i] = gids[i];
	}
}

void fx_user_update(fx_user_t *user, fx_pid_t pid, fx_fuse_req_t req)
{
	int rc;
	fx_capf_t capf = 0;

	rc = capf_by_pid(pid, &capf);
	if (rc == 0) {
		user->u_capf = capf;
	}

	if (req != NULL) {
		user_getgroups(user, req);
	}
}

void fx_user_setctx(const fx_user_t *user, fx_uctx_t *uctx)
{
	fx_cred_t *cred;

	cred = &uctx->u_cred;
	cred->cr_ngids = fx_min(user->u_ngids, FX_NELEMS(cred->cr_gids));
	for (size_t i = 0; i < cred->cr_ngids; ++i) {
		cred->cr_gids[i] = user->u_gids[i];
	}

	uctx->u_capf = user->u_capf;
	if (user->u_uid == 0) {
		uctx->u_root = FNX_TRUE;
		uctx->u_capf |= FNX_CAPF_ALL;
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static const void *getkey_user(const fx_tlink_t *tlnk)
{
	const fx_user_t  *user;

	user = tlink_to_user(tlnk);
	return &user->u_uid;
}

static int keycmp_uid(const void *k1, const void *k2)
{
	const fx_uid_t *uid1 = (const fx_uid_t *)(k1);
	const fx_uid_t *uid2 = (const fx_uid_t *)(k2);

	return fx_ucompare3(*uid1, *uid2);
}

static const fx_treehooks_t s_usersmap_hooks = {
	.getkey_hook = getkey_user,
	.keycmp_hook = keycmp_uid
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

void fx_usersdb_init(fx_usersdb_t *usersdb)
{
	fx_tree_init(&usersdb->users, FX_TREE_AVL, &s_usersmap_hooks);
	fx_list_init(&usersdb->frees);
	fx_foreach_arrelem(usersdb->pool, user_init);

	for (size_t i = 0; i < FX_NELEMS(usersdb->pool); ++i) {
		fx_list_push_front(&usersdb->frees, &usersdb->pool[i].u_link);
	}
	usersdb->last = NULL;
}

void fx_usersdb_destroy(fx_usersdb_t *usersdb)
{
	fx_foreach_arrelem(usersdb->pool, user_destroy);
	fx_list_destroy(&usersdb->frees);
	fx_tree_destroy(&usersdb->users);
}

fx_user_t *fx_usersdb_lookup(fx_usersdb_t *usersdb, fx_uid_t uid)
{
	fx_tlink_t *tlnk;
	fx_user_t  *user = NULL;

	/* Optimization: fast-lookup to last accessed user */
	if ((usersdb->last != NULL) &&
	    (usersdb->last->u_uid == uid)) {
		return usersdb->last;
	}

	/* Noraml case: tree lookup */
	tlnk = fx_tree_find(&usersdb->users, &uid);
	if (tlnk != NULL) {
		user = tlink_to_user(tlnk);
		usersdb->last = user;
	}
	return user;
}

static fx_user_t *usersdb_evict(fx_usersdb_t *usersdb)
{
	fx_tlink_t *end, *itr;
	fx_user_t *user = NULL;

	itr = fx_tree_begin(&usersdb->users);
	end = fx_tree_end(&usersdb->users);
	while (itr != end) {
		user = tlink_to_user(itr);
		if (user != usersdb->last) {
			fx_tree_remove(&usersdb->users, itr);
			break;
		}
	}
	return user;
}

static fx_user_t *usersdb_getfree(fx_usersdb_t *usersdb)
{
	fx_link_t  *lnk;
	fx_user_t *user = NULL;

	lnk = fx_list_pop_front(&usersdb->frees); /* Pop free entry */
	if (lnk != NULL) {
		user = link_to_user(lnk);
	} else {
		user = usersdb_evict(usersdb);
	}

	fx_assert(user != NULL);
	return user;
}

fx_user_t *fx_usersdb_insert(fx_usersdb_t *usersdb, fx_uid_t uid)
{
	int rc;
	fx_user_t *user;

	user = usersdb_getfree(usersdb);
	user_setup(user, uid);
	rc = fx_tree_insert_unique(&usersdb->users, &user->u_tlnk);
	fx_assert(rc == 0);

	return user;
}

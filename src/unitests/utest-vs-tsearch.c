/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <search.h>
#include "fnxinfra.h"
#include "randutil.h"

#define MAGIC       (0x6A1C13B)
#define MAX_SIZE    503


struct keyval {
	fx_tlink_t link;
	int magic;
	int key;
};
typedef struct keyval   keyval_t;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static keyval_t *tlnk_to_keyval(const fx_tlink_t *tlnk)
{
	const keyval_t *kv;

	kv = fx_container_of(tlnk, keyval_t, link);
	return (keyval_t *)kv;
}

static const void *keyval_getkey(const fx_tlink_t *x)
{
	const keyval_t *kv;

	kv = tlnk_to_keyval(x);
	return &kv->key;
}

static int keyval_keycmp(const void *x_k, const void *y_k)
{
	const int *x_key;
	const int *y_key;

	x_key = (const int *)(x_k);
	y_key = (const int *)(y_k);

	return (*y_key) - (*x_key);
}

static void keyval_check(const keyval_t *kv)
{
	fx_assert(kv->magic == MAGIC);
}

static void keyval_init(keyval_t *kv, int key)
{
	fx_tlink_init(&kv->link);
	kv->magic   = MAGIC;
	kv->key     = key;
}

static void keyval_destroy(keyval_t *kv)
{
	keyval_check(kv);
	kv->magic = kv->key = 0;
}

static keyval_t *keyval_new(int key)
{
	int flags = FX_BZERO | FX_NOFAIL;
	keyval_t *kv;

	kv = fx_xmalloc(sizeof(*kv), flags);
	keyval_init(kv, key);

	return kv;
}

static void keyval_delete(keyval_t *kv)
{
	keyval_destroy(kv);
	fx_free(kv, sizeof(*kv));
}

static const fx_treehooks_t s_tree_hooks = {
	.getkey_hook    = keyval_getkey,
	.keycmp_hook    = keyval_keycmp
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int tsearch_compare(const void *x, const void *y)
{
	int k_x, k_y;

	k_x = *(const int *)x;
	k_y = *(const int *)y;

	return (k_y - k_x);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
check_equal_trees(fx_tree_t *tree, void **rootp, int *keys, size_t nkeys)
{
	size_t i;
	int key;
	fx_tlink_t *itr = NULL;
	keyval_t *kv     = NULL;
	void *p;
	const int **z;

	for (i = 0; i < nkeys; ++i) {
		key = keys[i];
		itr = fx_tree_find(tree, &key);
		fx_assert(itr != NULL);
		kv  = tlnk_to_keyval(itr);
		keyval_check(kv);

		p = tfind(&key, rootp, tsearch_compare);
		z = (const int **)p;
		fx_assert(p != NULL);
		fx_assert(**z == kv->key);
	}
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
test_tree_insert_remove(fx_tree_t *tree, void **rootp, int *keys, size_t nkeys)
{
	int rc, key;
	size_t i;
	fx_tlink_t *itr = NULL;
	keyval_t *kv     = NULL;
	void *p;
	int **z;

	for (i = 0; i < nkeys; ++i) {
		key = keys[i];
		kv  = keyval_new(key);

		itr = &kv->link;
		rc  = fx_tree_insert_unique(tree, itr);
		fx_assert(rc == 0);

		p   = tsearch(&keys[i], rootp, tsearch_compare);
		fx_assert(p != NULL);
		z   = (int **)p;
		fx_assert(**z == key);

		check_equal_trees(tree, rootp, keys, i + 1);
	}

	for (i = 0; i < nkeys; ++i) {
		key = keys[i];
		itr = fx_tree_find(tree, &key);
		kv  = tlnk_to_keyval(itr);
		keyval_check(kv);

		fx_tree_remove(tree, itr);
		keyval_delete(kv);

		p = tdelete(&keys[i], rootp, tsearch_compare);
		fx_assert(p != NULL);

		check_equal_trees(tree, rootp, keys + i + 1, nkeys - i - 1);
	}
}

static void test_trees(size_t nkeys, struct random_data *rnd_dat)
{
	size_t i, nkeys_sz;
	fx_treetype_e types[] = { FX_TREE_AVL, FX_TREE_RB, FX_TREE_TREAP };
	fx_treetype_e tree_type;
	fx_tree_t tree;
	void *rootp = NULL;
	int *keys;

	nkeys_sz = sizeof(int) * nkeys;
	keys = fx_xmalloc(nkeys_sz, FX_NOFAIL);
	for (i = 0; i < FX_ARRAYSIZE(types); ++i) {
		generate_keys(keys, nkeys, 1);
		random_shuffle(keys, nkeys, rnd_dat);

		tree_type = types[i];
		fx_tree_init(&tree, tree_type, &s_tree_hooks);
		test_tree_insert_remove(&tree, &rootp, keys, nkeys);
		fx_tree_destroy(&tree);
		rootp = NULL;
	}
	fx_free(keys, nkeys_sz);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
int main(void)
{
	size_t nkeys = MAX_SIZE;
	struct random_data *rnd_dat;

	rnd_dat = create_randomizer();
	test_trees(nkeys, rnd_dat);

	return EXIT_SUCCESS;
}



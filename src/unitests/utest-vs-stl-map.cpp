/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>

#include <utility>
#include <algorithm>
#include <map>
#include <set>
#include <list>
#include <vector>
#include <pthread.h>
#include "fnxinfra.h"
#include "randutil.h"


// Defs:
#define MAGIC           (0xDA337FC)
#define COUNT           301111


typedef std::map<int, int>      Map;

struct map_node {
	fx_tlink_t link;
	int key;
	int value;
	int magic;
};
typedef struct map_node     map_node_t;

static size_t s_count_max = COUNT;
static int    s_full_test = 0;



/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static const void *getkey(const fx_tlink_t *x)
{
	const map_node_t *x_kv = reinterpret_cast<const map_node_t *>(x);
	return &x_kv->key;
}

static int keycmp(const void *x_k, const void *y_k)
{
	const int *x_key = static_cast<const int *>(x_k);
	const int *y_key = static_cast<const int *>(y_k);

	return ((*y_key) - (*x_key));
}

static int mkvalue(int key)
{
	return abs((key * (key + 19)) ^ MAGIC);
}

static void check_node(const map_node_t *p_node)
{
	fx_assert(p_node != 0);
	fx_assert(p_node->magic == MAGIC);
}

static map_node_t *node_from_tlink(const fx_tlink_t *x)
{
	return fx_container_of(x, map_node_t, link);
}

static fx_tlink_t *new_node(int key, int value)
{
	map_node_t *p_kv = new map_node_t;
	fx_bzero(p_kv, sizeof(*p_kv));

	p_kv->key       = key;
	p_kv->value     = value;
	p_kv->magic     = MAGIC;

	return &(p_kv->link);
}

static void delete_node(fx_tlink_t *x, void *ptr)
{
	map_node_t *p_kv = node_from_tlink(x);
	check_node(p_kv);
	p_kv->magic = 1;
	delete p_kv;
	fx_unused(ptr);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
template <typename Iterator>
void check_equal(const Iterator &stl_itr, const fx_tlink_t *fx_itr)
{
	const map_node_t *p_node;

	p_node = fx_container_of(fx_itr, map_node_t, link);
	check_node(p_node);

	const int key1 = stl_itr->first;
	const int key2 = p_node->key;
	fx_assert(key1 == key2);

	const int value1 = stl_itr->second;
	const int value2 = p_node->value;
	fx_assert(value1 == value2);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

template <typename MapT>
void check_equal(const MapT &stl_map,
                 const fx_tree_t *fx_map, size_t cnt = (2 * COUNT))
{
	typedef typename MapT::const_iterator   const_iterator;

	int key;
	size_t n, stl_map_sz, fx_map_sz;

	n = cnt;
	stl_map_sz = stl_map.size();
	fx_map_sz = fx_tree_size(fx_map);
	fx_assert(stl_map_sz == fx_map_sz);

	const_iterator stl_iterator = stl_map.begin();
	const fx_tlink_t *fx_iterator = fx_tree_begin(fx_map);
	while ((stl_iterator != stl_map.end()) && (n-- > 0)) {
		check_equal(stl_iterator, fx_iterator);

		key = stl_iterator->first;

		const_iterator stl_iterator2 = stl_map.find(key);
		const fx_tlink_t *fx_iterator2  = fx_tree_find(fx_map, &key);
		check_equal(stl_iterator2, fx_iterator2);

		++stl_iterator;
		fx_iterator = fx_tree_next(fx_map, fx_iterator);
	}

	if (cnt >= fx_map_sz) {
		const fx_tlink_t *fx_iterator_end = fx_tree_end(fx_map);
		fx_assert(fx_iterator == fx_iterator_end);
	}
}

template <typename MapT>
void check_equal_light(const MapT &stl_map, const fx_tree_t *fx_map)
{
	size_t sz;

	sz = stl_map.size();
	if (sz > 0) {
		sz = std::min(sz, size_t(17));
		check_equal(stl_map, fx_map, sz);
	}
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static int select_existing_elem(const Map &stl_map, int *p_key)
{
	typedef Map::const_iterator stl_iterator;

	size_t sz, n;
	stl_iterator stl_itr;

	sz = stl_map.size();
	if (sz == 0) {
		return -1;
	}

	stl_itr = stl_map.begin();
	n = sz % 17;
	while (n-- > 0) {
		++stl_itr;
	}
	*p_key = stl_itr->second;
	return 0;

}

static int select_random_key(struct random_data *rnd)
{
	int rc, r;

	rc  = random_r(rnd, &r);
	fx_assert(rc == 0);

	return r;
}

static void
test_map_oparations(Map &stl_map, fx_tree_t *fx_map, struct random_data *rnd)
{
	typedef Map::iterator   stl_iterator;
	typedef fx_tlink_t    *fx_iterator;

	int rc, key, val;
	stl_iterator stl_itr;
	fx_iterator fx_itr;
	fx_tlink_t *tlnk;

	for (size_t i = 0; i < s_count_max; ++i) {
		rc = -1;
		if (i % 13) {
			rc = select_existing_elem(stl_map, &key);
		}
		if (rc != 0) {
			key = select_random_key(rnd);
		}

		val = mkvalue(key);

		stl_itr = stl_map.find(key);
		fx_itr = fx_tree_find(fx_map, &key);

		if (stl_itr == stl_map.end()) {
			fx_assert(fx_itr == NULL);
			stl_map.insert(std::make_pair(key, val));
			fx_tree_insert(fx_map, new_node(key, val));
			tlnk = fx_tree_insert_replace(fx_map, new_node(key, val));
			delete_node(tlnk, NULL);
		} else if (key & 0x01) {
			stl_map.erase(stl_itr);
			fx_tree_remove(fx_map, fx_itr);
			delete_node(fx_itr, NULL);
		}

		if (s_full_test) {
			check_equal(stl_map, fx_map);
		} else {
			check_equal_light(stl_map, fx_map);
		}
	}
	check_equal(stl_map, fx_map);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_vs_stl_map(fx_treetype_e tree_type)
{
	int rc;
	size_t i, n;
	unsigned int seed;
	char statebuf[17];
	struct random_data rnd;
	Map stl_map;
	fx_tree_t fx_map;
	fx_treehooks_t ops;

	fx_bzero(statebuf, sizeof(statebuf));
	fx_bzero(&rnd, sizeof(rnd));
	fx_bzero(&fx_map, sizeof(fx_map));

	seed = static_cast<unsigned>(time(0));
	n    = FX_NELEMS(statebuf);
	for (i = 0; i < n; ++i) {
		statebuf[i] = static_cast<char>(seed + i);
	}
	rc   = initstate_r(seed, statebuf, sizeof(statebuf), &rnd);
	fx_assert(rc == 0);

	ops.getkey_hook = getkey;
	ops.keycmp_hook = keycmp;
	fx_tree_init(&fx_map, tree_type, &ops);
	test_map_oparations(stl_map, &fx_map, &rnd);
	fx_tree_clear(&fx_map, delete_node, NULL);
	fx_tree_destroy(&fx_map);
}


static void *test_avl_map(void *p)
{
	test_vs_stl_map(FX_TREE_AVL);
	return p;
}

static void *test_rb_map(void *p)
{
	test_vs_stl_map(FX_TREE_RB);
	return p;
}

static void *test_treap_map(void *p)
{
	test_vs_stl_map(FX_TREE_TREAP);
	return p;
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int main(int argc, char *argv[])
{
	int rc;
	pthread_t th_avl, th_rb, th_treap;

	if ((argc > 0) && (argv[1])) {
		s_full_test = 1;
	}

	rc = pthread_create(&th_avl, NULL, test_avl_map, NULL);
	fx_assert(rc == 0);

	rc = pthread_create(&th_rb, NULL, test_rb_map, NULL);
	fx_assert(rc == 0);

	rc = pthread_create(&th_treap, NULL, test_treap_map, NULL);
	fx_assert(rc == 0);

	rc = pthread_join(th_avl, NULL);
	fx_assert(rc == 0);

	rc = pthread_join(th_rb, NULL);
	fx_assert(rc == 0);

	rc = pthread_join(th_treap, NULL);
	fx_assert(rc == 0);

	return 0;
}



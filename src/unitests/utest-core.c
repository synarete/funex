/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <sys/mount.h>

#include "fnxinfra.h"
#include "fnxcore.h"

static void check_vobj(fx_vtype_e vtype)
{
	fx_vnode_t *vnode;

	vnode = fx_new_vobj(vtype, NULL);
	fx_assert(vnode != NULL);
	fx_del_vobj(vnode, NULL);
}

static void check_dobj(fx_vtype_e vtype)
{
	size_t dsize, frgsz;

	frgsz = FNX_FRGSIZE;
	dsize = fx_metasize(vtype);
	fx_assert(dsize > 0);
	fx_assert((dsize % frgsz) == 0);
}

static void check_vobjs(void)
{
	fx_vtype_e vtype;
	const fx_vinfo_t *vinfo;

	for (int i = FNX_VTYPE_NONE; i < FNX_VTYPE_ANY; ++i) {
		vtype = (fx_vtype_e)i;
		if (fx_isvmeta(vtype)) {
			vinfo = fx_get_vinfo(vtype);
			fx_assert(vinfo != NULL);
			fx_assert(vinfo->init != NULL);
			fx_assert(vinfo->destroy != NULL);
			fx_assert(vinfo->dexport != NULL);
			fx_assert(vinfo->dimport != NULL);
			fx_assert(vinfo->dcheck != NULL);

			check_vobj(vtype);
			check_dobj(vtype);
		}
	}
}

static void check_sizes(void)
{
	/* Make sure we did not get wild */
	fx_staticassert(sizeof(fx_task_t) < 30000);
}

int main(void)
{
	fx_verify_core();
	check_vobjs();
	check_sizes();

	return EXIT_SUCCESS;
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "fnxinfra.h"
#include "randutil.h"

#define MAGIC1          (0x1ef459)
#define MAGIC2          (0x778edf)
#define NITER           1213


struct key_value_node {
	fx_tlink_t link;
	int key;
	int magic1;
	char pad;
	int value;
	int magic2;
};
typedef struct key_value_node    key_value_node_t;

static int *s_rand_keys;


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int value_for_key(int key)
{
	return key * key;
}

static key_value_node_t *tlnk_to_node(const fx_tlink_t *tlnk)
{
	const key_value_node_t *node;

	node = fx_container_of(tlnk, key_value_node_t, link);
	return (key_value_node_t *)node;
}

static const void *getkey(const fx_tlink_t *x)
{
	const key_value_node_t *x_kv;

	x_kv   = tlnk_to_node(x);
	return &x_kv->key;
}

static int keycmp(const void *x_k, const void *y_k)
{
	const int *x_key;
	const int *y_key;

	x_key = (const int *)(x_k);
	y_key = (const int *)(y_k);

	return (*y_key) - (*x_key);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void delete_node(fx_tlink_t *x, void *ptr)
{
	fx_tlink_destroy(x);
	fx_free(x, sizeof(key_value_node_t));
	fx_unused(ptr);
}

static fx_tlink_t *new_node(int key)
{
	size_t sz;
	key_value_node_t *p_kv;

	sz   = sizeof(key_value_node_t);
	p_kv = (key_value_node_t *)fx_malloc(sz);
	fx_assert(p_kv != NULL);

	fx_bzero(p_kv, sizeof(*p_kv));

	p_kv->key     = key;
	p_kv->magic1  = MAGIC1;
	p_kv->magic2  = MAGIC2;
	p_kv->value   = value_for_key(key);

	fx_tlink_init(&p_kv->link);

	return (fx_tlink_t *)(p_kv);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void check_sequence(const fx_tree_t *tree, size_t nelems)
{
	size_t sz;
	int i, n, k;
	const key_value_node_t *p_kv;
	const fx_tlink_t *itr;
	const fx_tlink_t *itr2;

	sz = fx_tree_size(tree);
	fx_assert(sz == nelems);

	i = 0;
	itr = fx_tree_begin(tree);
	while (!fx_tree_iseoseq(tree, itr)) {
		n = i + 1;

		itr2 = fx_tree_find(tree, &n);
		fx_assert(itr2 != fx_tree_end(tree));

		p_kv = tlnk_to_node(itr2);
		fx_assert(p_kv->magic1  == MAGIC1);
		fx_assert(p_kv->magic2  == MAGIC2);
		fx_assert(p_kv->key     == n);
		fx_assert(p_kv->value   == value_for_key(n));

		k = (int)fx_tree_count(tree, &n);
		fx_assert(k == 1);

		++i;
		itr = fx_tree_next(tree, itr);
	}
	fx_assert(i == (int)nelems);
}


static const fx_treehooks_t s_tree_hooks = {
	.getkey_hook    = getkey,
	.keycmp_hook    = keycmp
};

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tree_basic(fx_tree_t *tree)
{
	int rc, key;
	size_t i, niter;
	const fx_tlink_t *itr;
	const fx_tlink_t *end;
	fx_tlink_t *node = NULL;

	niter = NITER;
	for (i = 0; i < niter; ++i) {
		key = (int)(i + 1);

		node = new_node(key);
		rc   = fx_tree_insert_unique(tree, node);
		fx_assert(rc == 0);

		check_sequence(tree, i + 1);
	}

	i = 0;
	itr = fx_tree_begin(tree);
	end = fx_tree_end(tree);
	while (itr != end) {
		node = (fx_tlink_t *)itr;
		fx_tree_remove(tree, node);

		delete_node(node, NULL);

		itr = fx_tree_begin(tree);
		end = fx_tree_end(tree);
		++i;
	}
	fx_assert(fx_tree_isempty(tree));
	fx_assert(i == niter);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tree_insert(fx_tree_t *tree)
{
	int rc, key, cond;
	size_t i, niter;
	const fx_tlink_t *itr;
	const fx_tlink_t *end;
	fx_tlink_t *node;

	niter = NITER;
	for (i = 0; i < niter; ++i) {
		key = (int)(i + 1);

		node = new_node(key);
		itr  = node;
		rc = fx_tree_insert_unique(tree, node);
		fx_assert(rc == 0);

		end  = fx_tree_end(tree);
		fx_assert(itr != end);
		fx_assert(itr == node);

		cond = fx_tree_iseoseq(tree, itr);
		fx_assert(!cond);
		cond = fx_tree_iseoseq(tree, end);
		fx_assert(cond);

		check_sequence(tree, i + 1);

		rc = fx_tree_insert_unique(tree, node);
		fx_assert(rc == -1);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tree_insert_replace(fx_tree_t *tree)
{
	int rc, key, cond;
	size_t i, niter;
	const fx_tlink_t *itr;
	const fx_tlink_t *end;
	fx_tlink_t *node, *node2, *node3;

	niter = NITER;
	for (i = 0; i < niter; ++i) {
		key = (int)(i + 1);

		node = new_node(key);
		itr  = node;
		rc = fx_tree_insert_unique(tree, node);
		fx_assert(rc == 0);

		end  = fx_tree_end(tree);
		fx_assert(itr != end);
		fx_assert(itr == node);

		cond = fx_tree_iseoseq(tree, itr);
		fx_assert(!cond);
		cond = fx_tree_iseoseq(tree, end);
		fx_assert(cond);

		check_sequence(tree, i + 1);

		node2 = new_node(key);
		node3 = fx_tree_insert_replace(tree, node2);
		fx_assert(node3 == node);
		delete_node(node3, NULL);

		/* re-insert same node, already linked into tree! */
		node3 = fx_tree_insert_replace(tree, node2);
		fx_assert(node3 == NULL);
	}

	fx_tree_clear(tree, delete_node, NULL);
	node = new_node(1);
	node2 = fx_tree_insert_replace(tree, node);
	fx_assert(node2 == NULL);
	fx_tree_clear(tree, delete_node, NULL);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tree_reset(fx_tree_t *tree)
{
	int rc, key;
	size_t i, niter, size;
	fx_tlink_t *list;
	fx_tlink_t *node;

	niter = NITER;
	for (i = 0; i < niter; ++i) {
		key = (int)(i + 1);
		node = new_node(key);
		rc = fx_tree_insert_unique(tree, node);
		fx_assert(rc == 0);
	}

	list = fx_tree_reset(tree);
	size = fx_tree_size(tree);
	fx_assert(list != NULL);
	fx_assert(size == 0);

	i = 0;
	while ((node = list) != NULL) {
		list = list->right;
		delete_node(node, NULL);
		++i;
	}
	fx_assert(i == niter);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tree_insert_rand(fx_tree_t *tree)
{
	int key;
	size_t i, niter;
	const fx_tlink_t *itr;
	const key_value_node_t *p_kv;
	fx_tlink_t *node;

	niter = NITER;
	for (i = 0; i < niter; ++i) {
		key = s_rand_keys[i];

		node = new_node(key);
		itr  = node;
		fx_tree_insert(tree, node);
	}

	for (i = 0; i < niter; ++i) {
		key = (int)(i + 1);
		itr = fx_tree_find(tree, &key);
		fx_assert(itr != NULL);
		p_kv = tlnk_to_node(itr);
		fx_assert(p_kv->key == key);
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

typedef void (*tree_test_fn)(fx_tree_t *);

static void
test_tree(fx_treetype_e tree_type, tree_test_fn test_fn)
{
	fx_tree_t *tree;

	tree = (fx_tree_t *)fx_malloc(sizeof(*tree));
	fx_tree_init(tree, tree_type, &s_tree_hooks);
	test_fn(tree);
	fx_tree_clear(tree, delete_node, NULL);
	fx_tree_destroy(tree);
	fx_free(tree, sizeof(*tree));
}

static void fx_test_trees(void)
{
	size_t i, j;
	fx_treetype_e tree_type;
	tree_test_fn test_fn;
	fx_treetype_e types[] = {
		FX_TREE_AVL,
		FX_TREE_RB,
		FX_TREE_TREAP
	};
	tree_test_fn tests[] = {
		test_tree_basic,
		test_tree_insert,
		test_tree_insert_replace,
		test_tree_reset,
		test_tree_insert_rand
	};

	for (i = 0; i < FX_ARRAYSIZE(types); ++i) {
		tree_type = types[i];
		for (j = 0; j < FX_ARRAYSIZE(tests); ++j) {
			test_fn = tests[j];
			test_tree(tree_type, test_fn);
		}
	}
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void set_random_keys(void)
{
	int *keys_arr;
	size_t nelems, sz;
	struct random_data *rnd;

	nelems = NITER;
	sz = sizeof(int) * nelems;
	keys_arr = (int *)fx_xmalloc(sz, FX_NOFAIL);

	rnd = create_randomizer();
	generate_keys(keys_arr, nelems, 1);
	random_shuffle(keys_arr, nelems, rnd);

	s_rand_keys = keys_arr;
}

static void free_random_keys(void)
{
	size_t nelems, sz;

	nelems = NITER;
	sz = sizeof(int) * nelems;
	fx_free(s_rand_keys, sz);
}

int main(void)
{
	set_random_keys();
	fx_test_trees();
	free_random_keys();

	return EXIT_SUCCESS;
}



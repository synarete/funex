/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/uio.h>

#include "fnxinfra.h"
#include "fnxcore.h"

#define MARK        '@'
#define SEGNBKS     FNX_SEGNBK
#define BLKSIZE     FNX_BLKSIZE
#define SEGSIZE     (BLKSIZE*SEGNBKS)
#define BKSZ        BLKSIZE
#define SGSZ        SEGSIZE
#define SGNB        SEGNBKS


struct tparam {
	fx_bool_t  check;
	fx_off_t   off;
	fx_size_t  len;
	fx_size_t  idx;
	fx_bkcnt_t cnt;
};
typedef struct tparam tparam_t;

struct tparams_pair {
	tparam_t first, second;
};
typedef struct tparams_pair tparams_t;


static fx_balloc_t s_alloc;

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_iobufs_t *new_iobufs(void)
{
	fx_iobufs_t *iobufs;

	iobufs = (fx_iobufs_t *)fx_xmalloc(sizeof(*iobufs), FX_NOFAIL);
	fx_iobufs_init(iobufs);
	fx_iobufs_setup(iobufs, FNX_INO_NULL, &s_alloc);

	return iobufs;
}

static void del_iobufs(fx_iobufs_t *iobufs)
{
	fx_iobufs_release(iobufs);
	fx_iobufs_destroy(iobufs);

	fx_xfree(iobufs, sizeof(*iobufs), FX_BZERO);
}

static void *new_data(size_t datsz)
{
	char *dat;

	dat   = (char *)fx_xmalloc(datsz, FX_NOFAIL | FX_BZERO);
	memset(dat, MARK, datsz);

	return dat;
}

static void del_data(void *dat, size_t datsz)
{
	fx_free(dat, datsz);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
check_iobuf(const fx_iobuf_t *iobuf, const tparam_t *prm, const char *dat)
{
	int cmp;
	fx_off_t off, beg, end, off2, soff;
	fx_size_t len, bkidx, bkcnt, sz, len2, slen;
	const char *dat2;
	const fx_brange_t *brange;
	const fx_bkref_t *bkref;

	if (!prm->check) {
		return;
	}
	off = prm->off;
	len = prm->len;
	bkidx = prm->idx;
	bkcnt = prm->cnt;
	brange = &iobuf->sgm.rng;

	fx_assert(brange->idx == bkidx);
	fx_assert(brange->cnt == bkcnt);
	if (bkcnt > 0) {
		fx_assert(brange->off == off);
	}

	beg = brange->off;
	end = fx_brange_end(brange);
	sz = (fx_size_t)(end - beg);
	fx_assert(len == sz);

	for (size_t pos, i = 0; i < prm->cnt; ++i) {
		pos = brange->idx + i;
		bkref = iobuf->bks[pos];
		fx_assert(bkref != NULL);
		fx_assert(bkref->blk != NULL);

		soff  = off % FNX_BLKSIZE;
		off2  = off_floor_blk(off + FNX_BLKSIZE);
		len2  = (fx_size_t)(off2 - off);
		slen  = fx_min(len, len2);
		dat2 = (const char *)(bkref->blk) + soff;

		if (slen > 0) {
			fx_assert(dat[0] == MARK);
		}

		fx_assert(off2 >= off);
		cmp  = memcmp(dat, dat2, slen);
		fx_assert(cmp == 0);

		dat += slen;
		off += (fx_off_t)slen;
		len -= slen;
	}
}

static void
check_iobufs(const fx_iobufs_t *iobufs,
             const tparams_t *params, const char *dat)
{
	check_iobuf(&iobufs->iob[0], &params->first, dat);
	check_iobuf(&iobufs->iob[1], &params->second, dat + params->first.len);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void
test_assign(fx_iobufs_t *iobufs,
            const tparams_t *params, const char *dat)
{
	int rc;
	fx_off_t off;
	fx_size_t len;

	off = params->first.off;
	len = params->first.len + params->second.len;
	rc = fx_iobufs_assign(iobufs, off, len, dat);
	fx_assert(rc == 0);
	check_iobufs(iobufs, params, dat);
}

static void
test_mkiovec(const fx_iobufs_t *iobufs, const tparams_t *params)
{
	int rc;
	size_t nelems, cnt;
	fx_iovec_t iov[2 * FNX_SEGNBK];

	nelems = FX_NELEMS(iov);
	rc = fx_iobufs_mkiovec(iobufs, iov, nelems, &cnt);
	fx_assert(rc == 0);
	fx_assert(cnt == (params->first.cnt + params->second.cnt));
}

static void
test_iobufs(const tparams_t *params)
{
	char *dat;
	size_t len;
	fx_iobufs_t *iobufs;

	len = params->first.len + params->second.len;
	dat = (char *)new_data(len);
	iobufs = new_iobufs();

	test_assign(iobufs, params, dat);
	test_mkiovec(iobufs, params);

	del_iobufs(iobufs);
	del_data(dat, len);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#define PRM(o, l, i, c) \
	{ .check = 1, .off = o, .len = l, .idx = i, .cnt = c }

#define PRM_NONE \
	{ .check = 0, .off = 0, .len = 0, .idx = 0, .cnt = 0 }

#define DEFTEST(name, prm1, prm2) \
	static const tparams_t name = { prm1, prm2 }

#define RUNTEST(name) test_iobufs(& name)


static void test_specific(void)
{
	int rc;
	fx_off_t  off = 135170;
	fx_size_t len = 131070;
	fx_iobufs_t *iobufs;
	char *dat;

	dat = (char *)new_data(len);
	iobufs = new_iobufs();
	rc = fx_iobufs_assign(iobufs, off, len, dat);
	fx_assert(rc == 0);

	del_iobufs(iobufs);
	del_data(dat, len);
}

int main(void)
{
	/* Unit-tests params definitions */
	DEFTEST(test1,
	        PRM(0, 1, 0, 1), PRM_NONE);
	DEFTEST(test2,
	        PRM(0, 2, 0, 1), PRM_NONE);
	DEFTEST(test3,
	        PRM(1, 2, 0, 1), PRM_NONE);
	DEFTEST(test4,
	        PRM(1, BKSZ - 1, 0, 1), PRM_NONE);
	DEFTEST(test5,
	        PRM(1, BKSZ + 1, 0, 2), PRM_NONE);
	DEFTEST(test6,
	        PRM(BKSZ - 1, 1, 0, 1), PRM_NONE);
	DEFTEST(test7,
	        PRM(BKSZ - 1, 2, 0, 2), PRM_NONE);
	DEFTEST(test8,
	        PRM(BKSZ + 1, 3, 1, 1), PRM_NONE);
	DEFTEST(test9,
	        PRM(BKSZ + 1, 2 * BKSZ, 1, 3), PRM_NONE);
	DEFTEST(test10,
	        PRM(10, SGSZ - 10, 0, SGNB), PRM_NONE);
	DEFTEST(test11,
	        PRM(BKSZ + 1, SGSZ - (2 * BKSZ) - 2, 1, SGNB - 2), PRM_NONE);
	DEFTEST(test12,
	        PRM(SGSZ / 2 + 1, (3 * BKSZ) + 7, SGNB / 2, 4), PRM_NONE);
	DEFTEST(test13,
	        PRM(0, SGSZ, 0, SGNB), PRM(SGSZ, 0, 0, 0));
	DEFTEST(test14,
	        PRM(0, SGSZ, 0, SGNB), PRM(SGSZ, 1, 0, 1));
	DEFTEST(test15,
	        PRM(0, SGSZ, 0, SGNB), PRM(SGSZ, SGSZ, 0, SGNB));
	DEFTEST(test16,
	        PRM(1, SGSZ - 1, 0, SGNB), PRM(SGSZ, SGSZ, 0, SGNB));
	DEFTEST(test17,
	        PRM(BKSZ + 1, SGSZ - BKSZ - 1, 1, SGNB - 1),
	        PRM(SGSZ, SGSZ - BKSZ - 1, 0, SGNB - 1));

	/* Init allocator */
	fx_balloc_init(&s_alloc);

	/* Unit-tests execution */
	RUNTEST(test1);
	RUNTEST(test2);
	RUNTEST(test3);
	RUNTEST(test4);
	RUNTEST(test5);
	RUNTEST(test6);
	RUNTEST(test7);
	RUNTEST(test8);
	RUNTEST(test9);
	RUNTEST(test10);
	RUNTEST(test11);
	RUNTEST(test12);
	RUNTEST(test13);
	RUNTEST(test14);
	RUNTEST(test15);
	RUNTEST(test16);
	RUNTEST(test17);

	/* Specific unitests */
	test_specific();

	/* Clean & destroy allocator */
	fx_balloc_clear(&s_alloc);
	fx_balloc_destroy(&s_alloc);

	return EXIT_SUCCESS;
}



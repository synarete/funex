/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "fnxinfra.h"
#include "fnxcore.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void utest_sockaddr(void)
{
	int rc, res;
	fx_sockaddr_t saddr1, saddr2;

	fx_sockaddr_loopback(&saddr1, 0);
	rc = fx_sockaddr_pton(&saddr2, "127.0.0.1");
	fx_assert(rc == 0);
	fx_assert(saddr1.in.sin_family == saddr2.in.sin_family);
	fx_assert(saddr1.in.sin_port == saddr2.in.sin_port);
	fx_assert(saddr1.in.sin_addr.s_addr == saddr2.in.sin_addr.s_addr);
	res = memcmp(&saddr1, &saddr2, sizeof(saddr1));
	fx_assert(res == 0);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static fx_inport_t s_udpport;

static int
udpsock_bind_any(fx_socket_t *sock, fx_sockaddr_t *saddr)
{
	int rc = -1;

	fx_sockaddr_any(saddr);
	for (size_t i = 10000; i < 20000; ++i) {
		s_udpport = (fx_inport_t)(i + 1);
		fx_sockaddr_setport(saddr, s_udpport);
		rc = fx_udpsock_bind(sock, saddr);
		if (rc == 0) {
			break;
		}
	}
	return rc;
}

static void utest_udpsock(void)
{
	int res, rc;
	size_t len, n = 0;
	fx_socket_t sock;
	fx_sockaddr_t saddr1, saddr2, srcaddr;
	fx_timespec_t ts = { 10000, 0 };
	char buf1[100] = "hello, world";
	char buf2[100];

	fx_udpsock_init(&sock);
	res = fx_udpsock_isopen(&sock);
	fx_assert(res == FNX_FALSE);
	rc = fx_udpsock_open(&sock);
	fx_assert(rc == 0);
	rc = udpsock_bind_any(&sock, &saddr1);
	fx_assert(rc == 0);

	fx_sockaddr_loopback(&saddr2, s_udpport);
	len = sizeof(buf1);
	rc = fx_udpsock_sendto(&sock, buf1, len, &saddr2, &n);
	fx_assert(rc == 0);
	fx_assert(n == len);

	rc = fx_udpsock_rselect(&sock, &ts);
	fx_assert(rc == 0);

	len = sizeof(buf2);
	rc = fx_udpsock_recvfrom(&sock, buf2, len, &n, &srcaddr);
	fx_assert(rc == 0);
	fx_assert(n == len);
	fx_assert(srcaddr.in.sin_port == saddr2.in.sin_port);

	fx_udpsock_close(&sock);
	fx_udpsock_destroy(&sock);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

#define MAGIC 71

static fx_inport_t s_tcpport;

static void tcpsock_bind_any(fx_socket_t *sock, fx_sockaddr_t *saddr)
{
	int rc = -1;

	fx_sockaddr_any(saddr);
	for (size_t i = 10000; i < 20000; ++i) {
		s_tcpport = (fx_inport_t)(i + 1);
		fx_sockaddr_setport(saddr, s_tcpport);
		rc = fx_tcpsock_bind(sock, saddr);
		if (rc == 0) {
			break;
		}
	}
	fx_assert(rc == 0);
}

static void *
tcpsock_serve1(void *p)
{
	int rc, res, num = 0;
	size_t sz = 0;
	fx_socket_t *sock;
	fx_socket_t acsock;
	fx_sockaddr_t peeraddr;
	struct ucred cred_peer, cred_self;

	fx_bzero(&cred_peer, sizeof(cred_peer));
	fx_bzero(&cred_self, sizeof(cred_self));
	fx_ucred_self(&cred_self);

	sock = (fx_socket_t *)p;
	rc = fx_tcpsock_listen(sock, 1);
	fx_assert(rc == 0);

	rc = fx_tcpsock_accept(sock, &acsock, &peeraddr);
	fx_assert(rc == 0);
	rc = fx_tcpsock_recv(&acsock, &num, sizeof(num), &sz);
	fx_assert(rc == 0);
	fx_assert(sz == sizeof(num));
	fx_assert(num == MAGIC);

	if (acsock.family == AF_UNIX) {
		rc = fx_tcpsock_peercred(&acsock, &cred_peer);
		fx_assert(rc == 0);
		res = memcmp(&cred_peer, &cred_self, sizeof(cred_peer));
		fx_assert(res == 0);
	}

	fx_tcpsock_close(&acsock);
	fx_tcpsock_destroy(&acsock);
	return NULL;
}

static void
utest_sendrecv(fx_socket_t *sock, fx_socket_t *connsock,
               const fx_sockaddr_t *connaddr)
{
	int rc, num = MAGIC;
	size_t sz = 0;
	pthread_t tid;

	rc = pthread_create(&tid, NULL, tcpsock_serve1, sock);
	fx_assert(rc == 0);

	sleep(1);
	rc = fx_tcpsock_connect(connsock, connaddr);
	fx_assert(rc == 0);
	fx_tcpsock_setkeepalive(connsock);
	fx_tcpsock_setnonblock(connsock);

	rc = fx_tcpsock_send(connsock, &num, sizeof(num), &sz);
	fx_assert(rc == 0);
	fx_assert(sz == sizeof(num));

	rc = fx_tcpsock_shutdown(connsock);
	fx_assert(rc == 0);

	pthread_join(tid, NULL);
}

static void utest_tcpsock(void)
{
	int rc;
	fx_sockaddr_t saddr, connaddr;
	fx_socket_t sock, connsock;

	fx_tcpsock_init(&sock);
	rc = fx_tcpsock_open(&sock);
	fx_assert(rc == 0);

	fx_tcpsock_init(&connsock);
	rc = fx_tcpsock_open(&connsock);
	fx_assert(rc == 0);

	tcpsock_bind_any(&sock, &saddr);
	fx_sockaddr_loopback(&connaddr, s_tcpport);

	utest_sendrecv(&sock, &connsock, &connaddr);

	fx_tcpsock_close(&sock);
	fx_tcpsock_destroy(&sock);
	fx_tcpsock_close(&connsock);
	fx_tcpsock_destroy(&connsock);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static char s_path[PATH_MAX];

static void bind_tcpusock(fx_socket_t *sock, fx_sockaddr_t *saddr)
{
	int rc;
	fx_uuid_t uuid;

	fx_uuid_generate(&uuid);
	snprintf(s_path, sizeof(s_path) - 1, "%s-%s",
	         program_invocation_name, uuid.str);
	rc = fx_sockaddr_setunix(saddr, s_path);
	fx_assert(rc == 0);
	rc = fx_tcpsock_bind(sock, saddr);
	fx_unused(sock);
}

static void utest_unixsock(void)
{
	int rc;
	fx_sockaddr_t saddr;
	fx_socket_t sock, connsock;

	fx_tcpsock_initu(&sock);
	fx_tcpsock_initu(&connsock);

	rc = fx_tcpsock_open(&sock);
	fx_assert(rc == 0);
	rc = fx_tcpsock_open(&connsock);
	fx_assert(rc == 0);

	bind_tcpusock(&sock, &saddr);
	utest_sendrecv(&sock, &connsock, &saddr);

	fx_tcpsock_close(&sock);
	fx_tcpsock_destroy(&sock);
	fx_tcpsock_close(&connsock);
	fx_tcpsock_destroy(&connsock);

	remove(s_path);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int main(void)
{
	/* TODO: Reenable */
	exit(0);

	utest_sockaddr();
	utest_udpsock();
	utest_tcpsock();
	utest_unixsock();
	return 0;
}


/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "fnxinfra.h"
#include "randutil.h"

#define MAGIC           (0xCC4466AA)
#define MAX_SIZE        (511)

struct element {
	int num;
	unsigned magic;
	fx_slink_t lnk;
};
typedef struct element element_t;

static void elem_init(element_t *elem)
{
	elem->num      = 0;
	elem->magic    = MAGIC;
	elem->lnk.next = NULL;
}

static void elem_destroy(element_t *elem)
{
	elem->num      = -1;
	elem->magic    = 0;
	elem->lnk.next = NULL;
}

static void elem_check(const element_t *elem)
{
	fx_assert(elem->magic == MAGIC);
}

static element_t *elem_new(size_t n)
{
	element_t *elem = NULL;

	elem = (element_t *)fx_malloc(sizeof(*elem));
	fx_assert(elem != 0);
	elem_init(elem);
	elem->num = (int)n;
	return elem;
}

static void elem_delete(element_t *elem)
{
	elem_destroy(elem);
	fx_free(elem, sizeof(*elem));
}

static element_t *slink_to_elem(fx_slink_t *lnk)
{
	element_t *elem = fx_container_of(lnk, element_t, lnk);
	elem_check(elem);
	return elem;
}

static fx_slink_t *elem_to_slink(element_t *elem)
{
	elem_check(elem);
	return &elem->lnk;
}

static void check_nomembers(const fx_slink_t *head)
{
	int isempty;

	isempty = fx_slist_isempty(head);
	fx_assert(isempty);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static int vequal(const fx_slink_t *lnk, const void *v)
{
	int rc, num;
	const element_t *elem;

	elem = slink_to_elem((fx_slink_t *)lnk);
	num  = *((const int *)v);

	rc = (elem->num == num) ? 1 : 0;
	return rc;
}

static void test_vfind(const fx_slink_t *head, int num)
{
	const fx_slink_t *lnk;
	const element_t *elem;

	lnk = fx_slist_find(head, vequal, &num);
	fx_assert(lnk != NULL);

	elem = slink_to_elem((fx_slink_t *)lnk);
	fx_assert(elem->num == num);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_pushpop(fx_slink_t *head)
{
	size_t i, n, len;
	element_t *elem;
	fx_slink_t *slnk;

	n = MAX_SIZE;
	for (i = 0; i < n; ++i) {
		len = fx_slist_length(head);
		fx_assert(len == i);

		elem = elem_new(len);
		slnk = elem_to_slink(elem);

		fx_slist_push_front(head, slnk);

		test_vfind(head, (int)len);
	}

	for (i = 0; i < n; ++i) {
		len = fx_slist_length(head);
		test_vfind(head, (int)(len - 1));

		slnk = fx_slist_pop_front(head);
		elem = slink_to_elem(slnk);
		elem_check(elem);

		len = fx_slist_length(head);
		fx_assert(elem->num == (int)len);

		elem_delete(elem);
	}

	check_nomembers(head);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_findremove(fx_slink_t *head)
{
	int num;
	size_t i, n, len;
	element_t *elem;
	fx_slink_t *slnk;

	n = MAX_SIZE;
	for (i = 0; i < n; ++i) {
		len = fx_slist_length(head);
		fx_assert(len == i);

		elem = elem_new(len);
		slnk = elem_to_slink(elem);
		fx_slist_push_front(head, slnk);
	}

	for (i = n; i > 0; --i) {
		num = (int)(i - 1);
		slnk = fx_slist_findremove(head, vequal, &num);
		fx_assert(slnk != NULL);

		elem = slink_to_elem(slnk);
		elem_delete(elem);
	}

	check_nomembers(head);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_random(fx_slink_t *head)
{
	int rc, num, *nums;
	size_t i, j, n, k, sz;
	element_t *elem;
	fx_slink_t *slnk;
	struct random_data *rnd_dat;
	int32_t rnd;

	rnd_dat = create_randomizer();

	sz = sizeof(int) * MAX_SIZE;
	nums = (int *)fx_xmalloc(sz, FX_NOFAIL);

	n = MAX_SIZE;
	for (i = 0; i < n; ++i) {
		rc = random_r(rnd_dat, &rnd);
		fx_assert(rc == 0);

		num = (int)rnd;
		nums[i] = num;

		elem = elem_new((size_t)num);
		slnk = elem_to_slink(elem);

		fx_slist_push_front(head, slnk);
	}

	rc = random_r(rnd_dat, &rnd);
	fx_assert(rc == 0);

	k = ((size_t)rnd) % n;
	for (i = 0; i < k; ++i) {
		j = n - i - 1;
		num = nums[i];
		nums[i] = nums[j];
		nums[j] = num;
	}

	for (i = 0; i < n; ++i) {
		num = nums[i];
		slnk = fx_slist_findremove(head, vequal, &num);
		fx_assert(slnk != NULL);

		elem = slink_to_elem(slnk);
		elem_delete(elem);
	}

	check_nomembers(head);

	fx_free(nums, sz);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void accum(fx_slink_t *lnk, const void *p)
{
	size_t *p_sum = (size_t *)p;
	const element_t *elem;

	elem = slink_to_elem(lnk);
	*p_sum += (size_t)(elem->num);
}

static void test_foreach(fx_slink_t *head)
{
	size_t i, n, total;
	element_t *elem;
	fx_slink_t *slnk;

	n = MAX_SIZE;
	for (i = 0; i < n; ++i) {
		elem = elem_new(1);
		slnk = elem_to_slink(elem);

		fx_slist_push_front(head, slnk);

		total = 0;
		fx_slist_foreach(head, accum, &total);
		fx_assert(total == (i + 1));
	}

	for (i = 0; i < n; ++i) {
		slnk = fx_slist_pop_front(head);
		fx_assert(slnk != NULL);
		elem = slink_to_elem(slnk);
		elem_delete(elem);
	}

	total = 0;
	fx_slist_foreach(head, accum, &total);
	fx_assert(total == 0);

	check_nomembers(head);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

static void test_slist(void)
{
	fx_slink_t sl_obj;
	fx_slink_t *head = &sl_obj;

	fx_slist_init(head);

	test_pushpop(head);
	test_findremove(head);
	test_random(head);
	test_foreach(head);

	fx_slist_destroy(head);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

int main(void)
{
	unsigned seed;

	seed = (unsigned)(time(NULL));
	srand(seed);

	test_slist();

	return EXIT_SUCCESS;
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef RANDUTIL_H_
#define RANDUTIL_H_

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "fnxinfra.h"


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Based on: http://www.stanford.edu/~blp/writings/clc/shuffle.html */
static void
random_shuffle(int *arr, size_t n, struct random_data *rnd_dat)
{
	int k;
	int32_t rnd;
	size_t i, j, r;

	if (n > 1) {
		for (i = 0; i < n - 1; i++) {
			random_r(rnd_dat, &rnd);
			r = (size_t)rnd;
			j = i + (r / (RAND_MAX / (n - i) + 1));
			k = arr[j];
			arr[j] = arr[i];
			arr[i] = k;
		}
	}
}

static void
generate_keys(int *arr, size_t n, size_t step)
{
	size_t i, k;

	if (!step) {
		step = 1;
	}

	k = 1;
	for (i = 0; i < n; ++i) {
		arr[i] = (int)(k);
		if ((i % step) == 0) {
			k++;
		}
	}
}

static struct random_data *
create_randomizer(void)
{
	int rc;
	unsigned seed;
	static char statebuf[256];
	static struct random_data rnd_dat;

	fx_bzero(statebuf, sizeof(statebuf));
	fx_bzero(&rnd_dat, sizeof(rnd_dat));

	seed = (unsigned)(time(NULL));
	rc = initstate_r(seed, statebuf, sizeof(statebuf), &rnd_dat);
	fx_assert(rc == 0);
	setstate_r(statebuf, &rnd_dat);

	return &rnd_dat;
}


static inline void suppress_compiler_warn_dont_use(void)
{
	create_randomizer();
	generate_keys(NULL, 0, 0);
	random_shuffle(NULL, 0, NULL);
}


#endif /* RANDUTIL_H_ */



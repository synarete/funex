/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "fnxinfra.h"

#define NELEMS(x)       FX_NELEMS(x)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_compare(fx_substr_t *ss)
{
	int cmp, eq, emp;
	size_t sz;

	fx_substr_init(ss, "123456789");
	sz = fx_substr_size(ss);
	fx_assert(sz == 9);

	emp = fx_substr_isempty(ss);
	fx_assert(!emp);

	cmp = fx_substr_compare(ss, "123");
	fx_assert(cmp > 0);

	cmp = fx_substr_compare(ss, "9");
	fx_assert(cmp < 0);

	cmp = fx_substr_compare(ss, "123456789");
	fx_assert(cmp == 0);

	eq = fx_substr_isequal(ss, "123456789");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find(fx_substr_t *ss)
{
	size_t n, pos, len;

	fx_substr_init(ss, "ABCDEF abcdef ABCDEF");

	n = fx_substr_find(ss, "A");
	fx_assert(n == 0);

	n = fx_substr_find(ss, "EF");
	fx_assert(n == 4);

	pos = 10;
	len = 2;
	n = fx_substr_nfind(ss, pos, "EF", len);
	fx_assert(n == 18);

	pos = 1;
	n = fx_substr_find_chr(ss, pos, 'A');
	fx_assert(n == 14);

	n = fx_substr_find(ss, "XXX");
	fx_assert(n > fx_substr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_rfind(fx_substr_t *ss)
{
	size_t n, pos, len;

	fx_substr_init(ss, "ABRACADABRA");

	n = fx_substr_rfind(ss, "A");
	fx_assert(n == 10);

	n = fx_substr_rfind(ss, "BR");
	fx_assert(n == 8);

	pos = fx_substr_size(ss) / 2;
	len = 2;
	n = fx_substr_nrfind(ss, pos, "BR", len);
	fx_assert(n == 1);

	pos = 1;
	n = fx_substr_rfind_chr(ss, pos, 'B');
	fx_assert(n == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_first_of(fx_substr_t *ss)
{
	size_t n, pos;

	fx_substr_init(ss, "012x456x89z");

	n = fx_substr_find_first_of(ss, "xyz");
	fx_assert(n == 3);

	pos = 5;
	n = fx_substr_nfind_first_of(ss, pos, "x..z", 4);
	fx_assert(n == 7);

	n = fx_substr_find_first_of(ss, "XYZ");
	fx_assert(n > fx_substr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_last_of(fx_substr_t *ss)
{
	size_t n, pos;

	fx_substr_init(ss, "AAAAA-BBBBB");

	n = fx_substr_find_last_of(ss, "xyzAzyx");
	fx_assert(n == 4);

	pos = 9;
	n = fx_substr_nfind_last_of(ss, pos, "X-Y", 3);
	fx_assert(n == 5);

	n = fx_substr_find_last_of(ss, "BBBBBBBBBBBBBBBBBBBBB");
	fx_assert(n == (fx_substr_size(ss) - 1));

	n = fx_substr_find_last_of(ss, "...");
	fx_assert(n > fx_substr_size(ss));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_first_not_of(fx_substr_t *ss)
{
	size_t n, pos;

	fx_substr_init(ss, "aaa bbb ccc * ddd + eee");

	n = fx_substr_find_first_not_of(ss, "a b c d e");
	fx_assert(n == 12);

	pos = 14;
	n = fx_substr_nfind_first_not_of(ss, pos, "d e", 3);
	fx_assert(n == 18);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_last_not_of(fx_substr_t *ss)
{
	size_t n, pos;

	fx_substr_init(ss, "-..3456.--");

	n = fx_substr_find_last_not_of(ss, ".-");
	fx_assert(n == 6);

	pos = 1;
	n = fx_substr_nfind_last_not_of(ss, pos, "*", 1);
	fx_assert(n == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_sub(fx_substr_t *ss)
{
	int rc;
	fx_substr_t sub;
	const char *abc;

	abc = "abcdefghijklmnopqrstuvwxyz";

	fx_substr_init_rd(ss, abc, 10);    /* "abcdefghij" */
	fx_substr_sub(ss, 2, 4, &sub);
	rc  = fx_substr_isequal(&sub, "cdef");
	fx_assert(rc == 1);

	fx_substr_rsub(ss, 3, &sub);
	rc  = fx_substr_isequal(&sub, "hij");
	fx_assert(rc == 1);

	fx_substr_chop(ss, 8, &sub);
	rc  = fx_substr_isequal(&sub, "ab");
	fx_assert(rc == 1);

	fx_substr_clone(&sub, ss);
	rc  = fx_substr_nisequal(&sub, ss->str, ss->len);
	fx_assert(rc == 1);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_count(fx_substr_t *ss)
{
	size_t n;

	fx_substr_init(ss, "xxx-xxx-xxx-xxx");

	n = fx_substr_count(ss, "xxx");
	fx_assert(n == 4);

	n = fx_substr_count_chr(ss, '-');
	fx_assert(n == 3);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_split(fx_substr_t *ss)
{
	int eq;
	fx_substr_pair_t split;

	fx_substr_init(ss, "ABC-DEF+123");
	fx_substr_split(ss, "-", &split);

	eq = fx_substr_isequal(&split.first, "ABC");
	fx_assert(eq);

	eq = fx_substr_isequal(&split.second, "DEF+123");
	fx_assert(eq);

	fx_substr_split(ss, " + * ", &split);
	eq = fx_substr_isequal(&split.first, "ABC-DEF");
	fx_assert(eq);

	eq = fx_substr_isequal(&split.second, "123");
	fx_assert(eq);


	fx_substr_split_chr(ss, 'B', &split);
	eq = fx_substr_isequal(&split.first, "A");
	fx_assert(eq);

	eq = fx_substr_isequal(&split.second, "C-DEF+123");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_rsplit(fx_substr_t *ss)
{
	int eq;
	fx_substr_pair_t split;

	fx_substr_init(ss, "XXX--YYY--ZZZ");
	fx_substr_rsplit(ss, "-.", &split);

	eq = fx_substr_isequal(&split.first, "XXX--YYY");
	fx_assert(eq);

	eq = fx_substr_isequal(&split.second, "ZZZ");
	fx_assert(eq);


	fx_substr_rsplit(ss, "+", &split);

	eq = fx_substr_nisequal(&split.first, ss->str, ss->len);
	fx_assert(eq);

	eq = fx_substr_isequal(&split.second, "ZZZ");
	fx_assert(!eq);


	fx_substr_init(ss, "1.2.3.4.5");
	fx_substr_rsplit_chr(ss, '.', &split);

	eq = fx_substr_isequal(&split.first, "1.2.3.4");
	fx_assert(eq);

	eq = fx_substr_isequal(&split.second, "5");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_trim(fx_substr_t *ss)
{
	int eq;
	fx_substr_t sub;

	fx_substr_init(ss, ".:ABCD");

	fx_substr_trim_any_of(ss, ":,.%^", &sub);
	eq  = fx_substr_isequal(&sub, "ABCD");
	fx_assert(eq);

	fx_substr_ntrim_any_of(ss,
	                       fx_substr_data(ss),
	                       fx_substr_size(ss),
	                       &sub);
	eq  = fx_substr_size(&sub) == 0;
	fx_assert(eq);

	fx_substr_trim_chr(ss, '.', &sub);
	eq  = fx_substr_isequal(&sub, ":ABCD");
	fx_assert(eq);

	fx_substr_trim(ss, 4, &sub);
	eq  = fx_substr_isequal(&sub, "CD");
	fx_assert(eq);

	fx_substr_trim_if(ss, fx_chr_ispunct, &sub);
	eq  = fx_substr_isequal(&sub, "ABCD");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_chop(fx_substr_t *ss)
{
	int eq;
	fx_substr_t sub;

	fx_substr_init(ss, "123....");

	fx_substr_chop_any_of(ss, "+*&^%$.", &sub);
	eq  = fx_substr_isequal(&sub, "123");
	fx_assert(eq);

	fx_substr_nchop_any_of(ss,
	                       fx_substr_data(ss),
	                       fx_substr_size(ss),
	                       &sub);
	eq  = fx_substr_isequal(&sub, "");
	fx_assert(eq);

	fx_substr_chop(ss, 6, &sub);
	eq  = fx_substr_isequal(&sub, "1");
	fx_assert(eq);

	fx_substr_chop_chr(ss, '.', &sub);
	eq  = fx_substr_isequal(&sub, "123");
	fx_assert(eq);

	fx_substr_chop_if(ss, fx_chr_ispunct, &sub);
	eq  = fx_substr_isequal(&sub, "123");
	fx_assert(eq);

	fx_substr_chop_if(ss, fx_chr_isprint, &sub);
	eq  = fx_substr_size(&sub) == 0;
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_strip(fx_substr_t *ss)
{
	int eq;
	size_t sz;
	const char *s;
	fx_substr_t sub;

	fx_substr_init(ss, ".....#XYZ#.........");

	fx_substr_strip_any_of(ss, "-._#", &sub);
	eq  = fx_substr_isequal(&sub, "XYZ");
	fx_assert(eq);

	fx_substr_strip_chr(ss, '.', &sub);
	eq  = fx_substr_isequal(&sub, "#XYZ#");
	fx_assert(eq);

	fx_substr_strip_if(ss, fx_chr_ispunct, &sub);
	eq  = fx_substr_isequal(&sub, "XYZ");
	fx_assert(eq);

	s  = fx_substr_data(ss);
	sz = fx_substr_size(ss);
	fx_substr_nstrip_any_of(ss, s, sz, &sub);
	eq  = fx_substr_isequal(&sub, "");
	fx_assert(eq);

	fx_substr_init(ss, " \t ABC\n\r\v");
	fx_substr_strip_ws(ss, &sub);
	eq = fx_substr_isequal(&sub, "ABC");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_find_token(fx_substr_t *ss)
{
	int eq;
	fx_substr_t tok;
	const char *seps;

	seps   = " \t\n\v\r";
	fx_substr_init(ss, " A BB \t  CCC    DDDD  \n");

	fx_substr_find_token(ss, seps, &tok);
	eq  = fx_substr_isequal(&tok, "A");
	fx_assert(eq);

	fx_substr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_substr_isequal(&tok, "BB");
	fx_assert(eq);

	fx_substr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_substr_isequal(&tok, "CCC");
	fx_assert(eq);

	fx_substr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_substr_isequal(&tok, "DDDD");
	fx_assert(eq);

	fx_substr_find_next_token(ss, &tok, seps, &tok);
	eq  = fx_substr_isequal(&tok, "");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_tokenize(fx_substr_t *ss)
{
	int rc, eq;
	size_t n_toks;
	fx_substr_t toks_list[7];
	const char *seps = " /:;.| " ;
	const char *line;

	line = "    /Ant:::Bee;:Cat:Dog;...Elephant.../Frog:/Giraffe///    ";
	fx_substr_init(ss, line);

	rc = fx_substr_tokenize(ss, seps, toks_list, 7, &n_toks);
	fx_assert(rc == 0);
	fx_assert(n_toks == 7);

	eq  = fx_substr_isequal(&toks_list[0], "Ant");
	fx_assert(eq);

	eq  = fx_substr_isequal(&toks_list[4], "Elephant");
	fx_assert(eq);

	eq  = fx_substr_isequal(&toks_list[6], "Giraffe");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_case(fx_substr_t *ss)
{
	int eq;
	char buf[20] = "0123456789abcdef";

	fx_substr_init_rwa(ss, buf);

	fx_substr_toupper(ss);
	eq  = fx_substr_isequal(ss, "0123456789ABCDEF");
	fx_assert(eq);

	fx_substr_tolower(ss);
	eq  = fx_substr_isequal(ss, "0123456789abcdef");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_common_prefix(fx_substr_t *ss)
{
	int eq, n;
	char buf1[] = "0123456789abcdef";

	fx_substr_init(ss, buf1);

	n = (int)fx_substr_common_prefix(ss, "0123456789ABCDEF");
	eq = (n == 10);
	fx_assert(eq);

	n = (int)fx_substr_common_prefix(ss, buf1);
	eq = (n == 16);
	fx_assert(eq);

	n = (int)fx_substr_common_prefix(ss, "XYZ");
	eq = (n == 0);
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_common_suffix(fx_substr_t *ss)
{
	int eq, n;
	char buf1[] = "abcdef0123456789";

	fx_substr_init(ss, buf1);

	n = (int)fx_substr_common_suffix(ss, "ABCDEF0123456789");
	eq = (n == 10);
	fx_assert(eq);

	n = (int)fx_substr_common_suffix(ss, buf1);
	eq = (n == 16);
	fx_assert(eq);

	n = (int)fx_substr_common_suffix(ss, "XYZ");
	eq = (n == 0);
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_assign(fx_substr_t *ss)
{
	int eq, n;
	fx_substr_t sub;
	char buf1[] = "0123456789......";
	const char *s;

	fx_substr_init_rw(ss, buf1, 10, 16);
	fx_substr_sub(ss, 10, 6, &sub);

	n = (int)fx_substr_size(&sub);
	eq = (n == 0);
	fx_assert(eq);

	n = (int)fx_substr_wrsize(&sub);
	eq = (n == 6);
	fx_assert(eq);

	s = "ABC";
	fx_substr_assign(ss, s);
	n = (int)fx_substr_size(ss);
	fx_assert(n == 3);
	n = (int)fx_substr_wrsize(ss);
	fx_assert(n == 16);

	eq = fx_substr_isequal(ss, s);
	fx_assert(eq);

	s = "ABCDEF";
	fx_substr_assign(&sub, s);
	eq = fx_substr_isequal(&sub, s);
	fx_assert(eq);

	s = "ABCDEF$$$";
	fx_substr_assign(&sub, s);
	n = (int)fx_substr_size(&sub);
	fx_assert(n == 6);
	n = (int)fx_substr_wrsize(&sub);
	fx_assert(n == 6);
	eq = fx_substr_isequal(&sub, s);
	fx_assert(!eq);

	fx_substr_sub(&sub, 5, 100, &sub);
	s = "XYZ";
	fx_substr_assign(&sub, s);
	n = (int)fx_substr_size(&sub);
	fx_assert(n == 1);
	n = (int)fx_substr_wrsize(&sub);
	fx_assert(n == 1);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_append(fx_substr_t *ss)
{
	int eq, n;
	char buf[20];
	const char *s;

	fx_substr_init_rw(ss, buf, 0, NELEMS(buf));
	s = "0123456789abcdef";

	fx_substr_append(ss, s);
	n = (int)fx_substr_size(ss);
	fx_assert(n == 16);
	n = (int)fx_substr_wrsize(ss);
	fx_assert(n == NELEMS(buf));
	eq = fx_substr_isequal(ss, s);
	fx_assert(eq);

	fx_substr_append(ss, s);
	n = (int)fx_substr_size(ss);
	fx_assert(n == NELEMS(buf));
	n = (int)fx_substr_wrsize(ss);
	fx_assert(n == NELEMS(buf));

	fx_substr_init_rw(ss, buf, 0, NELEMS(buf));
	s = "01";
	fx_substr_nappend(ss, s, 1);
	n = (int)fx_substr_size(ss);
	fx_assert(n == 1);
	fx_substr_nappend(ss, s + 1, 1);
	n = (int)fx_substr_size(ss);
	fx_assert(n == 2);
	eq = fx_substr_isequal(ss, s);
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_insert(fx_substr_t *ss)
{
	int eq, n;
	const char *s;
	char buf[20];

	fx_substr_init_rw(ss, buf, 0, NELEMS(buf));

	s = "0123456789";
	fx_substr_insert(ss, 0, s);
	n = (int)fx_substr_size(ss);
	fx_assert(n == 10);
	eq = fx_substr_isequal(ss, s);
	fx_assert(eq);

	fx_substr_insert(ss, 10, s);
	n = (int)fx_substr_size(ss);
	fx_assert(n == 20);
	eq = fx_substr_isequal(ss, "01234567890123456789");
	fx_assert(eq);

	fx_substr_insert(ss, 1, "....");
	n = (int)fx_substr_size(ss);
	fx_assert(n == 20);
	eq = fx_substr_isequal(ss, "0....123456789012345");
	fx_assert(eq);

	fx_substr_insert(ss, 16, "%%%");
	n = (int)fx_substr_size(ss);
	fx_assert(n == 20);
	eq = fx_substr_isequal(ss, "0....12345678901%%%2");
	fx_assert(eq);

	fx_substr_insert_chr(ss, 1, 20, '$');
	n = (int)fx_substr_size(ss);
	fx_assert(n == 20);
	eq = fx_substr_isequal(ss, "0$$$$$$$$$$$$$$$$$$$");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_replace(fx_substr_t *ss)
{
	int eq, n;
	const char *s;
	char buf[10];

	fx_substr_init_rw(ss, buf, 0, NELEMS(buf));

	s = "ABCDEF";
	fx_substr_replace(ss, 0, 2, s);
	n = (int) fx_substr_size(ss);
	fx_assert(n == 6);
	eq = fx_substr_isequal(ss, s);
	fx_assert(eq);

	fx_substr_replace(ss, 1, 2, s);
	eq = fx_substr_isequal(ss, "AABCDEFDEF");
	fx_assert(eq);

	fx_substr_replace(ss, 6, 3, s);
	eq = fx_substr_isequal(ss, "AABCDEABCD");
	fx_assert(eq);

	fx_substr_replace_chr(ss, 0, 10, 30, '.');
	eq = fx_substr_isequal(ss, "..........");
	fx_assert(eq);

	fx_substr_replace_chr(ss, 1, 8, 4, 'A');
	eq = fx_substr_isequal(ss, ".AAAA.");
	fx_assert(eq);

	fx_substr_nreplace(ss, 2, 80,
	                   fx_substr_data(ss),
	                   fx_substr_size(ss));
	eq = fx_substr_isequal(ss, ".A.AAAA.");
	fx_assert(eq);

	fx_substr_nreplace(ss, 4, 80,
	                   fx_substr_data(ss),
	                   fx_substr_size(ss));
	eq = fx_substr_isequal(ss, ".A.A.A.AAA");  /* Truncated */
	fx_assert(eq);
}


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_erase(fx_substr_t *ss)
{
	int n, eq;
	char buf[5];

	fx_substr_init_rw(ss, buf, 0, NELEMS(buf));

	fx_substr_assign(ss, "ABCDEF");
	eq = fx_substr_isequal(ss, "ABCDE");
	fx_assert(eq);

	fx_substr_erase(ss, 1, 2);
	eq = fx_substr_isequal(ss, "ADE");
	fx_assert(eq);

	fx_substr_erase(ss, 0, 100);
	eq = fx_substr_isequal(ss, "");
	fx_assert(eq);

	n = (int) fx_substr_wrsize(ss);
	fx_assert(n == NELEMS(buf));
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_reverse(fx_substr_t *ss)
{
	int eq;
	char buf[40];

	fx_substr_init_rw(ss, buf, 0, NELEMS(buf));
	fx_substr_assign(ss, "abracadabra");
	fx_substr_reverse(ss);

	eq = fx_substr_isequal(ss, "arbadacarba");
	fx_assert(eq);
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
static void test_copyto(fx_substr_t *ss)
{
	int eq;
	char buf[10];
	char pad = '@';
	size_t sz;

	fx_substr_init(ss, "123456789");
	sz = fx_substr_size(ss);
	fx_assert(sz == 9);

	sz = fx_substr_copyto(ss, buf, sizeof(buf));
	fx_assert(sz == 9);

	eq = !strcmp(buf, "123456789");
	fx_assert(eq);
	fx_assert(pad == '@');
}

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
int main(void)
{
	fx_substr_t substr_obj;
	fx_substr_t *ss = &substr_obj;

	test_compare(ss);
	test_find(ss);
	test_rfind(ss);
	test_find_first_of(ss);
	test_find_last_of(ss);
	test_find_first_not_of(ss);
	test_find_last_not_of(ss);
	test_sub(ss);
	test_count(ss);
	test_split(ss);
	test_rsplit(ss);
	test_trim(ss);
	test_chop(ss);
	test_strip(ss);
	test_find_token(ss);
	test_tokenize(ss);
	test_case(ss);
	test_common_prefix(ss);
	test_common_suffix(ss);

	test_assign(ss);
	test_append(ss);
	test_insert(ss);
	test_replace(ss);
	test_erase(ss);
	test_reverse(ss);
	test_copyto(ss);

	fx_substr_destroy(ss);

	return EXIT_SUCCESS;
}



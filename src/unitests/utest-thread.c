/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                            Funex File-System                                *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex is a free software; you can redistribute it and/or modify it under   *
 *  the terms of the GNU General Public License as published by the Free       *
 *  Software Foundation, either version 3 of the License, or (at your option)  *
 *  any later version.                                                         *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more      *
 *  details.                                                                   *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License along    *
 *  with this program. If not, see <http://www.gnu.org/licenses/>              *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#include "fnxconfig.h"
#include "fnxinfra.h"

struct fx_tentry {
	fx_thread_t *te_thread;
};
typedef struct fx_tentry   fx_tentry_t;


static void tentry_exec(void *arg)
{
	fx_tentry_t *te = (fx_tentry_t *)arg;
	free(te->te_thread);
	te->te_thread = NULL;
}

static void tentry_start(fx_tentry_t *te)
{
	fx_thread_t *thread;

	thread = (fx_thread_t *)malloc(sizeof(fx_thread_t));
	fx_assert(thread != NULL);

	te->te_thread = thread;

	fx_thread_start(thread, tentry_exec, te);
}

int main(void)
{
	size_t i, sz, nelems;
	fx_tentry_t *te = NULL;
	fx_tentry_t *te_arr;

	fx_set_default_sigactions();

	sz     = sizeof(fx_tentry_t);
	nelems = 128;
	te_arr = (fx_tentry_t *)(malloc(nelems * sz));

	for (i = 0; i < nelems; ++i) {
		te = &te_arr[i];
		tentry_start(te);
	}
	for (i = 0; i < nelems; ++i) {
		te = &te_arr[i];
		while (te->te_thread != NULL) {
			fx_usleep(1);
		}
	}

	free(te_arr);

	return 0;
}



/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FNXDEFS_H_
#define FNXDEFS_H_

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Generic common defines */

/* Booleans */
#define FNX_TRUE                (1)
#define FNX_FALSE               (0)

/* Common power-of-2 sizes */
#define FNX_KILO                (1ULL << 10)
#define FNX_MEGA                (1ULL << 20)
#define FNX_GIGA                (1ULL << 30)
#define FNX_TERA                (1ULL << 40)
#define FNX_PETA                (1ULL << 50)
#define FNX_EXA                 (1ULL << 60)

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Filsystem's base-constants */

/* Unique File-System type magic-number (see statfs(2) or kernel's magic.h) */
#define FNX_FSMAGIC             (0x16121973) /* Birthday */

/* File-system version number: */
#define FNX_FSVERSION           (1)


/* Frangmet-size: The size in bytes of the minimum allocation unit */
#define FNX_FRGSIZE             (512)

/* Block-size: I/O data unit size (bytes) */
#define FNX_BLKSIZE             (4096)

/* Number of fragments within block */
#define FNX_BLKNFRG             (8)

/* Space-chunk: Volume's sub-mapping unit */
#define FNX_SPCSIZE             (1 << 20) /* 1M */

/* Number of blocks per volume's space-mapping chunk */
#define FNX_SPCNBK              (256)

/* Section-size: Meta-data segment unit size (bytes) */
#define FNX_SECSIZE             (1 << 16) /* 64K */

/* Number of blocks within meta-data-section */
#define FNX_SECNBK              (16)

/* Number of fragments within meta-data-section */
#define FNX_SECNFRG             (128)

/* Segment size: File's sub-mapping region */
#define FNX_SEGSIZE             (1 << 18) /* 256K */

/* Number of blocks within file's segment */
#define FNX_SEGNBK              (64)

/* Slice size: File's sub-unit of segments */
#define FNX_SLICESIZE           (1 << 29) /* 512M */

/* Number of segments within file's slice */
#define FNX_SLICENSEG           (2048)

/* Storage-unit: Virtual-device sub-chunk */
#define FNX_STUSIZE             (1 << 22) /* 4M */

/* Total number of blocks per storage-unit chunk */
#define FNX_STUNBK              (1024)

/* Number of effective-blocks within storage-unit chunk */
#define FNX_STUENBK             (FNX_STUNBK - FNX_SECNBK)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Limits: */

/* Volume's min/max blocks-capacity */
#define FNX_VOLNBK_MIN          (FNX_SPCNBK)
#define FNX_VOLNBK_MAX          (FNX_SPCNBK * (1UL << 24))

/* Maximum number of slices in regular file */
#define FNX_NSLICES_MAX         (8192) /* 8K */

/* Maximum hard-links to inode */
#define FNX_LINK_MAX            (32768)

/* Maximum number of simultaneous active users */
#define FNX_NUSERS_MAX          (64)

/* Maximum groups per user */
#define FNX_NGROUPS_MAX         (32)

/* Maximum number of simultaneous mounts */
#define FNX_MOUNT_MAX           (32)

/* Maximum file-length (not including null terminator) */
#define FNX_NAME_MAX            (255)

/* Maximum path length (including NUL) */
#define FNX_PATH_MAX            (4096)

/* Maximum length for symbolic-link values (including NUL) */
#define FNX_SSYMLNK_MAX         (508)
#define FNX_SYMLNK_MAX          (4096) /* FNX_PATH_MAX */

/* Maximum number of simultaneous open file-handles */
#define FNX_OPENFH_MAX          (4096)

/* Maximum number of entries in single directory */
#define FNX_DIRNCHILDS_MAX      (131072) /* 128K */


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* On-disk Metadata definitions */

/* First mark-byte of every in-use fragment */
#define FNX_HDRMARK              ('F')

/* Meta-data version */
#define FNX_HDRVERS              (1)

/* Section-size: Meta-data segment unit size (bytes) */
#define FNX_DSECSIZE             (FNX_SECSIZE)

/* Size of each meta-data unit header (bytes) */
#define FNX_HDRSIZE              (32)

/* Size of meta-data sub-unit (bytes) */
#define FNX_RECORDSIZE           (64)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Special IDs: */

/* Special sub-volume ids */
#define FNX_SVOL_NULL           (0)
#define FNX_SVOL_META           (1)
#define FNX_SVOL_DATA           (2)

/* Special LBA values */
#define FNX_LBA_MAX             ((1ULL << 48) - 1)
#define FNX_LBA_NULL            (~(0ULL))
#define FNX_LBA_ZERO            (0)
#define FNX_LBA_PSTART          ((1ULL << 40) - 1)

/* Special section-index values */
#define FNX_SEC_MAX             (1ULL << 56)
#define FNX_SEC_SUPER           (0)

/* Fragment-index within special sections */
#define FNX_FRG_SUPER           (16)
#define FNX_FRG_ROOT            (32)

/* Special-types' values */
#define FNX_UID_NULL            (-1)
#define FNX_UID_MAX             (1 << 30)
#define FNX_GID_NULL            (-1)
#define FNX_OFF_NULL            (-1)

/* Special ino/xno numbers */
#define FNX_INO_NULL            (0)
#define FNX_INO_ROOT            (1)
#define FNX_INO_SUPER           (2)
#define FNX_INO_SPACE           (3)
#define FNX_INO_PSROOT          (11)
#define FNX_INO_APEX0           (0x100000)
#define FNX_INO_ANY             (~(0ULL))
#define FNX_INO_MAX             ((1ULL << 56) - 1)
#define FNX_XNO_NULL            (0)

/* Directory defs */
#define FNX_DIRTOP_NDENT        (223)
#define FNX_DIREXT_NDENT        (211)
#define FNX_DIREXT_NSEGS        (4096)
#define FNX_DIRSEG_NDENT        (59)
#define FNX_DIR_NSEGDENTS       (FNX_DIRSEG_NDENT * FNX_DIREXT_NSEGS)

#define FNX_DOFF_NONE           (FNX_OFF_NULL)
#define FNX_DOFF_SELF           (0)
#define FNX_DOFF_PARENT         (1)
#define FNX_DOFF_BEGIN          (2)
#define FNX_DOFF_BEGINX         (FNX_DIRTOP_NDENT)
#define FNX_DOFF_BEGINS         (FNX_DOFF_BEGINX + FNX_DIREXT_NDENT)
#define FNX_DOFF_END            (FNX_DOFF_BEGINS + FNX_DIR_NSEGDENTS)
#define FNX_DOFF_PROOT          (FNX_DOFF_END + 1)


/* Regular-file defs */
#define FNX_ROFF_NONE           (FNX_OFF_NULL)
#define FNX_ROFF_BEGIN          (0)
#define FNX_ROFF_END            ((1ULL * FNX_NSLICES_MAX) * FNX_SLICESIZE)

#define FNX_REGSIZE_MAX         (FNX_ROFF_END)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

/* Block's on-disk control flags */
#define FNX_BLKF_FUZZY          (1 << 0) /* Uninitialized */


/* Inodes on-disk control flags */
#define FNX_INODEF_ROOT         (1 << 0)
#define FNX_INODEF_EXT          (1 << 1)


/* Mount options */
#define FNX_MNTF_RDONLY         (1 << 0)
#define FNX_MNTF_NOSUID         (1 << 1)
#define FNX_MNTF_NODEV          (1 << 2)
#define FNX_MNTF_NOEXEC         (1 << 3)
#define FNX_MNTF_MANDLOCK       (1 << 6)
#define FNX_MNTF_DIRSYNC        (1 << 7)
#define FNX_MNTF_NOATIME        (1 << 10)
#define FNX_MNTF_NODIRATIME     (1 << 11)
#define FNX_MNTF_RELATIME       (1 << 21)

#define FNX_MNTF_STRICT         (1ULL << 33)

#define FNX_MNTF_DEFAULT  \
	(FNX_MNTF_NOATIME | FNX_MNTF_NODIRATIME)


/* Capability flags */
#define FNX_CAPF_CHOWN          (1 << 0)
#define FNX_CAPF_FOWNER         (1 << 3)
#define FNX_CAPF_FSETID         (1 << 4)
#define FNX_CAPF_ADMIN          (1 << 21)
#define FNX_CAPF_MKNOD          (1 << 27)

#define FNX_CAPF_ALL \
	(FNX_CAPF_CHOWN | FNX_CAPF_FOWNER | \
	 FNX_CAPF_FSETID | FNX_CAPF_ADMIN | FNX_CAPF_MKNOD)

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/* Setattr control flags (0-8 same as in FUSE) */
#define FNX_SETATTR_MODE        (1 << 0)
#define FNX_SETATTR_UID         (1 << 1)
#define FNX_SETATTR_GID         (1 << 2)
#define FNX_SETATTR_SIZE        (1 << 3)
#define FNX_SETATTR_ATIME       (1 << 4)
#define FNX_SETATTR_MTIME       (1 << 5)
#define FNX_SETATTR_ATIME_NOW   (1 << 7)
#define FNX_SETATTR_MTIME_NOW   (1 << 8)
#define FNX_SETATTR_BTIME       (1 << 9)
#define FNX_SETATTR_CTIME       (1 << 10)
#define FNX_SETATTR_BTIME_NOW   (1 << 11)
#define FNX_SETATTR_CTIME_NOW   (1 << 12)


/* Time specific control flags */
#define FNX_BTIME               FNX_SETATTR_BTIME
#define FNX_BTIME_NOW           FNX_SETATTR_BTIME_NOW
#define FNX_ATIME               FNX_SETATTR_ATIME
#define FNX_ATIME_NOW           FNX_SETATTR_ATIME_NOW
#define FNX_MTIME               FNX_SETATTR_MTIME
#define FNX_MTIME_NOW           FNX_SETATTR_MTIME_NOW
#define FNX_CTIME               FNX_SETATTR_CTIME
#define FNX_CTIME_NOW           FNX_SETATTR_CTIME_NOW

#define FNX_AMTIME              (FNX_ATIME | FNX_MTIME)
#define FNX_AMCTIME             (FNX_ATIME | FNX_MTIME | FNX_CTIME)
#define FNX_BAMCTIME            (FNX_BTIME | FNX_ATIME | FNX_MTIME | FNX_CTIME)
#define FNX_AMTIME_NOW          (FNX_ATIME_NOW | FNX_MTIME_NOW)
#define FNX_ACTIME_NOW          (FNX_ATIME_NOW | FNX_CTIME_NOW)
#define FNX_MCTIME_NOW          (FNX_MTIME_NOW | FNX_CTIME_NOW)
#define FNX_AMCTIME_NOW         (FNX_ATIME_NOW | FNX_MCTIME_NOW)
#define FNX_BAMCTIME_NOW        (FNX_BTIME_NOW | FNX_AMCTIME_NOW)
#define FNX_SETATTR_AMTIME_ANY  (FNX_AMTIME | FNX_AMTIME_NOW)


/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * On-disk magic numbers
 */
#define FNX_MAGIC1              (161803398)     /* Golden ratio */
#define FNX_MAGIC2              (271828182)     /* Euler's number e */
#define FNX_MAGIC3              (787499699)     /* Chaitin's Constant */
#define FNX_MAGIC4              (141421356)     /* sqrt(2) */
#define FNX_MAGIC5              (179424691)     /* 10000001st prime */
#define FNX_MAGIC6              (314159265)     /* Pi */
#define FNX_MAGIC7              (466920160)     /* Feigenbaum C1 */
#define FNX_MAGIC8              (0x15320B74)    /* Plastic number */
#define FNX_MAGIC9              (0x93C467E3)    /* Euler-Mascheroni */

#define FNX_SUPER_MAGIC         FNX_MAGIC1
#define FNX_INODE_MAGIC         FNX_MAGIC2
#define FNX_DIR_MAGIC           FNX_MAGIC3
#define FNX_DIREXT_MAGIC        FNX_MAGIC4
#define FNX_DIRSEG_MAGIC        FNX_MAGIC5
#define FNX_VSTAT_MAGIC         FNX_MAGIC6

/*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/
/*
 * Hash-functions types
 */
enum FNX_HFUNC {
	FNX_HFUNC_NONE      = 0,
	FNX_HFUNC_SIPHASH   = 2,
	FNX_HFUNC_BLAKE128  = 3,
	FNX_HFUNC_CUBE512   = 4,
	/* Complex types */
	FNX_HFUNC_SIP_DINO  = 71
};


/*
 * V-Elements/on-disk types
 */
enum FNX_VTYPE {
	FNX_VTYPE_NONE      = 0,        /* No-value (not-valid) */
	FNX_VTYPE_SUPER     = 1,        /* Super-block */
	FNX_VTYPE_SPMAP     = 2,        /* Space-mapping block */
	FNX_VTYPE_CHRDEV    = 11,       /* Char device file */
	FNX_VTYPE_BLKDEV    = 12,       /* Block device file */
	FNX_VTYPE_SOCK      = 13,       /* Socket */
	FNX_VTYPE_FIFO      = 14,       /* FIFO */
	FNX_VTYPE_SSYMLNK   = 15,       /* Symbolic link (short) */
	FNX_VTYPE_SYMLNK    = 16,       /* Symbolic link (long) */
	FNX_VTYPE_REFLNK    = 17,       /* Referenced link */
	FNX_VTYPE_DIR       = 21,       /* Directory head */
	FNX_VTYPE_DIREXT    = 22,       /* Directory extension */
	FNX_VTYPE_DIRSEG    = 23,       /* Directory sub-segment */
	FNX_VTYPE_REG       = 31,       /* Regular-file */
	FNX_VTYPE_SLICE     = 32,       /* Slice bitmap */
	FNX_VTYPE_SEGMNT    = 34,       /* Segment mapping */
	/* Shadows */
	FNX_VTYPE_BK        = 101,      /* Raw block */
	FNX_VTYPE_SEC       = 102,      /* Raw section */
	FNX_VTYPE_EMPTY     = 127,      /* Unused region */
	FNX_VTYPE_ANY       = 255       /* Anonymous */
};

#endif /* FNXDEFS_H_ */


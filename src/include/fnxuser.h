/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FNXUSER_H_
#define FNXUSER_H_

#include <stdlib.h>
#include <limits.h>


struct stat;
struct statvfs;

struct fnx_dirent {
	ino_t   d_ino;  /* Mapped ino */
	loff_t  d_off;  /* Next off */
	mode_t  d_type; /* Type as in stat */
	char    d_name[NAME_MAX + 1];
};
typedef struct fnx_dirent fnx_dirent_t;


struct fnx_mntent {
	char *target;
	char *options;
};
typedef struct fnx_mntent fnx_mntent_t;


/* Stat-operations wrappers */
int fnx_fstat(int, struct stat *);

int fnx_stat(const char *, struct stat *);

int fnx_lstat(const char *, struct stat *);

int fnx_statdev(const char *, struct stat *, dev_t *);

int fnx_statdir(const char *, struct stat *);

int fnx_statedir(const char *, struct stat *);

int fnx_statreg(const char *, struct stat *);

int fnx_statblk(const char *, struct stat *);

int fnx_statvol(const char *, struct stat *, mode_t);

int fnx_statsz(const char *, struct stat *, loff_t *);


int fnx_fstatvfs(int, struct statvfs *);

int fnx_statvfs(const char *, struct statvfs *);


int fnx_chmod(const char *, mode_t);

int fnx_fchmod(int, mode_t);


/* Dir-operations wrappers */
int fnx_mkdir(const char *, mode_t);

int fnx_rmdir(const char *);

int fnx_getdent(int, loff_t, fnx_dirent_t *);


/* I/O wrappers */
int fnx_create(const char *, mode_t, int *);

int fnx_open(const char *, int, mode_t, int *);

int fnx_close(int);

int fnx_read(int, void *, size_t, size_t *);

int fnx_pread(int, void *, size_t, loff_t, size_t *);

int fnx_write(int, const void *, size_t, size_t *);

int fnx_pwrite(int, const void *, size_t, loff_t, size_t *);

int fnx_lseek(int, loff_t, int, loff_t *);

int fnx_fsync(int);

int fnx_fdatasync(int);

int fnx_fallocate(int, int, loff_t, loff_t);

int fnx_truncate(const char *, loff_t);

int fnx_ftruncate(int, loff_t);


/* Path operations wrappers */
int fnx_access(const char *, int);

int fnx_link(const char *, const char *);

int fnx_unlink(const char *);

int fnx_rename(const char *, const char *);

int fnx_realpath(const char *, char **);

char *fnx_makepath(const char *, ...);

char *fnx_joinpath(const char *, const char *);

void fnx_freepath(char *);


/* Special */
int fnx_readlink(const char *, char *, size_t, size_t *);

int fnx_symlink(const char *, const char *);

int fnx_mkfifo(const char *, mode_t);


/* Mmap wrappers */
int fnx_mmap(int, loff_t, size_t, int, int, void **);

int fnx_munmap(void *, size_t);

int fnx_msync(void *, size_t, int);

/* Mount & file-system operations */
int fnx_fsboundary(const char *, char **);

int fnx_checkfusemnt(const char *);

int fnx_funexmnts(fnx_mntent_t [], size_t, size_t *);

int fnx_freemnts(fnx_mntent_t [], size_t);


/* Extended (ioctl) operations */
int fnx_fclone(int, int);

int fnx_fquery(int, unsigned long *);


#endif /* FNXUSER_H_ */



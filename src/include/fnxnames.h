/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FNXNAMES_H_
#define FNXNAMES_H_

/* Mount info */
#define FNX_FSNAME              "funex"

/* Fs-type string in /proc/self/mountinfo */
#define FNX_MNTFSTYPE           "fuse.funex"

/* Auxiliary-daemon IPC socket */
#define FNX_AUXDSOCK            "funex.sock"


/* Pseudo filesystem's root-dir name */
#define FNX_PSROOTNAME          ".funex"

/* Pseudo filename for ioctls */
#define FNX_PSIOCTLNAME         "ioctl"

/* Pseudo filename to control fs-halt */
#define FNX_PSHALTNAME          "halt"

/* Pseudo dirname for super-block repr */
#define FNX_PSSUPERDNAME        "super"

/* Pseudo filename to query meta-data stats */
#define FNX_PSVSTATSNAME        "vstats"

/* Pseudo filename to query operations stats */
#define FNX_PSOPSTATSNAME       "opstat"

/* Pseudo filename to query io-counters */
#define FNX_PSIOSTATSNAME       "iostat"

/* Pseudo filename to query layout configuration */
#define FNX_PSLAYOUTNAME        "layout"

/* Pseudo dirname for cache */
#define FNX_PSCACHEDNAME        "cache"

/* Pseudo filename to query cache-stats */
#define FNX_PSCSTATSNAME        "stats"

/* Pseudo filename to query cache-limits */
#define FNX_PSCLIMITSSNAME      "limits"


#endif /* FNXNAMES_H_ */


#ifndef FNXCONFIG_H_
#define FNXCONFIG_H_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif

#ifndef _LARGE_FILE_SOURCE
#define _LARGE_FILE_SOURCE 1
#endif

#endif /* FNXCONFIG_H_ */


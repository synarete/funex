/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              Funex Library                                  *
 *                                                                             *
 *  Copyright (C) 2014 Synarete                                                *
 *                                                                             *
 *  Funex library is a free software; you can redistribute it and/or modify it *
 *  under the terms of the GNU Lesser General Public License as published by   *
 *  the Free Software Foundation; either version 3 of the License, or (at your *
 *  option) any later version.                                                 *
 *                                                                             *
 *  Funex is distributed in the hope that it will be useful, but WITHOUT ANY   *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for    *
 *  more details.                                                              *
 *                                                                             *
 *  You should have received a copy of GNU Lesser General Public License       *
 *  along with the library. If not, see <http://www.gnu.org/licenses/>         *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
#ifndef FNXCORE_H_
#define FNXCORE_H_

#include "fnxdefs.h"
#include "fnxnames.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "core/meta.h"
#include "core/types.h"
#include "core/stats.h"
#include "core/proto.h"
#include "core/metai.h"
#include "core/index.h"
#include "core/htod.h"
#include "core/addr.h"
#include "core/addri.h"
#include "core/alloc.h"
#include "core/elems.h"
#include "core/execq.h"
#include "core/iobuf.h"
#include "core/super.h"
#include "core/inode.h"
#include "core/inodei.h"
#include "core/dir.h"
#include "core/diri.h"
#include "core/file.h"
#include "core/ioctl.h"
#include "core/vio.h"
#include "core/task.h"
#include "core/repr.h"

#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* FNXCORE_H_ */


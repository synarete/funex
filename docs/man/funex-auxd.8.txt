.. include:: common.txt

============
 funex-auxd
============

----------------------------------
Control and query auxiliary daemon
----------------------------------

:Date:           |date|
:Manual section: 8
:Manual group:   Funex Manual

..


SYNOPSIS
========
**funex auxd** [-s|--usock=<path>]
..


DESCRIPTION
===========
Control and query auxiliary daemon.

..


SEE ALSO
========
**funex**\(1)


COPYING
=======
|copyright|
..

|copying_gpl|
..



